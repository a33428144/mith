﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using System.Data.Linq.SqlClient;
using System.IO;
using Mith.Areas.GoX.Models;
using Mith.Models;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;

namespace Mith.Areas.GoX.Models
{
    public class ExpertArticleModel
    {
        #region Class
        public class ExpertArticleShow
        {
            [Key]
            [DisplayName("編號")]
            public int Id { get; set; }
            [DisplayName("達人")]
            public int? ExpertId { get; set; }
            public string Expert_Name { get; set; }
            [DisplayName("標題")]
            [Required(ErrorMessage = "必填欄位")]
            public string Title { get; set; }
            [DisplayName("出處網址")]
            [DataType(DataType.Url, ErrorMessage = "請輸入正確的網址格式")]
            public string FromUrl { get; set; }

            #region 圖片&段落
            [DisplayName("圖片1")]
            public string Pic1 { get; set; }
            [DisplayName("段落1")]
            [DataType(DataType.MultilineText)]
            public string Content1 { get; set; }
            [DisplayName("圖片2")]
            public string Pic2 { get; set; }
            [DisplayName("段落2")]
            [DataType(DataType.MultilineText)]
            public string Content2 { get; set; }
            [DisplayName("圖片3")]
            public string Pic3 { get; set; }
            [DisplayName("段落3")]
            [DataType(DataType.MultilineText)]
            public string Content3 { get; set; }
            [DisplayName("圖片4")]
            public string Pic4 { get; set; }
            [DisplayName("段落4")]
            [DataType(DataType.MultilineText)]
            public string Content4 { get; set; }
            [DisplayName("圖片5")]
            public string Pic5 { get; set; }
            [DisplayName("段落5")]
            [DataType(DataType.MultilineText)]
            public string Content5 { get; set; }

						[DisplayName("刪除圖片1")]
						public bool Delte_Pic1 { get; set; }

						[DisplayName("刪除圖片2")]
						public bool Delte_Pic2 { get; set; }

						[DisplayName("刪除圖片3")]
						public bool Delte_Pic3 { get; set; }

						[DisplayName("刪除圖片4")]
						public bool Delte_Pic4 { get; set; }

						[DisplayName("刪除圖片5")]
						public bool Delte_Pic5 { get; set; }
            #endregion

            #region 推薦商品
            public string RecommendSN1 { get; set; }
            public string RecommendSN2 { get; set; }
            public string RecommendSN3 { get; set; }
            public string RecommendSN4 { get; set; }
            public string RecommendSN5 { get; set; }
            #endregion
            [DisplayName("按讚數")]
            public int Like { get; set; }
            [DisplayName("上架")]
            public bool Display { get; set; }
            [DisplayName("建立時間")]
            public DateTime? CreateTime { get; set; }
            [DisplayName("更新時間")]
            public DateTime? UpdateTime { get; set; }
            [DisplayName("更新人員")]
            public int LastUserId { get; set; }
            [DisplayName("更新人員")]
            public string LastUser_Name { get; set; }
        }

        public string Keyword = "";
        public int Search_ExpertId = 0;
        public int Search_Display = 0;
        public DateTime? CreateTime_St = null;
        public DateTime? CreateTime_End = null;
        public string TypeSort = "CreateTime";
        #endregion
        public ExpertArticleModel()
        {
        }
        public void Clear()
        {
            Keyword = "";
            Search_ExpertId = 0;
            Search_Display = 0;
            CreateTime_St = null;
            CreateTime_End = null;
            TypeSort = "CreateTime";
        }

        #region Get

        public ExpertArticleShow Get_One(int id)
        {
            MithDataContext db = new MithDataContext();
            var data = (from i in db.Article_View
                        where i.Id == id
                        select new ExpertArticleShow
                        {
                            Id = i.Id,
                            ExpertId = i.ExpertId,
                            Title = i.Title,
                            FromUrl = i.FromUrl,

                            Pic1 = i.Pic1,
                            Content1 = HttpUtility.HtmlDecode(i.Content1),
                            Pic2 = i.Pic2,
                            Content2 = HttpUtility.HtmlDecode(i.Content2),
                            Pic3 = i.Pic3,
                            Content3 = HttpUtility.HtmlDecode(i.Content3),
                            Pic4 = i.Pic4,
                            Content4 = HttpUtility.HtmlDecode(i.Content4),
                            Pic5 = i.Pic5,
                            Content5 = HttpUtility.HtmlDecode(i.Content5),

                            RecommendSN1 = i.RecommendSN1,
                            RecommendSN2 = i.RecommendSN2,
                            RecommendSN3 = i.RecommendSN3,
                            RecommendSN4 = i.RecommendSN4,
                            RecommendSN5 = i.RecommendSN5,

                            Like = i.Like,
                            Display = i.Display,
                            CreateTime = i.CreateTime,
                            UpdateTime = i.UpdateTime,
                            LastUser_Name = i.LastUser_Name,
                        }).FirstOrDefault();
            db.Connection.Close();
            return data;
        }

        public List<ExpertArticleShow> Get_Data(int p = 1, int take = 10)
        {
            MithDataContext db = new MithDataContext();

            var data = Get_Sort(Get()).Skip((p - 1) * take).Take(take);

            List<ExpertArticleShow> item = new List<ExpertArticleShow>();

            item = (from i in data
                    select new ExpertArticleShow
                    {
                        Id = i.Id,
                        Title = i.Title,
                        ExpertId = i.ExpertId,
                        Expert_Name = i.Expert_Name,
                        Like = i.Like,
                        Display = i.Display,
                        CreateTime = i.CreateTime,
                    }).ToList();

            db.Connection.Close();
            return item;
        }

        public List<ExpertArticleShow> Get_Data_Popular(int p = 1, int take = 10)
        {
            MithDataContext db = new MithDataContext();

            var data = Get().Where(w=>w.Like > 0);

						#region 計算達人過去一個月文章按讚總和
						List<ExpertArticleShow> ArticleLikeData = db.ExecuteQuery<ExpertArticleShow>
							(@"Select " +
												"ArticleId as Id, " +
												"COUNT (ArticleId) as [Like] " +
								"From LikeArticle " +
								"Where DATEADD(hh,8,CreateTime)  >= DATEADD(hh,8, DateAdd(m,-1,GetDate()))" +
								"Group By " +
												"ArticleId "
							).ToList();
						#endregion

            List<ExpertArticleShow> item = new List<ExpertArticleShow>();

						ExpertArticleShow ExpertArticleData = new ExpertArticleShow();

						foreach (var s in data)
						{
							ExpertArticleData = new ExpertArticleShow
							{
								Id = s.Id,
								Title = s.Title,
								ExpertId = s.ExpertId,
								Expert_Name = s.Expert_Name,
								Display = s.Display,
								Like = ArticleLikeData.FirstOrDefault(f => f.Id == s.Id) != null ? ArticleLikeData.FirstOrDefault(f => f.Id == s.Id).Like : 0,
								CreateTime = s.CreateTime.Value
							};
							item.Add(ExpertArticleData);

						}

            item = item.OrderByDescending(o => o.Like).Take(20).ToList();

            db.Connection.Close();
            return item;
        }

        public Method.Paging Get_Page(int p = 1, int take = 10, bool Popular = false)
        {
            return Method.Get_Page(Get_Count(Popular), p, take);
        }

        public int Get_Count(bool Popular = false)
        {
            return Get().Count();
        }

        private IQueryable<Article_View> Get()
        {
            MithDataContext db = new MithDataContext();
            IQueryable<Article_View> data = db.Article_View.Where(w => w.ExpertId != null);

            if (Keyword != "")
            {
                data = data.Where(Query(Keyword));
            }
            if (Search_ExpertId != 0)
            {
                data = data.Where(w => w.ExpertId == Search_ExpertId);
            }
            if (Search_Display != 0)
            {
                if (Search_Display == 1)
                {
                    data = data.Where(w => w.Display == true);
                }
                else
                {
                    data = data.Where(w => w.Display == false);
                }
            }
            if (CreateTime_St != null)
            {
                data = data.Where(w => w.CreateTime >= CreateTime_St.Value);
            }
            if (CreateTime_End != null)
            {
                CreateTime_End = DateTime.Parse(CreateTime_End.Value.ToString("yyyy/MM/dd") + " 23:59:00");
                data = data.Where(w => w.CreateTime <= CreateTime_End);
            }

            db.Connection.Close();
            return data;
        }

        private IQueryable<Article_View> Get_Sort(IQueryable<Article_View> data)
        {
            switch (TypeSort)
            {
                case "CreateTime":
                    data = data.OrderByDescending(o => o.Id);
                    break;
                case "Like":
                    data = data.OrderByDescending(o => o.Like).ThenByDescending(o => o.Id);
                    break;
                default:
                    data = data.OrderByDescending(o => o.Id);
                    break;
            }
            return data;
        }

        private string Query(string query = "")
        {
            string sql = "";

            if (query != "")
            {
                query = HttpUtility.HtmlEncode(query);
                sql = Method.SQL_Combin(sql, "Title", "OR", "( \"" + query + "\")", ".Contains", false);
            }

            return sql;
        }

        #endregion

        #region insert
        public int Insert(ExpertArticleShow item)
        {
            MithDataContext db = new MithDataContext();
            Article new_item = new Article
            {
                Id = item.Id,
                ExpertId = item.ExpertId,
                Title = item.Title,
                FromUrl = item.FromUrl,

                Pic1 = item.Pic1,
                Content1 = HttpUtility.HtmlEncode(item.Content1),
                Pic2 = item.Pic2,
                Content2 = HttpUtility.HtmlEncode(item.Content2),
                Pic3 = item.Pic3,
                Content3 = HttpUtility.HtmlEncode(item.Content3),
                Pic4 = item.Pic4,
                Content4 = HttpUtility.HtmlEncode(item.Content4),
                Pic5 = item.Pic5,
                Content5 = HttpUtility.HtmlEncode(item.Content5),

                RecommendSN1 = item.RecommendSN1,
                RecommendSN2 = item.RecommendSN2,
                RecommendSN3 = item.RecommendSN3,
                RecommendSN4 = item.RecommendSN4,
                RecommendSN5 = item.RecommendSN5,

                Like = 0,
                Display = true,
								OnTop = false,
                CreateTime = DateTime.UtcNow,
                UpdateTime = DateTime.UtcNow,
                LastUserId = 0
								
            };
            db.Article.InsertOnSubmit(new_item);
            db.SubmitChanges();
            db.Connection.Close();
            return new_item.Id;
        }

        #endregion

        #region update

        public int Update(ExpertArticleShow item)
        {
            try
            {
                MithDataContext db = new MithDataContext();
                int Id = 0;
                var data = db.Article.FirstOrDefault(f => f.Id == item.Id);
                if (data != null)
                {
                    data.ExpertId = item.ExpertId;
                    data.Title = item.Title;
                    data.FromUrl = item.FromUrl;

                    data.Pic1 = item.Pic1;
                    data.Content1 = HttpUtility.HtmlEncode(item.Content1);
                    data.Pic2 = item.Pic2;
                    data.Content2 = HttpUtility.HtmlEncode(item.Content2);
                    data.Pic3 = item.Pic3;
                    data.Content3 = HttpUtility.HtmlEncode(item.Content3);
                    data.Pic4 = item.Pic4;
                    data.Content4 = HttpUtility.HtmlEncode(item.Content4);
                    data.Pic5 = item.Pic5;
                    data.Content5 = HttpUtility.HtmlEncode(item.Content5);

                    data.RecommendSN1 = item.RecommendSN1;
                    data.RecommendSN2 = item.RecommendSN2;
                    data.RecommendSN3 = item.RecommendSN3;
                    data.RecommendSN4 = item.RecommendSN4;
                    data.RecommendSN5 = item.RecommendSN5;

                    data.Display = item.Display;
                    data.UpdateTime = DateTime.UtcNow;
                    data.LastUserId = item.LastUserId;
                    db.SubmitChanges();
                    Id = data.Id;
                }
                db.Connection.Close();
                return Id;
            }
            catch { return -1; }
        }

        #endregion

        #region Delete
        public void Delete(int id = 0)
        {
            MithDataContext db = new MithDataContext();
            try
            {
                //文章
                var Article = db.Article.FirstOrDefault(w => w.Id == id);
                //文章留言
                var ArticleMessage = db.ArticleMessage.Where(w => w.ArticleId == id);
                //文章按讚
                var LikeArticle = db.LikeArticle.Where(w => w.ArticleId == id);

                if (Article != null)
                {
                    //先刪文章留言
                    if (ArticleMessage != null)
                    {
                        db.ArticleMessage.DeleteAllOnSubmit(ArticleMessage);
                        db.SubmitChanges();
                    }

                    //刪文章按讚
                    if (LikeArticle != null)
                    {
                        db.LikeArticle.DeleteAllOnSubmit(LikeArticle);
                        db.SubmitChanges();
                    }

                    //刪文章
                    db.Article.DeleteOnSubmit(Article);
                    db.SubmitChanges();
                }
            }
            catch (Exception e)
            {
            }
            finally
            {
                db.Connection.Close();
            }
        }

        #endregion



    }
}