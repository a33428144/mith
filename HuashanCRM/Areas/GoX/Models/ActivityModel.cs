﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using System.Text;
using System.Data.Linq.SqlClient;
using System.IO;
using System.Drawing;
using Mith.Areas.GoX.Models;
using Mith.Models;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using LinqToExcel;


namespace Mith.Areas.GoX.Models
{
	public class ActivityModel
	{
		#region Class
		public class ActivityShow
		{
			[Key]
			[DisplayName("編號")]
			public int Id { get; set; }
			[DisplayName("商品編號")]
			public string SN { get; set; }
			[DisplayName("商品名稱")]
			public string Name { get; set; }
			[DisplayName("商品照片")]
			public string Pic_Middle { get; set; }
			public string Pic_Small { get; set; }
			[DisplayName("是否置頂")]
			public bool OnTop { get; set; }
			[DisplayName("是否為前100展示")]
			public bool Promotion100 { get; set; }
			[DisplayName("是否為前30件熱銷")]
			public bool Hot30 { get; set; }
			[DisplayName("活動圖片Id")]
			public int? ActivityPicId { get; set; }
			public int Sort { get; set; }

			public string Pic1 { get; set; }
			public string Pic2 { get; set; }
			public string Pic3 { get; set; }
			public string Pic4 { get; set; }
			public string Pic5 { get; set; }
			public bool Delete_Pic1 { get; set; }
			public bool Delete_Pic2 { get; set; }
			public bool Delete_Pic3 { get; set; }
			public bool Delete_Pic4 { get; set; }
			public bool Delete_Pic5 { get; set; }
		}

		public string Search_Group = "";
		public bool IsSearch = false;
		public string Search_Area = "";
		public string Search_Pattern = "";
		public int Search_ActivityPicId = 0;

		public string Keyword = "";
		public int Search_Type = 0;
		public int Search_Season = 0;
		public int Search_Color = 0;
		public int Search_Style = 0;
		public int Search_Fit = 0;
		public int Search_SpecialOffer = 0;
		public int Search_CompanyId = 0;
		public DateTime? CreateTime_St = null;
		public DateTime? CreateTime_End = null;
		#endregion

		public ActivityModel()
		{
		}
		public void Clear()
		{
			Search_Group = "";
			IsSearch = false;
			Search_Area = "";
			Search_Pattern = "";
			Search_ActivityPicId = 0;

			Keyword = "";
			Search_Type = 0;
			Search_Season = 0;
			Search_Color = 0;
			Search_Style = 0;
			Search_Fit = 0;
			Search_SpecialOffer = 0;
			Search_CompanyId = 0;
			CreateTime_St = null;
			CreateTime_End = null;
		}

		#region Get

		public List<ActivityShow> Get_Data()
		{
			MithDataContext db = new MithDataContext();
			var data = Get();
			data = data.OrderByDescending(o => o.Sort);
			List<ActivityShow> item = new List<ActivityShow>();
			item = (from i in data
							select new ActivityShow
							{
								Id = i.Id,
								SN = i.SN,
								Name = i.Name,
								Pic_Middle = i.Pic_Middle,
								Pic_Small = i.Pic_Small,
								OnTop = i.OnTop,
								Promotion100 = i.Promotion100,
								Hot30 = i.Hot30,
								ActivityPicId = i.ActivityPicId,
								Sort = i.Sort,
							}).ToList();
			db.Connection.Close();

			return item;
		}

		DateTime LastWeek = DateTime.Parse(DateTime.UtcNow.AddHours(8).AddDays(-7).ToString("yyyy/MM/dd HH:") + "00:00");

		private IQueryable<Activity_View> Get()
		{
			MithDataContext db = new MithDataContext();
			IQueryable<Activity_View> data = db.Activity_View;

			//沒設搜尋條件
			if (IsSearch == false)
			{
				switch (Search_Group)
				{
					//【週週新品】條件：一星期內的商品、有設定為置頂、有商品圖片
					case "New":
						data = data.Where(w => w.CreateTime >= LastWeek && w.OnTop == true && w.Pic_Count > 0).OrderByDescending(o => o.Sort);
						break;
					//【百變穿搭】條件：有設定為前100件銷售
					case "Promotion100":
						data = data.Where(w => w.Promotion100 == true).OrderByDescending(o => o.Sort);
						break;
					//【熱銷 Top30】條件：有設定為置頂、有商品圖片
					case "Hot30":
						data = data.Where(w => w.Hot30 == true && w.Pic_Count > 0).OrderByDescending(o => o.Sort);
						break;
					//【活動圖片】條件：有設定活動圖片
					case "ActivityPic":
						data = data.Where(w => w.ActivityPicId == Search_ActivityPicId).OrderByDescending(o => o.Sort);
						break;
				}
			}
			//有設搜尋條件
			else
			{
				bool boo;
				if (Search_Area == "Left")
				{
					boo = true;
				}
				else
				{
					boo = false;
				}

				switch (Search_Group)
				{
					//【週週新品】條件：一星期內的商品、有商品圖片
					case "New":
						data = data.Where(w => w.CreateTime >= LastWeek && w.OnTop == boo && w.Pic_Count > 0).OrderByDescending(o => o.Id);
						break;
					//【百變穿搭】
					case "Promotion100":
						data = data.Where(w => w.Promotion100 == boo).OrderByDescending(o => o.Id);
						break;
					//【熱銷 Top30】條件：有商品圖片
					case "Hot30":
						data = data.Where(w => w.Hot30 == boo && w.Pic_Count > 0).OrderByDescending(o => o.Id);
						break;
					//【活動圖片】條件：沒設定活動圖片
					case "ActivityPic":
						data = data.Where(w => w.ActivityPicId == null).OrderByDescending(o => o.Id);
						break;
				}
			}
			if (Search_Pattern != "")
			{
				data = data.Where(w => w.Pattern == Search_Pattern);
			}

			if (Keyword != "")
			{
				data = data.Where(Query(Keyword));
			}
			if (Search_Type != 0)
			{
				data = data.Where(w => w.Type == Search_Type);
			}

			if (Search_Season != 0)
			{
				data = data.Where(w => w.Season == Search_Season);
			}

			if (Search_Color != 0)
			{
				data = data.Where(w => w.Color == Search_Color);
			}

			if (Search_Style != 0)
			{
				data = data.Where(w => w.Style.Contains(Search_Style.ToString()));
			}

			if (Search_Fit != 0)
			{
				data = data.Where(w => w.Fit.Contains(Search_Fit.ToString()));
			}

			if (Search_CompanyId != 0)
			{
				data = data.Where(w => w.CompanyId == Search_CompanyId);
			}

			if (CreateTime_St != null)
			{
				data = data.Where(w => w.CreateTime >= CreateTime_St.Value);
			}
			if (CreateTime_End != null)
			{
				CreateTime_End = DateTime.Parse(CreateTime_End.Value.ToString("yyyy/MM/dd") + " 23:59:00");
				data = data.Where(w => w.CreateTime <= CreateTime_End);

			}
			int i = data.Count();
			db.Connection.Close();
			return data;
		}

		public ActivityShow Get_One()
		{
			MithDataContext db = new MithDataContext();
			var data = (from i in db.ActivityPic
									where i.Id == 1
									select new ActivityShow
									{
										Pic1 = i.Pic1,
										Pic2 = i.Pic2,
										Pic3 = i.Pic3,
										Pic4 = i.Pic4,
										Pic5 = i.Pic5
									}).FirstOrDefault();
			db.Connection.Close();
			return data;
		}

		public Method.Paging Get_Page(int p = 1, int take = 10)
		{
			return Method.Get_Page(Get_Count(), p, take);
		}

		public int Get_Count()
		{
			return Get().Count();
		}

		private string Query(string query = "")
		{
			string sql = "";

			if (query != "")
			{
				query = HttpUtility.HtmlEncode(query);
				sql = Method.SQL_Combin(sql, "SN", "OR", "( \"" + query + "\")", ".Contains", false);
				sql = Method.SQL_Combin(sql, "Name", "OR", "( \"" + query + "\")", ".Contains", false);
				sql = Method.SQL_Combin(sql, "Brand_Name", "OR", "( \"" + query + "\")", ".Contains", false);
			}

			return sql;
		}

		#endregion

		#region update

		public int Update(ActivityModel.ActivityShow item)
		{
			try
			{
				MithDataContext db = new MithDataContext();
				var data = db.ActivityPic.FirstOrDefault(f => f.Id == 1);
				data.Pic1 = item.Pic1;
				data.Pic2 = item.Pic2;
				data.Pic3 = item.Pic3;
				data.Pic4 = item.Pic4;
				data.Pic5 = item.Pic5;
				db.SubmitChanges();
				db.Connection.Close();
				return 1;
			}
			catch { return -1; }
		}

		public void Ajax_UpdateOnTop(int Id = 0, bool OnTop = false)
		{
			try
			{
				MithDataContext db = new MithDataContext();
				var data = db.Article.FirstOrDefault(f => f.Id == Id);

				if (data != null)
				{
					data.OnTop = OnTop;
					db.SubmitChanges();
				}
				db.Connection.Close();
			}
			catch { }
		}

		#endregion

		#region Ajax

		//是否置頂
		public void Ajax_UpdateOnTop(int Id = 0, bool OnTop = false, int Sort = 1)
		{
			try
			{
				MithDataContext db = new MithDataContext();
				var data = db.Goods.FirstOrDefault(f => f.Id == Id);

				if (data != null)
				{
					data.OnTop = OnTop;
					if (OnTop == true)
					{
						data.Sort = Sort;
					}
					else
					{
						data.Sort = 0;
					}
					db.SubmitChanges();
				}
				db.Connection.Close();
			}
			catch { }
		}

		//是否為前100件展示款
		public void Ajax_UpdatePromotion100(int Id = 0, bool Promotion100 = false, int Sort = 1)
		{
			try
			{
				MithDataContext db = new MithDataContext();
				var data = db.Goods.FirstOrDefault(f => f.Id == Id);

				if (data != null)
				{
					data.Promotion100 = Promotion100;
					if (Promotion100 == true)
					{
						data.Sort = Sort;
					}
					else
					{
						data.Sort = 0;
					}
					db.SubmitChanges();
				}
				db.Connection.Close();
			}
			catch { }
		}

		//是否為前30件熱銷款
		public void Ajax_UpdateHot30(int Id = 0, bool Hot30 = false, int Sort = 1)
		{
			try
			{
				MithDataContext db = new MithDataContext();
				var data = db.Goods.FirstOrDefault(f => f.Id == Id);

				if (data != null)
				{
					data.Hot30 = Hot30;
					if (Hot30 == true)
					{
						data.Sort = Sort;
					}
					else
					{
						data.Sort = 0;
					}
					db.SubmitChanges();
				}
				db.Connection.Close();
			}
			catch { }
		}

		//更新活動圖片
		public void Ajax_UpdateActivityPic(int Id = 0, int ActivityPicId = 0, int Sort = 1)
		{
			try
			{
				MithDataContext db = new MithDataContext();
				var data = db.Goods.FirstOrDefault(f => f.Id == Id);

				if (data != null)
				{
					if (ActivityPicId != 0)
					{
						data.ActivityPicId = ActivityPicId;
						data.Sort = Sort;
					}
					else
					{
						data.ActivityPicId = null;
						data.Sort = 0;
					}
					db.SubmitChanges();
				}
				db.Connection.Close();
			}
			catch { }
		}
		#endregion

		#region 下拉式選單

		/// <summary>
		/// 取得品牌下拉選單
		/// </summary>
		/// <param name="companyId">廠商Id</param>
		/// <returns>選單</returns>
		public static List<SelectListItem> Get_Brand_SelectListItem(int companyId, int brandId)
		{
			MithDataContext db = new MithDataContext();
			List<SelectListItem> data = new List<SelectListItem>();
			var Brand = db.Brand.Where(w => w.CompanyId == companyId);
			if (Brand.Count() > 1)
			{
				data.Add(new SelectListItem
				{
					Selected = brandId == 0,
					Text = "請選擇或輸入文字",
					Value = "0"
				});
			}
			foreach (var item in Brand)
			{
				data.AddRange(new SelectListItem
				{
					Selected = brandId == item.Id,
					Text = item.Name,
					Value = item.Id.ToString()
				});
			}
			db.Connection.Close();
			return data;
		}

		#endregion

	}
}