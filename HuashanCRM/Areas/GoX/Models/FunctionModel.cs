﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mith.Models;
using System.Linq.Dynamic;

namespace Mith.Areas.GoX.Models
{
    public class FunctionModel
    {
        public class FunctionShow
        {
            [Key]
            [DisplayName("編號")]
            public int Id { get; set; }

            [DisplayName("分類名稱")]
            public int? CategoryId { get; set; }
            [DisplayName("分類名稱")]
            public string CategoryName { get; set; }

            [DisplayName("模組Controller")]
            public string Controller { get; set; }
            [DisplayName("模組首頁")]
            public string Action { get; set; }
            [DisplayName("類型")]
            public string Type { get; set; }
            [DisplayName("名稱")]
            public string Name { get; set; }

            [DisplayName("是否顯示")]
            public bool Display { get; set; }
            [DisplayName("排序")]
            public int Sort { get; set; }
        }

        public FunctionModel()
        { }

        public static Dictionary<string, string> Get_TypeList(bool superadmin)
        {
            Dictionary<string, string> tmp = new Dictionary<string, string>();
            if (superadmin)
            {
                tmp.Add(Admin, "SuperAdmin模組");
            }
            tmp.Add(System, "系統模組");
            tmp.Add(Module, "一般模組");
            return tmp;
        }

        public static List<SelectListItem> Get_TypeList(string type="")
        {
            List<SelectListItem> tmp = new List<SelectListItem>();
            /*tmp.Add(new SelectListItem
            {
                Selected = type == Admin,
                Text = "SuperAdmin模組",
                Value = Admin
            });
            tmp.Add(new SelectListItem
            {
                Selected = type == System,
                Text = "系統模組",
                Value = System
            });*/
            tmp.Add(new SelectListItem
            {
                Selected = type == Module,
                Text = "一般模組",
                Value = Admin
            });
            return tmp;
        }

        public static List<SelectListItem> Get_ControllerList()
        {
            List<SelectListItem> tmp = new List<SelectListItem>();
            tmp.Add(new SelectListItem
            {
                Selected = false,
                Text = "最新消息模組",
                Value = "News"
            });
            tmp.Add(new SelectListItem
            {
                Selected = false,
                Text = "商店模組",
                Value = "Store"
            });
            tmp.Add(new SelectListItem
            {
                Selected = false,
                Text = "相簿模組",
                Value = "Album"
            });
            tmp.Add(new SelectListItem
            {
                Selected = false,
                Text = "簡介模組",
                Value = "Introduction"
            });
            tmp.Add(new SelectListItem
            {
                Selected = false,
                Text = "聯絡我們模組",
                Value = "Contact"
            });
            tmp.Add(new SelectListItem
            {
                Selected = false,
                Text = "抽獎模組",
                Value = "Winning"
            });
            tmp.Add(new SelectListItem
            {
                Selected = false,
                Text = "考試模組",
                Value = "Quiz"
            });
            return tmp;
        }

        public class FunctionMenu
        {
            public int CId;
            public int FId;
            public int Sort;
            public string Controller;
            public string Action;
            public string Name;
            public bool Category;
            public bool Pass;
            public List<FunctionList> Item;
        }
        public class FunctionList
        {
            public int FId;
            public string Controller;
            public string Action;
            public string Name;
             public bool Pass;
        }

        public const string Admin = "Admin";
        public const string System = "System";
        public const string Module = "Module";
        public string Keyword = "";
        public List<int> Cid = new List<int>();
        public List<string> Type = new List<string>();
        public bool? Search_Enable = null;

        public void Clear()
        {
            Keyword = "";
            Cid = new List<int>();
            Type = new List<string>();
            Search_Enable = null;
        }

        #region get
        public static Function Get_One(int id)
        {
            MithDataContext db = new MithDataContext();
            var data = db.Function.FirstOrDefault(f => f.Id == id);
            db.Connection.Close();
            return data;
        }

        public static List<FunctionList> Get_MenuAdmin()
        {
            MithDataContext db = new MithDataContext();
            var data = (from i in db.Function
                        where i.Display && i.CategoryId == null && i.Type == Admin
                        orderby i.Sort, i.Id
                        select new FunctionList
                        {
                            FId = i.Id,
                            Action = i.Action,
                            Controller = i.Controller,
                            Name = i.Name,
                            Pass = true
                        }).ToList();
            db.Connection.Close();
            return data;
        }

        public static List<FunctionList> Get_MenuSystem(int user_level, bool admin)
        {
            MithDataContext db = new MithDataContext();
            var data = (from i in db.Function
                       where i.Display && i.CategoryId == null && i.Type == System
                       orderby i.Sort , i.Id
                       select new FunctionList
                       {
                           FId = i.Id,
                           Action = i.Action,
                           Controller = i.Controller,
                           Name = i.Name,
                           Pass = admin ? true : (db.CompetenceTable.FirstOrDefault(f => f.FunctionId == i.Id && f.UserLevelId == user_level) == null ? false :
                                       db.CompetenceTable.FirstOrDefault(f => f.FunctionId == i.Id && f.UserLevelId == user_level).Read)
                       }).ToList();
            db.Connection.Close();
            return data;
        }

        public static List<FunctionMenu> Get_Menu(int user_level, bool admin)
        {
            MithDataContext db = new MithDataContext();
            var data = (from i in db.Category
                        where  i.Display && i.Fun_Id == 0
                        orderby i.Sort, i.Id
                        select new FunctionMenu
                        {
                            CId = i.Id,
                            FId = 0,
                            Category = true,
                            Action = "",
                            Controller = "",
                            Name = i.Name,
                            Pass = true,
                            Sort = i.Sort,
                            Item = null
                            /*Item = db.Function.Where(w => w.CategoryId == i.Id && w.Display && w.Type == Module).OrderBy(o => o.Sort).Select(s => new FunctionList
                            {
                                FId = s.Id,
                                Action = s.Action,
                                Controller = s.Controller,
                                Name = s.Name,
                                Pass = db.CompetenceTable.FirstOrDefault(f => f.FunctionId == s.Id && f.UserLevelId == user_level) == null ? false :
                                         admin || ((db.CompetenceTable.FirstOrDefault(f => f.FunctionId == s.Id && f.UserLevelId == user_level).Competence & 1) > 0)
                            }).ToList()*/
                        }).Union(
                            from i in db.Function
                            where i.Display && i.CategoryId == null && i.Type == Module
                            orderby i.Sort, i.Id
                            select new FunctionMenu
                            {
                                CId = 0,
                                FId = i.Id,
                                Action = i.Action,
                                Controller = i.Controller,
                                Category = false,
                                Item = null,
                                Name = i.Name,
                                Sort = i.Sort,
                                Pass = admin ? true : (db.CompetenceTable.FirstOrDefault(f => f.FunctionId == i.Id && f.UserLevelId == user_level) == null ? false :
                                         db.CompetenceTable.FirstOrDefault(f => f.FunctionId == i.Id && f.UserLevelId == user_level).Read)
                            }
                       ).OrderByDescending(o => o.Category).ThenBy(t => t.Sort).ToList();
            foreach (var i in data)
            {
                if (i.Category)
                {
                    i.Item = db.Function.Where(w => w.CategoryId == i.CId && w.Display && w.Type == Module).OrderBy(o => o.Sort).Select(s => new FunctionList
                    {
                        FId = s.Id,
                        Action = s.Action,
                        Controller = s.Controller,
                        Name = s.Name,
                        Pass = admin ? true : (db.CompetenceTable.FirstOrDefault(f => f.FunctionId == s.Id && f.UserLevelId == user_level) == null ? false :
                                 db.CompetenceTable.FirstOrDefault(f => f.FunctionId == s.Id && f.UserLevelId == user_level).Read)
                    }).ToList();
                }
            }
            db.Connection.Close();
            return data;
        }

        public static bool Get_OneForCompetence(string name, int user_level)
        {
            MithDataContext db = new MithDataContext();
            var data = db.Function.FirstOrDefault(f => f.Name == name);
            if (data == null)
            {
                return false;
            }

            return db.CompetenceTable.FirstOrDefault(f => f.FunctionId == data.Id && f.UserLevelId == user_level) == null ? false :
                       db.CompetenceTable.FirstOrDefault(f => f.FunctionId == data.Id && f.UserLevelId == user_level).Read;
        }

        private List<FunctionShow> Convert(MithDataContext db, List<Function> data)
        {
            bool se = Search_Enable == null ? false : Search_Enable.Value;
            return data.Select(s => new FunctionShow
            {
                Id = s.Id,
                CategoryId = s.CategoryId,
                CategoryName = s.CategoryId!=null ?
                               (db.Category.FirstOrDefault(f => f.Fun_Id == 0 && f.Id == s.CategoryId.Value) != null ?
                               db.Category.FirstOrDefault(f => f.Fun_Id == 0 && f.Id == s.CategoryId.Value).Name : "未分類"
                               ) : se ? "" : "未分類",
                Action=s.Action,
                Controller = s.Controller,
                Type = s.Type,
                Name = s.Name,
                Display = s.Display,
                Sort = s.Sort
            }).ToList();
        }

        public FunctionShow Get_OneShow(int id)
        {
            MithDataContext db = new MithDataContext();
            FunctionShow item = null;
            var data = db.Function.FirstOrDefault(f => f.Id == id);
            if (data != null)
            {
                item = new FunctionShow
                {
                    Id = data.Id,
                    CategoryId = data.CategoryId,
                    CategoryName = data.CategoryId != null ?
                                   (db.Category.FirstOrDefault(f => f.Fun_Id == 0 && f.Id == data.CategoryId.Value ) != null ?
                                   db.Category.FirstOrDefault(f => f.Fun_Id == 0 && f.Id == data.CategoryId.Value).Name : "未分類"
                                   ) : "未分類",
                    Action = data.Action,
                    Controller = data.Controller,
                    Type = data.Type,
                    Name = data.Name,
                    Display = data.Display,
                    Sort = data.Sort
                };
            }
            db.Connection.Close();
            return item;
        }

        public List<FunctionShow> Get_Data(int p = 1, int take = 10)
        {
            try
            {
                MithDataContext db = new MithDataContext();

                var data = Get().OrderByDescending(o=>o.Id).Skip((p - 1) * take).Take(take);
                List<FunctionShow> item = Convert(db, data.ToList());
                db.Connection.Close();
                return item;
            }
            catch { return new List<FunctionShow>(); }
        }

        public Method.Paging Get_Page(int p = 1, int take = 10, int pages = 5)
        {
            return Method.Get_Page(Get_Count(), p, take, pages);
        }

        public int Get_Count()
        {
            try
            {
                return Get().Count();
            }
            catch { return 0; }
        }

        private IQueryable<Function> Get()
        {
            MithDataContext db = new MithDataContext();
            IQueryable<Function> data = db.Function;

            if (Keyword != "")
            {
                data = data.Where(Query(Keyword));
            }
            if (Search_Enable != null)
            {
                data = data.Where(w => w.Display == Search_Enable.Value);
            }
            if (Cid.Any())
            {
                data = data.Where(w => Cid.Contains(w.CategoryId.Value));
            }
            if (Type.Any())
            {
                data = data.Where(w => Type.Contains(w.Type));
            }
            db.Connection.Close();
            return data;
        }

        private string Query(string query = "")
        {
            string sql = "";

            if (query != "")
            {
                query = HttpUtility.HtmlEncode(query);
                sql = Method.SQL_Combin(sql, "Name", "OR", "( \"" + query + "\")", ".Contains", false);
            }

            return sql;
        }
        #endregion

        #region insert
        public int Insert(FunctionShow item)
        {
            try
            {
                MithDataContext db = new MithDataContext();
                Function new_item = new Function
                {
                   Action="Index",
                   CategoryId = item.CategoryId,
                   Display = item.Display,
                   Name = item.Name,
                   Sort = item.Sort,
                   Type = Module,
                   Controller = item.Controller
                };
                db.Function.InsertOnSubmit(new_item);
                db.SubmitChanges();
                db.Connection.Close();
                return new_item.Id;
            }
            catch { return -1; }
        }

        public void Module_Initial(string module, int fun_id, string name, int user_id)
        {
            MithDataContext db = new MithDataContext();
            switch (module)
            {
                case "Store":
                    db.CategoryConfig.InsertOnSubmit(new CategoryConfig
                    {
                        EnableCategory = false,
                        EnableIcon =false,
                        Fun_Id = fun_id
                    });
                    break;
            }
            db.SubmitChanges();
            db.Connection.Close();
        }
        #endregion

        #region update
        public int Update(FunctionShow item, bool admin)
        {
            MithDataContext db = new MithDataContext();
            var data = db.Function.FirstOrDefault(f => f.Id == item.Id);
            if (data != null)
            {
                if (data.Type == Module)
                {
                    data.Name = item.Name;
                    data.CategoryId = item.CategoryId;
                    data.Display = item.Display;
                    data.Sort = item.Sort;
                }
                if ((data.Type == System || data.Type == Admin) && admin)
                {
                    data.Display = item.Display;
                    data.Sort = item.Sort;
                }
                db.SubmitChanges();
                db.Connection.Close();
                return data.Id;
            }
            db.Connection.Close();
            return -1;
        }

        public int Update_Enable(int[] id, bool admin, bool display)
        {
            if (id.IsNotNullOrEmpty())
            {
                MithDataContext db = new MithDataContext();
                var data = db.Function.Where(w => id.Contains(w.Id));

                if (!admin)
                {
                    data = data.Where(w => w.Type == Module);
                }

                foreach (var i in data)
                {
                    i.Display = display;
                }
                db.SubmitChanges();
                db.Connection.Close();
                return data.Count();
            }
            return -1;
        }
        #endregion

        #region delete
        public void Delete(int id)
        {
            Delete(new int[] { id });
        }

        public void Delete(int[] id)
        {
            if (id.IsNotNullOrEmpty())
            {
                MithDataContext db = new MithDataContext();
                var data = db.Function.Where(w => id.Contains(w.Id) && w.Type==Module);
                if (data.Any())
                {
                    foreach (var i in data)
                    {
                    }
                    db.Function.DeleteAllOnSubmit(data);
                    db.SubmitChanges();
                }
                db.Connection.Close();
            }
        }
        #endregion
    }
}