﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using System.Data.Linq.SqlClient;
using System.IO;
using Mith.Areas.GoX.Models;
using Mith.Models;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;

namespace Mith.Areas.GoX.Models
{
    public class CompanyModel
    {
        #region Class
        public class CompanyShow
        {
            #region 登記資料
            [Key]
            [DisplayName("廠商編號")]
            public int Id { get; set; }
            [DisplayName("廠商編號")]
            [Required(ErrorMessage = "必填欄位")]
            [StringLength(3, MinimumLength = 3, ErrorMessage = "請輸入3碼數字")]
            [DataAnnotationsExtensions.Integer(ErrorMessage = "請輸入數字")]
            public string SN { get; set; }
            [DisplayName("名稱")]
            [Required(ErrorMessage = "必填欄位")]
            public string Name { get; set; }
            [DisplayName("統一編號")]
						[StringLength(8, MinimumLength = 8, ErrorMessage = "請輸入8碼數字")]
						[DataAnnotationsExtensions.Integer(ErrorMessage = "請輸入數字")]
            public string TaxId { get; set; }
            [DisplayName("電話")]
            [Required(ErrorMessage = "必填欄位")]
            public string Tel { get; set; }
            [DisplayName("傳真")]
            public string Fax { get; set; }
            [DisplayName("縣市")]
            public int CityId { get; set; }
            [DisplayName("區域")]
            public int AreaId { get; set; }
            [DisplayName("地址")]
            [Required(ErrorMessage = "必填欄位")]
            public string Address { get; set; }
            [DisplayName("負責人姓名")]
            public string BossName { get; set; }
            [DisplayName("負責人身分證字號")]
            public string BossIdentityCard { get; set; }
            [DisplayName("姓名")]
            public string ContactName { get; set; }
            [DisplayName("電話")]
            public string ContactTel { get; set; }
            [DisplayName("Email (系統通知)")]
            [Required(ErrorMessage = "必填欄位")]
            [DataType(DataType.EmailAddress)]
            [EmailAddress(ErrorMessage = "請輸入正確的電子信箱")]
            public string ContactEmail { get; set; }
            #endregion

            #region 合作類別 / 狀態
            [DisplayName("簽約日期")]

            public DateTime? ContractDate { get; set; }
            [DisplayName("攝影美編費用")]
            [DataAnnotationsExtensions.Integer(ErrorMessage = "請輸入數字")]
            public int? ArtEditorCost { get; set; }
            [DisplayName("拍攝來回運費")]
            [DataAnnotationsExtensions.Integer(ErrorMessage = "請輸入數字")]
            public int? FreightFee { get; set; }
            [DisplayName("管理費 %")]
            [DataAnnotationsExtensions.Integer(ErrorMessage = "請輸入數字")]
            public int? ManagementFee { get; set; }
            [DisplayName("合作中")]
            public bool Collaborate { get; set; }
            [DisplayName("分類")]
            public int Type { get; set; }
            public string TypeName { get; set; }
            [DisplayName("商品種類")]
            public string GoodsKind { get; set; }
            #endregion

            #region 帳務資料
            [DisplayName("姓名")]
            public string AccountingName { get; set; }
            [DisplayName("電話")]
            public string AccountingTel { get; set; }
            [DisplayName("Email (帳務通知)")]
            [DataType(DataType.EmailAddress)]
            [EmailAddress(ErrorMessage = "請輸入正確的電子信箱")]
            public string AccountingEmail { get; set; }
            [DisplayName("金融單位序號")]
            public string BankCode { get; set; }
            [DisplayName("金融單位")]
            public string BankName { get; set; }
            [DisplayName("帳號")]
            public string Account { get; set; }
            [DisplayName("帳戶名稱")]
            public string AccountName { get; set; }
            #endregion

            [DisplayName("建立時間")]
            public DateTime? CreateTime { get; set; }
            [DisplayName("更新時間")]
            public DateTime? UpdateTime { get; set; }
            [DisplayName("啟用")]
            public bool Enable { get; set; }
            [DisplayName("更新人員")]
            public int LastUserId { get; set; }
            [DisplayName("更新人員")]
            public string LastUser_Name { get; set; }

            public int Goods_Count { get; set; }
        }

        public class Export
        {
            [Key]
            public string SN { get; set; }
            public string Name { get; set; }
            public string TaxId { get; set; }
            public string Tel { get; set; }
            public string Fax { get; set; }
            public string City_Name { get; set; }
            public string Area_Name { get; set; }
            public string Address { get; set; }
            public string BossName { get; set; }
            public string BossIdentityCard { get; set; }
            public string ContactName { get; set; }
            public string ContactTel { get; set; }
            public string ContactEmail { get; set; }
            public string ContractDate { get; set; }
            public string ArtEditorCost { get; set; }
            public string FreightFee { get; set; }
            public string ManagementFee { get; set; }
            public string Collaborate { get; set; }
            public string TypeName { get; set; }
            public string GoodsKind { get; set; }
            public string AccountingName { get; set; }
            public string AccountingTel { get; set; }
            public string AccountingEmail { get; set; }
            public string BankCode { get; set; }
            public string BankName { get; set; }
            public string Account { get; set; }
            public string AccountName { get; set; }
            public string CreateTime { get; set; }
        }

        public string Keyword = "";
        public int Search_Collaborate = 0;
        public int Search_Type = -1;
        public int Search_GoodsKind = 0;
        public DateTime? CreateTime_St = null;
        public DateTime? CreateTime_End = null;

        #endregion
        public CompanyModel()
        {
        }
        public void Clear()
        {
            Keyword = "";
            Search_Collaborate = 0;
            Search_Type = -1;
            Search_GoodsKind = 0;
            CreateTime_St = null;
            CreateTime_End = null;
        }

        #region Get

        public CompanyShow Get_One_Step1(int id)
        {
            MithDataContext db = new MithDataContext();
            var data = (from i in db.Company_View
                        where i.Id == id
                        select new CompanyShow
                        {
                            SN = i.SN,
                            Name = i.Name,
                            TaxId = i.TaxId,
                            Tel = i.Tel,
                            Fax = i.Fax,
                            CityId = i.CityId ?? 0,
                            AreaId = i.AreaId ?? 0,
                            Address = i.Address,
                            BossName = i.BossName,
                            BossIdentityCard = i.BossIdentityCard,
                            ContactName = i.ContactName,
                            ContactTel = i.ContactTel,
                            ContactEmail = i.ContactEmail,
                            CreateTime = i.CreateTime,
                            UpdateTime = i.UpdateTime,
                            LastUser_Name = i.LastUser_Name,
                        }).FirstOrDefault();
            db.Connection.Close();
            return data;
        }

        public CompanyShow Get_One_Step2(int id)
        {
            MithDataContext db = new MithDataContext();
            var data = (from i in db.Company_View
                        where i.Id == id
                        select new CompanyShow
                        {
                            SN = i.SN,
                            Name = i.Name,
                            ContractDate = i.ContractDate ?? null,
                            ArtEditorCost = i.ArtEditorCost ?? 0,
                            FreightFee = i.FreightFee ?? 0,
                            ManagementFee = i.ManagementFee ?? 0,
                            Type = i.Type ?? 0,
                            TypeName = i.TypeName,
                            GoodsKind = i.GoodsKind,
                            Collaborate = i.Collaborate,
                            CreateTime = i.CreateTime,
                            UpdateTime = i.UpdateTime,
                            LastUser_Name = i.LastUser_Name,
                            Goods_Count = db.Goods.Where(w => w.CompanyId == id).Count()
                        }).FirstOrDefault();
            db.Connection.Close();
            return data;
        }

        public CompanyShow Get_One_Step3(int id)
        {
            MithDataContext db = new MithDataContext();
            var data = (from i in db.Company_View
                        where i.Id == id
                        select new CompanyShow
                        {
                            SN = i.SN,
                            Name = i.Name,
                            AccountingName = i.AccountingName,
                            AccountingTel = i.AccountingTel,
                            AccountingEmail = i.AccountingEmail,
                            BankCode = i.BankCode,
                            BankName = i.BankName,
                            Account = i.Account,
                            AccountName = i.AccountName,
                            CreateTime = i.CreateTime,
                            UpdateTime = i.UpdateTime,
                            LastUser_Name = i.LastUser_Name,
                        }).FirstOrDefault();
            db.Connection.Close();
            return data;
        }

        public List<CompanyShow> Get_Data(int p = 1, int take = 10)
        {
            MithDataContext db = new MithDataContext();
            var data = Get().OrderByDescending(o => o.Id).Skip((p - 1) * take).Take(take);
            List<CompanyShow> item = new List<CompanyShow>();
            item = (from i in data
                    select new CompanyShow
                    {
                        Id = i.Id,
                        SN = i.SN,
                        Name = i.Name,
                        Tel = i.Tel,
                        TypeName = i.TypeName,
                        GoodsKind = HttpUtility.HtmlDecode(Get_GoodsKinde_Name(i.GoodsKind)),
                        Collaborate = i.Collaborate,
                        CreateTime = i.CreateTime.Value
                    }).ToList();
            db.Connection.Close();
            return item;
        }
        public Method.Paging Get_Page(int p = 1, int take = 10)
        {
            return Method.Get_Page(Get_Count(), p, take);
        }

        public int Get_Count()
        {
            return Get().Count();
        }

        private IQueryable<Company_View> Get()
        {
            MithDataContext db = new MithDataContext();
            IQueryable<Company_View> data = db.Company_View;
            if (Keyword != "")
            {
                data = data.Where(Query(Keyword));
            }
            if (Search_Collaborate != 0)
            {
                if (Search_Collaborate == 1)
                {
                    data = data.Where(w => w.Collaborate == true);
                }
                else
                {
                    data = data.Where(w => w.Collaborate == false);
                }
            }
            if (Search_Type != -1)
            {
                data = data.Where(w => w.Type == Search_Type);
            }
            if (Search_GoodsKind != 0)
            {
                data = data.Where(w => w.GoodsKind.Contains(Search_GoodsKind.ToString()));
            }
            if (CreateTime_St != null)
            {
                data = data.Where(w => w.CreateTime >= CreateTime_St.Value);
            }
            if (CreateTime_End != null)
            {
                CreateTime_End = DateTime.Parse(CreateTime_End.Value.ToString("yyyy/MM/dd") + " 23:59:00");
                data = data.Where(w => w.CreateTime <= CreateTime_End);
            }
            db.Connection.Close();
            return data;
        }

        private string Query(string query = "")
        {
            string sql = "";

            if (query != "")
            {
                query = HttpUtility.HtmlEncode(query);
                sql = Method.SQL_Combin(sql, "SN", "OR", "( \"" + query + "\")", ".Contains", false);
                sql = Method.SQL_Combin(sql, "Name", "OR", "( \"" + query + "\")", ".Contains", false);
            }

            return sql;
        }

        #endregion

        #region insert
        public int Insert(CompanyShow item)
        {



            MithDataContext db = new MithDataContext();
            Company new_item = new Company
            {
                Id = item.Id,
                SN = item.Type + item.SN,
                Name = item.Name,
                TaxId = item.TaxId,
                Tel = item.Tel,
                Fax = item.Fax,
                CityId = item.CityId,
                AreaId = item.AreaId,
                Address = item.Address,
                BossName = item.BossName,
                BossIdentityCard = item.BossIdentityCard,
                ContactName = item.ContactName,
                ContactTel = item.ContactTel,
                ContactEmail = item.ContactEmail,
                ContractDate = item.ContractDate,
                ArtEditorCost = item.ArtEditorCost,
                FreightFee = item.FreightFee,
                ManagementFee = item.ManagementFee,
                Collaborate = item.Collaborate,
                Type = item.Type,
                GoodsKind = item.GoodsKind,
                AccountingName = item.AccountingName,
                AccountingTel = item.AccountingTel,
                AccountingEmail = item.AccountingEmail,
                BankCode = item.BankCode,
                BankName = item.BankName,
                Account = item.Account,
                AccountName = item.AccountName,
                CreateTime = DateTime.UtcNow,
                UpdateTime = DateTime.UtcNow,
                LastUserId = 0
            };
            db.Company.InsertOnSubmit(new_item);
            db.SubmitChanges();
            db.Connection.Close();
            return new_item.Id;
        }

        #endregion

        #region update

        public int Update_Step1(CompanyShow item)
        {
            try
            {
                MithDataContext db = new MithDataContext();
                int Id = 0;
                var data = db.Company.FirstOrDefault(f => f.Id == item.Id);
                if (data != null)
                {
                    data.Name = item.Name;
                    data.TaxId = item.TaxId;
                    data.Tel = item.Tel;
                    data.Fax = item.Fax;
                    data.CityId = item.CityId;
                    data.AreaId = item.AreaId;
                    data.Address = item.Address;
                    data.BossName = item.BossName;
                    data.BossIdentityCard = item.BossIdentityCard;
                    data.ContactName = item.ContactName;
                    data.ContactTel = item.ContactTel;
                    data.ContactEmail = item.ContactEmail;
                    data.UpdateTime = DateTime.UtcNow;
                    data.LastUserId = item.LastUserId;
                    db.SubmitChanges();
                    Id = data.Id;
                }
                db.Connection.Close();
                return Id;
            }
            catch { return -1; }
        }

        public int Update_Step2(CompanyShow item)
        {
            try
            {
                MithDataContext db = new MithDataContext();
                int Id = 0;
                var data = db.Company.FirstOrDefault(f => f.Id == item.Id);
                if (data != null)
                {
                    data.SN = item.Type + item.SN.Substring(1, 3);
                    data.ContractDate = item.ContractDate;
                    data.ArtEditorCost = item.ArtEditorCost;
                    data.FreightFee = item.FreightFee;
                    data.ManagementFee = item.ManagementFee;
                    data.Type = item.Type;
                    data.GoodsKind = item.GoodsKind;
                    data.Collaborate = item.Collaborate;
                    data.UpdateTime = DateTime.UtcNow;
                    data.LastUserId = item.LastUserId;
                    db.SubmitChanges();
                    Id = data.Id;
                }
                db.Connection.Close();
                return Id;
            }
            catch { return -1; }
        }

        public int Update_Step3(CompanyShow item)
        {
            try
            {
                MithDataContext db = new MithDataContext();
                int Id = 0;
                var data = db.Company.FirstOrDefault(f => f.Id == item.Id);
                if (data != null)
                {
                    data.AccountingName = item.AccountingName;
                    data.AccountingTel = item.AccountingTel;
                    data.AccountingEmail = item.AccountingEmail;
                    data.BankCode = item.BankCode;
                    data.BankName = item.BankName;
                    data.Account = item.Account;
                    data.AccountName = item.AccountName;
                    data.UpdateTime = DateTime.UtcNow;
                    data.LastUserId = item.LastUserId;
                    db.SubmitChanges();
                    Id = data.Id;
                }
                db.Connection.Close();
                return Id;
            }
            catch { return -1; }
        }

        #endregion

        #region Delete
        public void Delete(int Id = 0)
        {
            if (Id > 0)
            {
                MithDataContext db = new MithDataContext();

                var GoodsList = db.Goods.Where(w => w.CompanyId == Id);

                if (GoodsList.Any() && GoodsList != null)
                {
                    foreach (var item in GoodsList)
                    {
                        GoodsModel Goods = new GoodsModel();
                        Goods.Delete(item.Id);

                        GoodsSN_SettingModel GoodsSN_Setting = new GoodsSN_SettingModel();
                        GoodsSN_Setting.Delete_SN(item.SN);
                    }
                }

                var Brand = db.Brand.Where(w => w.CompanyId == Id);
                if (Brand.Any() && Brand != null)
                {
                    db.Brand.DeleteAllOnSubmit(Brand);
                    db.SubmitChanges();
                }

                var Company = db.Company.FirstOrDefault(w => w.Id == Id);
                if (Company != null)
                {
                    db.Company.DeleteOnSubmit(Company);
                    db.SubmitChanges();
                }
                db.Connection.Close();
            }
        }


        #endregion

        public string Get_GoodsKinde_Name(string GoodsKind)
        {
            MithDataContext db = new MithDataContext();

            try
            {
                var data = GoodsKind.Split(",").ToList();
                string name = "";
                foreach (var item in data)
                {
                    name += db.Category.FirstOrDefault(w => w.Fun_Id == 5 && w.Group == "GoodsKind" && w.Number == int.Parse(item)).Name + "、";
                }
                name = name.Substring(0, name.Length - 1);
                return name;
            }
            catch
            {
                return "";
            }
            finally
            {
                db.Connection.Close();
            }
        }

        #region Export
        public MemoryStream Set_Excel(List<Export> data)
        {
            //標題
            List<string> header = new List<string>();
            header.Add("廠商編號");
            header.Add("名稱");
            header.Add("統一編號");
            header.Add("電話");
            header.Add("傳真");
            header.Add("地址");
            header.Add("負責人-姓名");
            header.Add("負責人-身分證字號");
            header.Add("聯絡窗口-姓名");
            header.Add("聯絡窗口-電話");
            header.Add("聯絡窗口-Email");
            header.Add("簽約日期");
            header.Add("攝影美編費用");
            header.Add("拍攝來回運費");
            header.Add("管理費%");
            header.Add("合作中");
            header.Add("分類");
            header.Add("商品種類");
            header.Add("帳務處理-姓名");
            header.Add("帳務處理-電話");
            header.Add("帳務處理-Email");
            header.Add("金融單位");
            header.Add("帳號");
            header.Add("帳戶名稱");
            header.Add("建立時間");
            MemoryStream ms = new MemoryStream();
            HSSFWorkbook workbook = new HSSFWorkbook();
            HSSFSheet sheet = (HSSFSheet)workbook.CreateSheet("sheet");

            //宣告headStyle、內容style
            HSSFCellStyle headStyle = null;
            HSSFCellStyle contStyle = null;
            headStyle = (HSSFCellStyle)workbook.CreateCellStyle();
            contStyle = (HSSFCellStyle)workbook.CreateCellStyle();
            HSSFFont font = null;
            font = (HSSFFont)workbook.CreateFont();
            HSSFCell cell = null;

            //標題粗體、黃色背景
            headStyle.FillForegroundColor = HSSFColor.LightYellow.Index;
            headStyle.FillPattern = NPOI.SS.UserModel.FillPattern.SolidForeground;
            //標題字型樣式
            font.FontHeightInPoints = 10;
            font.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
            font.FontName = "Microsoft JhengHei";
            headStyle.SetFont(font);
            //內容字型樣式(自動換行)
            contStyle.WrapText = true;
            //contStyle.VerticalAlignment = NPOI.SS.UserModel.VerticalAlignment.Center;
            //宣告headRow
            sheet.CreateRow(0);
            //設定head背景、粗體、內容
            foreach (var i in header)
            {
                cell = (HSSFCell)sheet.GetRow(0).CreateCell(header.IndexOf(i));
                cell.SetCellValue(i);
                cell.CellStyle = headStyle;
            }
            //資料欄位
            for (int i = 0; i < data.Count; i++)
            {
                sheet.CreateRow(i + 1);

                sheet.GetRow(i + 1).CreateCell(0).SetCellValue(data[i].SN);
                sheet.GetRow(i + 1).CreateCell(1).SetCellValue(data[i].Name);
                sheet.GetRow(i + 1).CreateCell(2).SetCellValue(data[i].TaxId);
                sheet.GetRow(i + 1).CreateCell(3).SetCellValue(data[i].Tel);
                sheet.GetRow(i + 1).CreateCell(4).SetCellValue(data[i].Fax);
                sheet.GetRow(i + 1).CreateCell(5).SetCellValue(data[i].City_Name + data[i].Area_Name + data[i].Address);
                sheet.GetRow(i + 1).CreateCell(6).SetCellValue(data[i].BossName);
                sheet.GetRow(i + 1).CreateCell(7).SetCellValue(data[i].BossIdentityCard);
                sheet.GetRow(i + 1).CreateCell(8).SetCellValue(data[i].ContactName);
                sheet.GetRow(i + 1).CreateCell(9).SetCellValue(data[i].ContactTel);
                sheet.GetRow(i + 1).CreateCell(10).SetCellValue(data[i].ContactEmail);
                sheet.GetRow(i + 1).CreateCell(11).SetCellValue(data[i].ContractDate);
                sheet.GetRow(i + 1).CreateCell(12).SetCellValue(data[i].ArtEditorCost);
                sheet.GetRow(i + 1).CreateCell(13).SetCellValue(data[i].FreightFee);
                sheet.GetRow(i + 1).CreateCell(14).SetCellValue(data[i].ManagementFee);
                sheet.GetRow(i + 1).CreateCell(15).SetCellValue(data[i].Collaborate);
                sheet.GetRow(i + 1).CreateCell(16).SetCellValue(data[i].TypeName);
                sheet.GetRow(i + 1).CreateCell(17).SetCellValue(data[i].GoodsKind);
                sheet.GetRow(i + 1).CreateCell(18).SetCellValue(data[i].AccountingName);
                sheet.GetRow(i + 1).CreateCell(19).SetCellValue(data[i].AccountingTel);
                sheet.GetRow(i + 1).CreateCell(20).SetCellValue(data[i].AccountingEmail);
                sheet.GetRow(i + 1).CreateCell(21).SetCellValue("(" + data[i].BankCode + ")" + data[i].BankName);
                sheet.GetRow(i + 1).CreateCell(22).SetCellValue(data[i].Account);
                sheet.GetRow(i + 1).CreateCell(23).SetCellValue(data[i].AccountName);
                sheet.GetRow(i + 1).CreateCell(24).SetCellValue(data[i].CreateTime);
            }
            foreach (var i in header)
            {
                sheet.SetColumnWidth(header.IndexOf(i), (short)256 * 12);
            }

            workbook.Write(ms);
            ms.Position = 0;
            ms.Flush();
            return ms;
        }

        public List<Export> Export_Data(CompanyModel data)
        {

            /*-------------------匯出宣告-------------------*/
            MithDataContext db = new MithDataContext();
            Export export = new Export();
            List<Export> exp = new List<Export>();
            var item = data.Get().OrderByDescending(o => o.Id);
            /*-------------------匯出宣告End-------------------*/

            foreach (var i in item)
            {

                export.SN = i.SN;
                export.Name = i.Name;
                export.TaxId = i.TaxId;
                export.Tel = i.Tel;
                export.Fax = i.Fax;
                export.City_Name = i.City_Name;
                export.Area_Name = i.Area_Name;
                export.Address = i.Address;
                export.BossName = i.BossName;
                export.BossIdentityCard = i.BossIdentityCard;
                export.ContactName = i.ContactName;
                export.ContactTel = i.ContactTel;
                export.ContactEmail = i.ContactEmail;
                if (!string.IsNullOrWhiteSpace(i.ContractDate.ToString()))
                {
                    export.ContractDate = i.ContractDate.Value.ToString("yyyy/MM/dd HH:mm");
                }
                export.ArtEditorCost = i.ArtEditorCost.ToStringOrDefault();
                export.FreightFee = i.FreightFee.ToStringOrDefault();
                export.ManagementFee = i.ManagementFee.ToStringOrDefault();
                if (i.Collaborate == true)
                {
                    export.Collaborate = "是";
                }
                else
                {
                    export.Collaborate = "否";
                }
                export.TypeName = i.TypeName;
                export.GoodsKind = Get_GoodsKinde_Name(i.GoodsKind);
                export.AccountingName = i.AccountingName;
                export.AccountingTel = i.AccountingTel;
                export.AccountingEmail = i.AccountingEmail;
                export.BankCode = i.BankCode;
                export.BankName = i.BankName;
                export.Account = i.Account;
                export.AccountName = i.AccountName;
                export.CreateTime = i.CreateTime.Value.ToString("yyyy/MM/dd HH:mm");

                exp.Add(export);
                export = new Export();
            }
            db.Connection.Close();
            return exp;
        }
        #endregion

    }
}