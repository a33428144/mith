﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using System.Data.Linq.SqlClient;
using System.IO;
using Mith.Areas.GoX.Models;
using Mith.Models;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;

namespace Mith.Areas.GoX.Models
{
    public class ExpertModel
    {
        #region Class
        public class ExpertShow
        {
            [Key]
            [DisplayName("編號")]
            public int Id { get; set; }
            [DisplayName("姓名")]
            [Required(ErrorMessage = "必填欄位")]
            public string Name { get; set; }
            [DisplayName("大頭照")]
            public string Pic { get; set; }
            [DisplayName("封面照")]
            public string Cover { get; set; }
            [DisplayName("官方網址")]
            [DataType(DataType.Url, ErrorMessage = "請輸入正確的網址格式")]
            public string Url { get; set; }
            [DisplayName("簡介")]
            [DataType(DataType.MultilineText)]
            [Required(ErrorMessage = "必填欄位")]
            public string Content { get; set; }
            [DisplayName("上線")]
            public bool Enable { get; set; }
            [DisplayName("被收藏次數")]
            public int Like { get; set; }
            [DisplayName("建立時間")]
            public DateTime? CreateTime { get; set; }
            [DisplayName("更新時間")]
            public DateTime? UpdateTime { get; set; }
            [DisplayName("更新人員")]
            public int LastUserId { get; set; }
            [DisplayName("更新人員")]
            public string LastUser_Name { get; set; }

						[DisplayName("刪除封面照")]
						public bool Delete_Cover { get; set; }
        }

        public class Export
        {
            [Key]
            public string SN { get; set; }
            public string Name { get; set; }
            public string TaxId { get; set; }
            public string Tel { get; set; }
            public string Fax { get; set; }
            public string City_Name { get; set; }
            public string Area_Name { get; set; }
            public string Address { get; set; }
            public string BossName { get; set; }
            public string BossIdentityCard { get; set; }
            public string ContactName { get; set; }
            public string ContactTel { get; set; }
            public string ContactEmail { get; set; }
            public string ContractDate { get; set; }
            public string ArtEditorCost { get; set; }
            public string FreightFee { get; set; }
            public string ManagementFee { get; set; }
            public string Collaborate { get; set; }
            public string TypeName { get; set; }
            public string GoodsKind { get; set; }
            public string AccountingName { get; set; }
            public string AccountingTel { get; set; }
            public string AccountingEmail { get; set; }
            public string BankCode { get; set; }
            public string BankName { get; set; }
            public string Account { get; set; }
            public string AccountName { get; set; }
            public string CreateTime { get; set; }
        }

        public string Keyword = "";
        public int Search_Enable = 0;
        public DateTime? CreateTime_St = null;
        public DateTime? CreateTime_End = null;
        public string TypeSort = "CreateTime";
        #endregion
        public ExpertModel()
        {
        }
        public void Clear()
        {
            Keyword = "";
            Search_Enable = 0;
            CreateTime_St = null;
            CreateTime_End = null;
            TypeSort = "CreateTime";
        }

        #region Get

        public ExpertShow Get_One(int id)
        {
            MithDataContext db = new MithDataContext();
            var data = (from i in db.Expert_View
                        where i.Id == id
                        select new ExpertShow
                        {
                            Id = i.Id,
                            Name = i.Name,
                            Pic = i.Pic,
                            Cover = i.Cover,
                            Url = i.Url,
                            Content = HttpUtility.HtmlDecode(i.Content),
                            Enable = i.Enable,
                            Like = i.Like,
                            CreateTime = i.CreateTime,
                            UpdateTime = i.UpdateTime,
                            LastUser_Name = i.LastUser_Name,
                        }).FirstOrDefault();
            db.Connection.Close();
            return data;
        }

        public List<ExpertShow> Get_Data(int p = 1, int take = 10)
        {
            MithDataContext db = new MithDataContext();
            var data = Get_Sort(Get()).Skip((p - 1) * take).Take(take);
            List<ExpertShow> item = new List<ExpertShow>();
            item = (from i in data
                    select new ExpertShow
                    {
                        Id = i.Id,
                        Name = i.Name,
                        Pic = i.Pic,
                        Cover = i.Cover,
                        Enable = i.Enable,
                        Like = i.Like,
                        CreateTime = i.CreateTime,
                    }).ToList();
            db.Connection.Close();
            return item;
        }

				public List<ExpertShow> Get_Data_Popular(int p = 1, int take = 10)
				{
					MithDataContext db = new MithDataContext();

					var data = Get().Where(w => w.Like > 0);

					#region 計算達人過去一個月追蹤總和
					List<Article_View> ExpertLikeData = db.ExecuteQuery<Article_View>
						(@"Select " +
											"ExpertId, " +
											"COUNT (ExpertId) as [Like] " +
							"From LikeExpert " +
							"Where DATEADD(hh,8,CreateTime)  >= DATEADD(hh,8, DateAdd(m,-1,GetDate()))" +
							"AND ExpertId > 0" +
							"Group By " +
											"ExpertId "
						).ToList();
					#endregion
					
					List<ExpertShow> item = new List<ExpertShow>();

					ExpertShow ExpertData = new ExpertShow();

					foreach (var s in data)
					{
						ExpertData = new ExpertShow
						{
							Id = s.Id,
							Name = s.Name,
							Pic = s.Pic,
							Cover = s.Cover,
							Enable = s.Enable,
							//過去一個月追蹤達人總和
							Like = ExpertLikeData.FirstOrDefault(f => f.ExpertId == s.Id) != null ? ExpertLikeData.FirstOrDefault(f => f.ExpertId == s.Id).Like : 0,							
							CreateTime = s.CreateTime,
						};
						item.Add(ExpertData);
					}
					item = item.OrderByDescending(o => o.Like).Take(20).ToList();
					db.Connection.Close();
					return item;
				}
        public Method.Paging Get_Page(int p = 1, int take = 10)
        {
            return Method.Get_Page(Get_Count(), p, take);
        }

        public int Get_Count()
        {
            return Get().Count();
        }

        private IQueryable<Expert_View> Get()
        {
            MithDataContext db = new MithDataContext();
            IQueryable<Expert_View> data = db.Expert_View;
            if (Keyword != "")
            {
                data = data.Where(Query(Keyword));
            }
            if (Search_Enable != 0)
            {
                if (Search_Enable == 1)
                {
                    data = data.Where(w => w.Enable == true);
                }
                else
                {
                    data = data.Where(w => w.Enable == false);
                }
            }
            if (CreateTime_St != null)
            {
                data = data.Where(w => w.CreateTime >= CreateTime_St.Value);
            }
            if (CreateTime_End != null)
            {
                CreateTime_End = DateTime.Parse(CreateTime_End.Value.ToString("yyyy/MM/dd") + " 23:59:00");
                data = data.Where(w => w.CreateTime <= CreateTime_End);
            }
            db.Connection.Close();
            return data;
        }

        private IQueryable<Expert_View> Get_Sort(IQueryable<Expert_View> data)
        {
            switch (TypeSort)
            {
                case "CreateTime":
                    data = data.OrderByDescending(o => o.Id);
                    break;
                case "Like":
                    data = data.OrderByDescending(o => o.Like).ThenByDescending(o => o.Id);
                    break;
                default:
                    data = data.OrderByDescending(o => o.Id);
                    break;
            }
            return data;
        }
        private string Query(string query = "")
        {
            string sql = "";

            if (query != "")
            {
                query = HttpUtility.HtmlEncode(query);
                sql = Method.SQL_Combin(sql, "SN", "OR", "( \"" + query + "\")", ".Contains", false);
                sql = Method.SQL_Combin(sql, "Name", "OR", "( \"" + query + "\")", ".Contains", false);
            }

            return sql;
        }

        #endregion

        #region insert
        public int Insert(ExpertShow item)
        {
            MithDataContext db = new MithDataContext();
            Expert new_item = new Expert
            {
                Id = item.Id,
                Name = item.Name,
                Pic = item.Pic,
                Cover = item.Cover,
                Url = item.Url,
                Content = HttpUtility.HtmlEncode(item.Content),
                Enable = item.Enable,
                Like = 0,
                CreateTime = DateTime.UtcNow,
                UpdateTime = DateTime.UtcNow,
                LastUserId = 0
            };
            db.Expert.InsertOnSubmit(new_item);
            db.SubmitChanges();
            db.Connection.Close();
            return new_item.Id;
        }

        #endregion

        #region update

        public int Update(ExpertShow item)
        {
            try
            {
                MithDataContext db = new MithDataContext();
                int Id = 0;
                var data = db.Expert.FirstOrDefault(f => f.Id == item.Id);
                if (data != null)
                {
                    data.Name = item.Name;
                    data.Pic = item.Pic;
                    data.Cover = item.Cover;
                    data.Url = item.Url;
                    data.Content = HttpUtility.HtmlEncode(item.Content);
                    data.Enable = item.Enable;
                    data.UpdateTime = DateTime.UtcNow;
                    data.LastUserId = item.LastUserId;
                    db.SubmitChanges();
                    Id = data.Id;
                }
                db.Connection.Close();
                return Id;
            }
            catch { return -1; }
        }

        #endregion

        #region Delete
        public void Delete(int id = 0)
        {
            if (id > 0)
            {
                Delete(new int[] { id });
            }
        }

        public void Delete(int[] id)
        {
            if (id != null)
            {
                if (id.Any())
                {
                    MithDataContext db = new MithDataContext();
                    var data = db.Expert.Where(w => id.Contains(w.Id));
                    if (data.Any())
                    {
                        db.Expert.DeleteAllOnSubmit(data);

                        db.SubmitChanges();
                    }
                    db.Connection.Close();
                }
            }
        }

        #endregion

        #region Export
        public MemoryStream Set_Excel(List<Export> data)
        {
            //標題
            List<string> header = new List<string>();
            header.Add("廠商編號");
            header.Add("名稱");
            header.Add("統一編號");
            header.Add("電話");
            header.Add("傳真");
            header.Add("地址");
            header.Add("負責人姓名");
            header.Add("負責人身分證字號");

            MemoryStream ms = new MemoryStream();
            HSSFWorkbook workbook = new HSSFWorkbook();
            HSSFSheet sheet = (HSSFSheet)workbook.CreateSheet("sheet");

            //宣告headStyle、內容style
            HSSFCellStyle headStyle = null;
            HSSFCellStyle contStyle = null;
            headStyle = (HSSFCellStyle)workbook.CreateCellStyle();
            contStyle = (HSSFCellStyle)workbook.CreateCellStyle();
            HSSFFont font = null;
            font = (HSSFFont)workbook.CreateFont();
            HSSFCell cell = null;

            //標題粗體、黃色背景
            headStyle.FillForegroundColor = HSSFColor.LightYellow.Index;
            headStyle.FillPattern = NPOI.SS.UserModel.FillPattern.SolidForeground;
            //標題字型樣式
            font.FontHeightInPoints = 10;
            font.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
            font.FontName = "Microsoft JhengHei";
            headStyle.SetFont(font);
            //內容字型樣式(自動換行)
            contStyle.WrapText = true;
            //contStyle.VerticalAlignment = NPOI.SS.UserModel.VerticalAlignment.Center;
            //宣告headRow
            sheet.CreateRow(0);
            //設定head背景、粗體、內容
            foreach (var i in header)
            {
                cell = (HSSFCell)sheet.GetRow(0).CreateCell(header.IndexOf(i));
                cell.SetCellValue(i);
                cell.CellStyle = headStyle;
            }
            //資料欄位
            for (int i = 0; i < data.Count; i++)
            {
                sheet.CreateRow(i + 1);

                sheet.GetRow(i + 1).CreateCell(0).SetCellValue(data[i].SN);
                sheet.GetRow(i + 1).CreateCell(1).SetCellValue(data[i].Name);
                sheet.GetRow(i + 1).CreateCell(2).SetCellValue(data[i].TaxId);
                sheet.GetRow(i + 1).CreateCell(3).SetCellValue(data[i].Tel);
                sheet.GetRow(i + 1).CreateCell(4).SetCellValue(data[i].Fax);
                sheet.GetRow(i + 1).CreateCell(5).SetCellValue(data[i].City_Name + data[i].Area_Name + data[i].Address);
                sheet.GetRow(i + 1).CreateCell(6).SetCellValue(data[i].BossName);
                sheet.GetRow(i + 1).CreateCell(7).SetCellValue(data[i].BossIdentityCard);
                sheet.GetRow(i + 1).CreateCell(8).SetCellValue(data[i].ContactName);
                sheet.GetRow(i + 1).CreateCell(9).SetCellValue(data[i].ContactTel);
                sheet.GetRow(i + 1).CreateCell(10).SetCellValue(data[i].ContactEmail);
                sheet.GetRow(i + 1).CreateCell(11).SetCellValue(data[i].ContractDate);
                sheet.GetRow(i + 1).CreateCell(12).SetCellValue(data[i].ArtEditorCost);
                sheet.GetRow(i + 1).CreateCell(13).SetCellValue(data[i].FreightFee);
                sheet.GetRow(i + 1).CreateCell(14).SetCellValue(data[i].ManagementFee);
                sheet.GetRow(i + 1).CreateCell(15).SetCellValue(data[i].Collaborate);
                sheet.GetRow(i + 1).CreateCell(16).SetCellValue(data[i].TypeName);
                sheet.GetRow(i + 1).CreateCell(17).SetCellValue(data[i].GoodsKind);
                sheet.GetRow(i + 1).CreateCell(18).SetCellValue(data[i].AccountingName);
                sheet.GetRow(i + 1).CreateCell(19).SetCellValue(data[i].AccountingTel);
                sheet.GetRow(i + 1).CreateCell(20).SetCellValue(data[i].AccountingEmail);
                sheet.GetRow(i + 1).CreateCell(21).SetCellValue(data[i].BankCode);
                sheet.GetRow(i + 1).CreateCell(22).SetCellValue(data[i].BankName);
                sheet.GetRow(i + 1).CreateCell(23).SetCellValue(data[i].Account);
                sheet.GetRow(i + 1).CreateCell(24).SetCellValue(data[i].AccountName);
                sheet.GetRow(i + 1).CreateCell(25).SetCellValue(data[i].CreateTime);
            }
            foreach (var i in header)
            {
                sheet.SetColumnWidth(header.IndexOf(i), (short)256 * 12);
            }

            workbook.Write(ms);
            ms.Position = 0;
            ms.Flush();
            return ms;
        }

        public List<Export> Export_Data(ExpertModel data)
        {

            /*-------------------匯出宣告-------------------*/
            MithDataContext db = new MithDataContext();
            Export export = new Export();
            List<Export> exp = new List<Export>();
            var item = data.Get().OrderByDescending(o => o.Id);
            /*-------------------匯出宣告End-------------------*/

            foreach (var i in item)
            {

                export.Name = i.Name;
                export.CreateTime = i.CreateTime.Value.ToString("yyyy/MM/dd HH:mm");

                exp.Add(export);
                export = new Export();
            }
            db.Connection.Close();
            return exp;
        }
        #endregion

    }
}