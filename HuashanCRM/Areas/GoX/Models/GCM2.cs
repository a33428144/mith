﻿/*using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Mith.Models;
using System.Net;
using Newtonsoft.Json;
using System.Text;
using System.IO;
using Newtonsoft.Json.Linq;

namespace Mith.Models
{
    public class GCM2
    {
        public class ErrorToken
        {
            public PushToken token { get; set; }
            public string error { get; set; }
        }

        public static void PushMessage(PushApp app)
        {
            MithDataContext db = new MithDataContext();

            try
            {
                List<PushToken> RID = db.PushToken.Where(w => w.AppID == app.Id).ToList();
                List<PushMessage> news = db.PushMessage.Where(n => n.AppID == app.Id && n.Content != "").ToList();

                int RID_count = RID.Count();
                List<ErrorToken> resent = new List<ErrorToken>();

                if (news.Any() && RID_count > 0)
                {
                    //產生json
                    List<JObject> post = Get_Send_Message_Android(news, RID, RID_count);
                    int count = 0;
                    foreach (JObject i in post)
                    {
                        //送出json, 取得結果
                        JObject result = Android_Post(i, app.Password);

                        //處理結果
                        JArray results = (JArray)result["results"];
                        List<ErrorToken> temp = Process_Result(db, results, RID[count++]);
                        if (count == RID_count)
                        {
                            count = 0;
                        }

                        if (temp.Any())
                        {
                            resent.AddRange(temp);
                        }
                    }

                    //清除已送過
                    db.PushMessage.DeleteAllOnSubmit(news);
                    db.SubmitChanges();
                }

                //啟動重送機制
                int resent_count = resent.Count();
                if (resent_count > 0)
                {
                    Process_Resent(db, app.Id, app.Password, news, resent, resent_count);
                }

                var repeat3 = from i in db.PushToken
                              where i.AppID == app.Id
                              group i by i.DeviceID into g
                              select g.Max(m => m.Id);
                if (repeat3 != null)
                    if (repeat3.Any())
                    {
                        var repeat4 = from i in db.PushToken
                                      where i.AppID == app.Id
                                        && !repeat3.Contains(i.Id)
                                      select i;
                        db.PushToken.DeleteAllOnSubmit(repeat4);

                        db.SubmitChanges();
                    }
            }
            catch (Exception e)
            {
                db.PushError.InsertOnSubmit(
                    new PushError
                    {
                        AppID = app.Id,
                        CreateTime = DateTime.UtcNow,
                        DeviceID = "",
                        ErrorMessage = e.Message,
                        Message = ""
                    });
                db.SubmitChanges();
            }
        }

        private static List<JObject> Get_Send_Message_Android(List<PushMessage> data, List<PushToken> RID, int RID_count)
        {
            List<JObject> post = new List<JObject>();
            foreach (var i in data)
            {
                JObject message = new JObject();
                message.Add(new JProperty("id", i.Id));
                message.Add(new JProperty("title", i.Title));
                message.Add(new JProperty("content", i.Content));
                message.Add(new JProperty("update_time", i.CreateTime));

                //int count = 0;
                foreach (var j in RID)
                {
                    JArray ja = new JArray();
                    ja.Add(new JValue(j.DeviceID));
                    //count++;
                    post.Add(new JObject(
                           new JProperty("data", message),
                           new JProperty("registration_ids", ja)
                           ));
                }
            }

            return post;
        }

        private static JObject Android_Post(JObject post, string API_Key)
        {
            //string API_Key = "AIzaSyDqbsMeNlq27Id6exxwFKcteEAhJBevaqk";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://android.googleapis.com/gcm/send");
            request.Method = "POST";
            request.ContentType = "application/json";
            request.Headers.Add(string.Format("Authorization: key={0}", API_Key));

            byte[] byteArray = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(post));
            request.ContentLength = byteArray.Length;

            Stream dataStream = request.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();

            //發出Request
            WebResponse response = request.GetResponse();
            Stream responseStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(responseStream);
            string responseStr = reader.ReadToEnd();
            reader.Close();
            responseStream.Close();
            response.Close();

            return (JObject)JsonConvert.DeserializeObject(responseStr);
        }

        private static List<ErrorToken> Process_Result(MithDataContext db, JArray result, PushToken RID)
        {
            List<ErrorToken> resent = new List<ErrorToken>();
            if (result.HasValues)
            {
                //int count = 0;
                foreach (var k in result)
                {
                    var data = db.PushToken.FirstOrDefault(p => p.DeviceID == RID.DeviceID);
                    if (data != null)
                    {
                        //成功
                        if (k["message_id"] != null)
                        {
                            //成功，但須改變ID
                            if (k["registration_id"] != null)
                            {
                                string did = k["registration_id"].ToString();
                                data.DeviceID = did;
                            }
                            data.LastSendTime = DateTime.UtcNow;
                            db.SubmitChanges();
                        }
                        //
                        else if (k["error"] != null)
                        {
                            string error = k["error"].ToString();
                            switch (error)
                            {
                                //刪除
                                case "InvalidRegistration":
                                case "NotRegistered":
                                    db.PushToken.DeleteOnSubmit(data);
                                    db.SubmitChanges();
                                    break;
                                //重送
                                //case "Unavailable":
                                default:
                                    resent.Add(new ErrorToken
                                    {
                                        token = RID,
                                        error = error
                                    });
                                    break;
                            }
                        }
                        else { }
                        //count++;
                    }
                }
            }
            return resent;
        }

        private static void Process_Resent(MithDataContext db, int appid, string API_Key, List<PushMessage> data, List<ErrorToken> error, int resent_count)
        {
            List<PushToken> resent = error.Select(s => s.token).ToList(); //new List<PushToken>();

            //產生json
            List<JObject> post = Get_Send_Message_Android(data, resent, resent_count);

            List<PushError> fail = new List<PushError>();
            int count = 0;
            foreach (JObject i in post)
            {
                string msg = i["data"]["content"].ToString();

                //送出json, 取得結果
                JObject result = Android_Post(i, API_Key);

                //處理結果
                JArray results = (JArray)result["results"];
                List<ErrorToken> temp = Process_Result(db, results, resent[count]);
                List<PushToken> temp2 = new List<PushToken>();

                if (temp.Any())
                {
                    foreach (ErrorToken j in temp)
                    {
                        fail.Add(new PushError
                        {
                            DeviceID = j.token.DeviceID,
                            CreateTime = DateTime.UtcNow,
                            ErrorMessage = j.error,
                            Message = msg,
                            AppID = appid

                        });
                        j.token.LastSendTime = DateTime.UtcNow;
                        temp2.Add(j.token);
                    }

                    if (fail.Count > 0)
                    {
                        db.PushError.InsertAllOnSubmit(fail);
                        db.PushToken.DeleteAllOnSubmit(temp2);
                        db.SubmitChanges();
                    }
                }
            }
        }
    }
}*/