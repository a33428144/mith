﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using Mith.Models;
using System.IO;

namespace Mith.Areas.GoX.Models
{
    public class PushTokenModel
    {
        public class PushTokenShow
        {
            [Key]
            [DisplayName("編號")]
            public int Id { get; set; }
            [DisplayName("Token")]
            public string Token { get; set; }
            [DisplayName("RandomCode")]
            public int RandomCode { get; set; }
            [DisplayName("App代號")]
            public int AppId { get; set; }
            [DisplayName("App名稱")]
            public string AppName { get; set; }
            [DisplayName("BadgeCount")]
            public int BadgeCount { get; set; }
            [DisplayName("建立時間")]
            public DateTime CreateTime { get; set; }
            [DisplayName("接收時間")]
            public DateTime UpdateTime { get; set; }
            [DisplayName("推播時間")]
            public DateTime? LastSendTime { get; set; }
            [DisplayName("帳號(E-Mail)")]
            public string Account { get; set; }
            [DisplayName("會員編號")]
            public int VolunteerId { get; set; }
            [DisplayName("會員編號")]
            public string VolunteerName { get; set; }
        }

        public PushTokenModel(int fun)
        {
            Fun_Id = fun;
        }

        public string Keyword = "";
        public string Sort = "";
        private int Fun_Id;
        public DateTime? send_time_start = null;
        public DateTime? send_time_end = null;
        public DateTime? update_time_start = null;
        public DateTime? update_time_end = null;
        public int AppId = 0;

        public void Clear()
        {
            Keyword = "";
            Sort = "";
            AppId = 0;
            send_time_start = null;
            send_time_end = null;
            update_time_start = null;
            update_time_end = null;
        }

        private PushTokenShow Convert(PushToken data)
        {
            VolunteersModel vm = new VolunteersModel();
            var Vol = vm.Get_One(data.VolunteerId);
            PushTokenShow item = new PushTokenShow
            {
                Id = data.Id,
                AppId = data.AppID,
                AppName = "",
                BadgeCount = data.BadgeCount,
                CreateTime = data.CreateTime,
                Token = data.Token,
                LastSendTime = data.LastSendTime,
                UpdateTime = data.UpdateTime,
                VolunteerId = data.VolunteerId,
                VolunteerName = ""
            };
            if (Vol != null)
            {
                item.VolunteerName = Vol.RealName;
            }
            PushAppModel pam = new PushAppModel(Fun_Id);
            var pdata = pam.Get_One(data.AppID);
            if (pdata != null)
            {
                item.AppName = pdata.AppName + "(" + pdata.OS_Name + ")";
            }
            return item;
        }

        #region get
        public PushTokenShow Get_One(int id)
        {
            try
            {
                MithDataContext db = new MithDataContext();

                var data = db.PushToken.FirstOrDefault(f => f.Id == id);
                if (data != null)
                {
                    return Convert(data);
                }
                db.Connection.Close();
            }
            catch { }
            return null;
        }
        public PushToken Get_OneByTokenID(int appid, string tokenID)
        {
            try
            {
                MithDataContext db = new MithDataContext();
                var data = db.PushToken.FirstOrDefault(f => f.AppID == appid && f.Token == tokenID);
                db.Connection.Close();
                return data;
            }
            catch { }
            return null;
        }
        public List<PushTokenShow> Get_Data(int p = 1, int take = 10)
        {
            try
            {
                MithDataContext db = new MithDataContext();

                List<PushTokenShow> item = Get_Sort(Get()).Skip((p - 1) * take).Take(take).ToList();
                db.Connection.Close();
                return item;
            }
            catch { return new List<PushTokenShow>(); }
        }

        public List<PushTokenShow> Get_Data2(int p = 1, int take = 10)
        {
            try
            {
                MithDataContext db = new MithDataContext();

                List<PushTokenShow> item = Get().OrderByDescending(o => o.UpdateTime).Skip((p - 1) * take).Take(take).ToList();
                db.Connection.Close();
                return item;
            }
            catch { return new List<PushTokenShow>(); }
        }

        public Method.Paging Get_Page(int p = 1, int take = 10, int pages = 5)
        {
            return Method.Get_Page(Get_Count(), p, take, pages);
        }

        public int Get_Count()
        {
            try
            {
                return Get().Count();
            }
            catch { return 0; }
        }

        private IQueryable<PushTokenShow> Get()
        {
            MithDataContext db = new MithDataContext();
            IQueryable<PushTokenShow> data = db.PushToken.Select(s => new PushTokenShow
            {
                Id = s.Id,
                AppId = s.AppID,
                AppName = db.PushApp.FirstOrDefault(f => f.Id == s.AppID && f.Del == false) == null ?
                            "" : db.PushApp.FirstOrDefault(f => f.Id == s.AppID && f.Del == false).AppName,
                RandomCode = s.RandomCode,
                BadgeCount = s.BadgeCount,
                CreateTime = s.CreateTime,
                Token = s.Token.Substring(0, 30),
                LastSendTime = s.LastSendTime,
                UpdateTime = s.UpdateTime,
                VolunteerId = s.VolunteerId,
                VolunteerName = db.Volunteers.FirstOrDefault(f => f.Id == s.VolunteerId) == null ?
                "" : db.Volunteers.FirstOrDefault(f => f.Id == s.VolunteerId).RealName,
            });

            if (Keyword != "")
            {
                data = data.Where(Query(Keyword));
            }
            if (AppId != 0)
            {
                data = data.Where(w => w.AppId == AppId);
            }
            if (send_time_start != null)
            {
                data = data.Where(w => w.LastSendTime >= send_time_start.Value.AddHours(-8));
            }
            if (send_time_end != null)
            {
                data = data.Where(w => w.LastSendTime <= send_time_end.Value.AddHours(-8));
            }
            if (update_time_start != null)
            {
                data = data.Where(w => w.UpdateTime >= update_time_start.Value);
            }
            if (update_time_end != null)
            {
                data = data.Where(w => w.UpdateTime <= update_time_end.Value);
            }
            db.Connection.Close();
            return data;
        }

        private IQueryable<PushTokenShow> Get_Sort(IQueryable<PushTokenShow> data)
        {
            switch (Sort)
            {
                case "update":
                    data = data.OrderByDescending(o => o.UpdateTime);
                    break;
                case "create":
                    data = data.OrderByDescending(o => o.CreateTime);
                    break;
                default:
                    data = data.OrderByDescending(o => o.Id);
                    break;
            }
            return data;
        }

        private string Query(string query = "")
        {
            string sql = "";

            if (query != "")
            {
                query = HttpUtility.HtmlEncode(query);
                sql = Method.SQL_Combin(sql, "DeviceId", "OR", "( \"" + query + "\")", ".Contains", false);
                sql = Method.SQL_Combin(sql, "VolunteerName", "OR", "( \"" + query + "\")", ".Contains", false);
                sql = Method.SQL_Combin(sql, "Token", "OR", "( \"" + query + "\")", ".Contains", false);
                sql = Method.SQL_Combin(sql, "AppName", "OR", "( \"" + query + "\")", ".Contains", false);
            }

            return sql;
        }
        #endregion

        #region insert
        public static int Insert_Update_ByToken(PushToken item)
        {
            try
            {
                MithDataContext db = new MithDataContext();
                var data = db.PushToken.FirstOrDefault(f => f.AppID == item.AppID && f.Token == item.Token);
                if (data == null)
                {
                    db.PushToken.InsertOnSubmit(item);
                }
                else
                {
                    data.VolunteerId = item.VolunteerId;
                    data.UpdateTime = DateTime.UtcNow;
                    data.BadgeCount = 0;
                }
                db.SubmitChanges();
                db.Connection.Close();
                return item.Id;
            }
            catch (Exception e)
            {
                ErrorRecordModel.Error_Record(-1, e.HResult.ToString(), "System/PushToken/Insert", false, e.Message + "<br />\r\n<br />\r\n" + e.Source + "<br />\r\n<br />\r\n" + e.StackTrace);
                return -1;
            }
        }

        public static int Insert_Update_ByDeviceId(PushToken item)
        {
            try
            {
                MithDataContext db = new MithDataContext();
                var data = db.PushToken.FirstOrDefault(f => f.AppID == item.AppID);
                if (data == null)
                {
                    db.PushToken.InsertOnSubmit(item);
                }
                else
                {
                    data.Token = item.Token;
                    data.VolunteerId = item.VolunteerId;
                    data.UpdateTime = DateTime.UtcNow;
                    data.BadgeCount = 0;
                }
                db.SubmitChanges();
                db.Connection.Close();
                return item.Id;
            }
            catch (Exception e)
            {
                ErrorRecordModel.Error_Record(-1, e.HResult.ToString(), "System/PushToken/Insert", false, e.Message + "<br />\r\n<br />\r\n" + e.Source + "<br />\r\n<br />\r\n" + e.StackTrace);
                return -1;
            }
        }

        public static int Insert_Update_ByVolunteersId(PushToken item)
        {
            try
            {
                MithDataContext db = new MithDataContext();
                var data = db.PushToken.FirstOrDefault(f => f.AppID == item.AppID && f.VolunteerId == item.VolunteerId);
                if (data == null)
                {
                    db.PushToken.InsertOnSubmit(item);
                }
                else
                {
                    data.Token = item.Token;
                    data.VolunteerId = item.VolunteerId;
                    data.UpdateTime = DateTime.UtcNow;
                    data.BadgeCount = 0;
                }
                db.SubmitChanges();
                db.Connection.Close();
                return item.Id;
            }
            catch (Exception e)
            {
                ErrorRecordModel.Error_Record(-1, e.HResult.ToString(), "System/PushToken/Insert", false, e.Message + "<br />\r\n<br />\r\n" + e.Source + "<br />\r\n<br />\r\n" + e.StackTrace);
                return -1;
            }
        }
        #endregion

        #region update
        public static int Update_ResetBadgeCount(int appid, string token)
        {
            MithDataContext db = new MithDataContext();
            var data = db.PushToken.FirstOrDefault(f => f.Token == token && appid == f.AppID);
            if (data != null)
            {
                data.BadgeCount = 0;
                data.UpdateTime = DateTime.UtcNow;
                db.SubmitChanges();
                db.Connection.Close();
                return data.Id;
            }
            db.Connection.Close();
            return -1;
        }

        public static int Update_SendTime_ByToken(string token)
        {
            MithDataContext db = new MithDataContext();
            var data = db.PushToken.FirstOrDefault(f => f.Token == token);
            if (data != null)
            {
                data.LastSendTime = DateTime.UtcNow;
                data.BadgeCount++;
                data.UpdateTime = DateTime.UtcNow;
                db.SubmitChanges();
                db.Connection.Close();
                return data.Id;
            }
            db.Connection.Close();
            return -1;
        }

        public static int Update_Token_ByToken(int appid, string token, string new_token)
        {
            MithDataContext db = new MithDataContext();
            var data = db.PushToken.FirstOrDefault(f => f.Token == token && appid == f.AppID);
            if (data != null)
            {
                data.Token = new_token;
                data.UpdateTime = DateTime.UtcNow;
                data.LastSendTime = DateTime.UtcNow;
                db.SubmitChanges();
                db.Connection.Close();
                return data.Id;
            }
            db.Connection.Close();
            return -1;
        }
        #endregion

        #region delete
        public static void Delete(int id)
        {
            if (id > 0)
            {
                Delete(new int[] { id });
            }
        }

        public static void Delete(int[] id)
        {
            if (id != null)
            {
                if (id.Any())
                {
                    MithDataContext db = new MithDataContext();
                    var data = db.PushToken.Where(w => id.Contains(w.Id));
                    if (data.Any())
                    {
                        db.PushToken.DeleteAllOnSubmit(data);
                        db.SubmitChanges();
                    }
                    db.Connection.Close();
                }
            }
        }

        public static void DeleteByTokenID(string Token)
        {
            MithDataContext db = new MithDataContext();
            var data = db.PushToken.Where(w => w.Token == Token);
            if (data.Any())
            {
                db.PushToken.DeleteAllOnSubmit(data);
                db.SubmitChanges();
            }
            db.Connection.Close();
        }
        public static void Delete_Repeat()
        {
        }
        #endregion
    }
}