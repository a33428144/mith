﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using System.Text;
using System.Text.RegularExpressions;
using Mith.Models;
using System.IO;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using LinqToExcel;


namespace Mith.Areas.GoX.Models
{
    public class GoodsSN_SettingModel
    {

        #region Class
        public class GoodsSN_SettingShow
        {
            [Key]
            [DisplayName("編號")]
            public int Id { get; set; }

            [DisplayName("商品編號")]
            public string SN { get; set; }
            [DisplayName("穿搭圖")]
            public string Files { get; set; }
            [DisplayName("點擊圖")]
            public string Pic1_Big { get; set; }
            [DisplayName("長 (不穿外套)")]
            public string Pic2_Big { get; set; }
            public string Pic2_Middle { get; set; }
            public string Pic2_Small { get; set; }

            [DisplayName("短 (不穿外套)")]
            public string Pic3_Big { get; set; }
            public string Pic3_Middle { get; set; }
            public string Pic3_Small { get; set; }

            [DisplayName("長 (穿外套)")]
            public string Pic4_Big { get; set; }
            public string Pic4_Middle { get; set; }
            public string Pic4_Small { get; set; }

            [DisplayName("短 (穿外套)")]
            public string Pic5_Big { get; set; }
            public string Pic5_Middle { get; set; }
            public string Pic5_Small { get; set; }

            [DisplayName("建立時間")]
            public DateTime? CreateTime { get; set; }
            [DisplayName("編號")]
            public int? GoodsId { get; set; }

        }

        public class GoodsSN_SettingShowEdit
        {
            [Key]
            [DisplayName("編號")]
            public int Id { get; set; }

            [DisplayName("商品編號")]
            public string SN { get; set; }
            [DisplayName("點擊圖")]
            public string Pic1_Big { get; set; }

            [DisplayName("長 (不穿外套)")]
            public string Pic2_Big { get; set; }
            public string Pic2_Middle { get; set; }
            public string Pic2_Small { get; set; }

            [DisplayName("短 (不穿外套)")]
            public string Pic3_Big { get; set; }
            public string Pic3_Middle { get; set; }
            public string Pic3_Small { get; set; }

            [DisplayName("長 (穿外套)")]
            public string Pic4_Big { get; set; }
            public string Pic4_Middle { get; set; }
            public string Pic4_Small { get; set; }

            [DisplayName("短 (穿外套)")]
            public string Pic5_Big { get; set; }
            public string Pic5_Middle { get; set; }
            public string Pic5_Small { get; set; }

            [DisplayName("建立時間")]
            public DateTime? CreateTime { get; set; }
            [DisplayName("更新時間")]
            public DateTime? UpdateTime { get; set; }
            [DisplayName("更新人員")]
            public int LastUser_Id { get; set; }
            [DisplayName("更新人員")]
            public string LastUser_Name { get; set; }

        }

        public partial class ImportClass
        {
            public string SN { get; set; }
        }

        public class CheckResult
        {
            public Guid ID { get; set; }

            public bool Success { get; set; }

            public int RowCount { get; set; }

            public int ErrorCount { get; set; }

            public string ErrorMessage { get; set; }

        }

        public class Export
        {
            public string Name { get; set; }
            public string Sex { get; set; }
            public string Birthday { get; set; }
            public string Age { get; set; }
            public string Email { get; set; }
            public string CreateTime { get; set; }
        }

        #endregion

        #region 初始值
        public string Keyword = "";
        public string Sort = "update";
        public string Search_Pattern = "0";
        public int Search_CompanyId = 0;
        public DateTime? CreateTime_St = null;
        public DateTime? CreateTime_End = null;
        public int Search_SnNotUse = 0;

        public void Clear_Params()
        {
            Keyword = "";
            Sort = "update";
            Search_Pattern = "0";
            Search_CompanyId = 0;
            CreateTime_St = null;
            CreateTime_End = null;
        }

        #endregion

        #region Convert


        #endregion

        #region Get

        private IQueryable<GoodsSN_BackEnd_View> Get()
        {
            MithDataContext db = new MithDataContext();
            IQueryable<GoodsSN_BackEnd_View> data = db.GoodsSN_BackEnd_View;

            if (Keyword != "")
            {
                Keyword = Keyword.Replace(" ", "");
                data = data.Where(Query(Keyword));
            }
            if (Search_Pattern != "0")
            {
                data = data.Where(w => w.Pattern == Search_Pattern);
            }
            if (Search_CompanyId != 0)
            {
                data = data.Where(w => w.CompanyId == Search_CompanyId);
            }
            if (CreateTime_St != null)
            {
                data = data.Where(w => w.CreateTime >= CreateTime_St.Value);
            }
            if (CreateTime_End != null)
            {
                CreateTime_End = DateTime.Parse(CreateTime_End.Value.ToString("yyyy/MM/dd") + " 23:59:00");
                data = data.Where(w => w.CreateTime <= CreateTime_End);
            }
            if (Search_SnNotUse != 0)
            {
                data = data.Where(w => w.GoodsId == null);
            }

            db.Connection.Close();
            return data;
        }

        public List<GoodsSN_SettingShow> Get_Data_Mith(int p = 1, int take = 10, bool ShowAll = false)
        {
            MithDataContext db = new MithDataContext();
            var data = Get();
            //不分頁秀全部
            if (ShowAll == true)
            {
                data = data.OrderByDescending(o => o.Id);
            }
            //有分頁
            else
            {
                data = data.OrderByDescending(o => o.Id).Skip((p - 1) * take).Take(take);
            }

            List<GoodsSN_SettingShow> item = new List<GoodsSN_SettingShow>();
            item = (from i in data
                    select new GoodsSN_SettingShow
                    {
                        Id = i.Id,
                        SN = i.SN,
                        Pic1_Big = i.Pic1_Big,
                        Pic2_Middle = i.Pic2_Middle,
                        Pic2_Small = i.Pic2_Small,
                        Pic3_Middle = i.Pic3_Middle,
                        Pic3_Small = i.Pic3_Small,
                        Pic4_Middle = i.Pic4_Middle,
                        Pic4_Small = i.Pic4_Small,
                        Pic5_Middle = i.Pic5_Middle,
                        Pic5_Small = i.Pic5_Small,
                        CreateTime = i.CreateTime,
                        GoodsId = i.GoodsId
                    }).ToList();
            db.Connection.Close();
            return item;
        }

        public List<GoodsSN_SettingShow> Get_Data_Company(int p = 1, int take = 10, bool ShowAll = false)
        {
            MithDataContext db = new MithDataContext();
            var data = Get();
            //不分頁秀全部
            if (ShowAll == true)
            {
                data = data.OrderByDescending(o => o.Id);
            }
            //有分頁
            else
            {
                data = data.OrderByDescending(o => o.Id).Skip((p - 1) * take).Take(take);
            }

            List<GoodsSN_SettingShow> item = new List<GoodsSN_SettingShow>();
            item = (from i in data
                    select new GoodsSN_SettingShow
                    {
                        Id = i.Id,
                        SN = i.SN,
                        Pic1_Big = i.Pic1_Big,
                        CreateTime = i.CreateTime,
                        GoodsId = i.GoodsId
                    }).ToList();
            db.Connection.Close();
            return item;
        }

        public GoodsSN_SettingShowEdit Get_One(int id)
        {
            MithDataContext db = new MithDataContext();
            var data = (from i in db.GoodsSN_BackEnd_View
                        where i.Id == id
                        select new GoodsSN_SettingShowEdit
                        {
                            Id = i.Id,
                            SN = i.SN,
                            Pic1_Big = i.Pic1_Big,
                            Pic2_Big = i.Pic2_Big,
                            Pic2_Middle = i.Pic2_Middle,
                            Pic2_Small = i.Pic2_Small,
                            Pic3_Big = i.Pic3_Big,
                            Pic3_Middle = i.Pic3_Middle,
                            Pic3_Small = i.Pic3_Small,
                            Pic4_Middle = i.Pic4_Middle,
                            Pic4_Small = i.Pic4_Small,
                            Pic5_Middle = i.Pic5_Middle,
                            Pic5_Small = i.Pic5_Small,
                            CreateTime = i.CreateTime,
                            UpdateTime = i.UpdateTime,
                            LastUser_Name = i.LastUser_Name
                        }).FirstOrDefault();
            db.Connection.Close();
            return data;
        }

        public Method.Paging Get_Page(int p = 1, int take = 10)
        {
            return Method.Get_Page(Get_Count(), p, take);
        }

        public int Get_Count()
        {
            try
            {
                return Get().Count();
            }
            catch { return 0; }
        }



        private string Query(string query = "")
        {
            string sql = "";

            if (query != "")
            {
                query = HttpUtility.HtmlEncode(query);
                sql = Method.SQL_Combin(sql, "SN", "OR", "( \"" + query + "\")", ".Contains", false);
            }

            return sql;
        }
        #endregion

        #region Insert
        public void Insert_multiple(string Path_Big, string Path_Middle, string Path_Small, string SN, string Pic_Type)
        {
            MithDataContext db = new MithDataContext();
            string Pic1_Big = "";

            string Pic2_Big = "";
            string Pic2_Middle = "";
            string Pic2_Small = "";

            string Pic3_Big = "";
            string Pic3_Middle = "";
            string Pic3_Small = "";

            string Pic4_Big = "";
            string Pic4_Middle = "";
            string Pic4_Small = "";

            string Pic5_Big = "";
            string Pic5_Middle = "";
            string Pic5_Small = "";

            if (Pic_Type == "1")
            {
                Pic1_Big = Path_Big;
            }
            else if (Pic_Type == "2")
            {
                Pic2_Big = Path_Big;
                Pic2_Middle = Path_Middle;
                Pic2_Small = Path_Small;
            }
            else if (Pic_Type == "3")
            {
                Pic3_Big = Path_Big;
                Pic3_Middle = Path_Middle;
                Pic3_Small = Path_Small;
            }
            else if (Pic_Type == "4")
            {
                Pic4_Big = Path_Big;
                Pic4_Middle = Path_Middle;
                Pic4_Small = Path_Small;
            }
            else if (Pic_Type == "5")
            {
                Pic5_Big = Path_Big;
                Pic5_Middle = Path_Middle;
                Pic5_Small = Path_Small;
            }
            else
            {

            }
            GoodsSN_Setting new_item = new GoodsSN_Setting
            {
                SN = SN,
                Pic1_Big = Pic1_Big,

                Pic2_Big = Pic2_Big,
                Pic2_Middle = Pic2_Middle,
                Pic2_Small = Pic2_Small,

                Pic3_Big = Pic3_Big,
                Pic3_Middle = Pic3_Middle,
                Pic3_Small = Pic3_Small,

                Pic4_Big = Pic4_Big,
                Pic4_Middle = Pic4_Middle,
                Pic4_Small = Pic4_Small,

                Pic5_Big = Pic5_Big,
                Pic5_Middle = Pic5_Middle,
                Pic5_Small = Pic5_Small,

                CreateTime = DateTime.UtcNow,
                UpdateTime = DateTime.UtcNow,
                LastUserId = 0
            };
            db.GoodsSN_Setting.InsertOnSubmit(new_item);
            db.SubmitChanges();
            db.Connection.Close();
        }
        #endregion

        #region Update

        public int Update_One(GoodsSN_SettingShowEdit item)
        {
            try
            {
                MithDataContext db = new MithDataContext();
                int Id = 0;
                var data = db.GoodsSN_Setting.FirstOrDefault(f => f.Id == item.Id);
                if (data != null)
                {
                    data.UpdateTime = DateTime.UtcNow;
                    data.LastUserId = item.LastUser_Id;
                    db.SubmitChanges();
                    Id = data.Id;
                }
                db.Connection.Close();
                return Id;
            }
            catch { return -1; }
        }

        public void Update_multiple(string Path_Big, string Path_Middle, string Path_Small, string SN, string Pic_Type)
        {
            try
            {
                //穿搭圖(商品)
                if (Pic_Type == "1")
                {
                    UpdatePic(Path_Big, Path_Middle, Path_Small, SN, "1");
                }
                //穿搭圖(長)
                else if (Pic_Type == "2")
                {
                    UpdatePic(Path_Big, Path_Middle, Path_Small, SN, "2");
                }
                //穿搭圖(短)
                else if (Pic_Type == "3")
                {
                    UpdatePic(Path_Big, Path_Middle, Path_Small, SN, "3");
                }
                else if (Pic_Type == "4")
                {
                    UpdatePic(Path_Big, Path_Middle, Path_Small, SN, "4");
                }
                else if (Pic_Type == "5")
                {
                    UpdatePic(Path_Big, Path_Middle, Path_Small, SN, "5");
                }
                else { }
            }
            catch { }
        }

        public void UpdatePic(string Path_Big, string Path_Middle, string Path_Small, string SN, string Pic_Type)
        {
            MithDataContext db = new MithDataContext();

            var data = db.GoodsSN_Setting.FirstOrDefault(w => w.SN == SN);
            if (data != null)
            {
                //穿搭圖(商品)
                if (Pic_Type == "1")
                {
                    BlobHelper.delete("goods-sn/" + "Big/" + SN, Path.GetFileName(data.Pic1_Big));
                    data.Pic1_Big = Path_Big;
                }
                //穿搭圖(長-不穿外套)
                else if (Pic_Type == "2")
                {
                    BlobHelper.delete("goods-sn/" + "Big/" + SN, Path.GetFileName(data.Pic2_Big));
                    data.Pic2_Big = Path_Big;

                    BlobHelper.delete("goods-sn/" + "Middle/" + SN, Path.GetFileName(data.Pic2_Middle));
                    data.Pic2_Middle = Path_Middle;

                    BlobHelper.delete("goods-sn/" + "Small/" + SN, Path.GetFileName(data.Pic2_Small));
                    data.Pic2_Small = Path_Small;
                }
                //穿搭圖(短-不穿外套)
                else if (Pic_Type == "3")
                {
                    BlobHelper.delete("goods-sn/" + "Big/" + SN, Path.GetFileName(data.Pic3_Big));
                    data.Pic3_Big = Path_Big;

                    BlobHelper.delete("goods-sn/" + "Middle/" + SN, Path.GetFileName(data.Pic3_Middle));
                    data.Pic3_Middle = Path_Middle;

                    BlobHelper.delete("goods-sn/" + "Small/" + SN, Path.GetFileName(data.Pic3_Small));
                    data.Pic3_Small = Path_Small;
                }
                //穿搭圖(短-不穿外套)
                else if (Pic_Type == "4")
                {
                    BlobHelper.delete("goods-sn/" + "Big/" + SN, Path.GetFileName(data.Pic4_Big));
                    data.Pic4_Big = Path_Big;

                    BlobHelper.delete("goods-sn/" + "Middle/" + SN, Path.GetFileName(data.Pic4_Middle));
                    data.Pic4_Middle = Path_Middle;

                    BlobHelper.delete("goods-sn/" + "Small/" + SN, Path.GetFileName(data.Pic4_Small));
                    data.Pic4_Small = Path_Small;
                }
                //穿搭圖(短-不穿外套)
                else if (Pic_Type == "5")
                {
                    BlobHelper.delete("goods-sn/" + "Big/" + SN, Path.GetFileName(data.Pic5_Big));
                    data.Pic5_Big = Path_Big;

                    BlobHelper.delete("goods-sn/" + "Middle/" + SN, Path.GetFileName(data.Pic5_Middle));
                    data.Pic5_Middle = Path_Middle;

                    BlobHelper.delete("goods-sn/" + "Small/" + SN, Path.GetFileName(data.Pic5_Small));
                    data.Pic5_Small = Path_Small;
                }
                else
                {
                }
                db.SubmitChanges();
                db.Connection.Close();
            }

            db.Connection.Close();

        }

        #endregion

        #region Delete
        public void Delete(int id)
        {
            if (id > 0)
            {
                Delete(new int[] { id });
            }
        }

        public void Delete(int[] id)
        {
            if (id != null)
            {
                if (id.Any())
                {
                    MithDataContext db = new MithDataContext();
                    var data = db.GoodsSN_Setting.Where(w => id.Contains(w.Id));

                    if (data.Any())
                    {

                        db.GoodsSN_Setting.DeleteAllOnSubmit(data);
                        db.SubmitChanges();
                    }
                    db.Connection.Close();
                }
            }
        }

        public void Delete_SN(string SN)
        {
            MithDataContext db = new MithDataContext();
            var data = db.GoodsSN_Setting.FirstOrDefault(w => w.SN == SN);

            if (data != null)
            {
                //穿搭圖(商品) 
                BlobHelper.delete("goods-sn/" + "Big/" + SN, Path.GetFileName(data.Pic1_Big));

                //穿搭圖(長-不穿外套)
                BlobHelper.delete("goods-sn/" + "Big/" + SN, Path.GetFileName(data.Pic2_Big));
                BlobHelper.delete("goods-sn/" + "Middle/" + SN, Path.GetFileName(data.Pic2_Middle));
                BlobHelper.delete("goods-sn/" + "Small/" + SN, Path.GetFileName(data.Pic2_Small));

                //穿搭圖(短-不穿外套)
                BlobHelper.delete("goods-sn/" + "Big/" + SN, Path.GetFileName(data.Pic3_Big));
                BlobHelper.delete("goods-sn/" + "Middle/" + SN, Path.GetFileName(data.Pic3_Middle));
                BlobHelper.delete("goods-sn/" + "Small/" + SN, Path.GetFileName(data.Pic3_Small));

                //穿搭圖(長-不穿外套)
                BlobHelper.delete("goods-sn/" + "Big/" + SN, Path.GetFileName(data.Pic4_Big));
                BlobHelper.delete("goods-sn/" + "Middle/" + SN, Path.GetFileName(data.Pic4_Middle));
                BlobHelper.delete("goods-sn/" + "Small/" + SN, Path.GetFileName(data.Pic4_Small));


                //穿搭圖(短-不穿外套)
                BlobHelper.delete("goods-sn/" + "Big/" + SN, Path.GetFileName(data.Pic5_Big));
                BlobHelper.delete("goods-sn/" + "Middle/" + SN, Path.GetFileName(data.Pic5_Middle));
                BlobHelper.delete("goods-sn/" + "Small/" + SN, Path.GetFileName(data.Pic5_Small));

                db.GoodsSN_Setting.DeleteOnSubmit(data);
                db.SubmitChanges();
            }
            db.Connection.Close();
        }


        #endregion

        #region 上傳圖片
        public int CheckSN_Count(string SN)
        {
            MithDataContext db = new MithDataContext();
            int count = db.GoodsSN_Setting.Where(w => w.SN == SN).Count();

            return count;
        }

        #endregion

        #region 匯入
        /// <summary>
        /// 檢查匯入的資料.
        /// </summary>
        /// <param name="fileName">檔案名稱</param>
        /// <param name="importSN">The import zip codes.</param>
        /// <returns></returns>
        public CheckResult CheckImportData(string fileName, List<ImportClass> importSN)
        {
            var result = new CheckResult();

            var targetFile = new FileInfo(fileName);

            if (!targetFile.Exists)
            {
                result.ID = Guid.NewGuid();
                result.Success = false;
                result.ErrorCount = 0;
                result.ErrorMessage = "匯入的檔案不存在";
                return result;
            }

            var excelFile = new ExcelQueryFactory(fileName);

            //欄位對映
            excelFile.AddMapping<ImportClass>(x => x.SN, "商品編號");


            //工作表名稱
            var excelContent = excelFile.Worksheet<ImportClass>("匯入商品編號");
            int errorCount = 0;
            int rowIndex = 1;
            var importErrorMessages = new List<string>();
            //載入Excel資料
            foreach (var row in excelContent)
            {
                var errorMessage = new StringBuilder();
                var GoodsSN_Setting = new ImportClass();

                if (!string.IsNullOrWhiteSpace(row.SN))
                {
                    MithDataContext db = new MithDataContext();
                    int Check_Count = 0;
                    db.Connection.Close();

                    //等於15碼
                    if (row.SN.Length == 15)
                    {
                        Check_Count = db.GoodsSN_Setting.Where(w => w.SN == row.SN).Count();
                        //0代表沒有重複
                        if (Check_Count == 0)
                        {
                            bool CheckSn = CheckSnType(row.SN);
                            //序號格式錯誤！
                            if (CheckSn == false)
                            {
                                errorMessage.Append("【商品序號】格式錯誤");
                            }
                        }
                        //序號重複
                        else
                        {
                            errorMessage.Append("【商品序號】為重複資料");
                        }
                    }
                    //小於15碼
                    else
                    {
                        errorMessage.Append("【商品序號】長度不為15碼！");
                    }
                    GoodsSN_Setting.SN = row.SN;
                    importSN.Add(GoodsSN_Setting);
                    rowIndex += 1;
                }

                if (errorMessage.Length > 0)
                {
                    errorCount += 1;
                    importErrorMessages.Add(string.Format(
                        "第 {0} 列資料：<br/>{1}{2}",
                        rowIndex,
                        errorMessage,
                        "<br/>"));
                }
            }

            try
            {
                result.ID = Guid.NewGuid();
                result.Success = errorCount.Equals(0);
                result.RowCount = importSN.Count;
                result.ErrorCount = errorCount;

                string allErrorMessage = string.Empty;

                foreach (var message in importErrorMessages)
                {
                    allErrorMessage += message;
                }

                result.ErrorMessage = allErrorMessage;

                return result;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// 資料匯入DB.
        /// </summary>
        /// <param name="fileName">檔案名稱</param>
        /// <param name="importSN">The import zip codes.</param>
        public void InsertImportData(string fileName = "", IEnumerable<ImportClass> importSN = null)
        {
            MithDataContext db = new MithDataContext();
            try
            {
                foreach (var item in importSN)
                {
                    var Repeat_Code = db.GoodsSN_Setting.FirstOrDefault(f => f.SN == item.SN);

                    if (Repeat_Code == null)
                    {
                        GoodsSN_Setting new_item = new GoodsSN_Setting
                        {
                            SN = item.SN,
                            CreateTime = DateTime.UtcNow,
                            UpdateTime = DateTime.UtcNow,
                            LastUserId = 0
                        };
                        db.GoodsSN_Setting.InsertOnSubmit(new_item);
                        db.SubmitChanges();
                        db.Connection.Close();
                    }
                }
            }
            catch
            {
                throw;
            }
            //一定會刪除此檔案
            finally
            {
                if (File.Exists(fileName))
                {
                    File.Delete(fileName);
                }
                db.Connection.Close();
            }
        }



        /// <summary>
        /// 判斷商品編號格式
        /// </summary>
        /// <param name="SN">商品編號</param>
        /// <returns>True or False</returns>
        public bool CheckSnType(string SN)
        {
            //序號前13位
            string SN_13 = SN.Substring(0, 13);
            //序號第11位
            string SN_14 = SN.Substring(13, 1);
            ///序號第15位
            string SN_15 = SN.Substring(14, 1);

            //驗證數字
            System.Text.RegularExpressions.Regex reg_num = new System.Text.RegularExpressions.Regex(@"^[0-9]+$");
            //驗證為T、O、D、S、P
            System.Text.RegularExpressions.Regex reg_str = new System.Text.RegularExpressions.Regex(@"^[TCDSP]+$");

            bool Check_SN_13 = reg_num.IsMatch(SN_13);
            bool Check_SN_14 = reg_str.IsMatch(SN_14);
            bool Check_SN_15 = reg_num.IsMatch(SN_15);

            //其中有一個為False，代表格式錯誤
            if (Check_SN_13 == false || Check_SN_14 == false || Check_SN_15 == false)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        #endregion
    }
}