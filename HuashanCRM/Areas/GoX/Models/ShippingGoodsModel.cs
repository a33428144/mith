﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Linq;
using System.Data.Linq.SqlClient;
using Mith.Areas.GoX.Models;
using Mith.Models;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;

namespace Mith.Areas.GoX.Models
{
    public class ShippingGoodsModel
    {
        #region Class
        public class ShippingGoodsShow
        {
            [Key]
            [DisplayName("編號")]
            public int Id { get; set; }
            [DisplayName("訂單編號")]
            public string SN { get; set; }
            [DisplayName("物流編號")]
            public string LogisticsSN { get; set; }
            [DisplayName("包裹編號")]
            public string PackageSN { get; set; }
            [DisplayName("取貨方式")]
            public int LogisticsType { get; set; }
            public string LogisticsType_Name { get; set; }
            [DisplayName("總計")]
            public int BuyTotal { get; set; }
            [DisplayName("姓名")]
            public string Volunteers_Name { get; set; }
            [DisplayName("電話")]
            public string Volunteers_Tel { get; set; }
            [DisplayName("Email")]
            public string Volunteers_Email { get; set; }
            [DisplayName("姓名")]
            public string ReceiverName { get; set; }
            [DisplayName("電話")]
            public string ReceiverTel { get; set; }
            [DisplayName("地址")]
            public string ReceiverAddress { get; set; }
            [DisplayName("門市編號")]
            public string ReceiverStoreId { get; set; }
            [DisplayName("門市名稱")]
            public string ReceiverStoreName { get; set; }
            [DisplayName("門市地址")]
            public string ReceiverStoreAddress { get; set; }
            [DisplayName("退貨門市編號")]
            public string ReturnStoreId { get; set; }
            [DisplayName("退貨門市名稱")]
            public string ReturnStoreName { get; set; }
            [DisplayName("退貨門市地址")]
            public string ReturnStoreAddress { get; set; }
            [DisplayName("出貨時間")]
            public DateTime? ShippingTime { get; set; }
            [DisplayName("出貨廠商")]
            public string Company_Name { get; set; }
            [DisplayName("建立時間")]
            public DateTime CreateTime { get; set; }
            [DisplayName("出貨人員")]
            public string LastUser_Name { get; set; }
						[DisplayName("到貨時間")]
						public DateTime? ArriveInTime { get; set; }
						[DisplayName("取貨時間")]
						public DateTime? ReceiveGoodsTime { get; set; }
            [DisplayName("訂單細節List")]
            public List<OrderDeatailShow> OrderDetailList { get; set; }


            //[DisplayName("已收到貨")]
            //public bool ReturnGoods { get; set; }
            //[DisplayName("收貨時間")]
            //public DateTime? ReturnGoodsTime { get; set; }
            //[DisplayName("同意退款")]
            //public bool AgreeRefund { get; set; }
            //[DisplayName("退款原因")]
            //public string RefundReason_Name { get; set; }
        }

        public class OrderDeatailShow
        {
            [DisplayName("編號")]
            public int Id { get; set; }
            [DisplayName("商品編號")]
            public string Goods_SN { get; set; }
            [DisplayName("商品名稱")]
            public string Goods_Name { get; set; }
            [DisplayName("商品顏色名稱")]
            public string Goods_ColorName { get; set; }
            [DisplayName("商品類別名稱")]
            public string Goods_TypeName { get; set; }
            [DisplayName("公司名稱")]
            public int CompanyId { get; set; }
            [DisplayName("公司名稱")]
            public string Company_Name { get; set; }
            [DisplayName("品牌名稱")]
            public string Brand_Name { get; set; }
            [DisplayName("購買尺寸")]
            public string BuySize { get; set; }
            [DisplayName("購買數量")]
            public int BuyQuantity { get; set; }
            [DisplayName("購買單價")]
            public int BuyPrice { get; set; }
            [DisplayName("購買總額")]
            public int BuyTotal { get; set; }
            [DisplayName("物流編號")]
            public string LogisticsSN { get; set; }
            [DisplayName("物流狀態")]
            public int LogisticsStatus { get; set; }
            [DisplayName("物流狀態")]
            public string LogisticsStatus_Name { get; set; }
            [DisplayName("應該出貨日期")]
            public DateTime? ShippingDeadline { get; set; }
            [DisplayName("出貨時間")]
            public DateTime? ShippingGoodsTime { get; set; }
            [DisplayName("更新時間")]
            public DateTime? UpdateTime { get; set; }
            [DisplayName("更新人員")]
            public int LastUserId { get; set; }
            [DisplayName("更新人員")]
            public string LastUser_Name { get; set; }

        }

        public class Export
        {
            [Key]
            public string SN { get; set; }
            public string Name { get; set; }
            public string TaxId { get; set; }
            public string Tel { get; set; }
            public string Fax { get; set; }
            public string City_Name { get; set; }
            public string Area_Name { get; set; }
            public string Address { get; set; }
            public string BossName { get; set; }
            public string BossIdentityCard { get; set; }
            public string ContactName { get; set; }
            public string ContactTel { get; set; }
            public string ContactEmail { get; set; }
            public string ContractDate { get; set; }
            public string ArtEditorCost { get; set; }
            public string FreightFee { get; set; }
            public string ManagementFee { get; set; }
            public string Collaborate { get; set; }
            public string TypeName { get; set; }
            public string GoodsKind { get; set; }
            public string AccountingName { get; set; }
            public string AccountingTel { get; set; }
            public string AccountingEmail { get; set; }
            public string BankCode { get; set; }
            public string BankName { get; set; }
            public string Account { get; set; }
            public string AccountName { get; set; }
            public string CreateTime { get; set; }
        }

        public string Keyword = "";
        public int Search_CompanyId = 0;
				public int Search_LogisticsStatus = 0;
        public DateTime? ShippingTime_St = null;
        public DateTime? ShippingTime_End = null;

        #endregion

        public ShippingGoodsModel()
        {
        }
        public void Clear()
        {
            Keyword = "";
            Search_CompanyId = 0;
						Search_LogisticsStatus = 0;
            ShippingTime_St = null;
            ShippingTime_End = null;
        }

        #region Get

        private IQueryable<ShippingGoods_Index_View> Get()
        {
            MithDataContext db = new MithDataContext();
            IQueryable<ShippingGoods_Index_View> data = db.ShippingGoods_Index_View;

            if (Search_CompanyId > 0)
            {
                data = data.Where(w => w.CompanyId == Search_CompanyId);
            }
            if (Keyword != "")
            {
                Keyword = Keyword.Replace(" ", "");
                data = data.Where(Query(Keyword));
            }

						if (Search_LogisticsStatus > 0)
						{
							data = data.Where(w => w.LogisticsStatus == Search_LogisticsStatus);
						}

            if (ShippingTime_St != null)
            {
                data = data.Where(w => w.ShippingTime >= ShippingTime_St);
            }
            if (ShippingTime_End != null)
            {
                ShippingTime_End = DateTime.Parse(ShippingTime_End.Value.ToString("yyyy/MM/dd") + " 23:59:00");
                data = data.Where(w => w.ShippingTime <= ShippingTime_End);
            }
            db.Connection.Close();
            return data;
        }

        public List<ShippingGoodsShow> Get_Data(int p = 1, int take = 10)
        {
            MithDataContext db = new MithDataContext();
            List<ShippingGoods_Index_View> data = new List<ShippingGoods_Index_View>();

            data = Get().OrderByDescending(o => o.ShippingTime).Skip((p - 1) * take).Take(take).ToList();

            List<ShippingGoodsShow> item = new List<ShippingGoodsShow>();
            item = (from i in data
                    select new ShippingGoodsShow
                    {
                        Id = i.Id,
                        SN = i.SN,
                        LogisticsSN = i.LogisticsSN,
                        PackageSN = i.PackageSN,
                        LogisticsType = i.LogisticsType,
                        LogisticsType_Name = i.LogisticsType_Name,
                        BuyTotal = i.BuyTotal.Value,
                        ReceiverName = i.ReceiverName,
                        ReceiverTel = i.ReceiverTel,
                        //ReceiverAddress = i.ReceiverAddress,
                        ReceiverStoreId = i.ReceiverStoreId,
                        ReceiverStoreName = i.ReceiverStoreName,
                        //ReceiverStoreAddress = i.ReceiverStoreAddress,
                        ShippingTime = i.ShippingTime,
                        Company_Name = i.Company_Name,
                        OrderDetailList = Get_OrderDetailList(i.LogisticsSN, "Index"),
                    }).ToList();

            db.Connection.Close();
            return item;
        }

        public ShippingGoodsShow Get_One(string LogisticsSN = "")
        {
            MithDataContext db = new MithDataContext();
            var data = (from i in db.ShippingGoods_Edit_View
                        where i.LogisticsSN == LogisticsSN
                        select new ShippingGoodsShow
                        {
                            Id = i.Id,
                            SN = i.SN,
                            LogisticsSN = i.LogisticsSN,
                            PackageSN = i.PackageSN,
                            LogisticsType = i.LogisticsType,
                            LogisticsType_Name = i.LogisticsType_Name,
                            BuyTotal = i.BuyTotal.Value,
                            Volunteers_Name = i.Volunteers_Name,
                            Volunteers_Tel = i.Volunteers_Tel,
                            Volunteers_Email = i.Volunteers_Email,
                            ReceiverName = i.ReceiverName,
                            ReceiverTel = i.ReceiverTel,
                            ReceiverAddress = i.ReceiverAddress,
                            ReceiverStoreId = i.ReceiverStoreId,
                            ReceiverStoreName = i.ReceiverStoreName,
                            ReceiverStoreAddress = i.ReceiverStoreAddress,
                            ReturnStoreId = i.ReturnStoreId,
                            ReturnStoreName = i.ReturnStoreName,
                            ReturnStoreAddress = i.ReturnStoreAddress,
                            ShippingTime = i.ShippingTime.Value,
                            Company_Name = i.Company_Name,
                            CreateTime = i.CreateTime.Value,
                            LastUser_Name = i.LastUser_Name,
														ArriveInTime = i.ArriveInTime,
														ReceiveGoodsTime = i.ReceiveGoodsTime,
                            OrderDetailList = Get_OrderDetailList(i.LogisticsSN, "Index"),
                        }).FirstOrDefault();
            db.Connection.Close();
            return data;
        }

        public Method.Paging Get_Page(int p = 1, int take = 10)
        {
            return Method.Get_Page(Get_Count(), p, take);
        }

        public int Get_Count()
        {
            return Get().Count();
        }

        private string Query(string query = "")
        {
            string sql = "";

            if (query != "")
            {
                query = HttpUtility.HtmlEncode(query);
                sql = Method.SQL_Combin(sql, "SN", "OR", "( \"" + query + "\")", ".Contains", false);
                sql = Method.SQL_Combin(sql, "LogisticsSN", "OR", "( \"" + query + "\")", ".Contains", false);
								sql = Method.SQL_Combin(sql, "PackageSN", "OR", "( \"" + query + "\")", ".Contains", false);
            }

            return sql;
        }
        /// <summary>
        /// 取得訂單主檔底下的訂單細節
        /// </summary>
        /// <param name="OrderId">訂單編號</param>
        /// <param name="CompanyId">廠商編號</param>
        public List<OrderDeatailShow> Get_OrderDetailList(string LogisticsSN = "", string Action = "")
        {
            MithDataContext db = new MithDataContext();
            try
            {
                #region 來自 Index頁
                if (Action == "Index")
                {
                    var data = (from i in db.OrderDetail_Edit_View
                                where i.LogisticsSN == LogisticsSN 
																	 && i.LogisticsStatus != 3 //退貨中
																	 && i.LogisticsStatus != 4 //收到退貨
																	 && i.LogisticsStatus < 8
                                orderby i.CompanyId
                                select new OrderDeatailShow
                                {
                                    Id = i.Id,
                                    Goods_SN = i.Goods_SN,
                                    Goods_Name = i.Goods_Name,
                                    Goods_ColorName = i.Goods_ColorName,
                                    Goods_TypeName = i.Goods_TypeName,
                                    CompanyId = i.CompanyId,
                                    Company_Name = i.Company_Name,
                                    BuySize = i.BuySize,
                                    BuyQuantity = i.BuyQuantity,
                                    BuyPrice = i.BuyPrice,
                                    BuyTotal = i.BuyTotal,
                                    LogisticsStatus = i.LogisticsStatus,
                                    LogisticsStatus_Name = i.LogisticsStatus_Name,
                                }).ToList();

                    if (Search_CompanyId > 0)
                    {
                        data = data.Where(w => w.CompanyId == Search_CompanyId).ToList();
                    }
                    return data;
                }
                #endregion

                #region 來自 Edit頁
                else
                {
                    var data = (from i in db.OrderDetail_Edit_View
																where i.LogisticsSN == LogisticsSN && i.LogisticsStatus < 7
                                orderby i.CompanyId
                                select new OrderDeatailShow
                                {
                                    Id = i.Id,
                                    Goods_SN = i.Goods_SN,
                                    Goods_Name = i.Goods_Name,
                                    Goods_ColorName = i.Goods_ColorName,
                                    Goods_TypeName = i.Goods_TypeName,
                                    CompanyId = i.CompanyId,
                                    BuySize = i.BuySize,
                                    BuyQuantity = i.BuyQuantity,
                                    BuyPrice = i.BuyPrice,
                                    BuyTotal = i.BuyTotal,
                                    ShippingDeadline = i.ShippingDeadline,
                                    Company_Name = i.Company_Name,
                                    LogisticsStatus = i.LogisticsStatus,
                                    LogisticsStatus_Name = i.LogisticsStatus_Name,
                                }).ToList();

                    if (Search_CompanyId > 0)
                    {
                        data = data.Where(w => w.CompanyId == Search_CompanyId).ToList();
                    }
                    return data;
                }
                #endregion
            }
            catch
            {
                return null;
            }
            finally
            {
                db.Connection.Close();
            }

        }

        public static string Get_711_PackegeSN(string LogisticsSN = "", int OrderDetailId = 0, string Total = "0")
        {
            MithDataContext db = new MithDataContext();
            try
            {
                string PackageSN = null;

                var OrderDetail = db.OrderDetail_Edit_View.FirstOrDefault(f => f.Id == OrderDetailId);

                if (OrderDetail != null && Total != "0")
                {
                    var Order = db.Order.FirstOrDefault(f => f.Id == OrderDetail.OrderId);

                    #region 處理廠商
                    string CompanyTel = db.Company.FirstOrDefault(f => f.Id == OrderDetail.CompanyId).Tel;
                    if (!string.IsNullOrWhiteSpace(CompanyTel))
                    {
                        CompanyTel = "0000000" + CompanyTel.Substring(CompanyTel.Length - 3, 3);
                    }
                    else
                    {
                        CompanyTel = "0000000000";
                    }
                    string CompanyName = OrderDetail.Company_Name;
                    if (CompanyName.Length > 5)
                    {
                        CompanyName = CompanyName.Substring(0, 5);
                    }
                    #endregion

                    #region 處理收件人
                    string ReceiverTel = "0000000" + Order.ReceiverTel.Substring(Order.ReceiverTel.Length - 3, 3);
                    string ReceiverName = Order.ReceiverName;
                    if (ReceiverName.Length > 5)
                    {
                        ReceiverName = ReceiverName.Substring(0, 5);
                    }
                    #endregion

                    #region 處理總額
                    switch (Total.Length)
                    {
                        case 5:
                            break;
                        case 4:
                            Total = "0" + Total;
                            break;
                        case 3:
                            Total = "00" + Total;
                            break;
                        case 2:
                            Total = "000" + Total;
                            break;
                        case 1:
                            Total = "0000" + Total;
                            break;
                    }
                    #endregion

                    //回傳結果
                    string Result = null;

                    //7-11要號 API
                    string API = "http://202.168.204.198/c2c_test/PaymentBack.ashx";

                    //發出 Request
                    HttpWebRequest request = HttpWebRequest.Create(API) as HttpWebRequest;

                    //要帶的參數
                    string Radom = Method.RandomKey(4, true, false, false, false) + "1";
                    string deadlinedate = DateTime.UtcNow.AddHours(8).AddDays(7).ToString("yyyyMMdd");
                    string Param =
                        "eshopid=851&" +
                        "eshopsonid=023&" +
                        "orderno=" + LogisticsSN + "&" +
                        "service_type=7&" +
                        "account=" + Total + "&" +
												"payment_cpname=MiTh&" +
                        "trade_describe=服飾&" +
                        "deadlinedate=" + deadlinedate + "&" +
                        "deadlinetime=2359&" +
                        "show_type=21&" +
                        "daishou_account=0&" +
                        "sender=" + CompanyName + "&" +
                        "sender_phone=" + CompanyTel + "&" +
                        "receiver=" + ReceiverName + "&" +
                        "receiver_phone=" + ReceiverTel + "&" +
                        "receiver_storeid=" + Order.ReceiverStoreId + "&" +
                        "return_storeid=894359";

                    //將變數進行編碼
                    byte[] bs = Encoding.UTF8.GetBytes(Param);
                    request.Method = "POST"; // POST方式
                    request.KeepAlive = true; //保持連線
                    request.ProtocolVersion = HttpVersion.Version10;
                    request.ContentType = "application/x-www-form-urlencoded";
                    request.ContentLength = bs.Length;

                    using (Stream reqStream = request.GetRequestStream())
                    {
                        reqStream.Write(bs, 0, bs.Length);
                    }

                    //API Response
                    using (WebResponse response = request.GetResponse())
                    {
                        //跨網域
                        StreamReader sr = new StreamReader(response.GetResponseStream(), Encoding.UTF8);

                        //讀取API回傳結果
                        Result = sr.ReadToEnd().Replace("<C2C xmlns=\"http://7-11.com.tw/online/OrderInfo/RS\">", "<C2C>").Replace("\r\n", "");

                        XmlDocument doc = new XmlDocument();

                        //回傳結果string轉Xml
                        doc.LoadXml(Result);

                        //XmlNodeList xmlList = doc.SelectNodes("/C2C");
                        //foreach (XmlNode node in xmlList)
                        //{
                        //    string paymentno = node["paymentno"].InnerText;   //寄貨編號
                        //}

                        string Status = doc.SelectSingleNode("/C2C/status").InnerText;
                        string OrderSN = doc.SelectSingleNode("/C2C/orderno").InnerText;
                        PackageSN = doc.SelectSingleNode("/C2C/paymentno").InnerText;

                    }
                }
                return PackageSN;
            }
            catch (Exception e)
            {
                return null;
            }
            finally
            {
                db.Connection.Close();
            }
        }

        #endregion

        #region Inser
        public int Insert_ReturnGoods_Order(int CityId = 0, int AreaId = 0, string ReceiverAddress = "",
            int ReceiveTimeRange = 0, string Remark = "", int Old_OrderId = 0, int LastUserId = 0)
        {
            MithDataContext db = new MithDataContext();
            try
            {
                string CityName = db.City.FirstOrDefault(f => f.Id == CityId).Name;
                string AreaName = db.Area.FirstOrDefault(f => f.Id == AreaId).Name;
                ReceiverAddress = CityName + AreaName + ReceiverAddress;

                int Id = db.Order.OrderByDescending(o => o.Id).FirstOrDefault() != null ? db.Order.OrderByDescending(o => o.Id).FirstOrDefault().Id + 1 : 1;

                //先取得原本的訂單資料
                var Old_Order = db.Order.FirstOrDefault(f => f.Id == Old_OrderId);
                string CathayStatus = null;

                //付款方式為信用卡
                if (Old_Order.CashFlowType == 2)
                {
                    CathayStatus = "9999";
                }

                var New_Order = new Order()
                {
                    Id = Id,
                    RelativeId = Old_Order.Id,
                    VolunteersId = Old_Order.VolunteersId,
                    SN = Old_Order.SN,                  
                    Status = 1, //未結單
                    LogisticsType = 1,
                    CashFlowType = Old_Order.CashFlowType,
                    CashFlowStatus = 3, //未退款
                    CathayStatus = CathayStatus,
                    BankCode = Old_Order.BankCode,
                    BankAccount = Old_Order.BankAccount,
                    AccountName = Old_Order.AccountName,
                    ReceiverName = Old_Order.ReceiverName,
                    ReceiverTel = Old_Order.ReceiverTel,
                    ReceiverAddress = ReceiverAddress,
                    ReceiveTimeRange = ReceiveTimeRange,
                    ReturnGoods = false,
                    RefundReason = 1,
                    AgreeRefund = false,
                    Remark = HttpUtility.HtmlDecode(Remark),
                    TaxId = Old_Order.TaxId,
                    PrintPaper = Old_Order.PrintPaper,
                    CreateTime = DateTime.UtcNow,
                    UpdateTime = DateTime.UtcNow,
                    LastUserId = LastUserId,
                };
                db.Order.InsertOnSubmit(New_Order);
                db.SubmitChanges();
                return Id;
            }
            catch (Exception e)
            {
                return -1;
            }
            finally
            {
                db.Connection.Close();
            }

        }


        #endregion

        #region update

        public int Update(ShippingGoodsShow item)
        {
            try
            {
                MithDataContext db = new MithDataContext();
                int Id = 0;
                var data = db.Order.FirstOrDefault(f => f.Id == item.Id);
                if (data != null)
                {
                    //data.Name = item.Name;
                    //data.Pic = item.Pic;
                    //data.Cover = item.Cover;
                    //data.Url = item.Url;
                    //data.Content = HttpUtility.HtmlEncode(item.Content);
                    //data.Enable = item.Enable;
                    //data.UpdateTime = DateTime.UtcNow;
                    //data.LastUserId = item.LastUserId;
                    db.SubmitChanges();
                    Id = data.Id;
                }
                db.Connection.Close();
                return Id;
            }
            catch { return -1; }
        }

        #endregion

        #region Delete
        public void Delete(int id = 0)
        {
            if (id > 0)
            {
                Delete(new int[] { id });
            }
        }

        public void Delete(int[] id)
        {
            if (id != null)
            {
                if (id.Any())
                {
                    MithDataContext db = new MithDataContext();
                    var data = db.Order.Where(w => id.Contains(w.Id));
                    if (data.Any())
                    {
                        db.Order.DeleteAllOnSubmit(data);

                        db.SubmitChanges();
                    }
                    db.Connection.Close();
                }
            }
        }

        #endregion

        public static List<SelectListItem> Get_ReceiverTimeRange_Select(int Id)
        {
            MithDataContext db = new MithDataContext();
            List<SelectListItem> data = new List<SelectListItem>();

            data.AddRange(db.Category.Where(w => w.Fun_Id == 10 && w.Group == "ReceiverTimeRange").OrderBy(o => o.Id).Select(s =>
                new SelectListItem
                {
                    Selected = Id == s.Id,
                    Text = s.Name,
                    Value = s.Id.ToString()
                }));
            db.Connection.Close();
            return data;
        }

        //#region Export
        //public MemoryStream Set_Excel(List<Export> data)
        //{
        //    //標題
        //    List<string> header = new List<string>();
        //    header.Add("廠商編號");
        //    header.Add("名稱");
        //    header.Add("統一編號");
        //    header.Add("電話");
        //    header.Add("傳真");
        //    header.Add("地址");
        //    header.Add("負責人姓名");
        //    header.Add("負責人身分證字號");

        //    MemoryStream ms = new MemoryStream();
        //    HSSFWorkbook workbook = new HSSFWorkbook();
        //    HSSFSheet sheet = (HSSFSheet)workbook.CreateSheet("sheet");

        //    //宣告headStyle、內容style
        //    HSSFCellStyle headStyle = null;
        //    HSSFCellStyle contStyle = null;
        //    headStyle = (HSSFCellStyle)workbook.CreateCellStyle();
        //    contStyle = (HSSFCellStyle)workbook.CreateCellStyle();
        //    HSSFFont font = null;
        //    font = (HSSFFont)workbook.CreateFont();
        //    HSSFCell cell = null;

        //    //標題粗體、黃色背景
        //    headStyle.FillForegroundColor = HSSFColor.LightYellow.Index;
        //    headStyle.FillPattern = NPOI.SS.UserModel.FillPattern.SolidForeground;
        //    //標題字型樣式
        //    font.FontHeightInPoints = 10;
        //    font.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
        //    font.FontName = "Microsoft JhengHei";
        //    headStyle.SetFont(font);
        //    //內容字型樣式(自動換行)
        //    contStyle.WrapText = true;
        //    //contStyle.VerticalAlignment = NPOI.SS.UserModel.VerticalAlignment.Center;
        //    //宣告headRow
        //    sheet.CreateRow(0);
        //    //設定head背景、粗體、內容
        //    foreach (var i in header)
        //    {
        //        cell = (HSSFCell)sheet.GetRow(0).CreateCell(header.IndexOf(i));
        //        cell.SetCellValue(i);
        //        cell.CellStyle = headStyle;
        //    }
        //    //資料欄位
        //    for (int i = 0; i < data.Count; i++)
        //    {
        //        sheet.CreateRow(i + 1);

        //        sheet.GetRow(i + 1).CreateCell(0).SetCellValue(data[i].SN);
        //        sheet.GetRow(i + 1).CreateCell(1).SetCellValue(data[i].Name);
        //        sheet.GetRow(i + 1).CreateCell(2).SetCellValue(data[i].TaxId);
        //        sheet.GetRow(i + 1).CreateCell(3).SetCellValue(data[i].Tel);
        //        sheet.GetRow(i + 1).CreateCell(4).SetCellValue(data[i].Fax);
        //        sheet.GetRow(i + 1).CreateCell(5).SetCellValue(data[i].City_Name + data[i].Area_Name + data[i].Address);
        //        sheet.GetRow(i + 1).CreateCell(6).SetCellValue(data[i].BossName);
        //        sheet.GetRow(i + 1).CreateCell(7).SetCellValue(data[i].BossIdentityCard);
        //        sheet.GetRow(i + 1).CreateCell(8).SetCellValue(data[i].ContactName);
        //        sheet.GetRow(i + 1).CreateCell(9).SetCellValue(data[i].ContactTel);
        //        sheet.GetRow(i + 1).CreateCell(10).SetCellValue(data[i].ContactEmail);
        //        sheet.GetRow(i + 1).CreateCell(11).SetCellValue(data[i].ContractDate);
        //        sheet.GetRow(i + 1).CreateCell(12).SetCellValue(data[i].ArtEditorCost);
        //        sheet.GetRow(i + 1).CreateCell(13).SetCellValue(data[i].FreightFee);
        //        sheet.GetRow(i + 1).CreateCell(14).SetCellValue(data[i].ManagementFee);
        //        sheet.GetRow(i + 1).CreateCell(15).SetCellValue(data[i].Collaborate);
        //        sheet.GetRow(i + 1).CreateCell(16).SetCellValue(data[i].TypeName);
        //        sheet.GetRow(i + 1).CreateCell(17).SetCellValue(data[i].GoodsKind);
        //        sheet.GetRow(i + 1).CreateCell(18).SetCellValue(data[i].AccountingName);
        //        sheet.GetRow(i + 1).CreateCell(19).SetCellValue(data[i].AccountingTel);
        //        sheet.GetRow(i + 1).CreateCell(20).SetCellValue(data[i].AccountingEmail);
        //        sheet.GetRow(i + 1).CreateCell(21).SetCellValue(data[i].BankCode);
        //        sheet.GetRow(i + 1).CreateCell(22).SetCellValue(data[i].BankName);
        //        sheet.GetRow(i + 1).CreateCell(23).SetCellValue(data[i].Account);
        //        sheet.GetRow(i + 1).CreateCell(24).SetCellValue(data[i].AccountName);
        //        sheet.GetRow(i + 1).CreateCell(25).SetCellValue(data[i].CreateTime);
        //    }
        //    foreach (var i in header)
        //    {
        //        sheet.SetColumnWidth(header.IndexOf(i), (short)256 * 12);
        //    }

        //    workbook.Write(ms);
        //    ms.Position = 0;
        //    ms.Flush();
        //    return ms;
        //}

        //public List<Export> Export_Data(ShippingGoodsModel data)
        //{

        //    /*-------------------匯出宣告-------------------*/
        //    MithDataContext db = new MithDataContext();
        //    Export export = new Export();
        //    List<Export> exp = new List<Export>();
        //    var item = data.Get().OrderByDescending(o => o.Id);
        //    /*-------------------匯出宣告End-------------------*/

        //    foreach (var i in item)
        //    {

        //        export.Name = i.Name;
        //        export.CreateTime = i.CreateTime.Value.ToString("yyyy/MM/dd HH:mm");

        //        exp.Add(export);
        //        export = new Export();
        //    }
        //    db.Connection.Close();
        //    return exp;
        //}
        //#endregion

    }
}