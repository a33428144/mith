﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using System.Data.Linq.SqlClient;
using System.IO;
using Mith.Areas.GoX.Models;
using Mith.Models;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;

namespace Mith.Areas.GoX.Models
{
    public class LikeExpertModel
    {
        #region Class
        public class LikeExpertShow
        {
            [Key]
            [DisplayName("編號")]
            public int Id { get; set; }
            public int ExpertId { get; set; }
            public int VolunteersId { get; set; }
            [DisplayName("達人姓名")]
            public string Expert_Name { get; set; }
            [DisplayName("留言會員")]
            public string Volunteers_Name { get; set; }
            public string Volunteers_Tel { get; set; }
            public string Volunteers_Email { get; set; }
            [DisplayName("新增時間")]
            public DateTime? CreateTime { get; set; }

        }

        public string Keyword = "";
        public DateTime? CreateTime_St = null;
        public DateTime? CreateTime_End = null;

        #endregion
        public LikeExpertModel()
        {
        }
        public void Clear()
        {
            Keyword = "";
            CreateTime_St = null;
            CreateTime_End = null;
        }

        #region Get (私訊會員)


        public List<LikeExpertShow> Get_Data(int p = 1, int take = 10, int VolunteersId = 0)
        {
            MithDataContext db = new MithDataContext();
            var data = Get(VolunteersId).OrderByDescending(o => o.Id).Skip((p - 1) * take).Take(take);
            List<LikeExpertShow> item = new List<LikeExpertShow>();
            item = (from i in data
                    select new LikeExpertShow
                    {
                        Id = i.Id,
                        ExpertId = i.ExpertId,
                        Expert_Name = i.Expert_Name,
                        VolunteersId = i.VolunteersId,
                        Volunteers_Name = i.Volunteers_Name,
                        Volunteers_Email = i.Volunteers_Email,
                        CreateTime = i.CreateTime
                    }).ToList();
            db.Connection.Close();
            return item;
        }
        public Method.Paging Get_Page(int p = 1, int take = 10, int VolunteersId = 0)
        {
            return Method.Get_Page(Get_Count(VolunteersId), p, take);
        }

        public int Get_Count(int VolunteersId = 0)
        {
            return Get(VolunteersId).Count();
        }

        private IQueryable<LikeExpert_View> Get(int VolunteersId = 0)
        {
            MithDataContext db = new MithDataContext();
            IQueryable<LikeExpert_View> data = db.LikeExpert_View.Where(w => w.VolunteersId == VolunteersId);
            if (Keyword != "")
            {
                data = data.Where(Query(Keyword));
            }

            db.Connection.Close();
            return data;
        }

        private string Query(string query = "")
        {
            string sql = "";

            if (query != "")
            {
                query = HttpUtility.HtmlEncode(query);
                sql = Method.SQL_Combin(sql, "Expert_Name", "OR", "( \"" + query + "\")", ".Contains", false);
            }

            return sql;
        }

        #endregion



        #region Delete

        public void Delete(int id = 0)
        {
            if (id > 0)
            {
                MithDataContext db = new MithDataContext();
                var data = db.LikeExpert.FirstOrDefault(w => w.Id == id);

                if (data != null)
                {
                    //再刪私訊會員
                    db.LikeExpert.DeleteOnSubmit(data);
                    db.SubmitChanges();
                }
                db.Connection.Close();
            }
        }

        #endregion



    }
}