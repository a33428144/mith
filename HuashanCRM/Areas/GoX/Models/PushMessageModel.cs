﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using Mith.Models;
using System.IO;

namespace Mith.Areas.GoX.Models
{
    public class PushMessageModel
    {
        public class PushMessageShow
        {
            [Key]
            [DisplayName("編號")]
            public int Id { get; set; }
            [DisplayName("App代號")]
            public int AppId { get; set; }
            [DisplayName("App名稱")]
            public string AppName { get; set; }
            [DisplayName("標題")]
            [Required(ErrorMessage = "必填欄位")]
            public string Title { get; set; }
            [DisplayName("內容")]
            [Required(ErrorMessage = "必填欄位")]
            public string Content { get; set; }
            [DisplayName("建立時間")]
            public DateTime CreateTime { get; set; }
            [DisplayName("接收會員編號")]
            public int? VolunteerId { get; set; }
            [DisplayName("會員")]
            public string VolunteerName { get; set; }
            [DisplayName("會員")]
            public string VolunteerAccount { get; set; }
            [DisplayName("狀態")]
            public bool Sended { get; set; }
        }

        public PushMessageModel(int fun)
        {
            Fun_Id = fun;
        }

        public string Keyword = "";
        private int Fun_Id;
        public bool? Search_Sended = null;
        public DateTime? create_time_start = null;
        public DateTime? create_time_end = null;
        public int AppId = 0;
        public int VolunteerId = 0;

        public void Clear()
        {
            Keyword = "";
            create_time_start = null;
            create_time_end = null;
            Search_Sended = null;
            AppId = 0;
            VolunteerId = 0;
        }

        private PushMessage Convert(PushMessageShow data)
        {
            return new PushMessage
            {
                AppID = data.AppId,
                Title = data.Title,
                Content = data.Content,
                VolunteerId = data.VolunteerId,
                CreateTime = DateTime.UtcNow,
                Sended = data.Sended
            };
        }

        private PushMessageShow Convert(PushMessage data)
        {
            PushMessageShow item = new PushMessageShow
            {
                Id = data.Id,
                AppId = data.AppID,
                AppName = "",
                CreateTime = data.CreateTime,
                Content = data.Content,
                Title = data.Title,
                VolunteerId = data.VolunteerId,
                VolunteerAccount = "",
                VolunteerName = "",
                Sended = data.Sended
            };
            if (data.VolunteerId != null)
            {
                VolunteersModel vm = new VolunteersModel();
                var Vol = vm.Get_One(data.VolunteerId.Value);
                if (Vol != null)
                {
                    item.VolunteerAccount = Vol.Email;
                    item.VolunteerName = Vol.RealName;
                }
            }
            
            PushAppModel pam = new PushAppModel(Fun_Id);
            var pdata = pam.Get_One(data.AppID);
            if (pdata != null)
            {
                item.AppName = pdata.AppName + "(" + pdata.OS_Name + ")";
            }
            return item;
        }

        #region get
        public List<PushMessageShow> Get_Many(int[] id)
        {
            try
            {
                MithDataContext db = new MithDataContext();

                var data = db.PushMessage.Where(f => id.Contains(f.Id));
                if (data != null)
                {
                    return data.Select(s=>new PushMessageShow
                    {
                        Id = s.Id,
                        AppId = s.AppID,
                        AppName = db.PushApp.FirstOrDefault(f => f.Id == s.AppID && f.Del == false) == null ?
                                    "" : db.PushApp.FirstOrDefault(f => f.Id == s.AppID && f.Del == false).AppName,
                        Content = s.Content,
                        CreateTime = s.CreateTime,
                        Sended = s.Sended,
                        Title = s.Title,
                        VolunteerId = s.VolunteerId,
                        VolunteerAccount = db.Volunteers.FirstOrDefault(f => f.Id == s.VolunteerId  ) == null ?
                        "" : db.Volunteers.FirstOrDefault(f => f.Id == s.VolunteerId  ).Email,
                        VolunteerName = db.Volunteers.FirstOrDefault(f => f.Id == s.VolunteerId  ) == null ?
                        "" : db.Volunteers.FirstOrDefault(f => f.Id == s.VolunteerId  ).RealName +" ( "+ s.VolunteerId+" ) "
                    }).ToList();
                }
                db.Connection.Close();
            }
            catch { }
            return null;
        }

        public PushMessageShow Get_One(int id)
        {
            try
            {
                MithDataContext db = new MithDataContext();

                var data = db.PushMessage.FirstOrDefault(f => f.Id == id);
                if (data != null)
                {
                    return Convert(data);
                }
                db.Connection.Close();
            }
            catch { }
            return null;
        }

        public List<PushMessageShow> Get_Data(int p = 1, int take = 10, int VolunteerId=0)
        {
            try
            {
                MithDataContext db = new MithDataContext();

                List<PushMessageShow> item = Get().OrderByDescending(o=>o.CreateTime).Skip((p - 1) * take).Take(take).ToList();

                db.Connection.Close();
                return item;
            }
            catch { return new List<PushMessageShow>(); }
        }

        public Method.Paging Get_Page(int p = 1, int take = 10, int pages = 5)
        {
            return Method.Get_Page(Get_Count(), p, take, pages);
        }

        public int Get_Count()
        {
            try
            {
                return Get().Count();
            }
            catch { return 0; }
        }

        private IQueryable<PushMessageShow> Get()
        {
            MithDataContext db = new MithDataContext();
            IQueryable<PushMessageShow> data = db.PushMessage.Select(s => new PushMessageShow
            {
                Id = s.Id,
                AppId = s.AppID,
                AppName = db.PushApp.FirstOrDefault(f => f.Id == s.AppID && f.Del == false) == null ?
                            "" : db.PushApp.FirstOrDefault(f => f.Id == s.AppID && f.Del == false).AppName,
                Content = s.Content,
                Sended = s.Sended,
                CreateTime = s.CreateTime,
                Title = s.Title,
                VolunteerId = s.VolunteerId,
                VolunteerAccount = db.Volunteers.FirstOrDefault(f => f.Id == s.VolunteerId  ) == null ?
                "" : db.Volunteers.FirstOrDefault(f => f.Id == s.VolunteerId  ).Email,
                VolunteerName = db.Volunteers.FirstOrDefault(f => f.Id == s.VolunteerId  ) == null ?
                "" : db.Volunteers.FirstOrDefault(f => f.Id == s.VolunteerId  ).RealName +" ( "+ s.VolunteerId+" ) "
            });

            if (Keyword != "")
            {
                data = data.Where(Query(Keyword));
            }
            if (AppId != 0 )
            {
                data = data.Where(w => w.AppId == AppId);
            }
            if (Search_Sended!=null)
            {
                data = data.Where(w => w.Sended == Search_Sended.Value);
            }
            if (create_time_start != null)
            {
                data = data.Where(w => w.CreateTime >= create_time_start.Value);
            }
            if (create_time_end != null)
            {
                data = data.Where(w => w.CreateTime <= create_time_end.Value);
            }
            if (VolunteerId != 0)
            {
                data = data.Where(w => w.VolunteerId == VolunteerId || w.VolunteerId ==0 );
            }
            db.Connection.Close();
            return data;
        }

        private string Query(string query = "")
        {
            string sql = "";

            if (query != "")
            {
                query = HttpUtility.HtmlEncode(query);
                sql = Method.SQL_Combin(sql, "Title", "OR", "( \"" + query + "\")", ".Contains", false);
                sql = Method.SQL_Combin(sql, "Content", "OR", "( \"" + query + "\")", ".Contains", false);
            }

            return sql;
        }
        #endregion

        #region insert
        public int Insert(PushMessageShow item)
        {
            MithDataContext db = new MithDataContext();
            PushMessage new_item = Convert(item);
            db.PushMessage.InsertOnSubmit(new_item);
            db.SubmitChanges();
            db.Connection.Close();
            return new_item.Id;
        }

        /// <summary>
        /// 新增資料
        /// </summary>
        /// <param name="appid">AppId</param>
        /// <param name="title">標題</param>
        /// <param name="content">內容</param>
        /// <param name="vid">vid==null ? 全部會員 : 指定會員</param>
        /// <returns> return > 0 ? 成功 : 失敗</returns>
        public static int Insert(int appid, string title, string content, int? vid)
        {
            try
            {
                MithDataContext db = new MithDataContext();
                var item = new PushMessage
                {
                    AppID = appid,
                    Title = title,
                    Content = content,
                    VolunteerId = vid,
                    CreateTime = DateTime.UtcNow,
                    Sended = false
                };
                db.PushMessage.InsertOnSubmit(item);
                db.SubmitChanges();
                db.Connection.Close();
                return item.Id;
            }
            catch (Exception e)
            {
                ErrorRecordModel.Error_Record(-2, e.HResult.ToString(), "System/PushMessage/Insert", false, e.Message + "<br />\r\n<br />\r\n" + e.Source + "<br />\r\n<br />\r\n" + e.StackTrace);
                return -1;
            }
        }
        #endregion

        #region update
        public int Update(PushMessageShow item)
        {
            MithDataContext db = new MithDataContext();
            var data = db.PushMessage.FirstOrDefault(f => f.Id == item.Id);

            if (data != null)
            {
                data.Title = item.Title;
                data.AppID = item.AppId;
                data.Content = item.Content;
                data.Sended = item.Sended;
                data.VolunteerId = item.VolunteerId;
                db.SubmitChanges();
                db.Connection.Close();
                return data.Id;
            }
            db.Connection.Close();
            return -1;
        }

        public static int Update_Sended(int id)
        {
            MithDataContext db = new MithDataContext();
            var data = db.PushMessage.FirstOrDefault(f => f.Id == id );

            if (data != null)
            {
                data.Sended = true;
                db.SubmitChanges();
                db.Connection.Close();
                return data.Id;
            }
            db.Connection.Close();
            return -1;
        }
        #endregion

        #region delete
        public static void Delete(int id)
        {
            if (id > 0)
            {
                Delete(new int[] { id });
            }
        }

        public static void Delete(int[] id)
        {
                 MithDataContext db = new MithDataContext();
            if (id != null)
            {
                if (id.Any())
                {               
                    var data = db.PushMessage.Where(w => id.Contains(w.Id));
                    if (data.Any())
                    {
                        db.PushMessage.DeleteAllOnSubmit(data);

                    }
                    db.SubmitChanges();
                 
                }
            }
                   db.Connection.Close();
        }
        #endregion

        public static List<SelectListItem> Select_Volunteers(int Id, bool all)
        {
            MithDataContext db = new MithDataContext();
            List<SelectListItem> data = new List<SelectListItem>();
            data.Add(new SelectListItem
            {
                Selected = Id == 0,
                Text = "全部",
                Value = "0"
            });
            data.AddRange(db.Volunteers.Where(f => f.Id != null).OrderBy(o => o.Id).Select(s =>
                new SelectListItem
                {
                    Selected = Id == s.Id,
                    Text = "編號 : " + s.Id + "　" + "姓名 : " + s.RealName,
                    Value = s.Id.ToString()
                }));
            db.Connection.Close();
            return data;
        }
    }
}