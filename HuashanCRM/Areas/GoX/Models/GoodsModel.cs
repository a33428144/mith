﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using System.Text;
using System.Data.Linq.SqlClient;
using System.IO;
using System.Drawing;
using Mith.Areas.GoX.Models;
using Mith.Models;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using LinqToExcel;


namespace Mith.Areas.GoX.Models
{
	public class GoodsModel
	{
		#region Class
		public class GoodsShow
		{
			[Key]
			[DisplayName("編號")]
			public int Id { get; set; }
			[DisplayName("商品編號")]
			[Required(ErrorMessage = "必填欄位")]
			public string SN { get; set; }
			[DisplayName("商品名稱")]
			[Required(ErrorMessage = "必填欄位")]
			public string Name { get; set; }
			[DisplayName("商品照片")]
			public string Files { get; set; }
			[DisplayName("售價")]
			[Range(1, 9999999, ErrorMessage = "需大於 0")]
			[Required(ErrorMessage = "必填欄位")]
			[DataAnnotationsExtensions.Integer(ErrorMessage = "請輸入數字")]
			public int Price { get; set; }
			[DisplayName("特價")]
			[Range(1, 9999999, ErrorMessage = "需大於 0")]
			[DataAnnotationsExtensions.Integer(ErrorMessage = "請輸入數字")]
			public int? SpecialOffer { get; set; }
			[DisplayName("類別")]
			public int Type { get; set; }
			[DisplayName("類別")]
			public string Type_Name { get; set; }
			[DisplayName("狀態")]
			public int Status { get; set; }
			[DisplayName("狀態")]
			public string Status_Name { get; set; }
			[DisplayName("集單日期")]
			public DateTime? LimitOrderStart { get; set; }
			[DisplayName("集單日期")]
			[Required(ErrorMessage = "必填欄位")]
			public DateTime? LimitOrderEnd { get; set; }
			[DisplayName("幾天出貨")]
			[Range(1, 9999999, ErrorMessage = "需大於 0")]
			[DataAnnotationsExtensions.Integer(ErrorMessage = "請輸入數字")]
			[Required(ErrorMessage = "必填欄位")]
			public int? FewDaysShipping { get; set; }
			[DisplayName("商品說明")]
			[DataType(DataType.MultilineText)]
			public string Content { get; set; }
			[DisplayName("款式")]
			public string Pattern { get; set; }
			public string Pattern_Name { get; set; }
			[DisplayName("季節")]
			public int Season { get; set; }
			public string Season_Name { get; set; }
			[DisplayName("顏色")]
			public int Color { get; set; }
			public string Color_Name { get; set; }
			[DisplayName("風格 (最多兩項)")]
			public string Style { get; set; }
			[DisplayName("適合 (最多兩項)")]
			public string Fit { get; set; }
			[DisplayName("貨到通知")]
			public int NoticeCount { get; set; }
			[DisplayName("穿搭次數")]
			public int WearCount { get; set; }
			[DisplayName("商品照")]
			public string Pic_Middle { get; set; }
			public string Pic_Small { get; set; }
			[DisplayName("建立時間")]
			public DateTime? CreateTime { get; set; }
			[DisplayName("公司Id")]
			public int CompanyId { get; set; }
			[DisplayName("公司名稱")]
			public string Company_Name { get; set; }
			[DisplayName("品牌")]
			public int BrandId { get; set; }
			[DisplayName("品牌")]
			public string Brand_Name { get; set; }

			[DisplayName("尺寸判斷")]
			public string SizeType { get; set; }
			[DisplayName("庫存/集單量")]
			public List<GoodsQuantity> SizeList { get; set; }
			[DisplayName("庫存量")]
			public int Quantity_Total { get; set; }
			[DisplayName("銷售量")]
			public int SellQuantity_Total { get; set; }

			#region 尺寸
			[DataAnnotationsExtensions.Integer(ErrorMessage = "請輸入數字")]
			public int? F { get; set; }
			[DataAnnotationsExtensions.Integer(ErrorMessage = "請輸入數字")]
			public int? XS { get; set; }
			[DataAnnotationsExtensions.Integer(ErrorMessage = "請輸入數字")]
			public int? S { get; set; }
			[DataAnnotationsExtensions.Integer(ErrorMessage = "請輸入數字")]
			public int? M { get; set; }
			[DataAnnotationsExtensions.Integer(ErrorMessage = "請輸入數字")]
			public int? L { get; set; }
			[DataAnnotationsExtensions.Integer(ErrorMessage = "請輸入數字")]
			public int? XL { get; set; }
			[DataAnnotationsExtensions.Integer(ErrorMessage = "請輸入數字")]
			public int? XXL { get; set; }
			#endregion

		}

		public class GoodsShowEdit
		{
			[Key]
			[DisplayName("編號")]
			public int Id { get; set; }
			[DisplayName("商品編號")]
			[Required(ErrorMessage = "必填欄位")]
			public string SN { get; set; }
			[DisplayName("商品名稱")]
			[Required(ErrorMessage = "必填欄位")]
			public string Name { get; set; }
			[DisplayName("商品照片")]
			public string Files { get; set; }
			[DisplayName("售價")]
			[Range(1, 9999999, ErrorMessage = "需大於 0")]
			[Required(ErrorMessage = "必填欄位")]
			[DataAnnotationsExtensions.Integer(ErrorMessage = "請輸入數字")]
			public int Price { get; set; }
			[DisplayName("特價")]
			[Range(1, 9999999, ErrorMessage = "需大於 0")]
			[DataAnnotationsExtensions.Integer(ErrorMessage = "請輸入數字")]
			public int? SpecialOffer { get; set; }
			[DisplayName("類別")]
			public int Type { get; set; }
			[DisplayName("類別")]
			public string Type_Name { get; set; }
			[DisplayName("狀態")]
			public int Status { get; set; }
			[DisplayName("狀態")]
			public string Status_Name { get; set; }
			[DisplayName("集單日期")]
			[DataType(DataType.Date)]
			public DateTime? LimitOrderStart { get; set; }
			[DisplayName("集單日期")]
			[DataType(DataType.Date)]
			[Required(ErrorMessage = "必填欄位")]
			public DateTime? LimitOrderEnd { get; set; }
			[DisplayName("幾天出貨")]
			[Range(1, 99999999999, ErrorMessage = "需大於 0")]
			[DataAnnotationsExtensions.Integer(ErrorMessage = "請輸入數字")]
			[Required(ErrorMessage = "必填欄位")]
			public int? FewDaysShipping { get; set; }
			[DisplayName("商品說明")]
			[DataType(DataType.MultilineText)]
			public string Content { get; set; }
			[DisplayName("款式")]
			public string Pattern { get; set; }
			[DisplayName("季節")]
			public int Season { get; set; }
			[DisplayName("顏色")]
			public int Color { get; set; }

			[DisplayName("風格 (最多兩項)")]
			public string Style { get; set; }
			[DisplayName("適合 (最多兩項)")]
			public string Fit { get; set; }
			[DisplayName("貨到通知")]
			public int NoticeCount { get; set; }
			[DisplayName("商品照片")]
			public string Pic_Middle { get; set; }
			public string Pic_Small { get; set; }
			[DisplayName("建立時間")]
			public DateTime? CreateTime { get; set; }
			[DisplayName("更新時間")]
			public DateTime? UpdateTime { get; set; }
			[DisplayName("更新人員")]
			public int LastUserId { get; set; }
			[DisplayName("更新人員")]
			public string LastUser_Name { get; set; }
			[DisplayName("公司")]
			public int CompanyId { get; set; }
			[DisplayName("公司名稱")]
			public string Company_Name { get; set; }
			[DisplayName("品牌")]
			public int BrandId { get; set; }
			[DisplayName("品牌")]
			public string Brand_Name { get; set; }

			[DisplayName("尺寸判斷")]
			public string SizeType { get; set; }
			[DisplayName("庫存量")]
			public List<SizeShow> SizeList { get; set; }

			#region 尺寸
			[DataAnnotationsExtensions.Integer(ErrorMessage = "請輸入數字")]
			public int? F { get; set; }
			[DataAnnotationsExtensions.Integer(ErrorMessage = "請輸入數字")]
			public int? XS { get; set; }
			[DataAnnotationsExtensions.Integer(ErrorMessage = "請輸入數字")]
			public int? S { get; set; }
			[DataAnnotationsExtensions.Integer(ErrorMessage = "請輸入數字")]
			public int? M { get; set; }
			[DataAnnotationsExtensions.Integer(ErrorMessage = "請輸入數字")]
			public int? L { get; set; }
			[DataAnnotationsExtensions.Integer(ErrorMessage = "請輸入數字")]
			public int? XL { get; set; }
			[DataAnnotationsExtensions.Integer(ErrorMessage = "請輸入數字")]
			public int? XXL { get; set; }
			#endregion

		}

		public class SizeShow
		{
			public string Name { get; set; }
			public int? Quantity { get; set; }
			public int? SellQuantity { get; set; }
		}

		public partial class ImportClass
		{
			[Key]
			public int Id { get; set; }
			public string SN { get; set; }
			public string Name { get; set; }
			public string Content { get; set; }
			public int Price { get; set; }
			public int? SpeicalOffer { get; set; }
			public string Type { get; set; }
			public int TypeId { get; set; }
			public DateTime? LimitOrderStart { get; set; }
			public DateTime? LimitOrderEnd { get; set; }
			public string FewDaysShipping { get; set; }
			public string Season { get; set; }
			public int SeasonId { get; set; }
			public string Color { get; set; }
			public int ColorId { get; set; }
			public string Style { get; set; }
			public string Fit { get; set; }
			public string SizeType { get; set; }
			public string BrandName { get; set; }
			public int BrandId { get; set; }
			public int F { get; set; }
			public int XS { get; set; }
			public int S { get; set; }
			public int M { get; set; }
			public int L { get; set; }
			public int XL { get; set; }
			public int XXL { get; set; }
		}

		public class CheckResult
		{
			public Guid ID { get; set; }

			public bool Success { get; set; }

			public int RowCount { get; set; }

			public int ErrorCount { get; set; }

			public string ErrorMessage { get; set; }

		}

		public class Export
		{
			[Key]
			public string SN { get; set; }
			public string Name { get; set; }
			public string Content { get; set; }
			public string Color_Name { get; set; }
			public int Price { get; set; }
			public int? SpecialOffer { get; set; }
			public string Type_Name { get; set; }
			public string LimitOrderStart { get; set; }
			public string LimitOrderEnd { get; set; }
			public string FewDaysShipping { get; set; }
			public string Pattern_Name { get; set; }
			public string Season_Name { get; set; }
			public string Style { get; set; }
			public string Fit { get; set; }
			public int Quantity_Total { get; set; }
			public int SellQuantity_Total { get; set; }
			public int? F_Quantity { get; set; }
			public int? F_SellQuantity { get; set; }
			public int? XS_Quantity { get; set; }
			public int? XS_SellQuantity { get; set; }
			public int? S_Quantity { get; set; }
			public int? S_SellQuantity { get; set; }
			public int? M_Quantity { get; set; }
			public int? M_SellQuantity { get; set; }
			public int? L_Quantity { get; set; }
			public int? L_SellQuantity { get; set; }
			public int? XL_Quantity { get; set; }
			public int? XL_SellQuantity { get; set; }
			public int? XXL_Quantity { get; set; }
			public int? XXL_SellQuantity { get; set; }
			public int NoticeCount { get; set; }
			public int WearCount { get; set; }
			public string Company_Name { get; set; }
			public string Brand_Name { get; set; }
			public string CreateTime { get; set; }
		}

		public string Keyword = "";
		public int Search_Type = 0;
		public string Search_Pattern = "0";
		public int Search_Season = 0;
		public int Search_Color = 0;
		public int Search_Style = 0;
		public int Search_Fit = 0;
		public int Search_Status = 0;
		public int Search_SpecialOffer = 0;
		public int Search_CompanyId = 0;
		public int Search_Quantity_Total = 0;
		public DateTime? CreateTime_St = null;
		public DateTime? CreateTime_End = null;
		public string TypeSort = "CreateTime";
		#endregion

		public GoodsModel()
		{
		}
		public void Clear()
		{
			Keyword = "";
			Search_Type = 0;
			Search_Pattern = "0";
			Search_Season = 0;
			Search_Color = 0;
			Search_Style = 0;
			Search_Fit = 0;
			Search_Status = 0;
			Search_SpecialOffer = 0;
			Search_CompanyId = 0;
			Search_Quantity_Total = 0;
			CreateTime_St = null;
			CreateTime_End = null;
			TypeSort = "CreateTime";
		}

		#region Get

		public List<GoodsShow> Get_Data_Mith(int p = 1, int take = 10)
		{
			MithDataContext db = new MithDataContext();

			var data = Get_Sort(Get()).Skip((p - 1) * take).Take(take);

			List<GoodsShow> item = new List<GoodsShow>();
			item = (from i in data
							select new GoodsShow
							{
								Id = i.Id,
								SN = i.SN,
								Name = i.Name.Length > 18 ? i.Name.Substring(0, 18) : i.Name,
								Pic_Middle = i.Pic_Middle,
								Pic_Small = i.Pic_Small,
								Type_Name = i.Type_Name,
								Quantity_Total = i.Quantity_Total.Value,
								SellQuantity_Total = i.SellQuantity_Total.Value,
								Price = i.Price,
								SpecialOffer = i.SpecialOffer,
								Status_Name = i.Status_Name,
								NoticeCount = i.NoticeCount,
								WearCount = i.WearCount,
								CreateTime = i.CreateTime.Value,
								Company_Name = i.Company_Name,
								Brand_Name = i.Brand_Name,
							}).ToList();
			db.Connection.Close();

			return item;
		}

		public List<GoodsShow> Get_Data_Company(int p = 1, int take = 10)
		{
			MithDataContext db = new MithDataContext();

			var data = Get_Sort(Get()).Skip((p - 1) * take).Take(take);

			List<GoodsShow> item = new List<GoodsShow>();
			item = (from i in data
							select new GoodsShow
							{
								Id = i.Id,
								SN = i.SN,
								Name = i.Name.Length > 18 ? i.Name.Substring(0, 18) : i.Name,
								Pic_Middle = i.Pic_Middle,
								Pic_Small = i.Pic_Small,
								Type_Name = i.Type_Name,
								SizeList = Get_SizeList(i.Id),
								Price = i.Price,
								SpecialOffer = i.SpecialOffer,
								Status_Name = i.Status_Name,
								NoticeCount = i.NoticeCount,
								WearCount = i.WearCount,
								CreateTime = i.CreateTime.Value,
								Brand_Name = i.Brand_Name,
							}).ToList();
			db.Connection.Close();

			return item;
		}

		public GoodsShowEdit Get_One_Step1(int id)
		{
			MithDataContext db = new MithDataContext();
			var data = (from i in db.Goods_Edit_View
									where i.Id == id
									select new GoodsShowEdit
									{
										Id = i.Id,
										SN = i.SN,
										Name = i.Name,
										Price = i.Price,
										SpecialOffer = i.SpecialOffer,
										Type = i.Type ?? 0,
										Type_Name = i.Type_Name,
										LimitOrderStart = i.LimitOrderStart,
										LimitOrderEnd = i.LimitOrderEnd,
										FewDaysShipping = i.FewDaysShipping,
										Content = HttpUtility.HtmlDecode(i.Content),
										SizeType = i.SizeType,
										SizeList = SizeList(i.Id),
										NoticeCount = i.NoticeCount,
										Status = i.Status,
										Status_Name = i.Status_Name,
										CreateTime = i.CreateTime,
										UpdateTime = i.UpdateTime,
										LastUser_Name = i.LastUser_Name,
										CompanyId = i.CompanyId,
										Company_Name = i.Company_Name,
										BrandId = i.BrandId,
										Brand_Name = i.Brand_Name,
									}).FirstOrDefault();
			db.Connection.Close();
			return data;
		}


		public GoodsShowEdit Get_One_Step2(int id)
		{
			MithDataContext db = new MithDataContext();
			var data = (from i in db.Goods_Edit_View
									where i.Id == id
									select new GoodsShowEdit
									{
										SN = i.SN,
										Id = i.Id,
										Name = i.Name,
										Pattern = i.Pattern,
										Season = i.Season,
										Color = i.Color,
										Style = i.Style,
										Fit = i.Fit,
										CreateTime = i.CreateTime,
										UpdateTime = i.UpdateTime,
										LastUser_Name = i.LastUser_Name,
									}).FirstOrDefault();
			db.Connection.Close();
			return data;
		}


		public Method.Paging Get_Page(int p = 1, int take = 10)
		{
			return Method.Get_Page(Get_Count(), p, take);
		}

		public int Get_Count()
		{
			return Get().Count();
		}

		private IQueryable<Goods_Index_View> Get()
		{
			MithDataContext db = new MithDataContext();
			IQueryable<Goods_Index_View> data = db.Goods_Index_View;

			if (Keyword != "")
			{
				Keyword = Keyword.Replace(" ", "");
				data = data.Where(Query(Keyword));
			}
			if (Search_Type != 0)
			{
				data = data.Where(w => w.Type == Search_Type);
			}

			if (Search_Pattern != "0")
			{
				data = data.Where(w => w.Pattern == Search_Pattern);
			}

			if (Search_Season != 0)
			{
				data = data.Where(w => w.Season == Search_Season);
			}

			if (Search_Color != 0)
			{
				data = data.Where(w => w.Color == Search_Color);
			}

			if (Search_Style != 0)
			{
				data = data.Where(w => w.Style.Contains(Search_Style.ToString()));
			}

			if (Search_Fit != 0)
			{
				data = data.Where(w => w.Fit.Contains(Search_Fit.ToString()));
			}

			if (Search_Status != 0)
			{
				data = data.Where(w => w.Status == Search_Status);
			}

			if (Search_SpecialOffer != 0)
			{
				if (Search_SpecialOffer == 1)
				{
					data = data.Where(w => w.SpecialOffer.Value != null);
				}
				else
				{
					data = data.Where(w => w.SpecialOffer.Value == null);
				}
			}

			if (Search_CompanyId != 0)
			{
				data = data.Where(w => w.CompanyId == Search_CompanyId);
			}
			if (Search_Quantity_Total != 0)
			{
				data = data.Where(w => w.Quantity_Total == 0);
			}

			if (CreateTime_St != null)
			{
				data = data.Where(w => w.CreateTime >= CreateTime_St.Value);
			}
			if (CreateTime_End != null)
			{
				CreateTime_End = DateTime.Parse(CreateTime_End.Value.ToString("yyyy/MM/dd") + " 23:59:00");
				data = data.Where(w => w.CreateTime <= CreateTime_End);

			}
			db.Connection.Close();
			return data;
		}

		private IQueryable<Goods_Index_View> Get_Sort(IQueryable<Goods_Index_View> data)
		{
			switch (TypeSort)
			{
				case "CreateTime":
					data = data.OrderByDescending(o => o.Id);
					break;
				case "NoticeCount":
					data = data.OrderByDescending(o => o.NoticeCount).ThenByDescending(o => o.Id);
					break;
				case "WearCount":
					data = data.OrderByDescending(o => o.WearCount).ThenByDescending(o => o.Id);
					break;
				case "Quantity_Total":
					data = data.OrderByDescending(o => o.Quantity_Total.Value).ThenByDescending(o => o.Id);
					break;
				case "SellQuantity_Total":
					data = data.OrderByDescending(o => o.SellQuantity_Total.Value).ThenByDescending(o => o.Id);
					break;
				default:
					data = data.OrderByDescending(o => o.Id);
					break;
			}
			return data;
		}

		private string Query(string query = "")
		{
			string sql = "";

			if (query != "")
			{
				query = HttpUtility.HtmlEncode(query);
				sql = Method.SQL_Combin(sql, "SN", "OR", "( \"" + query + "\")", ".Contains", false);
				sql = Method.SQL_Combin(sql, "Name", "OR", "( \"" + query + "\")", ".Contains", false);
				sql = Method.SQL_Combin(sql, "Brand_Name", "OR", "( \"" + query + "\")", ".Contains", false);
			}

			return sql;
		}

		public List<SizeShow> SizeList(int id)
		{
			MithDataContext db = new MithDataContext();
			try
			{
				var Category = db.Category.Where(w => w.Group == "Size");
				var GoodsQuantity = db.GoodsQuantity.Where(w => w.GoodsId == id);
				var data = (from i in Category
										join j in GoodsQuantity
										on i.Name equals j.Size into k
										from j in k.DefaultIfEmpty()
										select new SizeShow
										{
											Name = i.Name,
											//有數量帶入數量，沒數量則為null
											Quantity = j == null ? 0 : j.Quantity,
											SellQuantity = j.SellQuantity ?? 0
										}).ToList();
				return data;
			}
			catch
			{
				return null;
			}
			finally
			{
				db.Connection.Close();
			}

		}

		/// <summary>
		/// 取得商品數量
		/// </summary>
		/// <param name="GoodsId">商品主檔編號</param>
		/// <returns>item</returns>
		public List<GoodsQuantity> Get_SizeList(int GoodsId)
		{
			MithDataContext db = new MithDataContext();
			var data = db.GoodsQuantity.Where(f => f.GoodsId == GoodsId) != null ? db.GoodsQuantity.Where(f => f.GoodsId == GoodsId).ToList() : null;
			db.Connection.Close();
			return data;
		}

		public static string Get_StyleName(string Style)
		{

			MithDataContext db = new MithDataContext();
			try
			{
				var data = Style.Split(",");
				string str = "";

				foreach (var item in data)
				{
					str += db.Category.FirstOrDefault(w => w.Number == int.Parse(item) && w.Group == "Style" && w.Fun_Id == 8).Name + ",";
				}

				return str.TrimEnd(',');
			}
			catch
			{
				return "";
			}
			finally { db.Connection.Close(); }
		}

		public static string Get_FitName(string Fit)
		{

			MithDataContext db = new MithDataContext();
			try
			{
				var data = Fit.Split(",");
				string str = "";

				foreach (var item in data)
				{
					str += db.Category.FirstOrDefault(w => w.Number == int.Parse(item) && w.Group == "Fit" && w.Fun_Id == 8).Name + ",";
				}
				return str.TrimEnd(',');
			}
			catch
			{
				return "";
			}
			finally { db.Connection.Close(); }
		}

		#endregion

		#region insert
		public int Insert(GoodsShow item)
		{

			item = Insert_Check(item);
			MithDataContext db = new MithDataContext();
			//新增商品主檔
			Goods new_item = new Goods
			{
				Id = item.Id,
				BrandId = item.BrandId,
				SN = item.SN,
				Name = item.Name,
				Status = 1,
				Price = item.Price,
				SpecialOffer = item.SpecialOffer,
				Type = item.Type,
				LimitOrderStart = item.LimitOrderStart,
				LimitOrderEnd = item.LimitOrderEnd,
				FewDaysShipping = item.FewDaysShipping,
				Content = HttpUtility.HtmlEncode(item.Content),
				SizeType = item.SizeType,
				Pattern = item.SN.Substring(13, 1),
				Season = item.Season,
				Color = item.Color,
				Style = item.Style,
				Fit = item.Fit,
				NoticeCount = 0,
				Sort = 0,
				CreateTime = DateTime.UtcNow,
				UpdateTime = DateTime.UtcNow,
				LastUserId = 0,
				CompanyId = item.CompanyId,

			};
			db.Goods.InsertOnSubmit(new_item);
			db.SubmitChanges();
			db.Connection.Close();

			#region 新增尺寸
			//選擇多尺寸
			if (item.SizeType == "Many")
			{
				if (item.XS != null && item.XS >= 0)
				{
					Insert_SizeQuantity(item.Id, "XS", item.XS.Value);
				}
				if (item.S != null && item.S >= 0)
				{
					Insert_SizeQuantity(item.Id, "S", item.S.Value);
				}
				if (item.M != null && item.M >= 0)
				{
					Insert_SizeQuantity(item.Id, "M", item.M.Value);
				}
				if (item.L != null && item.L >= 0)
				{
					Insert_SizeQuantity(item.Id, "L", item.L.Value);
				}
				if (item.XL != null && item.XL >= 0)
				{
					Insert_SizeQuantity(item.Id, "XL", item.XL.Value);
				}
				if (item.XXL != null && item.XXL >= 0)
				{
					Insert_SizeQuantity(item.Id, "XXL", item.XXL.Value);
				}
			}
			//選擇單尺寸
			else
			{
				if (item.F != null && item.F >= 0)
				{
					Insert_SizeQuantity(item.Id, "F", item.F.Value);
				}
			}
			#endregion

			return new_item.Id;
		}

		/// <summary>
		/// 後端驗證 (防止錯誤資料)
		/// </summary>
		/// <param name="item">Class</param>
		/// <returns>item</returns>
		/// 
		public GoodsShow Insert_Check(GoodsShow item)
		{
			//類型 = 現貨
			if (item.Type == 1)
			{
				item.LimitOrderStart = null;
				item.LimitOrderEnd = null;
				item.FewDaysShipping = null;
			}
			//類型 = 預訂(製)貨
			else if (item.Type == 2)
			{
				item.LimitOrderStart = null;
				item.LimitOrderEnd = null;
			}
			//類型 = 訂製款-集量生產
			else if (item.Type == 3)
			{
				item.LimitOrderStart = DateTime.Parse(item.LimitOrderStart.Value.ToString("yyyy/MM/dd") + " 00:00:00");
				item.LimitOrderEnd = DateTime.Parse(item.LimitOrderEnd.Value.ToString("yyyy/MM/dd") + " 23:59:50");
			}
			//類型 = 特別訂購款
			else
			{
				item.LimitOrderStart = null;
				item.LimitOrderEnd = null;
				item.FewDaysShipping = null;
				item.F = null;
				item.XS = null;
				item.S = null;
				item.M = null;
				item.L = null;
				item.XL = null;
				item.XXL = null;
			}

			//選擇多尺寸
			if (item.SizeType == "Many")
			{
				item.F = null;
			}
			//選擇單尺寸
			else
			{
				item.XS = null;
				item.S = null;
				item.M = null;
				item.L = null;
				item.XL = null;
				item.XXL = null;
			}
			return item;

		}

		public void Insert_Files(int GoodsId, string Path_Big, string Path_Middle, string Path_Small, int CompanyId)
		{
			MithDataContext db = new MithDataContext();

			var Check = db.GoodsPic.Where(w => w.GoodsId == GoodsId);
			bool Show = false;
			if (!Check.Any())
			{
				Show = true;
			}
			GoodsPic new_item = new GoodsPic
			{
				GoodsId = GoodsId,
				Pic_Big = Path_Big,
				Pic_Middle = Path_Middle,
				Pic_Small = Path_Small,
				CompanyId = CompanyId,
				Show = Show,
			};
			db.GoodsPic.InsertOnSubmit(new_item);
			db.SubmitChanges();
			db.Connection.Close();
		}

		public void Insert_SizeQuantity(int GoodsId, string Size, int Quantity)
		{
			if (Quantity >= 0)
			{
				MithDataContext db = new MithDataContext();

				GoodsQuantity new_item = new GoodsQuantity
				{
					GoodsId = GoodsId,
					Size = Size,
					Quantity = Quantity,
					LimitOrderQuantity = 0,
					SellQuantity = 0
				};
				db.GoodsQuantity.InsertOnSubmit(new_item);
				db.SubmitChanges();
				db.Connection.Close();
			}
		}
		#endregion

		#region update

		//public int Update_Step1(GoodsShowEdit item)
		//{
		//    item = Update_Step1_Check(item);
		//    try
		//    {
		//        MithDataContext db = new MithDataContext();
		//        int Id = 0;
		//        int ThisSize = 0;
		//        var data = db.Goods.FirstOrDefault(f => f.Id == item.Id);
		//        if (data != null)
		//        {
		//            data.SN = item.SN;
		//            data.Name = item.Name;
		//            data.Price = item.Price;
		//            data.SpecialOffer = item.SpecialOffer;
		//            data.Type = item.Type;
		//            data.LimitOrderStart = item.LimitOrderStart;
		//            data.LimitOrderEnd = item.LimitOrderEnd;
		//            data.FewDaysShipping = item.FewDaysShipping;
		//            data.SizeType = item.SizeType;
		//            data.Content = HttpUtility.HtmlEncode(item.Content);
		//            data.Status = item.Status;
		//            data.UpdateTime = DateTime.UtcNow;
		//            data.LastUserId = item.LastUserId;
		//            db.SubmitChanges();
		//            Id = data.Id;

		//            #region 編輯尺寸
		//            //選擇多尺寸
		//            if (item.SizeType == "Many")
		//            {
		//                if (item.XS != null && item.XS >= 0)
		//                {
		//                    ThisSize = db.GoodsQuantity.Where(w => w.GoodsId == item.Id && w.Size == "XS").Count();
		//                    if (ThisSize == 0)
		//                    {
		//                        Insert_SizeQuantity(item.Id, "XS", item.XS.Value);
		//                    }
		//                    else
		//                    {
		//                        Update_SizeQuantity(item.Id, "XS", item.XS.Value);
		//                    }
		//                }
		//                if (item.S != null && item.S >= 0)
		//                {
		//                    ThisSize = db.GoodsQuantity.Where(w => w.GoodsId == item.Id && w.Size == "S").Count();
		//                    if (ThisSize == 0)
		//                    {
		//                        Insert_SizeQuantity(item.Id, "S", item.S.Value);
		//                    }
		//                    else
		//                    {
		//                        Update_SizeQuantity(item.Id, "S", item.S.Value);
		//                    }
		//                }
		//                if (item.M != null && item.M >= 0)
		//                {
		//                    ThisSize = db.GoodsQuantity.Where(w => w.GoodsId == item.Id && w.Size == "M").Count();
		//                    if (ThisSize == 0)
		//                    {
		//                        Insert_SizeQuantity(item.Id, "M", item.M.Value);
		//                    }
		//                    else
		//                    {
		//                        Update_SizeQuantity(item.Id, "M", item.M.Value);
		//                    }
		//                }
		//                if (item.L != null && item.L >= 0)
		//                {
		//                    ThisSize = db.GoodsQuantity.Where(w => w.GoodsId == item.Id && w.Size == "L").Count();
		//                    if (ThisSize == 0)
		//                    {
		//                        Insert_SizeQuantity(item.Id, "L", item.L.Value);
		//                    }
		//                    else
		//                    {
		//                        Update_SizeQuantity(item.Id, "L", item.L.Value);
		//                    }
		//                }
		//                if (item.XL != null && item.XL >= 0)
		//                {
		//                    ThisSize = db.GoodsQuantity.Where(w => w.GoodsId == item.Id && w.Size == "XL").Count();
		//                    if (ThisSize == 0)
		//                    {
		//                        Insert_SizeQuantity(item.Id, "XL", item.XL.Value);
		//                    }
		//                    else
		//                    {
		//                        Update_SizeQuantity(item.Id, "XL", item.XL.Value);
		//                    }
		//                }
		//                if (item.XXL != null && item.XXL >= 0)
		//                {
		//                    ThisSize = db.GoodsQuantity.Where(w => w.GoodsId == item.Id && w.Size == "XXL").Count();
		//                    if (ThisSize == 0)
		//                    {
		//                        Insert_SizeQuantity(item.Id, "XXL", item.XXL.Value);
		//                    }
		//                    else
		//                    {
		//                        Update_SizeQuantity(item.Id, "XXL", item.XXL.Value);
		//                    }
		//                }
		//            }
		//            //選擇單尺寸
		//            else
		//            {
		//                if (item.F != null && item.F >= 0)
		//                {
		//                    ThisSize = db.GoodsQuantity.Where(w => w.GoodsId == item.Id && w.Size == "F").Count();
		//                    if (ThisSize == 0)
		//                    {
		//                        Insert_SizeQuantity(item.Id, "F", item.F.Value);
		//                    }
		//                    else
		//                    {
		//                        Update_SizeQuantity(item.Id, "F", item.F.Value);
		//                    }
		//                }
		//            }
		//            #endregion
		//        }
		//        db.Connection.Close();
		//        return Id;
		//    }
		//    catch { return -1; }
		//}

		public int Update(GoodsShowEdit item_Step1, GoodsShowEdit item_Step2)
		{
			try
			{
				item_Step1 = Update_Step1_Check(item_Step1);
				MithDataContext db = new MithDataContext();
				int Id = 0;
				int ThisSize = 0;
				var data = db.Goods.FirstOrDefault(f => f.Id == item_Step1.Id);
				if (data != null)
				{
					//Update_Step1
					data.SN = item_Step1.SN;
					data.Name = item_Step1.Name;
					data.Price = item_Step1.Price;
					data.SpecialOffer = item_Step1.SpecialOffer;
					data.Type = item_Step1.Type;
					data.BrandId = item_Step1.BrandId;
					data.LimitOrderStart = item_Step1.LimitOrderStart;
					data.LimitOrderEnd = item_Step1.LimitOrderEnd;
					data.FewDaysShipping = item_Step1.FewDaysShipping;
					data.SizeType = item_Step1.SizeType;
					data.Content = HttpUtility.HtmlEncode(item_Step1.Content);
					data.Status = item_Step1.Status;

					#region 編輯尺寸
					//選擇多尺寸
					if (item_Step1.SizeType == "Many")
					{
						if (item_Step1.XS != null && item_Step1.XS >= 0)
						{
							ThisSize = db.GoodsQuantity.Where(w => w.GoodsId == item_Step1.Id && w.Size == "XS").Count();
							if (ThisSize == 0)
							{
								Insert_SizeQuantity(item_Step1.Id, "XS", item_Step1.XS.Value);
							}
							else
							{
								Update_SizeQuantity(item_Step1.Id, "XS", item_Step1.XS.Value);
							}
						}
						if (item_Step1.S != null && item_Step1.S >= 0)
						{
							ThisSize = db.GoodsQuantity.Where(w => w.GoodsId == item_Step1.Id && w.Size == "S").Count();
							if (ThisSize == 0)
							{
								Insert_SizeQuantity(item_Step1.Id, "S", item_Step1.S.Value);
							}
							else
							{
								Update_SizeQuantity(item_Step1.Id, "S", item_Step1.S.Value);
							}
						}
						if (item_Step1.M != null && item_Step1.M >= 0)
						{
							ThisSize = db.GoodsQuantity.Where(w => w.GoodsId == item_Step1.Id && w.Size == "M").Count();
							if (ThisSize == 0)
							{
								Insert_SizeQuantity(item_Step1.Id, "M", item_Step1.M.Value);
							}
							else
							{
								Update_SizeQuantity(item_Step1.Id, "M", item_Step1.M.Value);
							}
						}
						if (item_Step1.L != null && item_Step1.L >= 0)
						{
							ThisSize = db.GoodsQuantity.Where(w => w.GoodsId == item_Step1.Id && w.Size == "L").Count();
							if (ThisSize == 0)
							{
								Insert_SizeQuantity(item_Step1.Id, "L", item_Step1.L.Value);
							}
							else
							{
								Update_SizeQuantity(item_Step1.Id, "L", item_Step1.L.Value);
							}
						}
						if (item_Step1.XL != null && item_Step1.XL >= 0)
						{
							ThisSize = db.GoodsQuantity.Where(w => w.GoodsId == item_Step1.Id && w.Size == "XL").Count();
							if (ThisSize == 0)
							{
								Insert_SizeQuantity(item_Step1.Id, "XL", item_Step1.XL.Value);
							}
							else
							{
								Update_SizeQuantity(item_Step1.Id, "XL", item_Step1.XL.Value);
							}
						}
						if (item_Step1.XXL != null && item_Step1.XXL >= 0)
						{
							ThisSize = db.GoodsQuantity.Where(w => w.GoodsId == item_Step1.Id && w.Size == "XXL").Count();
							if (ThisSize == 0)
							{
								Insert_SizeQuantity(item_Step1.Id, "XXL", item_Step1.XXL.Value);
							}
							else
							{
								Update_SizeQuantity(item_Step1.Id, "XXL", item_Step1.XXL.Value);
							}
						}
					}
					//選擇單尺寸
					else
					{
						if (item_Step1.F != null && item_Step1.F >= 0)
						{
							ThisSize = db.GoodsQuantity.Where(w => w.GoodsId == item_Step1.Id && w.Size == "F").Count();
							if (ThisSize == 0)
							{
								Insert_SizeQuantity(item_Step1.Id, "F", item_Step1.F.Value);
							}
							else
							{
								Update_SizeQuantity(item_Step1.Id, "F", item_Step1.F.Value);
							}
						}
					}
					#endregion

					//Update_Step2
					data.Pattern = item_Step2.SN.Substring(13, 1);
					data.Season = item_Step2.Season;
					data.Color = item_Step2.Color;
					data.Style = item_Step2.Style;
					data.Fit = item_Step2.Fit;
					data.UpdateTime = DateTime.UtcNow;
					data.LastUserId = item_Step2.LastUserId;
					db.SubmitChanges();
					Id = data.Id;
				}
				db.Connection.Close();
				return Id;
			}
			catch { return -1; }
		}

		/// <summary>
		/// 後端驗證 (防止錯誤資料)
		/// </summary>
		/// <param name="item">Class</param>
		/// <returns>item</returns>
		public GoodsShowEdit Update_Step1_Check(GoodsShowEdit item)
		{
			//類型 = 現貨
			if (item.Type == 1)
			{
				item.LimitOrderStart = null;
				item.LimitOrderEnd = null;
				item.FewDaysShipping = null;
			}
			//類型 = 預訂(製)貨
			else if (item.Type == 2)
			{
				item.LimitOrderStart = null;
				item.LimitOrderEnd = null;
			}
			//類型 = 訂製款-集量生產
			else if (item.Type == 3)
			{
				item.LimitOrderStart = DateTime.Parse(item.LimitOrderStart.Value.ToString("yyyy/MM/dd") + " 00:00:00");
				item.LimitOrderEnd = DateTime.Parse(item.LimitOrderEnd.Value.ToString("yyyy/MM/dd") + " 23:59:50");
			}
			//類型 = 特別訂購款
			else
			{
				item.LimitOrderStart = null;
				item.LimitOrderEnd = null;
				item.FewDaysShipping = null;
				item.F = null;
				item.XS = null;
				item.S = null;
				item.M = null;
				item.L = null;
				item.XL = null;
				item.XXL = null;
			}

			//選擇多尺寸
			if (item.SizeType == "Many")
			{
				item.F = null;
			}
			//選擇單尺寸
			else
			{
				item.XS = null;
				item.S = null;
				item.M = null;
				item.L = null;
				item.XL = null;
				item.XXL = null;
			}
			return item;

		}

		public void Update_SizeQuantity(int GoodsId, string Size, int Quantity)
		{
			if (Quantity >= 0)
			{
				MithDataContext db = new MithDataContext();
				var data = db.GoodsQuantity.FirstOrDefault(f => f.GoodsId == GoodsId && f.Size == Size);
				if (data != null)
				{
					data.Quantity = Quantity;
					db.SubmitChanges();
				}
				db.Connection.Close();
			}
		}
		#endregion

		#region Delete
		public void Delete(int id = 0)
		{
			if (id >= 0)
			{
				MithDataContext db = new MithDataContext();

				//刪除商品圖片
				DeletePic(0, id);

				//刪除商品數量
				var GoodsQuantity = db.GoodsQuantity.Where(w => w.GoodsId == id);
				if (GoodsQuantity.Any() && GoodsQuantity != null)
				{
					db.GoodsQuantity.DeleteAllOnSubmit(GoodsQuantity);
					db.SubmitChanges();
				}

				//刪除購物車商品
				var GoodsShoppingCart = db.GoodsShoppingCart.Where(w => w.GoodsId == id);
				if (GoodsShoppingCart.Any() && GoodsShoppingCart != null)
				{
					db.GoodsShoppingCart.DeleteAllOnSubmit(GoodsShoppingCart);
					db.SubmitChanges();
				}

				//刪除商品QA
				var GoodsQA = db.GoodsQA.Where(w => w.GoodsId == id);
				if (GoodsQA.Any() && GoodsQA != null)
				{
					db.GoodsQA.DeleteAllOnSubmit(GoodsQA);
					db.SubmitChanges();
				}

				//刪除訂單細節
				var OrderDetail = db.OrderDetail.Where(w => w.GoodsId == id);
				if (OrderDetail.Any() && OrderDetail != null)
				{
					foreach (var item in OrderDetail)
					{
						var Order = db.Order.FirstOrDefault(f => f.Id == item.OrderId);
						if (Order != null)
						{
							db.Order.DeleteOnSubmit(Order);
							db.SubmitChanges();
						}
						db.OrderDetail.DeleteOnSubmit(item);
						db.SubmitChanges();
					}

				}

				//刪除商品主檔
				var Goods = db.Goods.FirstOrDefault(w => w.Id == id);
				if (Goods != null)
				{
					var Article = db.Article.Where(w => w.RecommendSN1 == Goods.SN || w.RecommendSN2 == Goods.SN || w.RecommendSN3 == Goods.SN || w.RecommendSN4 == Goods.SN || w.RecommendSN5 == Goods.SN);

					if (Article.Any() && Article != null)
					{
						foreach (var item in Article)
						{
							var LikeArticle = db.LikeArticle.Where(f => f.ArticleId == item.Id);
							db.LikeArticle.DeleteAllOnSubmit(LikeArticle);
							db.SubmitChanges();

							var ArticleMessage = db.ArticleMessage.Where(f => f.ArticleId == item.Id);
							db.ArticleMessage.DeleteAllOnSubmit(ArticleMessage);
							db.SubmitChanges();

							db.Article.DeleteOnSubmit(item);
							db.SubmitChanges();
						}
					}
					db.Goods.DeleteOnSubmit(Goods);
					db.SubmitChanges();
				}
				db.Connection.Close();
			}
		}


		public void DeletePic(int Id = 0, int GoodsId = 0)
		{
			MithDataContext db = new MithDataContext();
			try
			{

				var GoodsPic = db.GoodsPic;
				List<GoodsPic> data = new List<GoodsPic>();
				//刪除商品單筆圖片
				if (Id > 0)
				{
					data = GoodsPic.Where(w => w.Id == Id).ToList();
				}

				//刪除商品底下所有圖片
				if (GoodsId > 0)
				{
					data = GoodsPic.Where(w => w.GoodsId == GoodsId).ToList();
				}
				if (data.Any())
				{
					foreach (var item in data)
					{
						BlobHelper.delete("goods/" + "Big/" + item.GoodsId, Path.GetFileName(item.Pic_Big));
						BlobHelper.delete("goods/" + "Middle/" + item.GoodsId, Path.GetFileName(item.Pic_Middle));
						BlobHelper.delete("goods/" + "Small/" + item.GoodsId, Path.GetFileName(item.Pic_Small));
					}
					db.GoodsPic.DeleteAllOnSubmit(data);
					db.SubmitChanges();
				}
			}
			catch { }
			finally
			{
				db.Connection.Close();
			}
		}

		#endregion

		#region 匯入
		/// <summary>
		/// 檢查匯入的資料.
		/// </summary>
		/// <param name="fileName">檔案商品名稱</param>
		/// <param name="ImportGoods">The import zip codes.</param>
		/// <returns></returns>
		public CheckResult CheckImportData(string fileName, List<ImportClass> ImportGoods, int CompanyId)
		{
			var result = new CheckResult();
			var targetFile = new FileInfo(fileName);
			bool IsLimitOrder = Method.Get_CompanyIsLimitOrder(CompanyId);
			if (!targetFile.Exists)
			{
				result.ID = Guid.NewGuid();
				result.Success = false;
				result.ErrorCount = 0;
				result.ErrorMessage = "匯入的檔案不存在";
				return result;
			}

			var excelFile = new ExcelQueryFactory(fileName);

			//欄位對映
			excelFile.AddMapping<ImportClass>(x => x.SN, "商品編號");
			excelFile.AddMapping<ImportClass>(x => x.Name, "商品名稱");
			excelFile.AddMapping<ImportClass>(x => x.Content, "商品說明");
			excelFile.AddMapping<ImportClass>(x => x.Price, "售價");
			excelFile.AddMapping<ImportClass>(x => x.SpeicalOffer, "特價");
			excelFile.AddMapping<ImportClass>(x => x.Type, "類別");
			excelFile.AddMapping<ImportClass>(x => x.FewDaysShipping, "幾天出貨");
			//集單廠商
			if (IsLimitOrder == true)
			{
				excelFile.AddMapping<ImportClass>(x => x.LimitOrderStart, "集單開始日期");
				excelFile.AddMapping<ImportClass>(x => x.LimitOrderEnd, "集單結束日期");
			}
			excelFile.AddMapping<ImportClass>(x => x.BrandName, "品牌");

			excelFile.AddMapping<ImportClass>(x => x.Color, "顏色");
			excelFile.AddMapping<ImportClass>(x => x.Season, "季節");
			excelFile.AddMapping<ImportClass>(x => x.Style, "風格");
			excelFile.AddMapping<ImportClass>(x => x.Fit, "適合");
			excelFile.AddMapping<ImportClass>(x => x.SizeType, "尺寸類別");
			//集單廠商
			if (IsLimitOrder == true)
			{
				excelFile.AddMapping<ImportClass>(x => x.F, "庫存/集單量(F)");
				excelFile.AddMapping<ImportClass>(x => x.XS, "庫存/集單量(XS)");
				excelFile.AddMapping<ImportClass>(x => x.S, "庫存/集單量(S)");
				excelFile.AddMapping<ImportClass>(x => x.M, "庫存/集單量(M)");
				excelFile.AddMapping<ImportClass>(x => x.L, "庫存/集單量(L)");
				excelFile.AddMapping<ImportClass>(x => x.XL, "庫存/集單量(XL)");
				excelFile.AddMapping<ImportClass>(x => x.XXL, "庫存/集單量(XXL)");
			}
			//非集單廠商
			else
			{
				excelFile.AddMapping<ImportClass>(x => x.F, "庫存(F)");
				excelFile.AddMapping<ImportClass>(x => x.XS, "庫存(XS)");
				excelFile.AddMapping<ImportClass>(x => x.S, "庫存(S)");
				excelFile.AddMapping<ImportClass>(x => x.M, "庫存(M)");
				excelFile.AddMapping<ImportClass>(x => x.L, "庫存(L)");
				excelFile.AddMapping<ImportClass>(x => x.XL, "庫存(XL)");
				excelFile.AddMapping<ImportClass>(x => x.XXL, "庫存(XXL)");
			}

			//工作表商品名稱
			var excelContent = excelFile.Worksheet<ImportClass>("匯入商品資料");
			int errorCount = 0;
			int rowIndex = 1;
			var importErrorMessages = new List<string>();

			int count = excelContent.Count();
			//載入Excel資料
			try
			{
				foreach (var row in excelContent)
				{
					if (!string.IsNullOrWhiteSpace(row.SN) && !string.IsNullOrWhiteSpace(row.Name) && !string.IsNullOrWhiteSpace(row.Type) && !string.IsNullOrWhiteSpace(row.SizeType))
					{
						var errorMessage = new StringBuilder();
						var Import = new ImportClass();
						//欄位為空白
						if (string.IsNullOrWhiteSpace(row.SN))
						{
							errorMessage.Append("【商品編號】不能為空白、");
						}
						else if (string.IsNullOrWhiteSpace(row.Name))
						{
							errorMessage.Append("【商品名稱】不能為空白、");
						}
						else if (string.IsNullOrWhiteSpace(row.Price.ToString()))
						{
							errorMessage.Append("【售價】不能為空白、");
						}
						else if (string.IsNullOrWhiteSpace(row.Type))
						{
							errorMessage.Append("【類別】不能為空白、");
						}
						else if (string.IsNullOrWhiteSpace(row.Color))
						{
							errorMessage.Append("【顏色】不能為空白、");
						}
						else if (string.IsNullOrWhiteSpace(row.Season))
						{
							errorMessage.Append("【季節】不能為空白、");
						}
						else if (string.IsNullOrWhiteSpace(row.Style))
						{
							errorMessage.Append("【風格】不能為空白、");
						}
						else if (string.IsNullOrWhiteSpace(row.Fit))
						{
							errorMessage.Append("【適合】不能為空白、");
						}
						else if (string.IsNullOrWhiteSpace(row.SizeType))
						{
							errorMessage.Append("【尺寸類別】不能為空白、");
						}
						else
						{
							MithDataContext db = new MithDataContext();
							row.SN = Import_Replace(row.SN);
							row.Content = row.Content.Replace("\n", "").Replace("\t", "");
							row.Type = Import_Replace(row.Type);
							row.Color = Import_Replace(row.Color);
							row.Season = Import_Replace(row.Season);
							row.Style = Import_Replace(row.Style);
							row.Fit = Import_Replace(row.Fit);
							row.SizeType = Import_Replace(row.SizeType);

							//驗證數字
							System.Text.RegularExpressions.Regex reg_num = new System.Text.RegularExpressions.Regex(@"^[0-9]+$");
							bool Check_Number = true;

							#region  檢查序號
							int Check_GoodsSN = 0;
							int Check_Goods = 0;

							Check_GoodsSN = db.GoodsSN_Setting.Where(w => w.SN == row.SN).Count();
							//大於0代表商品編號輸入正確
							if (Check_GoodsSN > 0)
							{
								Check_Goods = db.Goods.Where(w => w.SN == row.SN).Count();
								//大於0代表此商品編號重複
								if (Check_Goods > 0)
								{
									errorMessage.Append("【商品編號】" + row.SN + " 已經被使用、");
								}
							}
							else
							{
								errorMessage.Append("【商品編號】" + row.SN + " 輸入錯誤、");
							}
							#endregion

							#region 檢查價格相關

							Check_Number = reg_num.IsMatch(row.Price.ToString());
							if (Check_Number == false)
							{
								errorMessage.Append("【售價】請輸入數字、");
							}
							//特價不為Null
							if (row.SpeicalOffer != null)
							{
								Check_Number = reg_num.IsMatch(row.SpeicalOffer.ToString());
								if (Check_Number == false)
								{
									errorMessage.Append("【特價】請輸入數字、");
								}

								if (row.SpeicalOffer > row.Price)
								{
									errorMessage.Append("【特價】不能大於【售價】、");
								}
							}
							db.Connection.Close();

							#endregion

							#region 檢查類別相關
							//取得類別Id
							row.TypeId = int.Parse(Check_Category("Type", row.Type));
							// -1代表抓不到Id
							if (row.TypeId == -1)
							{
								errorMessage.Append("【類別】" + row.Type + " 輸入錯誤、");
							}
							else
							{
								//類別=輸入訂製款-集量生產，廠商身分卻不為訂製款-集量生產
								if (row.TypeId == 3 && IsLimitOrder == false)
								{
									errorMessage.Append("您的【合作類別】無法選擇【訂製款-集量生產】、");
								}

								// 類別 = 現貨
								if (row.TypeId == 1)
								{
									//檢查尺寸類別
									if (string.IsNullOrWhiteSpace(row.SizeType))
									{
										errorMessage.Append("【類別】輸入現貨，【尺寸類別】不能為空白、");
									}
									else
									{
										if (row.SizeType == "單")
										{
											row.SizeType = "One";
										}
										else if (row.SizeType == "多")
										{
											row.SizeType = "Many";
										}
										else
										{
											errorMessage.Append("【尺寸類別】" + row.SizeType + " 輸入錯誤、");
										}
									}
									row.FewDaysShipping = null;
									row.LimitOrderStart = null;
									row.LimitOrderEnd = null;

								}
								// 類別 = 預訂(製)貨
								else if (row.TypeId == 2)
								{
									//檢查幾天出貨
									if (string.IsNullOrWhiteSpace(row.FewDaysShipping))
									{
										errorMessage.Append("【類別】輸入預訂(製)貨，【幾天出貨】不能為空白、");
									}
									else
									{
										Check_Number = reg_num.IsMatch(row.FewDaysShipping.ToString());
										if (Check_Number == false)
										{
											errorMessage.Append("【幾天出貨】請輸入數字、");
										}
									}
									//檢查尺寸類別
									if (string.IsNullOrWhiteSpace(row.SizeType))
									{
										errorMessage.Append("【類別】輸入預訂(製)貨，【尺寸類別】不能為空白、");
									}
									else
									{
										if (row.SizeType == "單")
										{
											row.SizeType = "One";
										}
										else if (row.SizeType == "多")
										{
											row.SizeType = "Many";
										}
										else
										{
											errorMessage.Append("【尺寸類別】" + row.SizeType + " 輸入錯誤、");
										}
									}
									row.LimitOrderStart = null;
									row.LimitOrderEnd = null;
								}
								// 類別 = 訂製款-集量生產
								else if (row.TypeId == 3 && IsLimitOrder == true)
								{
									//檢查集單日期
									if (string.IsNullOrWhiteSpace(row.LimitOrderStart.Value.ToString()) || string.IsNullOrWhiteSpace(row.LimitOrderEnd.Value.ToString()))
									{
										errorMessage.Append("【類別】輸入訂製款-集量生產，【集單日期】不能為空白、");
									}
									else
									{
										//驗證日期
										System.Text.RegularExpressions.Regex reg_date = new System.Text.RegularExpressions.Regex("^([0-9]{4})[./]{1}([0-9]{1,2})[./]{1}([0-9]{1,2})$");
										bool Check_DATE = true;

										//檢查集單開始日期
										Check_DATE = CheckDateType(row.LimitOrderStart.Value.ToString("yyyy/MM/dd"));
										if (Check_DATE == false)
										{
											errorMessage.Append("【集單開始日期】" + row.LimitOrderStart + " 輸入錯誤、");
										}

										//檢查集單結束日期
										Check_DATE = CheckDateType(row.LimitOrderEnd.Value.ToString("yyyy/MM/dd"));
										if (Check_DATE == false)
										{
											errorMessage.Append("【集單結束日期】" + row.LimitOrderEnd + " 輸入錯誤、");
										}
									}

									//檢查幾天出貨
									if (string.IsNullOrWhiteSpace(row.FewDaysShipping))
									{
										errorMessage.Append("【類別】輸入訂製款-集量生產，【幾天出貨】不能為空白、");
									}
									else
									{
										Check_Number = reg_num.IsMatch(row.FewDaysShipping.ToString());
										if (Check_Number == false)
										{
											errorMessage.Append("【幾天出貨】請輸入數字、");
										}
									}

									//檢查尺寸類別
									if (string.IsNullOrWhiteSpace(row.SizeType))
									{
										errorMessage.Append("【類別】輸入訂製款-集量生產，【尺寸類別】不能為空白、");
									}
									else
									{
										if (row.SizeType == "單")
										{
											row.SizeType = "One";
										}
										else if (row.SizeType == "多")
										{
											row.SizeType = "Many";
										}
										else
										{
											errorMessage.Append("【尺寸類別】" + row.SizeType + " 輸入錯誤、");
										}
									}
								}
								// 類別 = 特別訂購款
								else
								{
									row.FewDaysShipping = null;
									row.LimitOrderStart = null;
									row.LimitOrderEnd = null;
									row.SizeType = null;
									row.F = 0;
									row.XS = 0;
									row.S = 0;
									row.M = 0;
									row.L = 0;
									row.XL = 0;
									row.XXL = 0;
								}
							}
							#endregion

							#region 檢查 品牌、顏色、季節、風格、適合

							row.BrandId = db.Brand.FirstOrDefault(w => w.Name.Equals(row.BrandName) && w.CompanyId == CompanyId) != null ? db.Brand.FirstOrDefault(w => w.Name.Equals(row.BrandName) && w.CompanyId == CompanyId).Id : -1;
							//-1代表品牌輸入錯誤
							if (row.BrandId == -1)
							{
								errorMessage.Append("【品牌】" + row.BrandName + " 輸入錯誤、");
							}

							//取得顏色Id
							row.ColorId = int.Parse(Check_Category("Color", row.Color));
							// -1代表抓不到Id
							if (row.ColorId == -1)
							{
								errorMessage.Append("【顏色】" + row.Color + " 輸入錯誤、");
							}

							//取得顏色Id
							row.SeasonId = int.Parse(Check_Category("Season", row.Season));
							// -1代表抓不到Id
							if (row.SeasonId == -1)
							{
								errorMessage.Append("【季節】" + row.Season + " 輸入錯誤、");
							}

							row.Style = Check_Style(row.Style);
							if (row.Style == "error")
							{
								errorMessage.Append("【風格】" + row.Style + " 輸入錯誤、");
							}

							row.Fit = Check_Fit(row.Fit);
							if (row.Fit == "error")
							{
								errorMessage.Append("【適合】" + row.Fit + " 輸入錯誤、");
							}

							#endregion

							db.Connection.Close();
						}
						Import.SN = row.SN;
						Import.Name = row.Name;
						Import.Content = row.Content;
						Import.Price = row.Price;
						Import.SpeicalOffer = row.SpeicalOffer;
						Import.TypeId = row.TypeId;
						Import.LimitOrderStart = row.LimitOrderStart;
						Import.LimitOrderEnd = row.LimitOrderEnd;
						Import.FewDaysShipping = row.FewDaysShipping;
						Import.SeasonId = row.SeasonId;
						Import.ColorId = row.ColorId;
						Import.Style = row.Style;
						Import.Fit = row.Fit;
						Import.SizeType = row.SizeType;
						Import.BrandId = row.BrandId;
						Import.F = row.F;
						Import.XS = row.XS;
						Import.S = row.S;
						Import.M = row.M;
						Import.L = row.L;
						Import.XL = row.XL;
						Import.XXL = row.XXL;

						if (errorMessage.Length > 0)
						{
							int rowIndex2 = rowIndex + 1;
							errorCount += 1;
							importErrorMessages.Add(string.Format(
									"第 {0} 列：<br/>{1}{2}",
									rowIndex2,
									errorMessage,
									"<br/>"));
						}
						ImportGoods.Add(Import);
						rowIndex += 1;
					}
				}
				//}
				//else
				//{
				//    errorCount += 1;
				//    importErrorMessages.Add(string.Format("請勿超過50筆資料"));
				//}

				result.ID = Guid.NewGuid();
				result.Success = errorCount.Equals(0);
				result.RowCount = ImportGoods.Count;
				result.ErrorCount = errorCount;

				string allErrorMessage = string.Empty;

				foreach (var message in importErrorMessages)
				{
					allErrorMessage += message;
				}

				result.ErrorMessage = allErrorMessage;

				return result;
			}
			catch (Exception ex)
			{
				throw;
			}
		}

		/// <summary>
		/// 資料匯入DB.
		/// </summary>
		/// <param name="fileName">檔案商品名稱</param>
		/// <param name="ImportGoods">The import zip codes.</param>
		public void InsertImportData(string fileName = "", IEnumerable<ImportClass> ImportGoods = null)
		{
			MithDataContext db = new MithDataContext();
			try
			{
				foreach (var item in ImportGoods)
				{
					//判斷商品編號是否存在
					var data = db.Goods.FirstOrDefault(f => f.SN == item.SN);
					if (data == null)
					{
						item.Id = db.Goods.OrderByDescending(o => o.Id).FirstOrDefault() != null ? db.Goods.OrderByDescending(o => o.Id).FirstOrDefault().Id + 1 : 1;

						if (item.LimitOrderStart != null)
						{
							item.LimitOrderStart = DateTime.Parse(item.LimitOrderStart.Value.ToString("yyyy/MM/dd") + " 00:00:00");
						}

						if (item.LimitOrderEnd != null)
						{
							item.LimitOrderEnd = DateTime.Parse(item.LimitOrderEnd.Value.ToString("yyyy/MM/dd") + " 23:59:50");
						}

						int? FewDaysShipping = null;

						if (item.FewDaysShipping != null)
						{
							FewDaysShipping = int.Parse(item.FewDaysShipping);
						}

						item.Content = item.Content.Replace("@@", Environment.NewLine);
						Goods new_item = new Goods
						{
							Id = item.Id,
							SN = item.SN,
							Name = item.Name,
							Status = 1,
							Price = item.Price,
							SpecialOffer = item.SpeicalOffer,
							Type = item.TypeId,
							LimitOrderStart = item.LimitOrderStart,
							LimitOrderEnd = item.LimitOrderEnd,
							FewDaysShipping = FewDaysShipping,
							Content = item.Content,
							Pattern = item.SN.Substring(13, 1),
							Season = item.SeasonId,
							Color = item.ColorId,
							Style = item.Style,
							Fit = item.Fit,
							NoticeCount = 0,
							Sort = 0,
							SizeType = item.SizeType,
							CreateTime = DateTime.UtcNow,
							UpdateTime = DateTime.UtcNow,
							LastUserId = 0,
							CompanyId = db.Brand.FirstOrDefault(f => f.Id == item.BrandId).CompanyId,
							BrandId = item.BrandId,
						};
						db.Goods.InsertOnSubmit(new_item);
						db.SubmitChanges();
						db.Connection.Close();

						#region 新增尺寸
						//選擇多尺寸
						if (item.SizeType == "Many")
						{
							if (item.XS > 0)
							{
								Insert_SizeQuantity(item.Id, "XS", item.XS);
							}
							if (item.S > 0)
							{
								Insert_SizeQuantity(item.Id, "S", item.S);
							}
							if (item.M > 0)
							{
								Insert_SizeQuantity(item.Id, "M", item.M);
							}
							if (item.L > 0)
							{
								Insert_SizeQuantity(item.Id, "L", item.L);
							}
							if (item.XL > 0)
							{
								Insert_SizeQuantity(item.Id, "XL", item.XL);
							}
							if (item.XXL > 0)
							{
								Insert_SizeQuantity(item.Id, "XXL", item.XXL);
							}
						}
						//選擇單尺寸
						else
						{
							if (item.F >= 0)
							{
								Insert_SizeQuantity(item.Id, "F", item.F);
							}
						}
						#endregion
					}
				}

			}
			catch
			{
				throw;
			}
			//一定會刪除此檔案
			finally
			{
				if (File.Exists(fileName))
				{
					File.Delete(fileName);
				}
				db.Connection.Close();
			}
		}

		#endregion

		#region 驗證匯入資料

		/// <summary>
		/// 匯入欄位字元處理
		/// </summary>
		/// <param name="Input">欄位</param>
		/// <returns>True or False</returns>
		public string Import_Replace(string Input)
		{
			Input = Input.Replace(" ", "").Replace("　", "").Replace("\r", "").Replace("\n", "").Replace("\t", "");
			return Input;
		}

		/// <summary>
		/// 判斷商品編號格式
		/// </summary>
		/// <param name="SN">商品編號</param>
		/// <returns>True or False</returns>
		public bool CheckSnType(string SN)
		{
			//序號前13位
			string SN_13 = SN.Substring(0, 13);
			//序號第11位
			string SN_14 = SN.Substring(13, 1);
			///序號第15位
			string SN_15 = SN.Substring(14, 1);

			//驗證數字
			System.Text.RegularExpressions.Regex reg_num = new System.Text.RegularExpressions.Regex(@"^[0-9]+$");
			//驗證為T、O、D、S、P
			System.Text.RegularExpressions.Regex reg2 = new System.Text.RegularExpressions.Regex(@"^[TCDSP]+$");

			bool Check_SN_13 = reg_num.IsMatch(SN_13);
			bool Check_SN_14 = reg2.IsMatch(SN_14);
			bool Check_SN_15 = reg_num.IsMatch(SN_15);

			//其中有一個為False，代表格式錯誤
			if (Check_SN_13 == false || Check_SN_14 == false || Check_SN_15 == false)
			{
				return false;
			}
			else
			{
				return true;
			}
		}


		/// <summary>
		/// 判斷集單日期格式
		/// </summary>
		/// <param name="LimitOrderDate">集單日期</param>
		/// <returns>True or False</returns>
		public bool CheckDateType(string LimitOrderDate)
		{
			LimitOrderDate = LimitOrderDate.Replace(" ", "").Replace("／", "/");
			//驗證日期
			System.Text.RegularExpressions.Regex reg_date = new System.Text.RegularExpressions.Regex("^([0-9]{4})[./]{1}([0-9]{1,2})[./]{1}([0-9]{1,2})$");
			bool Check_Date = true;

			Check_Date = reg_date.IsMatch(LimitOrderDate);
			//日期格式正確
			if (Check_Date == true)
			{
				var item = LimitOrderDate.Split("/");
				int year = int.Parse(item[0]);
				int month = int.Parse(item[1]);
				int date = int.Parse(item[2]);
				//年份錯誤
				if (year <= 0 || year > 9999)
				{
					return false;
				}
				//月份錯誤
				if (month <= 0 || month > 12)
				{
					return false;
				}
				//日期錯誤
				if (date <= 0 || date > 31)
				{
					return false;
				}
				return true;
			}
			//日期格式錯誤
			else
			{
				return false;
			}

		}

		/// <summary>
		/// 判斷匯入資料是否存在
		/// </summary>
		/// <param name="Group">群組</param>
		/// <param name="Name">匯入資料</param>
		/// <returns>True or False</returns>
		public static string Check_Category(string Group, string Name)
		{
			MithDataContext db = new MithDataContext();
			if (Group == "Pattern")
			{
				string Remark = db.Category.FirstOrDefault(w => w.Fun_Id == 8 && w.Group == Group && w.Name == Name) != null ? db.Category.FirstOrDefault(w => w.Fun_Id == 8 && w.Group == Group && w.Name == Name).Remark.ToString() : "";
				db.Connection.Close();
				return Remark;
			}
			else
			{
				string Id = db.Category.FirstOrDefault(w => w.Fun_Id == 8 && w.Group == Group && w.Name == Name) != null ? db.Category.FirstOrDefault(w => w.Fun_Id == 8 && w.Group == Group && w.Name == Name).Number.ToString() : "-1";
				db.Connection.Close();
				return Id;
			}

		}

		/// <summary>
		/// 判斷匯入資料是否存在
		/// </summary>
		/// <param name="Style">匯入資料</param>
		/// <returns>True or False</returns>
		private string Check_Style(string Style)
		{

			MithDataContext db = new MithDataContext();
			try
			{
				var item = Style.Split(",");
				string str = "";
				if (item.Count() == 1)
				{
					str += db.Category.FirstOrDefault(w => w.Name == item[0] && w.Group == "Style" && w.Fun_Id == 8).Number + ",";
					return str.TrimEnd(',');
				}
				else if (item.Count() == 2)
				{
					for (int i = 0; i < item.Count(); i++)
					{
						str += db.Category.FirstOrDefault(w => w.Name == item[i] && w.Group == "Style" && w.Fun_Id == 8).Number + ",";
					}
					return str.TrimEnd(',');
				}
				else
				{
					return "error";
				}


			}
			catch
			{
				return "error";
			}
			finally { db.Connection.Close(); }
		}

		/// <summary>
		/// 判斷匯入資料是否存在
		/// </summary>
		/// <param name="Style">匯入資料</param>
		/// <returns>True or False</returns>
		private string Check_Fit(string Fit)
		{

			MithDataContext db = new MithDataContext();
			try
			{
				var item = Fit.Split(",");
				string str = "";
				if (item.Count() == 1)
				{
					str += db.Category.FirstOrDefault(w => w.Name == item[0] && w.Group == "Fit" && w.Fun_Id == 8).Number + ",";
					return str.TrimEnd(',');
				}
				else if (item.Count() == 2)
				{
					for (int i = 0; i < item.Count(); i++)
					{
						str += db.Category.FirstOrDefault(w => w.Name == item[i] && w.Group == "Fit" && w.Fun_Id == 8).Number + ",";
					}
					return str.TrimEnd(',');
				}
				else
				{
					return "error";
				}


			}
			catch
			{
				return "error";
			}
			finally { db.Connection.Close(); }
		}
		#endregion

		#region 匯出 (亞設)
		public MemoryStream Set_Excel_Mith(List<Export> data, bool IsLimitOrder)
		{
			//標題
			List<string> header = new List<string>();
			header.Add("商品編號");
			header.Add("商品名稱");
			header.Add("類別");
			header.Add("售價");
			header.Add("特價");
			header.Add("款式");
			header.Add("顏色");
			header.Add("季節");
			header.Add("風格");
			header.Add("適合");
			header.Add("庫存量");
			header.Add("銷售量");
			header.Add("貨到通知");
			header.Add("穿搭次數");
			header.Add("廠商");
			header.Add("品牌");
			header.Add("建立時間");

			MemoryStream ms = new MemoryStream();
			HSSFWorkbook workbook = new HSSFWorkbook();
			HSSFSheet sheet = (HSSFSheet)workbook.CreateSheet("匯出商品資料");

			//宣告headStyle、內容style
			HSSFCellStyle headStyle = null;
			HSSFCellStyle contStyle = null;
			headStyle = (HSSFCellStyle)workbook.CreateCellStyle();
			contStyle = (HSSFCellStyle)workbook.CreateCellStyle();
			HSSFFont font = null;
			font = (HSSFFont)workbook.CreateFont();
			HSSFCell cell = null;

			//標題粗體、黃色背景
			headStyle.FillForegroundColor = HSSFColor.LightYellow.Index;
			headStyle.FillPattern = NPOI.SS.UserModel.FillPattern.SolidForeground;
			//標題字型樣式
			font.FontHeightInPoints = 10;
			font.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
			font.FontName = "Microsoft JhengHei";
			headStyle.SetFont(font);
			//內容字型樣式(自動換行)
			contStyle.WrapText = true;
			//contStyle.VerticalAlignment = NPOI.SS.UserModel.VerticalAlignment.Center;
			//宣告headRow
			sheet.CreateRow(0);
			//設定head背景、粗體、內容
			foreach (var i in header)
			{
				cell = (HSSFCell)sheet.GetRow(0).CreateCell(header.IndexOf(i));
				cell.SetCellValue(i);
				cell.CellStyle = headStyle;
			}
			//資料欄位
			for (int i = 0; i < data.Count; i++)
			{
				sheet.CreateRow(i + 1);
				sheet.GetRow(i + 1).CreateCell(0).SetCellValue(data[i].SN);
				sheet.GetRow(i + 1).CreateCell(1).SetCellValue(data[i].Name);
				sheet.GetRow(i + 1).CreateCell(2).SetCellValue(data[i].Type_Name);
				sheet.GetRow(i + 1).CreateCell(3).SetCellValue(data[i].Price);
				sheet.GetRow(i + 1).CreateCell(4).SetCellValue(data[i].SpecialOffer ?? 0);
				sheet.GetRow(i + 1).CreateCell(5).SetCellValue(data[i].Pattern_Name);
				sheet.GetRow(i + 1).CreateCell(6).SetCellValue(data[i].Color_Name);
				sheet.GetRow(i + 1).CreateCell(7).SetCellValue(data[i].Season_Name);
				sheet.GetRow(i + 1).CreateCell(8).SetCellValue(data[i].Style);
				sheet.GetRow(i + 1).CreateCell(9).SetCellValue(data[i].Fit);
				sheet.GetRow(i + 1).CreateCell(10).SetCellValue(data[i].Quantity_Total);
				sheet.GetRow(i + 1).CreateCell(11).SetCellValue(data[i].SellQuantity_Total);

				sheet.GetRow(i + 1).CreateCell(12).SetCellValue(data[i].NoticeCount);
				sheet.GetRow(i + 1).CreateCell(13).SetCellValue(data[i].WearCount);
				sheet.GetRow(i + 1).CreateCell(14).SetCellValue(data[i].Company_Name);
				sheet.GetRow(i + 1).CreateCell(15).SetCellValue(data[i].Brand_Name);
				sheet.GetRow(i + 1).CreateCell(16).SetCellValue(data[i].CreateTime);
			}
			foreach (var i in header)
			{
				sheet.SetColumnWidth(header.IndexOf(i), (short)256 * 12);
			}

			sheet.SetColumnWidth(header.IndexOf("商品編號"), (short)256 * 16);
			sheet.SetColumnWidth(header.IndexOf("商品名稱"), (short)256 * 20);
			sheet.SetColumnWidth(header.IndexOf("類別"), (short)256 * 10);
			sheet.SetColumnWidth(header.IndexOf("售價"), (short)256 * 8);
			sheet.SetColumnWidth(header.IndexOf("特價"), (short)256 * 8);
			sheet.SetColumnWidth(header.IndexOf("款式"), (short)256 * 8);
			sheet.SetColumnWidth(header.IndexOf("顏色"), (short)256 * 8);
			sheet.SetColumnWidth(header.IndexOf("季節"), (short)256 * 8);
			sheet.SetColumnWidth(header.IndexOf("風格"), (short)256 * 12);
			sheet.SetColumnWidth(header.IndexOf("適合"), (short)256 * 12);
			sheet.SetColumnWidth(header.IndexOf("庫存量"), (short)256 * 8);
			sheet.SetColumnWidth(header.IndexOf("銷售量"), (short)256 * 8);
			sheet.SetColumnWidth(header.IndexOf("貨到通知"), (short)256 * 8);
			sheet.SetColumnWidth(header.IndexOf("穿搭次數"), (short)256 * 8);
			sheet.SetColumnWidth(header.IndexOf("廠商"), (short)256 * 15);
			sheet.SetColumnWidth(header.IndexOf("品牌"), (short)256 * 15);
			sheet.SetColumnWidth(header.IndexOf("建立時間"), (short)256 * 15);


			workbook.Write(ms);
			ms.Position = 0;
			ms.Flush();
			return ms;
		}

		public List<Export> Export_Data_Mith()
		{

			/*-------------------匯出宣告-------------------*/
			MithDataContext db = new MithDataContext();
			Export export = new Export();
			List<Export> exp = new List<Export>();
			var data = Get_Sort(Get());
			/*-------------------匯出宣告End-------------------*/

			foreach (var i in data)
			{
				export.SN = i.SN;
				export.Name = i.Name;
				export.Type_Name = i.Type_Name;
				export.Price = i.Price;
				export.SpecialOffer = i.SpecialOffer ?? null;
				export.Pattern_Name = i.Pattern_Name;
				export.Color_Name = i.Color_Name;
				export.Season_Name = i.Season_Name;
				export.Style = Get_StyleName(i.Style);
				export.Fit = Get_FitName(i.Fit);
				export.Quantity_Total = i.Quantity_Total.Value;
				export.SellQuantity_Total = i.SellQuantity_Total.Value;
				export.NoticeCount = i.NoticeCount;
				export.WearCount = i.WearCount;
				export.CreateTime = i.CreateTime.Value.ToString("yyyy/MM/dd HH:mm");
				export.Company_Name = i.Company_Name;
				exp.Add(export);
				export = new Export();
			}
			db.Connection.Close();
			return exp;
		}

		#endregion

		#region 匯出 (廠商)
		public MemoryStream Set_Excel_Company(List<Export> data, bool IsLimitOrder)
		{
			//標題
			List<string> header = new List<string>();
			header.Add("商品編號");
			header.Add("商品名稱");
			header.Add("類別");
			header.Add("售價");
			header.Add("特價");
			header.Add("款式");
			header.Add("顏色");
			header.Add("季節");
			header.Add("風格");
			header.Add("適合");
			if (IsLimitOrder == true)
			{
				header.Add("庫存/集單量 (F)");
				header.Add("銷售 (F)");
				header.Add("庫存/集單量 (XS)");
				header.Add("銷售 (XS)");
				header.Add("庫存/集單量 (S)");
				header.Add("銷售 (S)");
				header.Add("庫存/集單量 (M)");
				header.Add("銷售 (M)");
				header.Add("庫存/集單量 (L)");
				header.Add("銷售 (L)");
				header.Add("庫存/集單量 (XL)");
				header.Add("銷售 (XL)");
				header.Add("庫存/集單量 (XXL)");
				header.Add("銷售 (XXL)");
			}
			else
			{
				header.Add("庫存 (F)");
				header.Add("銷售 (F)");
				header.Add("庫存 (XS)");
				header.Add("銷售 (XS)");
				header.Add("庫存 (S)");
				header.Add("銷售 (S)");
				header.Add("庫存 (M)");
				header.Add("銷售 (M)");
				header.Add("庫存 (L)");
				header.Add("銷售 (L)");
				header.Add("庫存 (XL)");
				header.Add("銷售 (XL)");
				header.Add("庫存 (XXL)");
				header.Add("銷售 (XXL)");
			}
			header.Add("貨到通知");
			header.Add("建立時間");
			header.Add("品牌");
			MemoryStream ms = new MemoryStream();
			HSSFWorkbook workbook = new HSSFWorkbook();
			HSSFSheet sheet = (HSSFSheet)workbook.CreateSheet("匯出商品資料");

			//宣告headStyle、內容style
			HSSFCellStyle headStyle = null;
			HSSFCellStyle contStyle = null;
			headStyle = (HSSFCellStyle)workbook.CreateCellStyle();
			contStyle = (HSSFCellStyle)workbook.CreateCellStyle();
			HSSFFont font = null;
			font = (HSSFFont)workbook.CreateFont();
			HSSFCell cell = null;

			//標題粗體、黃色背景
			headStyle.FillForegroundColor = HSSFColor.LightYellow.Index;
			headStyle.FillPattern = NPOI.SS.UserModel.FillPattern.SolidForeground;
			//標題字型樣式
			font.FontHeightInPoints = 10;
			font.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
			font.FontName = "Microsoft JhengHei";
			headStyle.SetFont(font);
			//內容字型樣式(自動換行)
			contStyle.WrapText = true;
			//contStyle.VerticalAlignment = NPOI.SS.UserModel.VerticalAlignment.Center;
			//宣告headRow
			sheet.CreateRow(0);
			//設定head背景、粗體、內容
			foreach (var i in header)
			{
				cell = (HSSFCell)sheet.GetRow(0).CreateCell(header.IndexOf(i));
				cell.SetCellValue(i);
				cell.CellStyle = headStyle;
			}
			//資料欄位
			for (int i = 0; i < data.Count; i++)
			{
				sheet.CreateRow(i + 1);
				sheet.GetRow(i + 1).CreateCell(0).SetCellValue(data[i].SN);
				sheet.GetRow(i + 1).CreateCell(1).SetCellValue(data[i].Name);
				sheet.GetRow(i + 1).CreateCell(2).SetCellValue(data[i].Type_Name);
				sheet.GetRow(i + 1).CreateCell(3).SetCellValue(data[i].Price);
				sheet.GetRow(i + 1).CreateCell(4).SetCellValue(data[i].SpecialOffer ?? 0);

				sheet.GetRow(i + 1).CreateCell(5).SetCellValue(data[i].Pattern_Name);
				sheet.GetRow(i + 1).CreateCell(6).SetCellValue(data[i].Color_Name);
				sheet.GetRow(i + 1).CreateCell(7).SetCellValue(data[i].Season_Name);
				sheet.GetRow(i + 1).CreateCell(8).SetCellValue(data[i].Style);
				sheet.GetRow(i + 1).CreateCell(9).SetCellValue(data[i].Fit);

				sheet.GetRow(i + 1).CreateCell(10).SetCellValue(data[i].F_Quantity ?? 0);
				sheet.GetRow(i + 1).CreateCell(11).SetCellValue(data[i].F_SellQuantity ?? 0);
				sheet.GetRow(i + 1).CreateCell(12).SetCellValue(data[i].XS_Quantity ?? 0);
				sheet.GetRow(i + 1).CreateCell(13).SetCellValue(data[i].XS_SellQuantity ?? 0);
				sheet.GetRow(i + 1).CreateCell(14).SetCellValue(data[i].S_Quantity ?? 0);
				sheet.GetRow(i + 1).CreateCell(15).SetCellValue(data[i].S_SellQuantity ?? 0);
				sheet.GetRow(i + 1).CreateCell(16).SetCellValue(data[i].M_Quantity ?? 0);
				sheet.GetRow(i + 1).CreateCell(17).SetCellValue(data[i].M_SellQuantity ?? 0);
				sheet.GetRow(i + 1).CreateCell(18).SetCellValue(data[i].L_Quantity ?? 0);
				sheet.GetRow(i + 1).CreateCell(19).SetCellValue(data[i].L_SellQuantity ?? 0);
				sheet.GetRow(i + 1).CreateCell(20).SetCellValue(data[i].XL_Quantity ?? 0);
				sheet.GetRow(i + 1).CreateCell(21).SetCellValue(data[i].XL_SellQuantity ?? 0);
				sheet.GetRow(i + 1).CreateCell(22).SetCellValue(data[i].XXL_Quantity ?? 0);
				sheet.GetRow(i + 1).CreateCell(23).SetCellValue(data[i].XXL_SellQuantity ?? 0);

				sheet.GetRow(i + 1).CreateCell(24).SetCellValue(data[i].NoticeCount);
				sheet.GetRow(i + 1).CreateCell(25).SetCellValue(data[i].CreateTime);
				sheet.GetRow(i + 1).CreateCell(26).SetCellValue(data[i].Brand_Name);
			}
			foreach (var i in header)
			{
				if (i == "商品編號")
				{
					sheet.SetColumnWidth(header.IndexOf(i), (short)256 * 16);

				}
				else if (i == "商品名稱")
				{
					sheet.SetColumnWidth(header.IndexOf(i), (short)256 * 20);

				}
				else
				{
					sheet.SetColumnWidth(header.IndexOf(i), (short)256 * 12);
				}
			}
			workbook.Write(ms);
			ms.Position = 0;
			ms.Flush();
			return ms;
		}

		public List<Export> Export_Data_Company()
		{

			/*-------------------匯出宣告-------------------*/
			MithDataContext db = new MithDataContext();
			Export export = new Export();
			List<Export> exp = new List<Export>();
			var data = Get_Sort(Get());

			/*-------------------匯出宣告End-------------------*/

			foreach (var i in data)
			{
				export.SN = i.SN;
				export.Name = i.Name;
				export.Type_Name = i.Type_Name;
				export.Pattern_Name = i.Pattern_Name;
				export.Color_Name = i.Color_Name;
				export.Season_Name = i.Season_Name;
				export.Style = Get_StyleName(i.Style);
				export.Fit = Get_FitName(i.Fit);
				export.Price = i.Price;
				export.SpecialOffer = i.SpecialOffer ?? null;

				export.F_Quantity = i.F_Quantity ?? 0;
				export.F_SellQuantity = i.F_SellQuantity ?? 0;
				export.XS_Quantity = i.XS_Quantity ?? 0;
				export.XS_SellQuantity = i.XS_SellQuantity ?? 0;
				export.S_Quantity = i.S_Quantity ?? 0;
				export.S_SellQuantity = i.S_SellQuantity ?? 0;
				export.M_Quantity = i.M_Quantity ?? 0;
				export.M_SellQuantity = i.M_SellQuantity ?? 0;
				export.L_Quantity = i.L_Quantity ?? 0;
				export.L_SellQuantity = i.L_SellQuantity ?? 0;
				export.XL_Quantity = i.XL_Quantity ?? 0;
				export.XL_SellQuantity = i.XL_SellQuantity ?? 0;
				export.XXL_Quantity = i.XXL_Quantity ?? 0;
				export.XXL_SellQuantity = i.XXL_SellQuantity ?? 0;

				export.NoticeCount = i.NoticeCount;
				export.WearCount = i.WearCount;
				export.CreateTime = i.CreateTime.Value.ToString("yyyy/MM/dd HH:mm");
				export.Company_Name = i.Company_Name;
				exp.Add(export);
				export = new Export();
			}
			db.Connection.Close();
			return exp;
		}

		#endregion

		#region 下拉式選單

		/// <summary>
		/// 取得品牌下拉選單
		/// </summary>
		/// <param name="companyId">廠商Id</param>
		/// <returns>選單</returns>
		public static List<SelectListItem> Get_Brand_SelectListItem(int companyId, int brandId)
		{
			MithDataContext db = new MithDataContext();
			List<SelectListItem> data = new List<SelectListItem>();
			var Brand = db.Brand.Where(w => w.CompanyId == companyId);
			if (Brand.Count() > 1)
			{
				data.Add(new SelectListItem
				{
					Selected = brandId == 0,
					Text = "請選擇或輸入文字",
					Value = "0"
				});
			}
			foreach (var item in Brand)
			{
				data.AddRange(new SelectListItem
				{
					Selected = brandId == item.Id,
					Text = item.Name,
					Value = item.Id.ToString()
				});
			}
			db.Connection.Close();
			return data;
		}

		#endregion

	}
}