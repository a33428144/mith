﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using System.Text;
using System.Data.Linq.SqlClient;
using System.IO;
using System.Drawing;
using Mith.Areas.GoX.Models;
using Mith.Models;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using LinqToExcel;


namespace Mith.Areas.GoX.Models
{
    public class LimitOrderModel
    {
        #region Class
        public class LimitOrderShow
        {
            [Key]
            [DisplayName("編號")]
            public int Id { get; set; }
            [DisplayName("商品編號")]
            public string SN { get; set; }
            [DisplayName("商品名稱")]
            public string Name { get; set; }
            [DisplayName("售價")]
            public int Price { get; set; }
            [DisplayName("特價")]
            public int? SpecialOffer { get; set; }
            [DisplayName("商品狀態")]
            public int Status { get; set; }
            [DisplayName("商品狀態")]
            public string Status_Name { get; set; }
            [DisplayName("集單日期")]
            public DateTime? LimitOrderStart { get; set; }
            [DisplayName("集單日期")]
            public DateTime? LimitOrderEnd { get; set; }
            [DisplayName("出貨期限")]
            public DateTime? ShippingDeadline { get; set; }
            [DisplayName("商品照")]
            public string Pic_Middle { get; set; }
            public string Pic_Small { get; set; }
            [DisplayName("尺寸")]
            public string Size { get; set; }
            [DisplayName("集單量")]
            public int Quantity { get; set; }
            [DisplayName("銷售量")]
            public int LimitOrderQuantity { get; set; }
            [DisplayName("集單狀態")]
            public string LimitStatus { get; set; }
            [DisplayName("建立時間")]
            public DateTime? CreateTime { get; set; }
            [DisplayName("公司Id")]
            public int CompanyId { get; set; }
            [DisplayName("公司Id")]
            public string Company_Name { get; set; }

        }

        public class LimitOrderShowEdit
        {
            [Key]
            [DisplayName("編號")]
            public int Id { get; set; }
            [DisplayName("商品編號")]
            [Required(ErrorMessage = "必填欄位")]
            public string SN { get; set; }
            [DisplayName("商品名稱")]
            [Required(ErrorMessage = "必填欄位")]
            public string Name { get; set; }
            [DisplayName("商品照片")]
            public string Files { get; set; }
            [DisplayName("售價")]
            [Range(1, 9999999, ErrorMessage = "需大於 0")]
            [Required(ErrorMessage = "必填欄位")]
            [DataAnnotationsExtensions.Integer(ErrorMessage = "請輸入數字")]
            public int Price { get; set; }
            [DisplayName("特價")]
            [Range(1, 9999999, ErrorMessage = "需大於 0")]
            [DataAnnotationsExtensions.Integer(ErrorMessage = "請輸入數字")]
            public int? SpecialOffer { get; set; }
            [DisplayName("類別")]
            public int Type { get; set; }
            [DisplayName("類別")]
            public string Type_Name { get; set; }
            [DisplayName("狀態")]
            public int Status { get; set; }
            [DisplayName("狀態")]
            public string Status_Name { get; set; }
            [DisplayName("集單日期")]
            [DataType(DataType.Date)]
            public DateTime? LimitOrderStart { get; set; }
            [DisplayName("集單日期")]
            [DataType(DataType.Date)]
            [Required(ErrorMessage = "必填欄位")]
            public DateTime? LimitOrderEnd { get; set; }
            [DisplayName("幾天出貨")]
            [Range(1, 99999999999, ErrorMessage = "需大於 0")]
            [DataAnnotationsExtensions.Integer(ErrorMessage = "請輸入數字")]
            [Required(ErrorMessage = "必填欄位")]
            public int? FewDaysShipping { get; set; }
            [DisplayName("商品說明")]
            [DataType(DataType.MultilineText)]
            public string Content { get; set; }
            [DisplayName("款式")]
            public string Pattern { get; set; }
            [DisplayName("季節")]
            public int Season { get; set; }
            [DisplayName("顏色")]
            public int Color { get; set; }

            [DisplayName("風格 (最多兩項)")]
            public string Style { get; set; }
            [DisplayName("適合 (最多兩項)")]
            public string Fit { get; set; }
            [DisplayName("貨到通知")]
            public int NoticeCount { get; set; }
            [DisplayName("商品照片")]
            public string Pic_Middle { get; set; }
            public string Pic_Small { get; set; }
            [DisplayName("建立時間")]
            public DateTime? CreateTime { get; set; }
            [DisplayName("更新時間")]
            public DateTime? UpdateTime { get; set; }
            [DisplayName("更新人員")]
            public int LastUserId { get; set; }
            [DisplayName("更新人員")]
            public string LastUser_Name { get; set; }
            [DisplayName("公司")]
            public int CompanyId { get; set; }
            [DisplayName("公司")]
            public string Company_Name { get; set; }
        }

        #endregion

        public string Keyword = "";
        public int Search_Status = 0;
        public string Search_LimitStatus = "";
        public int Search_CompanyId = 0;
        public int Search_GoodsId = 0;
        public int Search_CashFlow = 0;
        public DateTime? LimitOrder_St = null;
        public DateTime? LimitOrder_End = null;

        public LimitOrderModel()
        {
        }
        public void Clear()
        {
            Keyword = "";
            Search_Status = 0;
            Search_LimitStatus = "";
            Search_CompanyId = 0;
            Search_GoodsId = 0;
            LimitOrder_St = null;
            LimitOrder_End = null;
        }

        #region Get

        #region Level1
        private IQueryable<LimitOrder_Index_View> Get_Level1()
        {
            MithDataContext db = new MithDataContext();
            IQueryable<LimitOrder_Index_View> data = db.LimitOrder_Index_View;

            if (Keyword != "")
            {
                data = data.Where(Query(Keyword));
            }

            if (Search_Status != 0)
            {
                data = data.Where(w => w.Status == Search_Status);
            }

            if (Search_LimitStatus != "")
            {
                data = data.Where(w => w.LimitStatus == Search_LimitStatus);
            }

            if (Search_CompanyId != 0)
            {
                data = data.Where(w => w.CompanyId == Search_CompanyId);
            }
            if (LimitOrder_St != null)
            {
                data = data.Where(w => w.LimitOrderStart >= LimitOrder_St.Value);
            }
            if (LimitOrder_End != null)
            {
                LimitOrder_End = DateTime.Parse(LimitOrder_End.Value.ToString("yyyy/MM/dd") + " 23:59:00");
                data = data.Where(w => w.LimitOrderEnd <= LimitOrder_End);
            }

            db.Connection.Close();
            return data;
        }

        public List<LimitOrderShow> Get_Data_Level1(int p = 1, int take = 10)
        {
            MithDataContext db = new MithDataContext();

            var data = Get_Level1().OrderByDescending(o => o.Id).Skip((p - 1) * take).Take(take);

            List<LimitOrderShow> item = new List<LimitOrderShow>();
            item = (from i in data
                    select new LimitOrderShow
                    {
                        Id = i.Id,
                        SN = i.SN,
                        Name = i.Name.Length > 18 ? i.Name.Substring(0, 18) : i.Name,
                        Pic_Middle = Get_Pic(i.Id, "Middle"),
                        Pic_Small = Get_Pic(i.Id, "Small"),
                        Price = i.Price,
                        SpecialOffer = i.SpecialOffer,
                        Status_Name = i.Status_Name,
                        Size = i.Size,
                        Quantity = i.Quantity ?? 0,
                        LimitOrderQuantity = i.LimitOrderQuantity,
                        LimitOrderStart = i.LimitOrderStart,
                        LimitOrderEnd = i.LimitOrderEnd,
                        ShippingDeadline = i.ShippingDeadline,
                        LimitStatus = i.LimitStatus,
                        CreateTime = i.CreateTime.Value,
                        Company_Name = i.Company_Name,
                    }).ToList();
            db.Connection.Close();

            return item;
        }

        public Method.Paging Get_Page(int p = 1, int take = 10)
        {
            return Method.Get_Page(Get_Count(), p, take);
        }

        public int Get_Count()
        {
            return Get_Level1().Count();
        }
        #endregion

        private string Query(string query = "")
        {
            string sql = "";

            if (query != "")
            {
                query = HttpUtility.HtmlEncode(query);
                sql = Method.SQL_Combin(sql, "SN", "OR", "( \"" + query + "\")", ".Contains", false);
                sql = Method.SQL_Combin(sql, "Name", "OR", "( \"" + query + "\")", ".Contains", false);
            }

            return sql;
        }

        /// <summary>
        /// 取得商品圖片
        /// </summary>
        /// <param name="GoodsId">商品主檔編號</param>
        /// <param name="PicSize">圖片大小</param>
        /// <returns>item</returns>
        public string Get_Pic(int GoodsId, string PicSize)
        {
            MithDataContext db = new MithDataContext();
            string pic = "";

            if (PicSize == "Middle")
            {
                pic = db.GoodsPic.FirstOrDefault(f => f.GoodsId == GoodsId) != null ? db.GoodsPic.FirstOrDefault(f => f.GoodsId == GoodsId).Pic_Middle : "";
            }
            else
            {
                pic = db.GoodsPic.FirstOrDefault(f => f.GoodsId == GoodsId) != null ? db.GoodsPic.FirstOrDefault(f => f.GoodsId == GoodsId).Pic_Small : "";
            }
            db.Connection.Close();
            return pic;
        }

        #endregion

        #region 下拉式選單

        /// <summary>
        /// 取得品牌下拉選單
        /// </summary>
        /// <param name="companyId">廠商Id</param>
        /// <returns>選單</returns>
        public static List<SelectListItem> Get_Brand_SelectListItem(int companyId, int brandId)
        {
            MithDataContext db = new MithDataContext();
            List<SelectListItem> data = new List<SelectListItem>();
            var Brand = db.Brand.Where(w => w.CompanyId == companyId);
            if (Brand.Count() > 1)
            {
                data.Add(new SelectListItem
                {
                    Selected = brandId == 0,
                    Text = "請選擇或輸入文字",
                    Value = "0"
                });
            }
            foreach (var item in Brand)
            {
                data.AddRange(new SelectListItem
                {
                    Selected = brandId == item.Id,
                    Text = item.Name,
                    Value = item.Id.ToString()
                });
            }
            db.Connection.Close();
            return data;
        }

        #endregion

    }
}