﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Linq;
using System.Data.Linq.SqlClient;
using Mith.Areas.GoX.Models;
using Mith.Models;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;

namespace Mith.Areas.GoX.Models
{
	public class ReturnGoodsModel
	{
		#region Class
		public class ReturnGoodsShow
		{
			[Key]
			[DisplayName("編號")]
			public int Id { get; set; }
			[DisplayName("關係訂單")]
			public int? RelativeId { get; set; }
			[DisplayName("訂單編號")]
			public string SN { get; set; }
			[DisplayName("物流編號")]
			[Required(ErrorMessage = "必填欄位")]
			public string LogisticsSN { get; set; }
			[DisplayName("包裹編號")]
			public string PackageSN { get; set; }
			[DisplayName("退貨方式")]
			public int LogisticsType { get; set; }
			public string LogisticsType_Name { get; set; }
			[DisplayName("總計")]
			public int BuyTotal { get; set; }
			[DisplayName("姓名")]
			public string ReceiverName { get; set; }
			[DisplayName("電話")]
			public string ReceiverTel { get; set; }
			[DisplayName("地址")]
			public string ReceiverAddress { get; set; }
			[DisplayName("取件時段")]
			public string ReceiveTimeRange_Name { get; set; }
			[DisplayName("退貨廠商")]
			public string Company_Name { get; set; }
			[DisplayName("備註")]
			public string Remark { get; set; }
			[DisplayName("已收到貨")]
			public bool ReturnGoods { get; set; }
			[DisplayName("到貨日期")]
			public DateTime? ReturnGoodsTime { get; set; }
			[DisplayName("同意退款")]
			public bool AgreeRefund { get; set; }
			[DisplayName("退貨時間")]
			public DateTime CreateTime { get; set; }
			[DisplayName("更新人員")]
			public int LastUserId { get; set; }
			[DisplayName("更新人員")]
			public string LastUser_Name { get; set; }

			[DisplayName("7-11到貨時間")]
			public DateTime? ArriveInTime { get; set; }

			[DisplayName("訂單細節List")]
			public List<OrderDeatailShow> OrderDetailList { get; set; }


		}

		public class OrderDeatailShow
		{
			[DisplayName("編號")]
			public int Id { get; set; }
			[DisplayName("商品編號")]
			public string Goods_SN { get; set; }
			[DisplayName("商品名稱")]
			public string Goods_Name { get; set; }
			[DisplayName("商品顏色名稱")]
			public string Goods_ColorName { get; set; }
			[DisplayName("商品類別名稱")]
			public string Goods_TypeName { get; set; }
			[DisplayName("公司名稱")]
			public int CompanyId { get; set; }
			[DisplayName("公司名稱")]
			public string Company_Name { get; set; }
			[DisplayName("品牌名稱")]
			public string Brand_Name { get; set; }
			[DisplayName("購買尺寸")]
			public string BuySize { get; set; }
			[DisplayName("購買數量")]
			public int BuyQuantity { get; set; }
			[DisplayName("購買單價")]
			public int BuyPrice { get; set; }
			[DisplayName("購買總額")]
			public int BuyTotal { get; set; }
			[DisplayName("物流編號")]
			public string LogisticsSN { get; set; }
			[DisplayName("物流狀態")]
			public int LogisticsStatus { get; set; }
			[DisplayName("物流狀態")]
			public string LogisticsStatus_Name { get; set; }
			[DisplayName("應該出貨日期")]
			public DateTime? ShippingDeadline { get; set; }
			[DisplayName("退貨時間")]
			public DateTime? ReturnGoodsTime { get; set; }

			[DisplayName("備註")]
			public string Remark { get; set; }
			[DisplayName("更新時間")]
			public DateTime? UpdateTime { get; set; }
			[DisplayName("更新人員")]
			public int LastUserId { get; set; }
			[DisplayName("更新人員")]
			public string LastUser_Name { get; set; }

		}

		public class Export
		{
			[Key]
			public string SN { get; set; }
			public string Name { get; set; }
			public string TaxId { get; set; }
			public string Tel { get; set; }
			public string Fax { get; set; }
			public string City_Name { get; set; }
			public string Area_Name { get; set; }
			public string Address { get; set; }
			public string BossName { get; set; }
			public string BossIdentityCard { get; set; }
			public string ContactName { get; set; }
			public string ContactTel { get; set; }
			public string ContactEmail { get; set; }
			public string ContractDate { get; set; }
			public string ArtEditorCost { get; set; }
			public string FreightFee { get; set; }
			public string ManagementFee { get; set; }
			public string Collaborate { get; set; }
			public string TypeName { get; set; }
			public string GoodsKind { get; set; }
			public string AccountingName { get; set; }
			public string AccountingTel { get; set; }
			public string AccountingEmail { get; set; }
			public string BankCode { get; set; }
			public string BankName { get; set; }
			public string Account { get; set; }
			public string AccountName { get; set; }
			public string CreateTime { get; set; }
		}

		public string Keyword = "";
		public int Search_CompanyId = 0;
		public int Search_LogisticsStatus = 0;
		public int Search_AgreeRefund = 0;
		public int Search_NotLogisticsSN = 0;
		public DateTime? CreateTime_St = null;
		public DateTime? CreateTime_End = null;

		#endregion

		public ReturnGoodsModel()
		{
		}
		public void Clear()
		{
			Keyword = "";
			Search_CompanyId = 0;
			Search_LogisticsStatus = 0;
			Search_AgreeRefund = 0;
			Search_NotLogisticsSN = 0;
			CreateTime_St = null;
			CreateTime_End = null;
		}

		#region Get

		private IQueryable<ReturnGoods_Index_View> Get()
		{
			MithDataContext db = new MithDataContext();
			IQueryable<ReturnGoods_Index_View> data = db.ReturnGoods_Index_View;

			if (Search_CompanyId > 0)
			{
				data = data.Where(w => w.CompanyId == Search_CompanyId);
			}
			if (Keyword != "")
			{
				Keyword = Keyword.Replace(" ", "");
				data = data.Where(Query(Keyword));
			}
			if (Search_LogisticsStatus != 0)
			{
				data = data.Where(w => w.LogisticsStatus == Search_LogisticsStatus);
			}
			if (Search_AgreeRefund != 0)
			{
				if (Search_AgreeRefund == 1)
				{
					data = data.Where(w => w.AgreeRefund == true);
				}
				else
				{
					data = data.Where(w => w.AgreeRefund == false);
				}
			}
			if (Search_NotLogisticsSN != 0)
			{
				data = data.Where(w => w.LogisticsSN == null || w.LogisticsSN == "");
			}
			if (CreateTime_St != null)
			{
				data = data.Where(w => w.CreateTime >= CreateTime_St);
			}
			if (CreateTime_End != null)
			{
				CreateTime_End = DateTime.Parse(CreateTime_End.Value.ToString("yyyy/MM/dd") + " 23:59:00");
				data = data.Where(w => w.CreateTime <= CreateTime_End);
			}
			db.Connection.Close();
			return data;
		}

		public List<ReturnGoodsShow> Get_Data(int p = 1, int take = 10)
		{
			MithDataContext db = new MithDataContext();
			List<ReturnGoods_Index_View> data = new List<ReturnGoods_Index_View>();

			data = Get().OrderByDescending(o => o.CreateTime).Skip((p - 1) * take).Take(take).ToList();

			List<ReturnGoodsShow> item = new List<ReturnGoodsShow>();
			item = (from i in data
							select new ReturnGoodsShow
							{
								Id = i.Id,
								SN = i.SN,
								LogisticsSN = i.LogisticsSN,
								PackageSN = i.PackageSN,
								LogisticsType = i.LogisticsType,
								LogisticsType_Name = i.LogisticsType_Name,
								BuyTotal = i.BuyTotal.Value,
								ReturnGoods = i.ReturnGoods,
								AgreeRefund = i.AgreeRefund,
								ReceiverName = i.ReceiverName,
								ReceiverTel = i.ReceiverTel,
								CreateTime = i.CreateTime.Value,
								Company_Name = i.Company_Name,
								OrderDetailList = Get_OrderDetailList(i.Id, "Index"),
							}).ToList();

			db.Connection.Close();
			return item;
		}

		public ReturnGoodsShow Get_One(int Id = 0)
		{
			MithDataContext db = new MithDataContext();
			var data = (from i in db.ReturnGoods_Edit_View
									where i.Id == Id
									select new ReturnGoodsShow
									{
										Id = i.Id,
										SN = i.SN,
										RelativeId = i.RelativeId,
										LogisticsSN = i.LogisticsSN,
										PackageSN = i.PackageSN,
										LogisticsType = i.LogisticsType,
										LogisticsType_Name = i.LogisticsType_Name,
										BuyTotal = i.BuyTotal.Value,
										ReceiverName = i.ReceiverName,
										ReceiverTel = i.ReceiverTel,
										ReceiverAddress = i.ReceiverAddress,
										ReceiveTimeRange_Name = i.ReceiveTimeRange_Name,
										Company_Name = i.Company_Name,
										ReturnGoods = i.ReturnGoods,
										ReturnGoodsTime = i.ReturnGoodsTime,
										AgreeRefund = i.AgreeRefund,
										Remark = HttpUtility.HtmlDecode(i.Remark),
										CreateTime = i.CreateTime.Value,
										LastUser_Name = i.LastUser_Name,
										ArriveInTime = i.ArriveInTime,
										OrderDetailList = Get_OrderDetailList(i.Id, "Index"),
									}).FirstOrDefault();
			db.Connection.Close();
			return data;
		}

		public Method.Paging Get_Page(int p = 1, int take = 10)
		{
			return Method.Get_Page(Get_Count(), p, take);
		}

		public int Get_Count()
		{
			return Get().Count();
		}

		private string Query(string query = "")
		{
			string sql = "";

			if (query != "")
			{
				query = HttpUtility.HtmlEncode(query);
				sql = Method.SQL_Combin(sql, "SN", "OR", "( \"" + query + "\")", ".Contains", false);
				sql = Method.SQL_Combin(sql, "LogisticsSN", "OR", "( \"" + query + "\")", ".Contains", false);
				sql = Method.SQL_Combin(sql, "PackageSN", "OR", "( \"" + query + "\")", ".Contains", false);
			}

			return sql;
		}
		/// <summary>
		/// 取得訂單主檔底下的訂單細節
		/// </summary>
		/// <param name="OrderId">訂單編號</param>
		/// <param name="CompanyId">廠商編號</param>
		public List<OrderDeatailShow> Get_OrderDetailList(int Id = 0, string Action = "")
		{
			MithDataContext db = new MithDataContext();
			try
			{
				#region 來自 Index頁
				if (Action == "Index")
				{
					var data = (from i in db.OrderDetail_Edit_View
											where i.OrderId == Id
											orderby i.CompanyId
											select new OrderDeatailShow
											{
												Id = i.Id,
												Goods_SN = i.Goods_SN,
												Goods_Name = i.Goods_Name,
												Goods_ColorName = i.Goods_ColorName,
												Goods_TypeName = i.Goods_TypeName,
												CompanyId = i.CompanyId,
												Company_Name = i.Company_Name,
												BuySize = i.BuySize,
												BuyQuantity = i.BuyQuantity,
												BuyPrice = i.BuyPrice,
												BuyTotal = i.BuyTotal,
												LogisticsStatus = i.LogisticsStatus,
												LogisticsStatus_Name = i.LogisticsStatus_Name,
											}).ToList();

					if (Search_CompanyId > 0)
					{
						data = data.Where(w => w.CompanyId == Search_CompanyId).ToList();

					}
					return data;
				}
				#endregion

				#region 來自 Edit頁
				else
				{
					var data = (from i in db.OrderDetail_Edit_View
											where i.OrderId == Id
											orderby i.CompanyId
											select new OrderDeatailShow
											{
												Id = i.Id,
												Goods_SN = i.Goods_SN,
												Goods_Name = i.Goods_Name,
												Goods_ColorName = i.Goods_ColorName,
												Goods_TypeName = i.Goods_TypeName,
												CompanyId = i.CompanyId,
												BuySize = i.BuySize,
												BuyQuantity = i.BuyQuantity,
												BuyPrice = i.BuyPrice,
												BuyTotal = i.BuyTotal,
												ShippingDeadline = i.ShippingDeadline,
												Company_Name = i.Company_Name,
												LogisticsStatus = i.LogisticsStatus,
												LogisticsStatus_Name = i.LogisticsStatus_Name,
											}).ToList();

					if (Search_CompanyId > 0)
					{
						data = data.Where(w => w.CompanyId == Search_CompanyId).ToList();

					}
					return data;
				}
				#endregion
			}
			catch
			{
				return null;
			}
			finally
			{
				db.Connection.Close();
			}

		}

		#endregion

		#region update

		public int Update(ReturnGoodsShow item)
		{
			MithDataContext db = new MithDataContext();
			try
			{
				int Id = 0;
				var Order = db.Order.FirstOrDefault(f => f.Id == item.Id);
				if (Order != null)
				{
					Order.ReturnGoods = item.ReturnGoods;
					Order.AgreeRefund = item.AgreeRefund;
					if (!string.IsNullOrWhiteSpace(item.ReturnGoodsTime.ToString()))
					{
						Order.ReturnGoodsTime = item.ReturnGoodsTime.Value.AddHours(-8);
					}

					Order.UpdateTime = DateTime.UtcNow;
					Order.LastUserId = item.LastUserId;
					db.SubmitChanges();
					Id = Order.Id;
				}

				var OrderDetail = db.OrderDetail.Where(f => f.OrderId == item.Id);
				foreach (var item2 in OrderDetail)
				{
					item2.LogisticsSN = item.LogisticsSN;

					//已收到貨
					if (item.ReturnGoods == true)
					{
						//收到退貨
						item2.LogisticsStatus = 4;
					}
					db.SubmitChanges();
				}

				db.Connection.Close();
				return Id;
			}
			catch { return -1; }
			finally
			{
				db.Connection.Close();
			}
		}

		#endregion

		#region Delete
		public void Delete(int id = 0)
		{
			if (id > 0)
			{
				Delete(new int[] { id });
			}
		}

		public void Delete(int[] id)
		{
			if (id != null)
			{
				if (id.Any())
				{
					MithDataContext db = new MithDataContext();
					var data = db.Order.Where(w => id.Contains(w.Id));
					if (data.Any())
					{
						db.Order.DeleteAllOnSubmit(data);

						db.SubmitChanges();
					}
					db.Connection.Close();
				}
			}
		}

		#endregion

		public static List<SelectListItem> Get_ReceiverTimeRange_Select(int Id)
		{
			MithDataContext db = new MithDataContext();
			List<SelectListItem> data = new List<SelectListItem>();

			data.AddRange(db.Category.Where(w => w.Fun_Id == 10 && w.Group == "ReceiverTimeRange").OrderBy(o => o.Id).Select(s =>
					new SelectListItem
					{
						Selected = Id == s.Id,
						Text = s.Name,
						Value = s.Id.ToString()
					}));
			db.Connection.Close();
			return data;
		}

		//#region Export
		//public MemoryStream Set_Excel(List<Export> data)
		//{
		//    //標題
		//    List<string> header = new List<string>();
		//    header.Add("廠商編號");
		//    header.Add("名稱");
		//    header.Add("統一編號");
		//    header.Add("電話");
		//    header.Add("傳真");
		//    header.Add("地址");
		//    header.Add("負責人姓名");
		//    header.Add("負責人身分證字號");

		//    MemoryStream ms = new MemoryStream();
		//    HSSFWorkbook workbook = new HSSFWorkbook();
		//    HSSFSheet sheet = (HSSFSheet)workbook.CreateSheet("sheet");

		//    //宣告headStyle、內容style
		//    HSSFCellStyle headStyle = null;
		//    HSSFCellStyle contStyle = null;
		//    headStyle = (HSSFCellStyle)workbook.CreateCellStyle();
		//    contStyle = (HSSFCellStyle)workbook.CreateCellStyle();
		//    HSSFFont font = null;
		//    font = (HSSFFont)workbook.CreateFont();
		//    HSSFCell cell = null;

		//    //標題粗體、黃色背景
		//    headStyle.FillForegroundColor = HSSFColor.LightYellow.Index;
		//    headStyle.FillPattern = NPOI.SS.UserModel.FillPattern.SolidForeground;
		//    //標題字型樣式
		//    font.FontHeightInPoints = 10;
		//    font.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
		//    font.FontName = "Microsoft JhengHei";
		//    headStyle.SetFont(font);
		//    //內容字型樣式(自動換行)
		//    contStyle.WrapText = true;
		//    //contStyle.VerticalAlignment = NPOI.SS.UserModel.VerticalAlignment.Center;
		//    //宣告headRow
		//    sheet.CreateRow(0);
		//    //設定head背景、粗體、內容
		//    foreach (var i in header)
		//    {
		//        cell = (HSSFCell)sheet.GetRow(0).CreateCell(header.IndexOf(i));
		//        cell.SetCellValue(i);
		//        cell.CellStyle = headStyle;
		//    }
		//    //資料欄位
		//    for (int i = 0; i < data.Count; i++)
		//    {
		//        sheet.CreateRow(i + 1);

		//        sheet.GetRow(i + 1).CreateCell(0).SetCellValue(data[i].SN);
		//        sheet.GetRow(i + 1).CreateCell(1).SetCellValue(data[i].Name);
		//        sheet.GetRow(i + 1).CreateCell(2).SetCellValue(data[i].TaxId);
		//        sheet.GetRow(i + 1).CreateCell(3).SetCellValue(data[i].Tel);
		//        sheet.GetRow(i + 1).CreateCell(4).SetCellValue(data[i].Fax);
		//        sheet.GetRow(i + 1).CreateCell(5).SetCellValue(data[i].City_Name + data[i].Area_Name + data[i].Address);
		//        sheet.GetRow(i + 1).CreateCell(6).SetCellValue(data[i].BossName);
		//        sheet.GetRow(i + 1).CreateCell(7).SetCellValue(data[i].BossIdentityCard);
		//        sheet.GetRow(i + 1).CreateCell(8).SetCellValue(data[i].ContactName);
		//        sheet.GetRow(i + 1).CreateCell(9).SetCellValue(data[i].ContactTel);
		//        sheet.GetRow(i + 1).CreateCell(10).SetCellValue(data[i].ContactEmail);
		//        sheet.GetRow(i + 1).CreateCell(11).SetCellValue(data[i].ContractDate);
		//        sheet.GetRow(i + 1).CreateCell(12).SetCellValue(data[i].ArtEditorCost);
		//        sheet.GetRow(i + 1).CreateCell(13).SetCellValue(data[i].FreightFee);
		//        sheet.GetRow(i + 1).CreateCell(14).SetCellValue(data[i].ManagementFee);
		//        sheet.GetRow(i + 1).CreateCell(15).SetCellValue(data[i].Collaborate);
		//        sheet.GetRow(i + 1).CreateCell(16).SetCellValue(data[i].TypeName);
		//        sheet.GetRow(i + 1).CreateCell(17).SetCellValue(data[i].GoodsKind);
		//        sheet.GetRow(i + 1).CreateCell(18).SetCellValue(data[i].AccountingName);
		//        sheet.GetRow(i + 1).CreateCell(19).SetCellValue(data[i].AccountingTel);
		//        sheet.GetRow(i + 1).CreateCell(20).SetCellValue(data[i].AccountingEmail);
		//        sheet.GetRow(i + 1).CreateCell(21).SetCellValue(data[i].BankCode);
		//        sheet.GetRow(i + 1).CreateCell(22).SetCellValue(data[i].BankName);
		//        sheet.GetRow(i + 1).CreateCell(23).SetCellValue(data[i].Account);
		//        sheet.GetRow(i + 1).CreateCell(24).SetCellValue(data[i].AccountName);
		//        sheet.GetRow(i + 1).CreateCell(25).SetCellValue(data[i].CreateTime);
		//    }
		//    foreach (var i in header)
		//    {
		//        sheet.SetColumnWidth(header.IndexOf(i), (short)256 * 12);
		//    }

		//    workbook.Write(ms);
		//    ms.Position = 0;
		//    ms.Flush();
		//    return ms;
		//}

		//public List<Export> Export_Data(ReturnGoodsModel data)
		//{

		//    /*-------------------匯出宣告-------------------*/
		//    MithDataContext db = new MithDataContext();
		//    Export export = new Export();
		//    List<Export> exp = new List<Export>();
		//    var item = data.Get().OrderByDescending(o => o.Id);
		//    /*-------------------匯出宣告End-------------------*/

		//    foreach (var i in item)
		//    {

		//        export.Name = i.Name;
		//        export.CreateTime = i.CreateTime.Value.ToString("yyyy/MM/dd HH:mm");

		//        exp.Add(export);
		//        export = new Export();
		//    }
		//    db.Connection.Close();
		//    return exp;
		//}
		//#endregion

	}
}