﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Mith.Models;
using System.Threading;

namespace Mith.Areas.GoX.Models
{
	public class PushModel
	{
		/*public class ErrorToken
		{
				public PushToken Token { get; set; }
				public string Error { get; set; }
		}*/

		public class PushList
		{
			public PushMessage Message { get; set; }
			public List<PushToken> Token { get; set; }
		}

		public class ResultData
		{
			public string Token { get; set; }
			public int Vid { get; set; }
			public string Title { get; set; }
			public string Message { get; set; }
			public bool Resent { get; set; }
			public int Badge { get; set; }
			public int MessageId { get; set; }
			public int AppId { get; set; }
		}

		public class PushParams
		{
			public PushApp App { get; set; }
			public HttpServerUtilityBase Server { get; set; }
			public int[] VolunteersId { get; set; }
			public int[] Msg { get; set; }
		}

		/// <summary>
		/// 推播 Android
		/// </summary>
		/// <param name="app">指定app</param>
		/// <param name="sv">server</param>
		/// <param name="VolunteersId">VolunteersId==null ? 全部會員 : 指定會員</param>
		/// <param name="Msg">Msg==null ? 全部訊息 : 指定訊息</param>
		public static void Push_Android(PushApp app, HttpServerUtilityBase sv, int[] VolunteersId = null, int[] Msg = null)
		{
			if (app.AllowPush)
			{
				if (VolunteersId[0] == 0)
				{
					VolunteersId = null;
				}
				ParameterizedThreadStart pts = null;

				pts = new ParameterizedThreadStart(GCM.PushMessage);

				if (pts != null)
				{
					Thread thread = new Thread(pts);
					thread.Start(new PushModel.PushParams
					{
						App = app,
						Server = sv,
						VolunteersId = VolunteersId,
						Msg = Msg
					});
				}
			}
		}

		/// <summary>
		/// 推播 ios
		/// </summary>
		/// <param name="app">指定app</param>
		/// <param name="sv">server</param>
		/// <param name="VolunteersId">VolunteersId==null ? 全部會員 : 指定會員</param>
		/// <param name="Msg">Msg==null ? 全部訊息 : 指定訊息</param>
		public static void Push_ios(PushApp app, HttpServerUtilityBase sv, int[] VolunteersId = null, int[] Msg = null)
		{
			if (app.AllowPush)
			{
				if (VolunteersId[0] == 0)
				{
					VolunteersId = null;
				}
				else
				{

				}
				ParameterizedThreadStart pts2 = null;

				pts2 = new ParameterizedThreadStart(APNS.PushMessage);

				if (pts2 != null)
				{
					Thread thread2 = new Thread(pts2);
					thread2.Start(new PushModel.PushParams
					{
						App = app,
						Server = sv,
						VolunteersId = VolunteersId,
						Msg = Msg
					});
				}
			}
		}

		/// <summary>
		/// 推播
		/// </summary>
		/// <param name="app">指定app</param>
		/// <param name="sv">server</param>
		/// <param name="VolunteersId">VolunteersId==null ? 全部會員 : 指定會員</param>
		/// <param name="Msg">Msg==null ? 全部訊息 : 指定訊息</param>
		public static void Push(int appid, HttpServerUtilityBase sv, int[] VolunteersId = null, int[] Msg = null)
		{
			MithDataContext db = new MithDataContext();

			string Title = db.PushMessage.FirstOrDefault(f => f.Id == Msg[0]).Title;

			int Search1 = Title.IndexOf("Android-Normal");

			if (Search1 > -1 )
			{
				////Android 一般版
				PushApp Android_Normal = db.PushApp.FirstOrDefault(f => f.Id == 1);
				Push_Android(Android_Normal, sv, VolunteersId, Msg);
			}

			int Search2 = Title.IndexOf("Android-Expert");

			if (Search2 > -1 )
			{
				////Android 達人版
				PushApp Android_Expert = db.PushApp.FirstOrDefault(f => f.Id == 2);
				Push_Android(Android_Expert, sv, VolunteersId, Msg);
			}

			int Search3 = Title.IndexOf("ios-Normal");

			if (Search3 > -1 )
			{
				//ios 一般版
				PushApp ios_Normal = db.PushApp.FirstOrDefault(f => f.Id == 3);
				Push_ios(ios_Normal, sv, VolunteersId, Msg);
			}

			int Search4 = Title.IndexOf("Andriod-Expert");

			if (Search4 > -1 )
			{
				//ios 達人版
				PushApp ios_Expert = db.PushApp.FirstOrDefault(f => f.Id == 4);
				Push_ios(ios_Expert, sv, VolunteersId, Msg);
			}

			int sum = Search1 + Search2 + Search3 + Search4;
			if(sum == -4)
			{
				////Android 一般版
				PushApp Android_Normal = db.PushApp.FirstOrDefault(f => f.Id == 1);
				Push_Android(Android_Normal, sv, VolunteersId, Msg);
				////Android 達人版
				PushApp Android_Expert = db.PushApp.FirstOrDefault(f => f.Id == 2);
				Push_Android(Android_Expert, sv, VolunteersId, Msg);
				//ios 一般版
				PushApp ios_Normal = db.PushApp.FirstOrDefault(f => f.Id == 3);
				Push_ios(ios_Normal, sv, VolunteersId, Msg);
				//ios 達人版
				PushApp ios_Expert = db.PushApp.FirstOrDefault(f => f.Id == 4);
				Push_ios(ios_Expert, sv, VolunteersId, Msg);
			}

		}
	}
}