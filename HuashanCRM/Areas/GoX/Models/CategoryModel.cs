﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using Mith.Models;
using System.Web.Mvc;
using System.IO;
using Mith.Areas.GoX.Models;

namespace Mith.Areas.GoX.Models
{
    public class CategoryModel
    {
        public class CategoryShow
        {
            [Key]
            [DisplayName("編號")]
            public int Id { get; set; }
            public int Fun_Id { get; set; }
            [DisplayName("分類名稱")]
            [Required(ErrorMessage = "必填欄位")]
            public string Name { get; set; }
            [DisplayName("排序")]
            [Required(ErrorMessage = "必填欄位")]
            public int Sort { get; set; }
            [DisplayName("是否顯示")]
            [Required(ErrorMessage = "必填欄位")]
            public bool Display { get; set; }
        }

        public class Checkbox_Category
        {
            public List<Checkbox_Category> items = new List<Checkbox_Category>();
            public int Id { get; set; }
            public string Name { get; set; }
            public bool Display { get; set; }
        }

        public class Drop_Category
        {
            public List<SelectListItem> items = new List<SelectListItem>();
            public int Id { get; set; }
            public string Name { get; set; }
        }
        public CategoryModel(int fun)
        {
            Fun_Id = fun;
            Config = Get_Config(fun);
        }

        public string Keyword = "";
        public string Sort = "id";
        private int Fun_Id;
        public bool? Search_Enable = null;
        public CategoryConfig Config { get; set; }

        public void Clear()
        {
            Keyword = "";
            Sort = "id";
            Search_Enable = null;
        }

        public static CategoryConfig Get_Config(int fun_id)
        {
            MithDataContext db = new MithDataContext();
            var data = db.CategoryConfig.FirstOrDefault(f => f.Fun_Id == fun_id);
            db.Connection.Close();
            return data;
        }

        public static int Set_Config(CategoryConfig item)
        {
            MithDataContext db = new MithDataContext();
            var data = db.CategoryConfig.FirstOrDefault(f => f.Fun_Id == item.Fun_Id && f.Id == item.Id);
            if (data != null)
            {
                data.EnableCategory = item.EnableCategory;
                data.EnableIcon = item.EnableIcon;
                db.SubmitChanges();
                return data.Id;
            }
            db.Connection.Close();
            return -1;
        }

        private List<CategoryShow> Convert(MithDataContext db, List<Category> item)
        {
            return item.Select(s => new CategoryShow
            {
                Id = s.Id,
                Fun_Id = s.Fun_Id,
                Name = s.Name,
                Sort = s.Sort,
                Display = s.Display,
            }).ToList();
        }

        private CategoryShow Convert(MithDataContext db, Category item)
        {
            return new CategoryShow
            {
                Id = item.Id,
                Fun_Id = item.Fun_Id,
                Name = item.Name,
                Sort = item.Sort,
                Display = item.Display,
            };
        }

        private Category Convert(CategoryShow item)
        {
            return new Category
            {
                Id = item.Id,
                Fun_Id = Fun_Id,
                Name = item.Name,
                Sort = item.Sort,
                Display = true,
            };
        }

        public static Dictionary<int, string> Get_CategoryName(int fun_id)
        {
            MithDataContext db = new MithDataContext();
            Dictionary<int, string> name = new Dictionary<int, string>();
            var data = db.Category.FirstOrDefault(w => w.Fun_Id == fun_id && w.Display == true);
            int id = 0;
            do
            {
                if (data != null)
                {
                    name.Add(data.Id, data.Name);
                }
            } while (id > 0);
            if (name.Any())
            {
                if (!name.ContainsKey(data.Id))
                {
                    name.Add(data.Id, data.Name);
                }
            }
            db.Connection.Close();
            return name.Reverse().ToDictionary(d => d.Key, d => d.Value);
        }

        /// <summary>
        /// 取指定分類底下所有子分類
        /// </summary>
        /// <param name="id"></param>
        /// <param name="fun_id"></param>
        /// <returns></returns>
        public static Checkbox_Category Get_Category_Cid(int id, int fun_id)
        {
            MithDataContext db = new MithDataContext();

            var data = db.Category.FirstOrDefault(w => w.Fun_Id == fun_id && w.Display == true && w.Id == id );
            if (data == null)
            {
                return null;
            }
            Checkbox_Category item = new Checkbox_Category();
            item.Id = data.Id;
            item.Name = data.Name;
            item.items.AddRange(Get_Category_Cid(db, data.Id, fun_id));
            return item;
        }

        /// <summary>
        /// 取指定分類底下所有子分類遞迴
        /// </summary>
        /// <param name="db"></param>
        /// <param name="id"></param>
        /// <param name="fun_id"></param>
        /// <returns></returns>
        private static List<Checkbox_Category> Get_Category_Cid(MithDataContext db, int id, int fun_id)
        {
            return db.Category.Where(w => w.Fun_Id == fun_id && w.Display == true ).OrderBy(o => o.Sort).ThenBy(t => t.Id).Select(s => new Checkbox_Category
            {
                Id = s.Id,
                Name = s.Name,
                items = Get_Category_Cid(db, s.Id, fun_id)
            }).ToList();
        }

        /// <summary>
        /// 取得主分類
        /// </summary>
        /// <param name="fun_id"></param>
        /// <param name="show"></param>
        /// <returns></returns>
        public static List<Category> Get_MainCategory(int fun_id, bool? show = null)
        {
            MithDataContext db = new MithDataContext();
            var data = db.Category.Where(w => w.Fun_Id == fun_id );
            if (show != null)
            {
                data = data.Where(w => w.Display == show);
            }
            var result = data.OrderBy(o => o.Sort).ThenBy(t => t.Id).ToList();
            db.Connection.Close();
            return result;
        }

        /// <summary>
        /// 取得所有分類(自動判斷是否有開子分類)
        /// </summary>
        /// <param name="fun_id"></param>
        /// <returns></returns>
        public static List<Checkbox_Category> Get_Category(int fun_id, bool? show)
        {
            MithDataContext db = new MithDataContext();
            var config = Get_Config(fun_id);

            if (config.EnableCategory)
            {
                return Gen_Check(db, 0, 1, fun_id, show);
            }
            else
            {
                var tmp = db.Category.Where(w => w.Fun_Id == fun_id  );
                if (show != null)
                {
                    tmp = tmp.Where(w => w.Display == show);
                }
                return tmp.OrderBy(o => o.Sort).ThenBy(t => t.Id)
                    .Select(s => new Checkbox_Category
                    {
                        Id = s.Id,
                        Name = s.Name,
                        Display = s.Display,
                        items = new List<Checkbox_Category>()
                    }).ToList();
            }
        }

        private static List<Checkbox_Category> Gen_Check(MithDataContext db, int id, int level, int fun_id, bool? show)
        {
            var tmp = db.Category.Where(w =>  w.Fun_Id == fun_id );
            if (show != null)
            {
                tmp = tmp.Where(w => w.Display == show);
            }
            return tmp.OrderBy(o => o.Sort).ThenBy(t => t.Id).Select(s => new Checkbox_Category
            {
                Id = s.Id,
                Name = s.Name,
                Display = s.Display,
                items = Gen_Check(db, s.Id, level + 1, fun_id, show)
            }).ToList();
        }
        public static string Get_Category_html(List<CategoryModel.Checkbox_Category> data, int[] cid, bool first = true)
        {
            string output = "";
            if (data.Any())
            {
                output += "<ul" + (first ? " class=\"menu\"" : "") + ">";
                foreach (var i in data)
                {
                    string check = "";
                    if (cid != null)
                    {
                        if (cid.Contains(i.Id))
                        {
                            check += "checked=\"checked\"";
                        }
                    }
                    output += "<li>";
                    output += "<label " + (i.items.Any() ? "" : " class=\"parent\"") + " >" +
                        "<input type=\"checkbox\" value=\"" + i.Id + "\" id=\"cid_" + i.Id + "\" name=\"cid\" " + check + " />" + i.Name +
                        "</label>";
                    if (i.items.Any())
                    {
                        output += Get_Category_html(i.items, cid, false);
                    }
                    output += "</li>";
                }
                output += "</ul>";
            }
            return output;
        }

        /// <summary>
        /// 前台分類menu
        /// </summary>
        /// <param name="data"></param>
        /// <param name="cid"></param>
        /// <param name="first"></param>
        /// <returns></returns>
        public static string Get_Category_Menu_html(List<CategoryModel.Checkbox_Category> data, int[] cid, bool first = true)
        {
            string output = "";
            if (data.Any())
            {
                output += "<ul" + (first ? " class=\"menu\"" : "") + ">";
                foreach (var i in data)
                {
                    string check = "unselected";
                    if (cid != null)
                    {
                        if (cid.Contains(i.Id))
                        {
                            check = "selected";
                        }
                    }
                    output += "<li class =\"" + check + "\">";
                    output += "<label " + (i.items.Any() ? "" : " class=\"parent\"") + " data-id=\"" + i.Id + "\">" + i.Name + "</label>";
                    if (i.items.Any())
                    {
                        output += Get_Category_Menu_html(i.items, cid, false);
                    }
                    output += "</li>";
                }
                output += "</ul>";
            }
            return output;
        }

        /// <summary>
        /// 下拉式選單(自動判斷是否有子分類)
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public List<SelectListItem> Get_Category_Select(int? id, bool select_item = true, string select_text = "請選擇")
        {
            int select = 0;
            if (id != null)
            {
                select = id.Value;
            }
            MithDataContext db = new MithDataContext();
            List<SelectListItem> data = new List<SelectListItem>();
            if (select_item)
            {
                data.Add(new SelectListItem
                {
                    Selected = select == 0,
                    Text = select_text,
                    Value = "0"
                });
            }
            var result = db.Category.Where(w => w.Fun_Id == Fun_Id && w.Display == true ).ToList();

            if (Config.EnableCategory)
            {
                data = Gen_Select(data, result, 0, select);
            }
            else
            {
                data.AddRange(db.Category.Where(w => w.Fun_Id == Fun_Id && w.Display == true ).OrderBy(o => o.Sort).ThenBy(t => t.Id).Select(s =>
                    new SelectListItem
                    {
                        Selected = select == s.Id,
                        Text = s.Name,
                        Value = s.Id.ToString()
                    }));
            }
            db.Connection.Close();
            return data;
        }

        private List<SelectListItem> Gen_Select(List<SelectListItem> data, List<Category> result, int id, int select)
        {
            var tmp = result.Where(w =>  w.Fun_Id == Fun_Id).OrderBy(o => o.Sort).ThenBy(t => t.Id);

            if (tmp != null)
            {
                if (tmp.Any())
                {
                    foreach (var item in tmp)
                    {
                        data.Add(new SelectListItem
                        {
                            Selected = select == item.Id,
                            Text = "",
                            Value = item.Id.ToString()
                        });
                        data = Gen_Select(data, result, item.Id, select);
                    }
                    //return data;
                }
            }
            return data;
        }


        /// <summary>
        /// 取指定分類
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public CategoryShow Get_One(int id)
        {
            try
            {
                MithDataContext db = new MithDataContext();
                var data = db.Category.FirstOrDefault(f => f.Id == id && f.Fun_Id == Fun_Id);
                if (data != null)
                {
                    if (Search_Enable == true)
                    {
                        if (!data.Display) return null;
                    }
                    //db.Connection.Close();
                    return Convert(db, data);
                }
                db.Connection.Close();
            }
            catch { }
            return null;
        }
        public static string Get_Name(int id)
        {
            try
            {
                MithDataContext db = new MithDataContext();
                var data = db.Category.FirstOrDefault(f => f.Id == id);
                db.Connection.Close();
                return data.Name;
            }
            catch { }
            return null;
        }
        public List<CategoryShow> Get_Data(int p = 1, int take = 10)
        {
            try
            {
                MithDataContext db = new MithDataContext();
                var data = Get_Sort(Get()).Skip((p - 1) * take).Take(take);
                List<CategoryShow> result = Convert(db, data.ToList());
                db.Connection.Close();
                return result;
                /*return Get_Sort(Get()).Skip((p - 1) * take).Take(take).Select(s => new CategoryShow
                {
                    Id = s.Id,
                    Fun_Id = s.Fun_Id,
                    UpId = s.UpId,
                    UpName = "",
                    Name = s.Name,
                    Sort = s.Sort,
                    Display = s.Display
                }).ToList();*/
            }
            catch { return new List<CategoryShow>(); }
        }

        public Method.Paging Get_Page(int p = 1, int take = 10)
        {
            return Method.Get_Page(Get_Count(), p, take);
        }

        public int Get_Count()
        {
            try
            {
                return Get().Count();
            }
            catch { return 0; }
        }

        private IQueryable<Category> Get()
        {
            MithDataContext db = new MithDataContext();
            IQueryable<Category> data = db.Category.Where(w => w.Fun_Id == Fun_Id );
            if (Keyword != "")
            {
                data = data.Where(Query(Keyword));
            }
            if (Search_Enable != null)
            {
                data = data.Where(w => w.Display == Search_Enable.Value);
            }
            db.Connection.Close();
            return data;
        }

        private IQueryable<Category> Get_Sort(IQueryable<Category> data)
        {
            switch (Sort)
            {
                case "sort":
                    data = data.OrderBy(o => o.Sort);
                    break;
                default:
                    data = data.OrderBy(o => o.Sort);
                    break;
            }
            return data;
        }

        private string Query(string query = "")
        {
            string sql = "";

            if (query != "")
            {
                query = HttpUtility.HtmlEncode(query);
                sql = Method.SQL_Combin(sql, "Name", "OR", "( \"" + query + "\")", ".Contains", false);
            }

            return sql;
        }

        public int Insert(CategoryShow item)
        {
            try
            {
                MithDataContext db = new MithDataContext();
                Category new_item = Convert(item);
                db.Category.InsertOnSubmit(new_item);
                db.SubmitChanges();
                db.Connection.Close();
                return new_item.Id;
            }
            catch { return -1; }
        }

        #region update
        public int Update(CategoryShow item, HttpServerUtilityBase Server, bool delete)
        {
            try
            {
                MithDataContext db = new MithDataContext();
                var data = db.Category.FirstOrDefault(f => f.Id == item.Id && f.Fun_Id == Fun_Id);
                if (data != null)
                {
                    data.Name = item.Name;
                    data.Sort = item.Sort;
                    data.Display = true;
                    db.SubmitChanges();
                    db.Connection.Close();
                    return data.Id;
                }
                db.Connection.Close();
                return -1;
            }
            catch { return -1; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cid1">來源</param>
        /// <param name="cid2">目標</param>
        /// <param name="Server"></param>
        /// <returns></returns>
        public int Update_CoverByCid(int cid1, int cid2, HttpServerUtilityBase Server)
        {
            var data1 = Get_One(cid1);
            var data2 = Get_One(cid2);

            if (data1 != null && data2 != null)
            {
                if (Update(data2, Server, Method.SiteDel) > 0)
                {
                    return data2.Id;
                }
            }
            return -1;
        }

        public int Update_Sort(List<int> id)
        {
            try
            {
                if (id != null)
                {
                    if (id.Any())
                    {
                        MithDataContext db = new MithDataContext();
                        int result = -1;
                        var data = db.Category.Where(w => id.Contains(w.Id) && w.Fun_Id == Fun_Id );
                        if (data.Any())
                        {
                            foreach (var i in id)
                            {
                                var tmp = data.FirstOrDefault(f => f.Id == i);
                                tmp.Sort = id.IndexOf(i);
                                db.SubmitChanges();
                                result = i;
                            }
                            db.Connection.Close();
                            return result;
                        }
                        db.Connection.Close();
                    }
                }
                return -1;
            }
            catch { return -1; }
        }

        public int Update_Move(int id, int upid)
        {
            return Update_Move(new int[] { id }, upid);
        }

        public int Update_Move(int[] id, int upid)
        {
            try
            {
                if (id != null)
                {
                    if (id.Any())
                    {
                        var tmp = Get_One(upid);
                        MithDataContext db = new MithDataContext();
                        int result = -1;
                        var data = db.Category.Where(w => id.Contains(w.Id) && w.Fun_Id == Fun_Id  && w.Id != upid);
                        if (data.Any())
                        {
                            foreach (var i in data)
                            {
                                result = i.Id;
                            }
                            db.SubmitChanges();
                            db.Connection.Close();
                            return result;
                        }
                        db.Connection.Close();
                    }
                }
                return -1;
            }
            catch { return -1; }
        }

        public int Update_Enable(int id, bool en)
        {
            return Update_Enable(new int[] { id }, en);
        }

        public int Update_Enable(int[] id, bool en)
        {
            try
            {
                if (id != null)
                {
                    if (id.Any())
                    {
                        MithDataContext db = new MithDataContext();
                        int result = -1;
                        var data = db.Category.Where(w => id.Contains(w.Id) && w.Fun_Id == Fun_Id );
                        if (data.Any())
                        {
                            foreach (var i in data)
                            {
                                i.Display = en;
                                result = i.Id;
                            }
                            db.SubmitChanges();
                            db.Connection.Close();
                            return result;
                        }
                        db.Connection.Close();
                    }
                }
                return -1;
            }
            catch { return -1; }
        }
        #endregion

        #region delete
        public void Delete(int id, HttpServerUtilityBase Server)
        {
            if (id > 0)
            {
                Delete(new int[] { id }, Server);
            }
        }

        public void Delete(int[] id, HttpServerUtilityBase Server)
        {
            if (id != null)
            {
                if (id.Any())
                {
                    MithDataContext db = new MithDataContext();
                    var data = db.Category.Where(w => id.Contains(w.Id) && w.Fun_Id == Fun_Id );
                    List<Category> resulte = new List<Category>();
                    resulte.AddRange(data);
                    if (Config.EnableCategory)
                    {
                        foreach (var i in data)
                        {
                            resulte = Gen_Item(resulte, db, i.Id);
                        }
                    }

                    if (resulte.Any())
                    {
                            db.Category.DeleteAllOnSubmit(resulte);
                            db.SubmitChanges();
                    }
                    db.Connection.Close();
                }
            }
        }

        public List<Category> Gen_Item(List<Category> data, MithDataContext db, int id)
        {
            var tmp = db.Category.Where(w => w.Fun_Id == Fun_Id );

            if (tmp != null)
            {
                if (tmp.Any())
                {
                    foreach (var item in tmp)
                    {
                        data.Add(item);
                        data = Gen_Item(data, db, item.Id);
                    }
                    //return data;
                }
            }
            return data;
        }
        #endregion
    }
}