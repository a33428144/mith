﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using System.Data.Linq.SqlClient;
using System.IO;
using Mith.Areas.GoX.Models;
using Mith.Models;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;

namespace Mith.Areas.GoX.Models
{
    public class PrivateMessageModel
    {
        #region Class
        public class PrivateMessageShow
        {
            [Key]
            [DisplayName("編號")]
            public int Id { get; set; }
            public int ExpertId { get; set; }
            public int VolunteersId { get; set; }
            [DisplayName("達人姓名")]
            public string Expert_Name { get; set; }
            [DisplayName("留言會員")]
            public string Volunteers_Name { get; set; }
            public string Volunteers_Tel { get; set; }
            public string Volunteers_Email { get; set; }
            [DisplayName("留言內容")]
            [DataType(DataType.MultilineText)]
            public string Message { get; set; }
            [DisplayName("發話")]
            public string From { get; set; }
            [DisplayName("留言時間")]
            public DateTime? CreateTime { get; set; }
           
        }

        public string Keyword = "";
        public DateTime? CreateTime_St = null;
        public DateTime? CreateTime_End = null;

        #endregion
        public PrivateMessageModel()
        {
        }
        public void Clear()
        {
            Keyword = "";
            CreateTime_St = null;
            CreateTime_End = null;
        }

        #region Get (私訊會員)


        public List<PrivateMessageShow> Get_Data_Group(int p = 1, int take = 10, int ExpertId=0)
        {
            MithDataContext db = new MithDataContext();
            var data = Get_Group(ExpertId).OrderByDescending(o => o.Id).Skip((p - 1) * take).Take(take);
            List<PrivateMessageShow> item = new List<PrivateMessageShow>();
            item = (from i in data
                    select new PrivateMessageShow
                    {
                        Id = i.Id,
                        ExpertId=  i.ExpertId,
                        Expert_Name = i.Expert_Name,
                        VolunteersId = i.VolunteersId,
                        Volunteers_Name = i.Volunteers_Name,
                        Volunteers_Tel = i.Volunteers_Tel,
                        Volunteers_Email = i.Volunteers_Email
                    }).ToList();
            db.Connection.Close();
            return item;
        }
        public Method.Paging Get_Page_Group(int p = 1, int take = 10, int ExpertId = 0)
        {
            return Method.Get_Page(Get_Count_Group(ExpertId), p, take);
        }

        public int Get_Count_Group(int ExpertId = 0)
        {
            return Get_Group(ExpertId).Count();
        }

        private IQueryable<PrivateMessageGroup_View> Get_Group(int ExpertId = 0)
        {
            MithDataContext db = new MithDataContext();
            IQueryable<PrivateMessageGroup_View> data = db.PrivateMessageGroup_View.Where(w => w.ExpertId == ExpertId);
            if (Keyword != "")
            {
                data = data.Where(Query_Group(Keyword));
            }

            db.Connection.Close();
            return data;
        }

        private string Query_Group(string query = "")
        {
            string sql = "";

            if (query != "")
            {
                query = HttpUtility.HtmlEncode(query);
                sql = Method.SQL_Combin(sql, "Volunteers_Name", "OR", "( \"" + query + "\")", ".Contains", false);
                sql = Method.SQL_Combin(sql, "Volunteers_Email", "OR", "( \"" + query + "\")", ".Contains", false);
            }

            return sql;
        }

        #endregion

        #region Get (私訊內容)


        public List<PrivateMessageShow> Get_Data_Content(int p = 1, int take = 20, int PrivateMessageGroupId = 0)
        {
            MithDataContext db = new MithDataContext();
            var data = Get_Content(PrivateMessageGroupId).OrderByDescending(o => o.Id).Skip((p - 1) * take).Take(take);
            List<PrivateMessageShow> item = new List<PrivateMessageShow>();
            item = (from i in data
                    select new PrivateMessageShow
                    {
                        Id = i.Id,
                        Message = HttpUtility.HtmlDecode(i.Message),
                        From = i.From,
                        CreateTime = i.CreateTime
                    }).ToList();
            db.Connection.Close();
            return item;
        }
        public Method.Paging Get_Page_Content(int p = 1, int take = 10, int ExpertId = 0)
        {
            return Method.Get_Page(Get_Count_Content(ExpertId), p, take);
        }

        public int Get_Count_Content(int ExpertId = 0)
        {
            return Get_Content(ExpertId).Count();
        }

        private IQueryable<PrivateMessageContent> Get_Content(int PrivateMessageGroupId=0)
        {
            MithDataContext db = new MithDataContext();
            IQueryable<PrivateMessageContent> data = db.PrivateMessageContent.Where(w => w.PrivateMessageGroupId == PrivateMessageGroupId);
            if (Keyword != "")
            {
                data = data.Where(Query_Content(Keyword));
            }
            if (CreateTime_St != null)
            {
                data = data.Where(w => w.CreateTime >= CreateTime_St.Value);
            }
            if (CreateTime_End != null)
            {
                CreateTime_End = DateTime.Parse(CreateTime_End.Value.ToString("yyyy/MM/dd") + " 23:59:00");
                data = data.Where(w => w.CreateTime <= CreateTime_End);
            }
            db.Connection.Close();
            return data;
        }

        private string Query_Content(string query = "")
        {
            string sql = "";

            if (query != "")
            {
                query = HttpUtility.HtmlEncode(query);
                sql = Method.SQL_Combin(sql, "Volunteers_Name", "OR", "( \"" + query + "\")", ".Contains", false);
                sql = Method.SQL_Combin(sql, "Volunteers_Email", "OR", "( \"" + query + "\")", ".Contains", false);
            }

            return sql;
        }

        #endregion

        #region Delete

        public void Delete_Group(int id=0)
        {
            if (id > 0)
            {
                    MithDataContext db = new MithDataContext();
                    var Group = db.PrivateMessageGroup.FirstOrDefault(w => w.Id == id);
                    var Content = db.PrivateMessageContent.Where(w => w.PrivateMessageGroupId == id);
                    if (Group != null)
                    {
                        //先刪私訊內容
                        db.PrivateMessageContent.DeleteAllOnSubmit(Content);
                        db.SubmitChanges();

                        //再刪私訊會員
                        db.PrivateMessageGroup.DeleteOnSubmit(Group);
                        db.SubmitChanges();
                    }
                    db.Connection.Close();
            }
        }

        public void Delete_Content(int id = 0)
        {
            if (id > 0)
            {
                MithDataContext db = new MithDataContext();
                var Content = db.PrivateMessageContent.Where(w => w.Id == id);
                if (Content != null)
                {
                    //先刪私訊內容
                    db.PrivateMessageContent.DeleteAllOnSubmit(Content);
                    db.SubmitChanges();
                }
                db.Connection.Close();
            }
        }

        #endregion



    }
}