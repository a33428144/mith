﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using Mith.Models;
using System.IO;

namespace Mith.Areas.GoX.Models
{
    public class PushErrorModel
    {
        public class PushErrorShow
        {
            [Key]
            [DisplayName("編號")]
            public int Id { get; set; }
            [DisplayName("App代號")]
            public int AppId { get; set; }
            [DisplayName("App名稱")]
            public string AppName { get; set; }
            [DisplayName("Token")]
            public string DeviceId { get; set; }
            [DisplayName("標題")]
            public string Title { get; set; }
            [DisplayName("內容")]
            public string Message { get; set; }
            [DisplayName("內容編號")]
            public int? MessageId { get; set; }
            [DisplayName("錯誤訊息")]
            public string ErrorMessageShow { get; set; }
            [DisplayName("錯誤訊息")]
            public string ErrorMessage { get; set; }
            [DisplayName("建立時間")]
            public DateTime CreateTime { get; set; }
            [DisplayName("接收會員編號")]
            public int? VolunteerId { get; set; }
            [DisplayName("會員")]
            public string VolunteerName { get; set; }
            [DisplayName("會員")]
            public string VolunteerAccount { get; set; }
        }

        public PushErrorModel(int fun)
        {
            Fun_Id = fun;
        }

        public string Keyword = "";
        private int Fun_Id;
        public DateTime? create_time_start = null;
        public DateTime? create_time_end = null;
        public List<int> AppId = new List<int>();

        public void Clear()
        {
            Keyword = "";
            AppId = new List<int>();
            create_time_start = null;
            create_time_end = null;
        }

        private PushErrorShow Convert(PushError data)
        {            
            PushErrorShow item = new PushErrorShow
            {
                Id = data.Id,
                AppId = data.AppID,
                AppName = "",
                CreateTime = data.CreateTime,
                DeviceId = data.DeviceID,
                ErrorMessage = data.ErrorMessage,
                Message = data.Message,
                MessageId = data.MessageId,
                Title = data.Title,
                VolunteerId = data.VolunteerId,
                VolunteerAccount = "",
                VolunteerName = ""
            };

            if (data.VolunteerId != null)
            {
                VolunteersModel vm = new VolunteersModel();
                var Vol = vm.Get_One(data.VolunteerId.Value);
                if (Vol != null)
                {
                    item.VolunteerAccount = Vol.Email;
                    item.VolunteerName = Vol.RealName;
                }
            }

            PushAppModel pam = new PushAppModel(Fun_Id);
            var pdata = pam.Get_One(data.AppID);
            if (pdata != null)
            {
                item.AppName = pdata.AppName + "(" + pdata.OS_Name + ")";
            }
            return item;
        }

        #region get
        public PushErrorShow Get_One(int id)
        {
            try
            {
                MithDataContext db = new MithDataContext();

                var data = db.PushError.FirstOrDefault(f => f.Id == id && f.Del==false);
                if (data != null)
                {
                    return Convert(data);
                }
                db.Connection.Close();
            }
            catch { }
            return null;
        }

        public List<PushErrorShow> Get_Data(int p = 1, int take = 10)
        {
            try
            {
                MithDataContext db = new MithDataContext();

                List<PushErrorShow> item = Get().OrderByDescending(o=>o.CreateTime).Skip((p - 1) * take).Take(take).ToList();
                db.Connection.Close();
                return item;
            }
            catch { return new List<PushErrorShow>(); }
        }

        public Method.Paging Get_Page(int p = 1, int take = 10, int pages = 5)
        {
            return Method.Get_Page(Get_Count(), p, take, pages);
        }

        public int Get_Count()
        {
            try
            {
                return Get().Count();
            }
            catch { return 0; }
        }

        private IQueryable<PushErrorShow> Get()
        {
            MithDataContext db = new MithDataContext();
            IQueryable<PushErrorShow> data = db.PushError.Where(w=>w.Del==false).Select(s => new PushErrorShow
            {
                Id = s.Id,
                AppId = s.AppID,
                AppName = db.PushApp.FirstOrDefault(f => f.Id == s.AppID && f.Del == false) == null ?
                            "" : db.PushApp.FirstOrDefault(f => f.Id == s.AppID && f.Del == false).AppName + "(" + db.PushApp.FirstOrDefault(f => f.Id == s.AppID && f.Del == false).OS_Name + ")",
                Message = s.Message,
                MessageId = s.MessageId,
                DeviceId = s.DeviceID.Substring(0, 20),
                ErrorMessageShow = s.ErrorMessage.Substring(0,30),
                ErrorMessage = s.ErrorMessage,
                CreateTime = s.CreateTime,
                Title = s.Title,
                VolunteerId = s.VolunteerId,
                VolunteerAccount = db.Volunteers.FirstOrDefault(f => f.Id == s.VolunteerId) == null ?
                "" : db.Volunteers.FirstOrDefault(f => f.Id == s.VolunteerId).Email,
                VolunteerName = db.Volunteers.FirstOrDefault(f => f.Id == s.VolunteerId) == null ?
                "" : db.Volunteers.FirstOrDefault(f => f.Id == s.VolunteerId).RealName,
            });

            if (Keyword != "")
            {
                data = data.Where(Query(Keyword));
            }
            if (AppId.Any())
            {
                data = data.Where(w => AppId.Contains(w.AppId));
            }
            if (create_time_start != null)
            {
                data = data.Where(w => w.CreateTime >= create_time_start.Value);
            }
            if (create_time_end != null)
            {
                data = data.Where(w => w.CreateTime <= create_time_end.Value);
            }
            db.Connection.Close();
            return data;
        }

        private string Query(string query = "")
        {
            string sql = "";

            if (query != "")
            {
                query = HttpUtility.HtmlEncode(query);

                sql = Method.SQL_Combin(sql, "Title", "OR", "( \"" + query + "\")", ".Contains", false);
                sql = Method.SQL_Combin(sql, "Message", "OR", "( \"" + query + "\")", ".Contains", false);
                sql = Method.SQL_Combin(sql, "ErrorMessage", "OR", "( \"" + query + "\")", ".Contains", false);
            }

            return sql;
        }
        #endregion

        #region insert
        public static int Insert(int appid, string token, string title, string content, int? vid, string error, int? mid)
        {
            try
            {
                MithDataContext db = new MithDataContext();
                PushError item = new PushError
                {
                    Title = title,
                    Del = false,
                    VolunteerId = vid,
                    AppID = appid,
                    CreateTime = DateTime.UtcNow,
                    DeviceID = token,
                    ErrorMessage = error,
                    Message = content,
                    MessageId = mid
                };
                db.PushError.InsertOnSubmit(item);
                db.SubmitChanges();
                db.Connection.Close();
                return item.Id;
            }
            catch (Exception e)
            {
                ErrorRecordModel.Error_Record(-1, e.HResult.ToString(), "System/PushError/Insert", false, e.Message + "<br />\r\n<br />\r\n" + e.Source + "<br />\r\n<br />\r\n" + e.StackTrace);
                return -1;
            }
        }
        #endregion

        #region update
        
        #endregion

        #region delete
        public static void Delete(int id)
        {
            if (id > 0)
            {
                Delete(new int[] { id });
            }
        }

        public static void Delete(int[] id)
        {
            if (id != null)
            {
                if (id.Any())
                {
                    MithDataContext db = new MithDataContext();
                    var data = db.PushError.Where(w => id.Contains(w.Id));
                    if (data.Any())
                    {
                        if (Method.SiteDel)
                        {
                            db.PushError.DeleteAllOnSubmit(data);
                        }
                        else
                        {
                            db.PushError.Update(w => data.Select(s => s.Id).ToList().Contains(w.Id), u => new PushError
                            {
                                Del = true
                            });
                        }
                        db.SubmitChanges();
                    }
                    db.Connection.Close();
                }
            }
        }

        public static void DeleteAll()
        {
            MithDataContext db = new MithDataContext();
            var data = db.PushError.Where(w=>w.Del==false);
            if (data.Any())
            {
                if (Method.SiteDel)
                {
                    db.PushError.DeleteAllOnSubmit(data);
                    db.SubmitChanges();
                }
                else
                {
                    db.PushError.Update(w => data.Select(s => s.Id).ToList().Contains(w.Id), u => new PushError
                    {
                        Del = true
                    });
                }
            }
            db.Connection.Close();
        }

        public static void DeleteMonth(int m)
        {
            if (m > 0)
            {
                MithDataContext db = new MithDataContext();
                var data = db.PushError.Where(w => Math.Floor((DateTime.UtcNow - w.CreateTime).TotalDays) >= m * 30 && w.Del == false);
                if (data.Any())
                {
                    if (Method.SiteDel)
                    {
                        db.PushError.DeleteAllOnSubmit(data);
                        db.SubmitChanges();
                    }
                    else
                    {
                        db.PushError.Update(w => data.Select(s => s.Id).ToList().Contains(w.Id), u => new PushError
                        {
                            Del = true
                        });
                    }
                }
                db.Connection.Close();
            }
        }
        #endregion
    }
}