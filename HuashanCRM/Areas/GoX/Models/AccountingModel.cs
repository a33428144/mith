﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Linq;
using System.Data.Linq.SqlClient;
using Mith.Areas.GoX.Models;
using Mith.Models;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;

namespace Mith.Areas.GoX.Models
{
	public class AccountingModel
	{
		#region Class
		public class AccountingShow
		{
			[Key]
			[DisplayName("編號")]
			public int Id { get; set; }
			public int OrderId { get; set; }
			[DisplayName("訂單編號")]
			public string Order_SN { get; set; }
			[DisplayName("訂單方向")]
			public int Direction { get; set; }
			[DisplayName("App推薦")]

			public string Recommend_Email { get; set; }

			[DisplayName("對帳廠商")]
			public string Company_Name { get; set; }

			[DisplayName("金流方式")]
			public int CashFlowType { get; set; }
			public string CashFlowType_Name { get; set; }

			[DisplayName("商品編號")]
			public string Goods_SN { get; set; }
			[DisplayName("商品名稱")]
			public string Goods_Name { get; set; }
			[DisplayName("購買尺寸")]
			public string BuySize { get; set; }
			[DisplayName("購買數量")]
			public int BuyQuantity { get; set; }
			[DisplayName("商品金額")]
			public int BuyTotal { get; set; }
			[DisplayName("結算金額")]
			public int CalculateTotal { get; set; }

			[DisplayName("推薦達人")]
			public string Expert_Name { get; set; }
		}


		public class Export
		{
			[DisplayName("訂單編號")]
			public string Order_SN { get; set; }
			[DisplayName("訂單方向")]
			public string Direction { get; set; }
			[DisplayName("App推薦")]
			public string Recommend_Email { get; set; }
			[DisplayName("對帳廠商")]
			public string Company_Name { get; set; }
			[DisplayName("金流方式")]
			public int CashFlowType { get; set; }
			public string CashFlowType_Name { get; set; }
			[DisplayName("商品編號")]
			public string Goods_SN { get; set; }
			[DisplayName("商品名稱")]
			public string Goods_Name { get; set; }
			[DisplayName("購買尺寸")]
			public string BuySize { get; set; }
			[DisplayName("購買數量")]
			public int BuyQuantity { get; set; }
			[DisplayName("商品金額")]
			public int BuyTotal { get; set; }
			[DisplayName("結算金額")]
			public int CalculateTotal { get; set; }
			[DisplayName("推薦達人")]
			public string Expert_Name { get; set; }
		}

		public string Keyword = "";
		public int Search_CompanyId = 0;
		public int Search_Direction = 0;
		public int Search_CashFlow = 0;
		public int Search_AccountingReason = 0;
		public DateTime? FinalTime_St = null;
		public DateTime? FinalTime_End = null;

		#endregion

		public AccountingModel()
		{
		}
		public void Clear()
		{
			Keyword = "";
			Search_CompanyId = 0;
			Search_AccountingReason = 0;
			FinalTime_St = null;
			FinalTime_End = null;
		}

		#region Get

		private List<Accouting_Index_View> Get()
		{
			MithDataContext db = new MithDataContext();

			string StartDate = db.Other.FirstOrDefault(f => f.Id == 18).Content;
			string EndDate = db.Other.FirstOrDefault(f => f.Id == 19).Content;

			DateTime AccoutingStart = DateTime.UtcNow.AddHours(8).AddMonths(-1);
			AccoutingStart = DateTime.Parse(AccoutingStart.ToString("yyyy/MM/") + StartDate + " 00:00:00");

			DateTime AccoutingEnd = DateTime.UtcNow.AddHours(8);
			AccoutingEnd = DateTime.Parse(AccoutingEnd.ToString("yyyy/MM/") + EndDate + " 23:59:59");

			List<Accouting_Index_View> data = db.Accouting_Index_View.Where(w =>
			 w.Direction == 1 && //正物流List
			 w.LogisticsStatus == 6 && //已取貨
			 w.ReceiveGoodsTime_AddTen >= AccoutingStart && //取貨時間+10 >= 對帳開始日期
			 w.ReceiveGoodsTime_AddTen < AccoutingEnd //取貨時間+10 < 對帳開始日期
			 ).ToList();

			//逆物流List
			List<Accouting_Index_View> data2 = db.Accouting_Index_View.Where(w =>
			 w.Direction == 2 && //逆物流
			 w.CashFlowStatus == 4 && //已退款
			 w.RefundTime >= AccoutingStart && //退款時間 >= 對帳開始日期
			 w.RefundTime < AccoutingEnd //退款時間 < 對帳開始日期
			 ).ToList();

			data = data.Concat(data2).ToList();

			if (Keyword != "")
			{
				Keyword = Keyword.Replace(" ", "");
				data = data.Where(w => w.Order_SN.Contains(Keyword)).ToList();

				data = data.ToList();
				(@"Select " +
									"ExpertId, " +
									"Count (ExpertId) as [Like] " +
					"From LikeExpert " +
					"Where DATEADD(hh,8,CreateTime)  >= DATEADD(hh,8, DateAdd(m,-1,GetDate()))" +
					"And ExpertId > 0" +
					"Group By " +
									"ExpertId "
				).ToList();

			}
			if (Search_Direction != 0)
			{
				data = data.Where(w => w.Direction == Search_Direction).ToList();
			}
			if (Search_CashFlow != 0)
			{
				data = data.Where(w => w.CashFlowType == Search_CashFlow).ToList();
			}
			if (Search_CompanyId != 0)
			{
				data = data.Where(w => w.CompanyId == Search_CompanyId).ToList();
			}

			db.Connection.Close();
			return data;
		}

		public List<AccountingShow> Get_Data(int p = 1, int take = 10)
		{
			MithDataContext db = new MithDataContext();
			List<Accouting_Index_View> data = new List<Accouting_Index_View>();

			data = Get().OrderByDescending(o => o.Id).Skip((p - 1) * take).Take(take).ToList();

			List<AccountingShow> item = new List<AccountingShow>();
			item = (from i in data
							select new AccountingShow
							{
								Id = i.Id,
								OrderId = i.OrderId,
								Order_SN = i.Order_SN,
								Direction = i.Direction.Value,
								CashFlowType = i.CashFlowType.Value,
								CashFlowType_Name = i.CashFlowType_Name,
								Goods_SN = i.Goods_SN,
								Goods_Name = i.Goods_Name,
								BuySize = i.BuySize,
								BuyQuantity = i.BuyQuantity,
								BuyTotal = i.BuyTotal,
								CalculateTotal = i.CalculateTotal,
								Expert_Name = i.Expert_Name,
								Company_Name = i.Company_Name,

							}).ToList();

			db.Connection.Close();
			return item;
		}
		private string Query(string query = "")
		{
			string sql = "";

			if (query != "")
			{
				query = HttpUtility.HtmlEncode(query);
				sql = Method.SQL_Combin(sql, "Name", "OR", "( \"" + query + "\")", ".Contains", false);
				sql = Method.SQL_Combin(sql, "Account", "OR", "( \"" + query + "\")", ".Contains", false);
			}

			return sql;
		}
		public Method.Paging Get_Page(int p = 1, int take = 10)
		{
			return Method.Get_Page(Get_Count(), p, take);
		}

		public int Get_Count()
		{
			return Get().Count();
		}

		#endregion

		#region Export
		public MemoryStream Set_Excel_Mith(List<Export> data)
		{
			//標題
			List<string> header = new List<string>();
			header.Add("訂單編號");
			header.Add("物流方向");
			header.Add("金流方式");
			header.Add("訂購商品");
			header.Add("訂購尺寸");
			header.Add("訂購數量");
			header.Add("商品總計");
			header.Add("結算金額");
			header.Add("對帳廠商");
			header.Add("App推薦者");
			header.Add("推薦達人");

			MemoryStream ms = new MemoryStream();
			HSSFWorkbook workbook = new HSSFWorkbook();
			HSSFSheet sheet = (HSSFSheet)workbook.CreateSheet("sheet");
			//欄位寬度
			sheet.SetColumnWidth(3, 5000);
			//宣告HeadStyle、內容style
			HSSFCellStyle HeadStyle = null;
			HSSFCellStyle contStyle = null;
			HeadStyle = (HSSFCellStyle)workbook.CreateCellStyle();
			contStyle = (HSSFCellStyle)workbook.CreateCellStyle();
			HSSFFont font = null;
			font = (HSSFFont)workbook.CreateFont();
			HSSFCell cell = null;

			//標題粗體、黃色背景
			HeadStyle.FillForegroundColor = HSSFColor.LightYellow.Index;
			HeadStyle.FillPattern = NPOI.SS.UserModel.FillPattern.SolidForeground;
			//標題字型樣式
			font.FontHeightInPoints = 10;
			font.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
			font.FontName = "Microsoft JhengHei";
			HeadStyle.SetFont(font);
			//內容字型樣式(自動換行)
			contStyle.WrapText = true;
			//contStyle.VerticalAlignment = NPOI.SS.UserModel.VerticalAlignment.Center;
			//宣告headRow
			sheet.CreateRow(0);
			//設定head背景、粗體、內容
			foreach (var i in header)
			{
				cell = (HSSFCell)sheet.GetRow(0).CreateCell(header.IndexOf(i));
				cell.SetCellValue(i);
				cell.CellStyle = HeadStyle;
			}

			//Style - 綠色字體
			HSSFCellStyle style_green = (HSSFCellStyle)workbook.CreateCellStyle();
			HSSFFont font_green = (HSSFFont)workbook.CreateFont();
			font_green.FontHeightInPoints = 10;
			font_green.FontName = "Microsoft JhengHei";
			font_green.Color = HSSFColor.Green.Index; //字的顏色
			style_green.SetFont(font_green);

			//Style - 紅色字體
			HSSFCellStyle style_red = (HSSFCellStyle)workbook.CreateCellStyle();
			HSSFFont font_red = (HSSFFont)workbook.CreateFont();
			font_red.FontHeightInPoints = 10;
			font_red.FontName = "Microsoft JhengHei";
			font_red.Color = HSSFColor.Red.Index; //字的顏色
			style_red.SetFont(font_red);


			//資料欄位
			for (int i = 0; i < data.Count; i++)
			{

				sheet.CreateRow(i + 1);
				sheet.GetRow(i + 1).CreateCell(0).SetCellValue(data[i].Order_SN);
				sheet.GetRow(i + 1).CreateCell(1).SetCellValue(data[i].Direction);
				sheet.GetRow(i + 1).CreateCell(2).SetCellValue(data[i].CashFlowType_Name);
				sheet.GetRow(i + 1).CreateCell(3).SetCellValue("【" + data[i].Goods_SN + "】" + data[i].Goods_Name);
				sheet.GetRow(i + 1).CreateCell(4).SetCellValue(data[i].BuySize);
				sheet.GetRow(i + 1).CreateCell(5).SetCellValue(data[i].BuyQuantity);
				sheet.GetRow(i + 1).CreateCell(6).SetCellValue(data[i].BuyTotal);
				sheet.GetRow(i + 1).CreateCell(7).SetCellValue(data[i].CalculateTotal);

				if (data[i].BuyTotal >= 0)
				{
					sheet.GetRow(i + 1).GetCell(6).CellStyle = style_green;
					sheet.GetRow(i + 1).GetCell(7).CellStyle = style_green;
				}
				else
				{
					sheet.GetRow(i + 1).GetCell(6).CellStyle = style_red;
					sheet.GetRow(i + 1).GetCell(7).CellStyle = style_red;
				}

				sheet.GetRow(i + 1).CreateCell(8).SetCellValue(data[i].Company_Name);
				sheet.GetRow(i + 1).CreateCell(9).SetCellValue(data[i].Recommend_Email);
				sheet.GetRow(i + 1).CreateCell(10).SetCellValue(data[i].Expert_Name);


			}

			sheet.SetColumnWidth(header.IndexOf("訂單編號"), (short)256 * 12);
			sheet.SetColumnWidth(header.IndexOf("物流方向"), (short)256 * 10);
			sheet.SetColumnWidth(header.IndexOf("金流方式"), (short)256 * 10);
			sheet.SetColumnWidth(header.IndexOf("訂購商品"), (short)256 * 40);
			sheet.SetColumnWidth(header.IndexOf("訂購尺寸"), (short)256 * 10);
			sheet.SetColumnWidth(header.IndexOf("訂購數量"), (short)256 * 10);
			sheet.SetColumnWidth(header.IndexOf("商品總計"), (short)256 * 10);
			sheet.SetColumnWidth(header.IndexOf("結算金額"), (short)256 * 10);
			sheet.SetColumnWidth(header.IndexOf("對帳廠商"), (short)256 * 15);
			sheet.SetColumnWidth(header.IndexOf("App推薦者"), (short)256 * 15);
			sheet.SetColumnWidth(header.IndexOf("推薦達人"), (short)256 * 15);


			workbook.Write(ms);
			ms.Position = 0;
			ms.Flush();
			return ms;
		}

		public List<Export> Export_Data(AccountingModel data)
		{

			/*-------------------匯出宣告-------------------*/
			MithDataContext db = new MithDataContext();
			Export export = new Export();
			List<Export> exp = new List<Export>();
			var item = data.Get().OrderByDescending(o => o.Id);
			/*-------------------匯出宣告End-------------------*/

			foreach (var i in item)
			{
				export.Order_SN = i.Order_SN;
				//正物流
				if (i.Direction.Value == 1)
				{
					export.Direction = "正";
					export.CashFlowType_Name = i.CashFlowType_Name;
				}
				//逆物流
				else
				{
					export.Direction = "逆";
					if (i.CashFlowType == 1)
					{
						export.CashFlowType_Name = "ATM退款";

					}
					else
					{
						export.CashFlowType_Name = "信用卡刷退";
					}
					i.BuyTotal = i.BuyTotal * -1;
					i.CalculateTotal = i.CalculateTotal * -1;
				}
				export.Goods_SN = i.Goods_SN;
				export.Goods_Name = i.Goods_Name;
				export.BuySize = i.BuySize;
				export.BuyQuantity = i.BuyQuantity;
				export.BuyTotal = i.BuyTotal;
				export.CalculateTotal = i.CalculateTotal;
				export.Company_Name = i.Company_Name;
				if (!string.IsNullOrWhiteSpace(i.Recommend_Email))
				{
					export.Recommend_Email = i.Recommend_Email;
				}
				else
				{
					export.Recommend_Email = "無";
				}
				if (!string.IsNullOrWhiteSpace(i.Expert_Name))
				{
					export.Expert_Name = i.Expert_Name;
				}
				else
				{
					export.Expert_Name = "無";
				}

				exp.Add(export);
				export = new Export();
			}
			db.Connection.Close();
			return exp;
		}
		#endregion
	}
}