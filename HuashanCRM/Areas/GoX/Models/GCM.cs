﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Mith.Models;
using Newtonsoft.Json;
using System.Text;
using System.IO;
using Newtonsoft.Json.Linq;
using PushSharp;
using PushSharp.Android;
using PushSharp.Core;
using System.Configuration;
using System.Threading;

namespace Mith.Areas.GoX.Models
{
    public class GCM
    {
        private static PushApp G_app;

        public static void PushMessage(object obj)
        {
            PushModel.PushParams param = (PushModel.PushParams)obj;

            MithDataContext db = new MithDataContext();

            try
            {
                G_app = param.App;
                int[] VolunteersId = param.VolunteersId;
                int[] Msg = param.Msg;

                PushBroker push = GetPush(G_app.Password);

                IQueryable<PushMessage> push_msg = db.PushMessage;

                if (Msg != null)
                {
                    push_msg = push_msg.Where(w => Msg.Contains(w.Id));
                }
                if (VolunteersId != null)
                {
                    push_msg = push_msg.Where(w => VolunteersId.Contains(w.VolunteerId.Value));
                    var push_data = push_msg.OrderByDescending(o => o.CreateTime).Select(s => new PushModel.PushList
                    {
                        Message = s,
                        Token = db.PushToken.Where(w => w.AppID == G_app.Id && w.VolunteerId == VolunteersId[0]).ToList()
                    });
                    if (push_data.Any())
                    {
                        foreach (var i in push_data)
                        {
                            foreach (var j in i.Token)
                            {
                                Send(push, j.Token, i.Message, j.BadgeCount);
                            }
                        }
                        PushTokenModel.Delete_Repeat();
                    }

                }
                else
                {
                    var push_data = push_msg.OrderByDescending(o => o.CreateTime).Select(s => new PushModel.PushList
                    {
                        Message = s,
                        Token = db.PushToken.Where(w => w.AppID == G_app.Id ).ToList()
                    });
                    if (push_data.Any())
                    {
                        foreach (var i in push_data)
                        {
                            int jj = i.Token.Count();
                            foreach (var j in i.Token)
                            {
                                Send(push, j.Token, i.Message, j.BadgeCount);
                            }
                        }
                        PushTokenModel.Delete_Repeat();
                    }
                }
            }
            catch (Exception e)
            {
                PushErrorModel.Insert(G_app.Id, "", "", "", null,
                    e.Message + "<br />\r\n<br />\r\n" + e.Source + "<br />\r\n<br />\r\n" + e.StackTrace, null);
            }
        }

        private static PushBroker GetPush(string pw)
        {
            PushBroker push = new PushBroker();
            push.OnNotificationSent += NotificationSent;
            push.OnChannelException += ChannelException;
            push.OnServiceException += ServiceException;
            push.OnNotificationFailed += NotificationFailed;
            push.OnDeviceSubscriptionExpired += DeviceSubscriptionExpired;
            push.OnDeviceSubscriptionChanged += DeviceSubscriptionChanged;
            push.RegisterGcmService(new GcmPushChannelSettings(pw));
            return push;
        }

        private static void Send(PushBroker push, string token, PushMessage msg, int badge, bool resent = false)
        {
            try
            {
                string json = JsonConvert.SerializeObject(Get_Send_Message_Android(msg, badge));
                push.QueueNotification(new GcmNotification()
                    .ForDeviceRegistrationId(token)
                    .WithJson(json)
                    .WithTag(Get_Tag(msg, token, resent)));
            }
            catch (Exception e)
            {
                PushErrorModel.Insert(msg.AppID, token, msg.Title, msg.Content, msg.VolunteerId == null ? 0 : msg.VolunteerId.Value,
                    e.Message + "<br />\r\n<br />\r\n" + e.Source + "<br />\r\n<br />\r\n" + e.StackTrace, msg.Id);
            }
        }

        private static JObject Get_Send_Message_Android(PushMessage data, int badge)
        {
            JObject message = new JObject();
            message.Add(new JProperty("title", data.Title));
            message.Add(new JProperty("content", data.Content));
            message.Add(new JProperty("badge", 0));

            return message;
        }

        private static string Get_Tag(PushMessage data, string RID, bool resent)
        {
            JObject message = new JObject();
            message.Add(new JProperty("RS", resent));
            //message.Add(new JProperty("AID", data.AppID));
            message.Add(new JProperty("MID", data.Id));
            //message.Add(new JProperty("VID", data.VolunteerId == null ? 0 : data.VolunteerId.Value));
            return JsonConvert.SerializeObject(message);
        }

        private static void NotificationSent(object sender, INotification notification)
        {
            PushModel.ResultData rd = Get_ResultData(notification);
            PushTokenModel.Update_SendTime_ByToken(rd.Token);
            PushMessageModel.Update_Sended(rd.MessageId);
            //PushMessageModel.Delete(rd.MessageId);
        }

        private static void NotificationFailed(object sender, INotification notification, Exception notificationFailureException)
        {
            //ApplePushService temp = (ApplePushService)sender;
            //ApplePushChannelSettings temp2 = (ApplePushChannelSettings)temp.ChannelSettings;
            PushModel.ResultData rd = Get_ResultData(notification);
            if (rd.Resent == true)
            {
                //PushTokenModel.DeleteByTokenID(rd.Token);
                PushErrorModel.Insert(rd.AppId, rd.Token, rd.Title, rd.Message, rd.Vid,
                    notificationFailureException.Message + "<br />\r\n<br />\r\n" + notificationFailureException.Source + "<br />\r\n<br />\r\n" + notificationFailureException.StackTrace, rd.MessageId);
            }
            else
            {
                PushBroker push = GetPush(G_app.Password);
                Send(push, rd.Token, new PushMessage
                {
                    AppID = rd.AppId,
                    Content = rd.Message,
                    Title = rd.Title,
                    VolunteerId = rd.Vid,
                    Id = rd.MessageId
                }, rd.Badge, true);
            }
        }

        private static void DeviceSubscriptionExpired(object sender, string expiredDeviceSubscriptionId, DateTime timestamp, INotification notification)
        {
            PushModel.ResultData rd = Get_ResultData(notification);
            //PushTokenModel.DeleteByTokenID(expiredDeviceSubscriptionId);
            PushErrorModel.Insert(rd.AppId, expiredDeviceSubscriptionId, rd.Title, rd.Message, rd.Vid, "Token Expired:" + expiredDeviceSubscriptionId, rd.MessageId);
        }

        private static PushModel.ResultData Get_ResultData(object notification)
        {
            PushModel.ResultData rd = new PushModel.ResultData();
            INotification temp = (INotification)notification;
            JObject temp1 = (JObject)JsonConvert.DeserializeObject(temp.ToString());
            JObject temp2 = (JObject)JsonConvert.DeserializeObject(temp.Tag.ToString());
            rd.Token = temp1["registration_ids"].ToString();
            if (rd.Token.StartsWith("[\r\n  \""))
            {
                rd.Token = rd.Token.Replace("[\r\n  \"", "");
            }
            if (rd.Token.EndsWith("\"\r\n]"))
            {
                rd.Token = rd.Token.Replace("\"\r\n]", "");
            }
            //rd.AppId = Convert.ToInt32(temp2["AID"].ToString());
            rd.MessageId = Convert.ToInt32(temp2["MID"].ToString());
            rd.Resent = Convert.ToBoolean(temp2["RS"].ToString());
            rd.Title = temp1["data"]["title"].ToString();
            rd.Message = temp1["data"]["content"].ToString();
            //rd.Vid = Convert.ToInt32(temp2["VID"].ToString());
            return rd;
        }

        private static void ChannelException(object sender, IPushChannel channel, Exception exception)
        {
            PushErrorModel.Insert(G_app.Id, "", "", "", null,
                exception.Message + "<br />\r\n<br />\r\n" + exception.Source + "<br />\r\n<br />\r\n" + exception.StackTrace, null);
            //Console.WriteLine("Channel Exception: " + sender + " -> " + exception);
        }

        private static void ServiceException(object sender, Exception exception)
        {
            PushErrorModel.Insert(G_app.Id, "", "", "", null,
                exception.Message + "<br />\r\n<br />\r\n" + exception.Source + "<br />\r\n<br />\r\n" + exception.StackTrace, null);
            //Console.WriteLine("Channel Exception: " + sender + " -> " + exception);
        }

        private static void DeviceSubscriptionChanged(object sender, string oldSubscriptionId, string newSubscriptionId, INotification notification)
        {
            //Currently this event will only ever happen for Android GCM
            PushModel.ResultData rd = Get_ResultData(notification);
            PushTokenModel.Update_Token_ByToken(rd.AppId, oldSubscriptionId, newSubscriptionId);
            PushTokenModel.Delete_Repeat();
        }

        /*private static void ChannelDestroyed(object sender)
        {
            Console.WriteLine("Channel Destroyed for: " + sender);
        }

        private static void ChannelCreated(object sender, IPushChannel pushChannel)
        {
            Console.WriteLine("Channel Created for: " + sender);
        }*/
    }
}