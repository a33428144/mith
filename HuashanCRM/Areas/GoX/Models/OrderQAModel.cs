﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using System.Threading;
using System.Threading.Tasks;
using Mith.Areas.GoX.Models;
using Mith.Models;
using System.Data.Linq.SqlClient;
using System.IO;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using LinqToExcel;
using System.Drawing;

namespace Mith.Areas.GoX.Models
{
    public class OrderQAModel
    {
        #region Class
        public class OrderQAShow
        {
            [Key]
            [DisplayName("編號")]
            public int Id { get; set; }

            [DisplayName("訂單編號")]
            public string OrderSN { get; set; }
    
            [DisplayName("會員姓名")]
            public string Volunteers_Name { get; set; }
            [DisplayName("會員電話")]
            public string Volunteers_Tel { get; set; }
            [DisplayName("會員Email")]
            public string Volunteers_Email { get; set; }
            [DisplayName("提問標題")]
            public string MessageTitle { get; set; }
            [DisplayName("提問內容")]
            [DataType(DataType.MultilineText)]
            public string Message { get; set; }
            [DisplayName("回覆內容")]
            public string ReplyMessage { get; set; }
            [DisplayName("提問時間")]
            public DateTime? CreateTime { get; set; }
            [DisplayName("回覆時間")]
            public DateTime? ReplyTime { get; set; }
            [DisplayName("回覆人員")]
            public int ReplyUserId { get; set; }
            [DisplayName("回覆人員")]
            public string ReplyUser_Name { get; set; }
            [DisplayName("公司名稱")]
            public string Company_Name { get; set; }

        }

        public string Keyword = "";
        public int Search_Reply = 0;
        public int Search_OrderId = 0;
        public int Search_CompanyId = 0;
        public DateTime? CreateTime_St = null;
        public DateTime? CreateTime_End = null;

        #endregion

        public OrderQAModel()
        {
        }
        public void Clear()
        {
            Keyword = "";
            CreateTime_St = null;
            CreateTime_End = null;
            Search_OrderId = 0;
            Search_Reply = 0;
            Search_CompanyId = 0;
        }

        #region Get

        public OrderQAShow Get_One(int id)
        {
            MithDataContext db = new MithDataContext();
            var data = (from i in db.OrderQA_View
                        where i.Id == id
                        select new OrderQAShow
                        {
                            Id = i.Id,
                            OrderSN = i.OrderSN,
                            Volunteers_Name = i.Volunteers_Name,
                            Volunteers_Tel = i.Volunteers_Tel,
                            Volunteers_Email = i.Volunteers_Email,
                            MessageTitle = i.MessageTitle,
                            Message = HttpUtility.HtmlDecode(i.Message),
                            CreateTime = i.CreateTime,
                            ReplyUser_Name = i.ReplyUser_Name,
                            ReplyMessage = HttpUtility.HtmlDecode(i.ReplyMessage),
                            ReplyTime = i.ReplyTime
                        }).FirstOrDefault();
            db.Connection.Close();
            return data;
        }


        public List<OrderQAShow> Get_Data(int p = 1, int take = 10)
        {
            MithDataContext db = new MithDataContext();
            var data = Get();
            data = data.OrderByDescending(o => o.Id).Skip((p - 1) * take).Take(take);
            List<OrderQAShow> item = new List<OrderQAShow>();
            item = (from i in data
                    select new OrderQAShow
                    {
                        Id = i.Id,
                        OrderSN = i.OrderSN,
                        Volunteers_Name = i.Volunteers_Name,
                        Volunteers_Tel = i.Volunteers_Tel,
                        Volunteers_Email = i.Volunteers_Email,
                        MessageTitle = i.MessageTitle,
                        CreateTime = i.CreateTime,
                        ReplyMessage = HttpUtility.HtmlDecode(i.ReplyMessage),
                        ReplyTime = i.ReplyTime
                    }).ToList();
            db.Connection.Close();
            return item;
        }
        public Method.Paging Get_Page(int p = 1, int take = 10, int CompanyId = 0)
        {
            return Method.Get_Page(Get_Count(), p, take);
        }

        public int Get_Count()
        {
            return Get().Count();
        }

        private IQueryable<OrderQA_View> Get()
        {
            MithDataContext db = new MithDataContext();
            IQueryable<OrderQA_View> data = db.OrderQA_View;
            if (Search_CompanyId > 0)
            {
                data = data.Where(w => w.CompanyId == Search_CompanyId);
            }

            if (Search_OrderId > 0)
            {
                data = data.Where(w => w.OrderId == Search_OrderId);
            }
            if (Keyword != "")
            {
                data = data.Where(Query(Keyword));
            }
            if (Search_Reply != 0)
            {
                if (Search_Reply == 1)
                {
                    data = data.Where(w => w.ReplyTime != null);
                }
                else
                {
                    data = data.Where(w => w.ReplyTime == null);
                }
            }
            if (CreateTime_St != null)
            {
                data = data.Where(w => w.CreateTime >= CreateTime_St.Value);
            }
            if (CreateTime_End != null)
            {
                data = data.Where(w => w.CreateTime <= CreateTime_End.Value);
            }

            db.Connection.Close();
            return data;
        }

        private string Query(string query = "")
        {
            string sql = "";

            if (query != "")
            {
                query = HttpUtility.HtmlEncode(query);
                sql = Method.SQL_Combin(sql, "Volunteers_Name", "OR", "( \"" + query + "\")", ".Contains", false);
                sql = Method.SQL_Combin(sql, "Volunteers_Email", "OR", "( \"" + query + "\")", ".Contains", false);
            }

            return sql;
        }

        #endregion

        #region update

        public int Update(OrderQAShow item)
        {
            try
            {
                MithDataContext db = new MithDataContext();
                int Id = 0;
                var data = db.OrderQA.FirstOrDefault(f => f.Id == item.Id);
                if (data != null)
                {
                    data.ReplyMessage = HttpUtility.HtmlEncode(item.ReplyMessage);
                    data.ReplyTime = DateTime.UtcNow;
                    data.ReplyUserId = item.ReplyUserId;
                    db.SubmitChanges();
                    Id = data.Id;
                }
                db.Connection.Close();

								Task t1 = Task.Run(() =>
								{
									var Order = db.Order_API_View.FirstOrDefault(f => f.SN == item.OrderSN);

									string EmailContent = HttpUtility.HtmlDecode("您好，您的訂單提問已回覆\n訂單編號：" + Order.SN + "\n回覆廠商：" + Order.Company_Name + "\n回覆內容：" + HttpUtility.HtmlEncode(item.ReplyMessage) + "\n===================================================================\n請至MiTH App查看，若有任何問題，可連繫MiTH客服：mith@mix-with.com");

									Method.Send_Mail(Order.Volunteers_Email, "MiTH App 訂單提問已回覆 [訂單編號" + Order.SN + "]", EmailContent);
								});

                return Id;
            }
            catch { return -1; }
        }

        #endregion

        #region Delete
        public void Delete(int id = 0)
        {
            if (id > 0)
            {
                Delete(new int[] { id });
            }
        }

        public void Delete(int[] id)
        {
            if (id != null)
            {
                if (id.Any())
                {
                    MithDataContext db = new MithDataContext();

                    var data = db.OrderQA.Where(w => id.Contains(w.Id));
                    if (data.Any() && data != null)
                    {
                        db.OrderQA.DeleteAllOnSubmit(data);
                        db.SubmitChanges();
                    }

                    db.Connection.Close();
                }
            }
        }

        #endregion

        #region 下拉式選單

        /// <summary>
        /// 取得商品下拉選單
        /// </summary>
        /// <param name="GoodsId">廠商Id</param>
        /// <returns>選單</returns>
        public static List<SelectListItem> Get_Goods_SelectListItem(int GoodsId)
        {
            MithDataContext db = new MithDataContext();
            List<SelectListItem> data = new List<SelectListItem>();
            var Goods = db.Goods;
            data.Add(new SelectListItem
            {
                Selected = GoodsId == 0,
                Text = "請選擇或輸入文字",
                Value = "0"
            });
            foreach (var item in Goods)
            {
                data.AddRange(new SelectListItem
                {
                    Selected = GoodsId == item.Id,
                    Text = "【" + item.SN + "】" + item.Name,
                    Value = item.Id.ToString()
                });
            }
            db.Connection.Close();
            return data;
        }

        #endregion

    }
}