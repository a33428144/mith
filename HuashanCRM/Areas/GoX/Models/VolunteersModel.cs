﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using Mith.Models;
using System.IO;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;


namespace Mith.Areas.GoX.Models
{
    public class VolunteersModel
    {

        #region Class
        public class VolunteersShow
        {
            [Key]
            [DisplayName("編號")]
            public int Id { get; set; }

            [DisplayName("信箱")]
						[DataType(DataType.EmailAddress)]
						[Required(ErrorMessage = "必填欄位")]
            public string Email { get; set; }

            [DisplayName("密碼")]
            [DataType(DataType.Password)]
						[Required(ErrorMessage = "必填欄位")]
						[StringLength(12, MinimumLength = 6, ErrorMessage = "需介於入6-12碼")]
            public string Password { get; set; }

            [DisplayName("顯示名稱")]
						[Required(ErrorMessage = "必填欄位")]
            public string ShowName { get; set; }

            [DisplayName("真實姓名")]
						[Required(ErrorMessage = "必填欄位")]
            public string RealName { get; set; }

            [DisplayName("電話")]
            public string ContactTel { get; set; }

            [DisplayName("縣市")]
            public int CityId { get; set; }

            [DisplayName("區域")]
            public int AreaId { get; set; }

            [DisplayName("地址")]
            public string Address { get; set; }

						[DisplayName("生日")]
						public DateTime Birthday { get; set; }

						[DisplayName("身高")]
						public int Height { get; set; }

						[DisplayName("體重")]
						public int Weight { get; set; }

						[DisplayName("大頭照")]
						public string Pic { get; set; }

						[DisplayName("統一編號")]
						[StringLength(8, MinimumLength = 8, ErrorMessage = "請輸入8碼數字")]
						[DataAnnotationsExtensions.Integer(ErrorMessage = "請輸入數字")]
						public string TaxId { get; set; }

            [DisplayName("App推薦")]
            public int RecommendId { get; set; }
            [DisplayName("App推薦")]
            public string Recommend_Email { get; set; }

            [DisplayName("註冊時間")]
            public DateTime? CreateTime { get; set; }

            [DisplayName("啟用")]
            public bool Enable { get; set; }

            public string Code { get; set; }
        }

        public class VolunteersShowEdit
        {
            [Key]
            [DisplayName("編號")]
            public int Id { get; set; }

            [DisplayName("照片")]
            public string Pic { get; set; }

            [DisplayName("信箱")]
            public string Email { get; set; }

            [DisplayName("密碼")]
            [DataType(DataType.Password)]
            public string Password { get; set; }

            [DisplayName("顯示名稱")]
            public string ShowName { get; set; }

            [DisplayName("真實姓名")]
            public string RealName { get; set; }

            [DisplayName("電話")]
            public string ContactTel { get; set; }

            [DisplayName("縣市")]
            public int? CityId { get; set; }
            public string City_Name { get; set; }

            [DisplayName("區域")]
            public int? AreaId { get; set; }
            public string Area_Name { get; set; }

						[DisplayName("地址")]
						public string Address { get; set; }

						[DisplayName("身高")]
						public int? Height { get; set; }
						public string Height_Name { get; set; }

						[DisplayName("體重")]
						public int? Weight { get; set; }
						public string Weight_Name { get; set; }

						[DisplayName("統一編號")]
						public string TaxId { get; set; }

            [DisplayName("App推薦者")]
            public int RecommendId { get; set; }
            [DisplayName("App推薦者")]
            public string Recommend_Email { get; set; }

            [DisplayName("註冊時間")]
            public DateTime? CreateTime { get; set; }
            [DisplayName("更新時間")]
            public DateTime? UpdateTime { get; set; }
            [DisplayName("更新人員")]
            public int LastUser_Id { get; set; }
            [DisplayName("更新人員")]
            public string LastUser_Name { get; set; }
            [DisplayName("啟用")]
            public bool Enable { get; set; }
        }

        public class Export
        {
            [Key]
            public string Id { get; set; }
            public string RealName { get; set; }
            public string ShowName { get; set; }
            public string ContactTel { get; set; }
            public string Email { get; set; }
            public string City_Name { get; set; }
            public string Area_Name { get; set; }
            public string Address { get; set; }
            public string Height_Name { get; set; }
            public string Weight_Name { get; set; }
            public string Recommend_Email { get; set; }
            public string CreateTime { get; set; }
        }

        #endregion

        #region 初始值
        public VolunteersModel()
        {
        }
        public string Keyword = "";
        public string Sort = "update";
        public int Search_Enable = 0;
        public DateTime? CreateTime_St = null;
        public DateTime? CreateTime_End = null;

        public void Clear_Params()
        {
            Keyword = "";
            Sort = "update";
            Search_Enable = 0;
            CreateTime_St = null;
            CreateTime_End = null;
        }

        #endregion

        #region Get

        private IQueryable<Volunteers_View> Get()
        {
            MithDataContext db = new MithDataContext();
            IQueryable<Volunteers_View> data = db.Volunteers_View;

            if (Keyword != "")
            {
                data = data.Where(Query(Keyword));
            }
            if (Search_Enable != 0)
            {
                if (Search_Enable == 1)
                {
                    data = data.Where(w => w.Enable == true);
                }
                else
                {
                    data = data.Where(w => w.Enable == false);
                }
            }
            if (CreateTime_St != null)
            {
                data = data.Where(w => w.CreateTime >= CreateTime_St.Value);
            }
            if (CreateTime_End != null)
            {
                CreateTime_End = DateTime.Parse(CreateTime_End.Value.ToString("yyyy/MM/dd") + " 23:59:00");
                data = data.Where(w => w.CreateTime <= CreateTime_End);
            }

            db.Connection.Close();
            return data;
        }

        public List<VolunteersShow> Get_Data(int p = 1, int take = 10)
        {
            MithDataContext db = new MithDataContext();
            var data = Get().OrderByDescending(o => o.Id).Skip((p - 1) * take).Take(take);
            List<VolunteersShow> item = new List<VolunteersShow>();
            item = (from i in data
                    select new VolunteersShow
                    {
                        Id = i.Id,
                        RealName = i.RealName,
                        ContactTel = i.ContactTel,
                        Email = i.Email,
                        Recommend_Email = i.Recommend_Email,
                        CreateTime = i.CreateTime,
                        Enable = i.Enable,
                        Code=  i.RegisterCode
                    }).ToList();
            db.Connection.Close();
            return item;
        }

        public VolunteersShowEdit Get_One(int id)
        {
            MithDataContext db = new MithDataContext();
            var data = (from i in db.Volunteers_View
                        where i.Id == id
                        select new VolunteersShowEdit
                        {
                            Id = i.Id,
                            Pic = i.Pic,
                            Email = i.Email,
                            ShowName = i.ShowName,
                            RealName = i.RealName,
                            ContactTel = i.ContactTel,
                            City_Name = i.City_Name,
                            Area_Name = i.Area_Name,
                            Address = i.Address,
														Height = i.Height,
														Height_Name = i.Height_Name,
														Weight = i.Weight,
														Weight_Name = i.Weight_Name,
														TaxId = i.TaxId,
                            RecommendId = i.RecommendId,
                            Recommend_Email = i.Recommend_Email,
                            CreateTime = i.CreateTime,
                            UpdateTime = i.UpdateTime,
                            LastUser_Name = i.LastUser_Name,
                            Enable = i.Enable
                        }).FirstOrDefault();
            db.Connection.Close();
            return data;
        }


        public Method.Paging Get_Page(int p = 1, int take = 10, int pages = 5)
        {
            return Method.Get_Page(Get_Count(), p, take, pages);
        }

        public int Get_Count()
        {
            try
            {
                return Get().Count();
            }
            catch { return 0; }
        }



        private string Query(string query = "")
        {
            string sql = "";

            if (query != "")
            {
                query = HttpUtility.HtmlEncode(query);
                sql = Method.SQL_Combin(sql, "RealName", "OR", "( \"" + query + "\")", ".Contains", false);
                sql = Method.SQL_Combin(sql, "ContactTel", "OR", "( \"" + query + "\")", ".Contains", false);
                sql = Method.SQL_Combin(sql, "Email", "OR", "( \"" + query + "\")", ".Contains", false);
            }

            return sql;
        }
        #endregion

        #region Insert
				public int Insert(VolunteersShow item)
				{

					MithDataContext db = new MithDataContext();
					Volunteers new_item = new Volunteers
					{
						Id = item.Id,
						Type = 1, //類型 = Mith註冊
						Email = item.Email,
						Password = Method.Get_HashPassword(item.Password),
						RealName = item.RealName,
						ShowName = item.ShowName,						
						ContactTel = item.ContactTel,
						CityId = item.CityId,
						AreaId = item.AreaId,
						Address = item.Address,
						Birthday = item.Birthday,
						Height = item.Height,
						Weight = item.Weight,
						RegisterCode = Method.RandomKey(5, true, true, false, false),
						RecommendId = 0,
						Pic = item.Pic,
						PushSales = false,
						PushExpert = false,
						PushFashion = false,
						TaxId = item.TaxId,
						CreateTime = DateTime.UtcNow,
						UpdateTime = DateTime.UtcNow,
						LastUserId = 0,
						Enable = item.Enable,
					};
					db.Volunteers.InsertOnSubmit(new_item);
					db.SubmitChanges();
					db.Connection.Close();
					return new_item.Id;
				}
        #endregion

        #region Update
        public int Update(VolunteersShowEdit item)
        {
            try
            {
                MithDataContext db = new MithDataContext();
                var data = db.Volunteers.FirstOrDefault(f => f.Id == item.Id);
                if (data != null)
                {
                    data.RecommendId = item.RecommendId;
                    data.Enable = item.Enable;
                    data.UpdateTime = DateTime.UtcNow;
                    data.LastUserId = item.LastUser_Id;
                    db.SubmitChanges();
                    db.Connection.Close();
                    return data.Id;
                }
                db.Connection.Close();
                return -1;
            }
            catch { return -1; }
        }

        #endregion

        #region Delete

        public void Delete(int Id, HttpServerUtilityBase Server)
        {
            if (Id > 0)
            {
                MithDataContext db = new MithDataContext();
                var data = db.Volunteers.Where(w => w.Id == Id);
                if (data.Any())
                {
                    db.Volunteers.DeleteAllOnSubmit(data);

                    db.SubmitChanges();
                }
                db.Connection.Close();
            }
        }
        #endregion

        #region Export
        public MemoryStream Set_Excel(List<Export> data)
        {
            //標題
            List<string> header = new List<string>();
            header.Add("真實姓名");
            header.Add("顯示名稱");
            header.Add("電話");
            header.Add("信箱");
            header.Add("地址");
            header.Add("身高");
            header.Add("體重");
            header.Add("App推薦者");
            header.Add("註冊時間");

            MemoryStream ms = new MemoryStream();
            HSSFWorkbook workbook = new HSSFWorkbook();
            HSSFSheet sheet = (HSSFSheet)workbook.CreateSheet("sheet");

            //宣告headStyle、內容style
            HSSFCellStyle headStyle = null;
            HSSFCellStyle contStyle = null;
            headStyle = (HSSFCellStyle)workbook.CreateCellStyle();
            contStyle = (HSSFCellStyle)workbook.CreateCellStyle();
            HSSFFont font = null;
            font = (HSSFFont)workbook.CreateFont();
            HSSFCell cell = null;

            //標題粗體、黃色背景
            headStyle.FillForegroundColor = HSSFColor.LightYellow.Index;
            headStyle.FillPattern = NPOI.SS.UserModel.FillPattern.SolidForeground;
            //標題字型樣式
            font.FontHeightInPoints = 10;
            font.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
            font.FontName = "Microsoft JhengHei";
            headStyle.SetFont(font);
            //內容字型樣式(自動換行)
            contStyle.WrapText = true;
            //contStyle.VerticalAlignment = NPOI.SS.UserModel.VerticalAlignment.Center;
            //宣告headRow
            sheet.CreateRow(0);
            //設定head背景、粗體、內容
            foreach (var i in header)
            {
                cell = (HSSFCell)sheet.GetRow(0).CreateCell(header.IndexOf(i));
                cell.SetCellValue(i);
                cell.CellStyle = headStyle;
            }
            //資料欄位
            for (int i = 0; i < data.Count; i++)
            {
                sheet.CreateRow(i + 1);
                sheet.GetRow(i + 1).CreateCell(0).SetCellValue(data[i].RealName);
                sheet.GetRow(i + 1).CreateCell(1).SetCellValue(data[i].ShowName);
                sheet.GetRow(i + 1).CreateCell(2).SetCellValue(data[i].ContactTel);
                sheet.GetRow(i + 1).CreateCell(3).SetCellValue(data[i].Email);
                sheet.GetRow(i + 1).CreateCell(4).SetCellValue(data[i].City_Name + data[i].Area_Name + data[i].Address);
                sheet.GetRow(i + 1).CreateCell(5).SetCellValue(data[i].Height_Name);
                sheet.GetRow(i + 1).CreateCell(6).SetCellValue(data[i].Weight_Name);
                sheet.GetRow(i + 1).CreateCell(7).SetCellValue(data[i].Recommend_Email);
                sheet.GetRow(i + 1).CreateCell(8).SetCellValue(data[i].CreateTime);
            }

						sheet.SetColumnWidth(header.IndexOf("真實姓名"), (short)256 * 10);
						sheet.SetColumnWidth(header.IndexOf("顯示名稱"), (short)256 * 10);
						sheet.SetColumnWidth(header.IndexOf("電話"), (short)256 * 12);
						sheet.SetColumnWidth(header.IndexOf("信箱"), (short)256 * 25);
						sheet.SetColumnWidth(header.IndexOf("地址"), (short)256 * 25);
						sheet.SetColumnWidth(header.IndexOf("身高"), (short)256 * 12);
						sheet.SetColumnWidth(header.IndexOf("體重"), (short)256 * 12);
						sheet.SetColumnWidth(header.IndexOf("App推薦者"), (short)256 * 25);
						sheet.SetColumnWidth(header.IndexOf("註冊時間"), (short)256 * 15);

            workbook.Write(ms);
            ms.Position = 0;
            ms.Flush();
            return ms;
        }

        public List<Export> Export_Data(VolunteersModel data)
        {

            /*-------------------匯出宣告-------------------*/
            MithDataContext db = new MithDataContext();
            Export export = new Export();
            List<Export> exp = new List<Export>();
            var item = data.Get().OrderByDescending(o => o.Id);
            /*-------------------匯出宣告End-------------------*/

            foreach (var i in item)
            {
							export.RealName = i.RealName;
                export.ShowName = i.ShowName;
                export.ContactTel = i.ContactTel;
                export.Email = i.Email;
                export.City_Name = i.City_Name;
                export.Area_Name = i.Area_Name;
                export.Address = i.Address;
                export.Height_Name = i.Height_Name;
                export.Weight_Name = i.Weight_Name;
                export.Recommend_Email = i.Recommend_Email;
                export.CreateTime = i.CreateTime.Value.ToString("yyyy/MM/dd HH:mm");
                exp.Add(export);
                export = new Export();
            }
            db.Connection.Close();
            return exp;
        }
        #endregion
    }
}