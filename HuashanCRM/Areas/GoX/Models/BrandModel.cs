﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using Mith.Areas.GoX.Models;
using Mith.Models;
using System.Data.Linq.SqlClient;
using System.IO;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;

namespace Mith.Areas.GoX.Models
{
    public class BrandModel
    {
        #region Class
        public class BrandShow
        {
            [Key]
            [DisplayName("編號")]
            public int Id { get; set; }
            [DisplayName("名稱")]
            [Required(ErrorMessage = "必填欄位")]
            public string Name { get; set; }
            [DisplayName("廠商")]
            public int? CompanyId { get; set; }
            [DisplayName("廠商")]
            public string Company_Name { get; set; }
            [DisplayName("帳號")]
            public string Account { get; set; }
            [DisplayName("尺寸表")]
            [Required(ErrorMessage = "必填欄位")]
            public string Pic_Size { get; set; }
            [DisplayName("建立時間")]
            public DateTime? CreateTime { get; set; }
            [DisplayName("更新時間")]
            public DateTime? UpdateTime { get; set; }
            [DisplayName("更新人員")]
            public int LastUserId { get; set; }
            [DisplayName("更新人員")]
            public string LastUser_Name { get; set; }

            public int UseBrand { get; set; }
        }

        public class BrandShowEdit
        {
            [Key]
            [DisplayName("編號")]
            public int Id { get; set; }
            [DisplayName("名稱")]
            [Required(ErrorMessage = "必填欄位")]
            public string Name { get; set; }
            [DisplayName("廠商")]
            public int? CompanyId { get; set; }
            [DisplayName("廠商")]
            public string Company_Name { get; set; }
            [DisplayName("尺寸表")]
            public string Pic_Size { get; set; }
            [DisplayName("建立時間")]
            public DateTime? CreateTime { get; set; }
            [DisplayName("更新時間")]
            public DateTime? UpdateTime { get; set; }
            [DisplayName("更新人員")]
            public int LastUserId { get; set; }
            [DisplayName("更新人員")]
            public string LastUser_Name { get; set; }
        }

        public class Export
        {
  
        }

        public string Keyword = "";
        public int Search_ComapnyId = 0;
        public DateTime? CreateTime_St = null;
        public DateTime? CreateTime_End = null;

        #endregion

        public BrandModel()
        {
        }
        public void Clear()
        {
            Keyword = "";
            Search_ComapnyId = 0;
            CreateTime_St = null;
            CreateTime_End = null;
        }

        #region Get

        public BrandShowEdit Get_One(int id)
        {
            MithDataContext db = new MithDataContext();
            var data = (from i in db.Brand_View
                        where i.Id == id
                        select new BrandShowEdit
                        {
                            Id = i.Id,
                            Name = i.Name,
                            CompanyId = i.CompanyId,
                            Pic_Size = i.Pic_Size,
                            CreateTime = i.CreateTime,
                            UpdateTime = i.UpdateTime,
                            LastUser_Name = i.LastUser_Name,
                        }).FirstOrDefault();
            db.Connection.Close();
            return data;
        }

        public List<BrandShow> Get_Data(int p = 1, int take = 10)
        {
            MithDataContext db = new MithDataContext();
            var data = Get().OrderByDescending(o => o.Id).Skip((p - 1) * take).Take(take);
            List<BrandShow> item = new List<BrandShow>();
            item = (from i in data
                    select new BrandShow
                    {
                        Id = i.Id,
                        Name = i.Name,
                        Company_Name = i.Company_Name,
                        Account = i.Account,
                        Pic_Size = i.Pic_Size,
                        CreateTime = i.CreateTime.Value,
                        UseBrand = Get_UseBrand_Count(i.Id)
                    }).ToList();
            db.Connection.Close();
            return item;
        }
        public Method.Paging Get_Page(int p = 1, int take = 10)
        {
            return Method.Get_Page(Get_Count(), p, take);
        }

        public int Get_Count()
        {
            return Get().Count();
        }

        private IQueryable<Brand_View> Get()
        {
            MithDataContext db = new MithDataContext();
            IQueryable<Brand_View> data = db.Brand_View;
            if (Keyword != "")
            {
                data = data.Where(Query(Keyword));
            }
            if (Search_ComapnyId != 0)
            {
                data = data.Where(w => w.CompanyId == Search_ComapnyId);
            }
            if (CreateTime_St != null)
            {
                data = data.Where(w => w.CreateTime >= CreateTime_St.Value);
            }
            if (CreateTime_End != null)
            {
                CreateTime_End = DateTime.Parse(CreateTime_End.Value.ToString("yyyy/MM/dd") + " 23:59:00");
                data = data.Where(w => w.CreateTime <= CreateTime_End);
            }
            db.Connection.Close();
            return data;
        }

        private string Query(string query = "")
        {
            string sql = "";

            if (query != "")
            {
                query = HttpUtility.HtmlEncode(query);
                sql = Method.SQL_Combin(sql, "Name", "OR", "( \"" + query + "\")", ".Contains", false);
                sql = Method.SQL_Combin(sql, "Account", "OR", "( \"" + query + "\")", ".Contains", false);
            }

            return sql;
        }

        /// <summary>
        /// 目前使用此品牌的商品數量
        /// </summary>
        /// <param name="BrandId">品牌Id</param>
        /// <returns>數量</returns>
        private int Get_UseBrand_Count(int BrandId)
        {
            MithDataContext db = new MithDataContext();

            try
            {
                int count = db.Goods.Where(w => w.BrandId == BrandId).Count();
                return count;
            }
            catch
            {
                return 0;
            }
            finally
            {
                db.Connection.Close();
            }
        }

        #endregion

        #region insert
        public int Insert(BrandShow item)
        {

            MithDataContext db = new MithDataContext();
            Brand new_item = new Brand
            {
                Id = item.Id,
                Name = item.Name,
                CompanyId = item.CompanyId.Value,
                Pic_Size = item.Pic_Size,
                CreateTime = DateTime.UtcNow,
                UpdateTime = DateTime.UtcNow,
                LastUserId = 0
            };
            db.Brand.InsertOnSubmit(new_item);
            db.SubmitChanges();
            db.Connection.Close();
            return new_item.Id;
        }

        #endregion

        #region update

        public int Update(BrandShowEdit item)
        {
            try
            {
                MithDataContext db = new MithDataContext();
                int Id = 0;
                var data = db.Brand.FirstOrDefault(f => f.Id == item.Id);
                if (data != null)
                {
                    data.Name = item.Name;
                    data.CompanyId = item.CompanyId.Value;
                    data.Pic_Size = item.Pic_Size;
                    data.UpdateTime = DateTime.UtcNow;
                    data.LastUserId = item.LastUserId;
                    db.SubmitChanges();
                    Id = data.Id;
                }
                db.Connection.Close();
                return Id;
            }
            catch { return -1; }
        }


        #endregion

        #region Delete
        public void Delete(int id = 0)
        {
            if (id > 0)
            {
                Delete(new int[] { id });
            }
        }

        public void Delete(int[] id)
        {
            if (id != null)
            {
                if (id.Any())
                {
                    MithDataContext db = new MithDataContext();
                    var data = db.Brand.Where(w => id.Contains(w.Id));
                    if (data.Any())
                    {
                        db.Brand.DeleteAllOnSubmit(data);

                        db.SubmitChanges();
                    }
                    db.Connection.Close();
                }
            }
        }

        #endregion


    }
}