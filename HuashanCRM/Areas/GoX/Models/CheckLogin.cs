﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Mith.Areas.GoX.Models;
using Mith.Models;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;

namespace Mith.Areas.GoX.Models
{
    public class CheckLogin
    {
        /*public enum Competence
        {
            Read,
            Insert,
            Update,
            Delete,
            Admin
        }*/

        public CheckLogin()
        { }

        public static bool CheckPermission(HttpContextBase httpContext, Competence Com, int fun_Id)
        {
            if (httpContext == null)
                throw new ArgumentNullException("httpContext");

            try
            {
                if (httpContext.Request.Cookies[Method.CookieName_Admin] == null)
                {
                    return false;
                }

                HttpCookie hc = httpContext.Request.Cookies[Method.CookieName_Admin];

                string data = "";
                FormsAuthenticationTicket authTicket = Method.Get_Cookie_Ticket_Admin(httpContext.Request.Cookies);

                if (authTicket == null)
                {
                    data = Method.AES(false, hc.Value, Method.AES_Key);
                    if (data == "")
                    {
                        return false;
                    }
                }
                else
                {
                    if (authTicket.Expired)
                        return false;
                    data = authTicket.UserData;
                }

                string[] result = data.Split(',');
                if (result.Count() != 3)
                {
                    return false;
                }

                string account = result[0];
                string name = result[1];
                string password = result[2];

                var mm = UserProfileModel.Login_Get_One(account);

                if (mm == null)
                {
                    return false;
                }

                if (mm.Password != password)
                {
                    httpContext.Session.RemoveAll();
                    hc.Expires = DateTime.Now.AddDays(-1);
                    httpContext.Response.SetCookie(hc);
                    return false;
                }
                /*if (mm.Id == 0)
                    return true;*/

                if (httpContext.Session.IsNewSession)
                {
                    UserProfileModel.Update_LoginTime(mm.Id);
                    LoginRecordModel.Login_Record(mm.Account, httpContext.Request, true);
                }
                else
                {
                    /*if ((hc.Expires - DateTime.Now).TotalMinutes <= 60)
                    {
                        LoginCookie(httpContext, hc);
                    }*/
                }

                if (Method.Get_Session(httpContext.Session, Method.SessionUserAccount_Admin) == "")
                {
                    LoginSession(httpContext, mm);
                }

                bool pass = Get_CompetenceCheck(Com, fun_Id, mm.LevelId);
                return pass;
            }
            catch { return false; }
        }

        public static bool CheckPermission(HttpContextBase httpContext, Competence Com, string function)
        {
            return CheckPermission(httpContext, Com, Get_FunctionId(function));
        }

        public static bool Get_CompetenceCheck(Competence c, int fun_Id, int user_level)
        {
            MithDataContext db = new MithDataContext();
            var ct = db.CompetenceTable.FirstOrDefault(w => w.FunctionId == fun_Id && w.UserLevelId == user_level);
            db.Connection.Close();
            HttpContext.Current.Session["Insert"] = ct.Insert;
            HttpContext.Current.Session["Update"] = ct.Update;
            HttpContext.Current.Session["Delete"] = ct.Delete;
            switch (c)
            {
                case Competence.Read:
                    return ct.Read;

                case Competence.Insert:
                    return ct.Insert;

                case Competence.Update:
                    return ct.Update;

                case Competence.Delete:
                    return ct.Delete;

                default:
                    return false;
            }
        }

        public static int Get_FunctionId(string fun)
        {
            MithDataContext db = new MithDataContext();
            var data = db.Function.FirstOrDefault(f => f.Name == fun);
            db.Connection.Close();
            return data == null ? 0 : data.Id;
        }

        private static void LoginSession(HttpContextBase httpContext, UserProfileModel.UserProfileShow item)
        {
            httpContext.Session.RemoveAll();

            httpContext.Session.Add(Method.SessionUserId_Admin, item.Id);
            httpContext.Session.Add(Method.SessionUserAccount_Admin, item.Account);
            httpContext.Session.Add(Method.SessionUserName_Admin, item.Name);
            httpContext.Session.Add(Method.SessionLevel_Admin, item.LevelId);
            httpContext.Session.Add(Method.SessionLevelName_Admin, item.Level_Name);
            httpContext.Session.Add(Method.SessionUserAccount_Admin, item.Enable);
        }
    }
}