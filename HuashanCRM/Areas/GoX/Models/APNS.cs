﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Mith.Models;
using Newtonsoft.Json;
using System.Text;
using System.IO;
using Newtonsoft.Json.Linq;
using PushSharp;
using PushSharp.Apple;
using PushSharp.Core;
using System.Configuration;

namespace Mith.Areas.GoX.Models
{
    public class APNS
    {
        private static PushApp G_app;

        private static HttpServerUtilityBase Server;

        public static void PushMessage(object obj)
        {
            PushModel.PushParams param = (PushModel.PushParams)obj;

            MithDataContext db = new MithDataContext();
            Server = param.Server;

            try
            {
                G_app = param.App;
                int[] VolunteersId = param.VolunteersId;
                int[] Msg = param.Msg;

                PushBroker push = GetPush(G_app.P12_Path, G_app.Password);

                IQueryable<PushMessage> push_msg = db.PushMessage;
                if (Msg != null)
                {
                    //push_msg = push_msg.Where(w => Msg.Contains(w.Id));

                    push_msg = push_msg.Where(w => w.Id == Msg[0]);
                }
                if (VolunteersId != null)
                {
                    push_msg = push_msg.Where(w => VolunteersId.Contains(w.VolunteerId.Value));
                    var push_data = push_msg.OrderByDescending(o => o.CreateTime).Select(s => new PushModel.PushList
                    {
                        Message = s,
                        Token = db.PushToken.Where(w => w.AppID == G_app.Id && w.VolunteerId == VolunteersId[0]).ToList()
                    });
                    if (push_data.Any())
                    {
                        foreach (var i in push_data)
                        {
                            foreach (var j in i.Token)
                            {
                                Send(push, j.Token, i.Message, j.BadgeCount);
                            }
                        }
                        PushTokenModel.Delete_Repeat();
                    }
                }
                else
                {
                    var push_data = push_msg.OrderByDescending(o => o.CreateTime).Select(s => new PushModel.PushList
                    {
                        Message = s,
                        Token = db.PushToken.Where(w => w.AppID == G_app.Id).ToList()
                    });

                    if (push_data.Any())
                    {
                        foreach (var i in push_data)
                        {
                            foreach (var j in i.Token)
                            {
                                Send(push, j.Token, i.Message, j.BadgeCount);
                            }
                        }
                        PushTokenModel.Delete_Repeat();
                    }
                }

            }
            catch (Exception e)
            {
                PushErrorModel.Insert(G_app.Id, "", "", "", null,
                    e.Message + "<br />\r\n<br />\r\n" + e.Source + "<br />\r\n<br />\r\n" + e.StackTrace, null);
            }
        }

        private static PushBroker GetPush(string path, string pw)
        {
            var apple = Method.FileToByte(Server.MapPath("~" + path));
            PushBroker push = new PushBroker();
            push.OnNotificationSent += NotificationSent;
            push.OnChannelException += ChannelException;
            push.OnServiceException += ServiceException;
            push.OnNotificationFailed += NotificationFailed;
            push.OnDeviceSubscriptionExpired += DeviceSubscriptionExpired;
            push.RegisterAppleService(new ApplePushChannelSettings(!G_app.Test, apple, pw));
            return push;
        }

        private static void Send(PushBroker push, string token, PushMessage msg, int badge, bool resent = false)
        {
            try
            {
                push.QueueNotification(new AppleNotification()
                    .ForDeviceToken(token)
                    .WithAlert(msg.Content)
                    .WithCustomItem("TK", token)
                    .WithCustomItem("RS", resent)
                    //.WithCustomItem("VID", msg.VolunteerId == null ? 0 : msg.VolunteerId.Value)
                    //.WithCustomItem("AID", msg.AppID)
                    .WithCustomItem("MID", msg.Id)
                    .WithSound("default").WithBadge(0));
            }
            catch (Exception e)
            {
                PushErrorModel.Insert(msg.AppID, token, "", msg.Content, msg.VolunteerId == null ? 0 : msg.VolunteerId.Value,
                    e.Message + "<br />\r\n<br />\r\n" + e.Source + "<br />\r\n<br />\r\n" + e.StackTrace, msg.Id);
            }
        }

        private static void NotificationSent(object sender, INotification notification)
        {
            PushModel.ResultData rd = Get_ResultData(notification);
            PushTokenModel.Update_SendTime_ByToken(rd.Token);
            PushMessageModel.Update_Sended(rd.MessageId);
            //PushMessageModel.Delete(rd.MessageId);
        }

        private static void NotificationFailed(object sender, INotification notification, Exception notificationFailureException)
        {
            //ApplePushService temp = (ApplePushService)sender;
            //ApplePushChannelSettings temp2 = (ApplePushChannelSettings)temp.ChannelSettings;
            PushModel.ResultData rd = Get_ResultData(notification);
            if (rd.Resent)
            {
                if (!notificationFailureException.Message.Contains("Exception of type 'PushSharp.Apple.NotificationFailureException' was thrown.") || !notificationFailureException.Message.Contains("已發生類型 'PushSharp.Apple.NotificationFailureException' 的例外狀況。"))
                {
                    // PushTokenModel.DeleteByTokenID(rd.Token);
                    PushErrorModel.Insert(rd.AppId, rd.Token, "", rd.Message, rd.Vid,
                        notificationFailureException.Message + "<br />\r\n<br />\r\n" + notificationFailureException.Source + "<br />\r\n<br />\r\n" + notificationFailureException.StackTrace, rd.MessageId);
                }
            }
            else
            {
                PushBroker push = GetPush(G_app.P12_Path, G_app.Password);
                Send(push, rd.Token, new PushMessage
                {
                    AppID = rd.AppId,
                    Content = rd.Message,
                    Title = rd.Title,
                    VolunteerId = rd.Vid,
                    Id = rd.MessageId
                }, rd.Badge, true);
            }
        }

        private static void DeviceSubscriptionExpired(object sender, string expiredDeviceSubscriptionId, DateTime timestamp, INotification notification)
        {
            PushModel.ResultData rd = Get_ResultData(notification);
            //PushTokenModel.DeleteByTokenID(expiredDeviceSubscriptionId);
            PushErrorModel.Insert(rd.AppId, expiredDeviceSubscriptionId, "", rd.Message, rd.Vid, "Token Expired:" + expiredDeviceSubscriptionId, rd.MessageId);
        }

        private static PushModel.ResultData Get_ResultData(INotification notification)
        {
            PushModel.ResultData rd = new PushModel.ResultData();
            JObject temp = (JObject)JsonConvert.DeserializeObject(notification.ToString());
            rd.Token = temp["TK"].ToString();
            //rd.Vid = Convert.ToInt32(temp["VID"].ToString());
            //rd.MessageId = temp["MID"].ToString();
            //rd.AppId = Convert.ToInt32(temp["AID"].ToString());
            rd.MessageId = Convert.ToInt32(temp["MID"].ToString());
            rd.Message = ((JObject)temp["aps"])["alert"].ToString();
            rd.Resent = Convert.ToBoolean(temp["RS"].ToString());
            rd.Badge = Convert.ToInt32(((JObject)temp["aps"])["badge"].ToString());
            return rd;
        }

        private static void ChannelException(object sender, IPushChannel channel, Exception exception)
        {
            PushErrorModel.Insert(G_app.Id, "", "", "", null,
                exception.Message + "<br />\r\n<br />\r\n" + exception.Source + "<br />\r\n<br />\r\n" + exception.StackTrace, null);
            //Console.WriteLine("Channel Exception: " + sender + " -> " + exception);
        }

        private static void ServiceException(object sender, Exception exception)
        {
            PushErrorModel.Insert(G_app.Id, "", "", "", null,
                exception.Message + "<br />\r\n<br />\r\n" + exception.Source + "<br />\r\n<br />\r\n" + exception.StackTrace, null);
            //Console.WriteLine("Channel Exception: " + sender + " -> " + exception);
        }

        /*private static void DeviceSubscriptionChanged(object sender, string oldSubscriptionId, string newSubscriptionId, INotification notification)
        {
            //Currently this event will only ever happen for Android GCM
            Console.WriteLine("Device Registration Changed:  Old-> " + oldSubscriptionId + "  New-> " + newSubscriptionId + " -> " + notification);
        }
        
        private static void ChannelDestroyed(object sender)
        {
            Console.WriteLine("Channel Destroyed for: " + sender);
        }

        private static void ChannelCreated(object sender, IPushChannel pushChannel)
        {
            Console.WriteLine("Channel Created for: " + sender);
        }*/
    }
}