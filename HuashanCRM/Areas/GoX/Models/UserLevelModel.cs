﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using Mith.Areas.GoX.Models;
using System.Web.Security;
using Mith.Models;
using System.Data.Linq.SqlClient;

namespace Mith.Areas.GoX.Models
{
    public class UserLevelModel
    {
        #region Class
        public class UserLevelShow
        {
            [Key]
            [DisplayName("編號")]
            public int Id { get; set; }
            [DisplayName("角色")]
            [Required(ErrorMessage = "必填欄位")]
            public int Role { get; set; }
            [DisplayName("角色")]
            public string Role_Name { get; set; }
            [DisplayName("廠商")]
            public int? CompanyId { get; set; }
            [DisplayName("所屬廠商")]
            public string Company_Name { get; set; }
            [DisplayName("權限")]
            [Required(ErrorMessage = "必填欄位")]
            public int LevelId { get; set; }
            [DisplayName("權限")]
            public string Level_Name { get; set; }
            [DisplayName("帳號")]
            [Required(ErrorMessage = "必填欄位")]
            public string Account { get; set; }
            [DisplayName("名稱")]
            [Required(ErrorMessage = "必填欄位")]
            public string Name { get; set; }
            [DisplayName("密碼(若不改密碼請留空)")]
            [Required(ErrorMessage = "必填欄位")]
            public string Password { get; set; }
            [DisplayName("建立時間")]
            public DateTime? CreateTime { get; set; }
            [DisplayName("更新時間")]
            public DateTime? UpdateTime { get; set; }
            [DisplayName("啟用")]
            public bool Enable { get; set; }
            [DisplayName("使用中此權限")]
            public int UseLevel { get; set; }
        }

        public class FunctionCompetenceShow
        {
            [Key]
            [DisplayName("編號")]
            public int Id { get; set; }
            public string Menu { get; set; }
            [DisplayName("功能名稱")]
            public string Name { get; set; }
            [DisplayName("讀取")]
            public bool Read { get; set; }
            [DisplayName("新增")]
            public bool Insert { get; set; }
            [DisplayName("編輯")]
            public bool Update { get; set; }
            [DisplayName("刪除")]
            public bool Delete { get; set; }
        }

        public string Keyword = "";
        public int Search_Role = 0;
        public int Search_CompanyId = 0;
        public DateTime? CreateTime_St = null;
        public DateTime? CreateTime_End = null;
        #endregion

        public UserLevelModel()
        {
        }

        public void Clear()
        {
            Keyword = "";
            Search_Role = 0;
            CreateTime_St = null;
            CreateTime_End = null;
        }

        #region Get
        private IQueryable<UserLevel_View> Get()
        {

            MithDataContext db = new MithDataContext();
            IQueryable<UserLevel_View> data = db.UserLevel_View.Where(w => w.Id != 3);
            if (Keyword != "")
            {
                data = data.Where(Query(Keyword));
            }
            if (Search_Role != 0)
            {
                data = data.Where(w => w.Role == Search_Role);
            }
            if (Search_CompanyId > 0)
            {
                data = data.Where(w => w.CompanyId == Search_CompanyId);
            }
            if (CreateTime_St != null)
            {
                data = data.Where(w => w.CreateTime >= CreateTime_St.Value);
            }
            if (CreateTime_End != null)
            {
                CreateTime_End = DateTime.Parse(CreateTime_End.Value.ToString("yyyy/MM/dd") + " 23:59:00");
                data = data.Where(w => w.CreateTime <= CreateTime_End);
            }
            db.Connection.Close();
            return data;
        }

        public List<UserLevelShow> Get_Data( int p = 1, int take = 10)
        {
            MithDataContext db = new MithDataContext();
            var Category = db.Category.Where(w => w.Group == "Role");
            var data = Get().OrderByDescending(o => o.Id).Skip((p - 1) * take).Take(take);
            List<UserLevelShow> item = new List<UserLevelShow>();
            item = (from i in data
                    select new UserLevelShow
                    {
                        Id = i.Id,
                        Name = i.Name,
                        Role = i.Role,
                        Role_Name = i.Role_Name,
                        CreateTime = i.CreateTime,
                        UpdateTime = i.UpdateTime,
                        UseLevel = Get_UseLevel_Count(i.Id),
                        Company_Name = i.Company_Name
                    }).ToList();
            db.Connection.Close();
            return item;
        }

        public Method.Paging Get_Page(int p = 1, int take = 10)
        {
            return Method.Get_Page(Get_Count(), p, take);
        }

        public int Get_Count()
        {
            return Get().Count();
        }



        private string Query(string query = "")
        {
            string sql = "";

            if (query != "")
            {
                query = HttpUtility.HtmlEncode(query);
                sql = Method.SQL_Combin(sql, "Name", "OR", "( \"" + query + "\")", ".Contains", false);
            }

            return sql;
        }

        public List<FunctionCompetenceShow> Get_RoleFunction(int id = 0, int Role = 1)
        {
            MithDataContext db = new MithDataContext();
            List<FunctionCompetenceShow> fcs = new List<FunctionCompetenceShow>();
            var data = db.Function.Where(w => w.Role.IndexOf(Role.ToString()) >= 0).OrderBy(o => o.Id);

            foreach (var i in data)
            {
                var com = db.CompetenceTable.FirstOrDefault(f => f.FunctionId == i.Id && f.UserLevelId == id);
                if (com == null)
                {
                    fcs.Add(new FunctionCompetenceShow
                    {
                        Id = i.Id,
                        Menu = i.Menu,
                        Name = i.Name,
                        Read = false,
                        Insert = false,
                        Update = false,
                        Delete = false,
                    });
                }
                else
                {
                    fcs.Add(new FunctionCompetenceShow
                    {
                        Id = i.Id,
                        Menu = i.Menu,
                        Name = i.Name,
                        Read = com.Read,
                        Insert = com.Insert,
                        Update = com.Update,
                        Delete = com.Delete,
                    });
                }
            }
            db.Connection.Close();
            return fcs;
        }

        public List<FunctionCompetenceShow> Get_MithFunction(int id = 0)
        {
            MithDataContext db = new MithDataContext();
            List<FunctionCompetenceShow> fcs = new List<FunctionCompetenceShow>();
            var data = db.Function.Where(w => w.Role.IndexOf("1") >= 0 && w.Id != 24 && w.Id != 25 && w.Id != 26 && w.Id != 27).OrderBy(o => o.Sort);

            foreach (var i in data)
            {
                var com = db.CompetenceTable.FirstOrDefault(f => f.FunctionId == i.Id && f.UserLevelId == id);
                if (com == null)
                {
                    fcs.Add(new FunctionCompetenceShow
                    {
                        Id = i.Id,
                        Menu = i.Menu,
                        Name = i.Name.Replace("文章資料1", "文章資料").Replace("文章資料2", "文章資料"),
                        Read = false,
                        Insert = false,
                        Update = false,
                        Delete = false,
                    });
                }
                else
                {
                    fcs.Add(new FunctionCompetenceShow
                    {
                        Id = i.Id,
                        Menu = i.Menu,
                        Name = i.Name.Replace("文章資料1", "文章資料").Replace("文章資料2", "文章資料"),
                        Read = com.Read,
                        Insert = com.Insert,
                        Update = com.Update,
                        Delete = com.Delete,
                    });
                }
            }
            db.Connection.Close();
            return fcs;
        }

        public List<FunctionCompetenceShow> Get_CompanyFunction(int id = 0)
        {
            MithDataContext db = new MithDataContext();
            List<FunctionCompetenceShow> fcs = new List<FunctionCompetenceShow>();
            var data = db.Function.Where(w => w.Role.IndexOf("2") >= 0).OrderBy(o => o.Sort);

            foreach (var i in data)
            {
                var com = db.CompetenceTable.FirstOrDefault(f => f.FunctionId == i.Id && f.UserLevelId == id);
                if (com == null)
                {
                    fcs.Add(new FunctionCompetenceShow
                    {
                        Id = i.Id,
                        Menu = i.Menu,
                        Name = i.Name,
                        Read = false,
                        Insert = false,
                        Update = false,
                        Delete = false,
                    });
                }
                else
                {
                    fcs.Add(new FunctionCompetenceShow
                    {
                        Id = i.Id,
                        Menu = i.Menu,
                        Name = i.Name,
                        Read = com.Read,
                        Insert = com.Insert,
                        Update = com.Update,
                        Delete = com.Delete,
                    });
                }
            }
            db.Connection.Close();
            return fcs;
        }

        /// <summary>
        /// 目前使用此權限的帳號數量
        /// </summary>
        /// <param name="LevelId">權限Id</param>
        /// <returns>數量</returns>
        private int Get_UseLevel_Count(int LevelId)
        {
            MithDataContext db = new MithDataContext();

            try
            {
                int count = db.UserProfile.Where(w => w.LevelId == LevelId).Count();
                return count;
            }
            catch
            {
                return 0;
            }
            finally
            {
                db.Connection.Close();
            }
        }

        #endregion

        #region insert

        public int Insert_UserLevel_Name(int level_id, string level_name, int role, int CompanyId)
        {
            MithDataContext db = new MithDataContext();
            var result = new UserLevel
            {
                Id = level_id,
                Name = level_name,
                Role = role,
                CreateTime = DateTime.UtcNow,
                UpdateTime = DateTime.UtcNow,
                LastUserId = 0,
                CompanyId = CompanyId
            };
            db.UserLevel.InsertOnSubmit(result);
            db.SubmitChanges();
            db.Connection.Close();
            return result.Id;
        }

        #endregion

        #region update

        public void Update_UserLevel_Name(int level_id, string level_name, int level_role, int user_id)
        {
            MithDataContext db = new MithDataContext();
            var data = db.UserLevel.FirstOrDefault(f => f.Id == level_id);
            if (data != null)
            {
                data.Name = level_name;
                data.Role = level_role;
                data.UpdateTime = DateTime.UtcNow;
                data.LastUserId = user_id;
                db.SubmitChanges();
                db.Connection.Close();
            }
            db.Connection.Close();
        }

        public int Update_FunctionCompetence(List<UserLevelModel.FunctionCompetenceShow> item, int level_id)
        {
            try
            {
                if (item.Any())
                {
                    MithDataContext db = new MithDataContext();

                    List<CompetenceTable> ct = new List<CompetenceTable>();
                    int last = 0;
                    foreach (var i in item)
                    {
                        var tmp = db.CompetenceTable.FirstOrDefault(f => f.UserLevelId == level_id && f.FunctionId == i.Id);
                        if (tmp == null)
                        {
                            ct.Add(new CompetenceTable
                            {
                                FunctionId = i.Id,
                                UserLevelId = level_id,
                                Read = i.Read,
                                Insert = i.Insert,
                                Update = i.Update,
                                Delete = i.Delete
                            });
                        }
                        else
                        {
                            tmp.Read = i.Read;
                            tmp.Insert = i.Insert;
                            tmp.Update = i.Update;
                            tmp.Delete = i.Delete;
                            last = tmp.Id;
                        }
                    }
                    db.CompetenceTable.InsertAllOnSubmit(ct);
                    db.SubmitChanges();
                    db.Connection.Close();
                    return ct.Any() ? ct.LastOrDefault().Id : last;
                }
            }
            catch (Exception ex) { }
            return -1;
        }

        #endregion

        #region Delete

        public void Delete(int id = 0)
        {
            if (id > 0)
            {
                Delete(new int[] { id });
            }
        }

        public void Delete(int[] id)
        {
            if (id != null)
            {
                if (id.Any())
                {
                    MithDataContext db = new MithDataContext();

                    var comp = db.CompetenceTable.Where(w => id.Contains(w.UserLevelId) && w.UserLevelId != 1);
                    if (comp.Any())
                    {
                        db.CompetenceTable.DeleteAllOnSubmit(comp);
                        db.SubmitChanges();
                    }

                    var level = db.UserLevel.Where(w => id.Contains(w.Id) && w.Id != 1);
                    if (level.Any())
                    {
                        db.UserLevel.DeleteAllOnSubmit(level);
                        db.SubmitChanges();
                    }

                    db.Connection.Close();
                }
            }
        }
        #endregion

    }
}