﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Linq;
using System.Data.Linq.SqlClient;
using Mith.Areas.GoX.Models;
using Mith.Models;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;

namespace Mith.Areas.GoX.Models
{
	public class RefundModel
	{
		#region Class
		public class RefundShow
		{
			[Key]
			[DisplayName("編號")]
			public int Id { get; set; }
			[DisplayName("訂單編號")]
			public string SN { get; set; }
			[DisplayName("國泰EPOS授權碼")]
			public string CathayAuthCode { get; set; }
			[DisplayName("姓名")]
			public string ReceiverName { get; set; }
			[DisplayName("電話")]
			public string ReceiverTel { get; set; }
			[DisplayName("地址")]
			public string ReceiverAddress { get; set; }
			[DisplayName("總計")]
			public int BuyTotal { get; set; }

			[DisplayName("退款方式")]
			public int CashFlowType { get; set; }
			public string CashFlowType_Name { get; set; }
			[DisplayName("退款原因")]
			public int RefundReason { get; set; }
			public string RefundReason_Name { get; set; }
			[DisplayName("已退款")]
			public int CashFlowStatus { get; set; }
			public string CashFlowStatus_Name { get; set; }
			[DisplayName("銀行代號")]
			public string BankCode { get; set; }
			[DisplayName("銀行帳號")]
			public string BankAccount { get; set; }
			[DisplayName("帳戶名稱")]
			public string AccountName { get; set; }
			[DisplayName("退款金額")]
			[Range(1, 9999999, ErrorMessage = "需大於 0")]
			[Required(ErrorMessage = "必填欄位")]
			[DataAnnotationsExtensions.Integer(ErrorMessage = "請輸入數字")]
			public int? RefundTotal { get; set; }
			[DisplayName("退款時間")]
			public DateTime? RefundTime { get; set; }
			[DisplayName("備註")]
			public string RefundRemark { get; set; }
			[DisplayName("退貨廠商")]
			public string Company_Name { get; set; }

			[DisplayName("更新人員")]
			public int LastUserId { get; set; }
			[DisplayName("更新人員")]
			public string LastUser_Name { get; set; }
			[DisplayName("建立時間")]
			public DateTime CreateTime { get; set; }
			[DisplayName("訂單細節List")]
			public List<OrderDeatailShow> OrderDetailList { get; set; }
		}

		public class OrderDeatailShow
		{
			[DisplayName("編號")]
			public int Id { get; set; }
			[DisplayName("商品編號")]
			public string Goods_SN { get; set; }
			[DisplayName("商品名稱")]
			public string Goods_Name { get; set; }
			[DisplayName("商品顏色名稱")]
			public string Goods_ColorName { get; set; }
			[DisplayName("商品類別名稱")]
			public string Goods_TypeName { get; set; }
			[DisplayName("公司名稱")]
			public int CompanyId { get; set; }
			[DisplayName("公司名稱")]
			public string Company_Name { get; set; }
			[DisplayName("品牌名稱")]
			public string Brand_Name { get; set; }
			[DisplayName("購買尺寸")]
			public string BuySize { get; set; }
			[DisplayName("購買數量")]
			public int BuyQuantity { get; set; }
			[DisplayName("購買單價")]
			public int BuyPrice { get; set; }
			[DisplayName("購買總額")]
			public int BuyTotal { get; set; }

			[DisplayName("備註")]
			public string Remark { get; set; }
			[DisplayName("更新時間")]
			public DateTime? UpdateTime { get; set; }
			[DisplayName("更新人員")]
			public int LastUserId { get; set; }
			[DisplayName("更新人員")]
			public string LastUser_Name { get; set; }

		}

		public class Export
		{
			[Key]
			public string SN { get; set; }
			public string Name { get; set; }
			public string TaxId { get; set; }
			public string Tel { get; set; }
			public string Fax { get; set; }
			public string City_Name { get; set; }
			public string Area_Name { get; set; }
			public string Address { get; set; }
			public string BossName { get; set; }
			public string BossIdentityCard { get; set; }
			public string ContactName { get; set; }
			public string ContactTel { get; set; }
			public string ContactEmail { get; set; }
			public string ContractDate { get; set; }
			public string ArtEditorCost { get; set; }
			public string FreightFee { get; set; }
			public string ManagementFee { get; set; }
			public string Collaborate { get; set; }
			public string TypeName { get; set; }
			public string GoodsKind { get; set; }
			public string AccountingName { get; set; }
			public string AccountingTel { get; set; }
			public string AccountingEmail { get; set; }
			public string BankCode { get; set; }
			public string BankName { get; set; }
			public string Account { get; set; }
			public string AccountName { get; set; }
			public string RefundTime { get; set; }
		}

		public string Keyword = "";
		public int Search_CompanyId = 0;
		public int Search_CashFlow = 0;
		public int Search_RefundReason = 0;
		public DateTime? RefundTime_St = null;
		public DateTime? RefundTime_End = null;

		#endregion

		public RefundModel()
		{
		}
		public void Clear()
		{
			Keyword = "";
			Search_CompanyId = 0;
			Search_RefundReason = 0;
			RefundTime_St = null;
			RefundTime_End = null;
		}

		#region Get

		private IQueryable<Refund_Index_View> Get()
		{
			MithDataContext db = new MithDataContext();
			IQueryable<Refund_Index_View> data = db.Refund_Index_View;

			if (Search_CompanyId > 0)
			{
				data = data.Where(w => w.CompanyId == Search_CompanyId);
			}
			if (Keyword != "")
			{
				Keyword = Keyword.Replace(" ", "");
				data = data.Where(Query(Keyword));
			}
			if (Search_CashFlow != 0)
			{
				if (Search_CashFlow == 1)
				{
					data = data.Where(w => w.CashFlowStatus == 4);
				}
				else
				{
					data = data.Where(w => w.CashFlowStatus == 3);
				}
			}
			if (Search_RefundReason != 0)
			{
				data = data.Where(w => w.RefundReason == Search_RefundReason);
			}
			if (RefundTime_St != null)
			{
				data = data.Where(w => w.RefundTime >= RefundTime_St);
			}
			if (RefundTime_End != null)
			{
				RefundTime_End = DateTime.Parse(RefundTime_End.Value.ToString("yyyy/MM/dd") + " 23:59:00");
				data = data.Where(w => w.RefundTime <= RefundTime_End);
			}
			db.Connection.Close();
			return data;
		}

		public List<RefundShow> Get_Data(int p = 1, int take = 10)
		{
			MithDataContext db = new MithDataContext();
			List<Refund_Index_View> data = new List<Refund_Index_View>();

			data = Get().OrderByDescending(o => o.Id).Skip((p - 1) * take).Take(take).ToList();

			List<RefundShow> item = new List<RefundShow>();
			item = (from i in data
							select new RefundShow
							{
								Id = i.Id,
								SN = i.SN,
								ReceiverName = i.ReceiverName,
								ReceiverTel = i.ReceiverTel,
								BuyTotal = i.BuyTotal.Value,
								CashFlowType = i.CashFlowType,
								CashFlowType_Name = i.CashFlowType_Name,
								CashFlowStatus = i.CashFlowStatus,
								CashFlowStatus_Name = i.CashFlowStatus_Name,
								RefundReason = i.RefundReason.Value,
								RefundReason_Name = i.RefundReason_Name,
								RefundTotal = i.RefundTotal,
								RefundTime = i.RefundTime,
								Company_Name = i.Company_Name,
								OrderDetailList = Get_OrderDetailList(i.Id, "Index"),
							}).ToList();

			db.Connection.Close();
			return item;
		}

		public RefundShow Get_One(int Id = 0)
		{
			MithDataContext db = new MithDataContext();
			var data = (from i in db.Refund_Edit_View
									where i.Id == Id
									select new RefundShow
									{
										Id = i.Id,
										SN = i.SN,
										CathayAuthCode = i.CathayAuthCode,
										ReceiverName = i.ReceiverName,
										ReceiverTel = i.ReceiverTel,
										ReceiverAddress = i.ReceiverAddress,
										BankCode = i.BankCode,
										BankAccount = i.BankAccount,
										BuyTotal = i.BuyTotal.Value,
										CashFlowType = i.CashFlowType,
										CashFlowType_Name = i.CashFlowType_Name,
										CashFlowStatus = i.CashFlowStatus,
										CashFlowStatus_Name = i.CashFlowStatus_Name,
										RefundReason = i.RefundReason.Value,
										RefundReason_Name = i.RefundReason_Name,
										RefundTotal = i.RefundTotal,
										RefundTime = i.RefundTime.Value,
										RefundRemark = HttpUtility.HtmlDecode(i.RefundRemark),
										Company_Name = i.Company_Name,
										LastUser_Name = i.LastUser_Name,
										CreateTime = i.CreateTime.Value,
										OrderDetailList = Get_OrderDetailList(i.Id, "Index"),
									}).FirstOrDefault();
			db.Connection.Close();
			return data;
		}

		public Method.Paging Get_Page(int p = 1, int take = 10)
		{
			return Method.Get_Page(Get_Count(), p, take);
		}

		public int Get_Count()
		{
			return Get().Count();
		}

		private string Query(string query = "")
		{
			string sql = "";

			if (query != "")
			{
				query = HttpUtility.HtmlEncode(query);
				sql = Method.SQL_Combin(sql, "SN", "OR", "( \"" + query + "\")", ".Contains", false);

			}

			return sql;
		}
		/// <summary>
		/// 取得訂單主檔底下的訂單細節
		/// </summary>
		/// <param name="OrderId">訂單編號</param>
		/// <param name="CompanyId">廠商編號</param>
		public List<OrderDeatailShow> Get_OrderDetailList(int Id = 0, string Action = "")
		{
			MithDataContext db = new MithDataContext();
			try
			{
				#region 來自 Index頁
				if (Action == "Index")
				{
					var data = (from i in db.OrderDetail_Edit_View
											where i.OrderId == Id
											orderby i.CompanyId
											select new OrderDeatailShow
											{
												Id = i.Id,
												Goods_SN = i.Goods_SN,
												Goods_Name = i.Goods_Name,
												Goods_ColorName = i.Goods_ColorName,
												Goods_TypeName = i.Goods_TypeName,
												CompanyId = i.CompanyId,
												Company_Name = i.Company_Name,
												BuySize = i.BuySize,
												BuyQuantity = i.BuyQuantity,
												BuyPrice = i.BuyPrice,
												BuyTotal = i.BuyTotal,
											}).ToList();

					if (Search_CompanyId > 0)
					{
						data = data.Where(w => w.CompanyId == Search_CompanyId).ToList();

					}
					return data;
				}
				#endregion

				#region 來自 Edit頁
				else
				{
					var data = (from i in db.OrderDetail_Edit_View
											where i.OrderId == Id
											orderby i.CompanyId
											select new OrderDeatailShow
											{
												Id = i.Id,
												Goods_SN = i.Goods_SN,
												Goods_Name = i.Goods_Name,
												Goods_ColorName = i.Goods_ColorName,
												Goods_TypeName = i.Goods_TypeName,
												CompanyId = i.CompanyId,
												BuySize = i.BuySize,
												BuyQuantity = i.BuyQuantity,
												BuyPrice = i.BuyPrice,
												BuyTotal = i.BuyTotal,
												Company_Name = i.Company_Name,
											}).ToList();

					if (Search_CompanyId > 0)
					{
						data = data.Where(w => w.CompanyId == Search_CompanyId).ToList();

					}
					return data;
				}
				#endregion
			}
			catch
			{
				return null;
			}
			finally
			{
				db.Connection.Close();
			}

		}

		#endregion

		#region update

		public int Update(RefundShow item, string Status = "")
		{
			MithDataContext db = new MithDataContext();
			try
			{
				int Id = 0;
				var data = db.Order.FirstOrDefault(f => f.Id == item.Id);
				if (data != null)
				{
					data.RefundTotal = item.RefundTotal;

					//ATM退款
					if (item.CashFlowType == 1)
					{
						if (!string.IsNullOrWhiteSpace(item.RefundTime.ToString()))
						{
							data.RefundTime = item.RefundTime.Value.AddHours(-8);
						}
						data.CashFlowStatus = 4;
						data.Status = 2; //已結單
					}
					//信用卡退款
					else
					{
						if (Status == "0000" && item.CashFlowStatus == 3)
						{
							data.CashFlowStatus = 4;
							data.RefundTime = DateTime.UtcNow;
							data.Status = 2; //已結單
						}
					}

					if (data.FinalTime == null)
					{
						data.FinalTime = DateTime.UtcNow;
					}
					data.RefundRemark = HttpUtility.HtmlEncode(item.RefundRemark);
					data.UpdateTime = DateTime.UtcNow;
					data.LastUserId = item.LastUserId;

					db.SubmitChanges();
					Id = data.Id;
				}
				return Id;
			}
			catch { return -1; }
			finally
			{
				db.Connection.Close();
			}
		}

		#endregion

		//#region Export
		//public MemoryStream Set_Excel(List<Export> data)
		//{
		//    //標題
		//    List<string> header = new List<string>();
		//    header.Add("廠商編號");
		//    header.Add("名稱");
		//    header.Add("統一編號");
		//    header.Add("電話");
		//    header.Add("傳真");
		//    header.Add("地址");
		//    header.Add("負責人姓名");
		//    header.Add("負責人身分證字號");

		//    MemoryStream ms = new MemoryStream();
		//    HSSFWorkbook workbook = new HSSFWorkbook();
		//    HSSFSheet sheet = (HSSFSheet)workbook.CreateSheet("sheet");

		//    //宣告headStyle、內容style
		//    HSSFCellStyle headStyle = null;
		//    HSSFCellStyle contStyle = null;
		//    headStyle = (HSSFCellStyle)workbook.CreateCellStyle();
		//    contStyle = (HSSFCellStyle)workbook.CreateCellStyle();
		//    HSSFFont font = null;
		//    font = (HSSFFont)workbook.CreateFont();
		//    HSSFCell cell = null;

		//    //標題粗體、黃色背景
		//    headStyle.FillForegroundColor = HSSFColor.LightYellow.Index;
		//    headStyle.FillPattern = NPOI.SS.UserModel.FillPattern.SolidForeground;
		//    //標題字型樣式
		//    font.FontHeightInPoints = 10;
		//    font.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
		//    font.FontName = "Microsoft JhengHei";
		//    headStyle.SetFont(font);
		//    //內容字型樣式(自動換行)
		//    contStyle.WrapText = true;
		//    //contStyle.VerticalAlignment = NPOI.SS.UserModel.VerticalAlignment.Center;
		//    //宣告headRow
		//    sheet.CreateRow(0);
		//    //設定head背景、粗體、內容
		//    foreach (var i in header)
		//    {
		//        cell = (HSSFCell)sheet.GetRow(0).CreateCell(header.IndexOf(i));
		//        cell.SetCellValue(i);
		//        cell.CellStyle = headStyle;
		//    }
		//    //資料欄位
		//    for (int i = 0; i < data.Count; i++)
		//    {
		//        sheet.CreateRow(i + 1);

		//        sheet.GetRow(i + 1).CreateCell(0).SetCellValue(data[i].SN);
		//        sheet.GetRow(i + 1).CreateCell(1).SetCellValue(data[i].Name);
		//        sheet.GetRow(i + 1).CreateCell(2).SetCellValue(data[i].TaxId);
		//        sheet.GetRow(i + 1).CreateCell(3).SetCellValue(data[i].Tel);
		//        sheet.GetRow(i + 1).CreateCell(4).SetCellValue(data[i].Fax);
		//        sheet.GetRow(i + 1).CreateCell(5).SetCellValue(data[i].City_Name + data[i].Area_Name + data[i].Address);
		//        sheet.GetRow(i + 1).CreateCell(6).SetCellValue(data[i].BossName);
		//        sheet.GetRow(i + 1).CreateCell(7).SetCellValue(data[i].BossIdentityCard);
		//        sheet.GetRow(i + 1).CreateCell(8).SetCellValue(data[i].ContactName);
		//        sheet.GetRow(i + 1).CreateCell(9).SetCellValue(data[i].ContactTel);
		//        sheet.GetRow(i + 1).CreateCell(10).SetCellValue(data[i].ContactEmail);
		//        sheet.GetRow(i + 1).CreateCell(11).SetCellValue(data[i].ContractDate);
		//        sheet.GetRow(i + 1).CreateCell(12).SetCellValue(data[i].ArtEditorCost);
		//        sheet.GetRow(i + 1).CreateCell(13).SetCellValue(data[i].FreightFee);
		//        sheet.GetRow(i + 1).CreateCell(14).SetCellValue(data[i].ManagementFee);
		//        sheet.GetRow(i + 1).CreateCell(15).SetCellValue(data[i].Collaborate);
		//        sheet.GetRow(i + 1).CreateCell(16).SetCellValue(data[i].TypeName);
		//        sheet.GetRow(i + 1).CreateCell(17).SetCellValue(data[i].GoodsKind);
		//        sheet.GetRow(i + 1).CreateCell(18).SetCellValue(data[i].AccountingName);
		//        sheet.GetRow(i + 1).CreateCell(19).SetCellValue(data[i].AccountingTel);
		//        sheet.GetRow(i + 1).CreateCell(20).SetCellValue(data[i].AccountingEmail);
		//        sheet.GetRow(i + 1).CreateCell(21).SetCellValue(data[i].BankCode);
		//        sheet.GetRow(i + 1).CreateCell(22).SetCellValue(data[i].BankName);
		//        sheet.GetRow(i + 1).CreateCell(23).SetCellValue(data[i].Account);
		//        sheet.GetRow(i + 1).CreateCell(24).SetCellValue(data[i].AccountName);
		//        sheet.GetRow(i + 1).CreateCell(25).SetCellValue(data[i].RefundTime);
		//    }
		//    foreach (var i in header)
		//    {
		//        sheet.SetColumnWidth(header.IndexOf(i), (short)256 * 12);
		//    }

		//    workbook.Write(ms);
		//    ms.Position = 0;
		//    ms.Flush();
		//    return ms;
		//}

		//public List<Export> Export_Data(RefundModel data)
		//{

		//    /*-------------------匯出宣告-------------------*/
		//    MithDataContext db = new MithDataContext();
		//    Export export = new Export();
		//    List<Export> exp = new List<Export>();
		//    var item = data.Get().OrderByDescending(o => o.Id);
		//    /*-------------------匯出宣告End-------------------*/

		//    foreach (var i in item)
		//    {

		//        export.Name = i.Name;
		//        export.RefundTime = i.RefundTime.Value.ToString("yyyy/MM/dd HH:mm");

		//        exp.Add(export);
		//        export = new Export();
		//    }
		//    db.Connection.Close();
		//    return exp;
		//}
		//#endregion

	}
}