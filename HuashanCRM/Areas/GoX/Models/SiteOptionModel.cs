﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Mith.Models;

namespace Mith.Areas.GoX.Models
{
    public class SiteOptionModel
    {
        public class SiteOptionShow
        {
            [Key]
            [DisplayName("編號")]
            public int Id { get; set; }
            [Required(ErrorMessage = "必填欄位")]
            [DisplayName("網站名稱")]
            public string WebTitle { get; set; }
            [DisplayName("網站Logo")]
            public string WebLogo { get; set; }
            [DisplayName("SEO:網站關鍵字(請用半形逗號,隔開)")]
            [Required(ErrorMessage = "必填欄位")]
            public string WebSEO { get; set; }
            [DisplayName("SEO:網站描述")]
            [Required(ErrorMessage = "必填欄位")]
            public string WebDescription { get; set; }
            [DisplayName("粉絲團網址")]
            public string WebFans { get; set; }
            [DisplayName("SMTP Server位置")]
            [Required(ErrorMessage = "必填欄位")]
            public string SMTP_Server { get; set; }
            [DisplayName("SMTP 是否需要帳密")]
            [Required(ErrorMessage = "必填欄位")]
            public bool SMTP_Login { get; set; }
            [DisplayName("SMTP 是否使用SSL")]
            [Required(ErrorMessage = "必填欄位")]
            public bool SMTP_SSL { get; set; }
            [DisplayName("SMTP Server埠號")]
            [Required(ErrorMessage = "必填欄位")]
            public int SMTP_Port { get; set; }
            [DisplayName("SMTP 帳號")]
            public string SMTP_Account { get; set; }
            [DisplayName("SMTP 密碼")]
            [DataType(DataType.Password)]
            public string SMTP_Password { get; set; }
            [DisplayName("客服信箱(寄件者信箱)")]
            [Required(ErrorMessage = "必填欄位")]
            [DataType(DataType.EmailAddress)]
            [EmailAddress(ErrorMessage = "請輸入正確電子信箱。")]
            public string Service_Mail { get; set; }
            [DisplayName("寄件者姓名")]
            [Required(ErrorMessage = "必填欄位")]
            public string FromName { get; set; }
            [DisplayName("網域")]
            public string DNS { get; set; }
            [DisplayName("SSL")]
            public bool SSL { get; set; }
            [DisplayName("真實刪除資料")]
            public bool Del { get; set; }
            [DisplayName("網站根目錄")]
            public string RootPath { get; set; }
            [DisplayName("網站Icon")]
            public string WebIcon { get; set; }
        }

        public SiteOptionModel()
        {
        }

        public static SiteOptionShow Get()
        {
            MithDataContext db = new MithDataContext();
            var data = db.WebConfig.FirstOrDefault();
            db.Connection.Close();
            if (data != null)
            {
                return new SiteOptionShow
                {
                    Id = data.Id,
                    WebTitle = data.WebTitle,
                    WebLogo = data.WebLogo != null && data.WebLogo != "" ? Method.RootPath + data.WebLogo : "",
                    WebSEO = data.WebSEO,
                    WebDescription = data.WebDescription,
                    WebFans = data.WebFans,
                    SMTP_Server = data.SMTP_Server,
                    SMTP_Port = data.SMTP_Port,
                    SMTP_Login = data.SMTP_Login,
                    SMTP_SSL = data.SMTP_SSL,
                    SMTP_Account = data.SMTP_Account,
                    SMTP_Password = data.SMTP_Password,
                    Service_Mail = data.Service_Mail,
                    FromName = data.FromName,
                    DNS = data.DNS,
                    SSL = data.SSL,
                    RootPath = data.RootPath,
                    Del = data.Del,
                    WebIcon = data.WebIcon != null && data.WebIcon != "" ? Method.RootPath + data.WebIcon : ""
                };
            }
            return new SiteOptionShow();
        }

        public static int Set(SiteOptionShow item, bool superadmin)
        {
            try
            {
                MithDataContext db = new MithDataContext();
                var data = db.WebConfig.FirstOrDefault();
                if (data != null)
                {
                    data.WebTitle = item.WebTitle;
                    if (item.WebLogo != "")
                    {
                        data.WebLogo = item.WebLogo;
                    }
                    if (item.WebIcon != "")
                    {
                        data.WebIcon = item.WebIcon;
                    }
                    data.WebSEO = item.WebSEO;
                    data.WebFans = item.WebFans;
                    data.WebDescription = item.WebDescription;
                    data.SMTP_Server = item.SMTP_Server;
                    data.SMTP_Port = item.SMTP_Port;
                    data.SMTP_Login = item.SMTP_Login;
                    data.SMTP_SSL = item.SMTP_SSL;
                    data.SMTP_Account = item.SMTP_Account;
                    data.SMTP_Password = item.SMTP_Password;
                    data.Service_Mail = item.Service_Mail;
                    data.FromName = item.FromName;

                    if (superadmin)
                    {
                        data.DNS = item.DNS == null ? "" : item.DNS;
                        data.SSL = item.SSL;
                        data.Del = item.Del;
                        data.RootPath = item.RootPath == null ? "" : item.RootPath;
                    }
                    db.SubmitChanges();
                    db.Connection.Close();
                    return data.Id;
                }
                else
                {
                    WebConfig wc = new WebConfig
                    {
                        WebTitle = item.WebTitle,
                        WebLogo = item.WebLogo,
                        WebFans = item.WebFans,
                        WebSEO = item.WebSEO,
                        WebIcon = item.WebIcon,
                        WebDescription = item.WebDescription,
                        SMTP_Server = item.SMTP_Server,
                        SMTP_Port = item.SMTP_Port,
                        SMTP_SSL = item.SMTP_SSL,
                        SMTP_Login = item.SMTP_Login,
                        SMTP_Account = item.SMTP_Account,
                        SMTP_Password = item.SMTP_Password,
                        Service_Mail = item.Service_Mail,
                        FromName = item.FromName,
                        DNS = "",
                        SSL = false
                    };
                    if (superadmin)
                    {
                        wc.DNS = item.DNS == null ? "" : item.DNS;
                        wc.SSL = item.SSL;
                        wc.RootPath = item.RootPath == null ? "" : item.RootPath;
                        wc.Del = item.Del;
                    }
                    db.WebConfig.InsertOnSubmit(wc);
                    db.SubmitChanges();
                    db.Connection.Close();
                    return wc.Id;
                }
            }
            catch { }
            return 0;
        }

        public static bool Check_SuperAdmin(string account, string password)
        {
            return (account == UserProfileModel.SuperAdmin && Method.GetMD5(password, true) == UserProfileModel.SuperAdminPassWord);
        }

        public static bool Check_SuperAdmin2(string account, string password)
        {
            return (account == UserProfileModel.SuperAdmin && password == UserProfileModel.SuperAdminPassWord);
        }

        public static string Get_Title()
        {
            MithDataContext db = new MithDataContext();
            var data = db.WebConfig.FirstOrDefault();
            db.Connection.Close();
            if (data != null)
            {
                return data.WebTitle;
            }
            return "";
        }

        public static string Get_RootPath()
        {
            MithDataContext db = new MithDataContext();
            var data = db.WebConfig.FirstOrDefault();
            db.Connection.Close();
            if (data != null)
            {
                return data.RootPath;
            }
            return "";
        }

        public static bool Get_Del()
        {
            MithDataContext db = new MithDataContext();
            var data = db.WebConfig.FirstOrDefault();
            db.Connection.Close();
            if (data != null)
            {
                return data.Del;
            }
            return false;
        }

        public static string Get_Logo()
        {
            MithDataContext db = new MithDataContext();
            var data = db.WebConfig.FirstOrDefault();
            db.Connection.Close();
            if (data != null)
            {
                if (data.WebLogo != null && data.WebLogo != "")
                {
                    return Method.RootPath + data.WebLogo;
                }
            }
            return "";
        }

        public static string Get_Icon()
        {
            MithDataContext db = new MithDataContext();
            var data = db.WebConfig.FirstOrDefault();
            db.Connection.Close();
            if (data != null)
            {
                if (data.WebIcon != null && data.WebIcon != "")
                {
                    return Method.RootPath + data.WebIcon;
                }
            }
            return "";
        }

        public static string Get_Fans()
        {
            MithDataContext db = new MithDataContext();
            var data = db.WebConfig.FirstOrDefault();
            db.Connection.Close();
            if (data != null)
            {
                return data.WebFans;
            }
            return "";
        }

        public static string Get_WebSEO()
        {
            MithDataContext db = new MithDataContext();
            var data = db.WebConfig.FirstOrDefault();
            db.Connection.Close();
            if (data != null)
            {
                return data.WebSEO;
            }
            return "";
        }

        public static string Get_WebDescription()
        {
            MithDataContext db = new MithDataContext();
            var data = db.WebConfig.FirstOrDefault();
            db.Connection.Close();
            if (data != null)
            {
                return data.WebDescription;
            }
            return "";
        }

        public static string Get_DNS()
        {
            MithDataContext db = new MithDataContext();
            var data = db.WebConfig.FirstOrDefault();
            db.Connection.Close();
            if (data != null)
            {
                return data.DNS;
            }
            return "";
        }

        public static bool Get_SSL()
        {
            MithDataContext db = new MithDataContext();
            var data = db.WebConfig.FirstOrDefault();
            db.Connection.Close();
            if (data != null)
            {
                return data.SSL;
            }
            return false;
        }
    }
}