﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using System.Text;
using System.Text.RegularExpressions;
using Mith.Models;
using System.IO;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using LinqToExcel;


namespace Mith.Areas.GoX.Models
{
    public class WearPicModel
    {

        #region Class
        //public class WearPicShow
        //{
        //    [Key]
        //    [DisplayName("編號")]
        //    public int Id { get; set; }

        //    [DisplayName("圖片編號")]
        //    public string SN { get; set; }
        //    [DisplayName("穿搭圖")]
        //    public string Files { get; set; }
        //    [DisplayName("點擊圖")]
        //    public string Pic1_Big { get; set; }
        //    [DisplayName("穿搭圖")]
        //    public string Pic2_Big { get; set; }
        //    public string Pic2_Middle { get; set; }
        //    public string Pic2_Small { get; set; }
        //    [DisplayName("類別")]
        //    public string Type { get; set; }
        //    public string TypeName { get; set; }
        //    [DisplayName("上架")]
        //    public bool Display { get; set; }
        //    [DisplayName("建立時間")]
        //    public DateTime? CreateTime { get; set; }

        //}

        public class WearPicShow
        {
            [Key]
            [DisplayName("編號")]
            public int Id { get; set; }

            [DisplayName("圖片編號")]
            public string SN { get; set; }
            [DisplayName("穿搭圖")]
            public string Files { get; set; }
            [DisplayName("點擊圖")]
            public string Pic1_Big { get; set; }

            [DisplayName("穿搭圖")]
            public string Pic2_Big { get; set; }
            public string Pic2_Middle { get; set; }
            public string Pic2_Small { get; set; }
            [DisplayName("類別")]
            public string Type { get; set; }
            public string TypeName { get; set; }
            [DisplayName("上架")]
            public bool Display { get; set; }
            [DisplayName("建立時間")]
            public DateTime? CreateTime { get; set; }
            [DisplayName("更新時間")]
            public DateTime? UpdateTime { get; set; }
            [DisplayName("更新人員")]
            public int LastUser_Id { get; set; }
            [DisplayName("更新人員")]
            public string LastUser_Name { get; set; }

        }

        #endregion

        #region 初始值
        public string Keyword = "";
        public string Sort = "update";
        public string Search_Type = "0";
        public int Search_Display = 0;
        public DateTime? CreateTime_St = null;
        public DateTime? CreateTime_End = null;

        public void Clear_Params()
        {
            Keyword = "";
            Sort = "update";
            Search_Type = "0";
            Search_Display = 0;
            CreateTime_St = null;
            CreateTime_End = null;
        }

        #endregion

        #region Get

        private IQueryable<WearPic_View> Get()
        {
            MithDataContext db = new MithDataContext();
            IQueryable<WearPic_View> data = db.WearPic_View;
            if (Keyword != "")
            {
                data = data.Where(Query(Keyword));
            }
            if (Search_Type != "0")
            {
                data = data.Where(w => w.Type == Search_Type);
            }
            if (Search_Display != 0)
            {
                if (Search_Display == 1)
                {
                    data = data.Where(w => w.Display == true);
                }
                else
                {
                    data = data.Where(w => w.Display == false);
                }
            }
            if (CreateTime_St != null)
            {
                data = data.Where(w => w.CreateTime >= CreateTime_St.Value);
            }
            if (CreateTime_End != null)
            {
                CreateTime_End = DateTime.Parse(CreateTime_End.Value.ToString("yyyy/MM/dd") + " 23:59:00");
                data = data.Where(w => w.CreateTime <= CreateTime_End);
            }

            db.Connection.Close();
            return data;
        }

        public List<WearPicShow> Get_Data(int p = 1, int take = 10)
        {
            MithDataContext db = new MithDataContext();
            var data = Get();
            data = data.OrderByDescending(o => o.Id).Skip((p - 1) * take).Take(take);

            List<WearPicShow> item = new List<WearPicShow>();
            item = (from i in data
                    select new WearPicShow
                    {
                        Id = i.Id,
                        SN = i.SN,
                        Pic1_Big = i.Pic1_Big,
                        Pic2_Middle = i.Pic2_Middle,
                        Pic2_Small = i.Pic2_Small,
                        Display = i.Display,
                        Type = i.Type,
                        TypeName = Get_TypeName(i.Type),
                        CreateTime = i.CreateTime,
                    }).ToList();
            db.Connection.Close();
            return item;
        }

        public WearPicShow Get_One(int id)
        {
            MithDataContext db = new MithDataContext();
            var data = (from i in db.WearPic_View
                        where i.Id == id
                        select new WearPicShow
                        {
                            Id = i.Id,
                            SN = i.SN,
                            Pic1_Big = i.Pic1_Big,
                            Pic2_Big = i.Pic2_Big,
                            Pic2_Middle = i.Pic2_Middle,
                            Pic2_Small = i.Pic2_Small,
                            Type = i.Type,
                            TypeName = Get_TypeName(i.Type),
                            Display = i.Display,
                            CreateTime = i.CreateTime,
                            UpdateTime = i.UpdateTime,
                            LastUser_Name = i.LastUser_Name
                        }).FirstOrDefault();
            db.Connection.Close();
            return data;
        }

        public Method.Paging Get_Page(int p = 1, int take = 10)
        {
            return Method.Get_Page(Get_Count(), p, take);
        }

        public int Get_Count()
        {
            try
            {
                return Get().Count();
            }
            catch { return 0; }
        }


        private string Query(string query = "")
        {
            string sql = "";

            if (query != "")
            {
                query = HttpUtility.HtmlEncode(query);
                sql = Method.SQL_Combin(sql, "SN", "OR", "( \"" + query + "\")", ".Contains", false);
            }

            return sql;
        }
        #endregion

        #region Insert
        public void Insert_multiple(string Path_Big, string Path_Middle, string Path_Small, string SN, string Pic_Type)
        {
            MithDataContext db = new MithDataContext();
            string Pic1_Big = "";

            string Pic2_Big = "";
            string Pic2_Middle = "";
            string Pic2_Small = "";

            if (Pic_Type == "1")
            {
                Pic1_Big = Path_Big;
            }
            else
            {
                Pic2_Big = Path_Big;
                Pic2_Middle = Path_Middle;
                Pic2_Small = Path_Small;
            }

            WearPic new_item = new WearPic
            {
                SN = SN,
                Pic1_Big = Pic1_Big,
                Pic2_Big = Pic2_Big,
                Pic2_Middle = Pic2_Middle,
                Pic2_Small = Pic2_Small,
                Display = true,
                CreateTime = DateTime.UtcNow,
                UpdateTime = DateTime.UtcNow,
                LastUserId = 0
            };
            db.WearPic.InsertOnSubmit(new_item);
            db.SubmitChanges();
            db.Connection.Close();
        }
        #endregion

        #region Update

        public int Update_One(WearPicShow item)
        {
            try
            {
                MithDataContext db = new MithDataContext();
                int Id = 0;
                var data = db.WearPic.FirstOrDefault(f => f.Id == item.Id);
                if (data != null)
                {
                    data.Display = item.Display;
                    data.UpdateTime = DateTime.UtcNow;
                    data.LastUserId = item.LastUser_Id;
                    db.SubmitChanges();
                    Id = data.Id;
                }
                db.Connection.Close();
                return Id;
            }
            catch { return -1; }
        }

        public void Update_multiple(string Path_Big, string Path_Middle, string Path_Small, string SN, string Pic_Type)
        {
            try
            {
                //點擊圖
                if (Pic_Type == "1")
                {
                    UpdatePic(Path_Big, Path_Middle, Path_Small, SN, "1");
                }
                //穿搭圖
                else
                {
                    UpdatePic(Path_Big, Path_Middle, Path_Small, SN, "2");
                }
            }
            catch { }
        }

        public void UpdatePic(string Path_Big, string Path_Middle, string Path_Small, string SN, string Pic_Type)
        {
            MithDataContext db = new MithDataContext();

            var data = db.WearPic.FirstOrDefault(w => w.SN == SN);
            if (data != null)
            {
                //點擊圖
                if (Pic_Type == "1")
                {
                    BlobHelper.delete("wear/" + "Big/" + SN, Path.GetFileName(data.Pic1_Big));
                    data.Pic1_Big = Path_Big;
                }
                //穿搭圖
                else
                {
                    BlobHelper.delete("wear/" + "Big/" + SN, Path.GetFileName(data.Pic2_Big));
                    data.Pic2_Big = Path_Big;

                    BlobHelper.delete("wear/" + "Middle/" + SN, Path.GetFileName(data.Pic2_Middle));
                    data.Pic2_Middle = Path_Middle;

                    BlobHelper.delete("wear/" + "Small/" + SN, Path.GetFileName(data.Pic2_Small));
                    data.Pic2_Small = Path_Small;
                }
                db.SubmitChanges();
                db.Connection.Close();
            }

            db.Connection.Close();

        }

        #endregion

        #region Delete
        public void Delete(int id, HttpServerUtilityBase Server, int user_id)
        {
            if (id > 0)
            {
                Delete(new int[] { id }, Server, user_id);
            }
        }

        public void Delete(int[] id, HttpServerUtilityBase Server, int user_id)
        {
            if (id != null)
            {
                if (id.Any())
                {
                    MithDataContext db = new MithDataContext();
                    var data = db.WearPic.Where(w => id.Contains(w.Id));
                    if (data.Any())
                    {
                        if (Method.SiteDel)
                        {
                            foreach (var i in data)
                            {
                                try
                                {

                                }
                                catch { }
                            }
                            db.WearPic.DeleteAllOnSubmit(data);
                        }
                        else
                        {
                        }
                        db.SubmitChanges();
                    }
                    db.Connection.Close();
                }
            }
        }

        #endregion

        #region 下拉式選單
        /// <summary>
        /// 取得款式下拉選單
        /// </summary>
        /// <returns>選單</returns>
        public static List<SelectListItem> Get_Type_SelectListItem(string type = "")
        {
            MithDataContext db = new MithDataContext();
            List<SelectListItem> data = new List<SelectListItem>();
            data.Add(new SelectListItem
            {
                Selected = type == "0",
                Text = "請選擇或輸入文字",
                Value = "0"
            });
            var Category = db.Category.Where(w => w.Fun_Id == 22 && w.Group == "Type");
            foreach (var item in Category)
            {
                data.AddRange(new SelectListItem
                {
                    Selected = type == item.Remark,
                    Text = item.Name,
                    Value = item.Remark
                });
            }
            db.Connection.Close();
            return data;
        }
        #endregion

        #region 上傳圖片
        public int CheckSN_Count(string SN)
        {
            MithDataContext db = new MithDataContext();
            int count = db.WearPic.Where(w => w.SN == SN).Count();

            return count;
        }

        #endregion

        private string Get_TypeName(string Type)
        {
            MithDataContext db = new MithDataContext();
            string Str = "";
            Str = db.Category.FirstOrDefault(f => f.Fun_Id == 22 && f.Group == "Type" && f.Remark == Type).Name;
            db.Connection.Close();
            return Str;
        }


    }
}