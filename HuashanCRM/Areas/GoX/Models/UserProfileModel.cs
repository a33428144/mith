﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using Mith.Areas.GoX.Models;
using System.Web.Security;
using Mith.Models;
using System.Data.Linq.SqlClient;

namespace Mith.Areas.GoX.Models
{

    public class UserProfileModel
    {
        #region Class
        public class UserProfileShow
        {
            [Key]
            [DisplayName("編號")]
            public int Id { get; set; }
            [DisplayName("角色")]
            public int Role { get; set; }
            [DisplayName("角色")]
            public string Role_Name { get; set; }
            [DisplayName("廠商")]
            public int? CompanyId { get; set; }
            [DisplayName("廠商")]
            public string Company_Name { get; set; }
            [DisplayName("達人")]
            public int? ExpertId { get; set; }
            [DisplayName("達人")]
            public string Expert_Name { get; set; }
            [DisplayName("權限")]
            [Required(ErrorMessage = "必填欄位")]
            public int LevelId { get; set; }
            [DisplayName("權限")]
            public string Level_Name { get; set; }
            [DisplayName("名稱")]
            [Required(ErrorMessage = "必填欄位")]
            public string Name { get; set; }
            [DisplayName("帳號")]
            [Required(ErrorMessage = "必填欄位")]
            [StringLength(12, MinimumLength = 6, ErrorMessage = "帳號的長度需為 6~12 碼")]
            public string Account { get; set; }
            [DisplayName("密碼")]
            [Required(ErrorMessage = "必填欄位")]
            [StringLength(12, MinimumLength = 6, ErrorMessage = "密碼的長度需為 6~12 碼")]
            public string Password { get; set; }
            [DisplayName("再次輸入密碼")]
            [System.Web.Mvc.Compare("Password", ErrorMessage = "兩次輸入的密碼必須相符！")]
            public string ConfirmPassword { get; set; }
            [DisplayName("建立時間")]
            public DateTime? CreateTime { get; set; }
            [DisplayName("更新時間")]
            public DateTime? UpdateTime { get; set; }
            [DisplayName("啟用")]
            public bool Enable { get; set; }

        }

        public class UserProfileShowEdit
        {
            [Key]
            [DisplayName("編號")]
            public int Id { get; set; }
            [DisplayName("角色")]
            public int Role { get; set; }
            [DisplayName("角色")]
            public string Role_Name { get; set; }
            [DisplayName("廠商")]
            public int? CompanyId { get; set; }
            [DisplayName("廠商")]
            public string Company_Name { get; set; }
            [DisplayName("達人")]
            public int? ExpertId { get; set; }
            [DisplayName("達人")]
            public string Expert_Name { get; set; }
            [DisplayName("權限")]
            [Required(ErrorMessage = "必填欄位")]
            public int LevelId { get; set; }
            [DisplayName("權限")]
            public string Level_Name { get; set; }
            [DisplayName("帳號")]
            public string Account { get; set; }
            [DisplayName("名稱")]
            [Required(ErrorMessage = "必填欄位")]
            public string Name { get; set; }
            [DisplayName("舊密碼")]
            public string OldPassword { get; set; }
            [DisplayName("密碼")]
            [StringLength(12, MinimumLength = 6, ErrorMessage = "密碼的長度需為 6~12 碼")]
            public string Password { get; set; }
            [DisplayName("再次輸入密碼")]
            [System.Web.Mvc.Compare("Password", ErrorMessage = "兩次輸入的密碼必須相符！")]
            public string ConfirmPassword { get; set; }
            [DisplayName("建立時間")]
            public DateTime? CreateTime { get; set; }
            [DisplayName("更新時間")]
            public DateTime? UpdateTime { get; set; }
            [DisplayName("啟用")]
            public bool Enable { get; set; }
            [DisplayName("更新人員")]
            public string LastUser_Name { get; set; }

        }

        public class LoginModel
        {
            [Display(Name = "帳號")]
            [Required(ErrorMessage = "必填欄位")]
            public string UserName { get; set; }

            [Display(Name = "密碼")]
            [Required(ErrorMessage = "必填欄位")]
            [DataType(DataType.Password)]
            [StringLength(12, MinimumLength = 6, ErrorMessage = "密碼的長度需為 6~12 碼")]
            public string Password { get; set; }

            [Display(Name = "記住我")]
            public bool RememberMe { get; set; }
        }

        public class UserLevelShow
        {
            [Key]
            [DisplayName("編號")]
            public int Id { get; set; }
            [DisplayName("權限名稱")]
            [Required(ErrorMessage = "必填欄位")]
            public string Name { get; set; }
        }

        public class FunctionCompetenceShow
        {
            [Key]
            [DisplayName("編號")]
            public int Id { get; set; }
            [DisplayName("功能名稱")]
            public string Name { get; set; }
            [DisplayName("讀取")]
            public bool Read { get; set; }
            [DisplayName("新增")]
            public bool Insert { get; set; }
            [DisplayName("編輯")]
            public bool Update { get; set; }
            [DisplayName("刪除")]
            public bool Delete { get; set; }
        }


        private const string hash = "soohoobook";

        public const string SuperAdmin = "jkdfzx";
        public const string SuperAdminPassWord = "5a21d442011858c353da69a3148ac5a4";

        public string Keyword = "";
        public int Search_Role = 0;
        public int Search_Level = 0;
        public int Search_CompanyId = 0;
        public int Search_Enable = 0;
        public bool Enable = false;
        public DateTime? CreateTime_St = null;
        public DateTime? CreateTime_End = null;
        #endregion

        public UserProfileModel()
        {
        }

        public void Clear()
        {
            Keyword = "";
            Search_Role = 0;
            Search_Level = 0;
            Search_CompanyId = 0;
            Search_Enable = 0;
            Enable = false;
            CreateTime_St = null;
            CreateTime_End = null;
        }

        public static bool IsUserInRole(string roleName)
        {
            string strLoginID = HttpContext.Current.User.Identity.Name;
            var data = Login_Get_One(strLoginID);
            if (data == null)
            {
                return false;
            }

            return (data.Level_Name == roleName);
        }

        public string Get_HashPassword(string input)
        {
            return Method.GetMD5(hash + input, true);
        }

        #region Get
        public IQueryable<UserLevel> Get_UserLevel()
        {
            MithDataContext db = new MithDataContext();
            IQueryable<UserLevel> data = db.UserLevel.Where(w => w.Id != 1).OrderBy(o => o.Id);
            db.Connection.Close();
            return data;
        }

        public UserLevelShow Get_UserLevel_One(int id)
        {
            MithDataContext db = new MithDataContext();
            var data = db.UserLevel.FirstOrDefault(f => f.Id == id);
            db.Connection.Close();
            if (data != null)
            {
                return new UserLevelShow
                {
                    Id = data.Id,
                    Name = data.Name
                };
            }
            return null;
        }

        public int Get_UserLevel_Count(string keyword = "")
        {
            MithDataContext db = new MithDataContext();
            IQueryable<UserLevel> data = db.UserLevel;
            db.Connection.Close();
            if (keyword != "")
            {
                data = from i in data
                       where SqlMethods.Like(i.Name, "%" + keyword + "%")
                       select i;
            }
            return data.Count();
        }

        public IQueryable<UserLevel> Get_UserLevel(string keyword = "", int p = 1, int show_number = 10)
        {
            MithDataContext db = new MithDataContext();
            IQueryable<UserLevel> data = db.UserLevel;
            db.Connection.Close();
            if (keyword != "")
            {
                data = from i in data
                       where SqlMethods.Like(i.Name, "%" + keyword + "%")
                       select i;
            }
            return data.OrderByDescending(o => o.Id).Skip((p - 1) * show_number).Take(show_number);
        }


        public UserProfileShowEdit Get_One(int id)
        {
            MithDataContext db = new MithDataContext();
            var Category = db.Category.Where(w => w.Group == "Role");
            var data = (from i in db.UserProfile_View
                        where i.Id == id
                        select new UserProfileShowEdit
                        {
                            Id = i.Id,
                            Account = i.Account,
                            Name = i.Name,
                            Role = i.Role,
                            LevelId = i.LevelId,
                            Level_Name = i.Level_Name,
                            CompanyId = i.CompanyId,
                            ExpertId = i.ExpertId,
                            CreateTime = i.CreateTime,
                            UpdateTime = i.UpdateTime,
                            LastUser_Name = i.LastUser_Name,
                            Enable = i.Enable
                        }).FirstOrDefault();
            db.Connection.Close();
            return data;
        }

        public static UserProfileShow Login_Get_One(string account)
        {
            MithDataContext db = new MithDataContext();
            var data = (from i in db.UserProfile
                        join j in db.UserLevel on i.LevelId equals j.Id
                        where i.Account == account &&
                        i.Enable == true && i.Del == false
                        select new UserProfileShow
                        {
                            Id = i.Id,
                            Account = i.Account,
                            Role = i.Role,
                            Role_Name = db.Category.FirstOrDefault(w => w.Group == "Role" && w.Number == i.Role).Name,
                            LevelId = i.LevelId,
                            Level_Name = j.Name,
                            Name = i.Name,
                            Password = i.Password,
                            CreateTime = i.CreateTime,
                            UpdateTime = i.UpdateTime,
                            Enable = i.Enable,
                            CompanyId = i.CompanyId
                        }).FirstOrDefault();
            db.Connection.Close();
            return data;
        }

        //取得登入資訊
        public UserProfileShow Get_LoginData(string account, string password)
        {
            MithDataContext db = new MithDataContext();
            var data = (from i in db.UserProfile
                        join j in db.UserLevel on i.LevelId equals j.Id
                        where i.Account == account &&
                        i.Password == Get_HashPassword(password)
                        && i.Enable == true && i.Del == false
                        select new UserProfileShow
                        {
                            Id = i.Id,
                            Account = i.Account,
                            Role = i.Role,
                            LevelId = i.LevelId,
                            Level_Name = j.Name,
                            Name = i.Name,
                            Password = i.Password,
                            CreateTime = i.CreateTime,
                            UpdateTime = i.UpdateTime,
                            Enable = i.Enable
                        }).FirstOrDefault();
            db.Connection.Close();
            return data;
        }

        public List<UserProfileShow> Get_Data(int p = 1, int take = 10)
        {
            MithDataContext db = new MithDataContext();
            var Category = db.Category.Where(w => w.Group == "Role");
            var data = Get().OrderByDescending(o => o.Id).Skip((p - 1) * take).Take(take);
            List<UserProfileShow> item = new List<UserProfileShow>();
            item = (from i in data
                    select new UserProfileShow
                    {
                        Id = i.Id,
                        Account = i.Account,
                        Role = i.Role,
                        Role_Name = i.Role_Name,
                        CompanyId = i.CompanyId,
                        Company_Name = i.Company_Name,
                        ExpertId = i.ExpertId,
                        Expert_Name = i.Expert_Name,
                        LevelId = i.LevelId,
                        Level_Name = i.Level_Name.Replace("達人權限", "-"),
                        Name = i.Name,
                        CreateTime = i.CreateTime,
                        UpdateTime = i.UpdateTime,
                        Enable = i.Enable
                    }).ToList();
            db.Connection.Close();
            return item;
        }
        public Method.Paging Get_Page(int p = 1, int take = 10)
        {
            return Method.Get_Page(Get_Count(), p, take);
        }

        public int Get_Count()
        {
            return Get().Count();
        }

        private IQueryable<UserProfile_View> Get()
        {
            MithDataContext db = new MithDataContext();
            IQueryable<UserProfile_View> data = db.UserProfile_View;
            if (Search_CompanyId > 0)
            {
                data = data.Where(w => w.CompanyId == Search_CompanyId && w.Id > 2);
            }
            if (Keyword != "")
            {
                data = data.Where(Query(Keyword));
            }
            if (Search_Role != 0)
            {
                data = data.Where(w => w.Role == Search_Role);
            }
            if (Search_Level != 0)
            {
                data = data.Where(w => w.LevelId == Search_Level);
            }
            if (Search_Enable != 0)
            {
                if (Search_Enable == 1)
                {
                    data = data.Where(w => w.Enable == true);
                }
                else
                {
                    data = data.Where(w => w.Enable == false);
                }
            }
            if (CreateTime_St != null)
            {
                data = data.Where(w => w.CreateTime >= CreateTime_St.Value);
            }
            if (CreateTime_End != null)
            {
                CreateTime_End = DateTime.Parse(CreateTime_End.Value.ToString("yyyy/MM/dd") + " 23:59:00");
                data = data.Where(w => w.CreateTime <= CreateTime_End);
            }
            db.Connection.Close();
            return data;
        }

        private string Query(string query = "")
        {
            string sql = "";

            if (query != "")
            {
                query = HttpUtility.HtmlEncode(query);
                sql = Method.SQL_Combin(sql, "Account", "OR", "( \"" + query + "\")", ".Contains", false);
                sql = Method.SQL_Combin(sql, "Name", "OR", "( \"" + query + "\")", ".Contains", false);
            }

            return sql;
        }

        //取得可讀取的模組
        public string Get_ReadFuntion(int LevelId)
        {
            MithDataContext db = new MithDataContext();
            var data = db.CompetenceTable.Where(w => w.UserLevelId == LevelId).ToList();

            data = data.Where(w => w.Read == true).ToList();
            string str = "";
            foreach (var i in data)
            {
                // FunctionId < 10的話補 0
                if (i.FunctionId < 10)
                {
                    str += "0" + i.FunctionId + ",";
                }
                else
                {
                    str += i.FunctionId + ",";
                }
            }
            db.Connection.Close();
            return str;
        }
        #endregion

        #region insert
        public int Insert(UserProfileShow item)
        {
            MithDataContext db = new MithDataContext();
            if (db.UserProfile.FirstOrDefault(f => f.Account.ToLower() == item.Account.ToLower()) == null)
            {
                if (item.Role == 1)
                {
                    item.CompanyId = null;
                    item.ExpertId = null;
                }
                //角色=廠商
                else if (item.Role == 2)
                {
                    item.ExpertId = null;
                }
                //角色=達人
                else
                {
                    item.LevelId = 3;
                    item.CompanyId = null;
                }

                UserProfile new_item = new UserProfile
                {
                    Account = item.Account,
                    Role = item.Role,
                    CompanyId = item.CompanyId,
                    ExpertId = item.ExpertId,
                    LevelId = item.LevelId,
                    Name = item.Name,
                    Password = Get_HashPassword(item.Password),
                    CreateTime = DateTime.UtcNow,
                    UpdateTime = DateTime.UtcNow,
                    Enable = item.Enable,
                    Del = false
                };
                db.UserProfile.InsertOnSubmit(new_item);
                db.SubmitChanges();
                db.Connection.Close();
                return new_item.Id;
            }
            else
            {
                db.Connection.Close();
                return -1;
            }

        }

        #endregion

        #region update

        public int Update(UserProfileShowEdit item)
        {
            try
            {
                MithDataContext db = new MithDataContext();
                int Id = 0;
                var data = db.UserProfile.FirstOrDefault(f => f.Id == item.Id);
                //角色=亞設人員
                if (item.Role == 1)
                {
                    item.CompanyId = null;
                    item.ExpertId = null;
                }
                //角色=廠商
                else if (item.Role == 2)
                {
                    item.ExpertId = null;
                }
                //角色=達人
                else
                {
                    item.LevelId = 3;
                    item.CompanyId = null;
                }
                if (data != null)
                {
                    data.Name = item.Name;
                    data.Role = item.Role;
                    data.CompanyId = item.CompanyId;
                    data.ExpertId = item.ExpertId;
                    if (item.LevelId > 0)
                    {
                        data.LevelId = item.LevelId;
                    }
                    if (!string.IsNullOrWhiteSpace(item.Password))
                    {
                        data.Password = Get_HashPassword(item.Password);
                    }
                    data.UpdateTime = DateTime.UtcNow;
                    data.Enable = item.Enable;
                    db.SubmitChanges();
                    Id = data.Id;
                }
                db.Connection.Close();
                return Id;
            }
            catch { return -1; }
        }

        public int Update_Enable(int id, bool en)
        {
            int[] id_arr = new int[1];
            id_arr[0] = id;
            return Update_Enable(id_arr, en);
        }

        public int Update_Enable(int[] id, bool en)
        {
            try
            {
                if (id != null)
                {
                    if (id.Any())
                    {
                        MithDataContext db = new MithDataContext();
                        var data = db.UserProfile.Where(w => id.Contains(w.Id) && w.Del == false);
                        if (data.Any())
                        {
                            foreach (var i in data)
                            {
                                i.Enable = en;
                                i.UpdateTime = DateTime.UtcNow;
                            }
                            db.SubmitChanges();
                        }
                        db.Connection.Close();
                        return id.LastOrDefault();
                    }
                }
                return -1;
            }
            catch { return -1; }
        }


        public static void Update_LoginTime(int id)
        {
            MithDataContext db = new MithDataContext();
            var data = db.UserProfile.FirstOrDefault(f => f.Id == id && f.Del == false);
            if (data != null)
            {
                data.LastLoginTime = DateTime.UtcNow;
                db.SubmitChanges();
            }
            db.Connection.Close();
        }
        #endregion

        #region Delete
        public void Delete(int id = 0)
        {
            if (id > 0)
            {
                Delete(new int[] { id });
            }
        }

        public void Delete(int[] id)
        {
            if (id != null)
            {
                if (id.Any())
                {
                    MithDataContext db = new MithDataContext();
                    var data = db.UserProfile.Where(w => id.Contains(w.Id) && w.Del == false);
                    if (data.Any())
                    {
                        if (Method.SiteDel)
                        {
                            db.UserProfile.DeleteAllOnSubmit(data);

                        }
                        else
                        {
                            foreach (var i in data)
                            {
                                i.Del = true;
                                i.UpdateTime = DateTime.UtcNow;
                            }
                        }
                        db.SubmitChanges();
                    }
                    db.Connection.Close();
                }
            }
        }

        #endregion

        public void Truncate_LoginRecord()
        {

            MithDataContext db = new MithDataContext();
            db.ExecuteCommand("truncate table LoginRecord");
            db.SubmitChanges();
            db.Connection.Close();
        }

        #region SelectListItem
        //By 新增、編輯
        public static List<SelectListItem> Get_UserLevel_SelectListItem(int role, int level, int CompanyId = 0)
        {
            MithDataContext db = new MithDataContext();
            List<SelectListItem> data = new List<SelectListItem>();
            data.Add(new SelectListItem
            {
                Selected = level == 0,
                Text = "請選擇或輸入文字",
                Value = "0"
            });
            var UserLevel = db.UserLevel.Where(w => w.Role == role);
            if (CompanyId > 0)
            {
                UserLevel = UserLevel.Where(w => w.CompanyId == CompanyId);
            }
            foreach (var item in UserLevel)
            {
                data.AddRange(new SelectListItem
                {
                    Selected = level == item.Id,
                    Text = item.Name,
                    Value = item.Id.ToString()
                });
            }
            db.Connection.Close();
            return data;
        }


        //By 首頁
        public static List<SelectListItem> Get_UserLevel_SelectListItem(int level)
        {
            MithDataContext db = new MithDataContext();
            List<SelectListItem> data = new List<SelectListItem>();
            //角色為亞設人員、廠商，才出現
            data.Add(new SelectListItem
            {
                Selected = level == 0,
                Text = "請選擇或輸入文字",
                Value = ""
            });
            var UserLevel = db.UserLevel.Where(w => w.Id != 3);
            foreach (var item in UserLevel)
            {
                data.AddRange(new SelectListItem
                {
                    Selected = level == item.Id,
                    Text = item.Name,
                    Value = item.Id.ToString()
                });
            }
            db.Connection.Close();
            return data;
        }

        /// <summary>
        /// Ajax- 關卡
        /// </summary>
        public static string Ajax_UserLevel_Html(int role, int level)
        {
            var data = Get_UserLevel_SelectListItem(role, level);
            string result = "";
            foreach (var i in data)
            {
                result += "<option value=\"" + i.Value + "\" " + (i.Selected ? "selected=\"selected\"" : "") + ">" + i.Text + "</option>";
            }
            return result;
        }
        #endregion
    }
}