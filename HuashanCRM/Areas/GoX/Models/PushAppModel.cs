﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using Mith.Models;
using System.Web.Mvc;
using System.IO;

namespace Mith.Areas.GoX.Models
{
    public class PushAppModel
    {
        public class PushAppModelShow
        {
            [Key]
            [DisplayName("編號")]
            public int Id { get; set; }
            [DisplayName("分類名稱")]
            public int? CategoryId { get; set; }
            [DisplayName("分類名稱")]
            public string CategoryName { get; set; }
            [DisplayName("允許推送")]
            public bool AllowPush { get; set; }
            [DisplayName("作業系統")]
            public string OS_Name { get; set; }
            [DisplayName("App名稱")]
            [Required(ErrorMessage = "必填欄位")]
            public string AppName { get; set; }
            [DisplayName("API Key ( Android ) 或 連線密碼 ( iOS )")]
            [Required(ErrorMessage = "必填欄位")]
            public string Password { get; set; }
            [DisplayName("P12檔案(iOS)")]
            public string P12_Path { get; set; }
            [DisplayName("測試版(iOS)")]
            public bool Test { get; set; }
            [DisplayName("建立時間")]
            public DateTime CreateTime { get; set; }
            [DisplayName("更新時間")]
            public DateTime UpdateTime { get; set; }
            [DisplayName("最後編修者")]
            public int UpdateUserId { get; set; }
            [DisplayName("最後編修者")]
            public string UpdateUser { get; set; }
        }


        public PushAppModel(int fun_id)
        {
            Fun_Id = fun_id;
        }

        private int Fun_Id;
        public string Keyword = "";
        public DateTime? create_time_start = null;
        public DateTime? create_time_end = null;
        public DateTime? update_time_start = null;
        public DateTime? update_time_end = null;
        public List<int> Cid = new List<int>();
        public bool AllowPushEnable = false;
        public bool AllowPush = false;

        public void Clear()
        {
            Keyword = "";
            Cid = new List<int>();
            create_time_start = null;
            create_time_end = null;
            update_time_start = null;
            update_time_end = null;
            AllowPushEnable = false;
            AllowPush = false;
        }

        private PushAppModelShow Convert(MithDataContext db, PushApp data)
        {
            var temp2 = db.UserProfile.FirstOrDefault(f => f.Id == data.LastUserId && f.Del == false);

            string name = "未分類";
                var temp = db.Category.FirstOrDefault(f => f.Id == data.CategoryId.Value);
                if (temp != null)
                {
                    name = temp.Name;
                }
            db.Connection.Close();
            return Convert(data);

        }

        private PushAppModelShow Convert(PushApp item)
        {
            return new PushAppModelShow
            {
                Id = item.Id,
                CreateTime = item.CreateTime,
                AllowPush = item.AllowPush,
                AppName = item.AppName,
                OS_Name = item.OS_Name,
                P12_Path = item.P12_Path!=null && item.P12_Path!="" ? Method.RootPath+item.P12_Path : "",
                Password = item.Password,
                Test = item.Test,
                UpdateTime = item.UpdateTime,
                UpdateUserId = item.LastUserId,
            };
        }

        private PushApp Convert(PushAppModelShow item)
        {
            return new PushApp
            {
                CategoryId = item.CategoryId,
                AllowPush = item.AllowPush,
                AppName = item.AppName,
                OS_Name = item.OS_Name,
                P12_Path = item.P12_Path,
                Password = item.Password,
                Test = item.Test,
                CreateTime = DateTime.UtcNow,
                UpdateTime = DateTime.UtcNow,
                LastUserId = item.UpdateUserId,
                Del = false
            };
        }
        
        #region get
        public static List<PushApp> Get_iOS(int[] id=null)
        {
            MithDataContext db = new MithDataContext();
            var data = db.PushApp.Where(w => w.Del == false && w.OS_Name=="iOS");
            if (id != null)
            {
                if (id.Any())
                {
                    data = data.Where(w => id.Contains(w.Id));
                }
            }
            return data.ToList();
        }

        public static List<PushApp> Get_Android(int[] id = null)
        {
            MithDataContext db = new MithDataContext();
            var data = db.PushApp.Where(w => w.Del == false && w.OS_Name == "Android");
            if (id != null)
            {
                if (id.Any())
                {
                    data = data.Where(w => id.Contains(w.Id));
                }
            }
            return data.ToList();
        }

        public List<SelectListItem> Get_Select(int id, bool selected)
        {
            List<SelectListItem> list = new List<SelectListItem>();

            //var data = Get_Data(1, Get_Count());

            //if (selected)
            //{
            //    list.Add(new SelectListItem
            //    {
            //        Selected = id==0,
            //        Text = "請選擇或輸入文字",
            //        Value = "0"
            //    });
            //}

            //list.AddRange(data.Select(s => new SelectListItem
            //    {
            //        Selected = id == s.Id,
            //        Text = s.AppName + "(" +s.OS_Name +")",
            //        Value = s.Id.ToString()
            //    }));

            return null;
        }

        public PushAppModelShow Get_One(int id)
        {
            try
            {
                MithDataContext db = new MithDataContext();
                var data = db.PushApp.FirstOrDefault(f => f.Id == id && f.Del == false);
                if (data != null)
                {
                    var temp2 = db.UserProfile.FirstOrDefault(f => f.Id == data.LastUserId && f.Del == false);
                    db.Connection.Close();
                    return Convert(data);
                }
                db.Connection.Close();
            }
            catch { }
            return null;
        }

        public List<PushAppModelShow> Get_Data(int p = 1, int take = 10)
        {
            MithDataContext db = new MithDataContext();
            var data = Get().OrderByDescending(o => o.Id).Skip((p - 1) * take).Take(take);
            List<PushAppModelShow> item = new List<PushAppModelShow>();
            item = (from i in data
                    select new PushAppModelShow
                    {
                        Id = i.Id,
                        CreateTime = i.CreateTime,
                        AllowPush = i.AllowPush,
                        AppName = i.AppName,
                        OS_Name = i.OS_Name,
                        P12_Path = i.P12_Path != null && i.P12_Path != "" ? Method.RootPath + i.P12_Path : "",
                        Password = i.Password,
                        Test = i.Test,
                        UpdateTime = i.UpdateTime,
                        UpdateUserId = i.LastUserId,
                        UpdateUser = ""
                    }).ToList();
            db.Connection.Close();
            return item;
        }

        public Method.Paging Get_Page(int p = 1, int take = 10, int pages = 5)
        {
            return Method.Get_Page(Get_Count(), p, take, pages);
        }

        public int Get_Count()
        {
            try
            {
                return Get().Count();
            }
            catch { return 0; }
        }

        private IQueryable<PushApp> Get()
        {
            MithDataContext db = new MithDataContext();
            IQueryable<PushApp> data = db.PushApp.Where(w=>w.Del==false);

            if (Keyword != "")
            {
                data = data.Where(Query(Keyword));
            }
            if (AllowPushEnable)
            {
                data = data.Where(w => w.AllowPush == AllowPush);
            }
            if (create_time_start != null)
            {
                data = data.Where(w => w.CreateTime >= create_time_start.Value);
            }
            if (create_time_end != null)
            {
                data = data.Where(w => w.CreateTime <= create_time_end.Value);
            }
            if (update_time_start != null)
            {
                data = data.Where(w => w.UpdateTime >= update_time_start.Value);
            }
            if (update_time_end != null)
            {
                data = data.Where(w => w.UpdateTime <= update_time_end.Value);
            }
            db.Connection.Close();
            return data.OrderByDescending(o=>o.Id);
        }

        private string Query(string query = "")
        {
            string sql = "";

            if (query != "")
            {
                query = HttpUtility.HtmlEncode(query);
                sql = Method.SQL_Combin(sql, "OS_Name", "OR", "( \"" + query + "\")", ".Contains", false);
                sql = Method.SQL_Combin(sql, "AppName", "OR", "( \"" + query + "\")", ".Contains", false);
            }

            return sql;
        }
        #endregion

        #region insert
        public int Insert(PushAppModelShow item)
        {
            try
            {
                MithDataContext db = new MithDataContext();
                PushApp new_item = Convert(item);
                db.PushApp.InsertOnSubmit(new_item);
                db.SubmitChanges();
                db.Connection.Close();
                return new_item.Id;
            }
            catch { return -1; }
        }
        #endregion

        #region update
        public int Update(PushAppModelShow item, HttpServerUtilityBase Server, bool delete)
        {
            try
            {
                MithDataContext db = new MithDataContext();
                var data = db.PushApp.FirstOrDefault(f => f.Id == item.Id && f.Del == false);
                if (data != null)
                {
                    data.AllowPush = item.AllowPush;
                    data.OS_Name = item.OS_Name;
                    if (data.P12_Path != null && data.P12_Path != "" && delete)
                    {
                        string path = Server.MapPath("~" + data.P12_Path);
                        if (File.Exists(path))
                        {
                            File.Delete(path);
                        }
                        data.P12_Path = null;
                    }
                    if (item.P12_Path != null && item.P12_Path != "")
                    {
                        data.P12_Path = item.P12_Path;
                    }
                    data.AppName = item.AppName;
                    data.CategoryId = item.CategoryId;
                    data.Password = item.Password;
                    data.Test = item.Test;
                    data.UpdateTime = DateTime.UtcNow;
                    data.LastUserId = item.UpdateUserId;
                    db.SubmitChanges();
                    db.Connection.Close();
                    return data.Id;
                }
                db.Connection.Close();
                return -1;
            }
            catch { return -1; }
        }

        public int Update_Enable(int id, int user_id, bool en)
        {
            return Update_Enable(new int[] { id }, user_id, en);
        }

        public int Update_Enable(int[] id, int user_id, bool en)
        {
            try
            {
                if (id != null)
                {
                    if (id.Any())
                    {
                        MithDataContext db = new MithDataContext();
                        int result = -1;
                        var data = db.PushApp.Where(w => id.Contains(w.Id) && w.Del==false);
                        if (data.Any())
                        {
                            foreach (var i in data)
                            {
                                i.AllowPush = en;
                                i.LastUserId = user_id;
                                i.UpdateTime = DateTime.UtcNow;
                                result = i.Id;
                            }
                            db.SubmitChanges();
                            db.Connection.Close();
                            return result;
                        }
                        db.Connection.Close();
                    }
                }
                return -1;
            }
            catch { return -1; }
        }
        #endregion

        #region delete
        public void Delete(int id, HttpServerUtilityBase Server, int user_id)
        {
            if (id > 0)
            {
                Delete(new int[] { id }, Server, user_id);
            }
        }

        public void Delete(int[] id, HttpServerUtilityBase Server, int user_id)
        {
            if (id != null)
            {
                if (id.Any())
                {
                    MithDataContext db = new MithDataContext();
                    var data = db.PushApp.Where(w => id.Contains(w.Id) && w.Del==false);
                    if (data.Any())
                    {
                        foreach (var i in data)
                        {
                            try
                            {
                                string path = Server.MapPath("~" + i.P12_Path);
                                if (File.Exists(path))
                                {
                                    File.Delete(path);
                                }
                            }
                            catch { }
                        }
                        if (Method.SiteDel)
                        {
                            db.PushApp.DeleteAllOnSubmit(data);
                        }
                        else
                        {
                            foreach (var i in data)
                            {
                                i.Del = true;
                                i.UpdateTime = DateTime.UtcNow;
                                i.LastUserId = user_id;
                            }
                        }
                        db.SubmitChanges();
                    }
                    db.Connection.Close();
                }
            }
        }
        #endregion
    }
}