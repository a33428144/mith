﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using System.Data.Linq.SqlClient;
using System.IO;
using Mith.Areas.GoX.Models;
using Mith.Models;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;

namespace Mith.Areas.GoX.Models
{
    public class ArticleMessageModel
    {
        #region Class
        public class ArticleMessageShow
        {
            [Key]
            [DisplayName("編號")]
            public int Id { get; set; }
            [DisplayName("文章標題")]
            public string Article_Title { get; set; }
            [DisplayName("達人姓名")]
            public string Expert_Name { get; set; }
            [DisplayName("留言會員")]
            public string Volunteers_Name { get; set; }
            public string Volunteers_Tel { get; set; }
            public string Volunteers_Email { get; set; }
            [DisplayName("留言標題")]
            public string MessageTitle { get; set; }
            [DisplayName("留言內容")]
            [DataType(DataType.MultilineText)]
            public string Message { get; set; }
            [DisplayName("留言時間")]
            public DateTime? CreateTime { get; set; }

            [DisplayName("回覆人員")]
            public int ReplyUserId { get; set; }
            [DisplayName("回覆人員")]
            public string ReplyUser_Name { get; set; }
            [DisplayName("回覆內容")]
            [DataType(DataType.MultilineText)]
            public string ReplyMessage { get; set; }
            [DisplayName("回覆時間")]
            public DateTime? ReplyTime { get; set; }
        }

        public string Keyword = "";
        public int Search_Type = 0;
        public int Search_ExpertId = 0;
        public int Search_Reply = 0;
        public DateTime? CreateTime_St = null;
        public DateTime? CreateTime_End = null;

        #endregion
        public ArticleMessageModel()
        {
        }
        public void Clear()
        {
            Keyword = "";
            Search_Type = 0;
            Search_ExpertId = 0;
            Search_Reply = 0;
            CreateTime_St = null;
            CreateTime_End = null;
        }

        #region Get

        public ArticleMessageShow Get_One(int id)
        {
            MithDataContext db = new MithDataContext();
            var data = (from i in db.ArticleMessage_View
                        where i.Id == id
                        select new ArticleMessageShow
                        {
                            Id = i.Id,
                            Volunteers_Name = i.Volunteers_Name,
                            Volunteers_Tel = i.Volunteers_Tel,
                            Volunteers_Email = i.Volunteers_Email,
                            MessageTitle = i.MessageTitle,
                            Message = HttpUtility.HtmlDecode(i.Message),
                            CreateTime = i.CreateTime,
                            ReplyMessage = HttpUtility.HtmlDecode(i.ReplyMessage),
                            ReplyTime = i.ReplyTime,
                            Expert_Name = i.Expert_Name,
                        }).FirstOrDefault();
            db.Connection.Close();
            return data;
        }

        public List<ArticleMessageShow> Get_Data(int p = 1, int take = 10, int ExpertId = 0)
        {
            MithDataContext db = new MithDataContext();
            var data = Get(ExpertId).OrderByDescending(o => o.Id).Skip((p - 1) * take).Take(take);
            List<ArticleMessageShow> item = new List<ArticleMessageShow>();
            item = (from i in data
                    select new ArticleMessageShow
                    {
                        Id = i.Id,
                        Article_Title = i.Article_Title,
                        Volunteers_Name = i.Volunteers_Name,
                        Volunteers_Tel = i.Volunteers_Tel,
                        Volunteers_Email = i.Volunteers_Email,
                        MessageTitle = i.MessageTitle,
                        Message = HttpUtility.HtmlDecode(i.Message),
                        CreateTime = i.CreateTime,
                        ReplyMessage = HttpUtility.HtmlDecode(i.ReplyMessage),
                        ReplyTime = i.ReplyTime,
                        Expert_Name = i.Expert_Name,
                    }).ToList();
            db.Connection.Close();
            return item;
        }
        public Method.Paging Get_Page(int p = 1, int take = 10, int ArticletId = 0)
        {
            return Method.Get_Page(Get_Count(ArticletId), p, take);
        }

        public int Get_Count(int ArticletId = 0)
        {
            return Get(ArticletId).Count();
        }

        private IQueryable<ArticleMessage_View> Get(int ArticletId = 0)
        {
            MithDataContext db = new MithDataContext();
            IQueryable<ArticleMessage_View> data = db.ArticleMessage_View.Where(w => w.ArticleId == ArticletId);

            if (Keyword != "")
            {
                data = data.Where(Query(Keyword));
            }
            if (Search_ExpertId != 0)
            {
                data = data.Where(w => w.ExpertId == Search_ExpertId);
            }
            if (Search_Reply != 0)
            {
                if (Search_Reply == 1)
                {
                    data = data.Where(w => w.ReplyTime != null);
                }
                else
                {
                    data = data.Where(w => w.ReplyTime == null);
                }
            }
            if (CreateTime_St != null)
            {
                data = data.Where(w => w.CreateTime >= CreateTime_St.Value);
            }
            if (CreateTime_End != null)
            {
                CreateTime_End = DateTime.Parse(CreateTime_End.Value.ToString("yyyy/MM/dd") + " 23:59:00");
                data = data.Where(w => w.CreateTime <= CreateTime_End);
            }
            db.Connection.Close();

            return data;
        }

        private string Query(string query = "")
        {
            string sql = "";

            if (query != "")
            {
                query = HttpUtility.HtmlEncode(query);
                sql = Method.SQL_Combin(sql, "Volunteers_Name", "OR", "( \"" + query + "\")", ".Contains", false);
                sql = Method.SQL_Combin(sql, "Volunteers_Email", "OR", "( \"" + query + "\")", ".Contains", false);
            }

            return sql;
        }

        #endregion

        #region update

        public int Update(ArticleMessageShow item)
        {
            try
            {
                MithDataContext db = new MithDataContext();
                int Id = 0;
                var data = db.ArticleMessage.FirstOrDefault(f => f.Id == item.Id);
                if (data != null)
                {
                    data.ReplyMessage = HttpUtility.HtmlEncode(item.ReplyMessage);
                    data.ReplyTime = DateTime.UtcNow;
                    data.ReplyUserId = item.ReplyUserId;
                    db.SubmitChanges();
                    Id = data.Id;
                }
                db.Connection.Close();
                return Id;
            }
            catch { return -1; }
        }

        #endregion

        #region Delete
        public void Delete(int id = 0)
        {
            if (id > 0)
            {
                Delete(new int[] { id });
            }
        }

        public void Delete(int[] id)
        {
            if (id != null)
            {
                if (id.Any())
                {
                    MithDataContext db = new MithDataContext();
                    var data = db.ArticleMessage.Where(w => id.Contains(w.Id));
                    if (data.Any())
                    {
                        db.ArticleMessage.DeleteAllOnSubmit(data);

                        db.SubmitChanges();
                    }
                    db.Connection.Close();
                }
            }
        }

        #endregion



    }
}