﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using Mith.Models;
using System.IO;

namespace Mith.Areas.GoX.Models
{
    public class ErrorRecordModel
    {
        public class ErrorRecordModelShow
        {
            [Key]
            [DisplayName("編號")]
            public int Id { get; set; }
            [DisplayName("錯誤代號")]
            public string Code { get; set; }
            [DisplayName("帳號")]
            public string Account { get; set; }
            [DisplayName("帳號")]
            public int AccountId { get; set; }
            [DisplayName("位置")]
            public bool Admin { get; set; }
            [DisplayName("錯誤訊息")]
            public string Message { get; set; }
            [DisplayName("瀏覽器")]
            public string Browser { get; set; }
            [DisplayName("建立時間")]
            public DateTime CreateTime { get; set; }
        }

        public ErrorRecordModel()
        {
        }

        public string Keyword = "";
        public bool Enable_Admin = false;
        public bool Admin = false;
        public DateTime? create_time_start = null;
        public DateTime? create_time_end = null;

        public void Clear()
        {
            Enable_Admin = false;
            Admin = false;
            Keyword = "";
            create_time_start = null;
            create_time_end = null;
        }

        private ErrorRecordModelShow Convert(ErrorRecord item, string account)
        {
            return new ErrorRecordModelShow
            {
                Id = item.Id,
                Account = account,
                Browser = item.Browser,
                AccountId = item.UserId,
                Admin = item.Admin,
                Code = item.Code,
                Message = item.Message,
                CreateTime = item.CreateTime
            };
        }

        private static ErrorRecord Convert(int user_id, string Code, string browser, bool admin, string Message)
        {
            return new ErrorRecord
            {
                UserId = user_id,
                Browser = browser,
                Admin = admin,
                Code = Code,
                Message = Message,
                CreateTime = DateTime.UtcNow,
                Del = false
            };
        }

        private string Get_Account(int user_id, bool Admin)
        {
            string account = "";
            if (Admin)
            {
                UserProfileModel mm = new UserProfileModel();
                var data = mm.Get_One(user_id);
                if (data != null)
                {
                    account = data.Name + "(" + data.Account + ")";
                }
            }
            else
            {
                VolunteersModel vm = new VolunteersModel();
                var data = vm.Get_One(user_id);
                if (data != null)
                {
                    account = data.RealName + "(" + data.Email + ")";
                }
            }
            return account;
        }

        #region get
        public ErrorRecordModelShow Get_One(int id)
        {
            try
            {
                MithDataContext db = new MithDataContext();
                var data = db.ErrorRecord.FirstOrDefault(f => f.Id == id && f.Del == false);
                if (data != null)
                {
                    return Convert(data, Get_Account(data.UserId, data.Admin));
                }
                db.Connection.Close();
            }
            catch { }
            return null;
        }

        public List<ErrorRecordModelShow> Get_Data(int p = 1, int take = 10)
        {
            try
            {
                return Get().Skip((p - 1) * take).Take(take).ToList();
            }
            catch { return new List<ErrorRecordModelShow>(); }
        }

        public Method.Paging Get_Page(int p = 1, int take = 10, int pages = 5)
        {
            return Method.Get_Page(Get_Count(), p, take, pages);
        }

        public int Get_Count()
        {
            try
            {
                return Get().Count();
            }
            catch { return 0; }
        }

        private IQueryable<ErrorRecordModelShow> Get()
        {
            MithDataContext db = new MithDataContext();
            IQueryable<ErrorRecordModelShow> data = db.ErrorRecord.Where(w=>w.Del==false).Select(s => new ErrorRecordModelShow
            {
                Id = s.Id,
                Account = s.Admin ? 
                    (db.UserProfile.FirstOrDefault(f=>f.Id==s.UserId)!=null ? db.UserProfile.FirstOrDefault(f=>f.Id==s.UserId).Name+"("+db.UserProfile.FirstOrDefault(f=>f.Id==s.UserId).Account+")" : "") :
                    (db.Volunteers.FirstOrDefault(f=>f.Id==s.UserId)!=null ? db.Volunteers.FirstOrDefault(f=>f.Id==s.UserId).RealName+"("+db.Volunteers.FirstOrDefault(f=>f.Id==s.UserId).Email+")" : "") ,
                AccountId= s.UserId,
                Admin = s.Admin,
                Browser = s.Browser,
                Code = s.Code,
                Message = s.Message,
                CreateTime = s.CreateTime
            });
            if (Keyword != "")
            {
                data = data.Where(Query(Keyword));
            }
            if (Enable_Admin)
            {
                data = data.Where(w => w.Admin == Admin);
            }
            if (create_time_start != null)
            {
                data = data.Where(w => w.CreateTime >= create_time_start.Value);
            }
            if (create_time_end != null)
            {
                data = data.Where(w => w.CreateTime <= create_time_end.Value);
            }
            db.Connection.Close();
            return data.OrderByDescending(o=>o.Id);
        }

        private string Query(string query = "")
        {
            string sql = "";

            if (query != "")
            {
                query = HttpUtility.HtmlEncode(query);
                sql = Method.SQL_Combin(sql, "Code", "OR", "( \"" + query + "\")", ".Contains", false);
                sql = Method.SQL_Combin(sql, "Message", "OR", "( \"" + query + "\")", ".Contains", false);
                sql = Method.SQL_Combin(sql, "Account", "OR", "( \"" + query + "\")", ".Contains", false);
                sql = Method.SQL_Combin(sql, "Browser", "OR", "( \"" + query + "\")", ".Contains", false);
            }

            return sql;
        }
        #endregion

        #region insert
        public static int Error_Record(int user_id, string Code, string browser, bool admin, string Message)
        {
            try
            {
                MithDataContext db = new MithDataContext();
                ErrorRecord item = Convert(user_id, Code, browser, admin, Message);
                db.ErrorRecord.InsertOnSubmit(item);
                db.SubmitChanges();
                db.Connection.Close();
                return item.Id;
            }
            catch { return -1; }
        }
        #endregion

        #region delete
        public void Delete(int id)
        {
            if (id > 0)
            {
                Delete(new int[] { id });
            }
        }

        public void Delete(int[] id)
        {
            if (id != null)
            {
                if (id.Any())
                {
                    MithDataContext db = new MithDataContext();
                    var data = db.ErrorRecord.Where(w => id.Contains(w.Id) && w.Del==false);
                    if (data.Any())
                    {
                        if (Method.SiteDel)
                        {
                            db.ErrorRecord.DeleteAllOnSubmit(data);
                            db.SubmitChanges();
                        }
                        else
                        {
                            db.ErrorRecord.Update(w => data.Select(s => s.Id).ToList().Contains(w.Id), u => new ErrorRecord
                            {
                                Del = true
                            });
                        }
                    }
                    db.Connection.Close();
                }
            }
        }

        public void DeleteAll()
        {
            MithDataContext db = new MithDataContext();
            var data = db.ErrorRecord.Where(w => w.Del == false); ;
            if (data.Any())
            {
                if (Method.SiteDel)
                {
                    db.ErrorRecord.DeleteAllOnSubmit(data);
                    db.SubmitChanges();
                }
                else
                {
                    db.ErrorRecord.Update(w => data.Select(s => s.Id).ToList().Contains(w.Id), u => new ErrorRecord
                    {
                        Del = true
                    });
                }
            }
            db.Connection.Close();
        }

        public void DeleteMonth(int m)
        {
            if (m > 0)
            {
                MithDataContext db = new MithDataContext();
                var data = db.ErrorRecord.Where(w => Math.Floor((DateTime.UtcNow - w.CreateTime).TotalDays) >= m * 30 && w.Del == false);
                if (data.Any())
                {
                    if (Method.SiteDel)
                    {
                        db.ErrorRecord.DeleteAllOnSubmit(data);
                        db.SubmitChanges();
                    }
                    else
                    {
                        db.ErrorRecord.Update(w => data.Select(s => s.Id).ToList().Contains(w.Id), u => new ErrorRecord
                        {
                            Del = true
                        });
                    }
                }
                db.Connection.Close();
            }
        }
        #endregion
    }
}