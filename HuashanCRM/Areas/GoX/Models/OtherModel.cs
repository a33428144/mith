﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using System.Data.Linq.SqlClient;
using System.IO;
using Mith.Areas.GoX.Models;
using Mith.Models;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;

namespace Mith.Areas.GoX.Models
{
    public class OtherModel
    {
        #region Class
        public class OtherShow
        {
            [Key]
            [DisplayName("編號")]
            public int Id { get; set; }
            [DisplayName("類別")]
            [Required(ErrorMessage = "必填欄位")]
            public string Name { get; set; }
            [DisplayName("內容")]
            [DataType(DataType.MultilineText)]
            public string Content { get; set; }

            [DisplayName("背景圖")]
            public string Pic_Big { get; set; }
            public string Pic_Middle { get; set; }
            public string Pic_Small { get; set; }

            [DisplayName("排序")]
            [Range(1, 6, ErrorMessage = "需介於1到6")]
            [DataAnnotationsExtensions.Integer(ErrorMessage = "請輸入數字")]
            public int? Sort { get; set; }
            [DisplayName("Input類型")]
            public string InputType { get; set; }
            [DisplayName("內容單位")]
            public string Unit { get; set; }
        }

        public string Keyword = "";

        #endregion
        public OtherModel()
        {
        }
        public void Clear()
        {
            Keyword = "";
        }

        #region Get

        public OtherShow Get_One(int id)
        {
            MithDataContext db = new MithDataContext();
            var data = (from i in db.Other
                        where i.Id == id
                        select new OtherShow
                        {
                            Id = i.Id,
                            Name = i.Name,
                            Content = HttpUtility.HtmlDecode(i.Content),
                            InputType = i.InputType,
                            Pic_Big = i.Pic_Big,
                            Pic_Small = i.Pic_Small,
                            Sort = i.Sort,
                        }).FirstOrDefault();
            db.Connection.Close();
            return data;
        }

        public List<OtherShow> Get_Data(int p = 1, int take = 20)
        {
            MithDataContext db = new MithDataContext();
            var data = Get().OrderByDescending(o => o.Id).Skip((p - 1) * take).Take(take);
            List<OtherShow> item = new List<OtherShow>();
            item = (from i in data
                    select new OtherShow
                    {
                        Id = i.Id,
                        Name = i.Name,
                        Content = HttpUtility.HtmlDecode(i.Content),
                        Unit = i.Unit,
                        Pic_Big = i.Pic_Big,
                        Pic_Small = i.Pic_Small,
                        Sort = i.Sort,
                    }).ToList();
            db.Connection.Close();
            return item;
        }
        public Method.Paging Get_Page(int p = 1, int take = 20)
        {
            return Method.Get_Page(Get_Count(), p, take);
        }

        public int Get_Count()
        {
            return Get().Count();
        }

        private IQueryable<Other> Get()
        {
            MithDataContext db = new MithDataContext();
            IQueryable<Other> data = db.Other;
            if (Keyword != "")
            {
                data = data.Where(Query(Keyword));
            }
            db.Connection.Close();
            return data;
        }

        private string Query(string query = "")
        {
            string sql = "";

            if (query != "")
            {
                query = HttpUtility.HtmlEncode(query);
                sql = Method.SQL_Combin(sql, "Name", "OR", "( \"" + query + "\")", ".Contains", false);
            }

            return sql;
        }

        #endregion

        #region update

        public int Update(OtherShow item)
        {
            try
            {
                MithDataContext db = new MithDataContext();
                int Id = 0;
                var data = db.Other.FirstOrDefault(f => f.Id == item.Id);
                if (data != null)
                {
                    if (item.Content == null)
                    {
                        item.Content = "";
                    }

                    if (item.InputType == "TextArea")
                    {
                        data.Content = HttpUtility.HtmlEncode(item.Content);
                    }
                    else if (item.InputType == "TextBox")
                    {
                        data.Content = item.Content;
                    }
                    else
                    {
                        data.Content = item.Content;
                        if (!string.IsNullOrWhiteSpace(item.Pic_Big))
                        {
                            data.Pic_Big = item.Pic_Big;
                        }
                        if (!string.IsNullOrWhiteSpace(item.Pic_Middle))
                        {
                            data.Pic_Middle = item.Pic_Middle;
                        }
                        if (!string.IsNullOrWhiteSpace(item.Pic_Small))
                        {
                            data.Pic_Small = item.Pic_Small;
                        }
                        data.Sort = item.Sort;
                    }

                    db.SubmitChanges();

                    Id = data.Id;
                }
                db.Connection.Close();
                return Id;
            }
            catch { return -1; }
        }

        #endregion


    }
}