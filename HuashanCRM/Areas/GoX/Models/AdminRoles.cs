﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Mith.Areas.GoX.Models;
using Mith.Models;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;

namespace Mith.Areas.GoX.Models
{
    public enum Competence
    {
        Read,
        Insert,
        Update,
        Delete,
        ToolBar,
        SuperAdmin
    }

    public class MyAuthorizeAttribute : AuthorizeAttribute
    {
        public Competence Com { get; set; }
        public string  function { get; set; }

        public override void OnAuthorization(AuthorizationContext context)
        {
            base.OnAuthorization(context);
            //判斷!! 當是未登入使用者時
            if (context.Result is HttpUnauthorizedResult)
            {
                //自己寫的Method，用來做判斷未登入使用者是從哪個頁面來的
                CheckPermission(context);
                return;
            }
        }
        private void CheckPermission(AuthorizationContext filterContext)
        {
            object areaName = null;

            //當使用者是從area來的，並且這個area名稱是Admin時(也就是後台管理者未登入時)
            if (filterContext.RouteData.DataTokens.TryGetValue("area", out areaName)
                && (areaName as string) == "GoX")
            {
                filterContext.Controller.TempData["err"] = "您的權限不足";
                //導向 /Default/LogOn

                filterContext.Result = new RedirectToRouteResult(Method.Admin_default,
                        new RouteValueDictionary
                      {
                          { "controller", Method.GoX_Default_Controller },
                          { "action", Method.GoX_Default_Action },
                          {"ReturnUrl",filterContext.HttpContext.Request.Url.PathAndQuery}
                          //{ "id", UrlParameter.Optional }
                      });
            }
            else
            {
                //否則就直接用預設的登入頁面
                filterContext.Result = new HttpUnauthorizedResult();
            }
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            bool Check = CheckLogin.CheckPermission(httpContext, Com, function);
            return Check;
        }
    }
}