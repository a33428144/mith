﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;
using System.Text;
using System.IO;
using Mith.Models;
using Mith.Areas.GoX.Models;

namespace Mith.Areas.GoX.Controllers
{
	public class OrderController : CategoryController
	{
		int fun_id = 10;

		#region 首頁
		[MyAuthorize(Com = Competence.Read, function = "訂單資料")]
		public ActionResult Index_Mith(string keyword = "", int Search_Direction = 0, int Search_LogisticsStatus = 0, int Search_Status = 0, int Search_CashFlow = 0, int Search_OverDeadline = 0, DateTime? CreateTime_St = null, DateTime? CreateTime_End = null,
				int p = 1, int show_number = 5)
		{
			OrderModel data = new OrderModel();
			var fun = FunctionModel.Get_One(fun_id);
			string get = "";
			if (keyword != "")
			{
				data.Keyword = keyword;
				ViewData["keyword"] = keyword;
				get += Method.Get_URLGet("keyword", keyword);
			}
			if (Search_Direction != 0)
			{
				data.Search_Direction = Search_Direction;
				ViewData["Search_Direction"] = Search_Direction;
				get += Method.Get_URLGet("Search_Direction", Search_Direction.ToString());
			}

			if (Search_LogisticsStatus != 0)
			{
				data.Search_LogisticsStatus = Search_LogisticsStatus;
				ViewData["Search_LogisticsStatus"] = Search_LogisticsStatus;
				get += Method.Get_URLGet("Search_LogisticsStatus", Search_LogisticsStatus.ToString());
			}

			if (Search_Status != 0)
			{
				data.Search_Status = Search_Status;
				ViewData["Search_Status"] = Search_Status;
				get += Method.Get_URLGet("Search_Status", Search_Status.ToString());
			}
			if (Search_CashFlow != 0)
			{
				data.Search_CashFlow = Search_CashFlow;
				ViewData["Search_CashFlow"] = Search_CashFlow;
				get += Method.Get_URLGet("Search_CashFlow", Search_CashFlow.ToString());
			}
			if (CreateTime_St != null)
			{
				data.CreateTime_St = CreateTime_St;
				ViewData["CreateTime_St"] = CreateTime_St;
				get += Method.Get_URLGet("CreateTime_St", CreateTime_St.Value.ToString("yyyy/MM/dd HH:mm:ss"));
			}
			if (CreateTime_End != null)
			{
				data.CreateTime_End = CreateTime_End;
				ViewData["CreateTime_End"] = CreateTime_End;
				get += Method.Get_URLGet("CreateTime_End", CreateTime_End.Value.ToString("yyyy/MM/dd HH:mm:ss"));
			}

			ViewBag.Title = fun.Name;
			ViewBag.Title_link = "訂單管理 > <a href=\"\">" + fun.Name + "</a>";

			ViewData["p"] = p;
			ViewData["page"] = data.Get_Page(p, show_number);
			ViewData["number"] = show_number;
			ViewData["get"] = get;
			Session["Url"] = Request.Url.AbsoluteUri;
			var Result = data.Get_Data_Mith(p, show_number, Search_OverDeadline);

			if (Search_OverDeadline != 0)
			{
				ViewData["Search_OverDeadline"] = Search_OverDeadline;
				get += Method.Get_URLGet("Search_OverDeadline", Search_OverDeadline.ToString());
				Result = Result.Where(w => w.Over_ShippingDeadline == true).ToList();
			}

			return View(Result);
		}

		[MyAuthorize(Com = Competence.Read, function = "訂單資料")]
		public ActionResult Index_Company(string keyword = "", int Search_Direction = 0, int Search_LogisticsStatus = 0, int Search_CashFlow = 0, int Search_OverDeadline = 0,
				DateTime? CreateTime_St = null, DateTime? CreateTime_End = null,
				int p = 1, int show_number = 5)
		{
			OrderModel data = new OrderModel();
			var fun = FunctionModel.Get_One(fun_id);
			string get = "";
			if (keyword != "")
			{
				data.Keyword = keyword;
				ViewData["keyword"] = keyword;
				get += Method.Get_URLGet("keyword", keyword);
			}
			if (Search_Direction != 0)
			{
				data.Search_Direction = Search_Direction;
				ViewData["Search_Direction"] = Search_Direction;
				get += Method.Get_URLGet("Search_Direction", Search_Direction.ToString());
			}

			if (Search_LogisticsStatus != 0)
			{
				data.Search_LogisticsStatus = Search_LogisticsStatus;
				ViewData["Search_LogisticsStatus"] = Search_LogisticsStatus;
				get += Method.Get_URLGet("Search_LogisticsStatus", Search_LogisticsStatus.ToString());
			}

			//正物流  - 查是否付款
			if (Search_CashFlow != 0)
			{
				data.Search_CashFlow = Search_CashFlow;
				ViewData["Search_CashFlow"] = Search_CashFlow;
				get += Method.Get_URLGet("Search_CashFlow", Search_CashFlow.ToString());
			}

			if (CreateTime_St != null)
			{
				data.CreateTime_St = CreateTime_St;
				ViewData["CreateTime_St"] = CreateTime_St;
				get += Method.Get_URLGet("CreateTime_St", CreateTime_St.Value.ToString("yyyy/MM/dd HH:mm:ss"));
			}
			if (CreateTime_End != null)
			{
				data.CreateTime_End = CreateTime_End;
				ViewData["CreateTime_End"] = CreateTime_End;
				get += Method.Get_URLGet("CreateTime_End", CreateTime_End.Value.ToString("yyyy/MM/dd HH:mm:ss"));
			}
			//廠商編號
			data.Search_CompanyId = Mith.Models.Method.Get_CompanyId_Admin(Request.Cookies, Session);

			ViewBag.Title = fun.Name;
			ViewBag.Title_link = "訂單管理 > <a href=\"\">" + fun.Name + "</a>";

			ViewData["p"] = p;
			ViewData["page"] = data.Get_Page(p, show_number);
			ViewData["number"] = show_number;
			ViewData["get"] = get;
			Session["Url"] = Request.Url.AbsoluteUri;

			var Result = data.Get_Data_Company(p, show_number, Search_OverDeadline);
			if (Search_OverDeadline != 0)
			{
				ViewData["Search_OverDeadline"] = Search_OverDeadline;
				get += Method.Get_URLGet("Search_OverDeadline", Search_OverDeadline.ToString());
				Result = Result.Where(w => w.Over_ShippingDeadline == true).ToList();
			}
			return View(Result);
		}

		#endregion

		#region 編輯

		[MyAuthorize(Com = Competence.Update, function = "訂單資料")]
		public ActionResult Edit_Mith(int id = 0)
		{
			MithDataContext db = new MithDataContext();
			OrderModel data = new OrderModel();
			var fun = FunctionModel.Get_One(fun_id);
			data.Search_CompanyId = 0;
			var item = data.Get_One(id);
			if (item == null)
			{
				return RedirectToAction("Index_Mith");
			}
			db.Connection.Close();
			ViewBag.Title = "訂單管理 > " + fun.Name + " > 詳情 ";
			ViewBag.Title_link = "訂單管理 > <a href=/Gox/Order/Index_Mith>" + fun.Name + "</a> > 詳情";
			if (item.Direction == 1)
			{
				return View("Edit_Mith_Forward", item);
			}
			else
			{
				return View("Edit_Mith_Backward", item);
			}

		}

		[MyAuthorize(Com = Competence.Update, function = "訂單資料")]
		public ActionResult Edit_Company(int id = 0, string ReturnStoreId = "", string ReturnStoreName = "", string ReturnStoreAddress = "", string From = "Mith")
		{
			MithDataContext db = new MithDataContext();
			OrderModel data = new OrderModel();
			var fun = FunctionModel.Get_One(fun_id);
			data.Search_CompanyId = Mith.Models.Method.Get_CompanyId_Admin(Request.Cookies, Session);

			var item = data.Get_One(id);
			if (item == null)
			{
				return RedirectToAction("Index_Mith");
			}
			db.Connection.Close();
			ViewBag.Title = "訂單管理 > " + fun.Name + " > 詳情 ";
			ViewBag.Title_link = "訂單管理 > <a href=/Gox/Order/Index_Company>" + fun.Name + "</a> > 詳情";

			//廠商編號
			int CompanyId = Mith.Models.Method.Get_CompanyId_Admin(Request.Cookies, Session);

			if (From == "Mith")
			{
				List<OrderDetail_Edit_View> OrderDetail = db.OrderDetail_Edit_View.Where(f => f.CompanyId == CompanyId && f.ReturnStoreId.Length > 1).OrderByDescending(o => o.Id).Take(1).ToList();

				if (OrderDetail.Count() > 0)
				{
					foreach (var item2 in OrderDetail)
					{
						ViewData["ReturnStoreId"] = item2.ReturnStoreId;
						ViewData["ReturnStoreName"] = item2.ReturnStoreName;
						ViewData["ReturnStoreAddress"] = item2.ReturnStoreAddress;
					}
				}
			}
			else
			{
				ViewData["ReturnStoreId"] = ReturnStoreId;
				ViewData["ReturnStoreName"] = ReturnStoreName;
				ViewData["ReturnStoreAddress"] = ReturnStoreAddress;
			}

			if (item.Direction == 1)
			{
				ViewData["From"] = From;
				return View("Edit_Company_Forward", item);
			}
			else
			{
				return View("Edit_Company_Backward", item);
			}

		}

		/// <summary>
		/// 出貨
		/// </summary>
		[ValidateInput(true)]
		[ValidateAntiForgeryToken]
		[HttpPost]
		public ActionResult Update_ShippingGoods(string ShippingVal = "", int LogisticsType = 0, string LogisticsSN = "", string ReturnStoreId = "", string ReturnStoreName = "", string ReturnStoreAddress = "", string Total = "0")
		{
			MithDataContext db = new MithDataContext();
			OrderModel data = new OrderModel();
			try
			{
				string PackageSN = null;
				var item = ShippingVal.Split(",");
				int OrderId = 0;
				string OrderSN = "";
				int LastUserId = Method.Get_UserId_Admin(Request.Cookies, Session);
				DateTime Now = DateTime.UtcNow;
				for (int i = 0; i < item.Count(); i++)
				{
					//只有狀態為備貨的訂單商品能出貨
					var OrderDetail = db.OrderDetail.FirstOrDefault(w => w.Id == int.Parse(item[i]) && w.LogisticsStatus > 0);
					if (OrderDetail != null)
					{
						OrderId = OrderDetail.OrderId;
						OrderSN = db.Order.FirstOrDefault(f => f.Id == OrderId).SN;
						if (i == 0)
						{
							if (LogisticsType == 2)
							{
								string ReceiverStoreId = db.Order.FirstOrDefault(f => f.Id == OrderId).ReceiverStoreId;
								LogisticsSN = "711" + Method.RandomKey(7, true, false, false, false);
								//取得7-11包裹要號
								PackageSN = OrderModel.Get_711_PackegeSN(LogisticsSN, OrderDetail.Id, Total, ReceiverStoreId, ReturnStoreId);
								if (PackageSN == null)
								{
									return Content("7-11串接發生錯誤！");
								}
							}

						}

						OrderDetail.LogisticsStatus = 2;
						OrderDetail.LogisticsSN = LogisticsSN;
						if (!string.IsNullOrWhiteSpace(PackageSN))
						{
							OrderDetail.PackageSN = PackageSN;
						}
						if (!string.IsNullOrWhiteSpace(ReturnStoreId))
						{
							OrderDetail.ReturnStoreId = ReturnStoreId;
						}
						if (!string.IsNullOrWhiteSpace(ReturnStoreName))
						{
							OrderDetail.ReturnStoreName = ReturnStoreName;
						}
						if (!string.IsNullOrWhiteSpace(ReturnStoreAddress))
						{
							OrderDetail.ReturnStoreAddress = ReturnStoreAddress;
						}
						OrderDetail.ShippingTime = Now;
						OrderDetail.LastUserId = LastUserId;
						db.SubmitChanges();
					}
				}
				return RedirectToAction("Index_Company", "ShippingGoods", new { keyword = OrderSN });
			}
			catch (Exception e)
			{
				return Content(e.ToString());
			}
			finally
			{
				db.Connection.Close();
			}

		}



		/// <summary>
		/// 退貨
		/// </summary>
		/// <param name="ReturnGoodsVal">退貨商品，字串為：訂單編號=退貨數量,</param>
		[ValidateInput(true)]
		[ValidateAntiForgeryToken]
		[HttpPost]
		public ActionResult Update_ReturnGoods(int CityId = 0, int AreaId = 0, string ReceiverAddress = "", int ReceiveTimeRange = 0, string Remark = "", string LogisticsSN = "", string ReturnGoodsVal = "")
		{
			MithDataContext db = new MithDataContext();
			OrderModel data = new OrderModel();
			int New_OrderId = 0;
			int TotalPrice = 0;
			try
			{
				int LastUserId = Method.Get_UserId_Admin(Request.Cookies, Session);
				//先拆逗號
				var item = ReturnGoodsVal.Split(",");
				for (int i = 0; i < item.Count(); i++)
				{
					//再拆等於（訂單編號：[0]　退貨數量：[1]）
					var item2 = item[i].Split("=");
					int OrderDetailId = int.Parse(item2[0]); //訂單編號
					int ReturnQuantity = int.Parse(item2[1]); // 退貨數量

					var Old_OrderDetail = db.OrderDetail.FirstOrDefault(f => f.Id == OrderDetailId);
					//商品總額

					#region 迴圈第一次，新增退貨的訂單主檔
					if (i == 0)
					{
						//新增退貨的訂單主檔
						New_OrderId = data.Insert_ReturnGoods_Order(1, CityId, AreaId, ReceiverAddress, ReceiveTimeRange, Remark, Old_OrderDetail.OrderId, LastUserId);
					}
					#endregion

					#region 新增退貨的訂單細節
					int BuyTotal = Old_OrderDetail.BuyPrice * ReturnQuantity;
					int CashFlowType = db.Order.FirstOrDefault(f => f.Id == New_OrderId).CashFlowType;

					var New_OrderDetail = new OrderDetail()
					{
						OrderId = New_OrderId,
						GoodsId = Old_OrderDetail.GoodsId,
						GoodsType = Old_OrderDetail.GoodsType,
						CompanyId = Old_OrderDetail.CompanyId,
						BrandId = Old_OrderDetail.BrandId,
						BuySize = Old_OrderDetail.BuySize,
						BuyQuantity = ReturnQuantity,
						BuyPrice = Old_OrderDetail.BuyPrice,
						BuyTotal = BuyTotal,
						CalculateTotal = Method.Get_CalculateTotal(CashFlowType, BuyTotal),
						LogisticsSN = LogisticsSN,
						LogisticsCompany = Old_OrderDetail.LogisticsCompany,
						LogisticsStatus = 3, //退貨中
						CreateTime = DateTime.UtcNow,
						UpdateTime = DateTime.UtcNow,
						LastUserId = LastUserId,
					};
					db.OrderDetail.InsertOnSubmit(New_OrderDetail);
					db.SubmitChanges();

					TotalPrice += BuyTotal;
					#endregion
				}

				#region 計算訂單總和
				var New_Order = db.Order.FirstOrDefault(f => f.Id == New_OrderId);
				if (New_Order != null)
				{
					New_Order.Total = TotalPrice;
					db.SubmitChanges();
				}

				#endregion

				return Redirect("/GoX/ReturnGoods/Index_Mith?keyword=" + LogisticsSN);
			}
			catch (Exception e)
			{
				//刪掉訂單
				WebApiMethod.Delete_Order((int)New_OrderId);
				return Content("Error");
			}
			finally
			{
				db.Connection.Close();
			}

		}

		#endregion

		#region 刪除

		[MyAuthorize(Com = Competence.Delete, function = "訂單資料")]
		public ActionResult Delete(int item, int p = 1)
		{
			OrderModel data = new OrderModel();
			data.Delete(item);

			return RedirectToAction("Index", new { p = p });
		}

		[MyAuthorize(Com = Competence.Delete, function = "訂單資料")]
		[HttpPost]
		public ActionResult Delete(int[] item)
		{
			OrderModel data = new OrderModel();
			data.Delete(item);

			return RedirectToAction("Index");
		}

		#endregion

		#region Ajax
		public PartialViewResult Get_SelectGoosList(int[] Id, string GoodsAction)
		{
			MithDataContext db = new MithDataContext();
			List<OrderModel.OrderDeatailShow> SelectGoosList = new List<OrderModel.OrderDeatailShow>();
			try
			{
				if (Id.Any() && Id != null)
				{
					for (int i = 0; i < Id.Count(); i++)
					{
						var OrderDetail = db.OrderDetail_Index_View.FirstOrDefault(f => f.Id == Id[i]);
						int ReturnGoodsQuantity = 0;
						//退貨時，確認收到退貨的數量
						if (GoodsAction == "ReturnGoods")
						{
							int OrderId = db.Order.FirstOrDefault(f => f.Id == OrderDetail.OrderId).Id;

							var ReturnOrder = db.Order.Where(f => f.RelativeId == OrderId);
							foreach (var item in ReturnOrder)
							{
								var ReturnOrderDetail = db.OrderDetail.FirstOrDefault(f => f.OrderId == item.Id && f.GoodsId == OrderDetail.GoodsId);
								if (ReturnOrderDetail != null)
								{
									ReturnGoodsQuantity += ReturnOrderDetail.BuyQuantity;
								}
							}
						}
						SelectGoosList.Add(new OrderModel.OrderDeatailShow()
						{
							Id = OrderDetail.Id,
							Goods_SN = OrderDetail.Goods_SN,
							Goods_Name = OrderDetail.Goods_Name,
							Goods_ColorName = OrderDetail.Goods_ColorName,
							BuySize = OrderDetail.BuySize,
							BuyQuantity = OrderDetail.BuyQuantity,
							ReturnGoodsQuantity = ReturnGoodsQuantity

						});
					}
					ViewData["SelectGoosList"] = SelectGoosList;
					ViewData["GoodsAction"] = GoodsAction;
				}
			}
			catch
			{
			}
			finally
			{
				db.Connection.Close();
			}
			return PartialView("_SelectGoosList");
		}

		//更新訂單狀態
		[HttpPost]
		public ActionResult Update_Status(string OrderId = "", string OrderDetailId = "", int Status = 0)
		{
			try
			{
				MithDataContext db = new MithDataContext();
				GoodsModel data = new GoodsModel();
				//從首頁(Index)來
				if (OrderId != "")
				{
					var item = OrderId.Split(",");
					for (int i = 0; i < item.Count(); i++)
					{
						//前台點擊備貨
						if (Status == 1)
						{
							//只有狀態為未接單的訂單能備貨
							var OrderDetail = db.OrderDetail.Where(w => w.OrderId == int.Parse(item[i]) && w.LogisticsStatus == -1);
							foreach (var item2 in OrderDetail)
							{
								item2.LogisticsStatus = Status;
								db.SubmitChanges();
							}
						}

					}
				}
				//從編輯(Edit)來
				if (OrderDetailId != "")
				{
					var item = OrderDetailId.Split(",");
					for (int i = 0; i < item.Count(); i++)
					{
						//前台點擊備貨
						if (Status == 1)
						{
							//只有狀態為未接單的訂單能備貨
							var OrderDetail = db.OrderDetail.FirstOrDefault(w => w.Id == int.Parse(item[i]) && w.LogisticsStatus == -1);

							if (OrderDetail != null)
							{
								OrderDetail.LogisticsStatus = Status;
								db.SubmitChanges();
							}
						}

					}
				}
				db.Connection.Close();
				return Content("Success");
			}
			catch { return Content("Failure "); }
		}

		[HttpPost]
		public ActionResult Session_chk_OrderEdit(string chk_OrderEdit)
		{
			Session["chk_OrderEdit"] = chk_OrderEdit;
			return Content("Success");
		}
		#endregion


		public ActionResult Payment(int Id)
		{
			MithDataContext db = new MithDataContext();

			var data = db.Order.FirstOrDefault(f => f.Id == Id);
			if (data != null)
			{
				data.CashFlowStatus = 2;
				db.SubmitChanges();
			}
			db.Connection.Close();
			return Content("付款完成");
		}
		//[HttpPost]
		//public ActionResult Export(string keyword = "")
		//{
		//    OrderModel data = new OrderModel();
		//    //---------------匯出條件搜索---------------//

		//    if (keyword != null)
		//    {
		//        data.Keyword = keyword;
		//    }
		//    //---------------匯出條件搜索end---------------//
		//    return File(data.Set_Excel(data.Export_Data(data)), "application/vnd.ms-excel", "訂單資料.xls");
		//}
	}
}

