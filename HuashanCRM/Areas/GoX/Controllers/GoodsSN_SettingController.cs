﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text;
using System.IO;
using Ionic.Zip;
using System.IO.Compression;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Mith.Models;
using Mith.Areas.GoX.Models;

namespace Mith.Areas.GoX.Controllers
{
    public class GoodsSN_SettingController : CategoryController
    {
        int fun_id = 7;

        [RoleAuthorize(Role = "亞設人員")]
        [MyAuthorize(Com = Competence.Read, function = "商品狀態")]
        public ActionResult Index_Mith(string keyword = "", string sort = "", string Search_Pattern = "0",
            int Search_CompanyId = 0, DateTime? CreateTime_St = null, DateTime? CreateTime_End = null, int Search_SnNotUse = 0, int p = 1, int show_number = 10)
        {
            bool ShowAll = false;
            GoodsSN_SettingModel data = new GoodsSN_SettingModel();
            var fun = FunctionModel.Get_One(fun_id);

            ViewBag.Title = fun.Name;
            ViewBag.Title_link = "<a href=\"\">" + fun.Name + "</a>";

            string get = "";

            if (keyword != "")
            {
                data.Keyword = keyword;
                ViewData["keyword"] = keyword;
                get += Method.Get_URLGet("keyword", keyword);
            }

            if (Search_Pattern != "0")
            {
                data.Search_Pattern = Search_Pattern;
                ViewData["Search_Pattern"] = Search_Pattern;
                get += Method.Get_URLGet("Search_Pattern", Search_Pattern.ToString());
            }
            if (Search_CompanyId != 0)
            {
                data.Search_CompanyId = Search_CompanyId;
                ViewData["Search_CompanyId"] = Search_CompanyId;
                get += Method.Get_URLGet("Search_CompanyId", Search_CompanyId.ToString());
            }
            if (CreateTime_St != null)
            {
                data.CreateTime_St = CreateTime_St;
                ViewData["CreateTime_St"] = CreateTime_St;
                get += Method.Get_URLGet("CreateTime_St", CreateTime_St.Value.ToString("yyyy/MM/dd HH:mm:ss"));
                ShowAll = true;
            }
            if (CreateTime_End != null)
            {
                data.CreateTime_End = CreateTime_End;
                ViewData["CreateTime_End"] = CreateTime_End;
                get += Method.Get_URLGet("CreateTime_End", CreateTime_End.Value.ToString("yyyy/MM/dd HH:mm:ss"));
                ShowAll = true;
            }
            if (Search_SnNotUse != 0)
            {
                data.Search_SnNotUse = Search_SnNotUse;
                ViewData["Search_SnNotUse"] = Search_SnNotUse;
                get += Method.Get_URLGet("Search_SnNotUse", Search_SnNotUse.ToString());
            }
            data.Sort = sort;
            get += Method.Get_URLGet("sort", sort);

            ViewData["p"] = p;
            ViewData["page"] = data.Get_Page(p, show_number);
            ViewData["keyword"] = keyword;
            ViewData["get"] = get;
            ViewData["ShowAll"] = ShowAll;
            Session["Url"] = Request.Url.AbsoluteUri;
            return View(data.Get_Data_Mith(p, show_number, ShowAll));
        }

        [RoleAuthorize(Role = "廠商")]
        [MyAuthorize(Com = Competence.Read, function = "商品狀態")]
        public ActionResult Index_Company(string keyword = "", string sort = "", string Search_Pattern = "0", DateTime? CreateTime_St = null, DateTime? CreateTime_End = null, int Search_SnNotUse = 0, int p = 1, int show_number = 10)
        {
            bool ShowAll = false;
            GoodsSN_SettingModel data = new GoodsSN_SettingModel();
            var fun = FunctionModel.Get_One(fun_id);

            ViewBag.Title = fun.Name;
            ViewBag.Title_link = "<a href=\"\">" + fun.Name + "</a>";

            string get = "";

            if (keyword != "")
            {
                data.Keyword = keyword;
                ViewData["keyword"] = keyword;
                get += Method.Get_URLGet("keyword", keyword);
            }

            if (Search_Pattern != "0")
            {
                data.Search_Pattern = Search_Pattern;
                ViewData["Search_Pattern"] = Search_Pattern;
                get += Method.Get_URLGet("Search_Pattern", Search_Pattern.ToString());
            }
            if (CreateTime_St != null)
            {
                data.CreateTime_St = CreateTime_St;
                ViewData["CreateTime_St"] = CreateTime_St;
                get += Method.Get_URLGet("CreateTime_St", CreateTime_St.Value.ToString("yyyy/MM/dd HH:mm:ss"));
                ShowAll = true;
            }
            if (CreateTime_End != null)
            {
                data.CreateTime_End = CreateTime_End;
                ViewData["CreateTime_End"] = CreateTime_End;
                get += Method.Get_URLGet("CreateTime_End", CreateTime_End.Value.ToString("yyyy/MM/dd HH:mm:ss"));
                ShowAll = true;
            }
            if (Search_SnNotUse != 0)
            {
                data.Search_SnNotUse = Search_SnNotUse;
                ViewData["Search_SnNotUse"] = Search_SnNotUse;
                get += Method.Get_URLGet("Search_SnNotUse", Search_SnNotUse.ToString());
            }
            data.Sort = sort;
            get += Method.Get_URLGet("sort", sort);

            data.Search_CompanyId = Mith.Models.Method.Get_CompanyId_Admin(Request.Cookies, Session);

            ViewData["p"] = p;
            ViewData["page"] = data.Get_Page(p, show_number);
            ViewData["keyword"] = keyword;
            ViewData["get"] = get;
            ViewData["ShowAll"] = ShowAll;
            Session["Url"] = Request.Url.AbsoluteUri;
            return View(data.Get_Data_Company(p, show_number, ShowAll));
        }

        [MyAuthorize(Com = Competence.Insert, function = "商品狀態")]
        public ActionResult Create()
        {
            GoodsSN_SettingModel data = new GoodsSN_SettingModel();
            var fun = FunctionModel.Get_One(fun_id);

            ViewBag.Title = fun.Name + " > 新增";
            ViewBag.Title_link = "<a href=\"Index_Mith\">" + fun.Name + "</a> > 新增";
            ViewBag.c_title = fun.Name + "資料";

            return View();
        }

        [ValidateInput(true)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Create(GoodsSN_SettingModel.GoodsSN_SettingShow item, List<HttpPostedFileBase> Files = null)
        {
            GoodsSN_SettingModel data = new GoodsSN_SettingModel();
            var fun = FunctionModel.Get_One(fun_id);
            if (ModelState.IsValid)
            {
                #region 新增圖片 ( 複選多張 )
                if (Files.Any() && Files[0] != null)
                {
                    foreach (var i in Files)
                    {
                        string SN = i.FileName.Substring(0, 15);
                        string Pic_Type = i.FileName.Substring(15, 1);
                        string Path_Big = null;
                        string Path_Middle = null;
                        string Path_Small = null;
                        string sContainer = "goods-sn";
                        string sImagePath = "Big/" + SN + "/";
                        string FileExtension = System.IO.Path.GetExtension(i.FileName);
                        string name = Method.RandomKey(5, true, false, false, false) + Pic_Type;

                        Size Middle = new Size();
                        Middle = new Size(540, 960);
                        Size Small = new Size();
                        Small = new Size(360, 640);

                        if (Pic_Type == "1")
                        {
                            Path_Big = BlobHelper.Upload_Blob(new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + FileExtension }, i.InputStream, name + FileExtension, Middle, Small, false)[0];
                        }
                        else
                        {
                            Path_Big = BlobHelper.Upload_Blob(new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + FileExtension }, i.InputStream, name + FileExtension, Middle, Small, true)[0];
                            Path_Middle = Path_Big.Replace("Big", "Middle");
                            Path_Small = Path_Big.Replace("Big", "Small");
                        }

                        int count = data.CheckSN_Count(SN);
                        //如果Count為0，代表沒有此序號進行Insert
                        if (count == 0)
                        {
                            data.Insert_multiple(Path_Big, Path_Middle, Path_Small, SN, Pic_Type);
                        }
                        //否則進行Update
                        else
                        {
                            data.Update_multiple(Path_Big, Path_Middle, Path_Small, SN, Pic_Type);
                        }
                    }
                }
                #endregion
            }

            TempData["FromAction"] = "新增成功！";
            return RedirectToAction("Index_Mith");
        }

        [MyAuthorize(Com = Competence.Update, function = "商品狀態")]
        public ActionResult Edit(int id = 0, int p = 1)
        {
            GoodsSN_SettingModel data = new GoodsSN_SettingModel();
            var fun = FunctionModel.Get_One(fun_id);

            ViewBag.Title = fun.Name + " > 編輯";
            ViewBag.Title_link = "<a href=\"../Index_Mith\">" + fun.Name + "</a> > 編輯";
            ViewBag.c_title = fun.Name + "資料";
            ViewData["p"] = p;
            var item = data.Get_One(id);
            if (item == null)
            {
                return RedirectToAction("Create");
            }
            return View(item);
        }

        [ValidateInput(true)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Edit(GoodsSN_SettingModel.GoodsSN_SettingShowEdit item,
        HttpPostedFileBase Pic1_Big, HttpPostedFileBase Pic2_Big, HttpPostedFileBase Pic3_Big,
            HttpPostedFileBase Pic4_Big, HttpPostedFileBase Pic5_Big, int p = 1)
        {
            string url = (string)Session["Url"];
            GoodsSN_SettingModel data = new GoodsSN_SettingModel();
            var fun = FunctionModel.Get_One(fun_id);
            item.LastUser_Id = Method.Get_UserId_Admin(Request.Cookies, Session);

            if (ModelState.IsValid)
            {
                string sContainer = "goods-sn";
                string sImagePath = "Big/" + item.SN + "/";
                string FileExtension = "";
                string name = Method.RandomKey(5, true, false, false, false);

                Size Middle = new Size();
                Middle = new Size(540, 960);
                Size Small = new Size();
                Small = new Size(360, 640);

                #region 更新圖片

                //商品圖
                if (Pic1_Big != null)
                {
                    name = name + "1";
                    FileExtension = System.IO.Path.GetExtension(Pic1_Big.FileName);
                    item.Pic1_Big = BlobHelper.Upload_Blob(new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + FileExtension }, Pic1_Big.InputStream, name + FileExtension, Middle, Small, false)[0];
                    data.UpdatePic(item.Pic1_Big, null, null, item.SN, "1");
                }
                //不穿外套 - 長版
                if (Pic2_Big != null)
                {
                    name = name + "2";
                    FileExtension = System.IO.Path.GetExtension(Pic2_Big.FileName);

                    item.Pic2_Big = BlobHelper.Upload_Blob(new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + FileExtension }, Pic2_Big.InputStream, name + FileExtension, Middle, Small, true)[0];

                    item.Pic2_Middle = item.Pic2_Big.Replace("Big", "Middle");
                    item.Pic3_Small = item.Pic2_Big.Replace("Big", "Small");

                    data.UpdatePic(item.Pic2_Big, item.Pic2_Middle, item.Pic3_Small, item.SN, "2");
                }
                //不穿外套 - 短版
                if (Pic3_Big != null)
                {
                    name = name + "3";
                    FileExtension = System.IO.Path.GetExtension(Pic3_Big.FileName);

                    item.Pic3_Big = BlobHelper.Upload_Blob(new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + FileExtension }, Pic3_Big.InputStream, name + FileExtension, Middle, Small, true)[0];

                    item.Pic3_Middle = item.Pic3_Big.Replace("Big", "Middle");
                    item.Pic3_Small = item.Pic3_Big.Replace("Big", "Small");

                    data.UpdatePic(item.Pic3_Big, item.Pic3_Middle, item.Pic3_Small, item.SN, "3");
                }
                if (Pic4_Big != null)
                {
                    name = name + "4";
                    FileExtension = System.IO.Path.GetExtension(Pic4_Big.FileName);

                    item.Pic4_Big = BlobHelper.Upload_Blob(new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + FileExtension }, Pic4_Big.InputStream, name + FileExtension, Middle, Small, true)[0];

                    item.Pic4_Middle = item.Pic4_Big.Replace("Big", "Middle");
                    item.Pic4_Small = item.Pic4_Big.Replace("Big", "Small");

                    data.UpdatePic(item.Pic4_Big, item.Pic4_Middle, item.Pic4_Small, item.SN, "4");
                }
                if (Pic5_Big != null)
                {
                    name = name + "5";
                    FileExtension = System.IO.Path.GetExtension(Pic5_Big.FileName);

                    item.Pic5_Big = BlobHelper.Upload_Blob(new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + FileExtension }, Pic5_Big.InputStream, name + FileExtension, Middle, Small, true)[0];

                    item.Pic5_Middle = item.Pic5_Big.Replace("Big", "Middle");
                    item.Pic5_Small = item.Pic5_Big.Replace("Big", "Small");

                    data.UpdatePic(item.Pic5_Big, item.Pic5_Middle, item.Pic5_Small, item.SN, "5");
                }
                #endregion

                if (data.Update_One(item) <= 0)
                {
                    ErrorRecordModel.Error_Record(Method.Get_UserId_Admin(Request.Cookies, Session), "GoodsSN_Setting_5", Request.UserAgent, true, "資料新增失敗");
                    TempData["err"] = "GoodsSN_Setting_5";
                    return RedirectToAction("Index_Mith");
                }

            }
            TempData["FromAction"] = "儲存成功！";
            if(!string.IsNullOrWhiteSpace(url))
            {
                return Redirect(url);
            }
            else
            {
                return RedirectToAction("Index_Mith");
            }
        }

        [MyAuthorize(Com = Competence.Delete, function = "商品狀態")]
        public ActionResult Delete(int item, int p = 1)
        {
            GoodsSN_SettingModel data = new GoodsSN_SettingModel();
            data.Delete(item);
            TempData["FromAction"] = "刪除成功！";
            return RedirectToAction("Index_Mith", new { p = p });
        }

        [MyAuthorize(Com = Competence.Delete, function = "商品狀態")]
        [HttpPost]
        public ActionResult Delete(int[] item)
        {

            GoodsSN_SettingModel data = new GoodsSN_SettingModel();
            data.Delete(item);
            TempData["FromAction"] = "刪除成功！";
            return RedirectToAction("Index_Mith");
        }

        #region 匯入
        private string ImportPath = "/upload/Import";
        /// <summary>
        /// Step1 將檔案上傳
        /// </summary>
        /// <param name="file">The file.</param>
        /// <returns>Json</returns>
        [HttpPost]
        public ActionResult Upload(HttpPostedFileBase file)
        {
            JObject jo = new JObject();
            string result = string.Empty;
            string fileExtName = Path.GetExtension(file.FileName).ToLower();

            if (!fileExtName.Equals(".xls", StringComparison.OrdinalIgnoreCase))
            {
                jo.Add("Result", false);
                jo.Add("Msg", "請上傳 .xls格式的檔案");
                result = JsonConvert.SerializeObject(jo);
                return Content(result, "application/json");
            }

            try
            {
                var uploadResult = this.FileUploadHandler(file);

                jo.Add("Result", !string.IsNullOrWhiteSpace(uploadResult));
                jo.Add("Msg", !string.IsNullOrWhiteSpace(uploadResult) ? uploadResult : "");
                result = JsonConvert.SerializeObject(jo);
            }
            catch (Exception ex)
            {
                jo.Add("Result", false);
                jo.Add("Msg", ex.Message);
                result = JsonConvert.SerializeObject(jo);
            }
            return Content(result, "application/json");
        }

        /// <summary>
        /// Step2 檔案上傳完，檢查檔案
        /// </summary>
        /// <param name="file">The file.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">file;上傳失敗：沒有檔案！</exception>
        /// <exception cref="System.InvalidOperationException">上傳失敗：檔案沒有內容！</exception>
        private string FileUploadHandler(HttpPostedFileBase file)
        {
            string result;

            if (file == null)
            {
                throw new ArgumentNullException("file", "上傳失敗：沒有檔案！");
            }
            if (file.ContentLength <= 0)
            {
                throw new InvalidOperationException("上傳失敗：檔案沒有內容！");
            }

            try
            {
                string virtualBaseFilePath = Url.Content(ImportPath);
                string filePath = HttpContext.Server.MapPath(virtualBaseFilePath);

                if (!Directory.Exists(filePath))
                {
                    Directory.CreateDirectory(filePath);
                }

                string newFileName = string.Concat(
                    DateTime.Now.ToString("yyyyMMddHHmmss"),
                    Path.GetExtension(file.FileName).ToLower());

                string fullFilePath = Path.Combine(Server.MapPath(ImportPath), newFileName);
                file.SaveAs(fullFilePath);

                result = newFileName;
            }
            catch (Exception ex)
            {
                throw;
            }
            return result;
        }

        /// <summary>
        /// Step3 取得檔案路徑，進行匯入
        /// </summary>
        /// <param name="savedFileName">檔案名稱</param>
        /// <returns>Json</returns>
        [HttpPost]
        public ActionResult Import(string savedFileName)
        {
            var jo = new JObject();
            string result;
            var fileName = string.Concat(Server.MapPath(ImportPath), "/", savedFileName);
            try
            {

                var importSN = new List<GoodsSN_SettingModel.ImportClass>();

                GoodsSN_SettingModel data = new GoodsSN_SettingModel();
                //檢查資料
                var checkResult = data.CheckImportData(fileName, importSN);

                jo.Add("Result", checkResult.Success);
                jo.Add("Msg", checkResult.Success ? string.Empty : checkResult.ErrorMessage);
                jo.Add("RowCount", checkResult.RowCount);
                //資料無誤
                if (checkResult.Success)
                {
                    //將資料Insert到DB
                    data.InsertImportData(fileName, importSN);
                }
                result = JsonConvert.SerializeObject(jo);
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                //一定會刪除此檔案
                if (System.IO.File.Exists(fileName))
                {
                    System.IO.File.Delete(fileName);
                }
            }
            return Content(result, "application/json");
        }
        #endregion

        #region Ajax
        [HttpPost]
        public ActionResult Delete_Ajax(string SN)
        {
            try
            {
                MithDataContext db = new MithDataContext();
                var item = SN.Split(",");
                for (int i = 0; i < item.Count(); i++)
                {
                    var data = db.GoodsSN_Setting.FirstOrDefault(f => f.SN == item[i]);
                    int Count = db.Goods.Where(w => w.SN == item[i]).Count();
                    if (data != null && Count == 0)
                    {
                        BlobHelper.delete("goods-sn/" + "Big/" + SN, Path.GetFileName(data.Pic1_Big));

                        BlobHelper.delete("goods-sn/" + "Big/" + SN, Path.GetFileName(data.Pic2_Big));
                        BlobHelper.delete("goods-sn/" + "Middle/" + SN, Path.GetFileName(data.Pic2_Middle));
                        BlobHelper.delete("goods-sn/" + "Small/" + SN, Path.GetFileName(data.Pic2_Small));

                        BlobHelper.delete("goods-sn/" + "Big/" + SN, Path.GetFileName(data.Pic3_Big));
                        BlobHelper.delete("goods-sn/" + "Middle/" + SN, Path.GetFileName(data.Pic3_Middle));
                        BlobHelper.delete("goods-sn/" + "Small/" + SN, Path.GetFileName(data.Pic3_Small));

                        BlobHelper.delete("goods-sn/" + "Big/" + SN, Path.GetFileName(data.Pic4_Big));
                        BlobHelper.delete("goods-sn/" + "Middle/" + SN, Path.GetFileName(data.Pic4_Middle));
                        BlobHelper.delete("goods-sn/" + "Small/" + SN, Path.GetFileName(data.Pic4_Small));

                        BlobHelper.delete("goods-sn/" + "Big/" + SN, Path.GetFileName(data.Pic5_Big));
                        BlobHelper.delete("goods-sn/" + "Middle/" + SN, Path.GetFileName(data.Pic5_Middle));
                        BlobHelper.delete("goods-sn/" + "Small/" + SN, Path.GetFileName(data.Pic5_Small));

                        db.GoodsSN_Setting.DeleteOnSubmit(data);
                        db.SubmitChanges();
                    }
                }
                db.SubmitChanges();
                db.Connection.Close();
                return Content("Success");
            }
            catch { return Content("Failure "); }
        }
        #endregion

        #region 多個檔案下載 & 上傳
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult MiupleUpload(HttpPostedFileBase fileData)
        {
            try
            {
                GoodsSN_SettingModel data = new GoodsSN_SettingModel();
                string SN = fileData.FileName.Substring(0, 15);
                string Pic_Type = fileData.FileName.Substring(15, 1);
                string Path_Big = null;
                string Path_Middle = null;
                string Path_Small = null;
                string sContainer = "goods-sn";
                string sImagePath = "Big/" + SN + "/";
                string FileExtension = System.IO.Path.GetExtension(fileData.FileName);
                string name = Method.RandomKey(5, true, false, false, false) + Pic_Type;

                Size Middle = new Size();
                Middle = new Size(540, 960);
                Size Small = new Size();
                Small = new Size(360, 640);

                if (Pic_Type == "1")
                {
                    Path_Big = BlobHelper.Upload_Blob(new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + FileExtension }, fileData.InputStream, name + FileExtension, Middle, Small, false)[0];
                }
                else
                {
                    Path_Big = BlobHelper.Upload_Blob(new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + FileExtension }, fileData.InputStream, name + FileExtension, Middle, Small, true)[0];
                    Path_Middle = Path_Big.Replace("Big", "Middle");
                    Path_Small = Path_Big.Replace("Big", "Small");
                }

                int count = data.CheckSN_Count(SN);
                //如果Count為0，代表沒有此序號進行Insert
                if (count == 0)
                {
                    data.Insert_multiple(Path_Big, Path_Middle, Path_Small, SN, Pic_Type);
                }
                //否則進行Update
                else
                {
                    data.Update_multiple(Path_Big, Path_Middle, Path_Small, SN, Pic_Type);
                }

                return Json(new { Success = true });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult Download(string SN = "")
        {
            if (SN != "")
            {
                byte[] zipContent = null;
                var stream = new MemoryStream();
                using (ZipFile zip = new ZipFile())
                {
                    MithDataContext db = new MithDataContext();
                    BlobHelper rep = new BlobHelper();

                    var data = SN.Split(",");

                    foreach (var sn in data)
                    {
                        var GoodsSN_Setting = db.GoodsSN_Setting.Where(w => w.SN == sn);
                        if (GoodsSN_Setting.Any() && GoodsSN_Setting != null)
                        {
                            foreach (var item in GoodsSN_Setting)
                            {
                                byte[] b;
                                if (!string.IsNullOrWhiteSpace(item.Pic1_Big))
                                {
                                    b = rep.DownloadFileFromBlob(sn, Path.GetFileName(item.Pic1_Big));
                                    zip.AddEntry(sn + ".png", b);

                                }
                            }
                        }

                    }
                    zip.Save(stream);
                    zipContent = stream.ToArray();
                    db.Connection.Close();
                    return File(zipContent, "zip", "Mith_穿搭圖.zip");
                    // use that stream for your usage.
                }
            }
            else
            {
                return null;
            }
        }
        #endregion

    }
}

