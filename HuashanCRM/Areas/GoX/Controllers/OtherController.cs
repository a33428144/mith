﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;
using System.Text;
using System.IO;
using Mith.Models;
using Mith.Areas.GoX.Models;

namespace Mith.Areas.GoX.Controllers
{
    public class OtherController : CategoryController
    {
        int fun_id = 17;

        #region 首頁
        [RoleAuthorize(Role = "亞設人員")]
        [MyAuthorize(Com = Competence.Read, function = "其它設定")]
        public ActionResult Index(string keyword = "", int p = 1, int show_number = 20)
        {
            OtherModel data = new OtherModel();
            var fun = FunctionModel.Get_One(fun_id);
            string get = "";
            if (keyword != "")
            {
                data.Keyword = keyword;
                ViewData["keyword"] = keyword;
                get += Method.Get_URLGet("keyword", keyword);
            }
            ViewData["get"] = get;

            ViewBag.Title = fun.Name;
            ViewBag.Title_link = " <a href=\"\">" + fun.Name + "</a>";

            ViewData["p"] = p;
            ViewData["page"] = data.Get_Page(p, show_number);
            ViewData["number"] = show_number;
            ViewData["get"] = get;
            Session["Url"] = Request.Url.AbsoluteUri;

            return View(data.Get_Data(p, show_number));
        }

        #endregion

        #region 編輯

        [MyAuthorize(Com = Competence.Update, function = "其它設定")]
        public ActionResult Edit(int id = 0)
        {
            MithDataContext db = new MithDataContext();
            OtherModel data = new OtherModel();
            var fun = FunctionModel.Get_One(fun_id);

            ViewBag.Title = " " + fun.Name + " > 編輯";
            ViewBag.Title_link = " <a href=\"Index\">" + fun.Name + "</a> > 編輯";
            ViewBag.c_title = fun.Name;

            var item = data.Get_One(id);
            if (item == null)
            {
                return RedirectToAction("Create_Step1");
            }
            db.Connection.Close();
            return View(item);
        }

        [MyAuthorize(Com = Competence.Update, function = "其它設定")]
        [ValidateInput(true)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Edit(OtherModel.OtherShow item, HttpPostedFileBase Pic_Big)
        {
            MithDataContext db = new MithDataContext();
            OtherModel data = new OtherModel();
            string url = (string)Session["Url"];
            if (ModelState.IsValid)
            {
                #region 更新圖片
                if (Pic_Big != null)
                {
                    string sContainer = "other";
                    string sImagePath = "Big/" + item.Id + "/";
                    string FileExtension = System.IO.Path.GetExtension(Pic_Big.FileName);
                    string name = Method.RandomKey(5, true, false, false, false);

                    Size Middle = new Size();
                    Middle = new Size(360, 360);
                    Size Small = new Size();
                    Small = new Size(180, 180);


                    //先刪除舊圖片
                    BlobHelper.delete("other/" + "Big/" + item.Id, Path.GetFileName(item.Pic_Big));
                    BlobHelper.delete("other/" + "Middle/" + item.Id, Path.GetFileName(item.Pic_Middle));
                    BlobHelper.delete("other/" + "Small/" + item.Id, Path.GetFileName(item.Pic_Small));

                    //上傳新圖片
                    item.Pic_Big = BlobHelper.Upload_Blob(new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + FileExtension }, Pic_Big.InputStream, name + FileExtension, Middle, Small, true)[0];
                    item.Pic_Middle = item.Pic_Big.Replace("Big", "Middle");
                    item.Pic_Small = item.Pic_Big.Replace("Big", "Small");
                }
                #endregion

                if (data.Update(item) <= 0)
                {
                    ErrorRecordModel.Error_Record(Method.Get_UserId_Admin(Request.Cookies, Session), "Other_5", Request.UserAgent, true, "資料新增失敗");
                    TempData["err"] = "Other_5";
                    return RedirectToAction("Index");
                }
            }

            db.Connection.Close();
            TempData["FromAction"] = "儲存成功！";
            if(!string.IsNullOrWhiteSpace(url))
            {
                return Redirect(url);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        #endregion

    }
}

