﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;
using System.Text;
using System.IO;
using Mith.Models;
using Mith.Areas.GoX.Models;

namespace Mith.Areas.GoX.Controllers
{
    public class BrandController : CategoryController
    {
        int fun_id = 6;

        #region 首頁
                [RoleAuthorize(Role = "亞設人員")]
        [MyAuthorize(Com = Competence.Read, function = "品牌資料")]
        public ActionResult Index(string keyword = "", int Search_ComapnyId = 0, DateTime? CreateTime_St = null, DateTime? CreateTime_End = null, int p = 1,
            int show_number = 10)
        {
            MithDataContext db = new MithDataContext();
            BrandModel data = new BrandModel();
            var fun = FunctionModel.Get_One(fun_id);
            string get = "";
            if (keyword != "")
            {
                data.Keyword = keyword;
                ViewData["keyword"] = keyword;
                get += Method.Get_URLGet("keyword", keyword);
            }
            if (Search_ComapnyId != 0)
            {
                data.Search_ComapnyId = Search_ComapnyId;
                ViewData["Search_ComapnyId"] = Search_ComapnyId;
                get += Method.Get_URLGet("Search_ComapnyId", Search_ComapnyId.ToString());
            }
            if (CreateTime_St != null)
            {
                data.CreateTime_St = CreateTime_St;
                ViewData["CreateTime_St"] = CreateTime_St;
                get += Method.Get_URLGet("CreateTime_St", CreateTime_St.Value.ToString("yyyy/MM/dd HH:mm:ss"));
            }
            if (CreateTime_End != null)
            {
                data.CreateTime_End = CreateTime_End;
                ViewData["CreateTime_End"] = CreateTime_End;
                get += Method.Get_URLGet("CreateTime_End", CreateTime_End.Value.ToString("yyyy/MM/dd HH:mm:ss"));
            }

            ViewBag.Title = fun.Name;
            ViewBag.Title_link = "廠商管理 > <a href=\"\">" + fun.Name + "</a>";

            ViewData["p"] = p;
            ViewData["page"] = data.Get_Page(p, show_number);
            ViewData["number"] = show_number;
            ViewData["get"] = get;
            Session["Url"] = Request.Url.AbsoluteUri;
            db.Connection.Close();
            return View(data.Get_Data(p, show_number));
        }

        #endregion

        #region 新增

        [MyAuthorize(Com = Competence.Insert, function = "品牌資料")]
        public ActionResult Create()
        {
            MithDataContext db = new MithDataContext();
            BrandModel data = new BrandModel();
            var fun = FunctionModel.Get_One(fun_id);

            ViewBag.Title = "廠商管理 > " + fun.Name + " > 新增 ";
            ViewBag.Title_link = "廠商管理 > <a href=\"Index\">" + fun.Name + "</a> > 新增 ";
            ViewBag.c_title = fun.Name;
            db.Connection.Close();
            return View();
        }

        [ValidateInput(true)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Create(BrandModel.BrandShow item, HttpPostedFileBase Pic_Size)
        {

            MithDataContext db = new MithDataContext();
            BrandModel data = new BrandModel();
            var fun = FunctionModel.Get_One(fun_id);
            item.Id = db.Brand.OrderByDescending(o => o.Id).FirstOrDefault() != null ? db.Brand.OrderByDescending(o => o.Id).FirstOrDefault().Id + 1 : 1;

            if (ModelState.IsValid)
            {
                #region 上傳圖片
                string sContainer = "brand";
                string sImagePath = item.Id + "/";
                string FileExtension = System.IO.Path.GetExtension(Pic_Size.FileName);
                string name = Method.RandomKey(5, true, false, false, false);

                Size Middle = new Size();
                Size Small = new Size();

                if (Pic_Size != null)
                {
                    //上傳新圖片
                    item.Pic_Size = BlobHelper.Upload_Blob(new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + FileExtension }, Pic_Size.InputStream, name + FileExtension, Middle, Small, false)[0];
                }
                #endregion

                if (data.Insert(item) <= 0)
                {
                    ErrorRecordModel.Error_Record(Method.Get_UserId_Admin(Request.Cookies, Session), "Brand_2", Request.UserAgent, true, "資料新增失敗");
                    TempData["err"] = "Brand_2，資料新增失敗";
                }
            }
            else
            {
                ErrorRecordModel.Error_Record(Method.Get_UserId_Admin(Request.Cookies, Session), "Brand_3", Request.UserAgent, true, "資料新增失敗");
                TempData["err"] = "Brand_3，資料新增失敗";
            }
            db.Connection.Close();
            return RedirectToAction("Index");
        }

        #endregion

        #region 編輯

        [MyAuthorize(Com = Competence.Update, function = "品牌資料")]
        public ActionResult Edit(int id = 0)
        {
            MithDataContext db = new MithDataContext();
            BrandModel data = new BrandModel();
            var fun = FunctionModel.Get_One(fun_id);

            ViewBag.Title = "廠商管理 > " + fun.Name + " > 編輯 ";
            ViewBag.Title_link = "廠商管理 > <a href=\"Index\">" + fun.Name + "</a> > 編輯 ";
            ViewBag.c_title = fun.Name;

            var item = data.Get_One(id);
            if (item == null)
            {
                return RedirectToAction("Create");
            }
            db.Connection.Close();
            return View(item);
        }

        [MyAuthorize(Com = Competence.Update, function = "品牌資料")]
        [ValidateInput(true)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Edit(BrandModel.BrandShowEdit item, HttpPostedFileBase Pic_Size)
        {
            BrandModel data = new BrandModel();
            var fun = FunctionModel.Get_One(fun_id);
            string url = (string)Session["Url"];

            if (ModelState.IsValid)
            {

                int user_id = Method.Get_UserId_Admin(Request.Cookies, Session);
                item.LastUserId = user_id;

                #region 更新圖片
                if (Pic_Size != null)
                {
                    string sContainer = "brand";
                    string sImagePath = item.Id + "/";
                    string FileExtension = System.IO.Path.GetExtension(Pic_Size.FileName);
                    string name = Method.RandomKey(5, true, false, false, false);

                    Size Middle = new Size();
                    Size Small = new Size();

                    //先刪除舊圖片
                    BlobHelper.delete(sContainer+"/" + item.Id, Path.GetFileName(item.Pic_Size));

                    //上傳新圖片
                    item.Pic_Size = BlobHelper.Upload_Blob(new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + FileExtension }, Pic_Size.InputStream, name + FileExtension, Middle, Small, false)[0];
                }
                #endregion

                if (data.Update(item) <= 0)
                {
                    ErrorRecordModel.Error_Record(Method.Get_UserId_Admin(Request.Cookies, Session), "Brand_5", Request.UserAgent, true, "資料新增失敗");
                    TempData["err"] = "Brand_5";
                    return RedirectToAction("Index");
                }
            }
            TempData["FromAction"] = "儲存成功！";
            if(!string.IsNullOrWhiteSpace(url))
            {
                return Redirect(url);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        #endregion

        #region 刪除

        [MyAuthorize(Com = Competence.Delete, function = "品牌資料")]
        public ActionResult Delete(int item, int p = 1)
        {
            if (!CheckLogin.CheckPermission(HttpContext, Competence.Delete, fun_id))
            {
                TempData["err"] = Method.Error_Permission;
                return RedirectToAction(Method.GoX_Default_Action, Method.GoX_Default_Controller, new { ReturnUrl = Request.RawUrl });
            }
            TempData["FromAction"] = "刪除成功！";
            BrandModel data = new BrandModel();
            data.Delete(item);

            return RedirectToAction("Index", new { p = p });
        }

        [MyAuthorize(Com = Competence.Delete, function = "品牌資料")]
        [HttpPost]
        public ActionResult Delete(int[] item)
        {
            if (!CheckLogin.CheckPermission(HttpContext, Competence.Delete, fun_id))
            {
                TempData["err"] = Method.Error_Permission;
                return RedirectToAction(Method.GoX_Default_Action, Method.GoX_Default_Controller, new { ReturnUrl = Request.RawUrl });
            }

            BrandModel data = new BrandModel();
            data.Delete(item);

            return RedirectToAction("Index");
        }

        #endregion

        //[HttpPost]
        //public ActionResult Export(string keyword = "")
        //{
        //    BrandModel data = new BrandModel();
        //    //---------------匯出條件搜索---------------//

        //    if (keyword != null)
        //    {
        //        data.Keyword = keyword;
        //    }
        //    //---------------匯出條件搜索end---------------//
        //    return File(data.Set_Excel(data.Export_Data(data)), "application/vnd.ms-excel", "會員資料.xls");
        //}
    }
}

