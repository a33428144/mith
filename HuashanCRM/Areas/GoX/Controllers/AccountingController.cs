﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;
using System.Text;
using System.IO;
using Mith.Models;
using Mith.Areas.GoX.Models;

namespace Mith.Areas.GoX.Controllers
{
	public class AccountingController : CategoryController
	{
		int fun_id = 15;

		#region 首頁
		[MyAuthorize(Com = Competence.Read, function = "對帳明細")]
		public ActionResult Index_Mith(string keyword = "", int Search_Direction = 0, int Search_CashFlow = 0, int Search_CompanyId = 0, int p = 1, int show_number = 10)
		{
			AccountingModel data = new AccountingModel();
			var fun = FunctionModel.Get_One(fun_id);
			string get = "";
			if (keyword != "")
			{
				data.Keyword = keyword;
				ViewData["keyword"] = keyword;
				get += Method.Get_URLGet("keyword", keyword);
			}
			if (Search_Direction != 0)
			{
				data.Search_Direction = Search_Direction;
				ViewData["Search_Direction"] = Search_Direction;
				get += Method.Get_URLGet("Search_Direction", Search_Direction.ToString());
			}
			if (Search_CashFlow != 0)
			{
				data.Search_CashFlow = Search_CashFlow;
				ViewData["Search_CashFlow"] = Search_CashFlow;
				get += Method.Get_URLGet("Search_CashFlow", Search_CashFlow.ToString());
			}
			if (Search_CompanyId != 0)
			{
				data.Search_CompanyId = Search_CompanyId;
				ViewData["Search_CompanyId"] = Search_CompanyId;
				get += Method.Get_URLGet("Search_CompanyId", Search_CompanyId.ToString());
			}

			ViewBag.Title = fun.Name;
			ViewBag.Title_link = "帳務管理 > <a href=\"\">" + fun.Name + "</a>";

			ViewData["p"] = p;
			ViewData["page"] = data.Get_Page(p, show_number);
			ViewData["number"] = show_number;
			ViewData["get"] = get;
			Session["Url"] = Request.Url.AbsoluteUri;
			var Result = data.Get_Data(p, show_number);
			return View(Result);
		}

		[MyAuthorize(Com = Competence.Read, function = "對帳明細")]
		public ActionResult Index_Company(string keyword = "", int Search_Direction = 0, int Search_CashFlow = 0, int p = 1, int show_number = 10)
		{
			AccountingModel data = new AccountingModel();
			var fun = FunctionModel.Get_One(fun_id);
			string get = "";
			if (keyword != "")
			{
				data.Keyword = keyword;
				ViewData["keyword"] = keyword;
				get += Method.Get_URLGet("keyword", keyword);
			}
			if (Search_Direction != 0)
			{
				data.Search_Direction = Search_Direction;
				ViewData["Search_Direction"] = Search_Direction;
				get += Method.Get_URLGet("Search_Direction", Search_Direction.ToString());
			}
			if (Search_CashFlow != 0)
			{
				data.Search_CashFlow = Search_CashFlow;
				ViewData["Search_CashFlow"] = Search_CashFlow;
				get += Method.Get_URLGet("Search_CashFlow", Search_CashFlow.ToString());
			}
			data.Search_CompanyId = Mith.Models.Method.Get_CompanyId_Admin(Request.Cookies, Session);
			ViewData["Search_CompanyId"] = data.Search_CompanyId;

			ViewBag.Title = fun.Name;
			ViewBag.Title_link = "帳務管理 > <a href=\"\">" + fun.Name + "</a>";

			ViewData["p"] = p;
			ViewData["page"] = data.Get_Page(p, show_number);
			ViewData["number"] = show_number;
			ViewData["get"] = get;
			Session["Url"] = Request.Url.AbsoluteUri;
			var Result = data.Get_Data(p, show_number);
			return View(Result);
		}

		#endregion

		#region 編輯
		[MyAuthorize(Com = Competence.Update, function = "對帳明細")]
		public ActionResult Edit_Mith(int id = 0)
		{
			MithDataContext db = new MithDataContext();
			AccountingModel data = new AccountingModel();
			var fun = FunctionModel.Get_One(fun_id);
			data.Search_CompanyId = 0;
			//var item = null;
			//if (item == null)
			//{
			//	return RedirectToAction("Index_Mith");
			//}
			db.Connection.Close();
			ViewBag.Title = "帳務管理 > " + fun.Name + " > 詳情 ";
			ViewBag.Title_link = "帳務管理 > <a href=/Gox/Order/Index_Mith>" + fun.Name + "</a> > 詳情";
			//if (item.Direction == 1)
			//{
			//	return View("Edit_Mith_Forward", item);
			//}
			//else
			//{
			//	return View("Edit_Mith_Backward", item);
			//}
			return View();
		}

		[MyAuthorize(Com = Competence.Update, function = "對帳明細")]
		public ActionResult Edit_Company(int id = 0, string ReturnStoreId = "", string ReturnStoreName = "", string ReturnStoreAddress = "", string From = "Mith")
		{
			MithDataContext db = new MithDataContext();
			OrderModel data = new OrderModel();
			var fun = FunctionModel.Get_One(fun_id);
			data.Search_CompanyId = Mith.Models.Method.Get_CompanyId_Admin(Request.Cookies, Session);

			var item = data.Get_One(id);
			if (item == null)
			{
				return RedirectToAction("Index_Mith");
			}
			db.Connection.Close();
			ViewBag.Title = "帳務管理 > " + fun.Name + " > 詳情 ";
			ViewBag.Title_link = "帳務管理 > <a href=/Gox/Order/Index_Company>" + fun.Name + "</a> > 詳情";

			//廠商編號
			int CompanyId = Mith.Models.Method.Get_CompanyId_Admin(Request.Cookies, Session);

			if (From == "Mith")
			{
				List<OrderDetail_Edit_View> OrderDetail = db.OrderDetail_Edit_View.Where(f => f.CompanyId == CompanyId && f.ReturnStoreId.Length > 1).OrderByDescending(o => o.Id).Take(1).ToList();

				if (OrderDetail.Count() > 0)
				{
					foreach (var item2 in OrderDetail)
					{
						ViewData["ReturnStoreId"] = item2.ReturnStoreId;
						ViewData["ReturnStoreName"] = item2.ReturnStoreName;
						ViewData["ReturnStoreAddress"] = item2.ReturnStoreAddress;
					}
				}
			}
			else
			{
				ViewData["ReturnStoreId"] = ReturnStoreId;
				ViewData["ReturnStoreName"] = ReturnStoreName;
				ViewData["ReturnStoreAddress"] = ReturnStoreAddress;
			}

			if (item.Direction == 1)
			{
				ViewData["From"] = From;
				return View("Edit_Company_Forward", item);
			}
			else
			{
				return View("Edit_Company_Backward", item);
			}

		}
		#endregion

		[HttpPost]
		public ActionResult Export(string keyword = "", int Search_Direction = 0, int Search_CashFlow = 0, int Search_CompanyId = 0)
		{
			AccountingModel data = new AccountingModel();
			//---------------匯出條件搜索---------------//

			if (keyword != null)
			{
				data.Keyword = keyword;
			}
			if (Search_Direction != 0)
			{
				data.Search_Direction = Search_Direction;
			}
			if (Search_CashFlow != 0)
			{
				data.Search_CashFlow = Search_CashFlow;
			}
			if (Search_CompanyId != 0)
			{
				data.Search_CompanyId = Search_CompanyId;
			}
			//---------------匯出條件搜索end---------------//
			return File(data.Set_Excel_Mith(data.Export_Data(data)), "application/vnd.ms-excel", "對帳明細.xls");
		}

	}
}

