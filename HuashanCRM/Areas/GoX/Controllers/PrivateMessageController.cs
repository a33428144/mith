﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;
using System.Text;
using System.IO;
using Mith.Models;
using Mith.Areas.GoX.Models;

namespace Mith.Areas.GoX.Controllers
{
    public class PrivateMessageController : CategoryController
    {
        int fun_id = 26;

        #region 首頁

        //[MyAuthorize(Com = Competence.Read, function = "私訊會員")]
        public ActionResult Index_Group(string keyword = "", int ExpertId = 0, DateTime? CreateTime_St = null, DateTime? CreateTime_End = null,  int p = 1, int show_number = 10)
        {
            MithDataContext db = new MithDataContext();
            PrivateMessageModel data = new PrivateMessageModel();
            var fun = FunctionModel.Get_One(fun_id);
            string get = "";

            if (ExpertId != 0)
            {
                ViewData["ExpertId"] = ExpertId;
                ViewData["Expert_Name"] = db.Expert_View.FirstOrDefault(f => f.Id == ExpertId).Name;
                get += Method.Get_URLGet("ExpertId", ExpertId.ToString());
            }
            if (keyword != "")
            {
                data.Keyword = keyword;
                ViewData["keyword"] = keyword;
                get += Method.Get_URLGet("keyword", keyword);
            }

            if (CreateTime_St != null)
            {
                data.CreateTime_St = CreateTime_St;
                ViewData["CreateTime_St"] = CreateTime_St;
                get += Method.Get_URLGet("CreateTime_St", CreateTime_St.Value.ToString("yyyy/MM/dd HH:mm:ss"));
            }
            if (CreateTime_End != null)
            {
                data.CreateTime_End = CreateTime_End;
                ViewData["CreateTime_End"] = CreateTime_End;
                get += Method.Get_URLGet("CreateTime_End", CreateTime_End.Value.ToString("yyyy/MM/dd HH:mm:ss"));
            }

            ViewBag.Title = fun.Name;
            ViewBag.Title_link = "達人管理 > <a href=\"/GoX/Expert/Index\">" + "達人資料" + "</a> ><a href=\"\">" + "私訊會員" + "</a>";

            ViewData["p"] = p;
            ViewData["page"] = data.Get_Page_Group(p, show_number, ExpertId);
            ViewData["number"] = show_number;
            ViewData["get"] = get;
            Session["Url"] = Request.Url.AbsoluteUri;
            db.Connection.Close();
            return View(data.Get_Data_Group(p, show_number, ExpertId));
        }

        //[MyAuthorize(Com = Competence.Read, function = "私訊會員")]
        public ActionResult Index_Content(string keyword = "", int ExpertId = 0, int VolunteersId = 0, int PrivateMessageGroupId = 0, DateTime? CreateTime_St = null, DateTime? CreateTime_End = null,int p = 1, int show_number = 20)
        {
            MithDataContext db = new MithDataContext();
            PrivateMessageModel data = new PrivateMessageModel();
            var fun = FunctionModel.Get_One(fun_id);
            string get = "";

            if (ExpertId != 0)
            {
                ViewData["ExpertId"] = ExpertId;
                ViewData["Expert_Name"] = db.Expert_View.FirstOrDefault(f => f.Id == ExpertId).Name;
                get += Method.Get_URLGet("ExpertId", ExpertId.ToString());
            }
            if (VolunteersId != 0)
            {
                ViewData["VolunteersId"] = VolunteersId;
                ViewData["Volunteers_Name"] = db.Volunteers_View.FirstOrDefault(f => f.Id == VolunteersId) != null ? db.Volunteers_View.FirstOrDefault(f => f.Id == VolunteersId).RealName : "";
                ViewData["Volunteers_Email"] = db.Volunteers.FirstOrDefault(f => f.Id == VolunteersId).Email;
                get += Method.Get_URLGet("VolunteersId", VolunteersId.ToString());
            }
            if (keyword != "")
            {
                data.Keyword = keyword;
                ViewData["keyword"] = keyword;
                get += Method.Get_URLGet("keyword", keyword);
            }

            if (CreateTime_St != null)
            {
                data.CreateTime_St = CreateTime_St;
                ViewData["CreateTime_St"] = CreateTime_St;
                get += Method.Get_URLGet("CreateTime_St", CreateTime_St.Value.ToString("yyyy/MM/dd HH:mm:ss"));
            }
            if (CreateTime_End != null)
            {
                data.CreateTime_End = CreateTime_End;
                ViewData["CreateTime_End"] = CreateTime_End;
                get += Method.Get_URLGet("CreateTime_End", CreateTime_End.Value.ToString("yyyy/MM/dd HH:mm:ss"));
            }

            ViewBag.Title = fun.Name;
            ViewBag.Title_link = "達人管理 > <a href=\"/GoX/Expert/Index\">" + "達人資料" + "</a> >  <a href=\"Index_Group?ExpertId=" + ExpertId + "\">" + "私訊會員" + "</a> > <a href=\"\">" + "私訊內容" + "</a>";

            ViewData["p"] = p;
            ViewData["page"] = data.Get_Page_Group(p, show_number, ExpertId);
            ViewData["number"] = show_number;
            ViewData["get"] = get;
            Session["Url"] = Request.Url.AbsoluteUri;
            db.Connection.Close();
            return View(data.Get_Data_Content(p, show_number, PrivateMessageGroupId));
        }

        #endregion

        #region 刪除

        //[MyAuthorize(Com = Competence.Delete, function = "私訊會員")]
        public ActionResult Delete_Group(int Id, int p = 1)
        {
            PrivateMessageModel data = new PrivateMessageModel();
            data.Delete_Group(Id);
            TempData["FromAction"] = "刪除成功！";
            return RedirectToAction("Index_Group", new { p = p });
        }


        //[MyAuthorize(Com = Competence.Delete, function = "私訊會員")]
        public ActionResult Delete_Content(int Id, int p = 1)
        {
            PrivateMessageModel data = new PrivateMessageModel();
            data.Delete_Content(Id);
            TempData["FromAction"] = "刪除成功！";
            return RedirectToAction("Index_Content", new { p = p });
        }


        #endregion

    }
}

