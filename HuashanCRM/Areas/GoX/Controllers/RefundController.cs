﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;
using System.Text;
using System.IO;
using Mith.Models;
using Mith.Areas.GoX.Models;

namespace Mith.Areas.GoX.Controllers
{
    public class RefundController : CategoryController
    {
        int fun_id = 14;

        #region 首頁
        [MyAuthorize(Com = Competence.Read, function = "退款資料")]
        public ActionResult Index(string keyword = "", int Search_CashFlow = 0, int Search_RefundReason = 0, int Search_CompanyId = 0, DateTime? RefundTime_St = null, DateTime? RefundTime_End = null, int p = 1, int show_number = 10)
        {
            RefundModel data = new RefundModel();
            var fun = FunctionModel.Get_One(fun_id);
            string get = "";
            if (keyword != "")
            {
                data.Keyword = keyword;
                ViewData["keyword"] = keyword;
                get += Method.Get_URLGet("keyword", keyword);
            }
            if (Search_CashFlow != 0)
            {
                data.Search_CashFlow = Search_CashFlow;
                ViewData["Search_CashFlow"] = Search_CashFlow;
                get += Method.Get_URLGet("Search_CashFlow", Search_CashFlow.ToString());
            }
            if (Search_RefundReason != 0)
            {
                data.Search_RefundReason = Search_RefundReason;
                ViewData["Search_RefundReason"] = Search_RefundReason;
                get += Method.Get_URLGet("Search_RefundReason", Search_RefundReason.ToString());
            }
            if (Search_CompanyId > 0)
            {
                data.Search_CompanyId = Search_CompanyId;
                ViewData["Search_CompanyId"] = Search_CompanyId;
                get += Method.Get_URLGet("Search_CompanyId", Search_CompanyId.ToString());
            }
            if (RefundTime_St != null)
            {
                data.RefundTime_St = RefundTime_St;
                ViewData["RefundTime_St"] = RefundTime_St;
                get += Method.Get_URLGet("RefundTime_St", RefundTime_St.Value.ToString("yyyy/MM/dd HH:mm:ss"));
            }
            if (RefundTime_End != null)
            {
                data.RefundTime_End = RefundTime_End;
                ViewData["RefundTime_End"] = RefundTime_End;
                get += Method.Get_URLGet("RefundTime_End", RefundTime_End.Value.ToString("yyyy/MM/dd HH:mm:ss"));
            }

            ViewBag.Title = fun.Name;
            ViewBag.Title_link = "帳務管理 > <a href=\"\">" + fun.Name + "</a>";

            ViewData["p"] = p;
            ViewData["page"] = data.Get_Page(p, show_number);
            ViewData["number"] = show_number;
            ViewData["get"] = get;
            Session["Url"] = Request.Url.AbsoluteUri;
            var Result = data.Get_Data(p, show_number);
            return View(Result);
        }

        #endregion

        #region 編輯

        [MyAuthorize(Com = Competence.Update, function = "退款資料")]
        public ActionResult Edit(int Id = 0)
        {
            MithDataContext db = new MithDataContext();
            RefundModel data = new RefundModel();
            var fun = FunctionModel.Get_One(fun_id);

            var item = data.Get_One(Id);
            if (item == null)
            {
                return RedirectToAction("Index_Mith");
            }
            db.Connection.Close();
            ViewBag.Title = "帳務管理 > " + fun.Name + " > 詳情 ";
            ViewBag.Title_link = "帳務管理 > <a href=/Gox/Refund/Index>" + fun.Name + "</a> > 詳情";
            return View(item);

        }

        [MyAuthorize(Com = Competence.Update, function = "退款資料")]
        [ValidateInput(true)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Edit(RefundModel.RefundShow item)
        {
            RefundModel data = new RefundModel();
            var fun = FunctionModel.Get_One(fun_id);
            string url = (string)Session["Url"];
            item.LastUserId = Method.Get_UserId_Admin(Request.Cookies, Session);

            #region ATM退款
            if (item.CashFlowType == 1)
            {
                if (data.Update(item, "") <= 0)
                {
                    ErrorRecordModel.Error_Record(Method.Get_UserId_Admin(Request.Cookies, Session), "Refund_5", Request.UserAgent, true, "資料新增失敗");
                    TempData["err"] = "Refund_5";
                    return RedirectToAction("Index");
                }

                TempData["FromAction"] = "儲存成功！";
                if (!string.IsNullOrWhiteSpace(url))
                {
                    return Redirect(url);
                }
                else
                {
                    return RedirectToAction("Index_Company");
                }
            }
            #endregion

            #region 信用卡刷退
            else
            {
                string Status = "";

                //金流 = 未退款
                if (item.CashFlowStatus == 3)
                {
                    Status = WebApiMethod.Refund_CreditCard(Server, item.SN, item.RefundTotal.ToString(), item.CathayAuthCode);

                    if (Status != "0000")
                    {
                        return Content("信用卡刷退失敗，請至國泰E-POS系統查看");
                    }
                }
                if (data.Update(item, Status) <= 0)
                {
                    ErrorRecordModel.Error_Record(Method.Get_UserId_Admin(Request.Cookies, Session), "Refund_5", Request.UserAgent, true, "資料新增失敗");
                    TempData["err"] = "Refund_5";
                    return RedirectToAction("Index");
                }

                TempData["FromAction"] = "儲存成功！";
                if (!string.IsNullOrWhiteSpace(url))
                {
                    return Redirect(url);
                }
                else
                {
                    return RedirectToAction("Index_Company");
                }
            }
            #endregion
        }

        #endregion

        #region Ajax
        //更新訂單狀態
        //[HttpPost]
        //public ActionResult Update_Status(string OrderId = "", string OrderDetailId = "", int Status = 0)
        //{
        //    try
        //    {
        //        MithDataContext db = new MithDataContext();
        //        GoodsModel data = new GoodsModel();
        //        //從首頁(Index)來
        //        if (OrderId != "")
        //        {
        //            var item = OrderId.Split(",");
        //            for (int i = 0; i < item.Count(); i++)
        //            {
        //                //前台點擊備貨
        //                if (Status == 1)
        //                {
        //                    //只有狀態為未接單的訂單能備貨
        //                    var OrderDetail = db.OrderDetail.Where(w => w.OrderId == int.Parse(item[i]) && w.LogisticsStatus == -1);
        //                    foreach (var item2 in OrderDetail)
        //                    {
        //                        item2.LogisticsStatus = Status;
        //                        db.SubmitChanges();
        //                    }
        //                }

        //            }
        //        }
        //        //從編輯(Edit)來
        //        if (OrderDetailId != "")
        //        {
        //            var item = OrderDetailId.Split(",");
        //            for (int i = 0; i < item.Count(); i++)
        //            {
        //                //前台點擊備貨
        //                if (Status == 1)
        //                {
        //                    //只有狀態為未接單的訂單能備貨
        //                    var OrderDetail = db.OrderDetail.FirstOrDefault(w => w.Id == int.Parse(item[i]) && w.LogisticsStatus == -1);

        //                    OrderDetail.LogisticsStatus = Status;
        //                    db.SubmitChanges();
        //                }

        //            }
        //        }
        //        db.Connection.Close();
        //        return Content("Success");
        //    }
        //    catch { return Content("Failure "); }
        //}
        #endregion

        //[HttpPost]
        //public ActionResult Export(string keyword = "")
        //{
        //    RefundModel data = new RefundModel();
        //    //---------------匯出條件搜索---------------//

        //    if (keyword != null)
        //    {
        //        data.Keyword = keyword;
        //    }
        //    //---------------匯出條件搜索end---------------//
        //    return File(data.Set_Excel(data.Export_Data(data)), "application/vnd.ms-excel", "退款資料.xls");
        //}
    }
}

