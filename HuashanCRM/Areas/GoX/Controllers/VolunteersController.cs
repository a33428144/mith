﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;
using System.Text;
using System.IO;
using Mith.Models;
using Mith.Areas.GoX.Models;

namespace Mith.Areas.GoX.Controllers
{
	public class VolunteersController : CategoryController
	{
		int fun_id = 3;

		[RoleAuthorize(Role = "亞設人員")]
		[MyAuthorize(Com = Competence.Read, function = "會員管理")]
		public ActionResult Index(string keyword = "", int Search_Enable = 0, DateTime? CreateTime_St = null, DateTime? CreateTime_End = null, string sort = "", int p = 1, int show_number = 10)
		{
			VolunteersModel data = new VolunteersModel();
			var fun = FunctionModel.Get_One(fun_id);
			string get = "";

			if (keyword != "")
			{
				data.Keyword = keyword;
				ViewData["keyword"] = keyword;
				get += Method.Get_URLGet("keyword", keyword);
			}
			if (Search_Enable != 0)
			{
				data.Search_Enable = Search_Enable;
				ViewData["Search_Enable"] = Search_Enable;
				get += Method.Get_URLGet("Search_Enable", Search_Enable.ToString());
			}
			if (CreateTime_St != null)
			{
				data.CreateTime_St = CreateTime_St;
				ViewData["CreateTime_St"] = CreateTime_St;
				get += Method.Get_URLGet("CreateTime_St", CreateTime_St.Value.ToString("yyyy/MM/dd HH:mm:ss"));
			}
			if (CreateTime_End != null)
			{
				data.CreateTime_End = CreateTime_End;
				ViewData["CreateTime_End"] = CreateTime_End;
				get += Method.Get_URLGet("CreateTime_End", CreateTime_End.Value.ToString("yyyy/MM/dd HH:mm:ss"));
			}

			ViewBag.Title = fun.Name;
			ViewBag.Title_link = "<a href=\"\">" + fun.Name + "</a>";

			data.Sort = sort;
			get += Method.Get_URLGet("sort", sort);

			ViewData["p"] = p;
			ViewData["page"] = data.Get_Page(p, show_number);
			ViewData["get"] = get;
			Session["Url"] = Request.Url.AbsoluteUri;
			return View(data.Get_Data(p, show_number));
		}

		[MyAuthorize(Com = Competence.Insert, function = "會員管理")]
		public ActionResult Create()
		{
			VolunteersModel data = new VolunteersModel();
			var fun = FunctionModel.Get_One(fun_id);

			ViewBag.Title = fun.Name + " > 新增";
			ViewBag.Title_link = "<a href=\"Index\">" + fun.Name + "</a> > 新增";
			ViewBag.c_title = fun.Name + "資料";

			return View();
		}

		[ValidateInput(true)]
		[ValidateAntiForgeryToken]
		[HttpPost]
		public ActionResult Create(VolunteersModel.VolunteersShow item, HttpPostedFileBase Pic)
		{
			MithDataContext db = new MithDataContext();
			VolunteersModel data = new VolunteersModel();
			var fun = FunctionModel.Get_One(fun_id);
			string path = fun.Controller + "_" + fun.Id;

			item.Id = db.Volunteers.OrderByDescending(o => o.Id).FirstOrDefault() != null ? db.Volunteers.OrderByDescending(o => o.Id).FirstOrDefault().Id + 1 : 1;

			#region 上傳圖片
			string sContainer = "volunteers";
			string sImagePath = item.Id + "/";
			string FileExtension = "";
			string name = Method.RandomKey(5, true, false, false, false);

			Size Middle = new Size();
			Size Small = new Size();

			if (Pic != null)
			{
				//上傳新圖片
				item.Pic = BlobHelper.Upload_Blob(new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + FileExtension }, Pic.InputStream, name + FileExtension, Middle, Small, false)[0];
			}
			#endregion

			if (data.Insert(item) <= 0)
			{
				ErrorRecordModel.Error_Record(Method.Get_UserId_Admin(Request.Cookies, Session), "Volunteers_2", Request.UserAgent, true, "資料新增失敗");
				TempData["err"] = "Volunteers_2，資料新增失敗";
			}
			TempData["FromAction"] = "新增成功！";
			db.Connection.Close();
			return RedirectToAction("Index");
		}

		[MyAuthorize(Com = Competence.Update, function = "會員管理")]
		public ActionResult Edit(int id = 0, int p = 1)
		{
			VolunteersModel data = new VolunteersModel();
			var fun = FunctionModel.Get_One(fun_id);

			ViewBag.Title = fun.Name + " > 編輯";
			ViewBag.Title_link = "<a href=\"../Index\">" + fun.Name + "</a> > 編輯";
			ViewBag.c_title = fun.Name + "資料";
			ViewData["p"] = p;
			var item = data.Get_One(id);
			if (item == null)
			{
				return RedirectToAction("Create");
			}
			return View(item);
		}
		[ValidateInput(true)]
		[ValidateAntiForgeryToken]
		[HttpPost]
		public ActionResult Edit(VolunteersModel.VolunteersShowEdit item, int p = 1)
		{
			VolunteersModel data = new VolunteersModel();
			var fun = FunctionModel.Get_One(fun_id);
			string path = fun.Controller + "_" + fun.Id;
			string url = (string)Session["Url"];
			if (ModelState.IsValid)
			{

				if (data.Update(item) <= 0)
				{
					ErrorRecordModel.Error_Record(Method.Get_UserId_Admin(Request.Cookies, Session), "Volunteers_5", Request.UserAgent, true, "資料新增失敗");
					TempData["err"] = "Volunteers_5";
					return RedirectToAction("Index");
				}
			}
			TempData["FromAction"] = "儲存成功！";
			if (!string.IsNullOrWhiteSpace(url))
			{
				return Redirect(url);
			}
			else
			{
				return RedirectToAction("Index");
			}
		}

		[MyAuthorize(Com = Competence.Delete, function = "會員管理")]
		public ActionResult Delete(int Id, int p = 1)
		{
			VolunteersModel data = new VolunteersModel();
			data.Delete(Id, Server);

			return RedirectToAction("Index", new { p = p });
		}


		[HttpPost]
		public ActionResult Export(string keyword = "")
		{
			VolunteersModel data = new VolunteersModel();
			//---------------匯出條件搜索---------------//

			if (keyword != null)
			{
				data.Keyword = keyword;
			}
			//---------------匯出條件搜索end---------------//
			return File(data.Set_Excel(data.Export_Data(data)), "application/vnd.ms-excel", "會員資料.xls");
		}
	}
}

