﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mith.Areas.GoX.Models;
using Mith.Models;

namespace Mith.Areas.GoX.Controllers
{
    public class UploadController : Controller
    {
        #region pic
        /*[MyAuthorize(Com = Competence.Update, function = "網站介紹A")]
        [HttpPost]
        public ActionResult IntroductionA(HttpPostedFileBase upload, string CKEditorFuncNum, string CKEditor, string langCode)
        {
            return Content(UploadPicture(upload, CKEditorFuncNum, CKEditor, langCode, "IntroductionA"));
        }

        [MyAuthorize(Com = Competence.Update, function = "網站介紹B")]
        [HttpPost]
        public ActionResult IntroductionB(HttpPostedFileBase upload, string CKEditorFuncNum, string CKEditor, string langCode)
        {
            return Content(UploadPicture(upload, CKEditorFuncNum, CKEditor, langCode, "IntroductionB"));
        }

        [MyAuthorize(Com = Competence.Update, function = "最新消息A")]
        [HttpPost]
        public ActionResult News(HttpPostedFileBase upload, string CKEditorFuncNum, string CKEditor, string langCode)
        {
            return Content(UploadPicture(upload, CKEditorFuncNum, CKEditor, langCode, "News"));
        }

        [MyAuthorize(Com = Competence.Update, function = "最新消息B")]
        [HttpPost]
        public ActionResult NewsB(HttpPostedFileBase upload, string CKEditorFuncNum, string CKEditor, string langCode)
        {
            return Content(UploadPicture(upload, CKEditorFuncNum, CKEditor, langCode, "NewsB"));
        }

        [MyAuthorize(Com = Competence.Update, function = "分店管理A")]
        [HttpPost]
        public ActionResult Store(HttpPostedFileBase upload, string CKEditorFuncNum, string CKEditor, string langCode)
        {
            return Content(UploadPicture(upload, CKEditorFuncNum, CKEditor, langCode, "Store"));
        }

        [MyAuthorize(Com = Competence.Update, function = "分店管理B")]
        [HttpPost]
        public ActionResult StoreB(HttpPostedFileBase upload, string CKEditorFuncNum, string CKEditor, string langCode)
        {
            return Content(UploadPicture(upload, CKEditorFuncNum, CKEditor, langCode, "StoreB"));
        }

        [MyAuthorize(Com = Competence.Update, function = "分店管理C")]
        [HttpPost]
        public ActionResult StoreC(HttpPostedFileBase upload, string CKEditorFuncNum, string CKEditor, string langCode)
        {
            return Content(UploadPicture(upload, CKEditorFuncNum, CKEditor, langCode, "StoreC"));
        }

        [MyAuthorize(Com = Competence.Update, function = "抽獎管理A")]
        [HttpPost]
        public ActionResult WinningA(HttpPostedFileBase upload, string CKEditorFuncNum, string CKEditor, string langCode)
        {
            return Content(UploadPicture(upload, CKEditorFuncNum, CKEditor, langCode, "WinningA"));
        }

        [MyAuthorize(Com = Competence.Update, function = "抽獎管理B")]
        [HttpPost]
        public ActionResult Winning(HttpPostedFileBase upload, string CKEditorFuncNum, string CKEditor, string langCode)
        {
            return Content(UploadPicture(upload, CKEditorFuncNum, CKEditor, langCode, "Winning"));
        }

        [MyAuthorize(Com = Competence.Update, function = "聯絡我們管理")]
        [HttpPost]
        public ActionResult Contact(HttpPostedFileBase upload, string CKEditorFuncNum, string CKEditor, string langCode)
        {
            return Content(UploadPicture(upload, CKEditorFuncNum, CKEditor, langCode, "Contact"));
        }

        [MyAuthorize(Com = Competence.Update, function = "會員管理")]
        [HttpPost]
        public ActionResult Agree(HttpPostedFileBase upload, string CKEditorFuncNum, string CKEditor, string langCode)
        {
            return Content(UploadPicture(upload, CKEditorFuncNum, CKEditor, langCode, "Agree"));
        }

        [MyAuthorize(Com = Competence.Update, function = "會員管理")]
        [HttpPost]
        public ActionResult Send_Message(HttpPostedFileBase upload, string CKEditorFuncNum, string CKEditor, string langCode)
        {
            return Content(UploadPicture(upload, CKEditorFuncNum, CKEditor, langCode, "Send_Message"));
        }

        [MyAuthorize(Com = Competence.Update, function = "會員管理")]
        [HttpPost]
        public ActionResult AccountRegist(HttpPostedFileBase upload, string CKEditorFuncNum, string CKEditor, string langCode)
        {
            return Content(UploadPicture(upload, CKEditorFuncNum, CKEditor, langCode, "AccountRegist"));
        }

        [MyAuthorize(Com = Competence.Update, function = "會員管理")]
        [HttpPost]
        public ActionResult EmailConfirm(HttpPostedFileBase upload, string CKEditorFuncNum, string CKEditor, string langCode)
        {
            return Content(UploadPicture(upload, CKEditorFuncNum, CKEditor, langCode, "EmailConfirm"));
        }

        [MyAuthorize(Com = Competence.Update, function = "會員管理")]
        [HttpPost]
        public ActionResult EmailPassword(HttpPostedFileBase upload, string CKEditorFuncNum, string CKEditor, string langCode)
        {
            return Content(UploadPicture(upload, CKEditorFuncNum, CKEditor, langCode, "EmailPassword"));
        }*/


        public ActionResult Picture(int fun_id, HttpPostedFileBase upload, string CKEditorFuncNum, string CKEditor, string langCode)
        {
            if (!CheckLogin.CheckPermission(HttpContext, Competence.Update, fun_id))
            {
                return Content("<html><body><script>alert('上傳失敗');</script></body></html>");
            }
            var fun = FunctionModel.Get_One(fun_id);
            string path = fun.Controller + "_" + fun.Id;

            string real_url = Method.Upload_File(upload, path, Server);
            if (real_url != "")
            {
                return Content(@"<html><body><script>window.parent.CKEDITOR.tools.callFunction(" + CKEditorFuncNum + ", \"" + real_url + "\", \"" + string.Empty + "\");</script></body></html>");
            }

            return Content("<html><body><script>alert('上傳失敗');</script></body></html>");
        }
        #endregion
    }
}
