﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;
using System.Text;
using System.IO;
using Mith.Models;
using Mith.Areas.GoX.Models;

namespace Mith.Areas.GoX.Controllers
{
	public class LimitOrderController : CategoryController
	{
		int fun_id = 11;

		#region 首頁
		[MyAuthorize(Com = Competence.Read, function = "集單資料")]

		#region Level1
		public ActionResult IndexLevel1_Mith(string keyword = "", int Search_Status = 0, string Search_LimitStatus = "", int Search_CompanyId = 0, DateTime? LimitOrder_St = null, DateTime? LimitOrder_End = null, int p = 1, int show_number = 10)
		{
			LimitOrderModel data = new LimitOrderModel();
			var fun = FunctionModel.Get_One(fun_id);
			string get = "";
			if (keyword != "")
			{
				data.Keyword = keyword;
				ViewData["keyword"] = keyword;
				get += Method.Get_URLGet("keyword", keyword);
			}
			if (Search_Status > 0)
			{
				data.Search_Status = Search_Status;
				ViewData["Search_Status"] = Search_Status;
				get += Method.Get_URLGet("Search_Status", Search_Status.ToString());
			}
			if (Search_LimitStatus != "")
			{
				data.Search_LimitStatus = Search_LimitStatus;
				ViewData["Search_LimitStatus"] = Search_LimitStatus;
				get += Method.Get_URLGet("Search_LimitStatus", Search_LimitStatus);
			}
			if (Search_CompanyId > 0)
			{
				data.Search_CompanyId = Search_CompanyId;
				ViewData["Search_CompanyId"] = Search_CompanyId;
				get += Method.Get_URLGet("Search_CompanyId", Search_CompanyId.ToString());
			}
			if (LimitOrder_St != null)
			{
				data.LimitOrder_St = LimitOrder_St;
				ViewData["LimitOrder_St"] = LimitOrder_St;
				get += Method.Get_URLGet("LimitOrder_St", LimitOrder_St.Value.ToString("yyyy/MM/dd HH:mm:ss"));
			}
			if (LimitOrder_End != null)
			{
				data.LimitOrder_End = LimitOrder_End;
				ViewData["LimitOrder_End"] = LimitOrder_End;
				get += Method.Get_URLGet("LimitOrder_End", LimitOrder_End.Value.ToString("yyyy/MM/dd HH:mm:ss"));
			}

			ViewBag.Title = fun.Name;
			ViewBag.Title_link = "訂單管理 > <a href=\"\">" + fun.Name + "</a>";

			ViewData["p"] = p;
			ViewData["page"] = data.Get_Page(p, show_number);
			ViewData["number"] = show_number;
			ViewData["get"] = get;
			Session["Url"] = Request.Url.AbsoluteUri;
			var Result = data.Get_Data_Level1(p, show_number);

			return View(Result);
		}

		public ActionResult IndexLevel1_Company(string keyword = "", int Search_Status = 0, DateTime? LimitOrder_St = null, DateTime? LimitOrder_End = null, string ReturnStoreId = "", string ReturnStoreName = "", string ReturnStoreAddress = "", string From = "Mith", int p = 1, int show_number = 10)
		{
			LimitOrderModel data = new LimitOrderModel();
			var fun = FunctionModel.Get_One(fun_id);
			string get = "";
			if (keyword != "")
			{
				data.Keyword = keyword;
				ViewData["keyword"] = keyword;
				get += Method.Get_URLGet("keyword", keyword);
			}
			if (Search_Status > 0)
			{
				data.Search_Status = Search_Status;
				ViewData["Search_Status"] = Search_Status;
				get += Method.Get_URLGet("Search_Status", Search_Status.ToString());
			}
			if (LimitOrder_St != null)
			{
				data.LimitOrder_St = LimitOrder_St;
				ViewData["LimitOrder_St"] = LimitOrder_St;
				get += Method.Get_URLGet("LimitOrder_St", LimitOrder_St.Value.ToString("yyyy/MM/dd HH:mm:ss"));
			}
			if (LimitOrder_End != null)
			{
				data.LimitOrder_End = LimitOrder_End;
				ViewData["LimitOrder_End"] = LimitOrder_End;
				get += Method.Get_URLGet("LimitOrder_End", LimitOrder_End.Value.ToString("yyyy/MM/dd HH:mm:ss"));
			}

			data.Search_CompanyId = Mith.Models.Method.Get_CompanyId_Admin(Request.Cookies, Session);
			ViewBag.Title = fun.Name;
			ViewBag.Title_link = "訂單管理 > <a href=\"\">" + fun.Name + "</a>";

			ViewData["p"] = p;
			ViewData["page"] = data.Get_Page(p, show_number);
			ViewData["number"] = show_number;
			ViewData["get"] = get;
			Session["Url"] = Request.Url.AbsoluteUri;
			var Result = data.Get_Data_Level1(p, show_number);

			if (From == "Mith")
			{
				MithDataContext db = new MithDataContext();
				List<OrderDetail_Edit_View> OrderDetail = db.OrderDetail_Edit_View.Where(f => f.CompanyId == data.Search_CompanyId && f.ReturnStoreId.Length > 1).OrderByDescending(o => o.Id).Take(1).ToList();

				if (OrderDetail.Count() > 0)
				{
					foreach (var item2 in OrderDetail)
					{
						ViewData["ReturnStoreId"] = item2.ReturnStoreId;
						ViewData["ReturnStoreName"] = item2.ReturnStoreName;
						ViewData["ReturnStoreAddress"] = item2.ReturnStoreAddress;
					}
				}
				db.Connection.Close();
			}
			else
			{
				ViewData["ReturnStoreId"] = ReturnStoreId;
				ViewData["ReturnStoreName"] = ReturnStoreName;
				ViewData["ReturnStoreAddress"] = ReturnStoreAddress;
			}
			ViewData["From"] = From;
			return View(Result);
		}

		#endregion


		#region Level2

		public ActionResult IndexLevel2_Mith(int Search_GoodsId = 0, string LimitStatus = "", string keyword = "", int Search_Direction = 0, int Search_Status = 0, int Search_CashFlow1 = 0, int Search_CashFlow2 = 0, DateTime? CreateTime_St = null, DateTime? CreateTime_End = null, int p = 1, int show_number = 5)
		{
			OrderModel data = new OrderModel();
			var fun = FunctionModel.Get_One(fun_id);
			string get = "";
			if (keyword != "")
			{
				data.Keyword = keyword;
				ViewData["keyword"] = keyword;
				get += Method.Get_URLGet("keyword", keyword);
			}
			if (Search_Direction != 0)
			{
				data.Search_Direction = Search_Direction;
				ViewData["Search_Direction"] = Search_Direction;
				get += Method.Get_URLGet("Search_Direction", Search_Direction.ToString());
			}

			if (Search_Status != 0)
			{
				data.Search_Status = Search_Status;
				ViewData["Search_Status"] = Search_Status;
				get += Method.Get_URLGet("Search_Status", Search_Status.ToString());
			}

			//正物流  - 查是否付款
			if (Search_Direction == 1 && Search_CashFlow1 != 0)
			{
				data.Search_CashFlow = Search_CashFlow1;
				ViewData["Search_CashFlow1"] = Search_CashFlow1;
				get += Method.Get_URLGet("Search_CashFlow1", Search_CashFlow1.ToString());
			}
			//逆物流 - 查是否退款
			if (Search_Direction == 2 && Search_CashFlow2 != 0)
			{
				data.Search_CashFlow = Search_CashFlow2;
				ViewData["Search_CashFlow2"] = Search_CashFlow2;
				get += Method.Get_URLGet("Search_CashFlow2", Search_CashFlow2.ToString());
			}

			if (CreateTime_St != null)
			{
				data.CreateTime_St = CreateTime_St;
				ViewData["CreateTime_St"] = CreateTime_St;
				get += Method.Get_URLGet("CreateTime_St", CreateTime_St.Value.ToString("yyyy/MM/dd HH:mm:ss"));
			}
			if (CreateTime_End != null)
			{
				data.CreateTime_End = CreateTime_End;
				ViewData["CreateTime_End"] = CreateTime_End;
				get += Method.Get_URLGet("CreateTime_End", CreateTime_End.Value.ToString("yyyy/MM/dd HH:mm:ss"));
			}
			data.Search_GoodsId = Search_GoodsId;
			ViewData["Search_GoodsId"] = Search_GoodsId;
			ViewData["LimitStatus"] = LimitStatus;

			ViewBag.Title = fun.Name;
			ViewBag.Title_link = "訂單管理 > <a href='/GoX/LimitOrder/IndexLevel1_Mith'>集單資料</a> > 購買列表";

			ViewData["p"] = p;
			ViewData["page"] = data.Get_LimitOrder_Page(p, show_number);
			ViewData["number"] = show_number;
			ViewData["get"] = get;
			Session["Url"] = Request.Url.AbsoluteUri;
			var Result = data.Get_LimitOrderData(p, show_number);
			return View(Result);
		}

		[MyAuthorize(Com = Competence.Read, function = "集單資料")]
		public ActionResult IndexLevel2_Company(int Search_GoodsId = 0, string LimitStatus = "", string keyword = "", int Search_Direction = 0, int Search_Logistics1 = 0, int Search_CashFlow1 = 0, DateTime? CreateTime_St = null, DateTime? CreateTime_End = null, int p = 1, int show_number = 5)
		{
			OrderModel data = new OrderModel();
			var fun = FunctionModel.Get_One(fun_id);
			string get = "";
			if (keyword != "")
			{
				data.Keyword = keyword;
				ViewData["keyword"] = keyword;
				get += Method.Get_URLGet("keyword", keyword);
			}
			if (Search_Direction != 0)
			{
				data.Search_Direction = Search_Direction;
				ViewData["Search_Direction"] = Search_Direction;
				get += Method.Get_URLGet("Search_Direction", Search_Direction.ToString());
			}

			//正物流  - 查是否付款
			if (Search_Direction == 1 && Search_CashFlow1 != 0)
			{
				data.Search_CashFlow = Search_CashFlow1;
				ViewData["Search_CashFlow1"] = Search_CashFlow1;
				get += Method.Get_URLGet("Search_CashFlow1", Search_CashFlow1.ToString());
			}

			if (CreateTime_St != null)
			{
				data.CreateTime_St = CreateTime_St;
				ViewData["CreateTime_St"] = CreateTime_St;
				get += Method.Get_URLGet("CreateTime_St", CreateTime_St.Value.ToString("yyyy/MM/dd HH:mm:ss"));
			}
			if (CreateTime_End != null)
			{
				data.CreateTime_End = CreateTime_End;
				ViewData["CreateTime_End"] = CreateTime_End;
				get += Method.Get_URLGet("CreateTime_End", CreateTime_End.Value.ToString("yyyy/MM/dd HH:mm:ss"));
			}
			//廠商編號
			data.Search_CompanyId = Mith.Models.Method.Get_CompanyId_Admin(Request.Cookies, Session);
			data.Search_GoodsId = Search_GoodsId;
			ViewData["Search_GoodsId"] = Search_GoodsId;
			ViewData["LimitStatus"] = LimitStatus;

			ViewBag.Title = fun.Name;
			ViewBag.Title_link = "訂單管理 > <a href='/GoX/LimitOrder/IndexLevel1_Company'>集單資料</a> > 購買列表";

			ViewData["p"] = p;
			ViewData["page"] = data.Get_LimitOrder_Page(p, show_number);
			ViewData["number"] = show_number;
			ViewData["get"] = get;
			Session["Url"] = Request.Url.AbsoluteUri;

			var Result = data.Get_LimitOrderData(p, show_number);
			return View(Result);
		}

		#endregion

		#endregion

		#region 編輯

		[MyAuthorize(Com = Competence.Update, function = "集單資料")]
		public ActionResult Edit_Mith(int id = 0, string LimitStatus = "")
		{
			MithDataContext db = new MithDataContext();
			OrderModel data = new OrderModel();
			var fun = FunctionModel.Get_One(fun_id);
			string url = (string)Session["Url"];
			data.Search_CompanyId = 0;
			var item = data.Get_One(id);
			if (item == null)
			{
				return Redirect(url);
			}
			db.Connection.Close();

			ViewBag.Title_link = "訂單管理 > <a href='/GoX/LimitOrder/IndexLevel1_Company'>集單資料</a> > <a href='" + url + "'>購買列表</a> > 詳情";
			ViewData["LimitStatus"] = LimitStatus;
			if (item.Direction == 1)
			{
				return View("Edit_Mith_Forward", item);
			}
			else
			{
				return View("Edit_Mith_Backward", item);
			}

		}

		[MyAuthorize(Com = Competence.Update, function = "集單資料")]
		public ActionResult Edit_Company(int id = 0, string LimitStatus = "", string ReturnStoreId = "", string ReturnStoreName = "", string ReturnStoreAddress = "", string From = "Mith")
		{
			MithDataContext db = new MithDataContext();
			OrderModel data = new OrderModel();
			var fun = FunctionModel.Get_One(fun_id);
			string url = (string)Session["Url"];

			data.Search_CompanyId = Mith.Models.Method.Get_CompanyId_Admin(Request.Cookies, Session);

			var item = data.Get_One(id);
			if (item == null)
			{
				return Redirect(url);
			}
			db.Connection.Close();


			ViewBag.Title_link = "訂單管理 > <a href='/GoX/LimitOrder/IndexLevel1_Company'>集單資料</a> > <a href='" + url + "'>購買列表</a> > 詳情";

			//廠商編號
			int CompanyId = Mith.Models.Method.Get_CompanyId_Admin(Request.Cookies, Session);


			if (From == "Mith")
			{
				var OrderDetail_Edit_View = db.OrderDetail_Edit_View.OrderByDescending(f => f.Id).FirstOrDefault(f => f.CompanyId == CompanyId);

				if (OrderDetail_Edit_View != null)
				{
					ViewData["ReturnStoreId"] = OrderDetail_Edit_View.ReturnStoreId;
					ViewData["ReciverStoreName"] = OrderDetail_Edit_View.ReturnStoreName;
					ViewData["ReturnStoreAddress"] = OrderDetail_Edit_View.ReturnStoreAddress;
				}
			}
			else
			{
				ViewData["ReturnStoreId"] = ReturnStoreId;
				ViewData["ReciverStoreName"] = ReturnStoreName;
				ViewData["ReturnStoreAddress"] = ReturnStoreAddress;
			}
			ViewData["LimitStatus"] = LimitStatus;
			if (item.Direction == 1)
			{
				ViewData["From"] = From;
				return View("Edit_Company_Forward", item);
			}
			else
			{
				return View("Edit_Company_Backward", item);
			}

		}

		/// <summary>
		/// 出貨
		/// </summary>
		[ValidateInput(true)]
		[ValidateAntiForgeryToken]
		[HttpPost]
		public ActionResult Update_ShippingGoods(string ShippingVal_711 = "", string ShippingVal_BlackCat = "", string ReturnStoreId = "", string ReturnStoreName = "", string ReturnStoreAddress = "")
		{
			MithDataContext db = new MithDataContext();
			OrderModel data = new OrderModel();
			try
			{
				int OrderId = 0;
				int OrderDetailId = 0;
				string LogisticsSN = "";
				int LastUserId = Method.Get_UserId_Admin(Request.Cookies, Session);
				DateTime Now = DateTime.UtcNow;
				int GoodsId = 0;

				#region 7-11出貨
				string PackageSN = null;
				var item_711 = ShippingVal_711.Split(",");
				if (item_711[0] != "")
				{
					for (int i = 0; i < item_711.Count(); i++)
					{
						OrderDetailId = int.Parse(item_711[i]); //訂單細節編號
						LogisticsSN = "711" + Method.RandomKey(7, true, false, false, false);
						//只有狀態為備貨的訂單商品能出貨
						var OrderDetail = db.OrderDetail.FirstOrDefault(w => w.Id == OrderDetailId && w.LogisticsStatus > 0);
						if (OrderDetail != null)
						{
							if (i == 0)
							{
								GoodsId = OrderDetail.GoodsId;
							}
							OrderId = OrderDetail.OrderId;
							var Order = db.Order.FirstOrDefault(f => f.Id == OrderId);

							string ReceiverStoreId = db.Order.FirstOrDefault(f => f.Id == OrderId).ReceiverStoreId;

							//取得7-11包裹要號
							PackageSN = OrderModel.Get_711_PackegeSN(LogisticsSN, OrderDetail.Id, Order.Total.ToString(), ReceiverStoreId, ReturnStoreId);
							if (PackageSN == null)
							{
								return Content("出貨發生錯誤！");
							}
							OrderDetail.LogisticsStatus = 2;
							OrderDetail.LogisticsSN = LogisticsSN;
							if (!string.IsNullOrWhiteSpace(PackageSN))
							{
								OrderDetail.PackageSN = PackageSN;
							}
							if (!string.IsNullOrWhiteSpace(ReturnStoreId))
							{
								OrderDetail.ReturnStoreId = ReturnStoreId;
							}
							if (!string.IsNullOrWhiteSpace(ReturnStoreName))
							{
								OrderDetail.ReturnStoreName = ReturnStoreName;
							}
							if (!string.IsNullOrWhiteSpace(ReturnStoreAddress))
							{
								OrderDetail.ReturnStoreAddress = ReturnStoreAddress;
							}
							OrderDetail.ShippingTime = Now;
							OrderDetail.LastUserId = LastUserId;
							db.SubmitChanges();
						}
					}
				}

				#endregion

				#region 黑貓出貨
				var item_BlackCat = ShippingVal_BlackCat.Split(",");
				if (item_BlackCat[0] != "")
				{
					for (int i = 0; i < item_BlackCat.Count(); i++)
					{
						//再拆等於（訂單細節編號：[0]、物流編號：[1]）
						var item2 = item_BlackCat[i].Split("=");
						OrderDetailId = int.Parse(item2[0]); //訂單細節編號
						LogisticsSN = item2[1]; // 物流編號

						//只有狀態為備貨的訂單商品能出貨
						var OrderDetail = db.OrderDetail.FirstOrDefault(w => w.Id == OrderDetailId && w.LogisticsStatus > 0);
						if (OrderDetail != null)
						{
							if (i == 0)
							{
								GoodsId = OrderDetail.GoodsId;
							}
							OrderId = OrderDetail.OrderId;
							var Order = db.Order.FirstOrDefault(f => f.Id == OrderId);

							OrderDetail.LogisticsStatus = 2;
							OrderDetail.LogisticsSN = LogisticsSN;
							OrderDetail.ShippingTime = Now;
							OrderDetail.LastUserId = LastUserId;
							db.SubmitChanges();
						}
					}
				}

				#endregion

				var Check = db.OrderDetail.Where(w => w.GoodsId == GoodsId && w.LogisticsStatus < 2);

				//此集單商品底下沒有未接單、備貨中的訂單時
				if (!Check.Any())
				{
					var Goods = db.Goods.FirstOrDefault(f => f.Id == GoodsId);
					if (Goods != null)
					{
						//集單商品出貨狀態 = true;
						Goods.LimitOrderShipping = true;
						db.SubmitChanges();
					}
				}

				var GoodsQuantity = db.GoodsQuantity.FirstOrDefault(f => f.GoodsId == GoodsId);
				string GoodsSN = "";
				if (GoodsQuantity != null)
				{
					//銷售量加上集單量
					GoodsQuantity.SellQuantity += GoodsQuantity.LimitOrderQuantity;
					//集單量歸0
					GoodsQuantity.LimitOrderQuantity = 0;
					db.SubmitChanges();

					GoodsSN = db.Goods.FirstOrDefault(f => f.Id == GoodsId).SN;
				}
				return Redirect("/GoX/LimitOrder/IndexLevel1_Company?keyword=" + GoodsSN);
			}
			catch (Exception e)
			{
				return Content(e.ToString());
			}
			finally
			{
				db.Connection.Close();
			}

		}


		/// <summary>
		/// 退貨
		/// </summary>
		/// <param name="ReturnGoodsVal">退貨商品，字串為：訂單編號=退貨數量,</param>
		[ValidateInput(true)]
		[ValidateAntiForgeryToken]
		[HttpPost]
		public ActionResult Update_ReturnGoods(int CityId = 0, int AreaId = 0, string ReceiverAddress = "",
				int ReceiveTimeRange = 0, string Remark = "", string LogisticsSN = "", string ReturnGoodsVal = "")
		{
			MithDataContext db = new MithDataContext();
			OrderModel data = new OrderModel();
			int New_OrderId = 0;
			try
			{
				int LastUserId = Method.Get_UserId_Admin(Request.Cookies, Session);
				//先拆逗號
				var item = ReturnGoodsVal.Split(",");
				for (int i = 0; i < item.Count(); i++)
				{
					//再拆等於（訂單編號：[0]　退貨數量：[1]）
					var item2 = item[i].Split("=");
					int OrderDetailId = int.Parse(item2[0]); //訂單編號
					int ReturnQuantity = int.Parse(item2[1]); // 退貨數量

					var Old_OrderDetail = db.OrderDetail.FirstOrDefault(f => f.Id == OrderDetailId);
					//商品總額
					int TotalPrice = 0;

					#region 迴圈第一次，新增退貨的訂單主檔
					if (i == 0)
					{
						//新增退貨的訂單主檔
						New_OrderId = data.Insert_ReturnGoods_Order(1, CityId, AreaId, ReceiverAddress, ReceiveTimeRange, Remark, Old_OrderDetail.OrderId, LastUserId);
					}
					#endregion

					#region 新增退貨的訂單細節
					int BuyTotal = 0;

					BuyTotal = Old_OrderDetail.BuyPrice * ReturnQuantity;

					var New_OrderDetail = new OrderDetail()
					{
						OrderId = New_OrderId,
						GoodsId = Old_OrderDetail.GoodsId,
						GoodsType = Old_OrderDetail.GoodsType,
						CompanyId = Old_OrderDetail.CompanyId,
						BrandId = Old_OrderDetail.BrandId,
						BuySize = Old_OrderDetail.BuySize,
						BuyQuantity = ReturnQuantity,
						BuyPrice = Old_OrderDetail.BuyPrice,
						BuyTotal = BuyTotal,
						LogisticsSN = LogisticsSN,
						LogisticsCompany = Old_OrderDetail.LogisticsCompany,
						LogisticsStatus = 3, //退貨中
						CreateTime = DateTime.UtcNow,
						UpdateTime = DateTime.UtcNow,
						LastUserId = LastUserId,
					};
					db.OrderDetail.InsertOnSubmit(New_OrderDetail);
					db.SubmitChanges();

					TotalPrice += BuyTotal;
					#endregion

					#region 計算訂單總和
					var New_Order = db.Order.FirstOrDefault(f => f.Id == (int)New_OrderId);
					if (New_Order != null)
					{
						New_Order.Total = TotalPrice;
						db.SubmitChanges();
					}

					#endregion

				}

				return Redirect("/GoX/ReturnGoods/Index_Mith");
			}
			catch (Exception e)
			{
				//刪掉訂單
				WebApiMethod.Delete_Order((int)New_OrderId);
				return Content("Error");
			}
			finally
			{
				db.Connection.Close();
			}

		}

		/// <summary>
		/// 退款
		/// </summary>
		public ActionResult Update_Refund(int GoodsId = 0)
		{
			MithDataContext db = new MithDataContext();
			try
			{
				var OrderDetail = db.OrderDetail.Where(w => w.GoodsId == GoodsId);
				foreach (var item in OrderDetail)
				{
					var Order = db.Order.FirstOrDefault(f => f.Id == item.OrderId);
					if (Order != null)
					{
						//金流 = ATM轉帳
						if (Order.CashFlowType == 1)
						{
							//未退款
							Order.CashFlowStatus = 3;
						}
						//金流 = 信用卡
						else
						{
							//串接國泰退款
							string Status = WebApiMethod.Refund_CreditCard(Server, Order.SN, Order.Total.ToString(), Order.CathayAuthCode);
							if (Status == "0000")
							{
								//已退款
								Order.CashFlowStatus = 4;
								//已結單
								Order.Status = 2;
								Order.RefundTotal = Order.Total;
								Order.RefundTime = DateTime.UtcNow;
							}
						}
						Order.RefundReason = 2;
						Order.AgreeRefund = true;
						Order.LastUserId = Method.Get_UserId_Admin(Request.Cookies, Session);
						db.SubmitChanges();
					}
				}
				//商品尺寸的集單數量歸零
				var GoodsQuantity = db.GoodsQuantity.FirstOrDefault(f => f.GoodsId == GoodsId);
				string GoodsSN = "";
				if (GoodsQuantity != null)
				{
					GoodsQuantity.LimitOrderQuantity = 0;
					db.SubmitChanges();
					GoodsSN = db.Goods.FirstOrDefault(f => f.Id == GoodsId).SN;
				}
				return Redirect("/GoX/LimitOrder/IndexLevel1_Mith?keyword=" + GoodsSN);
			}
			catch (Exception e)
			{
				return Content(e.Message);
			}
			finally
			{
				db.Connection.Close();
			}

		}

		#endregion

		#region Ajax
		public PartialViewResult Get_SelectGoosList(int GoodsId = 0)
		{
			MithDataContext db = new MithDataContext();
			List<OrderModel.OrderDeatailShow> SelectGoosList = new List<OrderModel.OrderDeatailShow>();
			try
			{
				if (GoodsId > 0)
				{
					var OrderDetail = db.OrderDetail_Edit_View.Where(w => w.GoodsId == GoodsId && w.LogisticsStatus == 1);
					foreach (var item in OrderDetail)
					{
						var Order = db.Order_Edit_View.FirstOrDefault(f => f.Id == item.OrderId);
						SelectGoosList.Add(new OrderModel.OrderDeatailShow()
						{
							OrderSN = Order.SN,
							Id = item.Id,
							GoodsId = item.GoodsId,
							Goods_SN = item.Goods_SN,
							Goods_Name = item.Goods_Name,
							Goods_ColorName = item.Goods_ColorName,
							LogisticsType = Order.LogisticsType,
							LogisticsType_Name = Order.LogisticsType_Name,
							BuySize = item.BuySize,
							BuyQuantity = item.BuyQuantity,
						});
					}
					ViewData["SelectGoosList"] = SelectGoosList;
				}
			}
			catch
			{
			}
			finally
			{
				db.Connection.Close();
			}
			return PartialView("_SelectGoosList");
		}

		//更新訂單狀態
		[HttpPost]
		public ActionResult Update_Status(string OrderId = "", string OrderDetailId = "", int Status = 0)
		{
			try
			{
				MithDataContext db = new MithDataContext();
				GoodsModel data = new GoodsModel();
				//從首頁(Index)來
				if (OrderId != "")
				{
					var item = OrderId.Split(",");
					for (int i = 0; i < item.Count(); i++)
					{
						//前台點擊備貨
						if (Status == 1)
						{
							//只有狀態為未接單的訂單能備貨
							var OrderDetail = db.OrderDetail.Where(w => w.OrderId == int.Parse(item[i]) && w.LogisticsStatus == -1);
							foreach (var item2 in OrderDetail)
							{
								item2.LogisticsStatus = Status;
								db.SubmitChanges();
							}
						}

					}
				}
				//從編輯(Edit)來
				if (OrderDetailId != "")
				{
					var item = OrderDetailId.Split(",");
					for (int i = 0; i < item.Count(); i++)
					{
						//前台點擊備貨
						if (Status == 1)
						{
							//只有狀態為未接單的訂單能備貨
							var OrderDetail = db.OrderDetail.FirstOrDefault(w => w.Id == int.Parse(item[i]) && w.LogisticsStatus == -1);

							OrderDetail.LogisticsStatus = Status;
							db.SubmitChanges();
						}

					}
				}
				db.Connection.Close();
				return Content("Success");
			}
			catch { return Content("Failure "); }
		}

		[HttpPost]
		public ActionResult Session_GoodsId(int GoodsId)
		{
			Session["GoodsId"] = GoodsId;
			return Content("Success");
		}
		#endregion

	}
}

