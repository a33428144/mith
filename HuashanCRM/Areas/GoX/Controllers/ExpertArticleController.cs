﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;
using System.Text;
using System.IO;
using Mith.Models;
using Mith.Areas.GoX.Models;

namespace Mith.Areas.GoX.Controllers
{
	public class ExpertArticleController : CategoryController
	{
		int fun_id = 23;

		#region 首頁

		[MyAuthorize(Com = Competence.Read, function = "文章資料1")]
		public ActionResult Index(string keyword = "", int Search_ExpertId = 0, int Search_Display = 0, DateTime? CreateTime_St = null, DateTime? CreateTime_End = null, string TypeSort = "CreateTime", int p = 1, int show_number = 10)
		{
			ExpertArticleModel data = new ExpertArticleModel();
			var fun = FunctionModel.Get_One(fun_id);
			string get = "";
			if (keyword != "")
			{
				data.Keyword = keyword;
				ViewData["keyword"] = keyword;
				get += Method.Get_URLGet("keyword", keyword);
			}
			if (Search_ExpertId != 0)
			{
				data.Search_ExpertId = Search_ExpertId;
				ViewData["Search_ExpertId"] = Search_ExpertId;
				get += Method.Get_URLGet("Search_ExpertId", Search_ExpertId.ToString());
			}
			if (Search_Display != 0)
			{
				data.Search_Display = Search_Display;
				ViewData["Search_Display"] = Search_Display;
				get += Method.Get_URLGet("Search_Display", Search_Display.ToString());
			}

			if (CreateTime_St != null)
			{
				data.CreateTime_St = CreateTime_St;
				ViewData["CreateTime_St"] = CreateTime_St;
				get += Method.Get_URLGet("CreateTime_St", CreateTime_St.Value.ToString("yyyy/MM/dd HH:mm:ss"));
			}
			if (CreateTime_End != null)
			{
				data.CreateTime_End = CreateTime_End;
				ViewData["CreateTime_End"] = CreateTime_End;
				get += Method.Get_URLGet("CreateTime_End", CreateTime_End.Value.ToString("yyyy/MM/dd HH:mm:ss"));
			}

			ViewBag.Title = fun.Name;
			ViewBag.Title_link = "達人管理 > <a href=/GoX/ExpertArticle/Index>" + "文章資料" + "</a>";

			ViewData["p"] = p;
			ViewData["page"] = data.Get_Page(p, show_number, false);
			ViewData["number"] = show_number;
			ViewData["get"] = get;
			Session["Url"] = Request.Url.AbsoluteUri;

			data.TypeSort = TypeSort;
			ViewData["TypeSort"] = TypeSort;

			return View(data.Get_Data(p, show_number));
		}

		[MyAuthorize(Com = Competence.Read, function = "文章資料1")]
		public ActionResult Popular(int p = 1, int show_number = 10)
		{
			ExpertArticleModel data = new ExpertArticleModel();
			var fun = FunctionModel.Get_One(fun_id);
			string get = "";

			ViewBag.Title = "達人管理 > " + "文章資料" + " > 人氣排行 Top20";
			ViewBag.Title_link = "達人管理 > <a href=/GoX/ExpertArticle/Index>" + "文章資料" + "</a> > " + "人氣排行 Top20";

			ViewData["p"] = p;
			ViewData["page"] = data.Get_Page(p, show_number, true);
			ViewData["number"] = show_number;
			ViewData["get"] = get;
			Session["Url"] = Request.Url.AbsoluteUri;

			return View(data.Get_Data_Popular(p, show_number));
		}

		#endregion

		#region 新增

		[MyAuthorize(Com = Competence.Insert, function = "文章資料1")]
		public ActionResult Create()
		{
			MithDataContext db = new MithDataContext();
			ExpertArticleModel data = new ExpertArticleModel();
			var fun = FunctionModel.Get_One(fun_id);

			ViewBag.Title = "達人管理 > " + "文章資料" + " > 新增";
			ViewBag.Title_link = "達人管理 > <a href=/GoX/ExpertArticle/Index>" + "文章資料" + "</a> > 新增";
			ViewBag.c_title = fun.Name;
			db.Connection.Close();
			return View();
		}


		[ValidateInput(true)]
		[ValidateAntiForgeryToken]
		[HttpPost]
		public ActionResult Create(ExpertArticleModel.ExpertArticleShow item, HttpPostedFileBase Pic1, HttpPostedFileBase Pic2, HttpPostedFileBase Pic3, HttpPostedFileBase Pic4, HttpPostedFileBase Pic5, HttpPostedFileBase Cover)
		{

			MithDataContext db = new MithDataContext();
			ExpertArticleModel data = new ExpertArticleModel();
			var fun = FunctionModel.Get_One(fun_id);
			item.Id = db.Article.OrderByDescending(o => o.Id).FirstOrDefault() != null ? db.Article.OrderByDescending(o => o.Id).FirstOrDefault().Id + 1 : 1;

			if (ModelState.IsValid)
			{
				#region 上傳圖片
				string sContainer = "article";
				string sImagePath = item.Id + "/";
				string FileExtension = "";
				string name = Method.RandomKey(5, true, false, false, false);

				Size Middle = new Size();
				Size Small = new Size();

				if (Pic1 != null)
				{
					FileExtension = System.IO.Path.GetExtension(Pic1.FileName);
					//上傳圖片1
					item.Pic1 = BlobHelper.Upload_Blob(new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + "1" + FileExtension }, Pic1.InputStream, name + "1" + FileExtension, Middle, Small, false)[0];
				}
				if (Pic2 != null)
				{
					FileExtension = System.IO.Path.GetExtension(Pic2.FileName);
					//上傳圖片2
					item.Pic2 = BlobHelper.Upload_Blob(new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + "2" + FileExtension }, Pic2.InputStream, name + "2" + FileExtension, Middle, Small, false)[0];
				}
				if (Pic3 != null)
				{
					FileExtension = System.IO.Path.GetExtension(Pic3.FileName);
					//上傳圖片3
					item.Pic3 = BlobHelper.Upload_Blob(new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + "3" + FileExtension }, Pic3.InputStream, name + "3" + FileExtension, Middle, Small, false)[0];
				}
				if (Pic4 != null)
				{
					FileExtension = System.IO.Path.GetExtension(Pic4.FileName);
					//上傳圖片4
					item.Pic4 = BlobHelper.Upload_Blob(new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + "4" + FileExtension }, Pic4.InputStream, name + "4" + FileExtension, Middle, Small, false)[0];
				}
				if (Pic5 != null)
				{
					FileExtension = System.IO.Path.GetExtension(Pic5.FileName);
					//上傳圖片5
					item.Pic5 = BlobHelper.Upload_Blob(new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + "5" + FileExtension }, Pic5.InputStream, name + "5" + FileExtension, Middle, Small, false)[0];
				}

				#endregion

				if (data.Insert(item) <= 0)
				{
					ErrorRecordModel.Error_Record(Method.Get_UserId_Admin(Request.Cookies, Session), "ExpertArticle_2", Request.UserAgent, true, "資料新增失敗");
					TempData["err"] = "ExpertArticle_2，資料新增失敗";
				}
			}
			else
			{
				ErrorRecordModel.Error_Record(Method.Get_UserId_Admin(Request.Cookies, Session), "ExpertArticle_3", Request.UserAgent, true, "資料新增失敗");
				TempData["err"] = "ExpertArticle_3，資料新增失敗";
			}
			db.Connection.Close();
			TempData["FromAction"] = "新增成功！";

			return RedirectToAction("Index");
		}

		#endregion

		#region 編輯

		[MyAuthorize(Com = Competence.Update, function = "文章資料1")]
		public ActionResult Edit(int id = 0)
		{
			MithDataContext db = new MithDataContext();
			ExpertArticleModel data = new ExpertArticleModel();
			var fun = FunctionModel.Get_One(fun_id);

			ViewBag.Title = "達人管理 > " + "文章資料" + " > 編輯";
			ViewBag.Title_link = "達人管理 > <a href=/GoX/ExpertArticle/Index>" + "文章資料" + "</a> > 編輯";
			ViewBag.c_title = fun.Name;

			var item = data.Get_One(id);
			if (item == null)
			{
				return RedirectToAction("Create_Step1");
			}
			db.Connection.Close();
			return View(item);
		}

		[MyAuthorize(Com = Competence.Update, function = "文章資料1")]
		[ValidateInput(true)]
		[ValidateAntiForgeryToken]
		[HttpPost]
		public ActionResult Edit(ExpertArticleModel.ExpertArticleShow item, HttpPostedFileBase Pic1, HttpPostedFileBase Pic2, HttpPostedFileBase Pic3, HttpPostedFileBase Pic4, HttpPostedFileBase Pic5)
		{
			MithDataContext db = new MithDataContext();
			ExpertArticleModel data = new ExpertArticleModel();
			string url = (string)Session["Url"];

			string sContainer = "article";
			string sImagePath = item.Id + "/";
			string FileExtension = "";
			string name = Method.RandomKey(5, true, false, false, false);

			Size Middle = new Size();
			Size Small = new Size();

			#region 刪除文章圖片
			if (item.Delte_Pic1 == true)
			{
				BlobHelper.delete(sContainer + "/" + item.Id, Path.GetFileName(item.Pic1));
				item.Pic1 = null;
			}
			if (item.Delte_Pic2 == true)
			{
				BlobHelper.delete(sContainer + "/" + item.Id, Path.GetFileName(item.Pic2));
				item.Pic2 = null;
			}
			if (item.Delte_Pic3 == true)
			{
				BlobHelper.delete(sContainer + "/" + item.Id, Path.GetFileName(item.Pic3));
				item.Pic3 = null;
			}
			if (item.Delte_Pic4 == true)
			{
				BlobHelper.delete(sContainer + "/" + item.Id, Path.GetFileName(item.Pic4));
				item.Pic4 = null;
			}
			if (item.Delte_Pic5 == true)
			{
				BlobHelper.delete(sContainer + "/" + item.Id, Path.GetFileName(item.Pic5));
				item.Pic5 = null;
			}
			#endregion

			if (ModelState.IsValid)
			{
				#region 更新圖片


				if (Pic1 != null)
				{
					FileExtension = System.IO.Path.GetExtension(Pic1.FileName);

					//先刪除舊圖片
					BlobHelper.delete(sContainer + "/" + item.Id, Path.GetFileName(item.Pic1));

					//上傳大頭貼
					item.Pic1 = BlobHelper.Upload_Blob(new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + "1" + FileExtension }, Pic1.InputStream, name + "1" + FileExtension, Middle, Small, false)[0];
				}

				if (Pic2 != null)
				{
					FileExtension = System.IO.Path.GetExtension(Pic2.FileName);

					//先刪除舊圖片
					BlobHelper.delete(sContainer + "/" + item.Id, Path.GetFileName(item.Pic2));

					//上傳大頭貼
					item.Pic2 = BlobHelper.Upload_Blob(new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + "2" + FileExtension }, Pic2.InputStream, name + "2" + FileExtension, Middle, Small, false)[0];
				}

				if (Pic3 != null)
				{
					FileExtension = System.IO.Path.GetExtension(Pic3.FileName);

					//先刪除舊圖片
					BlobHelper.delete(sContainer + "/" + item.Id, Path.GetFileName(item.Pic3));

					//上傳大頭貼
					item.Pic3 = BlobHelper.Upload_Blob(new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + "3" + FileExtension }, Pic3.InputStream, name + "3" + FileExtension, Middle, Small, false)[0];
				}

				if (Pic4 != null)
				{
					FileExtension = System.IO.Path.GetExtension(Pic4.FileName);

					//先刪除舊圖片
					BlobHelper.delete(sContainer + "/" + item.Id, Path.GetFileName(item.Pic4));

					//上傳大頭貼
					item.Pic4 = BlobHelper.Upload_Blob(new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + "4" + FileExtension }, Pic4.InputStream, name + "4" + FileExtension, Middle, Small, false)[0];
				}

				if (Pic5 != null)
				{
					FileExtension = System.IO.Path.GetExtension(Pic5.FileName);

					//先刪除舊圖片
					BlobHelper.delete(sContainer + "/" + item.Id, Path.GetFileName(item.Pic5));

					//上傳大頭貼
					item.Pic5 = BlobHelper.Upload_Blob(new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + "5" + FileExtension }, Pic5.InputStream, name + "5" + FileExtension, Middle, Small, false)[0];
				}

				#endregion

				int user_id = Method.Get_UserId_Admin(Request.Cookies, Session);
				item.LastUserId = user_id;

				if (data.Update(item) <= 0)
				{
					ErrorRecordModel.Error_Record(Method.Get_UserId_Admin(Request.Cookies, Session), "ExpertArticle_5", Request.UserAgent, true, "資料新增失敗");
					TempData["err"] = "ExpertArticle_5";
					return RedirectToAction("Index");
				}
			}

			db.Connection.Close();
			TempData["FromAction"] = "儲存成功！";
			if (!string.IsNullOrWhiteSpace(url))
			{
				return Redirect(url);
			}
			else
			{
				return RedirectToAction("Index");
			}
		}

		#endregion

		#region 刪除

		[MyAuthorize(Com = Competence.Delete, function = "文章資料1")]
		public ActionResult Delete(int item, int p = 1)
		{
			string url = (string)Session["Url"];
			ExpertArticleModel data = new ExpertArticleModel();
			data.Delete(item);
			TempData["FromAction"] = "刪除成功！";

			if (!string.IsNullOrWhiteSpace(url))
			{
				return Redirect(url);
			}
			else
			{
				return RedirectToAction("Index");
			}
		}

		#endregion

		public JsonResult Get_GoodsAutocomplete(string search)
		{
			MithDataContext db = new MithDataContext();
			var data2 = db.Goods.Where(w => w.Status == 2 && w.SN.Contains(search)).ToList();
			List<string> data = new List<string>();
			foreach (var item in data2)
			{
				data.Add(item.SN);
			}
			var result = data.ToList();
			return Json(result, JsonRequestBehavior.AllowGet);
		}
	}
}

