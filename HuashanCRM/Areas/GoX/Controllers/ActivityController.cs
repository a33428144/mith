﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using System.Web.UI.DataVisualization.Charting;
using System.Text;
using System.IO;
using System.Drawing;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Mith.Models;
using Mith.Areas.GoX.Models;

namespace Mith.Areas.GoX.Controllers
{
	public class ActivityController : CategoryController
	{

		#region 最新商品
		[RoleAuthorize(Role = "亞設人員")]
		[MyAuthorize(Com = Competence.Read, function = "最新商品")]
		public ActionResult New_Index(string Search_Pattern = "T")
		{
			MithDataContext db = new MithDataContext();
			ActivityModel data = new ActivityModel();
			var fun = FunctionModel.Get_One(18);
			string get = "";
			data.Search_Pattern = Search_Pattern;
			string PatternName = "";
			PatternName = Method.Get_PatternName(Search_Pattern);
			ViewData["Search_Pattern"] = Search_Pattern;
			ViewBag.Title = fun.Name;
			ViewBag.Title_link = "活動管理 > <a href=\"\">" + fun.Name + " (" + PatternName + ")" + "</a>";
			ViewData["get"] = get;

			data.Search_Group = "New";
			data.IsSearch = false;
			db.Connection.Close();
			return View(data.Get_Data());
		}

		[HttpPost]
		public ActionResult New_Area1(string Search_Area = "", string keyword = "", int Search_Type = 0, string Search_Pattern = "T", int Search_Season = 0, int Search_Color = 0, int Search_Style = 0, int Search_Fit = 0, int Search_SpecialOffer = 0, DateTime? CreateTime_St = null, DateTime? CreateTime_End = null, int Search_CompanyId = 0)
		{
			MithDataContext db = new MithDataContext();
			ActivityModel data = new ActivityModel();
			string get = "";
			data.Search_Pattern = Search_Pattern;
			string PatternName = "";
			PatternName = Method.Get_PatternName(Search_Pattern);
			ViewData["Search_Pattern"] = Search_Pattern;
			if (keyword != "")
			{
				data.Keyword = keyword;
				ViewData["keyword"] = keyword;
				get += Method.Get_URLGet("keyword", keyword);
			}

			#region Search_變數
			if (Search_Type != 0)
			{
				data.Search_Type = Search_Type;
				ViewData["Search_Type"] = Search_Type;
				get += Method.Get_URLGet("Search_Type", Search_Type.ToString());
			}
			if (Search_Season != 0)
			{
				data.Search_Season = Search_Season;
				ViewData["Search_Season"] = Search_Season;
				get += Method.Get_URLGet("Search_Season", Search_Season.ToString());
			}
			if (Search_Color != 0)
			{
				data.Search_Color = Search_Color;
				ViewData["Search_Color"] = Search_Color;
				get += Method.Get_URLGet("Search_Color", Search_Color.ToString());
			}
			if (Search_Style != 0)
			{
				data.Search_Style = Search_Style;
				ViewData["Search_Style"] = Search_Style;
				get += Method.Get_URLGet("Search_Style", Search_Style.ToString());
			}
			if (Search_Fit != 0)
			{
				data.Search_Fit = Search_Fit;
				ViewData["Search_Fit"] = Search_Fit;
				get += Method.Get_URLGet("Search_Fit", Search_Fit.ToString());
			}
			if (Search_SpecialOffer != 0)
			{
				data.Search_SpecialOffer = Search_SpecialOffer;
				ViewData["Search_SpecialOffer"] = Search_SpecialOffer;
				get += Method.Get_URLGet("Search_SpecialOffer", Search_SpecialOffer.ToString());
			}
			if (Search_CompanyId != 0)
			{
				data.Search_CompanyId = Search_CompanyId;
				ViewData["Search_CompanyId"] = Search_CompanyId;
				get += Method.Get_URLGet("Search_CompanyId", Search_CompanyId.ToString());
			}
			#endregion

			if (CreateTime_St != null)
			{
				data.CreateTime_St = CreateTime_St;
				ViewData["CreateTime_St"] = CreateTime_St;
				get += Method.Get_URLGet("CreateTime_St", CreateTime_St.Value.ToString("yyyy/MM/dd HH:mm:ss"));
			}
			if (CreateTime_End != null)
			{
				data.CreateTime_End = CreateTime_End;
				ViewData["CreateTime_End"] = CreateTime_End;
				get += Method.Get_URLGet("CreateTime_End", CreateTime_End.Value.ToString("yyyy/MM/dd HH:mm:ss"));
			}

			ViewData["get"] = get;

			#region 從哪個頁面來，是否有搜尋
			data.Search_Group = "New";
			data.IsSearch = true;
			data.Search_Area = Search_Area;
			#endregion

			db.Connection.Close();
			return PartialView(data.Get_Data());
		}

		[HttpPost]
		public ActionResult New_Area2(string Search_Area = "", string keyword = "", int Search_Type = 0, string Search_Pattern = "T", int Search_Season = 0, int Search_Color = 0, int Search_Style = 0, int Search_Fit = 0, int Search_SpecialOffer = 0, DateTime? CreateTime_St = null, DateTime? CreateTime_End = null, int Search_CompanyId = 0)
		{
			MithDataContext db = new MithDataContext();
			ActivityModel data = new ActivityModel();

			string get = "";
			data.Search_Pattern = Search_Pattern;
			string PatternName = "";
			PatternName = Method.Get_PatternName(Search_Pattern);
			ViewData["Search_Pattern"] = Search_Pattern;
			if (keyword != "")
			{
				data.Keyword = keyword;
				ViewData["keyword"] = keyword;
				get += Method.Get_URLGet("keyword", keyword);
			}

			#region Search_變數
			if (Search_Type != 0)
			{
				data.Search_Type = Search_Type;
				ViewData["Search_Type"] = Search_Type;
				get += Method.Get_URLGet("Search_Type", Search_Type.ToString());
			}
			if (Search_Season != 0)
			{
				data.Search_Season = Search_Season;
				ViewData["Search_Season"] = Search_Season;
				get += Method.Get_URLGet("Search_Season", Search_Season.ToString());
			}
			if (Search_Color != 0)
			{
				data.Search_Color = Search_Color;
				ViewData["Search_Color"] = Search_Color;
				get += Method.Get_URLGet("Search_Color", Search_Color.ToString());
			}
			if (Search_Style != 0)
			{
				data.Search_Style = Search_Style;
				ViewData["Search_Style"] = Search_Style;
				get += Method.Get_URLGet("Search_Style", Search_Style.ToString());
			}
			if (Search_Fit != 0)
			{
				data.Search_Fit = Search_Fit;
				ViewData["Search_Fit"] = Search_Fit;
				get += Method.Get_URLGet("Search_Fit", Search_Fit.ToString());
			}
			if (Search_SpecialOffer != 0)
			{
				data.Search_SpecialOffer = Search_SpecialOffer;
				ViewData["Search_SpecialOffer"] = Search_SpecialOffer;
				get += Method.Get_URLGet("Search_SpecialOffer", Search_SpecialOffer.ToString());
			}
			if (Search_CompanyId != 0)
			{
				data.Search_CompanyId = Search_CompanyId;
				ViewData["Search_CompanyId"] = Search_CompanyId;
				get += Method.Get_URLGet("Search_CompanyId", Search_CompanyId.ToString());
			}
			#endregion

			if (CreateTime_St != null)
			{
				data.CreateTime_St = CreateTime_St;
				ViewData["CreateTime_St"] = CreateTime_St;
				get += Method.Get_URLGet("CreateTime_St", CreateTime_St.Value.ToString("yyyy/MM/dd HH:mm:ss"));
			}
			if (CreateTime_End != null)
			{
				data.CreateTime_End = CreateTime_End;
				ViewData["CreateTime_End"] = CreateTime_End;
				get += Method.Get_URLGet("CreateTime_End", CreateTime_End.Value.ToString("yyyy/MM/dd HH:mm:ss"));
			}
			ViewData["get"] = get;

			#region 從哪個頁面來，是否有搜尋
			data.Search_Group = "New";
			data.IsSearch = true;
			data.Search_Area = Search_Area;
			#endregion
			db.Connection.Close();
			return PartialView(data.Get_Data());
		}

		#endregion

		#region 前100件展示款

		[RoleAuthorize(Role = "亞設人員")]
		[MyAuthorize(Com = Competence.Read, function = "前100件展示")]
		public ActionResult Promotion100_Index(string Search_Pattern = "T")
		{
			MithDataContext db = new MithDataContext();
			ActivityModel data = new ActivityModel();
			var fun = FunctionModel.Get_One(19);
			string get = "";
			data.Search_Pattern = Search_Pattern;
			string PatternName = "";
			PatternName = Method.Get_PatternName(Search_Pattern);
			ViewData["Search_Pattern"] = Search_Pattern;
			ViewBag.Title = fun.Name;
			ViewBag.Title_link = "活動管理 > <a href=\"\">" + fun.Name + " (" + PatternName + ")" + "</a>";
			ViewData["get"] = get;

			data.Search_Group = "Promotion100";
			data.IsSearch = false;
			db.Connection.Close();
			return View(data.Get_Data());
		}

		[HttpPost]
		public ActionResult Promotion100_Area1(string Search_Area = "", string keyword = "", int Search_Type = 0, string Search_Pattern = "T", int Search_Season = 0, int Search_Color = 0, int Search_Style = 0, int Search_Fit = 0, int Search_SpecialOffer = 0, DateTime? CreateTime_St = null, DateTime? CreateTime_End = null, int Search_CompanyId = 0)
		{
			MithDataContext db = new MithDataContext();
			ActivityModel data = new ActivityModel();
			string get = "";
			data.Search_Pattern = Search_Pattern;
			string PatternName = "";
			PatternName = Method.Get_PatternName(Search_Pattern);
			ViewData["Search_Pattern"] = Search_Pattern;
			if (keyword != "")
			{
				data.Keyword = keyword;
				ViewData["keyword"] = keyword;
				get += Method.Get_URLGet("keyword", keyword);
			}

			#region Search_變數
			if (Search_Type != 0)
			{
				data.Search_Type = Search_Type;
				ViewData["Search_Type"] = Search_Type;
				get += Method.Get_URLGet("Search_Type", Search_Type.ToString());
			}
			if (Search_Season != 0)
			{
				data.Search_Season = Search_Season;
				ViewData["Search_Season"] = Search_Season;
				get += Method.Get_URLGet("Search_Season", Search_Season.ToString());
			}
			if (Search_Color != 0)
			{
				data.Search_Color = Search_Color;
				ViewData["Search_Color"] = Search_Color;
				get += Method.Get_URLGet("Search_Color", Search_Color.ToString());
			}
			if (Search_Style != 0)
			{
				data.Search_Style = Search_Style;
				ViewData["Search_Style"] = Search_Style;
				get += Method.Get_URLGet("Search_Style", Search_Style.ToString());
			}
			if (Search_Fit != 0)
			{
				data.Search_Fit = Search_Fit;
				ViewData["Search_Fit"] = Search_Fit;
				get += Method.Get_URLGet("Search_Fit", Search_Fit.ToString());
			}
			if (Search_SpecialOffer != 0)
			{
				data.Search_SpecialOffer = Search_SpecialOffer;
				ViewData["Search_SpecialOffer"] = Search_SpecialOffer;
				get += Method.Get_URLGet("Search_SpecialOffer", Search_SpecialOffer.ToString());
			}
			if (Search_CompanyId != 0)
			{
				data.Search_CompanyId = Search_CompanyId;
				ViewData["Search_CompanyId"] = Search_CompanyId;
				get += Method.Get_URLGet("Search_CompanyId", Search_CompanyId.ToString());
			}
			#endregion

			if (CreateTime_St != null)
			{
				data.CreateTime_St = CreateTime_St;
				ViewData["CreateTime_St"] = CreateTime_St;
				get += Method.Get_URLGet("CreateTime_St", CreateTime_St.Value.ToString("yyyy/MM/dd HH:mm:ss"));
			}
			if (CreateTime_End != null)
			{
				data.CreateTime_End = CreateTime_End;
				ViewData["CreateTime_End"] = CreateTime_End;
				get += Method.Get_URLGet("CreateTime_End", CreateTime_End.Value.ToString("yyyy/MM/dd HH:mm:ss"));
			}
			ViewData["get"] = get;

			#region 從哪個頁面來，是否有搜尋
			data.Search_Group = "Promotion100";
			data.IsSearch = true;
			data.Search_Area = Search_Area;
			#endregion

			db.Connection.Close();
			return PartialView(data.Get_Data());
		}

		[HttpPost]
		public ActionResult Promotion100_Area2(string Search_Area = "", string keyword = "", int Search_Type = 0, string Search_Pattern = "T", int Search_Season = 0, int Search_Color = 0, int Search_Style = 0, int Search_Fit = 0, int Search_SpecialOffer = 0, DateTime? CreateTime_St = null, DateTime? CreateTime_End = null, int Search_CompanyId = 0)
		{
			MithDataContext db = new MithDataContext();
			ActivityModel data = new ActivityModel();
			string get = "";
			data.Search_Pattern = Search_Pattern;
			string PatternName = "";
			PatternName = Method.Get_PatternName(Search_Pattern);
			ViewData["Search_Pattern"] = Search_Pattern;
			if (keyword != "")
			{
				data.Keyword = keyword;
				ViewData["keyword"] = keyword;
				get += Method.Get_URLGet("keyword", keyword);
			}

			#region Search_變數
			if (Search_Type != 0)
			{
				data.Search_Type = Search_Type;
				ViewData["Search_Type"] = Search_Type;
				get += Method.Get_URLGet("Search_Type", Search_Type.ToString());
			}
			if (Search_Season != 0)
			{
				data.Search_Season = Search_Season;
				ViewData["Search_Season"] = Search_Season;
				get += Method.Get_URLGet("Search_Season", Search_Season.ToString());
			}
			if (Search_Color != 0)
			{
				data.Search_Color = Search_Color;
				ViewData["Search_Color"] = Search_Color;
				get += Method.Get_URLGet("Search_Color", Search_Color.ToString());
			}
			if (Search_Style != 0)
			{
				data.Search_Style = Search_Style;
				ViewData["Search_Style"] = Search_Style;
				get += Method.Get_URLGet("Search_Style", Search_Style.ToString());
			}
			if (Search_Fit != 0)
			{
				data.Search_Fit = Search_Fit;
				ViewData["Search_Fit"] = Search_Fit;
				get += Method.Get_URLGet("Search_Fit", Search_Fit.ToString());
			}
			if (Search_SpecialOffer != 0)
			{
				data.Search_SpecialOffer = Search_SpecialOffer;
				ViewData["Search_SpecialOffer"] = Search_SpecialOffer;
				get += Method.Get_URLGet("Search_SpecialOffer", Search_SpecialOffer.ToString());
			}
			if (Search_CompanyId != 0)
			{
				data.Search_CompanyId = Search_CompanyId;
				ViewData["Search_CompanyId"] = Search_CompanyId;
				get += Method.Get_URLGet("Search_CompanyId", Search_CompanyId.ToString());
			}
			#endregion

			if (CreateTime_St != null)
			{
				data.CreateTime_St = CreateTime_St;
				ViewData["CreateTime_St"] = CreateTime_St;
				get += Method.Get_URLGet("CreateTime_St", CreateTime_St.Value.ToString("yyyy/MM/dd HH:mm:ss"));
			}
			if (CreateTime_End != null)
			{
				data.CreateTime_End = CreateTime_End;
				ViewData["CreateTime_End"] = CreateTime_End;
				get += Method.Get_URLGet("CreateTime_End", CreateTime_End.Value.ToString("yyyy/MM/dd HH:mm:ss"));
			}
			ViewData["get"] = get;

			#region 從哪個頁面來，是否有搜尋
			data.Search_Group = "Promotion100";
			data.IsSearch = true;
			data.Search_Area = Search_Area;
			#endregion
			db.Connection.Close();
			return PartialView(data.Get_Data());
		}

		#endregion

		#region 熱銷 Top30款

		[RoleAuthorize(Role = "亞設人員")]
		[MyAuthorize(Com = Competence.Read, function = "熱銷 Top30")]
		public ActionResult Hot30_Index(string Search_Pattern = "T")
		{
			MithDataContext db = new MithDataContext();
			ActivityModel data = new ActivityModel();
			var fun = FunctionModel.Get_One(20);
			string get = "";
			data.Search_Pattern = Search_Pattern;
			string PatternName = "";
			PatternName = Method.Get_PatternName(Search_Pattern);
			ViewData["Search_Pattern"] = Search_Pattern;
			ViewBag.Title = fun.Name;
			ViewBag.Title_link = "活動管理 > <a href=\"\">" + fun.Name + " (" + PatternName + ")" + "</a>";
			ViewData["get"] = get;

			data.Search_Group = "Hot30";
			data.IsSearch = false;
			db.Connection.Close();
			return View(data.Get_Data());
		}

		[HttpPost]
		public ActionResult Hot30_Area1(string Search_Area = "", string keyword = "", int Search_Type = 0, string Search_Pattern = "T", int Search_Season = 0, int Search_Color = 0, int Search_Style = 0, int Search_Fit = 0, int Search_SpecialOffer = 0, DateTime? CreateTime_St = null, DateTime? CreateTime_End = null, int Search_CompanyId = 0)
		{
			MithDataContext db = new MithDataContext();
			ActivityModel data = new ActivityModel();
			string get = "";
			data.Search_Pattern = Search_Pattern;
			string PatternName = "";
			PatternName = Method.Get_PatternName(Search_Pattern);
			ViewData["Search_Pattern"] = Search_Pattern;
			if (keyword != "")
			{
				data.Keyword = keyword;
				ViewData["keyword"] = keyword;
				get += Method.Get_URLGet("keyword", keyword);
			}

			#region Search_變數
			if (Search_Type != 0)
			{
				data.Search_Type = Search_Type;
				ViewData["Search_Type"] = Search_Type;
				get += Method.Get_URLGet("Search_Type", Search_Type.ToString());
			}
			if (Search_Season != 0)
			{
				data.Search_Season = Search_Season;
				ViewData["Search_Season"] = Search_Season;
				get += Method.Get_URLGet("Search_Season", Search_Season.ToString());
			}
			if (Search_Color != 0)
			{
				data.Search_Color = Search_Color;
				ViewData["Search_Color"] = Search_Color;
				get += Method.Get_URLGet("Search_Color", Search_Color.ToString());
			}
			if (Search_Style != 0)
			{
				data.Search_Style = Search_Style;
				ViewData["Search_Style"] = Search_Style;
				get += Method.Get_URLGet("Search_Style", Search_Style.ToString());
			}
			if (Search_Fit != 0)
			{
				data.Search_Fit = Search_Fit;
				ViewData["Search_Fit"] = Search_Fit;
				get += Method.Get_URLGet("Search_Fit", Search_Fit.ToString());
			}
			if (Search_SpecialOffer != 0)
			{
				data.Search_SpecialOffer = Search_SpecialOffer;
				ViewData["Search_SpecialOffer"] = Search_SpecialOffer;
				get += Method.Get_URLGet("Search_SpecialOffer", Search_SpecialOffer.ToString());
			}
			if (Search_CompanyId != 0)
			{
				data.Search_CompanyId = Search_CompanyId;
				ViewData["Search_CompanyId"] = Search_CompanyId;
				get += Method.Get_URLGet("Search_CompanyId", Search_CompanyId.ToString());
			}
			#endregion

			if (CreateTime_St != null)
			{
				data.CreateTime_St = CreateTime_St;
				ViewData["CreateTime_St"] = CreateTime_St;
				get += Method.Get_URLGet("CreateTime_St", CreateTime_St.Value.ToString("yyyy/MM/dd HH:mm:ss"));
			}
			if (CreateTime_End != null)
			{
				data.CreateTime_End = CreateTime_End;
				ViewData["CreateTime_End"] = CreateTime_End;
				get += Method.Get_URLGet("CreateTime_End", CreateTime_End.Value.ToString("yyyy/MM/dd HH:mm:ss"));
			}
			ViewData["get"] = get;

			#region 從哪個頁面來，是否有搜尋
			data.Search_Group = "Hot30";
			data.IsSearch = true;
			data.Search_Area = Search_Area;
			#endregion

			db.Connection.Close();
			return PartialView(data.Get_Data());
		}

		[HttpPost]
		public ActionResult Hot30_Area2(string Search_Area = "", string keyword = "", int Search_Type = 0, string Search_Pattern = "T", int Search_Season = 0, int Search_Color = 0, int Search_Style = 0, int Search_Fit = 0, int Search_SpecialOffer = 0, DateTime? CreateTime_St = null, DateTime? CreateTime_End = null, int Search_CompanyId = 0)
		{
			MithDataContext db = new MithDataContext();
			ActivityModel data = new ActivityModel();
			string get = "";
			data.Search_Pattern = Search_Pattern;
			string PatternName = "";
			PatternName = Method.Get_PatternName(Search_Pattern);
			ViewData["Search_Pattern"] = Search_Pattern;
			if (keyword != "")
			{
				data.Keyword = keyword;
				ViewData["keyword"] = keyword;
				get += Method.Get_URLGet("keyword", keyword);
			}

			#region Search_變數
			if (Search_Type != 0)
			{
				data.Search_Type = Search_Type;
				ViewData["Search_Type"] = Search_Type;
				get += Method.Get_URLGet("Search_Type", Search_Type.ToString());
			}
			if (Search_Season != 0)
			{
				data.Search_Season = Search_Season;
				ViewData["Search_Season"] = Search_Season;
				get += Method.Get_URLGet("Search_Season", Search_Season.ToString());
			}
			if (Search_Color != 0)
			{
				data.Search_Color = Search_Color;
				ViewData["Search_Color"] = Search_Color;
				get += Method.Get_URLGet("Search_Color", Search_Color.ToString());
			}
			if (Search_Style != 0)
			{
				data.Search_Style = Search_Style;
				ViewData["Search_Style"] = Search_Style;
				get += Method.Get_URLGet("Search_Style", Search_Style.ToString());
			}
			if (Search_Fit != 0)
			{
				data.Search_Fit = Search_Fit;
				ViewData["Search_Fit"] = Search_Fit;
				get += Method.Get_URLGet("Search_Fit", Search_Fit.ToString());
			}
			if (Search_SpecialOffer != 0)
			{
				data.Search_SpecialOffer = Search_SpecialOffer;
				ViewData["Search_SpecialOffer"] = Search_SpecialOffer;
				get += Method.Get_URLGet("Search_SpecialOffer", Search_SpecialOffer.ToString());
			}
			if (Search_CompanyId != 0)
			{
				data.Search_CompanyId = Search_CompanyId;
				ViewData["Search_CompanyId"] = Search_CompanyId;
				get += Method.Get_URLGet("Search_CompanyId", Search_CompanyId.ToString());
			}
			#endregion

			if (CreateTime_St != null)
			{
				data.CreateTime_St = CreateTime_St;
				ViewData["CreateTime_St"] = CreateTime_St;
				get += Method.Get_URLGet("CreateTime_St", CreateTime_St.Value.ToString("yyyy/MM/dd HH:mm:ss"));
			}
			if (CreateTime_End != null)
			{
				data.CreateTime_End = CreateTime_End;
				ViewData["CreateTime_End"] = CreateTime_End;
				get += Method.Get_URLGet("CreateTime_End", CreateTime_End.Value.ToString("yyyy/MM/dd HH:mm:ss"));
			}
			ViewData["get"] = get;

			#region 從哪個頁面來，是否有搜尋
			data.Search_Group = "Hot30";
			data.IsSearch = true;
			data.Search_Area = Search_Area;
			#endregion
			db.Connection.Close();
			return PartialView(data.Get_Data());
		}

		#endregion

		#region 活動圖片
		[RoleAuthorize(Role = "亞設人員")]
		[MyAuthorize(Com = Competence.Read, function = "活動圖片")]
		public ActionResult ActivityPic_Index(int Search_ActivityPicId = 1)
		{
			MithDataContext db = new MithDataContext();
			ActivityModel data = new ActivityModel();
			var fun = FunctionModel.Get_One(21);
			string get = "";

			data.Search_ActivityPicId = Search_ActivityPicId;
			string ActivityPicName = "";
			ActivityPicName = Method.Get_ActivityPicName(Search_ActivityPicId);
			ViewData["Search_ActivityPicId"] = Search_ActivityPicId;

			#region 取得活動圖片
			switch (Search_ActivityPicId)
			{
				case 1:
					ViewData["Pic"] = db.ActivityPic.FirstOrDefault(f => f.Id == 1).Pic1;
					break;
				case 2:
					ViewData["Pic"] = db.ActivityPic.FirstOrDefault(f => f.Id == 1).Pic2;
					break;
				case 3:
					ViewData["Pic"] = db.ActivityPic.FirstOrDefault(f => f.Id == 1).Pic3;
					break;
				case 4:
					ViewData["Pic"] = db.ActivityPic.FirstOrDefault(f => f.Id == 1).Pic4;
					break;
				case 5:
					ViewData["Pic"] = db.ActivityPic.FirstOrDefault(f => f.Id == 1).Pic5;
					break;
			}
			#endregion

			ViewBag.Title = fun.Name;
			ViewBag.Title_link = "活動管理 > <a href=\"\">" + fun.Name + " (" + ActivityPicName + ")" + "</a>";
			ViewData["get"] = get;

			data.Search_Group = "ActivityPic";
			data.IsSearch = false;
			db.Connection.Close();
			return View(data.Get_Data());
		}

		[HttpPost]
		public ActionResult ActivityPic_Area1(string Search_Area = "", string keyword = "", int Search_Type = 0, int Search_ActivityPicId = 1, int Search_Season = 0, int Search_Color = 0, int Search_Style = 0, int Search_Fit = 0, int Search_SpecialOffer = 0, DateTime? CreateTime_St = null, DateTime? CreateTime_End = null, int Search_CompanyId = 0)
		{
			MithDataContext db = new MithDataContext();
			ActivityModel data = new ActivityModel();
			string get = "";

			data.Search_ActivityPicId = Search_ActivityPicId;
			string ActivityPicName = "";
			ActivityPicName = Method.Get_ActivityPicName(Search_ActivityPicId);
			ViewData["Search_ActivityPicId"] = Search_ActivityPicId;

			if (keyword != "")
			{
				data.Keyword = keyword;
				ViewData["keyword"] = keyword;
				get += Method.Get_URLGet("keyword", keyword);
			}

			#region Search_變數
			if (Search_Type != 0)
			{
				data.Search_Type = Search_Type;
				ViewData["Search_Type"] = Search_Type;
				get += Method.Get_URLGet("Search_Type", Search_Type.ToString());
			}
			if (Search_Season != 0)
			{
				data.Search_Season = Search_Season;
				ViewData["Search_Season"] = Search_Season;
				get += Method.Get_URLGet("Search_Season", Search_Season.ToString());
			}
			if (Search_Color != 0)
			{
				data.Search_Color = Search_Color;
				ViewData["Search_Color"] = Search_Color;
				get += Method.Get_URLGet("Search_Color", Search_Color.ToString());
			}
			if (Search_Style != 0)
			{
				data.Search_Style = Search_Style;
				ViewData["Search_Style"] = Search_Style;
				get += Method.Get_URLGet("Search_Style", Search_Style.ToString());
			}
			if (Search_Fit != 0)
			{
				data.Search_Fit = Search_Fit;
				ViewData["Search_Fit"] = Search_Fit;
				get += Method.Get_URLGet("Search_Fit", Search_Fit.ToString());
			}
			if (Search_SpecialOffer != 0)
			{
				data.Search_SpecialOffer = Search_SpecialOffer;
				ViewData["Search_SpecialOffer"] = Search_SpecialOffer;
				get += Method.Get_URLGet("Search_SpecialOffer", Search_SpecialOffer.ToString());
			}
			if (Search_CompanyId != 0)
			{
				data.Search_CompanyId = Search_CompanyId;
				ViewData["Search_CompanyId"] = Search_CompanyId;
				get += Method.Get_URLGet("Search_CompanyId", Search_CompanyId.ToString());
			}
			#endregion

			if (CreateTime_St != null)
			{
				data.CreateTime_St = CreateTime_St;
				ViewData["CreateTime_St"] = CreateTime_St;
				get += Method.Get_URLGet("CreateTime_St", CreateTime_St.Value.ToString("yyyy/MM/dd HH:mm:ss"));
			}
			if (CreateTime_End != null)
			{
				data.CreateTime_End = CreateTime_End;
				ViewData["CreateTime_End"] = CreateTime_End;
				get += Method.Get_URLGet("CreateTime_End", CreateTime_End.Value.ToString("yyyy/MM/dd HH:mm:ss"));
			}

			ViewData["get"] = get;

			#region 從哪個頁面來，是否有搜尋
			data.Search_Group = "ActivityPic";
			data.IsSearch = true;
			data.Search_Area = Search_Area;
			#endregion

			db.Connection.Close();
			return PartialView(data.Get_Data());
		}

		[HttpPost]
		public ActionResult ActivityPic_Area2(string Search_Area = "", string keyword = "", int Search_Type = 0, int Search_ActivityPicId = 1, int Search_Season = 0, int Search_Color = 0, int Search_Style = 0, int Search_Fit = 0, int Search_SpecialOffer = 0, DateTime? CreateTime_St = null, DateTime? CreateTime_End = null, int Search_CompanyId = 0)
		{
			MithDataContext db = new MithDataContext();
			ActivityModel data = new ActivityModel();

			string get = "";

			data.Search_ActivityPicId = Search_ActivityPicId;
			string ActivityPicName = "";
			ActivityPicName = Method.Get_ActivityPicName(Search_ActivityPicId);
			ViewData["Search_ActivityPicId"] = Search_ActivityPicId;

			if (keyword != "")
			{
				data.Keyword = keyword;
				ViewData["keyword"] = keyword;
				get += Method.Get_URLGet("keyword", keyword);
			}

			#region Search_變數
			if (Search_Type != 0)
			{
				data.Search_Type = Search_Type;
				ViewData["Search_Type"] = Search_Type;
				get += Method.Get_URLGet("Search_Type", Search_Type.ToString());
			}
			if (Search_Season != 0)
			{
				data.Search_Season = Search_Season;
				ViewData["Search_Season"] = Search_Season;
				get += Method.Get_URLGet("Search_Season", Search_Season.ToString());
			}
			if (Search_Color != 0)
			{
				data.Search_Color = Search_Color;
				ViewData["Search_Color"] = Search_Color;
				get += Method.Get_URLGet("Search_Color", Search_Color.ToString());
			}
			if (Search_Style != 0)
			{
				data.Search_Style = Search_Style;
				ViewData["Search_Style"] = Search_Style;
				get += Method.Get_URLGet("Search_Style", Search_Style.ToString());
			}
			if (Search_Fit != 0)
			{
				data.Search_Fit = Search_Fit;
				ViewData["Search_Fit"] = Search_Fit;
				get += Method.Get_URLGet("Search_Fit", Search_Fit.ToString());
			}
			if (Search_SpecialOffer != 0)
			{
				data.Search_SpecialOffer = Search_SpecialOffer;
				ViewData["Search_SpecialOffer"] = Search_SpecialOffer;
				get += Method.Get_URLGet("Search_SpecialOffer", Search_SpecialOffer.ToString());
			}
			if (Search_CompanyId != 0)
			{
				data.Search_CompanyId = Search_CompanyId;
				ViewData["Search_CompanyId"] = Search_CompanyId;
				get += Method.Get_URLGet("Search_CompanyId", Search_CompanyId.ToString());
			}
			#endregion

			if (CreateTime_St != null)
			{
				data.CreateTime_St = CreateTime_St;
				ViewData["CreateTime_St"] = CreateTime_St;
				get += Method.Get_URLGet("CreateTime_St", CreateTime_St.Value.ToString("yyyy/MM/dd HH:mm:ss"));
			}
			if (CreateTime_End != null)
			{
				data.CreateTime_End = CreateTime_End;
				ViewData["CreateTime_End"] = CreateTime_End;
				get += Method.Get_URLGet("CreateTime_End", CreateTime_End.Value.ToString("yyyy/MM/dd HH:mm:ss"));
			}
			ViewData["get"] = get;

			#region 從哪個頁面來，是否有搜尋
			data.Search_Group = "ActivityPic";
			data.IsSearch = true;
			data.Search_Area = Search_Area;
			#endregion
			db.Connection.Close();
			return PartialView(data.Get_Data());
		}

		[MyAuthorize(Com = Competence.Update, function = "文章資料1")]
		public ActionResult Edit_ActivityPic()
		{
			MithDataContext db = new MithDataContext();
			ActivityModel data = new ActivityModel();
			var fun = FunctionModel.Get_One(21);

			ViewBag.Title = fun.Name;
			ViewBag.Title_link = "活動管理 >  <a href=/GoX/Activity/ActivityPic_Index>活動圖片</a> > 編輯圖片";
			ViewBag.c_title = fun.Name;

			var item = data.Get_One();
			if (item == null)
			{
				return RedirectToAction("ActivityPic_Index");
			}
			db.Connection.Close();
			return View(item);
		}

		[ValidateInput(true)]
		[ValidateAntiForgeryToken]
		[HttpPost]
		public ActionResult Edit_ActivityPic(ActivityModel.ActivityShow item, HttpPostedFileBase Pic1, HttpPostedFileBase Pic2, HttpPostedFileBase Pic3, HttpPostedFileBase Pic4, HttpPostedFileBase Pic5)
		{
			MithDataContext db = new MithDataContext();
			ActivityModel data = new ActivityModel();

			string sContainer = "activity";
			string sImagePath = 1 + "/";
			string FileExtension = "";
			string name = Method.RandomKey(5, true, false, false, false);

			Size Middle = new Size();
			Size Small = new Size();


			#region 刪除文章圖片

			if (item.Delete_Pic1 == true)
			{
				BlobHelper.delete(sContainer + "/" + 1, Path.GetFileName(item.Pic1));
				item.Pic1 = null;
			}
			if (item.Delete_Pic2 == true)
			{
				BlobHelper.delete(sContainer + "/" + 1, Path.GetFileName(item.Pic2));
				item.Pic2 = null;
			}
			if (item.Delete_Pic3 == true)
			{
				BlobHelper.delete(sContainer + "/" + 1, Path.GetFileName(item.Pic3));
				item.Pic3 = null;
			}
			if (item.Delete_Pic4 == true)
			{
				BlobHelper.delete(sContainer + "/" + 1, Path.GetFileName(item.Pic4));
				item.Pic4 = null;
			}
			if (item.Delete_Pic5 == true)
			{
				BlobHelper.delete(sContainer + "/" + 1, Path.GetFileName(item.Pic5));
				item.Pic5 = null;
			}
			#endregion

			if (ModelState.IsValid)
			{
				#region 更新圖片

				if (Pic1 != null)
				{
					FileExtension = System.IO.Path.GetExtension(Pic1.FileName);

					//先刪除舊圖片
					BlobHelper.delete(sContainer + "/" + item.Id, Path.GetFileName(item.Pic1));

					//上傳活動圖
					item.Pic1 = BlobHelper.Upload_Blob(new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + "1" + FileExtension }, Pic1.InputStream, name + "1" + FileExtension, Middle, Small, false)[0];
				}

				if (Pic2 != null)
				{
					FileExtension = System.IO.Path.GetExtension(Pic2.FileName);

					//先刪除舊圖片
					BlobHelper.delete(sContainer + "/" + item.Id, Path.GetFileName(item.Pic2));

					//上傳活動圖
					item.Pic2 = BlobHelper.Upload_Blob(new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + "2" + FileExtension }, Pic2.InputStream, name + "2" + FileExtension, Middle, Small, false)[0];
				}

				if (Pic3 != null)
				{
					FileExtension = System.IO.Path.GetExtension(Pic3.FileName);

					//先刪除舊圖片
					BlobHelper.delete(sContainer + "/" + item.Id, Path.GetFileName(item.Pic3));

					//上傳活動圖
					item.Pic3 = BlobHelper.Upload_Blob(new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + "3" + FileExtension }, Pic3.InputStream, name + "3" + FileExtension, Middle, Small, false)[0];
				}

				if (Pic4 != null)
				{
					FileExtension = System.IO.Path.GetExtension(Pic4.FileName);

					//先刪除舊圖片
					BlobHelper.delete(sContainer + "/" + item.Id, Path.GetFileName(item.Pic4));

					//上傳活動圖
					item.Pic4 = BlobHelper.Upload_Blob(new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + "4" + FileExtension }, Pic4.InputStream, name + "4" + FileExtension, Middle, Small, false)[0];
				}

				if (Pic5 != null)
				{
					FileExtension = System.IO.Path.GetExtension(Pic5.FileName);

					//先刪除舊圖片
					BlobHelper.delete(sContainer + "/" + item.Id, Path.GetFileName(item.Pic5));

					//上傳活動圖
					item.Pic5 = BlobHelper.Upload_Blob(new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + "5" + FileExtension }, Pic5.InputStream, name + "5" + FileExtension, Middle, Small, false)[0];
				}

				#endregion

				if (data.Update(item) <= 0)
				{
					ErrorRecordModel.Error_Record(Method.Get_UserId_Admin(Request.Cookies, Session), "FashionArticle_5", Request.UserAgent, true, "資料新增失敗");
					TempData["err"] = "FashionArticle_5";
					return RedirectToAction("Index");
				}
			}

			db.Connection.Close();
			return RedirectToAction("Edit_ActivityPic");
		}

		#endregion

		#region Ajax
		[HttpPost]
		public ActionResult Ajax_OnTop(string chk_Activity, bool OnTop)
		{
			MithDataContext db = new MithDataContext();
			ActivityModel data = new ActivityModel();
			try
			{
				var data2 = chk_Activity.Split(",");

				int Index = 1;
				int Sort = 0;

				foreach (var item in data2)
				{
					if (Index == 1)
					{
						Sort = db.Goods.FirstOrDefault(f => f.OnTop == true) != null ? db.Goods.OrderByDescending(o => o.Sort).FirstOrDefault(f => f.OnTop == true).Sort : 1;
					}
					Sort++;
					data.Ajax_UpdateOnTop(int.Parse(item), OnTop, Sort);
					Index++;
				}
			}
			catch
			{
				return Content("Failed");
			}
			finally
			{
				db.Connection.Close();
			}
			return Content("Success");
		}

		[HttpPost]
		public ActionResult Ajax_Promotion100(string chk_Activity, bool Promotion100)
		{
			MithDataContext db = new MithDataContext();
			ActivityModel data = new ActivityModel();
			try
			{
				var data2 = chk_Activity.Split(",");

				int Index = 1;
				int Sort = 0;

				foreach (var item in data2)
				{
					if (Index == 1)
					{
						Sort = db.Goods.FirstOrDefault(f => f.Promotion100 == true) != null ? db.Goods.OrderByDescending(o => o.Sort).FirstOrDefault(f => f.Promotion100 == true).Sort : 1;
					}
					Sort++;
					data.Ajax_UpdatePromotion100(int.Parse(item), Promotion100, Sort);
					Index++;
				}
			}
			catch
			{
				return Content("Failed");
			}
			finally
			{
				db.Connection.Close();
			}
			return Content("Success");
		}

		[HttpPost]
		public ActionResult Ajax_Hot30(string chk_Activity, bool Hot30)
		{
			MithDataContext db = new MithDataContext();
			ActivityModel data = new ActivityModel();
			try
			{
				var data2 = chk_Activity.Split(",");

				int Index = 1;
				int Sort = 0;

				foreach (var item in data2)
				{
					if (Index == 1)
					{
						Sort = db.Goods.FirstOrDefault(f => f.Hot30 == true) != null ? db.Goods.OrderByDescending(o => o.Sort).FirstOrDefault(f => f.Hot30 == true).Sort : 1;
					}
					Sort++;
					data.Ajax_UpdateHot30(int.Parse(item), Hot30, Sort);
					Index++;
				}
			}
			catch
			{
				return Content("Failed");
			}
			finally
			{
				db.Connection.Close();
			}
			return Content("Success");
		}

		[HttpPost]
		public ActionResult Ajax_ActivityPic(string chk_Activity, int ActivityPicId = 0)
		{
			MithDataContext db = new MithDataContext();
			ActivityModel data = new ActivityModel();
			try
			{
				var data2 = chk_Activity.Split(",");

				int Index = 1;
				int Sort = 0;

				foreach (var item in data2)
				{
					if (Index == 1)
					{
						Sort = db.Goods.FirstOrDefault(f => f.ActivityPicId != null) != null ? db.Goods.OrderByDescending(o => o.Sort).FirstOrDefault(f => f.ActivityPicId != null).Sort : 1;
					}
					Sort++;
					data.Ajax_UpdateActivityPic(int.Parse(item), ActivityPicId, Sort);
					Index++;
				}
			}
			catch
			{
				return Content("Failed");
			}
			finally
			{
				db.Connection.Close();
			}
			return Content("Success");
		}

		#endregion
	}
}

