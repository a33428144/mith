﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;
using System.Text;
using System.IO;
using Mith.Models;
using Mith.Areas.GoX.Models;

namespace Mith.Areas.GoX.Controllers
{
	public class FashionArticleController : CategoryController
	{
		int fun_id = 29;

		#region 首頁
		//[RoleAuthorize(Role = "亞設人員")]
		[MyAuthorize(Com = Competence.Read, function = "文章資料2")]
		public ActionResult Index(string keyword = "", int Search_Type = 0, int Search_Display = 0, DateTime? CreateTime_St = null, DateTime? CreateTime_End = null, int p = 1, int show_number = 10)
		{
			FashionArticleModel data = new FashionArticleModel();
			var fun = FunctionModel.Get_One(fun_id);
			string get = "";
			if (keyword != "")
			{
				data.Keyword = keyword;
				ViewData["keyword"] = keyword;
				get += Method.Get_URLGet("keyword", keyword);
			}
			if (Search_Type != 0)
			{
				data.Search_Type = Search_Type;
				ViewData["Search_Type"] = Search_Type;
				get += Method.Get_URLGet("Search_Type", Search_Type.ToString());
			}
			if (Search_Display != 0)
			{
				data.Search_Display = Search_Display;
				ViewData["Search_Display"] = Search_Display;
				get += Method.Get_URLGet("Search_Display", Search_Display.ToString());
			}

			if (CreateTime_St != null)
			{
				data.CreateTime_St = CreateTime_St;
				ViewData["CreateTime_St"] = CreateTime_St;
				get += Method.Get_URLGet("CreateTime_St", CreateTime_St.Value.ToString("yyyy/MM/dd HH:mm:ss"));
			}
			if (CreateTime_End != null)
			{
				data.CreateTime_End = CreateTime_End;
				ViewData["CreateTime_End"] = CreateTime_End;
				get += Method.Get_URLGet("CreateTime_End", CreateTime_End.Value.ToString("yyyy/MM/dd HH:mm:ss"));
			}

			ViewBag.Title = fun.Name;
			ViewBag.Title_link = "時尚新訊 > <a href=\"\">" + "文章資料" + "</a>";

			ViewData["p"] = p;
			ViewData["page"] = data.Get_Page(p, show_number);
			ViewData["number"] = show_number;
			ViewData["get"] = get;
			Session["Url"] = Request.Url.AbsoluteUri;

			return View(data.Get_Data(p, show_number));
		}

		[MyAuthorize(Com = Competence.Read, function = "文章資料2")]
		public ActionResult Popular(int p = 1, int show_number = 10)
		{
			FashionArticleModel data = new FashionArticleModel();
			var fun = FunctionModel.Get_One(fun_id);
			string get = "";

			ViewBag.Title = "時尚新訊 > " + "文章資料" + " > 人氣排行 Top20";
			ViewBag.Title_link = "時尚新訊 > <a href=/GoX/FashionArticle/Index>" + "文章資料" + "</a> > " + "人氣排行 Top20";

			ViewData["p"] = p;
			ViewData["page"] = data.Get_Page(p, show_number, true);
			ViewData["number"] = show_number;
			ViewData["get"] = get;
			Session["Url"] = Request.Url.AbsoluteUri;

			return View(data.Get_Data_Popular(p, show_number));
		}

		#endregion

		#region 首頁設定
		[MyAuthorize(Com = Competence.Read, function = "文章資料2")]
		public ActionResult Show(string keyword = "", int Search_Type = 0, int Search_Display = 0, DateTime? CreateTime_St = null, DateTime? CreateTime_End = null)
		{
			FashionArticleModel data = new FashionArticleModel();
			var fun = FunctionModel.Get_One(fun_id);
			string get = "";
			if (keyword != "")
			{
				data.Keyword = keyword;
				ViewData["keyword"] = keyword;
				get += Method.Get_URLGet("keyword", keyword);
			}
			if (Search_Type != 0)
			{
				data.Search_Type = Search_Type;
				ViewData["Search_Type"] = Search_Type;
				get += Method.Get_URLGet("Search_Type", Search_Type.ToString());
			}
			if (Search_Display != 0)
			{
				data.Search_Display = Search_Display;
				ViewData["Search_Display"] = Search_Display;
				get += Method.Get_URLGet("Search_Display", Search_Display.ToString());
			}

			if (CreateTime_St != null)
			{
				data.CreateTime_St = CreateTime_St;
				ViewData["CreateTime_St"] = CreateTime_St;
				get += Method.Get_URLGet("CreateTime_St", CreateTime_St.Value.ToString("yyyy/MM/dd HH:mm:ss"));
			}
			if (CreateTime_End != null)
			{
				data.CreateTime_End = CreateTime_End;
				ViewData["CreateTime_End"] = CreateTime_End;
				get += Method.Get_URLGet("CreateTime_End", CreateTime_End.Value.ToString("yyyy/MM/dd HH:mm:ss"));
			}

			ViewBag.Title = fun.Name;
			ViewBag.Title_link = "時尚新訊 > <a href=\"\">" + "首頁文章" + "</a>";

			return View(data.Get_Data_Show());
		}

		[HttpPost]
		public ActionResult Show_Area1(string keyword = "", int Search_Type = 0, string Search_Area = "")
		{
			MithDataContext db = new MithDataContext();
			FashionArticleModel data = new FashionArticleModel();
			var fun = FunctionModel.Get_One(29);
			string get = "";
			if (keyword != "")
			{
				data.Keyword = keyword;
				ViewData["keyword"] = keyword;
				get += Method.Get_URLGet("keyword", keyword);
			}

			if (Search_Type != 0)
			{
				data.Search_Type = Search_Type;
				ViewData["Search_Type"] = Search_Type;
				get += Method.Get_URLGet("Search_Type", Search_Type.ToString());
			}

			ViewBag.Title = fun.Name;
			ViewBag.Title_link = "時尚新訊 > <a href=\"\">" + "首頁文章" + "</a>";
			ViewData["get"] = get;

			data.IsSearch = true;
			data.Search_Area = Search_Area;

			db.Connection.Close();
			return PartialView(data.Get_Data_Show());
		}

		[HttpPost]
		public ActionResult Show_Area2(string keyword = "", int Search_Type = 0, string Search_Area = "")
		{
			MithDataContext db = new MithDataContext();
			FashionArticleModel data = new FashionArticleModel();
			var fun = FunctionModel.Get_One(29);
			string get = "";
			if (keyword != "")
			{
				data.Keyword = keyword;
				ViewData["keyword"] = keyword;
				get += Method.Get_URLGet("keyword", keyword);
			}

			if (Search_Type != 0)
			{
				data.Search_Type = Search_Type;
				ViewData["Search_Type"] = Search_Type;
				get += Method.Get_URLGet("Search_Type", Search_Type.ToString());
			}

			ViewBag.Title = fun.Name;
			ViewBag.Title_link = "時尚新訊 > <a href=\"\">" + "首頁文章" + "</a>";
			ViewData["get"] = get;

			data.IsSearch = true;
			data.Search_Area = Search_Area;

			db.Connection.Close();
			return PartialView(data.Get_Data_Show());
		}

		#endregion

		#region 新增

		[MyAuthorize(Com = Competence.Insert, function = "文章資料2")]
		public ActionResult Create()
		{
			MithDataContext db = new MithDataContext();
			FashionArticleModel data = new FashionArticleModel();
			var fun = FunctionModel.Get_One(fun_id);

			ViewBag.Title = "時尚新訊 > " + "文章資料" + " > 新增";
			ViewBag.Title_link = "時尚新訊 > <a href=/GoX/FashionArticle/Index>" + "文章資料" + "</a> > 新增";
			ViewBag.c_title = fun.Name;
			db.Connection.Close();
			return View();
		}


		[ValidateInput(true)]
		[ValidateAntiForgeryToken]
		[HttpPost]
		public ActionResult Create(FashionArticleModel.FashionArticleShow item, HttpPostedFileBase Cover, HttpPostedFileBase Pic1, HttpPostedFileBase Pic2, HttpPostedFileBase Pic3, HttpPostedFileBase Pic4, HttpPostedFileBase Pic5)
		{

			MithDataContext db = new MithDataContext();
			FashionArticleModel data = new FashionArticleModel();
			var fun = FunctionModel.Get_One(fun_id);
			item.Id = db.Article.OrderByDescending(o => o.Id).FirstOrDefault() != null ? db.Article.OrderByDescending(o => o.Id).FirstOrDefault().Id + 1 : 1;
			item.LastUserId = Method.Get_UserId_Admin(Request.Cookies, Session);

			#region 上傳圖片
			string sContainer = "article";
			string sImagePath = item.Id + "/";
			string FileExtension = "";
			string name = Method.RandomKey(5, true, false, false, false);

			Size Middle = new Size();
			Size Small = new Size();

			if (Cover != null)
			{
				FileExtension = System.IO.Path.GetExtension(Cover.FileName);
				//上傳大頭貼
				item.Cover = BlobHelper.Upload_Blob(new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + FileExtension }, Cover.InputStream, name + FileExtension, Middle, Small, false)[0];
			}

			if (Pic1 != null)
			{
				FileExtension = System.IO.Path.GetExtension(Pic1.FileName);
				//上傳圖片1
				item.Pic1 = BlobHelper.Upload_Blob(new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + "1" + FileExtension }, Pic1.InputStream, name + "1" + FileExtension, Middle, Small, false)[0];
			}
			if (Pic2 != null)
			{
				FileExtension = System.IO.Path.GetExtension(Pic2.FileName);
				//上傳圖片2
				item.Pic2 = BlobHelper.Upload_Blob(new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + "2" + FileExtension }, Pic2.InputStream, name + "2" + FileExtension, Middle, Small, false)[0];
			}
			if (Pic3 != null)
			{
				FileExtension = System.IO.Path.GetExtension(Pic3.FileName);
				//上傳圖片3
				item.Pic3 = BlobHelper.Upload_Blob(new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + "3" + FileExtension }, Pic3.InputStream, name + "3" + FileExtension, Middle, Small, false)[0];
			}
			if (Pic4 != null)
			{
				FileExtension = System.IO.Path.GetExtension(Pic4.FileName);
				//上傳圖片4
				item.Pic4 = BlobHelper.Upload_Blob(new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + "4" + FileExtension }, Pic4.InputStream, name + "4" + FileExtension, Middle, Small, false)[0];
			}
			if (Pic5 != null)
			{
				FileExtension = System.IO.Path.GetExtension(Pic5.FileName);
				//上傳圖片5
				item.Pic5 = BlobHelper.Upload_Blob(new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + "5" + FileExtension }, Pic5.InputStream, name + "5" + FileExtension, Middle, Small, false)[0];
			}

			#endregion

			if (data.Insert(item) <= 0)
			{
				ErrorRecordModel.Error_Record(Method.Get_UserId_Admin(Request.Cookies, Session), "FashionArticle_2", Request.UserAgent, true, "資料新增失敗");
				TempData["err"] = "FashionArticle_2，資料新增失敗";
			}
			db.Connection.Close();
			TempData["FromAction"] = "新增成功！";

			return RedirectToAction("Index");
		}

		#endregion

		#region 編輯

		[MyAuthorize(Com = Competence.Update, function = "文章資料2")]
		public ActionResult Edit(int id = 0)
		{
			MithDataContext db = new MithDataContext();
			FashionArticleModel data = new FashionArticleModel();
			var fun = FunctionModel.Get_One(fun_id);

			ViewBag.Title = "時尚新訊 > " + "文章資料" + " > 編輯";
			ViewBag.Title_link = "時尚新訊 > <a href=/GoX/FashionArticle/Index>" + "文章資料" + "</a> > 編輯";
			ViewBag.c_title = fun.Name;

			var item = data.Get_One(id);
			if (item == null)
			{
				return RedirectToAction("Create_Step1");
			}
			db.Connection.Close();
			return View(item);
		}

		[ValidateInput(true)]
		[ValidateAntiForgeryToken]
		[HttpPost]
		public ActionResult Edit(FashionArticleModel.FashionArticleShow item, HttpPostedFileBase Cover, HttpPostedFileBase Pic1, HttpPostedFileBase Pic2, HttpPostedFileBase Pic3, HttpPostedFileBase Pic4, HttpPostedFileBase Pic5)
		{
			MithDataContext db = new MithDataContext();
			FashionArticleModel data = new FashionArticleModel();
			string url = (string)Session["Url"];

			string sContainer = "article";
			string sImagePath = item.Id + "/";
			string FileExtension = "";
			string name = Method.RandomKey(5, true, false, false, false);

			Size Middle = new Size();
			Size Small = new Size();

			#region 刪除文章圖片
			if (item.Delte_Cover == true)
			{
				BlobHelper.delete(sContainer + "/" + item.Id, Path.GetFileName(item.Cover));
				item.Cover = null;
			}
			if (item.Delte_Pic1 == true)
			{
				BlobHelper.delete(sContainer + "/" + item.Id, Path.GetFileName(item.Pic1));
				item.Pic1 = null;
			}
			if (item.Delte_Pic2 == true)
			{
				BlobHelper.delete(sContainer + "/" + item.Id, Path.GetFileName(item.Pic2));
				item.Pic2 = null;
			}
			if (item.Delte_Pic3 == true)
			{
				BlobHelper.delete(sContainer + "/" + item.Id, Path.GetFileName(item.Pic3));
				item.Pic3 = null;
			}
			if (item.Delte_Pic4 == true)
			{
				BlobHelper.delete(sContainer + "/" + item.Id, Path.GetFileName(item.Pic4));
				item.Pic4 = null;
			}
			if (item.Delte_Pic5 == true)
			{
				BlobHelper.delete(sContainer + "/" + item.Id, Path.GetFileName(item.Pic5));
				item.Pic5 = null;
			}
			#endregion

			#region 更新圖片
			if (Cover != null)
			{
				FileExtension = System.IO.Path.GetExtension(Cover.FileName);
				//先刪除舊圖片
				BlobHelper.delete(sContainer + "/" + item.Id, Path.GetFileName(item.Cover));

				//上傳封面照
				item.Cover = BlobHelper.Upload_Blob(new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + FileExtension }, Cover.InputStream, name + FileExtension, Middle, Small, false)[0];
			}

			if (Pic1 != null)
			{
				FileExtension = System.IO.Path.GetExtension(Pic1.FileName);

				//先刪除舊圖片
				BlobHelper.delete(sContainer + "/" + item.Id, Path.GetFileName(item.Pic1));

				//上傳大頭貼
				item.Pic1 = BlobHelper.Upload_Blob(new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + "1" + FileExtension }, Pic1.InputStream, name + "1" + FileExtension, Middle, Small, false)[0];
			}

			if (Pic2 != null)
			{
				FileExtension = System.IO.Path.GetExtension(Pic2.FileName);

				//先刪除舊圖片
				BlobHelper.delete(sContainer + "/" + item.Id, Path.GetFileName(item.Pic2));

				//上傳大頭貼
				item.Pic2 = BlobHelper.Upload_Blob(new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + "2" + FileExtension }, Pic2.InputStream, name + "2" + FileExtension, Middle, Small, false)[0];
			}

			if (Pic3 != null)
			{
				FileExtension = System.IO.Path.GetExtension(Pic3.FileName);

				//先刪除舊圖片
				BlobHelper.delete(sContainer + "/" + item.Id, Path.GetFileName(item.Pic3));

				//上傳大頭貼
				item.Pic3 = BlobHelper.Upload_Blob(new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + "3" + FileExtension }, Pic3.InputStream, name + "3" + FileExtension, Middle, Small, false)[0];
			}

			if (Pic4 != null)
			{
				FileExtension = System.IO.Path.GetExtension(Pic4.FileName);

				//先刪除舊圖片
				BlobHelper.delete(sContainer + "/" + item.Id, Path.GetFileName(item.Pic4));

				//上傳大頭貼
				item.Pic4 = BlobHelper.Upload_Blob(new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + "4" + FileExtension }, Pic4.InputStream, name + "4" + FileExtension, Middle, Small, false)[0];
			}

			if (Pic5 != null)
			{
				FileExtension = System.IO.Path.GetExtension(Pic5.FileName);

				//先刪除舊圖片
				BlobHelper.delete(sContainer + "/" + item.Id, Path.GetFileName(item.Pic5));

				//上傳大頭貼
				item.Pic5 = BlobHelper.Upload_Blob(new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + "5" + FileExtension }, Pic5.InputStream, name + "5" + FileExtension, Middle, Small, false)[0];
			}

			#endregion
			item.LastUserId = Method.Get_UserId_Admin(Request.Cookies, Session);

			if (data.Update(item) <= 0)
			{
				ErrorRecordModel.Error_Record(Method.Get_UserId_Admin(Request.Cookies, Session), "FashionArticle_5", Request.UserAgent, true, "資料新增失敗");
				TempData["err"] = "FashionArticle_5";
				return RedirectToAction("Index");
			}

			db.Connection.Close();
			TempData["FromAction"] = "儲存成功！";
			if (!string.IsNullOrWhiteSpace(url))
			{
				return Redirect(url);
			}
			else
			{
				return RedirectToAction("Index");
			}
		}

		#endregion

		#region Delete
		public void Delete(int id = 0)
		{
			MithDataContext db = new MithDataContext();
			try
			{
				//文章
				var Article = db.Article.FirstOrDefault(w => w.Id == id);
				//文章留言
				var ArticleMessage = db.ArticleMessage.Where(w => w.ArticleId == id);
				//文章按讚
				var LikeArticle = db.LikeArticle.Where(w => w.ArticleId == id);

				if (Article != null)
				{
					//先刪文章留言
					if (ArticleMessage != null)
					{
						db.ArticleMessage.DeleteAllOnSubmit(ArticleMessage);
						db.SubmitChanges();
					}

					//刪文章按讚
					if (LikeArticle != null)
					{
						db.LikeArticle.DeleteAllOnSubmit(LikeArticle);
						db.SubmitChanges();
					}

					//刪文章
					db.Article.DeleteOnSubmit(Article);
					db.SubmitChanges();
				}
			}
			catch (Exception e)
			{
			}
			finally
			{
				db.Connection.Close();
			}
		}

		#endregion

		#region Ajax

		[HttpPost]
		public ActionResult Ajax_OnTop(string chk_Article, bool OnTop)
		{
			MithDataContext db = new MithDataContext();
			FashionArticleModel data = new FashionArticleModel();
			try
			{
				var data2 = chk_Article.Split(",");


				foreach (var item in data2)
				{
					data.Ajax_UpdateOnTop(int.Parse(item), OnTop);
				}
			}
			catch
			{
				return Content("Failed");
			}
			finally
			{
				db.Connection.Close();
			}
			return Content("Success");
		}

		#endregion

		public JsonResult Get_GoodsAutocomplete(string search)
		{
			MithDataContext db = new MithDataContext();
			var data2 = db.Goods.Where(w => w.Status == 2 && w.SN.Contains(search)).ToList();
			List<string> data = new List<string>();
			foreach (var item in data2)
			{
				data.Add(item.SN);

			}
			var result = data.ToList();
			//var result = data.Where(x => x.ToLower().StartsWith(search.ToLower())).ToList();

			return Json(result, JsonRequestBehavior.AllowGet);
		}
	}
}

