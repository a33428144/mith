﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using System.Web.UI.DataVisualization.Charting;
using System.Text;
using System.IO;
using System.Drawing;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Mith.Models;
using Mith.Areas.GoX.Models;

namespace Mith.Areas.GoX.Controllers
{
    public class OrderQAController : CategoryController
    {
        int fun_id = 28;

        #region 首頁

        [RoleAuthorize(Role = "亞設人員")]
        [MyAuthorize(Com = Competence.Read, function = "訂單Q&A")]
        public ActionResult Index_Mith(string keyword = "", DateTime? CreateTime_St = null, DateTime? CreateTime_End = null, int Search_Reply = 0, int Search_OrderId = 0, string OrderSN = "", int p = 1, int show_number = 10)
        {
            MithDataContext db = new MithDataContext();
            OrderQAModel data = new OrderQAModel();
            var fun = FunctionModel.Get_One(fun_id);

            string get = "";

            if (keyword != "")
            {
                data.Keyword = keyword;
                ViewData["keyword"] = keyword;
                get += Method.Get_URLGet("keyword", keyword);
            }

            if (Search_Reply != 0)
            {
                data.Search_Reply = Search_Reply;
                ViewData["Search_Reply"] = Search_Reply;
                get += Method.Get_URLGet("Search_Reply", Search_Reply.ToString());
            }

            if (Search_OrderId != 0)
            {
                data.Search_OrderId = Search_OrderId;
                ViewData["Search_OrderId"] = Search_OrderId;
                get += Method.Get_URLGet("Search_OrderId", Search_OrderId.ToString());
                ViewData["OrderSN"] = OrderSN;
            }

            if (CreateTime_St != null)
            {
                data.CreateTime_St = CreateTime_St;
                ViewData["CreateTime_St"] = CreateTime_St;
                get += Method.Get_URLGet("CreateTime_St", CreateTime_St.Value.ToString("yyyy/MM/dd HH:mm:ss"));
            }
            if (CreateTime_End != null)
            {
                data.CreateTime_End = CreateTime_End;
                ViewData["CreateTime_End"] = CreateTime_End;
                get += Method.Get_URLGet("CreateTime_End", CreateTime_End.Value.ToString("yyyy/MM/dd HH:mm:ss"));
            }
            ViewBag.Title = fun.Name;
            ViewBag.Title_link = "訂單管理 > <a href=\"\">" + fun.Name + "</a>";

            ViewData["p"] = p;
            ViewData["page"] = data.Get_Page(p, show_number, 0);
            ViewData["number"] = show_number;
            ViewData["get"] = get;
            Session["Url"] = Request.Url.AbsoluteUri;
            db.Connection.Close();
            return View(data.Get_Data(p, show_number));
        }


        [MyAuthorize(Com = Competence.Read, function = "訂單Q&A")]
        public ActionResult Index_Company(string keyword = "", DateTime? CreateTime_St = null, DateTime? CreateTime_End = null, int Search_GoodsId = 0, int Search_Reply = 0, int p = 1, int show_number = 10)
        {
            MithDataContext db = new MithDataContext();
            OrderQAModel data = new OrderQAModel();
            var fun = FunctionModel.Get_One(fun_id);
            string get = "";

            if (keyword != "")
            {
                data.Keyword = keyword;
                ViewData["keyword"] = keyword;
                get += Method.Get_URLGet("keyword", keyword);
            }
            if (Search_Reply != 0)
            {
                data.Search_Reply = Search_Reply;
                ViewData["Search_Reply"] = Search_Reply;
                get += Method.Get_URLGet("Search_Reply", Search_Reply.ToString());
            }

            if (CreateTime_St != null)
            {
                data.CreateTime_St = CreateTime_St;
                ViewData["CreateTime_St"] = CreateTime_St;
                get += Method.Get_URLGet("CreateTime_St", CreateTime_St.Value.ToString("yyyy/MM/dd HH:mm:ss"));
            }
            if (CreateTime_End != null)
            {
                data.CreateTime_End = CreateTime_End;
                ViewData["CreateTime_End"] = CreateTime_End;
                get += Method.Get_URLGet("CreateTime_End", CreateTime_End.Value.ToString("yyyy/MM/dd HH:mm:ss"));
            }
            ViewBag.Title = fun.Name;
            ViewBag.Title_link = "訂單管理 > <a href=\"\">" + fun.Name + "</a>";
            data.Search_CompanyId = Mith.Models.Method.Get_CompanyId_Admin(Request.Cookies, Session);
            ViewData["p"] = p;
            ViewData["page"] = data.Get_Page(p, show_number);
            ViewData["number"] = show_number;
            ViewData["get"] = get;
            Session["Url"] = Request.Url.AbsoluteUri;
            db.Connection.Close();
            return View(data.Get_Data(p, show_number));
        }
        
        #endregion

        #region 編輯

        [RoleAuthorize(Role = "亞設人員")]
        [MyAuthorize(Com = Competence.Update, function = "訂單Q&A")]
        public ActionResult Edit_Mith(int id = 0)
        {
            OrderQAModel data = new OrderQAModel();
            var fun = FunctionModel.Get_One(fun_id);

            ViewBag.Title = "訂單管理 > " + fun.Name + " > 詳情";
            ViewBag.Title_link = "訂單管理 > <a href=\"\">" + fun.Name + "</a> > 詳情 ";
            ViewBag.c_title = fun.Name;

            var item = data.Get_One(id);
            if (item == null)
            {
                return RedirectToAction("Index_Mith");
            }
            return View(item);
        }

        [RoleAuthorize(Role = "廠商")]
        [MyAuthorize(Com = Competence.Update, function = "訂單Q&A")]
        public ActionResult Edit_Company(int id = 0)
        {
            MithDataContext db = new MithDataContext();
            OrderQAModel data = new OrderQAModel();
            var fun = FunctionModel.Get_One(fun_id);

            ViewBag.Title = "訂單管理 > " + fun.Name + " > 編輯";
            ViewBag.Title_link = "訂單管理 > <a href=\"\">" + fun.Name + "</a> > 編輯 ";
            ViewBag.c_title = fun.Name;

            var item = data.Get_One(id);
            if (item == null)
            {
                return RedirectToAction("Index_Company");
            }
            db.Connection.Close();
            return View(item);
        }

        [MyAuthorize(Com = Competence.Update, function = "訂單Q&A")]
        [ValidateInput(true)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Edit(OrderQAModel.OrderQAShow item)
        {
            OrderQAModel data = new OrderQAModel();
            var fun = FunctionModel.Get_One(fun_id);
            string url = (string)Session["Url"];
            int Role = Mith.Models.Method.Get_UserRole_Admin(Request.Cookies, Session);

            item.ReplyUserId = Method.Get_UserId_Admin(Request.Cookies, Session);

            if (data.Update(item) <= 0)
            {
                ErrorRecordModel.Error_Record(Method.Get_UserId_Admin(Request.Cookies, Session), "Goods_5", Request.UserAgent, true, "資料新增失敗");
                TempData["err"] = "Goods_5";
                if(!string.IsNullOrWhiteSpace(url))
                {
                    return Redirect(url);
                }
                else
                {
                    return RedirectToAction("Index_Company");
                }
            }


            TempData["FromAction"] = "儲存成功！";
            if(!string.IsNullOrWhiteSpace(url))
            {
                return Redirect(url);
            }
            else
            {
                if (Role == 1)
                {
                    return RedirectToAction("Index_Mith");
                }
                else
                {
                    return RedirectToAction("Index_Company");
                }
            }
        }

        #endregion

        #region 刪除

        [MyAuthorize(Com = Competence.Delete, function = "訂單Q&A")]
        public ActionResult Delete(int item, int p = 1)
        {
            int Role = Mith.Models.Method.Get_UserRole_Admin(Request.Cookies, Session);
            OrderQAModel data = new OrderQAModel();
            data.Delete(item);
            TempData["FromAction"] = "刪除成功！";
            if (Role == 1)
            {
                return RedirectToAction("Index_Mith", new { p = p });
            }
            else
            {
                return RedirectToAction("Index_Company", new { p = p });
            }

        }

        //[MyAuthorize(Com = Competence.Delete, function = "訂單Q&A")]
        //[HttpPost]
        //public ActionResult Delete(int[] item)
        //{
        //    if (!CheckLogin.CheckPermission(HttpContext, Competence.Delete, fun_id))
        //    {
        //        TempData["err"] = Method.Error_Permission;
        //        return RedirectToAction(Method.GoX_Default_Action, Method.GoX_Default_Controller, new { ReturnUrl = Request.RawUrl });
        //    }

        //    OrderQAModel data = new OrderQAModel();
        //    data.Delete(item);

        //    return RedirectToAction("Index");
        //}

        #endregion

    }
}

