﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mith.Areas.GoX.Models;
using WebMatrix.WebData;
using System.Web.Security;
using System.Web.Mvc.Html;
using System.Text.RegularExpressions;
using Mith.Models;

namespace Mith.Areas.GoX.Controllers
{
	public class UserProfileController : Controller
	{
		UserProfileModel data = new UserProfileModel();
		int fun_id = 2;

		[RoleAuthorize(Role = "亞設人員")]
		[MyAuthorize(Com = Competence.Read, function = "帳號管理")]
		public ActionResult Index_Mith(int Search_Role = 0, int Search_Level = 0, int Search_Enable = 0, string keyword = "", int Search_CompanyId = 0, DateTime? CreateTime_St = null, DateTime? CreateTime_End = null, int p = 1, int show_number = 10)
		{
			var fun = FunctionModel.Get_One(fun_id);

			ViewBag.Title = fun.Name;
			ViewBag.Title_link = "<a href=\"\">" + fun.Name + "</a>";

			string get = "";
			if (keyword != "")
			{
				data.Keyword = keyword;
				ViewData["keyword"] = keyword;
				get += Method.Get_URLGet("keyword", keyword);
			}
			if (Search_Role != 0)
			{

				data.Search_Role = Search_Role;
				ViewData["Search_Role"] = Search_Role;
				get += Method.Get_URLGet("Search_Role", Search_Role.ToString());
			}
			if (Search_Level != 0)
			{

				data.Search_Level = Search_Level;
				ViewData["Search_Level"] = Search_Level;
				get += Method.Get_URLGet("Search_Level", Search_Level.ToString());
			}
			if (Search_CompanyId != 0)
			{
				data.Search_CompanyId = Search_CompanyId;
				ViewData["Search_CompanyId"] = Search_CompanyId;
				get += Method.Get_URLGet("Search_CompanyId", Search_CompanyId.ToString());
			}
			if (Search_Enable != 0)
			{

				data.Search_Enable = Search_Enable;
				ViewData["Search_Enable"] = Search_Enable;
				get += Method.Get_URLGet("Search_Enable", Search_Enable.ToString());
			}
			if (CreateTime_St != null)
			{
				data.CreateTime_St = CreateTime_St;
				ViewData["CreateTime_St"] = CreateTime_St;
				get += Method.Get_URLGet("CreateTime_St", CreateTime_St.Value.ToString("yyyy/MM/dd HH:mm:ss"));
			}
			if (CreateTime_End != null)
			{
				data.CreateTime_End = CreateTime_End;
				ViewData["CreateTime_End"] = CreateTime_End;
				get += Method.Get_URLGet("CreateTime_End", CreateTime_End.Value.ToString("yyyy/MM/dd HH:mm:ss"));
			}

			int CompanyId = Mith.Models.Method.Get_CompanyId_Admin(Request.Cookies, Session);

			ViewData["p"] = p;
			ViewData["page"] = data.Get_Page(p, show_number);
			ViewData["number"] = show_number;
			ViewData["get"] = get;
			Session["Url"] = Request.Url.AbsoluteUri;

			return View(data.Get_Data(p, show_number));

		}

		[RoleAuthorize(Role = "廠商")]
		[MyAuthorize(Com = Competence.Read, function = "帳號管理")]
		public ActionResult Index_Company(int Search_Level = 0, int Search_Enable = 0, string keyword = "", DateTime? CreateTime_St = null, DateTime? CreateTime_End = null, int p = 1, int show_number = 10)
		{
			var fun = FunctionModel.Get_One(fun_id);

			ViewBag.Title = fun.Name;
			ViewBag.Title_link = "<a href=\"\">" + fun.Name + "</a>";

			string get = "";
			if (keyword != "")
			{
				data.Keyword = keyword;
				ViewData["keyword"] = keyword;
				get += Method.Get_URLGet("keyword", keyword);
			}
			if (Search_Level != 0)
			{

				data.Search_Level = Search_Level;
				ViewData["Search_Level"] = Search_Level;
				get += Method.Get_URLGet("Search_Level", Search_Level.ToString());
			}
			if (Search_Enable != 0)
			{

				data.Search_Enable = Search_Enable;
				ViewData["Search_Enable"] = Search_Enable;
				get += Method.Get_URLGet("Search_Enable", Search_Enable.ToString());
			}
			if (CreateTime_St != null)
			{
				data.CreateTime_St = CreateTime_St;
				ViewData["CreateTime_St"] = CreateTime_St;
				get += Method.Get_URLGet("CreateTime_St", CreateTime_St.Value.ToString("yyyy/MM/dd HH:mm:ss"));
			}
			if (CreateTime_End != null)
			{
				data.CreateTime_End = CreateTime_End;
				ViewData["CreateTime_End"] = CreateTime_End;
				get += Method.Get_URLGet("CreateTime_End", CreateTime_End.Value.ToString("yyyy/MM/dd HH:mm:ss"));
			}

			data.Search_CompanyId = Mith.Models.Method.Get_CompanyId_Admin(Request.Cookies, Session);
			ViewData["CompanyId"] = data.Search_CompanyId;
			ViewData["p"] = p;
			ViewData["page"] = data.Get_Page(p, show_number);
			ViewData["number"] = show_number;
			ViewData["get"] = get;
			Session["Url"] = Request.Url.AbsoluteUri;

			return View(data.Get_Data(p, show_number));

		}

		[RoleAuthorize(Role = "亞設人員")]
		[MyAuthorize(Com = Competence.Insert, function = "帳號管理")]
		public ActionResult Create_Mith()
		{
			var fun = FunctionModel.Get_One(fun_id);
			ViewBag.Title = fun.Name + " > 新增";
			ViewBag.Title_link = "<a href=\"Index_Mith\">" + fun.Name + "</a> > 新增";
			ViewBag.c_title = fun.Name + "資料";

			return View();
		}

		[RoleAuthorize(Role = "廠商")]
		[MyAuthorize(Com = Competence.Insert, function = "帳號管理")]
		public ActionResult Create_Company()
		{
			var fun = FunctionModel.Get_One(fun_id);
			ViewBag.Title = fun.Name + " > 新增";
			ViewBag.Title_link = "<a href=\"Index_Company\">" + fun.Name + "</a> > 新增";
			ViewBag.c_title = fun.Name + "資料";

			return View();
		}

		[HttpPost]
		public ActionResult Create(UserProfileModel.UserProfileShow item)
		{

			if (Regex.IsMatch(item.Account, @"[\W_]+"))
			{
				ErrorRecordModel.Error_Record(Method.Get_UserId_Admin(Request.Cookies, Session), "UserProfile_17", Request.UserAgent, true, "帳號使用特殊字元");
				TempData["err"] = "帳號請勿使用特殊字元或空格";
			}
			else
			{
				if (data.Insert(item) <= 0)
				{
					ErrorRecordModel.Error_Record(Method.Get_UserId_Admin(Request.Cookies, Session), "UserProfile_1", Request.UserAgent, true, "新增失敗，帳號重複");
					TempData["err"] = "UserProfile_1，新增失敗(帳號重複)";
				}
			}

			TempData["FromAction"] = "新增成功！";

			int Role = Mith.Models.Method.Get_UserRole_Admin(Request.Cookies, Session);
			if (Role == 1)
			{
				return RedirectToAction("Index_Mith");
			}
			else
			{
				return RedirectToAction("Index_Company");
			}
		}

		[RoleAuthorize(Role = "亞設人員")]
		[MyAuthorize(Com = Competence.Update, function = "帳號管理")]
		public ActionResult Edit_Mith(int id = 0, int p = 1)
		{
			var fun = FunctionModel.Get_One(fun_id);

			ViewBag.Title = fun.Name + " > 編輯";
			ViewBag.Title_link = "<a href=\"../Index_Mith\">" + fun.Name + "</a> > 編輯";
			ViewBag.c_title = fun.Name + "資料";
			ViewData["p"] = p;
			var item = data.Get_One(id);
			return View(item);
		}

		[RoleAuthorize(Role = "廠商")]
		[MyAuthorize(Com = Competence.Update, function = "帳號管理")]
		public ActionResult Edit_Company(int id = 0, int p = 1)
		{
			var fun = FunctionModel.Get_One(fun_id);

			ViewBag.Title = fun.Name + " > 編輯";
			ViewBag.Title_link = "<a href=\"../Index_Company\">" + fun.Name + "</a> > 編輯";
			ViewBag.c_title = fun.Name + "資料";
			ViewData["p"] = p;
			var item = data.Get_One(id);
			return View(item);
		}

		[HttpPost]
		public ActionResult Edit(UserProfileModel.UserProfileShowEdit item, int p = 1)
		{
			string url = (string)Session["Url"];
			int Role = Mith.Models.Method.Get_UserRole_Admin(Request.Cookies, Session);

			if (data.Update(item) <= 0)
			{
				ErrorRecordModel.Error_Record(Method.Get_UserId_Admin(Request.Cookies, Session), "UserProfile_5", Request.UserAgent, true, "資料更新失敗");
				TempData["err"] = "UserProfile_5";

				if (Role == 1)
				{
					return RedirectToAction("Index_Mith");
				}
				else
				{
					return RedirectToAction("Index_Company");
				}
			}

			TempData["FromAction"] = "儲存成功！";
			if (!string.IsNullOrWhiteSpace(url))
			{
				return Redirect(url);
			}
			else
			{

				if (Role == 1)
				{
					return RedirectToAction("Index_Mith");
				}
				else
				{
					return RedirectToAction("Index_Company");
				}
			}
		}

		[MyAuthorize(Com = Competence.Delete, function = "帳號管理")]
		public ActionResult Delete(int item, int p = 1)
		{
			data.Delete(item);
			int Role = Mith.Models.Method.Get_UserRole_Admin(Request.Cookies, Session);
			TempData["FromAction"] = "刪除成功！";
			ViewData["p"] = p;
			if (Role == 1)
			{
				return RedirectToAction("Index_Mith");
			}
			else
			{
				return RedirectToAction("Index_Company");
			}
		}

		[MyAuthorize(Com = Competence.Delete, function = "帳號管理")]
		[HttpPost]
		public ActionResult Delete(int[] item)
		{
			data.Delete(item);

			int Role = Mith.Models.Method.Get_UserRole_Admin(Request.Cookies, Session);

			if (Role == 1)
			{
				return RedirectToAction("Index_Mith");
			}
			else
			{
				return RedirectToAction("Index_Company");
			}
		}

		[HttpPost]
		public ActionResult Ajax_UserLevel(int role)
		{
			return Content(UserProfileModel.Ajax_UserLevel_Html(role, 0));
		}
		public ActionResult Truncate()
		{
			data.Truncate_LoginRecord();

			return RedirectToAction("Index");
		}

		#region 登入&登出相關
		[AllowAnonymous]
		public ActionResult Login(string ReturnUrl)
		{
			ViewBag.Title = "登入";
			ViewBag.Title_link = "<a href=\"Login\">登入</a>";
			ViewBag.ReturnUrl = ReturnUrl;
			if (Method.Is_Login_Admin(Request.Cookies))
			{
				if (ReturnUrl == "")
				{
					ReturnUrl = Method.RootPath + "/GoX/";
					ViewBag.ReturnUrl = ReturnUrl;
				}
				if (TempData["err"] != null)
				{
					return View();
				}
				return RedirectToLocal(ReturnUrl);
			}
			if (HttpContext.Request.Cookies["Cookie_Accout"] != null)
			{
				// if the mvcvalue exists as a cookie, use the Cookies to get its value
				string Cookie_Accout = HttpContext.Request.Cookies["Cookie_Accout"].Value;
				ViewData["Cookie_Accout"] = Cookie_Accout;
			}
			return View(new UserProfileModel.LoginModel());
		}

		[HttpPost]
		[AllowAnonymous]
		[ValidateAntiForgeryToken]
		public ActionResult Login(UserProfileModel.LoginModel login, string ReturnUrl)
		{
			if (ModelState.IsValid)
			{
				LoginRecordModel lrm = new LoginRecordModel();
				lrm.create_time_start = DateTime.UtcNow.AddHours(-1);
				lrm.create_time_end = DateTime.UtcNow;
				lrm.Enable_Login = true;
				lrm.Login = false;
				lrm.Keyword = Request.UserHostAddress;

				UserProfileModel.UserProfileShow account = data.Get_LoginData(login.UserName, login.Password);

				if (account == null)
				{
					LoginRecordModel.Login_Record(login.UserName, Request, false);
					TempData["err"] = "帳號或密碼不正確";
					ErrorRecordModel.Error_Record(Method.Get_UserId_Admin(Request.Cookies, Session), "UserProfile_18", Request.UserAgent, true, "帳號或密碼不正確");
					return View();
				}

				//先登出
				SignOut();
				HttpCookie cookie = new HttpCookie(Method.CookieName_Admin);
				cookie.Path = "/";
				cookie.HttpOnly = true;
				cookie.Expires = DateTime.UtcNow.AddHours(8).AddDays(1);
				cookie.Value = Method.AES(true, account.Account + "," + account.Name + "," + account.Password, Method.AES_Key);
				Response.Cookies.Add(cookie);

				Session.Add(Method.SessionUserId_Admin, account.Id);
				//Session.Add(Method.SessionUserAccount_Admin, account.Account);
				Session.Add(Method.SessionUserName_Admin, account.Name);
				Session.Add(Method.SessionLevel_Admin, account.LevelId);
				Session.Add(Method.SessionLevelName_Admin, account.Level_Name);

				UserProfileModel.Update_LoginTime(account.Id);
				LoginRecordModel.Login_Record(login.UserName, Request, true);

				HttpCookie Cookie_Accout = new HttpCookie("Cookie_Accout", account.Account);
				// set the cookie's expiration date
				Cookie_Accout.Expires = DateTime.Now.AddYears(1);
				// set the cookie on client's browser
				HttpContext.Response.Cookies.Add(Cookie_Accout);

				return RedirectToAction("Index", "Admin");
			}
			if (login.UserName != null)
			{
				LoginRecordModel.Login_Record(login.UserName, Request, false);
			}
			return View();
		}

		public ActionResult Off()//string logoff = "")
		{
			SignOut();//logoff);
			return RedirectToAction("Index", "Admin");
		}

		private void SignOut()//string debug = "")
		{
			//FormsAuthentication.SignOut();
			if (Request.Cookies[Method.CookieName_Admin] != null)
			{
				Response.Cache.SetCacheability(HttpCacheability.NoCache);

				HttpCookie cookie = new HttpCookie(Method.CookieName_Admin, "");
				cookie.HttpOnly = true;
				cookie.Expires = DateTime.Now.AddYears(-1);
				Response.SetCookie(cookie);
			}
			Session.RemoveAll();

		}

		private ActionResult RedirectToLocal(string ReturnUrl)
		{
			if (Url.IsLocalUrl(ReturnUrl))
			{
				return Redirect(ReturnUrl);
			}
			else
			{
				return RedirectToAction("Index", "Admin");
			}
		}
		#endregion

		[HttpPost]
		public ActionResult Check_OldPassword(string OldPassword = "")
		{
			MithDataContext db = new MithDataContext();
			OldPassword = Method.Get_HashPassword(OldPassword);
			var data = db.UserProfile.FirstOrDefault(w => w.Password == OldPassword);
			if (data != null)
			{
				return Content("True");
			}
			else
			{
				return Content("False");
			}
		}

	}
}