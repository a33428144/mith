﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;
using System.Text;
using System.IO;
using Mith.Models;
using Mith.Areas.GoX.Models;

namespace Mith.Areas.GoX.Controllers
{
    public class CompanyController : CategoryController
    {
        int fun_id = 5;

        #region 首頁
        [RoleAuthorize(Role = "亞設人員")]
        [MyAuthorize(Com = Competence.Read, function = "廠商資料")]
        public ActionResult Index(string keyword = "", int Search_Collaborate = 0, int Search_Type = -1, int Search_GoodsKind = 0, DateTime? CreateTime_St = null, DateTime? CreateTime_End = null, int p = 1, int show_number = 10)
        {
            CompanyModel data = new CompanyModel();
            var fun = FunctionModel.Get_One(fun_id);
            string get = "";
            if (keyword != "")
            {
                data.Keyword = keyword;
                ViewData["keyword"] = keyword;
                get += Method.Get_URLGet("keyword", keyword);
            }
            if (Search_Collaborate != 0)
            {
                data.Search_Collaborate = Search_Collaborate;
                ViewData["Search_Collaborate"] = Search_Collaborate;
                get += Method.Get_URLGet("Search_Collaborate", Search_Collaborate.ToString());
            }
            if (Search_Type != -1)
            {
                data.Search_Type = Search_Type;
                ViewData["Search_Type"] = Search_Type;
                get += Method.Get_URLGet("Search_Type", Search_Type.ToString());
            }

            if (Search_GoodsKind != 0)
            {
                data.Search_GoodsKind = Search_GoodsKind;
                ViewData["Search_GoodsKind"] = Search_GoodsKind;
                get += Method.Get_URLGet("Search_GoodsKind", Search_GoodsKind.ToString());
            }
            if (CreateTime_St != null)
            {
                data.CreateTime_St = CreateTime_St;
                ViewData["CreateTime_St"] = CreateTime_St;
                get += Method.Get_URLGet("CreateTime_St", CreateTime_St.Value.ToString("yyyy/MM/dd HH:mm:ss"));
            }
            if (CreateTime_End != null)
            {
                data.CreateTime_End = CreateTime_End;
                ViewData["CreateTime_End"] = CreateTime_End;
                get += Method.Get_URLGet("CreateTime_End", CreateTime_End.Value.ToString("yyyy/MM/dd HH:mm:ss"));
            }

            ViewBag.Title = fun.Name;
            ViewBag.Title_link = "廠商管理 > <a href=\"\">" + fun.Name + "</a>";

            ViewData["p"] = p;
            ViewData["page"] = data.Get_Page(p, show_number);
            ViewData["number"] = show_number;
            ViewData["get"] = get;
            Session["Url"] = Request.Url.AbsoluteUri;
            Session["item"] = null;
            return View(data.Get_Data(p, show_number));
        }

        #endregion

        #region 新增

        [MyAuthorize(Com = Competence.Insert, function = "廠商資料")]
        public ActionResult Create_Step1()
        {
            MithDataContext db = new MithDataContext();
            CompanyModel data = new CompanyModel();
            var fun = FunctionModel.Get_One(fun_id);

            ViewBag.Title = "廠商管理 > " + fun.Name + " > 新增 (基本資訊)";
            ViewBag.Title_link = "廠商管理 > <a href=/GoX/Company/Index>" + fun.Name + "</a> > 新增 (基本資訊)";
            ViewBag.c_title = fun.Name;
            db.Connection.Close();
            return View();
        }

        [ValidateInput(true)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Create_Step1(CompanyModel.CompanyShow item)
        {

            MithDataContext db = new MithDataContext();
            CompanyModel data = new CompanyModel();
            var fun = FunctionModel.Get_One(fun_id);

            item.Id = db.Company.OrderByDescending(o => o.Id).FirstOrDefault() != null ? db.Company.OrderByDescending(o => o.Id).FirstOrDefault().Id + 1 : 1;
            db.Connection.Close();
            Session["item"] = item;
            return RedirectToAction("Create_Step2");
        }

        [MyAuthorize(Com = Competence.Insert, function = "廠商資料")]
        public ActionResult Create_Step2(CompanyModel.CompanyShow item)
        {
            MithDataContext db = new MithDataContext();
            CompanyModel data = new CompanyModel();
            var fun = FunctionModel.Get_One(fun_id);
            item = Session["item"] as CompanyModel.CompanyShow;
            if (item == null)
            {
                return RedirectToAction("Create_Step1");
            }
            ViewBag.Title = "廠商管理 > " + fun.Name + " >  新增 (合作類別 / 狀態)";
            ViewBag.Title_link = "廠商管理 > <a href=/GoX/Company/Index>" + fun.Name + "</a> >  新增 (合作類別 / 狀態)";
            ViewBag.c_title = fun.Name;

            db.Connection.Close();
            return View(item);
        }

        [ValidateInput(true)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Create_Step2(CompanyModel.CompanyShow item, string post = "")
        {

            MithDataContext db = new MithDataContext();
            CompanyModel data = new CompanyModel();
            var fun = FunctionModel.Get_One(fun_id);

            Session["item"] = item;
            return RedirectToAction("Create_Step3");
        }


        [MyAuthorize(Com = Competence.Insert, function = "廠商資料")]
        public ActionResult Create_Step3(CompanyModel.CompanyShow item)
        {
            MithDataContext db = new MithDataContext();
            CompanyModel data = new CompanyModel();
            var fun = FunctionModel.Get_One(fun_id);
            item = Session["item"] as CompanyModel.CompanyShow;
            if (item == null)
            {
                return RedirectToAction("Create_Step1");
            }
            ViewBag.Title = "廠商管理 > " + fun.Name + " >  新增 (帳務資料)";
            ViewBag.Title_link = "廠商管理 > <a href=/GoX/Company/Index>" + fun.Name + "</a> >  新增 (帳務資料)";
            ViewBag.c_title = fun.Name;

            db.Connection.Close();
            return View(item);
        }

        [ValidateInput(true)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Create_Step3(CompanyModel.CompanyShow item, string IsPost = "")
        {

            MithDataContext db = new MithDataContext();
            CompanyModel data = new CompanyModel();
            var fun = FunctionModel.Get_One(fun_id);

            if (ModelState.IsValid)
            {
                if (data.Insert(item) <= 0)
                {
                    ErrorRecordModel.Error_Record(Method.Get_UserId_Admin(Request.Cookies, Session), "Company_2", Request.UserAgent, true, "資料新增失敗");
                    TempData["err"] = "Company_2，資料新增失敗";
                }
            }
            else
            {
                ErrorRecordModel.Error_Record(Method.Get_UserId_Admin(Request.Cookies, Session), "Company_3", Request.UserAgent, true, "資料新增失敗");
                TempData["err"] = "Company_3，資料新增失敗";
            }
            db.Connection.Close();
            TempData["FromAction"] = "新增成功！";
            Session["item"] = null;
            return RedirectToAction("Index");
        }

        #endregion

        #region 編輯

        [MyAuthorize(Com = Competence.Update, function = "廠商資料")]
        public ActionResult Edit_Step1(int id = 0)
        {
            MithDataContext db = new MithDataContext();
            CompanyModel data = new CompanyModel();
            var fun = FunctionModel.Get_One(fun_id);

            ViewBag.Title = "廠商管理 > " + fun.Name + " > 編輯 (基本資訊)";
            ViewBag.Title_link = "廠商管理 > <a href=/GoX/Company/Index>" + fun.Name + "</a> > 編輯 (基本資訊)";
            ViewBag.c_title = fun.Name;

            var item = data.Get_One_Step1(id);
            if (item == null)
            {
                return RedirectToAction("Create_Step1");
            }
            db.Connection.Close();
            return View(item);
        }

        [MyAuthorize(Com = Competence.Update, function = "廠商資料")]
        [ValidateInput(true)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Edit_Step1(CompanyModel.CompanyShow item)
        {
            MithDataContext db = new MithDataContext();
            CompanyModel data = new CompanyModel();
            string url = (string)Session["Url"];

            int user_id = Method.Get_UserId_Admin(Request.Cookies, Session);
            item.LastUserId = user_id;

            if (data.Update_Step1(item) <= 0)
            {
                ErrorRecordModel.Error_Record(Method.Get_UserId_Admin(Request.Cookies, Session), "Company_5", Request.UserAgent, true, "資料新增失敗");
                TempData["err"] = "Company_5";
                return RedirectToAction("Index");
            }
            db.Connection.Close();
            TempData["FromAction"] = "儲存成功！";
            if(!string.IsNullOrWhiteSpace(url))
            {
                return Redirect(url);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        [MyAuthorize(Com = Competence.Update, function = "廠商資料")]
        public ActionResult Edit_Step2(int id = 0)
        {
            MithDataContext db = new MithDataContext();
            CompanyModel data = new CompanyModel();
            var fun = FunctionModel.Get_One(fun_id);

            ViewBag.Title = "廠商管理 > " + fun.Name + " >  編輯 (合作類別 / 狀態)";
            ViewBag.Title_link = "廠商管理 > <a href=/GoX/Company/Index>" + fun.Name + "</a> >  編輯 (合作類別 / 狀態)";
            ViewBag.c_title = fun.Name;

            var item = data.Get_One_Step2(id);
            if (item == null)
            {
                return RedirectToAction("Create_Step1");
            }
            db.Connection.Close();
            return View(item);
        }

        [MyAuthorize(Com = Competence.Update, function = "廠商資料")]
        [ValidateInput(true)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Edit_Step2(CompanyModel.CompanyShow item)
        {
            MithDataContext db = new MithDataContext();
            CompanyModel data = new CompanyModel();
            string url = (string)Session["Url"];

            int user_id = Method.Get_UserId_Admin(Request.Cookies, Session);
            item.LastUserId = user_id;

            if (data.Update_Step2(item) <= 0)
            {
                ErrorRecordModel.Error_Record(Method.Get_UserId_Admin(Request.Cookies, Session), "Company_5", Request.UserAgent, true, "資料新增失敗");
                TempData["err"] = "Company_5";
                return RedirectToAction("Index");
            }

            db.Connection.Close();
            TempData["FromAction"] = "儲存成功！";
            if(!string.IsNullOrWhiteSpace(url))
            {
                return Redirect(url);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        [MyAuthorize(Com = Competence.Update, function = "廠商資料")]
        public ActionResult Edit_Step3(int id = 0)
        {
            MithDataContext db = new MithDataContext();
            CompanyModel data = new CompanyModel();
            var fun = FunctionModel.Get_One(fun_id);

            ViewBag.Title = "廠商管理 > " + fun.Name + " >  編輯 (帳務資料)";
            ViewBag.Title_link = "廠商管理 > <a href=/GoX/Company/Index>" + fun.Name + "</a> >  編輯 (帳務資料)";
            ViewBag.c_title = fun.Name;

            var item = data.Get_One_Step3(id);
            if (item == null)
            {
                return RedirectToAction("Create_Step1");
            }
            db.Connection.Close();
            return View(item);
        }

        [MyAuthorize(Com = Competence.Update, function = "廠商資料")]
        [ValidateInput(true)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Edit_Step3(CompanyModel.CompanyShow item)
        {
            MithDataContext db = new MithDataContext();
            CompanyModel data = new CompanyModel();
            string url = (string)Session["Url"];

            int user_id = Method.Get_UserId_Admin(Request.Cookies, Session);
            item.LastUserId = user_id;

            if (data.Update_Step3(item) <= 0)
            {
                ErrorRecordModel.Error_Record(Method.Get_UserId_Admin(Request.Cookies, Session), "Company_5", Request.UserAgent, true, "資料新增失敗");
                TempData["err"] = "Company_5";
                return RedirectToAction("Index");
            }

            db.Connection.Close();
            TempData["FromAction"] = "儲存成功！";
            if(!string.IsNullOrWhiteSpace(url))
            {
                return Redirect(url);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        #endregion

        #region 刪除

        [MyAuthorize(Com = Competence.Delete, function = "廠商資料")]
        public ActionResult Delete(int Id, int p = 1)
        {
            CompanyModel data = new CompanyModel();
            data.Delete(Id);
            TempData["FromAction"] = "刪除成功！";
            return RedirectToAction("Index", new { p = p });
        }

        //[MyAuthorize(Com = Competence.Delete, function = "廠商資料")]
        //[HttpPost]
        //public ActionResult Delete(int[] item)
        //{
        //    CompanyModel data = new CompanyModel();
        //    data.Delete(item);

        //    return RedirectToAction("Index");
        //}

        #endregion

        [HttpPost]
        public ActionResult Export(string keyword = "", int Search_Collaborate = 0, int Search_Type = -1, int Search_GoodsKind = 0, DateTime? CreateTime_St = null, DateTime? CreateTime_End = null)
        {
            CompanyModel data = new CompanyModel();
            //---------------匯出條件搜索---------------//

            if (keyword != "")
            {
                data.Keyword = keyword;
            }
            if (Search_Collaborate != 0)
            {
                data.Search_Collaborate = Search_Collaborate;
            }
            if (Search_Type != -1)
            {
                data.Search_Type = Search_Type;
            }

            if (Search_GoodsKind != 0)
            {
                data.Search_GoodsKind = Search_GoodsKind;
            }
            if (CreateTime_St != null)
            {
                data.CreateTime_St = CreateTime_St;
            }
            if (CreateTime_End != null)
            {
                data.CreateTime_End = CreateTime_End;
            }
            //---------------匯出條件搜索end---------------//
            return File(data.Set_Excel(data.Export_Data(data)), "application/vnd.ms-excel", "廠商資料.xls");
        }

        #region Ajax

        [HttpPost]
        public ActionResult Check_SN(string SN)
        {
            MithDataContext db = new MithDataContext();
            int Count = db.Company.Where(f => f.SN == SN).Count();
            if (Count > 0)
            {
                return Content("Reply");
            }
            else
            {
                return Content("NotReply");
            }
        }
        #endregion

    }
}

