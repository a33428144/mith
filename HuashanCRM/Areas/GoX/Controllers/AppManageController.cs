﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mith.Models;
using Mith.Areas.GoX.Models;


namespace Mith.Areas.GoX.Controllers
{
    public class AppManageController : CategoryController
    {
        private const int fun_id = 4;
        PushAppModel data = new PushAppModel(fun_id);
        CategoryModel cm_data = new CategoryModel(fun_id);

        [MyAuthorize(Com = Competence.Read, function = "推播管理")]
        public ActionResult Index(int[] cid = null, string keyword = "", int enable = 0, int disable = 0,
            DateTime? create_time_start = null, DateTime? create_time_end = null,
            DateTime? update_time_start = null, DateTime? update_time_end = null, int search_time_offset = 0,
            int p = 1, int show_number = 10)
        {
            ViewBag.Title = "App管理";
            ViewBag.Title_link = "<a href=\"\">App管理</a>";
            ViewData["fun_id"] = fun_id;

            string get = Method.Get_URLGet("keyword", keyword);
            data.Keyword = keyword;
            if (cid != null)
            {
                if (cid.Any())
                {
                    data.Cid.Clear();
                    data.Cid.AddRange(cid);
                    get += Method.Get_URLGet("cid", cid);
                    ViewData["cid"] = cid;
                }
            }
            if (enable == 1 && disable == 1)
            {
                data.AllowPushEnable = false;
            }
            else
            {
                if (enable == 1)
                {
                    data.AllowPushEnable = true;
                    data.AllowPush = true;
                    get += Method.Get_URLGet("enable", enable.ToString());
                }
                if (disable == 1)
                {
                    data.AllowPushEnable = true;
                    data.AllowPush = false;
                    get += Method.Get_URLGet("disable", disable.ToString());
                }
            }
            if (create_time_start != null)
            {
                get += Method.Get_URLGet("create_time_start", create_time_start.Value.ToString("yyyy/MM/dd HH:mm:ss"));
                ViewData["create_time_start"] = create_time_start;
                data.create_time_start = create_time_start.Value.AddMinutes(search_time_offset);
            }
            if (create_time_end != null)
            {
                get += Method.Get_URLGet("create_time_end", create_time_end.Value.ToString("yyyy/MM/dd HH:mm:ss"));
                ViewData["create_time_end"] = create_time_end;
                data.create_time_end = create_time_end.Value.AddMinutes(search_time_offset);
            }
            if (update_time_start != null)
            {
                get += Method.Get_URLGet("update_time_start", update_time_start.Value.ToString("yyyy/MM/dd HH:mm:ss"));
                ViewData["update_time_start"] = update_time_start;
                data.update_time_start = update_time_start.Value.AddMinutes(search_time_offset);
            }
            if (update_time_end != null)
            {
                get += Method.Get_URLGet("update_time_end", update_time_end.Value.ToString("yyyy/MM/dd HH:mm:ss"));
                ViewData["update_time_end"] = update_time_end;
                data.update_time_end = update_time_end.Value.AddMinutes(search_time_offset);
            }

            ViewData["p"] = p;
            ViewData["page"] = data.Get_Page(p, show_number);
            ViewData["keyword"] = keyword;
            ViewData["enable"] = enable;
            ViewData["disable"] = disable;

            ViewData["get"] = get;
            return View(data.Get_Data(p, show_number));
        }


        [MyAuthorize(Com = Competence.Insert, function = "推播管理")]
        public ActionResult Create()
        {
            ViewBag.Title = "App管理 > 新增";
            ViewBag.Title_link = "<a href=\"Index\">App管理</a> > 新增";
            ViewBag.c_title = "推播管理資料";
            ViewData["fun_id"] = fun_id;
            return View();
        }

        [MyAuthorize(Com = Competence.Insert, function = "推播管理")]
        [HttpPost]
        public ActionResult Create(PushAppModel.PushAppModelShow item, HttpPostedFileBase P12_Path = null)
        {
            if (ModelState.IsValid)
            {
                int user_id = Method.Get_UserId_Admin(Request.Cookies, Session);

                item.UpdateUserId = user_id;

                string url = Method.Upload_File(P12_Path, "AppManage", Server);
                item.P12_Path = (url == "") ? null : url;

                if (data.Insert(item) <= 0)
                {
                    ErrorRecordModel.Error_Record(Method.Get_UserId_Admin(Request.Cookies, Session), "App_2", Request.UserAgent, true, "資料新增失敗");
                    TempData["err"] = "App_2，資料新增失敗";
                }
            }
            else
            {
                ErrorRecordModel.Error_Record(Method.Get_UserId_Admin(Request.Cookies, Session), "App_3", Request.UserAgent, true, "資料新增失敗");
                TempData["err"] = "App_3，資料新增失敗";
            }
            return RedirectToAction("Index");
        }

        [MyAuthorize(Com = Competence.Update, function = "推播管理")]
        public ActionResult Edit(int id = 0, int p = 1)
        {
            ViewBag.Title = "App管理 > 編輯";
            ViewBag.Title_link = "<a href=\"../Index\">App管理</a> > 編輯";
            ViewBag.c_title = "推播管理資料";
            ViewData["fun_id"] = fun_id;
            ViewData["p"] = p;
            var item = data.Get_One(id);
            if (item == null)
            {
                return RedirectToAction("Create");
            }
            return View(item);
        }

        [MyAuthorize(Com = Competence.Update, function = "推播管理")]
        [HttpPost]
        public ActionResult Edit(PushAppModel.PushAppModelShow item, HttpPostedFileBase P12_Path = null, int delete = 0, int p = 1)
        {
            if (ModelState.IsValid)
            {
                int user_id = Method.Get_UserId_Admin(Request.Cookies, Session);
                item.UpdateUserId = user_id;

                string url = Method.Upload_File(P12_Path, "AppManage", Server);
                item.P12_Path = (url == "") ? "" : url;

                if (data.Update(item, Server, delete == 1) <= 0)
                {
                    ErrorRecordModel.Error_Record(Method.Get_UserId_Admin(Request.Cookies, Session), "App_5", Request.UserAgent, true, "資料更新失敗");
                    TempData["err"] = "App_5";
                    return RedirectToAction("Index");
                }
            }

            return RedirectToAction("Index", new { p = p });
        }

        [MyAuthorize(Com = Competence.Delete, function = "推播管理")]
        public ActionResult Delete(int item, int p = 1)
        {
            data.Delete(item, Server, Method.Get_UserId_Admin(Request.Cookies, Session));

            return RedirectToAction("Index", new { p = p });
        }

        [MyAuthorize(Com = Competence.Delete, function = "推播管理")]
        [HttpPost]
        public ActionResult Delete(int[] item)
        {
            data.Delete(item, Server, Method.Get_UserId_Admin(Request.Cookies, Session));

            return RedirectToAction("Index");
        }

        [MyAuthorize(Com = Competence.Update, function = "推播管理")]
        [HttpPost]
        public ActionResult Set_Enable(int[] item, int p = 1)
        {
            int user_id = Method.Get_UserId_Admin(Request.Cookies, Session);

            data.Update_Enable(item, user_id, true);

            return RedirectToAction("Index", new { p = p });
        }

        [MyAuthorize(Com = Competence.Update, function = "推播管理")]
        [HttpPost]
        public ActionResult Set_Disable(int[] item, int p = 1)
        {
            int user_id = Method.Get_UserId_Admin(Request.Cookies, Session);
            data.Update_Enable(item, user_id, false);

            return RedirectToAction("Index", new { p = p });
        }

        [MyAuthorize(Com = Competence.Update, function = "推播管理")]
        [HttpPost]
        public ActionResult Push(int[] item, int p)
        {
            if (item != null)
            {
                foreach (var i in item)
                {
                    PushModel.Push(i, Server, null, null);
                }
            }
            return RedirectToAction("Index", new { p = p });
        }

        [MyAuthorize(Com = Competence.Update, function = "推播管理")]
        public ActionResult PushOne(int id, int p)
        {
            PushModel.Push(id, Server, null, null);
            return RedirectToAction("Index", new { p = p });
        }

        #region token
        [MyAuthorize(Com = Competence.Read, function = "推播管理")]
        public ActionResult TokenIndex(int appid = 0, string keyword = "",
            DateTime? send_time_start = null, DateTime? send_time_end = null,
            DateTime? update_time_start = null, DateTime? update_time_end = null, int search_time_offset = 0,
            int p = 1, int show_number = 10)
        {
            ViewBag.Title = "Token管理";
            ViewBag.Title_link = "<a href=\"TokenIndex\">Token管理</a>";
            PushTokenModel ptm = new PushTokenModel(fun_id);

            string get = Method.Get_URLGet("keyword", keyword);
            ptm.Keyword = keyword;
            if (appid != 0)
            {
                ptm.AppId = appid;
                get += Method.Get_URLGet("appid", appid.ToString());
                ViewData["appid"] = appid;
            }

            if (send_time_start != null)
            {
                get += Method.Get_URLGet("send_time_start", send_time_start.Value.ToString("yyyy/MM/dd HH:mm:ss"));
                ViewData["send_time_start"] = send_time_start;
                ptm.send_time_start = send_time_start.Value.AddMinutes(search_time_offset);
            }
            if (send_time_end != null)
            {
                get += Method.Get_URLGet("send_time_end", send_time_end.Value.ToString("yyyy/MM/dd HH:mm:ss"));
                ViewData["send_time_end"] = send_time_end;
                ptm.send_time_end = send_time_end.Value.AddMinutes(search_time_offset);
            }
            if (update_time_start != null)
            {
                get += Method.Get_URLGet("update_time_start", update_time_start.Value.ToString("yyyy/MM/dd HH:mm:ss"));
                ViewData["update_time_start"] = update_time_start;
                ptm.update_time_start = update_time_start.Value.AddMinutes(search_time_offset);
            }
            if (update_time_end != null)
            {
                get += Method.Get_URLGet("update_time_end", update_time_end.Value.ToString("yyyy/MM/dd HH:mm:ss"));
                ViewData["update_time_end"] = update_time_end;
                ptm.update_time_end = update_time_end.Value.AddMinutes(search_time_offset);
            }

            ViewData["p"] = p;
            ViewData["page"] = ptm.Get_Page(p, show_number);
            ViewData["keyword"] = keyword;
            ViewData["get"] = get;
            return View(ptm.Get_Data2(p, show_number));
        }

        [MyAuthorize(Com = Competence.Delete, function = "推播管理")]
        public ActionResult TokenDelete(int item, int p = 1)
        {
            PushTokenModel.Delete(item);

            return RedirectToAction("TokenIndex", new { p = p });
        }

        [MyAuthorize(Com = Competence.Delete, function = "推播管理")]
        [HttpPost]
        public ActionResult TokenDelete(int[] item)
        {
            PushTokenModel.Delete(item);

            return RedirectToAction("TokenIndex");
        }
        #endregion

        #region 訊息管理
        [MyAuthorize(Com = Competence.Read, function = "推播管理")]
        public ActionResult MessageIndex(int appid = 0, string keyword = "", int sended = 0, int send_no = 0, DateTime? create_time_start = null, DateTime? create_time_end = null, int search_time_offset = 0, int p = 1, int show_number = 10, int VolunteerId = 0)
        {
            if (!CheckLogin.CheckPermission(HttpContext, Competence.Read, fun_id))
            {
                TempData["err"] = Method.Error_Permission;
                return RedirectToAction(Method.GoX_Default_Action, Method.GoX_Default_Controller, new { ReturnUrl = Request.RawUrl });
            }

            ViewBag.Title = "推播管理";
            ViewBag.Title_link = "推播管理";
            PushMessageModel pmm = new PushMessageModel(fun_id);

            string get = Method.Get_URLGet("keyword", keyword);
            pmm.Keyword = keyword;
            if (appid != 0)
            {
                pmm.AppId = appid;
                get += Method.Get_URLGet("appid", appid.ToString());
                ViewData["appid"] = appid;
            }

            if (sended == 1 && send_no == 1)
            {
                pmm.Search_Sended = null;
            }
            else
            {
                if (sended == 1)
                {
                    pmm.Search_Sended = true;
                    get += Method.Get_URLGet("sended", sended.ToString());
                    ViewData["sended"] = sended;
                }
                if (send_no == 1)
                {
                    pmm.Search_Sended = false;
                    get += Method.Get_URLGet("send_no", send_no.ToString());
                    ViewData["send_no"] = send_no;
                }
            }

            if (create_time_start != null)
            {
                get += Method.Get_URLGet("create_time_start", create_time_start.Value.ToString("yyyy/MM/dd HH:mm:ss"));
                ViewData["create_time_start"] = create_time_start;
                pmm.create_time_start = create_time_start.Value.AddMinutes(search_time_offset);
            }
            if (create_time_end != null)
            {
                get += Method.Get_URLGet("create_time_end", create_time_end.Value.ToString("yyyy/MM/dd HH:mm:ss"));
                ViewData["create_time_end"] = create_time_end;
                pmm.create_time_end = create_time_end.Value.AddMinutes(search_time_offset);
            }
            if (VolunteerId != 0)
            {
                get += Method.Get_URLGet("VolunteerId", VolunteerId.ToString());
                ViewData["VolunteerId"] = VolunteerId;
                pmm.VolunteerId = VolunteerId;
            }

            ViewData["p"] = p;
            ViewData["page"] = pmm.Get_Page(p, show_number);
            ViewData["keyword"] = keyword;
            ViewData["get"] = get;
            Session["Url"] = Request.Url.AbsoluteUri;
            return View(pmm.Get_Data(p, show_number, VolunteerId));
        }

        [MyAuthorize(Com = Competence.Insert, function = "推播管理")]
        public ActionResult MessageCreate()
        {
            if (!CheckLogin.CheckPermission(HttpContext, Competence.Insert, fun_id))
            {
                TempData["err"] = Method.Error_Permission;
                return RedirectToAction(Method.GoX_Default_Action, Method.GoX_Default_Controller, new { ReturnUrl = Request.RawUrl });
            }
            ViewBag.Title = "推播管理 > 新增";
            ViewBag.Title_link = "<a href=\"../MessageIndex\">推播管理</a> > 新增";
            ViewBag.c_title = "推播管理";
            return View();
        }

        [HttpPost]
        public ActionResult MessageCreate(PushMessageModel.PushMessageShow item)
        {
            PushMessageModel pmm = new PushMessageModel(fun_id);
            MithDataContext db = new MithDataContext();
            int id = 0;
            if (ModelState.IsValid)
            {
                id = pmm.Insert(item);
                if (id <= 0)
                {
                    ErrorRecordModel.Error_Record(Method.Get_UserId_Admin(Request.Cookies, Session), "App_4", Request.UserAgent, true, "資料新增失敗");
                    TempData["err"] = "App_4，資料新增失敗";
                }
            }
            return RedirectToAction("MessageIndex", new { p = 1 });
        }

        [MyAuthorize(Com = Competence.Update, function = "推播管理")]
        public ActionResult MessageEdit(int id = 0, int p = 1)
        {
            if (!CheckLogin.CheckPermission(HttpContext, Competence.Update, fun_id))
            {
                TempData["err"] = Method.Error_Permission;
                return RedirectToAction(Method.GoX_Default_Action, Method.GoX_Default_Controller, new { ReturnUrl = Request.RawUrl });
            }

            ViewBag.Title = "推播管理 > 編輯";
            ViewBag.Title_link = "<a href=\"../MessageIndex\">推播管理</a> > 編輯";
            ViewBag.c_title = "推播管理";
            PushMessageModel pmm = new PushMessageModel(fun_id);
            var item = pmm.Get_One(id);
            if (item == null)
            {
                return RedirectToAction("Create");
            }

            return View(item);
        }

        [HttpPost]
        public ActionResult MessageEdit(PushMessageModel.PushMessageShow item, int p = 1)
        {
            string url = (string)Session["Url"];
            if (ModelState.IsValid)
            {
                PushMessageModel pmm = new PushMessageModel(fun_id);
                if (pmm.Update(item) <= 0)
                {
                    ErrorRecordModel.Error_Record(Method.Get_UserId_Admin(Request.Cookies, Session), "App_6", Request.UserAgent, true, "資料新增失敗");
                    TempData["err"] = "App_6";
                    return RedirectToAction("MessageIndex");
                }
            }

            if(!string.IsNullOrWhiteSpace(url))
            {
                return Redirect(url);
            }
            else
            {
                return RedirectToAction("MessageIndex");
            }
        }

        [MyAuthorize(Com = Competence.Update, function = "推播管理")]
        [HttpPost]
        public ActionResult MessagePush(int[] item, int p = 1)
        {
            string url = (string)Session["Url"];
            PushMessageModel pmm = new PushMessageModel(fun_id);
            var list = pmm.Get_Many(item);
            if (list != null)
            {
                foreach (var i in list)
                {
                    PushModel.Push(i.AppId, Server, null, new int[] { i.Id });
                }
            }
            if(!string.IsNullOrWhiteSpace(url))
            {
                return Redirect(url);
            }
            else
            {
                return RedirectToAction("MessageIndex");
            }
        }

        [MyAuthorize(Com = Competence.Update, function = "推播管理")]
        public ActionResult MessagePushOne(int id = 0, int p = 1)
        {
            string url = (string)Session["Url"];
            MithDataContext db = new MithDataContext();
            PushMessageModel pmm = new PushMessageModel(fun_id);
            var item = pmm.Get_One(id);
            int? Vol = 0;
            if (item.VolunteerId != 0 && item.VolunteerId != null)
            {
                Vol = item.VolunteerId;
            }
            if (item != null)
            {
                PushModel.Push(item.AppId, Server, new int[] { Vol.Value }, new int[] { item.Id });
            }

            if(!string.IsNullOrWhiteSpace(url))
            {
                return Redirect(url);
            }
            else
            {
                return RedirectToAction("MessageIndex");
            }
        }

        [MyAuthorize(Com = Competence.Delete, function = "推播管理")]
        public ActionResult MessageDelete(int item, int p = 1)
        {
            if (!CheckLogin.CheckPermission(HttpContext, Competence.Delete, fun_id))
            {
                TempData["err"] = Method.Error_Permission;
                return RedirectToAction(Method.GoX_Default_Action, Method.GoX_Default_Controller, new { ReturnUrl = Request.RawUrl });
            }
            PushMessageModel.Delete(item);

            return RedirectToAction("MessageIndex", new { p = p });
        }

        [MyAuthorize(Com = Competence.Delete, function = "推播管理")]
        [HttpPost]
        public ActionResult MessageDelete(int[] item)
        {
            PushMessageModel.Delete(item);

            return RedirectToAction("MessageIndex");
        }
        #endregion

        #region error
        [MyAuthorize(Com = Competence.Read, function = "推播管理")]
        public ActionResult ErrorIndex(int[] appid, string keyword = "",
            DateTime? create_time_start = null, DateTime? create_time_end = null, int search_time_offset = 0,
            int p = 1, int show_number = 10)
        {
            ViewBag.Title = "錯誤記錄";
            ViewBag.Title_link = "<a href=\"ErrorIndex\">錯誤記錄</a>";
            PushErrorModel pem = new PushErrorModel(fun_id);

            string get = Method.Get_URLGet("keyword", keyword);
            pem.Keyword = keyword;
            if (appid != null)
            {
                if (appid.Any())
                {
                    pem.AppId.Clear();
                    pem.AppId.AddRange(appid);
                    get += Method.Get_URLGet("appid", appid);
                    ViewData["appid"] = appid;
                }
            }

            if (create_time_start != null)
            {
                get += Method.Get_URLGet("create_time_start", create_time_start.Value.ToString("yyyy/MM/dd HH:mm:ss"));
                ViewData["create_time_start"] = create_time_start;
                pem.create_time_start = create_time_start.Value.AddMinutes(search_time_offset);
            }
            if (create_time_end != null)
            {
                get += Method.Get_URLGet("create_time_end", create_time_end.Value.ToString("yyyy/MM/dd HH:mm:ss"));
                ViewData["create_time_end"] = create_time_end;
                pem.create_time_end = create_time_end.Value.AddMinutes(search_time_offset);
            }

            ViewData["p"] = p;
            ViewData["page"] = pem.Get_Page(p, show_number);
            ViewData["keyword"] = keyword;
            ViewData["get"] = get;
            return View(pem.Get_Data(p, show_number));
        }

        [MyAuthorize(Com = Competence.Delete, function = "推播管理")]
        public ActionResult ErrorDelete(int item, int p = 1)
        {
            PushErrorModel.Delete(item);

            return RedirectToAction("ErrorIndex", new { p = p });
        }

        [MyAuthorize(Com = Competence.Delete, function = "推播管理")]
        [HttpPost]
        public ActionResult ErrorDelete(int[] item)
        {
            PushErrorModel.Delete(item);

            return RedirectToAction("ErrorIndex");
        }

        [MyAuthorize(Com = Competence.Delete, function = "推播管理")]
        public ActionResult DeleteAll()
        {
            PushErrorModel.DeleteAll();

            return RedirectToAction("ErrorIndex");
        }

        [MyAuthorize(Com = Competence.Delete, function = "推播管理")]
        public ActionResult DeleteMonth(int DeleteMonth)
        {
            PushErrorModel.DeleteMonth(DeleteMonth);

            return RedirectToAction("ErrorIndex");
        }
        #endregion
    }
}

