﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;
using System.Text;
using System.IO;
using Mith.Models;
using Mith.Areas.GoX.Models;

namespace Mith.Areas.GoX.Controllers
{
    public class StyleAdviceController : CategoryController
    {
        int fun_id = 25;

        #region 首頁

        //[MyAuthorize(Com = Competence.Read, function = "穿搭諮詢")]
        public ActionResult Index(string keyword = "", int ExpertId = 0, DateTime? CreateTime_St = null, DateTime? CreateTime_End = null, int Search_Reply = 0, int p = 1, int show_number = 10)
        {
            MithDataContext db = new MithDataContext();
            StyleAdviceModel data = new StyleAdviceModel();
            var fun = FunctionModel.Get_One(fun_id);
            string get = "";

            if (ExpertId != 0)
            {
                ViewData["ExpertId"] = ExpertId;
                ViewData["Expert_Name"] = db.Expert_View.FirstOrDefault(f => f.Id == ExpertId).Name;
                get += Method.Get_URLGet("ExpertId", ExpertId.ToString());
            }
            if (keyword != "")
            {
                data.Keyword = keyword;
                ViewData["keyword"] = keyword;
                get += Method.Get_URLGet("keyword", keyword);
            }

            if (Search_Reply != 0)
            {
                data.Search_Reply = Search_Reply;
                ViewData["Search_Reply"] = Search_Reply;
                get += Method.Get_URLGet("Search_Reply", Search_Reply.ToString());
            }

            if (CreateTime_St != null)
            {
                data.CreateTime_St = CreateTime_St;
                ViewData["CreateTime_St"] = CreateTime_St;
                get += Method.Get_URLGet("CreateTime_St", CreateTime_St.Value.ToString("yyyy/MM/dd HH:mm:ss"));
            }
            if (CreateTime_End != null)
            {
                data.CreateTime_End = CreateTime_End;
                ViewData["CreateTime_End"] = CreateTime_End;
                get += Method.Get_URLGet("CreateTime_End", CreateTime_End.Value.ToString("yyyy/MM/dd HH:mm:ss"));
            }

            ViewBag.Title = fun.Name;
            ViewBag.Title_link = "達人管理 > <a href=\"/GoX/Expert/Index\">" + "達人資料" + "</a> ><a href=\"\">" + "穿搭諮詢" + "</a>";

            ViewData["p"] = p;
            ViewData["page"] = data.Get_Page(p, show_number, ExpertId);
            ViewData["number"] = show_number;
            ViewData["get"] = get;
            Session["Url"] = Request.Url.AbsoluteUri;
            db.Connection.Close();
            return View(data.Get_Data(p, show_number, ExpertId));
        }

        #endregion

				#region 編輯

				public ActionResult Edit(int id = 0, string From = "")
				{
					StyleAdviceModel data = new StyleAdviceModel();

					var fun = FunctionModel.Get_One(fun_id);


					var item = data.Get_One(id);
					if (item == null)
					{
						return RedirectToAction("Index_Mith");
					}

					ViewBag.Title = fun.Name;
					ViewBag.Title_link = "達人管理 > <a href=\"/GoX/Expert/Index\">" + "達人資料" + "</a> ><a href=/GoX/StyleAdvice?ExpertId="+item.ExpertId+">" + "穿搭諮詢" + "</a> > 詳情";

					return View(item);
				}
				#endregion

        #region 刪除

        //[MyAuthorize(Com = Competence.Delete, function = "穿搭諮詢")]
        public ActionResult Delete(int item, int p = 1)
        {
            StyleAdviceModel data = new StyleAdviceModel();
            data.Delete(item);
            TempData["FromAction"] = "刪除成功！";
            string Url = Session["Url"] as string;
            if (Url != null)
            {
                return Redirect(Url);
            }
            else {
                return Redirect("/GoX/Expert/Index");
            }
        }

        //[MyAuthorize(Com = Competence.Delete, function = "穿搭諮詢")]
        [HttpPost]
        public ActionResult Delete(int[] item)
        {
            StyleAdviceModel data = new StyleAdviceModel();
            data.Delete(item);

            return RedirectToAction("Index");
        }

        #endregion

    }
}

