﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;
using System.Text;
using System.IO;
using Mith.Models;
using Mith.Areas.GoX.Models;

namespace Mith.Areas.GoX.Controllers
{
    public class LikeExpertController : CategoryController
    {
        int fun_id = 27;

        #region 首頁

        //[MyAuthorize(Com = Competence.Read, function = "私訊會員")]
        public ActionResult Index(string keyword = "", int VolunteersId = 0, DateTime? CreateTime_St = null, DateTime? CreateTime_End = null,  int p = 1, int show_number = 10)
        {
            MithDataContext db = new MithDataContext();
            LikeExpertModel data = new LikeExpertModel();
            var fun = FunctionModel.Get_One(fun_id);
            string get = "";

            if (VolunteersId != 0)
            {
                ViewData["VolunteersId"] = VolunteersId;
                ViewData["Volunteers_Name"] = db.Volunteers_View.FirstOrDefault(f => f.Id == VolunteersId) != null ? db.Volunteers_View.FirstOrDefault(f => f.Id == VolunteersId).RealName:"";
                ViewData["Volunteers_Email"] = db.Volunteers.FirstOrDefault(f => f.Id == VolunteersId).Email;
                get += Method.Get_URLGet("ExpertId", VolunteersId.ToString());
            }
            if (keyword != "")
            {
                data.Keyword = keyword;
                ViewData["keyword"] = keyword;
                get += Method.Get_URLGet("keyword", keyword);
            }

            if (CreateTime_St != null)
            {
                data.CreateTime_St = CreateTime_St;
                ViewData["CreateTime_St"] = CreateTime_St;
                get += Method.Get_URLGet("CreateTime_St", CreateTime_St.Value.ToString("yyyy/MM/dd HH:mm:ss"));
            }
            if (CreateTime_End != null)
            {
                data.CreateTime_End = CreateTime_End;
                ViewData["CreateTime_End"] = CreateTime_End;
                get += Method.Get_URLGet("CreateTime_End", CreateTime_End.Value.ToString("yyyy/MM/dd HH:mm:ss"));
            }

            ViewBag.Title = fun.Name;
            ViewBag.Title_link = " <a href=\"/GoX/Volunteers/Index\">" + "會員管理" + "</a> ><a href=\"\">" + "追蹤達人" + "</a>";

            ViewData["p"] = p;
            ViewData["page"] = data.Get_Page(p, show_number, VolunteersId);
            ViewData["number"] = show_number;
            ViewData["get"] = get;
            Session["Url"] = Request.Url.AbsoluteUri;
            db.Connection.Close();
            return View(data.Get_Data(p, show_number, VolunteersId));
        }

        
        #endregion

        #region 刪除

        //[MyAuthorize(Com = Competence.Delete, function = "私訊會員")]
        public ActionResult Delete(int Id, int p = 1)
        {
            LikeExpertModel data = new LikeExpertModel();
            data.Delete(Id);
            TempData["FromAction"] = "刪除成功！";
            return RedirectToAction("Index", new { p = p });
        }

        #endregion

    }
}

