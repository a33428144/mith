﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mith.Models;
using Mith.Areas.GoX.Models;

namespace Mith.Areas.GoX.Controllers
{
    public class CategoryController : Controller
    {
        public bool Refresh
        {
            get
            {
                return this.Request.Headers["Pragma"] == "no-cache" ||
                       this.Request.Headers["Cache-Control"] == "max-age=0";
            }
        }
        public ActionResult CategoryConfig(int fun_id)
        {
            if (!CheckLogin.CheckPermission(HttpContext, Competence.SuperAdmin, fun_id))
            {
                TempData["err"] = Method.Error_Permission;
                return RedirectToAction(Method.GoX_Default_Action, Method.GoX_Default_Controller, new { ReturnUrl = Request.RawUrl });
            }

            var fun = FunctionModel.Get_One(fun_id);
            ViewBag.Title = fun.Name + " > 分類設定";
            ViewBag.Title_link = "<a href=\"Index?fun_id=" + fun_id + "\">" + fun.Name + "</a> > 分類設定";
            ViewData["path"] = fun.Controller;
            return View("~/Areas/GoX/Views/Category/CategoryConfig.cshtml", CategoryModel.Get_Config(fun_id));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CategoryConfig(CategoryConfig item)
        {
            if (!CheckLogin.CheckPermission(HttpContext, Competence.SuperAdmin, item.Fun_Id))
            {
                TempData["err"] = Method.Error_Permission;
                return RedirectToAction(Method.GoX_Default_Action, Method.GoX_Default_Controller, new { ReturnUrl = Request.RawUrl });
            }

            TempData["save"] = CategoryModel.Set_Config(item);

            return RedirectToAction("CategoryConfig", new { fun_id = item.Fun_Id });
        }

        //[MyAuthorize(Com = Competence.Read, function = "相簿管理")]
        public ActionResult Category(int fun_id, int upid = 0, int enable = 0, int disable = 0, string keyword = "", string sort = "", int p = 1, int show_number = 10)
        {
            if (!CheckLogin.CheckPermission(HttpContext, Competence.Read, fun_id))
            {
                TempData["err"] = Method.Error_Permission;
                return RedirectToAction(Method.GoX_Default_Action, Method.GoX_Default_Controller, new { ReturnUrl = Request.RawUrl });
            }

            var fun = FunctionModel.Get_One(fun_id);
            CategoryModel cm_data = new CategoryModel(fun_id);
            string path = fun.Controller;
            string get = Method.Get_URLGet("fun_id", fun_id.ToString());

            ViewBag.Title = fun.Name + " > 分類管理";
            ViewBag.Title_link = "<a href=\"Index?fun_id=" + fun_id + "\">" + fun.Name + "</a> > 分類管理";
            ViewData["fun_id"] = fun_id;
            ViewData["path"] = path;

            cm_data.Keyword = keyword;
            get += Method.Get_URLGet("keyword", keyword);
            if (enable == 1 && disable == 1)
            {
                cm_data.Search_Enable = null;
            }
            else
            {
                if (enable == 1)
                {
                    cm_data.Search_Enable = true;
                    get += Method.Get_URLGet("enable", enable.ToString());
                }
                if (disable == 1)
                {
                    cm_data.Search_Enable = false;
                    get += Method.Get_URLGet("disable", disable.ToString());
                }
            }
            cm_data.Sort = sort;
            get += Method.Get_URLGet("sort", sort);
            ViewData["p"] = p;
            ViewData["page"] = cm_data.Get_Page(p, show_number);
            ViewData["keyword"] = keyword;
            ViewData["enable"] = enable;
            ViewData["disable"] = disable;
            ViewData["get"] = get;

            return View("~/Areas/GoX/Views/Category/Category.cshtml", cm_data.Get_Data(p, show_number));
        }

        //[MyAuthorize(Com = Competence.Insert, function = "相簿管理")]
        public ActionResult CategoryCreate(int fun_id, int upid = 0)
        {
            if (!CheckLogin.CheckPermission(HttpContext, Competence.Insert, fun_id))
            {
                TempData["err"] = Method.Error_Permission;
                return RedirectToAction(Method.GoX_Default_Action, Method.GoX_Default_Controller, new { ReturnUrl = Request.RawUrl });
            }

            CategoryModel cm_data = new CategoryModel(fun_id);
            var fun = FunctionModel.Get_One(fun_id);
            string path = fun.Controller;

            ViewBag.Title = fun.Name + " > 分類管理" +" > 新增";
            ViewBag.Title_link = "<a href=\"Index?fun_id=" + fun_id + "\">" + fun.Name + "</a> > <a href=\"Category?fun_id=" + fun_id + "\">分類管理</a>" +  " > 新增";
            ViewBag.c_title = fun.Name + "分類";
            ViewData["path"] = path;
            ViewData["fun_id"] = fun_id;
            ViewData["upid"] = upid;
            return View("~/Areas/GoX/Views/Category/CategoryCreate.cshtml");
        }

        //[MyAuthorize(Com = Competence.Insert, function = "相簿管理")]
        [HttpPost]
        public ActionResult CategoryCreate(CategoryModel.CategoryShow item)
        {
            if (!CheckLogin.CheckPermission(HttpContext, Competence.Insert, item.Fun_Id))
            {
                TempData["err"] = Method.Error_Permission;
                return RedirectToAction(Method.GoX_Default_Action, Method.GoX_Default_Controller, new { ReturnUrl = Request.RawUrl });
            }

            CategoryModel cm_data = new CategoryModel(item.Fun_Id);

            if (ModelState.IsValid)
            {
                if (cm_data.Insert(item) <= 0)
                {
                    ErrorRecordModel.Error_Record(Method.Get_UserId_Admin(Request.Cookies, Session), "Category_11", Request.UserAgent, true, "資料新增失敗");
                    TempData["err"] = "Category_11，資料新增失敗";
                }
            }
            else
            {
                ErrorRecordModel.Error_Record(Method.Get_UserId_Admin(Request.Cookies, Session), "Category_12", Request.UserAgent, true, "資料新增失敗");
                TempData["err"] = "Category_12，資料新增失敗";
            }
            return RedirectToAction("Category", new {  fun_id = item.Fun_Id });
        }

        //[MyAuthorize(Com = Competence.Update, function = "相簿管理")]
        public ActionResult CategoryEdit(int fun_id, int upid, int id = 0, int p = 1)
        {
            if (!CheckLogin.CheckPermission(HttpContext, Competence.Update, fun_id))
            {
                TempData["err"] = Method.Error_Permission;
                return RedirectToAction(Method.GoX_Default_Action, Method.GoX_Default_Controller, new { ReturnUrl = Request.RawUrl });
            }

            var fun = FunctionModel.Get_One(fun_id);
            CategoryModel cm_data = new CategoryModel(fun_id);
            string path = fun.Controller;

            ViewBag.Title = fun.Name + " > 分類管理" + " > 編輯";
            ViewBag.Title_link = "<a href=\"../Index?fun_id=" + fun_id + "\">" + fun.Name + "</a> > <a href=\"../Category?fun_id=" + fun_id + "\">分類管理</a>" +  " > 編輯";
            ViewBag.c_title = fun.Name + "分類";
            ViewData["path"] = path;
            ViewData["fun_id"] = fun_id;
            ViewData["p"] = p;
            ViewData["upid"] = upid;
            var item = cm_data.Get_One(id);
            if (item == null)
            {
                return RedirectToAction("CategoryCreate", new { upid = upid, fun_id = fun_id });
            }
            return View("~/Areas/GoX/Views/Category/CategoryEdit.cshtml", item);
        }

        //[MyAuthorize(Com = Competence.Update, function = "相簿管理")]
        [HttpPost]
        public ActionResult CategoryEdit(CategoryModel.CategoryShow item, int upid, int delete = 0, int p = 1)
        {
            if (!CheckLogin.CheckPermission(HttpContext, Competence.Update, item.Fun_Id))
            {
                TempData["err"] = Method.Error_Permission;
                return RedirectToAction(Method.GoX_Default_Action, Method.GoX_Default_Controller, new { ReturnUrl = Request.RawUrl });
            }

            CategoryModel cm_data = new CategoryModel(item.Fun_Id);

            if (ModelState.IsValid)
            {
                if (cm_data.Update(item, Server, delete == 1) <= 0)
                {
                    ErrorRecordModel.Error_Record(Method.Get_UserId_Admin(Request.Cookies, Session), "Category_14", Request.UserAgent, true, "資料更新失敗");
                    TempData["err"] = "Category_14";
                    return RedirectToAction("Category", new { p = p, upid = upid, fun_id = item.Fun_Id });
                }
            }

            return RedirectToAction("Category", new { p = p, upid = upid, fun_id = item.Fun_Id });
        }

        //[MyAuthorize(Com = Competence.Delete, function = "相簿管理")]
        public ActionResult CategoryDelete(int fun_id, int item, int upid, int p = 1)
        {
            if (!CheckLogin.CheckPermission(HttpContext, Competence.Delete, fun_id))
            {
                TempData["err"] = Method.Error_Permission;
                return RedirectToAction(Method.GoX_Default_Action, Method.GoX_Default_Controller, new { ReturnUrl = Request.RawUrl });
            }

            CategoryModel cm_data = new CategoryModel(fun_id);

            cm_data.Delete(item, Server);

            return RedirectToAction("Category", new { p = p, upid = upid, fun_id = fun_id });
        }

        //[MyAuthorize(Com = Competence.Delete, function = "相簿管理")]
        [HttpPost]
        public ActionResult CategoryDelete(int fun_id, int[] item, int upid)
        {
            if (!CheckLogin.CheckPermission(HttpContext, Competence.Delete, fun_id))
            {
                TempData["err"] = Method.Error_Permission;
                return RedirectToAction(Method.GoX_Default_Action, Method.GoX_Default_Controller, new { ReturnUrl = Request.RawUrl });
            }

            CategoryModel cm_data = new CategoryModel(fun_id);

            cm_data.Delete(item, Server);

            return RedirectToAction("Category", new { upid = upid, fun_id = fun_id });
        }

        //[MyAuthorize(Com = Competence.Update, function = "相簿管理")]
        [HttpPost]
        public ActionResult CategorySet_Enable(int fun_id, int[] item, int upid, int p = 1)
        {
            if (!CheckLogin.CheckPermission(HttpContext, Competence.Update, fun_id))
            {
                TempData["err"] = Method.Error_Permission;
                return RedirectToAction(Method.GoX_Default_Action, Method.GoX_Default_Controller, new { ReturnUrl = Request.RawUrl });
            }

            CategoryModel cm_data = new CategoryModel(fun_id);

            cm_data.Update_Enable(item, true);

            return RedirectToAction("Category", new { p = p, upid = upid, fun_id = fun_id });
        }

        //[MyAuthorize(Com = Competence.Update, function = "相簿管理")]
        [HttpPost]
        public ActionResult CategorySet_Disable(int fun_id, int[] item, int upid, int p = 1)
        {
            if (!CheckLogin.CheckPermission(HttpContext, Competence.Update, fun_id))
            {
                TempData["err"] = Method.Error_Permission;
                return RedirectToAction(Method.GoX_Default_Action, Method.GoX_Default_Controller, new { ReturnUrl = Request.RawUrl });
            }

            CategoryModel cm_data = new CategoryModel(fun_id);

            cm_data.Update_Enable(item, false);

            return RedirectToAction("Category", new { p = p, upid = upid, fun_id = fun_id });
        }
    }
}
