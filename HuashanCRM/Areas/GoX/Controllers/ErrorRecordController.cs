﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mith.Models;
using Mith.Areas.GoX.Models;

namespace Mith.Areas.GoX.Controllers
{
    [MyAuthorize(Com = Competence.SuperAdmin, function = "錯誤記錄")]
    public class ErrorRecordController : Controller
    {
        ErrorRecordModel data = new ErrorRecordModel();

        //[MyAuthorize(Com = Competence.Read, function = "錯誤記錄")]
        public ActionResult Index(string keyword = "", int admin = 0,
            DateTime? create_time_start = null, DateTime? create_time_end = null, int search_time_offset = 0,
            int p = 1, int show_number = 10)
        {
            ViewBag.Title = "錯誤記錄管理";
            ViewBag.Title_link = "<a href=\"\">錯誤記錄管理</a>";
            string get = Method.Get_URLGet("keyword", keyword);
            data.Keyword = keyword;
            if (create_time_start != null)
            {
                get += Method.Get_URLGet("create_time_start", create_time_start.Value.ToString("yyyy/MM/dd HH:mm:ss"));
                ViewData["create_time_start"] = create_time_start;
                data.create_time_start = create_time_start.Value.AddMinutes(search_time_offset);
            }
            if (create_time_end != null)
            {
                get += Method.Get_URLGet("create_time_end", create_time_end.Value.ToString("yyyy/MM/dd HH:mm:ss"));
                ViewData["create_time_end"] = create_time_end;
                data.create_time_end = create_time_end.Value.AddMinutes(search_time_offset);
            }
            if (admin > 0)
            {
                data.Enable_Admin = true;
                get += Method.Get_URLGet("admin", admin.ToString());
                ViewData["admin"] = admin;
                switch (admin)
                {
                    case 1:
                        data.Admin = false;
                        break;
                    case 2:
                        data.Admin = true;
                        break;
                    default:
                        data.Enable_Admin = false;
                        data.Admin = false;
                        break;
                }
            }

            ViewData["p"] = p;
            ViewData["page"] = data.Get_Page(p, show_number);
            ViewData["keyword"] = keyword;
            ViewData["get"] = get;
            return View(data.Get_Data(p, show_number));
        }

        //[MyAuthorize(Com = Competence.Delete, function = "錯誤記錄")]
        public ActionResult Delete(int item, int p = 1)
        {
            data.Delete(item);

            return RedirectToAction("Index", new { p = p });
        }

        //[MyAuthorize(Com = Competence.Delete, function = "錯誤記錄")]
        [HttpPost]
        public ActionResult Delete(int[] item)
        {
            data.Delete(item);

            return RedirectToAction("Index");
        }

        //[MyAuthorize(Com = Competence.Delete, function = "錯誤記錄")]
        public ActionResult DeleteAll()
        {
            data.DeleteAll();

            return RedirectToAction("Index");
        }

        //[MyAuthorize(Com = Competence.Delete, function = "錯誤記錄")]
        public ActionResult DeleteMonth(int DeleteMonth)
        {
            data.DeleteMonth(DeleteMonth);

            return RedirectToAction("Index");
        }
    }
}

