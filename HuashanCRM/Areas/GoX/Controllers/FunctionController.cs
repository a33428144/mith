﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mith.Models;
using Mith.Areas.GoX.Models;

namespace Mith.Areas.GoX.Controllers
{
    [MyAuthorize(Com = Competence.Read, function = "功能模組")]
    public class FunctionController : CategoryController
    {
        private const int fun_id = 0;
        FunctionModel data = new FunctionModel();

        public ActionResult Index(int[] cid, string keyword = "", int enable = 0, int disable = 0,
            string[] type=null, int p = 1, int show_number = 10)
        {
            ViewBag.Title = "功能模組";
            ViewBag.Title_link = "<a href=\"Index\">功能模組</a>";
            string get = Method.Get_URLGet("keyword", keyword);
            data.Keyword = keyword;
            if (cid != null)
            {
                if (cid.Any())
                {
                    data.Cid.Clear();
                    data.Cid.AddRange(cid);
                    get += Method.Get_URLGet("cid", cid);
                    ViewData["cid"] = cid;
                }
            }
            if (enable == 1 && disable == 1)
            {
                data.Search_Enable = null;
            }
            else
            {
                if (enable == 1)
                {
                    data.Search_Enable = true;
                    get += Method.Get_URLGet("enable", enable.ToString());
                    ViewData["enable"] = enable;
                }
                if (disable == 1)
                {
                    data.Search_Enable = false;
                    get += Method.Get_URLGet("disable", disable.ToString());
                    ViewData["disable"] = disable;
                }
            }

            if (Method.Is_Login_SuperAdmin(Request.Cookies))
            {
                if (type != null)
                {
                    data.Type.AddRange(type);
                    get += Method.Get_URLGet("type", type);
                    ViewData["type"] = type;
                }
            }
            else
            {
                type = new string[] { "Module" };
                data.Type.AddRange(type);
                get += Method.Get_URLGet("type", type);
                ViewData["type"] = type;
            }
            

            ViewData["p"] = p;
            ViewData["page"] = data.Get_Page(p, show_number);
            ViewData["keyword"] = keyword;
            ViewBag.list_cid = CategoryModel.Get_Category_html(CategoryModel.Get_Category(fun_id,null), cid, true);//cm_data.Get_Category_List();
            ViewData["get"] = get;
            return View(data.Get_Data(p, show_number));
        }

        [MyAuthorize(Com = Competence.SuperAdmin, function = "功能模組")]
        public ActionResult Create()
        {
            ViewBag.Title = "功能模組 > 新增";
            ViewBag.Title_link = "<a href=\"Index\">功能模組</a> > 新增";
            CategoryModel cm_data = new CategoryModel(fun_id);
            ViewData["CategoryItem"] = cm_data.Get_Category_Select(0);

            return View();
        }

        [MyAuthorize(Com = Competence.SuperAdmin, function = "功能模組")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(FunctionModel.FunctionShow item)
        {
            if (ModelState.IsValid)
            {
                int id = data.Insert(item);
                if (id > 0)
                {
                    data.Module_Initial(item.Controller, id, item.Name, Method.Get_UserId_Admin(Request.Cookies, Session));
                }
            }
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id, int p = 1)
        {
            ViewBag.Title = "功能模組 > 編輯";
            ViewBag.Title_link = "<a href=\"../Index\">功能模組</a> > 編輯";
            ViewData["p"] = p;
            var item = data.Get_OneShow(id);

            if (!Method.Is_Login_SuperAdmin(Request.Cookies) && item.Type!="Module")
            {
                return RedirectToAction("Index", new { p = p });
            }

            CategoryModel cm_data = new CategoryModel(fun_id);
            ViewData["CategoryItem"] = cm_data.Get_Category_Select(item.CategoryId);

            return View(item);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(FunctionModel.FunctionShow item, int p = 1)
        {
            if (ModelState.IsValid)
            {
                data.Update(item, Method.Is_Login_SuperAdmin(Request.Cookies));
            }

            return RedirectToAction("Index", new { p = p });
        }

        [MyAuthorize(Com = Competence.SuperAdmin, function = "功能模組")]
        public ActionResult Delete(int id, int p = 1)
        {
            data.Delete(id);
            return RedirectToAction("Index", new { p = p });
        }

        [MyAuthorize(Com = Competence.SuperAdmin, function = "功能模組")]
        [HttpPost]
        public ActionResult Delete(int[] item, int p = 1)
        {
            data.Delete(item);
            return RedirectToAction("Index", new { p = p });
        }

        [HttpPost]
        public ActionResult Set_Enable(int[] item, int p = 1)
        {
            data.Update_Enable(item, Method.Is_Login_SuperAdmin(Request.Cookies), true);

            return RedirectToAction("Index", new { p = p });
        }

        [HttpPost]
        public ActionResult Set_Disable(int[] item, int p = 1)
        {
            data.Update_Enable(item, Method.Is_Login_SuperAdmin(Request.Cookies), false);

            return RedirectToAction("Index", new { p = p });
        }
    }
}
