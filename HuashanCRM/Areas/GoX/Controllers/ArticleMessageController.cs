﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;
using System.Text;
using System.IO;
using Mith.Models;
using Mith.Areas.GoX.Models;

namespace Mith.Areas.GoX.Controllers
{
	public class ArticleMessageController : CategoryController
	{
		#region 首頁

		//[MyAuthorize(Com = Competence.Read, function = "文章留言1")]
		public ActionResult Index_Expert(string keyword = "", int ArticletId = 0, int ExpertId = 0, DateTime? CreateTime_St = null, DateTime? CreateTime_End = null, int Search_Reply = 0, int p = 1, int show_number = 10)
		{
			MithDataContext db = new MithDataContext();
			ArticleMessageModel data = new ArticleMessageModel();
			string get = "";

			if (ArticletId != 0)
			{
				ViewData["ArticletId"] = ArticletId;
				ViewData["Article_Title"] = db.Article_View.FirstOrDefault(f => f.Id == ArticletId).Title;
				get += Method.Get_URLGet("ArticletId", ArticletId.ToString());
			}
			if (ExpertId != 0)
			{
				ViewData["ExpertId"] = ExpertId;
				ViewData["Expert_Name"] = db.Expert_View.FirstOrDefault(f => f.Id == ExpertId).Name;
				get += Method.Get_URLGet("ExpertId", ExpertId.ToString());
			}
			if (keyword != "")
			{
				data.Keyword = keyword;
				ViewData["keyword"] = keyword;
				get += Method.Get_URLGet("keyword", keyword);
			}

			if (Search_Reply != 0)
			{
				data.Search_Reply = Search_Reply;
				ViewData["Search_Reply"] = Search_Reply;
				get += Method.Get_URLGet("Search_Reply", Search_Reply.ToString());
			}

			if (CreateTime_St != null)
			{
				data.CreateTime_St = CreateTime_St;
				ViewData["CreateTime_St"] = CreateTime_St;
				get += Method.Get_URLGet("CreateTime_St", CreateTime_St.Value.ToString("yyyy/MM/dd HH:mm:ss"));
			}
			if (CreateTime_End != null)
			{
				data.CreateTime_End = CreateTime_End;
				ViewData["CreateTime_End"] = CreateTime_End;
				get += Method.Get_URLGet("CreateTime_End", CreateTime_End.Value.ToString("yyyy/MM/dd HH:mm:ss"));
			}

			ViewBag.Title_link = "達人管理 > <a href=\"ExpertArticle/Index\">" + "文章資料" + "</a> > " + "文章留言";

			ViewData["p"] = p;
			ViewData["page"] = data.Get_Page(p, show_number, ArticletId);
			ViewData["number"] = show_number;
			ViewData["get"] = get;
			Session["Url"] = Request.Url.AbsoluteUri;
			db.Connection.Close();
			return View(data.Get_Data(p, show_number, ArticletId));
		}

		public ActionResult Index_Fashion(string keyword = "", int ArticletId = 0, DateTime? CreateTime_St = null, DateTime? CreateTime_End = null, int Search_Reply = 0, int p = 1, int show_number = 10)
		{
			MithDataContext db = new MithDataContext();
			ArticleMessageModel data = new ArticleMessageModel();
			string get = "";

			if (ArticletId != 0)
			{
				ViewData["ArticletId"] = ArticletId;
				ViewData["Article_Title"] = db.Article_View.FirstOrDefault(f => f.Id == ArticletId).Title;
				get += Method.Get_URLGet("ArticletId", ArticletId.ToString());
			}
			if (keyword != "")
			{
				data.Keyword = keyword;
				ViewData["keyword"] = keyword;
				get += Method.Get_URLGet("keyword", keyword);
			}

			if (Search_Reply != 0)
			{
				data.Search_Reply = Search_Reply;
				ViewData["Search_Reply"] = Search_Reply;
				get += Method.Get_URLGet("Search_Reply", Search_Reply.ToString());
			}

			if (CreateTime_St != null)
			{
				data.CreateTime_St = CreateTime_St;
				ViewData["CreateTime_St"] = CreateTime_St;
				get += Method.Get_URLGet("CreateTime_St", CreateTime_St.Value.ToString("yyyy/MM/dd HH:mm:ss"));
			}
			if (CreateTime_End != null)
			{
				data.CreateTime_End = CreateTime_End;
				ViewData["CreateTime_End"] = CreateTime_End;
				get += Method.Get_URLGet("CreateTime_End", CreateTime_End.Value.ToString("yyyy/MM/dd HH:mm:ss"));
			}

			ViewBag.Title_link = "時尚新訊 > <a href=\"FashionArticle/Index\">" + "文章資料" + "</a> > " + "文章留言";

			ViewData["p"] = p;
			ViewData["page"] = data.Get_Page(p, show_number, ArticletId);
			ViewData["number"] = show_number;
			ViewData["get"] = get;
			Session["Url"] = Request.Url.AbsoluteUri;
			db.Connection.Close();
			return View(data.Get_Data(p, show_number, ArticletId));
		}

		#endregion

		#region 編輯

		[RoleAuthorize(Role = "亞設人員")]
		[MyAuthorize(Com = Competence.Update, function = "商品Q&A")]
		public ActionResult Edit(int id = 0, string From = "")
		{
			ArticleMessageModel data = new ArticleMessageModel();

			if (From == "Expert")
			{
				ViewBag.Title_link = "達人管理 > <a href=\"ExpertArticle/Index\">" + "文章資料" + "</a> > " + "文章留言";
			}
			else
			{
				ViewBag.Title_link = "時尚新訊 > <a href=\"FashionArticle/Index\">" + "文章資料" + "</a> > " + "文章留言";
			}

			var item = data.Get_One(id);
			if (item == null)
			{
				return RedirectToAction("Index_Mith");
			}
			return View(item);
		}
		#endregion

		#region 刪除

		//[MyAuthorize(Com = Competence.Delete, function = "文章留言1")]
		public ActionResult Delete(int item, int p = 1)
		{
			ArticleMessageModel data = new ArticleMessageModel();
			data.Delete(item);
			TempData["FromAction"] = "刪除成功！";
			return RedirectToAction("Index", new { p = p });
		}

		//[MyAuthorize(Com = Competence.Delete, function = "文章留言1")]
		[HttpPost]
		public ActionResult Delete(int[] item)
		{
			ArticleMessageModel data = new ArticleMessageModel();
			data.Delete(item);

			return RedirectToAction("Index");
		}

		#endregion
	}
}

