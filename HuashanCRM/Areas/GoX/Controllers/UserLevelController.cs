﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mith.Areas.GoX.Models;
using WebMatrix.WebData;
using System.Web.Security;
using System.Web.Mvc.Html;
using System.Text.RegularExpressions;
using Mith.Models;

namespace Mith.Areas.GoX.Controllers
{
    public class UserLevelController : Controller
    {
        UserLevelModel data = new UserLevelModel();
        int fun_id = 1;

        [RoleAuthorize(Role = "亞設人員")]
        [MyAuthorize(Com = Competence.Read, function = "權限管理")]
        public ActionResult Index_Mith(string keyword = "", int Search_Role = 0, int Search_CompanyId = 0, DateTime? CreateTime_St = null, DateTime? CreateTime_End = null, int p = 1, int show_number = 10)
        {
            var fun = FunctionModel.Get_One(fun_id);

            ViewBag.Title = fun.Name;
            ViewBag.Title_link = "<a href=\"\">" + fun.Name + "</a>";
            string get = "";
            if (keyword != "")
            {
                data.Keyword = keyword;
                ViewData["keyword"] = keyword;
                get += Method.Get_URLGet("keyword", keyword);
            }
            if (Search_Role != 0)
            {

                data.Search_Role = Search_Role;
                ViewData["Search_Role"] = Search_Role;
                get += Method.Get_URLGet("Search_Role", Search_Role.ToString());
            }
            if (Search_CompanyId != 0)
            {
                data.Search_CompanyId = Search_CompanyId;
                ViewData["Search_CompanyId"] = Search_CompanyId;
                get += Method.Get_URLGet("Search_CompanyId", Search_CompanyId.ToString());
            }
            if (CreateTime_St != null)
            {
                data.CreateTime_St = CreateTime_St;
                ViewData["CreateTime_St"] = CreateTime_St;
                get += Method.Get_URLGet("CreateTime_St", CreateTime_St.Value.ToString("yyyy/MM/dd HH:mm:ss"));
            }
            if (CreateTime_End != null)
            {
                data.CreateTime_End = CreateTime_End;
                ViewData["CreateTime_End"] = CreateTime_End;
                get += Method.Get_URLGet("CreateTime_End", CreateTime_End.Value.ToString("yyyy/MM/dd HH:mm:ss"));
            }

            ViewData["p"] = p;
            ViewData["page"] = data.Get_Page(p, show_number);
            ViewData["number"] = show_number;
            ViewData["get"] = get;
            Session["Url"] = Request.Url.AbsoluteUri;

            return View(data.Get_Data( p, show_number));
        }

        [RoleAuthorize(Role = "廠商")]
        [MyAuthorize(Com = Competence.Read, function = "權限管理")]
        public ActionResult Index_Company(string keyword = "", DateTime? CreateTime_St = null, DateTime? CreateTime_End = null, int p = 1, int show_number = 10)
        {
            var fun = FunctionModel.Get_One(fun_id);

            ViewBag.Title = fun.Name;
            ViewBag.Title_link = "<a href=\"\">" + fun.Name + "</a>";
            string get = "";
            if (keyword != "")
            {
                data.Keyword = keyword;
                ViewData["keyword"] = keyword;
                get += Method.Get_URLGet("keyword", keyword);
            }
            if (CreateTime_St != null)
            {
                data.CreateTime_St = CreateTime_St;
                ViewData["CreateTime_St"] = CreateTime_St;
                get += Method.Get_URLGet("CreateTime_St", CreateTime_St.Value.ToString("yyyy/MM/dd HH:mm:ss"));
            }
            if (CreateTime_End != null)
            {
                data.CreateTime_End = CreateTime_End;
                ViewData["CreateTime_End"] = CreateTime_End;
                get += Method.Get_URLGet("CreateTime_End", CreateTime_End.Value.ToString("yyyy/MM/dd HH:mm:ss"));
            }
            data.Search_CompanyId = Mith.Models.Method.Get_CompanyId_Admin(Request.Cookies, Session);

            ViewData["p"] = p;
            ViewData["page"] = data.Get_Page(p, show_number);
            ViewData["number"] = show_number;
            ViewData["get"] = get;
            Session["Url"] = Request.Url.AbsoluteUri;

            return View(data.Get_Data(p, show_number));
        }

        [RoleAuthorize(Role = "亞設人員")]
        [MyAuthorize(Com = Competence.Insert, function = "權限管理")]
        public ActionResult Create_Mith()
        {
            MithDataContext db = new MithDataContext();
            int id = db.UserLevel.OrderByDescending(o => o.Id).FirstOrDefault() != null ? db.UserLevel.OrderByDescending(o => o.Id).FirstOrDefault().Id + 1 : 1;
            var fun = FunctionModel.Get_One(fun_id);
            ViewBag.Title = fun.Name + " > 新增";
            ViewBag.Title_link = "<a href=\"Index_Mith\">" + fun.Name + "</a> > 新增";
            ViewBag.c_title = fun.Name + "資料";
            ViewData["Level_Id"] = id;

            ViewData["MithFunctionList"] = data.Get_MithFunction(id);
            ViewData["CompanyFunctionList"] = data.Get_CompanyFunction(id);
            db.Connection.Close();
            return View();
        }

        [RoleAuthorize(Role = "廠商")]
        [MyAuthorize(Com = Competence.Insert, function = "權限管理")]
        public ActionResult Create_Company()
        {
            MithDataContext db = new MithDataContext();
            int id = db.UserLevel.OrderByDescending(o => o.Id).FirstOrDefault() != null ? db.UserLevel.OrderByDescending(o => o.Id).FirstOrDefault().Id + 1 : 1;
            var fun = FunctionModel.Get_One(fun_id);
            ViewBag.Title = fun.Name + " > 新增";
            ViewBag.Title_link = "<a href=\"Index_Company\">" + fun.Name + "</a> > 新增";
            ViewBag.c_title = fun.Name + "資料";
            ViewData["Level_Id"] = id;
            ViewData["CompanyFunctionList"] = data.Get_CompanyFunction(id);
            db.Connection.Close();
            return View();
        }

        [HttpPost]
        [ValidateInput(true)]
        [ValidateAntiForgeryToken]
        public ActionResult Create(List<UserLevelModel.FunctionCompetenceShow> MithFunctionList, List<UserLevelModel.FunctionCompetenceShow> CompanyFunctionList, int Level_Id = 0, string Level_Name = "", int Level_Role = 0, int CompanyId = -1)
        {
            int result = 0;
            if (ModelState.IsValid)
            {
                data.Insert_UserLevel_Name(Level_Id, Level_Name, Level_Role, CompanyId);

                //角色：亞設
                if (MithFunctionList != null && Level_Role == 1)
                {
                    result = data.Update_FunctionCompetence(MithFunctionList, Level_Id);
                }
                //角色：廠商
                if (CompanyFunctionList != null && Level_Role == 2)
                {
                    result = data.Update_FunctionCompetence(CompanyFunctionList, Level_Id);
                }
                if (result <= 0)
                {
                    ErrorRecordModel.Error_Record(Method.Get_UserId_Admin(Request.Cookies, Session), "UserLevel_16", Request.UserAgent, true, "資料新增失敗");
                    TempData["err"] = "UserLevel_16";
                }

            }

            TempData["FromAction"] = "新增成功！";
            int Role = Mith.Models.Method.Get_UserRole_Admin(Request.Cookies, Session);
            if (Role == 1)
            {
                return RedirectToAction("Index_Mith");
            }
            else
            {
                return RedirectToAction("Index_Company");
            }
        }

        [RoleAuthorize(Role = "亞設人員")]
        [MyAuthorize(Com = Competence.Update, function = "權限管理")]
        public ActionResult Edit_Mith(int id = 0, int p = 1)
        {
            MithDataContext db = new MithDataContext();
            var fun = FunctionModel.Get_One(fun_id);

            ViewBag.Title = fun.Name + " > 編輯";
            ViewBag.Title_link = "<a href=\"../Index_Mith\">" + fun.Name + "</a> > 編輯";
            ViewBag.c_title = fun.Name + "資料";
            List<UserLevel_View> LevelList = new List<UserLevel_View>();
            LevelList = db.UserLevel_View.Where(f => f.Id == id).ToList();
            ViewData["LevelList"] = LevelList;

            ViewData["MithFunctionList"] = data.Get_MithFunction(id);
            ViewData["CompanyFunctionList"] = data.Get_CompanyFunction(id);
            db.Connection.Close();
            return View();
        }

        [RoleAuthorize(Role = "廠商")]
        [MyAuthorize(Com = Competence.Update, function = "權限管理")]
        public ActionResult Edit_Company(int id = 0, int p = 1)
        {
            MithDataContext db = new MithDataContext();
            var fun = FunctionModel.Get_One(fun_id);

            ViewBag.Title = fun.Name + " > 編輯";
            ViewBag.Title_link = "<a href=\"../Index_Company\">" + fun.Name + "</a> > 編輯";
            ViewBag.c_title = fun.Name + "資料";
            List<UserLevel_View> LevelList = new List<UserLevel_View>();
            LevelList = db.UserLevel_View.Where(f => f.Id == id).ToList();
            ViewData["LevelList"] = LevelList;
            ViewData["CompanyFunctionList"] = data.Get_CompanyFunction(id);
            db.Connection.Close();
            return View();
        }

        [HttpPost]
        [ValidateInput(true)]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(List<UserLevelModel.FunctionCompetenceShow> MithFunctionList, List<UserLevelModel.FunctionCompetenceShow> CompanyFunctionList,
            int Level_Id = 0, string Level_Name = "", int Level_Role = 2)
        {
            string url = (string)Session["Url"];
            int result = 0;
            if (ModelState.IsValid)
            {
                int user_id = Method.Get_UserId_Admin(Request.Cookies, Session);
                data.Update_UserLevel_Name(Level_Id, Level_Name, Level_Role, user_id);

                //角色：亞設
                if (MithFunctionList != null && Level_Role == 1)
                {
                    result = data.Update_FunctionCompetence(MithFunctionList, Level_Id);
                }
                //角色：廠商
                if (CompanyFunctionList != null && Level_Role == 2)
                {
                    result = data.Update_FunctionCompetence(CompanyFunctionList, Level_Id);
                }
            }
            if (result <= 0)
            {
                ErrorRecordModel.Error_Record(Method.Get_UserId_Admin(Request.Cookies, Session), "UserLevel_16", Request.UserAgent, true, "資料更新失敗");
                TempData["err"] = "UserLevel_16";
            }
            TempData["FromAction"] = "儲存成功！";
            if(!string.IsNullOrWhiteSpace(url))
            {
                return Redirect(url);
            }
            else
            {
                int Role = Mith.Models.Method.Get_UserRole_Admin(Request.Cookies, Session);
                if (Role == 1)
                {
                    return RedirectToAction("Index_Mith");
                }
                else
                {
                    return RedirectToAction("Index_Company");
                }
            }
        }

        [MyAuthorize(Com = Competence.Delete, function = "權限管理")]
        public ActionResult Delete(int item, int p = 1)
        {
            data.Delete(item);
            TempData["FromAction"] = "刪除成功！";
            int Role = Mith.Models.Method.Get_UserRole_Admin(Request.Cookies, Session);
            if (Role == 1)
            {
                return RedirectToAction("Index_Mith");
            }
            else
            {
                return RedirectToAction("Index_Company");
            }
        }

        [MyAuthorize(Com = Competence.Delete, function = "權限管理")]
        [HttpPost]
        public ActionResult Delete(int[] item)
        {

            data.Delete(item);

            int Role = Mith.Models.Method.Get_UserRole_Admin(Request.Cookies, Session);
            if (Role == 1)
            {
                return RedirectToAction("Index_Mith");
            }
            else
            {
                return RedirectToAction("Index_Company");
            }
        }

        //public ActionResult Ajax_GetFunction(int Id, int Role)
        //{

        //    return PartialView("Ajax_GetFunction", data.Get_Function(Id, Role));

        //}

    }
}
