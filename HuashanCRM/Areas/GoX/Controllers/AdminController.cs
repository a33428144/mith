﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mith.Models;

namespace Mith.Areas.GoX.Controllers
{
    public class AdminController : Controller
    {
        public ActionResult Index(int p = 1, int show_number = 10)
        {
            ViewBag.Title = "首頁";
            ViewBag.Title_link = "<a href=\"\">首頁 - 後台管理系統</a>";
            if (!Method.Is_Login_Admin(Request.Cookies))
            {
                return RedirectToAction("Login", "UserProfile", new { ReturnUrl=Method.RootPath+"/GoX/Admin/Index" });
            }
            return View();
        }

        public ActionResult Error()
        {
            ViewBag.Title = "發生錯誤";
            return View();
        }
    }
}
