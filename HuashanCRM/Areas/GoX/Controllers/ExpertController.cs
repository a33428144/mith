﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;
using System.Text;
using System.IO;
using Mith.Models;
using Mith.Areas.GoX.Models;

namespace Mith.Areas.GoX.Controllers
{
	public class ExpertController : CategoryController
	{
		int fun_id = 16;

		#region 首頁
		[RoleAuthorize(Role = "亞設人員")]
		[MyAuthorize(Com = Competence.Read, function = "達人資料")]
		public ActionResult Index(string keyword = "", int Search_Enable = 0, DateTime? CreateTime_St = null, DateTime? CreateTime_End = null, string TypeSort = "CreateTime", int p = 1, int show_number = 10)
		{
			ExpertModel data = new ExpertModel();
			var fun = FunctionModel.Get_One(fun_id);
			string get = "";
			if (keyword != "")
			{
				data.Keyword = keyword;
				ViewData["keyword"] = keyword;
				get += Method.Get_URLGet("keyword", keyword);
			}
			if (Search_Enable != 0)
			{
				data.Search_Enable = Search_Enable;
				ViewData["Search_Enable"] = Search_Enable;
				get += Method.Get_URLGet("Search_Enable", Search_Enable.ToString());
			}

			if (CreateTime_St != null)
			{
				data.CreateTime_St = CreateTime_St;
				ViewData["CreateTime_St"] = CreateTime_St;
				get += Method.Get_URLGet("CreateTime_St", CreateTime_St.Value.ToString("yyyy/MM/dd HH:mm:ss"));
			}
			if (CreateTime_End != null)
			{
				data.CreateTime_End = CreateTime_End;
				ViewData["CreateTime_End"] = CreateTime_End;
				get += Method.Get_URLGet("CreateTime_End", CreateTime_End.Value.ToString("yyyy/MM/dd HH:mm:ss"));
			}

			ViewBag.Title = fun.Name;
			ViewBag.Title_link = "達人管理 > <a href=\"\">" + fun.Name + "</a>";

			ViewData["p"] = p;
			ViewData["page"] = data.Get_Page(p, show_number);
			ViewData["number"] = show_number;
			ViewData["get"] = get;
			Session["Url"] = Request.Url.AbsoluteUri;

			data.TypeSort = TypeSort;
			ViewData["TypeSort"] = TypeSort;

			return View(data.Get_Data(p, show_number));
		}

		[RoleAuthorize(Role = "亞設人員")]
		[MyAuthorize(Com = Competence.Read, function = "達人資料")]
		public ActionResult Popular(int p = 1, int show_number = 10)
		{
			ExpertModel data = new ExpertModel();
			var fun = FunctionModel.Get_One(fun_id);
			string get = "";

			ViewBag.Title = "造型達人 > " + "達人資料" + " > 人氣排行 Top20";
			ViewBag.Title_link = "造型達人 > <a href=/GoX/Expert/Index>" + "達人資料" + "</a> > " + "人氣排行 Top20";

			ViewData["p"] = p;
			ViewData["page"] = data.Get_Page(p, show_number);
			ViewData["number"] = show_number;
			ViewData["get"] = get;
			Session["Url"] = Request.Url.AbsoluteUri;

			return View(data.Get_Data_Popular(p, show_number));
		}

		#endregion

		#region 新增

		[MyAuthorize(Com = Competence.Insert, function = "達人資料")]
		public ActionResult Create()
		{
			MithDataContext db = new MithDataContext();
			ExpertModel data = new ExpertModel();
			var fun = FunctionModel.Get_One(fun_id);

			ViewBag.Title = "達人管理 > " + fun.Name + " > 新增";
			ViewBag.Title_link = "達人管理 > <a href=\"Index\">" + fun.Name + "</a> > 新增";
			ViewBag.c_title = fun.Name;
			db.Connection.Close();
			return View();
		}


		[ValidateInput(true)]
		[ValidateAntiForgeryToken]
		[HttpPost]
		public ActionResult Create(ExpertModel.ExpertShow item, HttpPostedFileBase Pic, HttpPostedFileBase Cover)
		{

			MithDataContext db = new MithDataContext();
			ExpertModel data = new ExpertModel();
			var fun = FunctionModel.Get_One(fun_id);
			item.Id = db.Expert.OrderByDescending(o => o.Id).FirstOrDefault() != null ? db.Expert.OrderByDescending(o => o.Id).FirstOrDefault().Id + 1 : 1;

			if (ModelState.IsValid)
			{
				#region 上傳圖片
				string sContainer = "expert";
				string sImagePath = item.Id + "/";
				string name = Method.RandomKey(5, true, false, false, false);
				string FileExtension = "";
				Size Middle = new Size();
				Size Small = new Size();

				if (Pic != null)
				{
					FileExtension = System.IO.Path.GetExtension(Pic.FileName);

					//上傳大頭貼
					item.Pic = BlobHelper.Upload_Blob(new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + "1" + FileExtension }, Pic.InputStream, name + "1" + FileExtension, Middle, Small, false)[0];
				}

				if (Cover != null)
				{
					FileExtension = System.IO.Path.GetExtension(Cover.FileName);
					//上傳大頭貼
					item.Cover = BlobHelper.Upload_Blob(new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + "2" + FileExtension }, Cover.InputStream, name + "2" + FileExtension, Middle, Small, false)[0];
				}
				#endregion

				if (data.Insert(item) <= 0)
				{
					ErrorRecordModel.Error_Record(Method.Get_UserId_Admin(Request.Cookies, Session), "Expert_2", Request.UserAgent, true, "資料新增失敗");
					TempData["err"] = "Expert_2，資料新增失敗";
				}
			}
			else
			{
				ErrorRecordModel.Error_Record(Method.Get_UserId_Admin(Request.Cookies, Session), "Expert_3", Request.UserAgent, true, "資料新增失敗");
				TempData["err"] = "Expert_3，資料新增失敗";
			}
			db.Connection.Close();
			TempData["FromAction"] = "新增成功！";

			return RedirectToAction("Index");
		}

		#endregion

		#region 編輯

		[MyAuthorize(Com = Competence.Update, function = "達人資料")]
		public ActionResult Edit(int id = 0)
		{
			MithDataContext db = new MithDataContext();
			ExpertModel data = new ExpertModel();
			var fun = FunctionModel.Get_One(fun_id);

			ViewBag.Title = "達人管理 > " + fun.Name + " > 編輯";
			ViewBag.Title_link = "達人管理 > <a href=/Gox/ExpertIndex>" + fun.Name + "</a> > 編輯";
			ViewBag.c_title = fun.Name;

			var item = data.Get_One(id);
			if (item == null)
			{
				return RedirectToAction("Create_Step1");
			}
			db.Connection.Close();
			return View(item);
		}

		[MyAuthorize(Com = Competence.Update, function = "達人資料")]
		[ValidateInput(true)]
		[ValidateAntiForgeryToken]
		[HttpPost]
		public ActionResult Edit(ExpertModel.ExpertShow item, HttpPostedFileBase Pic, HttpPostedFileBase Cover)
		{
			MithDataContext db = new MithDataContext();
			ExpertModel data = new ExpertModel();
			string url = (string)Session["Url"];
			if (ModelState.IsValid)
			{
				string sContainer = "expert";
				string sImagePath = item.Id + "/";
				string FileExtension = "";
				string name = Method.RandomKey(5, true, false, false, false);

				Size Middle = new Size();
				Size Small = new Size();

				#region 刪除封面照
				if (item.Delete_Cover == true)
				{
					BlobHelper.delete(sContainer + "/" + item.Id, Path.GetFileName(item.Cover));
					item.Cover = null;
				}
				#endregion

				#region 更新圖片

				if (Pic != null)
				{
					FileExtension = System.IO.Path.GetExtension(Pic.FileName);
					//先刪除舊圖片
					BlobHelper.delete(sContainer + "/" + item.Id, Path.GetFileName(item.Pic));

					//上傳大頭貼
					item.Pic = BlobHelper.Upload_Blob(new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + "1" + FileExtension }, Pic.InputStream, name + "1" + FileExtension, Middle, Small, false)[0];
				}

				if (Cover != null)
				{
					FileExtension = System.IO.Path.GetExtension(Cover.FileName);
					//先刪除舊圖片
					BlobHelper.delete(sContainer + "/" + item.Id, Path.GetFileName(item.Cover));

					//上傳大頭貼
					item.Cover = BlobHelper.Upload_Blob(new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + "2" + FileExtension }, Cover.InputStream, name + "2" + FileExtension, Middle, Small, false)[0];
				}
				#endregion

				int user_id = Method.Get_UserId_Admin(Request.Cookies, Session);
				item.LastUserId = user_id;

				if (data.Update(item) <= 0)
				{
					ErrorRecordModel.Error_Record(Method.Get_UserId_Admin(Request.Cookies, Session), "Expert_5", Request.UserAgent, true, "資料新增失敗");
					TempData["err"] = "Expert_5";
					return RedirectToAction("Index");
				}
			}

			db.Connection.Close();
			TempData["FromAction"] = "儲存成功！";
			if (!string.IsNullOrWhiteSpace(url))
			{
				return Redirect(url);
			}
			else
			{
				return RedirectToAction("Index");
			}
		}

		#endregion

		#region 刪除

		[MyAuthorize(Com = Competence.Delete, function = "達人資料")]
		public ActionResult Delete(int item, int p = 1)
		{
			ExpertModel data = new ExpertModel();
			data.Delete(item);
			TempData["FromAction"] = "刪除成功！";
			return RedirectToAction("Index", new { p = p });
		}

		[MyAuthorize(Com = Competence.Delete, function = "達人資料")]
		[HttpPost]
		public ActionResult Delete(int[] item)
		{
			ExpertModel data = new ExpertModel();
			data.Delete(item);

			return RedirectToAction("Index");
		}

		#endregion

		[HttpPost]
		public ActionResult Export(string keyword = "")
		{
			ExpertModel data = new ExpertModel();
			//---------------匯出條件搜索---------------//

			if (keyword != null)
			{
				data.Keyword = keyword;
			}
			//---------------匯出條件搜索end---------------//
			return File(data.Set_Excel(data.Export_Data(data)), "application/vnd.ms-excel", "達人資料.xls");
		}
	}
}

