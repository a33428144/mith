﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using System.Web.UI.DataVisualization.Charting;
using System.Text;
using System.IO;
using System.Drawing;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Mith.Models;
using Mith.Areas.GoX.Models;

namespace Mith.Areas.GoX.Controllers
{
	public class GoodsController : CategoryController
	{
		int fun_id = 8;

		#region 首頁

		[RoleAuthorize(Role = "亞設人員")]
		[MyAuthorize(Com = Competence.Read, function = "商品資料編輯")]
		public ActionResult Index_Mith(string keyword = "", int Search_Type = 0, string Search_Pattern = "0", int Search_Season = 0, int Search_Color = 0, int Search_Style = 0, int Search_Fit = 0, int Search_Status = 0, int Search_SpecialOffer = 0, DateTime? CreateTime_St = null, DateTime? CreateTime_End = null, int Search_CompanyId = 0, int Search_Quantity_Total = 0, string TypeSort = "CreateTime", int p = 1, int show_number = 10)
		{
			MithDataContext db = new MithDataContext();
			GoodsModel data = new GoodsModel();
			var fun = FunctionModel.Get_One(fun_id);
			string get = "";

			if (keyword != "")
			{
				data.Keyword = keyword;
				ViewData["keyword"] = keyword;
				get += Method.Get_URLGet("keyword", keyword);
			}

			#region Search_變數
			if (Search_Type != 0)
			{
				data.Search_Type = Search_Type;
				ViewData["Search_Type"] = Search_Type;
				get += Method.Get_URLGet("Search_Type", Search_Type.ToString());
			}
			if (Search_Pattern != "0")
			{
				data.Search_Pattern = Search_Pattern;
				ViewData["Search_Pattern"] = Search_Pattern;
				get += Method.Get_URLGet("Search_Pattern", Search_Pattern.ToString());
			}
			if (Search_Season != 0)
			{
				data.Search_Season = Search_Season;
				ViewData["Search_Season"] = Search_Season;
				get += Method.Get_URLGet("Search_Season", Search_Season.ToString());
			}
			if (Search_Color != 0)
			{
				data.Search_Color = Search_Color;
				ViewData["Search_Color"] = Search_Color;
				get += Method.Get_URLGet("Search_Color", Search_Color.ToString());
			}
			if (Search_Style != 0)
			{
				data.Search_Style = Search_Style;
				ViewData["Search_Style"] = Search_Style;
				get += Method.Get_URLGet("Search_Style", Search_Style.ToString());
			}
			if (Search_Fit != 0)
			{
				data.Search_Fit = Search_Fit;
				ViewData["Search_Fit"] = Search_Fit;
				get += Method.Get_URLGet("Search_Fit", Search_Fit.ToString());
			}
			if (Search_Status != 0)
			{
				data.Search_Status = Search_Status;
				ViewData["Search_Status"] = Search_Status;
				get += Method.Get_URLGet("Search_Status", Search_Status.ToString());
			}
			if (Search_SpecialOffer != 0)
			{
				data.Search_SpecialOffer = Search_SpecialOffer;
				ViewData["Search_SpecialOffer"] = Search_SpecialOffer;
				get += Method.Get_URLGet("Search_SpecialOffer", Search_SpecialOffer.ToString());
			}
			if (Search_CompanyId != 0)
			{
				data.Search_CompanyId = Search_CompanyId;
				ViewData["Search_CompanyId"] = Search_CompanyId;
				get += Method.Get_URLGet("Search_CompanyId", Search_CompanyId.ToString());
			}
			if (Search_Quantity_Total != 0)
			{
				data.Search_Quantity_Total = Search_Quantity_Total;
				ViewData["Search_Quantity_Total"] = Search_Quantity_Total;
				get += Method.Get_URLGet("Search_Quantity_Total", Search_Quantity_Total.ToString());
			}
			#endregion

			if (CreateTime_St != null)
			{
				data.CreateTime_St = CreateTime_St;
				ViewData["CreateTime_St"] = CreateTime_St;
				get += Method.Get_URLGet("CreateTime_St", CreateTime_St.Value.ToString("yyyy/MM/dd HH:mm:ss"));
			}
			if (CreateTime_End != null)
			{
				data.CreateTime_End = CreateTime_End;
				ViewData["CreateTime_End"] = CreateTime_End;
				get += Method.Get_URLGet("CreateTime_End", CreateTime_End.Value.ToString("yyyy/MM/dd HH:mm:ss"));
			}
			ViewBag.Title = fun.Name;
			ViewBag.Title_link = "商品管理 > <a href=\"\">" + fun.Name + "</a>";

			ViewData["show_number"] = show_number;
			ViewData["p"] = p;
			ViewData["page"] = data.Get_Page(p, show_number);
			ViewData["number"] = show_number;
			ViewData["get"] = get;
			Session["Url"] = Request.Url.AbsoluteUri;
			Session["item"] = null;

			data.TypeSort = TypeSort;
			ViewData["TypeSort"] = TypeSort;
			get += Method.Get_URLGet("TypeSort", TypeSort);
			db.Connection.Close();
			return View(data.Get_Data_Mith(p, show_number));
		}

		[RoleAuthorize(Role = "廠商")]
		[MyAuthorize(Com = Competence.Read, function = "商品資料編輯")]
		public ActionResult Index_Company(string keyword = "", int Search_Type = 0, string Search_Pattern = "0", int Search_Season = 0, int Search_Color = 0, int Search_Style = 0, int Search_Fit = 0, int Search_Status = 0, int Search_SpecialOffer = 0, int Search_Quantity_Total = 0, DateTime? CreateTime_St = null, DateTime? CreateTime_End = null, string TypeSort = "CreateTime", int p = 1, int show_number = 10)
		{
			MithDataContext db = new MithDataContext();
			GoodsModel data = new GoodsModel();
			var fun = FunctionModel.Get_One(fun_id);
			string get = "";
			if (keyword != "")
			{
				data.Keyword = keyword;
				ViewData["keyword"] = keyword;
				get += Method.Get_URLGet("keyword", keyword);
			}

			#region Search_變數
			if (Search_Type != 0)
			{
				data.Search_Type = Search_Type;
				ViewData["Search_Type"] = Search_Type;
				get += Method.Get_URLGet("Search_Type", Search_Type.ToString());
			}
			if (Search_Pattern != "0")
			{
				data.Search_Pattern = Search_Pattern;
				ViewData["Search_Pattern"] = Search_Pattern;
				get += Method.Get_URLGet("Search_Pattern", Search_Pattern.ToString());
			}
			if (Search_Season != 0)
			{
				data.Search_Season = Search_Season;
				ViewData["Search_Season"] = Search_Season;
				get += Method.Get_URLGet("Search_Season", Search_Season.ToString());
			}
			if (Search_Color != 0)
			{
				data.Search_Color = Search_Color;
				ViewData["Search_Color"] = Search_Color;
				get += Method.Get_URLGet("Search_Color", Search_Color.ToString());
			}
			if (Search_Style != 0)
			{
				data.Search_Style = Search_Style;
				ViewData["Search_Style"] = Search_Style;
				get += Method.Get_URLGet("Search_Style", Search_Style.ToString());
			}
			if (Search_Fit != 0)
			{
				data.Search_Fit = Search_Fit;
				ViewData["Search_Fit"] = Search_Fit;
				get += Method.Get_URLGet("Search_Fit", Search_Fit.ToString());
			}
			if (Search_Status != 0)
			{
				data.Search_Status = Search_Status;
				ViewData["Search_Status"] = Search_Status;
				get += Method.Get_URLGet("Search_Status", Search_Status.ToString());
			}
			if (Search_SpecialOffer != 0)
			{
				data.Search_SpecialOffer = Search_SpecialOffer;
				ViewData["Search_SpecialOffer"] = Search_SpecialOffer;
				get += Method.Get_URLGet("Search_SpecialOffer", Search_SpecialOffer.ToString());
			}

			if (Search_Quantity_Total != 0)
			{
				data.Search_Quantity_Total = Search_Quantity_Total;
				ViewData["Search_Quantity_Total"] = Search_Quantity_Total;
				get += Method.Get_URLGet("Search_Quantity_Total", Search_Quantity_Total.ToString());
			}
			#endregion

			if (CreateTime_St != null)
			{
				data.CreateTime_St = CreateTime_St;
				ViewData["CreateTime_St"] = CreateTime_St;
				get += Method.Get_URLGet("CreateTime_St", CreateTime_St.Value.ToString("yyyy/MM/dd HH:mm:ss"));
			}
			if (CreateTime_End != null)
			{
				data.CreateTime_End = CreateTime_End;
				ViewData["CreateTime_End"] = CreateTime_End;
				get += Method.Get_URLGet("CreateTime_End", CreateTime_End.Value.ToString("yyyy/MM/dd HH:mm:ss"));
			}

			int CompanyId = Mith.Models.Method.Get_CompanyId_Admin(Request.Cookies, Session);
			data.Search_CompanyId = CompanyId;
			ViewData["CompanyId"] = CompanyId;

			ViewBag.Title = fun.Name;
			ViewBag.Title_link = "商品管理 > <a href=\"\">" + fun.Name + "</a>";

			ViewData["p"] = p;
			ViewData["page"] = data.Get_Page(p, show_number);
			ViewData["number"] = show_number;
			ViewData["get"] = get;
			Session["Url"] = Request.Url.AbsoluteUri;
			db.Connection.Close();
			Session["item"] = null;
			data.TypeSort = TypeSort;
			ViewData["TypeSort"] = TypeSort;
			get += Method.Get_URLGet("TypeSort", TypeSort);
			return View(data.Get_Data_Company(p, show_number));
		}

		#endregion

		#region 新增

		[MyAuthorize(Com = Competence.Insert, function = "商品資料編輯")]
		public ActionResult Create_Step1()
		{
			MithDataContext db = new MithDataContext();
			GoodsModel data = new GoodsModel();
			var fun = FunctionModel.Get_One(fun_id);

			ViewBag.Title = "商品管理 > " + fun.Name + " > 新增 (基本資訊)";
			ViewBag.Title_link = "商品管理 > <a href=\"Index_Company\">" + fun.Name + "</a> > 新增 (基本資訊)";
			ViewBag.c_title = fun.Name;

			int CompanyId = Mith.Models.Method.Get_CompanyId_Admin(Request.Cookies, Session);

			bool IsLimitOrder = true;
			string GoodsKind = db.Company.FirstOrDefault(f => f.Id == CompanyId) != null ? db.Company.FirstOrDefault(f => f.Id == CompanyId).GoodsKind : "";
			if (!GoodsKind.Contains("3"))
			{
				IsLimitOrder = false;
			}
			ViewData["CompanyId"] = CompanyId;
			ViewData["IsLimitOrder"] = IsLimitOrder;

			db.Connection.Close();
			return View();
		}

		[ValidateInput(true)]
		[ValidateAntiForgeryToken]
		[HttpPost]
		public ActionResult Create_Step1(GoodsModel.GoodsShow item)
		{

			GoodsModel data = new GoodsModel();
			var fun = FunctionModel.Get_One(fun_id);

			Session["item_Step1"] = item;  //GoodsModel.GoodsShow 

			return RedirectToAction("Create_Step2");
		}

		[RoleAuthorize(Role = "廠商")]
		[MyAuthorize(Com = Competence.Insert, function = "商品資料編輯")]
		public ActionResult Create_Step2(GoodsModel.GoodsShow item)
		{
			MithDataContext db = new MithDataContext();
			GoodsModel data = new GoodsModel();
			var fun = FunctionModel.Get_One(fun_id);
			item = Session["item_Step1"] as GoodsModel.GoodsShow;

			if (item == null)
			{
				return RedirectToAction("Create_Step1");
			}
			ViewBag.Title = "商品管理 > " + fun.Name + " > 新增 (樣式設定)";
			ViewBag.Title_link = "商品管理 > <a href=\"Index_Company\">" + fun.Name + "</a> > 新增 (樣式設定)";
			ViewBag.c_title = fun.Name;

			db.Connection.Close();
			return View(item);
		}

		[ValidateInput(true)]
		[ValidateAntiForgeryToken]
		[HttpPost]
		public ActionResult Create_Step2(GoodsModel.GoodsShow item, List<HttpPostedFileBase> Files = null)
		{

			MithDataContext db = new MithDataContext();
			GoodsModel data = new GoodsModel();
			var fun = FunctionModel.Get_One(fun_id);
			//List<HttpPostedFileBase> Files = Session["Files"] as List<HttpPostedFileBase>;
			item.Id = db.Goods.OrderByDescending(o => o.Id).FirstOrDefault() != null ? db.Goods.OrderByDescending(o => o.Id).FirstOrDefault().Id + 1 : 1;
			if (data.Insert(item) <= 0)
			{
				ErrorRecordModel.Error_Record(Method.Get_UserId_Admin(Request.Cookies, Session), "Goods_2", Request.UserAgent, true, "資料新增失敗");
				TempData["err"] = "Goods_2，資料新增失敗";
			}

			#region 新增圖片 ( 複選多張 )
			if (Files.Any() && Files[0] != null)
			{
				try
				{
					foreach (var i in Files)
					{
						string sContainer = "goods";
						string sImagePath = "Big/" + item.Id + "/";
						string FileExtension = System.IO.Path.GetExtension(i.FileName);
						string name = Method.RandomKey(5, true, false, false, false);

						Size Middle = new Size();
						Middle = new Size(864, 640);
						Size Small = new Size();
						Small = new Size(432, 320);

						string Path_Big = BlobHelper.Upload_Blob(new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + FileExtension }, i.InputStream, name + FileExtension, Middle, Small, true)[0];
						string Path_Middle = Path_Big.Replace("Big", "Middle");
						string Path_Small = Path_Big.Replace("Big", "Small");

						//新增商品圖片
						data.Insert_Files(item.Id, Path_Big, Path_Middle, Path_Small, item.CompanyId);
					}
				}
				//圖片新增失敗
				catch
				{
				}
			}
			#endregion

			db.Connection.Close();
			TempData["FromAction"] = "新增成功！";
			Session["item_Step1"] = null;
			//Session["Files"] = null;
			return RedirectToAction("Index_Company");
		}

		#endregion

		#region 編輯

		[MyAuthorize(Com = Competence.Update, function = "商品資料編輯")]
		public ActionResult Edit_Step1_Mith(int id = 0)
		{
			GoodsModel data = new GoodsModel();
			var fun = FunctionModel.Get_One(fun_id);

			ViewBag.Title = "商品管理 > " + fun.Name + " > 詳情 (基本資訊) ";
			ViewBag.Title_link = "商品管理 > <a href=\"Index_Mith\">" + fun.Name + "</a> > 詳情 (基本資訊)";
			ViewBag.c_title = fun.Name;

			var item = data.Get_One_Step1(id);
			if (item == null)
			{
				return RedirectToAction("Create_Step1");
			}
			return View(item);
		}

		[MyAuthorize(Com = Competence.Update, function = "商品資料編輯")]
		public ActionResult Edit_Step1_Company(int id = 0)
		{
			MithDataContext db = new MithDataContext();
			GoodsModel data = new GoodsModel();
			var fun = FunctionModel.Get_One(fun_id);

			ViewBag.Title = "商品管理 > " + fun.Name + " > 編輯 (基本資訊) ";
			ViewBag.Title_link = "商品管理 >  <a href=\"Index_Company\">" + fun.Name + "</a> > 編輯 (基本資訊)";
			ViewBag.c_title = fun.Name;

			var item = data.Get_One_Step1(id);
			if (item == null)
			{
				return RedirectToAction("Index_Company");
			}
			bool IsLimitOrder = true;
			string GoodsKind = db.Company.FirstOrDefault(f => f.Id == item.CompanyId) != null ? db.Company.FirstOrDefault(f => f.Id == item.CompanyId).GoodsKind : "";
			if (!GoodsKind.Contains("3"))
			{
				IsLimitOrder = false;
			}
			ViewData["IsLimitOrder"] = IsLimitOrder;


			db.Connection.Close();
			return View(item);
		}

		[MyAuthorize(Com = Competence.Update, function = "商品資料編輯")]
		[ValidateInput(true)]
		[ValidateAntiForgeryToken]
		[HttpPost]
		public ActionResult Edit_Step1(GoodsModel.GoodsShowEdit item)
		{
			GoodsModel data = new GoodsModel();
			var fun = FunctionModel.Get_One(fun_id);
			//string url = (string)Session["Url"];
			int Role = Mith.Models.Method.Get_UserRole_Admin(Request.Cookies, Session);

			item.LastUserId = Method.Get_UserId_Admin(Request.Cookies, Session);
			Session["item_Step1"] = item;  //GoodsModel.GoodsShowEdit 

			//    if (data.Update_Step1(item) <= 0)
			//    {
			//        ErrorRecordModel.Error_Record(Method.Get_UserId_Admin(Request.Cookies, Session), "Goods_5", Request.UserAgent, true, "資料新增失敗");
			//        TempData["err"] = "Goods_5";
			//        if (!string.IsNullOrWhiteSpace(url))
			//        {
			//            return Redirect(url);
			//        }
			//        else
			//        {
			//            return RedirectToAction("Index_Mith");
			//        }
			//    }

			if (Role == 1)
			{
				return RedirectToAction("Edit_Step2_Mith", new { id = item.Id });
			}
			else
			{
				return RedirectToAction("Edit_Step2_Company", new { id = item.Id });
			}
			//TempData["FromAction"] = "儲存成功！";
			//if(!string.IsNullOrWhiteSpace(url))
			//{
			//    return Redirect(url);
			//}
			//else
			//{
			//    if (Role == 1)
			//    {
			//        return RedirectToAction("Index_Mith");
			//    }
			//    else
			//    {
			//        return RedirectToAction("Index_Company");
			//    }
			//}
		}

		[MyAuthorize(Com = Competence.Update, function = "商品資料編輯")]
		public ActionResult Edit_Step2_Mith(int id = 0)
		{
			MithDataContext db = new MithDataContext();
			GoodsModel data = new GoodsModel();
			var fun = FunctionModel.Get_One(fun_id);

			ViewBag.Title = "商品管理 > " + fun.Name + " > 詳情 (樣式設定) ";
			ViewBag.Title_link = "商品管理 >  <a href=\"Index_Mith\">" + fun.Name + "</a> > 詳情 (樣式設定)";
			ViewBag.c_title = fun.Name;

			var item = data.Get_One_Step2(id);
			if (item == null)
			{
				return RedirectToAction("Create_Step1");
			}
			ViewData["Get_PicList"] = db.GoodsPic.Where(w => w.GoodsId == item.Id).ToList();
			db.Connection.Close();
			return View(item);
		}

		[MyAuthorize(Com = Competence.Update, function = "商品資料編輯")]
		public ActionResult Edit_Step2_Company(int id = 0)
		{
			MithDataContext db = new MithDataContext();
			GoodsModel data = new GoodsModel();
			var fun = FunctionModel.Get_One(fun_id);

			ViewBag.Title = "商品管理 > " + fun.Name + " > 編輯 (樣式設定) ";
			ViewBag.Title_link = "商品管理 >  <a href=\"Index_Company\">" + fun.Name + "</a> > 編輯 (樣式設定)";
			ViewBag.c_title = fun.Name;

			var item = data.Get_One_Step2(id);
			if (item == null)
			{
				return RedirectToAction("Create_Step1");
			}
			ViewData["Get_PicList"] = db.GoodsPic.Where(w => w.GoodsId == item.Id).ToList();
			ViewData["Delete"] = null;
			db.Connection.Close();
			return View(item);
		}

		[MyAuthorize(Com = Competence.Update, function = "商品資料編輯")]
		[ValidateInput(true)]
		[ValidateAntiForgeryToken]
		[HttpPost]
		public ActionResult Edit_Step2(GoodsModel.GoodsShowEdit item_Step2, List<HttpPostedFileBase> Files = null)
		{

			GoodsModel data = new GoodsModel();
			var fun = FunctionModel.Get_One(fun_id);
			string url = (string)Session["Url"];
			int user_id = Method.Get_UserId_Admin(Request.Cookies, Session);
			item_Step2.LastUserId = user_id;
			GoodsModel.GoodsShowEdit item_Step1 = Session["item_Step1"] as GoodsModel.GoodsShowEdit;
			if (data.Update(item_Step1, item_Step2) <= 0)
			{
				ErrorRecordModel.Error_Record(Method.Get_UserId_Admin(Request.Cookies, Session), "Goods_5", Request.UserAgent, true, "資料新增失敗");
				TempData["err"] = "Goods_5";
				if (!string.IsNullOrWhiteSpace(url))
				{
					return Redirect(url);
				}
				else
				{
					return RedirectToAction("Index_Company");
				}
			}

			#region 新增圖片 ( 複選多張 )
			if (Files.Any() && Files[0] != null)
			{
				try
				{
					foreach (var i in Files)
					{
						string sContainer = "goods";
						string sImagePath = "Big/" + item_Step2.Id + "/";
						string FileExtension = System.IO.Path.GetExtension(i.FileName);
						string name = Method.RandomKey(5, true, false, false, false);

						Size Middle = new Size();
						Middle = new Size(864, 640);
						Size Small = new Size();
						Small = new Size(432, 320);

						string Path_Big = BlobHelper.Upload_Blob(new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + FileExtension }, i.InputStream, name + FileExtension, Middle, Small, true)[0];
						string Path_Middle = Path_Big.Replace("Big", "Middle");
						string Path_Small = Path_Big.Replace("Big", "Small");

						//新增商品圖片
						data.Insert_Files(item_Step2.Id, Path_Big, Path_Middle, Path_Small, item_Step2.CompanyId);
					}
				}
				//圖片新增失敗
				catch
				{
				}
			}
			#endregion

			TempData["FromAction"] = "儲存成功！";
			if (!string.IsNullOrWhiteSpace(url))
			{
				return Redirect(url);
			}
			else
			{
				return RedirectToAction("Index_Company");
			}
		}

		#endregion

		#region 刪除

		[MyAuthorize(Com = Competence.Delete, function = "商品資料編輯")]
		public ActionResult Delete(int item, int p = 1)
		{
			string url = (string)Session["Url"];
			int Role = Mith.Models.Method.Get_UserRole_Admin(Request.Cookies, Session);
			GoodsModel data = new GoodsModel();
			data.Delete(item);
			TempData["FromAction"] = "刪除成功！";

			if (!string.IsNullOrWhiteSpace(url))
			{
				return Redirect(url);
			}
			else
			{
				if (Role == 1)
				{
					return RedirectToAction("Index_Mith", new { p = p });
				}
				else
				{
					return RedirectToAction("Index_Company", new { p = p });
				}
			}

		}

		//[MyAuthorize(Com = Competence.Delete, function = "商品資料編輯")]
		//[HttpPost]
		//public ActionResult Delete(int[] item)
		//{
		//    if (!CheckLogin.CheckPermission(HttpContext, Competence.Delete, fun_id))
		//    {
		//        TempData["err"] = Method.Error_Permission;
		//        return RedirectToAction(Method.GoX_Default_Action, Method.GoX_Default_Controller, new { ReturnUrl = Request.RawUrl });
		//    }

		//    GoodsModel data = new GoodsModel();
		//    data.Delete(item);

		//    return RedirectToAction("Index");
		//}

		#endregion

		#region 匯入
		private string ImportPath = "/upload/Import";
		/// <summary>
		/// Step1 將檔案上傳
		/// </summary>
		/// <param name="file">The file.</param>
		/// <returns>Json</returns>
		[HttpPost]
		public ActionResult Upload(HttpPostedFileBase file)
		{
			JObject jo = new JObject();
			string result = string.Empty;
			string fileExtName = Path.GetExtension(file.FileName).ToLower();

			if (!fileExtName.Equals(".xls", StringComparison.OrdinalIgnoreCase))
			{
				jo.Add("Result", false);
				jo.Add("Msg", "請上傳 .xls格式的檔案");
				result = JsonConvert.SerializeObject(jo);
				return Content(result, "application/json");
			}

			try
			{
				string uploadResult = this.FileUploadHandler(file);

				jo.Add("Result", !string.IsNullOrWhiteSpace(uploadResult));
				jo.Add("Msg", !string.IsNullOrWhiteSpace(uploadResult) ? uploadResult : "");
				result = JsonConvert.SerializeObject(jo);
			}
			catch (Exception ex)
			{
				jo.Add("Result", false);
				jo.Add("Msg", ex.Message);
				result = JsonConvert.SerializeObject(jo);
			}
			return Content(result, "application/json");
		}

		/// <summary>
		/// Step2 檔案上傳完，檢查檔案
		/// </summary>
		/// <param name="file">The file.</param>
		/// <returns></returns>
		/// <exception cref="System.ArgumentNullException">file;上傳失敗：沒有檔案！</exception>
		/// <exception cref="System.InvalidOperationException">上傳失敗：檔案沒有內容！</exception>
		private string FileUploadHandler(HttpPostedFileBase file)
		{
			string result;

			if (file == null)
			{
				throw new ArgumentNullException("file", "上傳失敗：沒有檔案！");
			}
			if (file.ContentLength <= 0)
			{
				throw new InvalidOperationException("上傳失敗：檔案沒有內容！");
			}

			try
			{
				string virtualBaseFilePath = Url.Content(ImportPath);
				string filePath = HttpContext.Server.MapPath(virtualBaseFilePath);

				if (!Directory.Exists(filePath))
				{
					Directory.CreateDirectory(filePath);
				}

				string newFileName = string.Concat(
						DateTime.Now.ToString("yyyyMMddHHmmss"),
						Path.GetExtension(file.FileName).ToLower());

				string fullFilePath = Path.Combine(Server.MapPath(ImportPath), newFileName);
				file.SaveAs(fullFilePath);

				result = newFileName;
			}
			catch (Exception ex)
			{
				throw;
			}
			return result;
		}

		/// <summary>
		/// Step3 取得檔案路徑，進行匯入
		/// </summary>
		/// <param name="savedFileName">檔案名稱</param>
		/// <returns>Json</returns>
		[HttpPost]
		public ActionResult Import(string savedFileName)
		{
			var jo = new JObject();
			string result;
			var fileName = string.Concat(Server.MapPath(ImportPath), "/", savedFileName);
			MithDataContext db = new MithDataContext();
			try
			{
				int CompanyId = Mith.Models.Method.Get_CompanyId_Admin(Request.Cookies, Session);
				var ImportGoods = new List<GoodsModel.ImportClass>();

				GoodsModel data = new GoodsModel();
				//檢查資料
				var checkResult = data.CheckImportData(fileName, ImportGoods, CompanyId);

				jo.Add("Result", checkResult.Success);
				jo.Add("Msg", checkResult.Success ? string.Empty : checkResult.ErrorMessage);
				jo.Add("RowCount", checkResult.RowCount);
				//資料都無誤
				if (checkResult.Success)
				{
					//將資料Insert到DB
					data.InsertImportData(fileName, ImportGoods);
				}
				result = JsonConvert.SerializeObject(jo);
			}
			catch (Exception ex)
			{

				throw;
			}
			finally
			{
				db.Connection.Close();
				//一定會刪除此檔案
				if (System.IO.File.Exists(fileName))
				{
					System.IO.File.Delete(fileName);
				}
			}
			return Content(result, "application/json");
		}
		#endregion

		#region 匯出 (亞設)
		[HttpPost]
		public ActionResult Export_Mith(string keyword = "", int Search_Type = 0, string Search_Pattern = "0", int Search_Season = 0, int Search_Color = 0, int Search_Style = 0, int Search_Fit = 0, int Search_Status = 0, int Search_SpecialOffer = 0, DateTime? CreateTime_St = null, DateTime? CreateTime_End = null, int Search_CompanyId = 0, string TypeSort = "CreateTime")
		{
			GoodsModel data = new GoodsModel();
			MithDataContext db = new MithDataContext();
			try
			{
				//---------------匯出條件搜索---------------//

				if (keyword != "")
				{
					data.Keyword = keyword;
				}

				#region Search_變數
				if (Search_Type != 0)
				{
					data.Search_Type = Search_Type;
				}
				if (Search_Pattern != "0")
				{
					data.Search_Pattern = Search_Pattern;
				}
				if (Search_Season != 0)
				{
					data.Search_Season = Search_Season;
				}
				if (Search_Color != 0)
				{
					data.Search_Color = Search_Color;
				}
				if (Search_Style != 0)
				{
					data.Search_Style = Search_Style;
				}
				if (Search_Fit != 0)
				{
					data.Search_Fit = Search_Fit;
				}
				if (Search_Status != 0)
				{
					data.Search_Status = Search_Status;
				}
				if (Search_SpecialOffer != 0)
				{
					data.Search_SpecialOffer = Search_SpecialOffer;
				}
				if (Search_CompanyId != 0)
				{
					data.Search_CompanyId = Search_CompanyId;
				}
				#endregion

				if (CreateTime_St != null)
				{
					data.CreateTime_St = CreateTime_St;
				}
				if (CreateTime_End != null)
				{
					data.CreateTime_End = CreateTime_End;
				}
				data.TypeSort = TypeSort;

				//---------------匯出條件搜索end---------------//
				return File(data.Set_Excel_Mith(data.Export_Data_Mith(), true), "application/vnd.ms-excel", "匯出商品資料.xls");
			}
			catch (Exception ex)
			{

				throw;
			}
			finally
			{
				db.Connection.Close();
			}
		}

		#endregion

		#region 匯出 (廠商)
		[HttpPost]
		public ActionResult Export_Company(string keyword = "", int Search_Type = 0, string Search_Pattern = "0", int Search_Season = 0, int Search_Color = 0, int Search_Style = 0, int Search_Fit = 0, int Search_Status = 0, int Search_SpecialOffer = 0, DateTime? CreateTime_St = null, DateTime? CreateTime_End = null, int Search_CompanyId = 0, string TypeSort = "CreateTime")
		{
			GoodsModel data = new GoodsModel();
			MithDataContext db = new MithDataContext();
			try
			{
				//---------------匯出條件搜索---------------//

				if (keyword != "")
				{
					data.Keyword = keyword;
				}

				#region Search_變數
				if (Search_Type != 0)
				{
					data.Search_Type = Search_Type;
				}
				if (Search_Pattern != "0")
				{
					data.Search_Pattern = Search_Pattern;
				}
				if (Search_Season != 0)
				{
					data.Search_Season = Search_Season;
				}
				if (Search_Color != 0)
				{
					data.Search_Color = Search_Color;
				}
				if (Search_Style != 0)
				{
					data.Search_Style = Search_Style;
				}
				if (Search_Fit != 0)
				{
					data.Search_Fit = Search_Fit;
				}
				if (Search_Status != 0)
				{
					data.Search_Status = Search_Status;
				}
				if (Search_SpecialOffer != 0)
				{
					data.Search_SpecialOffer = Search_SpecialOffer;
				}
				if (Search_CompanyId != 0)
				{
					data.Search_CompanyId = Search_CompanyId;
				}
				#endregion

				if (CreateTime_St != null)
				{
					data.CreateTime_St = CreateTime_St;
				}
				if (CreateTime_End != null)
				{
					data.CreateTime_End = CreateTime_End;
				}
				data.TypeSort = TypeSort;

				bool IsLimitOrder = true;
				string GoodsKind = db.Company.FirstOrDefault(f => f.Id == Search_CompanyId) != null ? db.Company.FirstOrDefault(f => f.Id == Search_CompanyId).GoodsKind : "3";
				if (!GoodsKind.Contains("3"))
				{
					IsLimitOrder = false;
				}

				//---------------匯出條件搜索end---------------//
				return File(data.Set_Excel_Company(data.Export_Data_Company(), IsLimitOrder), "application/vnd.ms-excel", "匯出商品資料.xls");
			}
			catch (Exception ex)
			{

				throw;
			}
			finally
			{
				db.Connection.Close();
			}
		}

		#endregion

		#region Ajax


		[HttpPost]
		public ActionResult Check_SN(string SN)
		{
			MithDataContext db = new MithDataContext();

			List<GoodsSN_Setting> PicList = db.GoodsSN_Setting.Where(f => f.SN == SN) != null ? db.GoodsSN_Setting.Where(f => f.SN == SN).ToList() : null;

			int Count = db.Goods.Where(w => w.SN == SN).Count();

			db.Connection.Close();
			return Json(new { PicList = PicList, Count = Count }, JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public PartialViewResult Ajax_DeletePic(int[] delete, int GoodsId = 0, int Show = 0)
		{
			MithDataContext db = new MithDataContext();
			GoodsModel data = new GoodsModel();
			try
			{
				if (delete != null)
				{
					for (int i = 0; i < delete.Count(); i++)
					{
						data.DeletePic(delete[i], 0);
					}
				}

				var GoodsPic = db.GoodsPic.Where(w => w.GoodsId == GoodsId);
				if (GoodsPic.Any())
				{
					foreach (var item in GoodsPic)
					{
						if (item.Id == Show)
						{
							item.Show = true;
						}
						else
						{
							item.Show = false;
						}
						db.SubmitChanges();
					}
				}

			}
			catch
			{
			}
			finally
			{
				ViewData["Get_PicList"] = db.GoodsPic.Where(w => w.GoodsId == GoodsId).ToList();
				db.Connection.Close();

				if (Request.Cookies["Cookie"] != null)
				{
					ViewData["Delete"] = "儲存成功！";
				}
				else
				{
					Response.Cookies["Cookie"].Value = Method.RandomKey(20, true, true, true, false);
					Response.Cookies["Cookie"].Expires = DateTime.UtcNow.AddHours(8).AddMinutes(1);
				}
			}

			return PartialView("_PartialImageList");
		}

		//更新商品狀態
		[HttpPost]
		public ActionResult Update_Status(string Id, int Status)
		{
			try
			{
				MithDataContext db = new MithDataContext();
				GoodsModel data = new GoodsModel();
				var item = Id.Split(",");
				for (int i = 0; i < item.Count(); i++)
				{

					//前台點擊覆核
					if (Status == 2)
					{
						//只有狀態為待覆核的商品能覆核
						var Goods = db.Goods.FirstOrDefault(f => f.Id == int.Parse(item[i]) && f.Status == 1);
						if (Goods != null)
						{
							Goods.Status = Status;
						}
					}
					//前台點擊上架
					else if (Status == 3)
					{
						//只有狀態為覆核的商品能上架
						var Goods = db.Goods.FirstOrDefault(f => f.Id == int.Parse(item[i]) && f.Status == 2 || f.Status == -1);
						if (Goods != null)
						{
							Goods.Status = Status;
						}
					}
					//前台點擊下架
					else if (Status == -1)
					{
						//只有狀態為上架的商品能下架
						var Goods = db.Goods.FirstOrDefault(f => f.Id == int.Parse(item[i]) && f.Status == 3);
						if (Goods != null)
						{
							Goods.Status = Status;
						}
					}
					//刪除商品
					else if (Status == 999)
					{
						var Goods = db.Goods.FirstOrDefault(f => f.Id == int.Parse(item[i]) && f.Status == 1);
						if (Goods != null)
						{
							data.Delete(int.Parse(item[i]));
						}
					}
					db.SubmitChanges();
				}

				db.Connection.Close();
				return Content("Success");
			}
			catch { return Content("Failure "); }
		}
		#endregion
	}
}

