﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;
using System.Text;
using System.IO;
using Mith.Models;
using Mith.Areas.GoX.Models;

namespace Mith.Areas.GoX.Controllers
{
    public class ShippingGoodsController : CategoryController
    {
        int fun_id = 12;

        #region 首頁
        [MyAuthorize(Com = Competence.Read, function = "出貨資料")]
				public ActionResult Index_Mith(string keyword = "",int Search_LogisticsStatus = 0, int Search_CompanyId = 0, DateTime? ShippingTime_St = null, DateTime? ShippingTime_End = null, int p = 1, int show_number = 8)
        {
            ShippingGoodsModel data = new ShippingGoodsModel();
            var fun = FunctionModel.Get_One(fun_id);
            string get = "";
            if (keyword != "")
            {
                data.Keyword = keyword;
                ViewData["keyword"] = keyword;
                get += Method.Get_URLGet("keyword", keyword);
            }

						if (Search_LogisticsStatus != 0)
						{
							data.Search_LogisticsStatus = Search_LogisticsStatus;
							ViewData["Search_LogisticsStatus"] = Search_LogisticsStatus;
							get += Method.Get_URLGet("Search_LogisticsStatus", Search_LogisticsStatus.ToString());
						}
            if (Search_CompanyId > 0)
            {
                data.Search_CompanyId = Search_CompanyId;
                ViewData["Search_CompanyId"] = Search_CompanyId;
                get += Method.Get_URLGet("Search_CompanyId", Search_CompanyId.ToString());
            }
            if (ShippingTime_St != null)
            {
                data.ShippingTime_St = ShippingTime_St;
                ViewData["ShippingTime_St"] = ShippingTime_St;
                get += Method.Get_URLGet("ShippingTime_St", ShippingTime_St.Value.ToString("yyyy/MM/dd HH:mm:ss"));
            }
            if (ShippingTime_End != null)
            {
                data.ShippingTime_End = ShippingTime_End;
                ViewData["ShippingTime_End"] = ShippingTime_End;
                get += Method.Get_URLGet("ShippingTime_End", ShippingTime_End.Value.ToString("yyyy/MM/dd HH:mm:ss"));
            }

            ViewBag.Title = fun.Name;
            ViewBag.Title_link = "物流管理 > <a href=\"\">" + fun.Name + "</a>";

            ViewData["p"] = p;
            ViewData["page"] = data.Get_Page(p, show_number);
            ViewData["number"] = show_number;
            ViewData["get"] = get;
            Session["Url"] = Request.Url.AbsoluteUri;
            var Result = data.Get_Data(p, show_number);
            return View(Result);
        }

        [MyAuthorize(Com = Competence.Read, function = "出貨資料")]
        public ActionResult Index_Company(string keyword = "", int Search_LogisticsStatus = 0, DateTime? ShippingTime_St = null, DateTime? ShippingTime_End = null, int p = 1, int show_number = 8)
        {
            ShippingGoodsModel data = new ShippingGoodsModel();
            var fun = FunctionModel.Get_One(fun_id);
            string get = "";
            if (keyword != "")
            {
                data.Keyword = keyword;
                ViewData["keyword"] = keyword;
                get += Method.Get_URLGet("keyword", keyword);
            }

						if (Search_LogisticsStatus != 0)
						{
							data.Search_LogisticsStatus = Search_LogisticsStatus;
							ViewData["Search_LogisticsStatus"] = Search_LogisticsStatus;
							get += Method.Get_URLGet("Search_LogisticsStatus", Search_LogisticsStatus.ToString());
						}

            if (ShippingTime_St != null)
            {
                data.ShippingTime_St = ShippingTime_St;
                ViewData["ShippingTime_St"] = ShippingTime_St;
                get += Method.Get_URLGet("ShippingTime_St", ShippingTime_St.Value.ToString("yyyy/MM/dd HH:mm:ss"));
            }
            if (ShippingTime_End != null)
            {
                data.ShippingTime_End = ShippingTime_End;
                ViewData["ShippingTime_End"] = ShippingTime_End;
                get += Method.Get_URLGet("ShippingTime_End", ShippingTime_End.Value.ToString("yyyy/MM/dd HH:mm:ss"));
            }
            //廠商編號
            data.Search_CompanyId = Mith.Models.Method.Get_CompanyId_Admin(Request.Cookies, Session);

            ViewBag.Title = fun.Name;
            ViewBag.Title_link = "物流管理 > <a href=\"\">" + fun.Name + "</a>";

            ViewData["p"] = p;
            ViewData["page"] = data.Get_Page(p, show_number);
            ViewData["number"] = show_number;
            ViewData["get"] = get;
            Session["Url"] = Request.Url.AbsoluteUri;

            var Result = data.Get_Data(p, show_number);
            return View(Result);
        }

        #endregion

        #region 編輯

        [MyAuthorize(Com = Competence.Update, function = "出貨資料")]
        public ActionResult Edit_Mith(string LogisticsSN = "")
        {
            MithDataContext db = new MithDataContext();
            ShippingGoodsModel data = new ShippingGoodsModel();
            var fun = FunctionModel.Get_One(fun_id);

            var item = data.Get_One(LogisticsSN);
            if (item == null)
            {
                return RedirectToAction("Index_Mith");
            }
            db.Connection.Close();
            ViewBag.Title = "物流管理 > " + fun.Name + " > 詳情 ";
            ViewBag.Title_link = "物流管理 > <a href=/Gox/ShippingGoods/Index_Mith>" + fun.Name + "</a> > 詳情";
            return View("Edit_Mith", item);

        }

        [MyAuthorize(Com = Competence.Update, function = "出貨資料")]
        public ActionResult Edit_Company(string LogisticsSN = "")
        {
            MithDataContext db = new MithDataContext();
            ShippingGoodsModel data = new ShippingGoodsModel();
            var fun = FunctionModel.Get_One(fun_id);

            var item = data.Get_One(LogisticsSN);
            if (item == null)
            {
                return RedirectToAction("Index_Mith");
            }
            string CompanyTel = db.Company.FirstOrDefault(f => f.Name == item.Company_Name).Tel;
            if (!string.IsNullOrWhiteSpace(CompanyTel))
            {
                ViewData["CompanyTel"] = "0000000" + CompanyTel.Substring(CompanyTel.Length - 3, 3);
            }
            else
            {
                ViewData["CompanyTel"] = "0000000000";
            }

            db.Connection.Close();
            ViewBag.Title = "物流管理 > " + fun.Name + " > 詳情 ";
            ViewBag.Title_link = "物流管理 > <a href=/Gox/ShippingGoods/Index_Company>" + fun.Name + "</a> > 詳情";
            return View("Edit_Company", item);

        }

        /// <summary>
        /// 出貨
        /// </summary>
        [ValidateInput(true)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult ShippingGoods(string ShippingGoodsVal = "", int LogisticsType = 0, string Remark = "", string LogisticsSN = "", string Total = "0")
        {
            MithDataContext db = new MithDataContext();
            ShippingGoodsModel data = new ShippingGoodsModel();
            try
            {
                string PackageSN = null;
                var item = ShippingGoodsVal.Split(",");
                int OrderId = 0;
                int LastUserId = Method.Get_UserId_Admin(Request.Cookies, Session);
                for (int i = 0; i < item.Count(); i++)
                {
                    //只有狀態為備貨的訂單商品能出貨
                    var OrderDetail = db.OrderDetail.FirstOrDefault(w => w.Id == int.Parse(item[i]) && w.LogisticsStatus == 1);

                    if (i == 0)
                    {
                        if (LogisticsType == 2)
                        {
                            LogisticsSN = "711" + Method.RandomKey(7, true, false, false, false);
                            PackageSN = ShippingGoodsModel.Get_711_PackegeSN(LogisticsSN, OrderDetail.Id, Total);
                        }
                        OrderId = OrderDetail.OrderId;
                    }

                    OrderDetail.LogisticsStatus = 2;
                    OrderDetail.LogisticsSN = LogisticsSN;
                    if (!string.IsNullOrWhiteSpace(PackageSN))
                    {
                        OrderDetail.PackageSN = PackageSN;
                    }
                    if (!string.IsNullOrWhiteSpace(Remark))
                    {
                        OrderDetail.Remark = HttpUtility.HtmlDecode(Remark);
                    }
                    OrderDetail.ShippingTime = DateTime.UtcNow;
                    OrderDetail.LastUserId = LastUserId;
                    db.SubmitChanges();
                }

                //訂單底下是否還有未出貨的商品
                int NotShippingGoods = db.OrderDetail.Where(w => w.OrderId == OrderId && w.LogisticsStatus < 2).Count();
                //商品皆已出貨，訂單狀態=已結單
                if (NotShippingGoods == 0)
                {
                    var Order = db.Order.FirstOrDefault(f => f.Id == OrderId);
                    Order.Status = 2;
                    db.SubmitChanges();
                }

                return Redirect("");
            }
            catch (Exception e)
            {
                return Content("Error");
            }
            finally
            {
                db.Connection.Close();
            }

        }


        /// <summary>
        /// 退貨
        /// </summary>
        /// <param name="ReturnGoodsVal">退貨商品，字串為：訂單編號=退貨數量,</param>
        [ValidateInput(true)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult ReturnGoods(int CityId = 0, int AreaId = 0, string ReceiverAddress = "",
            int ReceiveTimeRange = 0, string Remark = "", string LogisticsSN = "", string ReturnGoodsVal = "")
        {
            MithDataContext db = new MithDataContext();
            ShippingGoodsModel data = new ShippingGoodsModel();
            try
            {
                //先拆逗號
                var item = ReturnGoodsVal.Split(",");
                for (int i = 0; i < item.Count(); i++)
                {
                    //再拆等於（訂單編號：[0]　退貨數量：[1]）
                    var item2 = item[i].Split("=");
                    int OrderDetailId = int.Parse(item2[0]); //訂單編號
                    int BuyQuantity = int.Parse(item2[1]); // 退貨數量

                    var Old_OrderDetail = db.OrderDetail.FirstOrDefault(f => f.Id == OrderDetailId);
                    //商品總額
                    int TotalPrice = 0;
                    #region 迴圈第一次，新增退貨的訂單主檔
                    if (i == 0)
                    {
                        int LastUserId = Method.Get_UserId_Admin(Request.Cookies, Session);

                        Session["New_OrderId"] = data.Insert_ReturnGoods_Order(CityId, AreaId, ReceiverAddress, ReceiveTimeRange, Remark, Old_OrderDetail.OrderId, LastUserId);
                    }
                    #endregion

                    #region 新增退貨的訂單細節
                    int BuyTotal = 0;

                    BuyTotal = Old_OrderDetail.BuyPrice * BuyQuantity;

                    var New_OrderDetail = new OrderDetail()
                    {
                        OrderId = (int)Session["New_OrderId"],
                        GoodsId = Old_OrderDetail.GoodsId,
                        CompanyId = Old_OrderDetail.CompanyId,
                        BrandId = Old_OrderDetail.BrandId,
                        BuySize = Old_OrderDetail.BuySize,
                        BuyQuantity = BuyQuantity,
                        BuyPrice = Old_OrderDetail.BuyPrice,
                        BuyTotal = BuyTotal,
                        LogisticsSN = LogisticsSN,
                        LogisticsCompany = Old_OrderDetail.LogisticsCompany,
                        LogisticsStatus = 3, //退貨中
                        ShippingTime = DateTime.UtcNow,
                        UpdateTime = DateTime.UtcNow,
                        LastUserId = 0,
                    };
                    db.OrderDetail.InsertOnSubmit(New_OrderDetail);
                    db.SubmitChanges();

                    TotalPrice += BuyTotal;
                    #endregion

                    #region 計算訂單總和
                    var New_Order = db.Order.FirstOrDefault(f => f.Id == (int)Session["New_OrderId"]);
                    if (New_Order != null)
                    {
                        New_Order.Total = TotalPrice;
                        db.SubmitChanges();
                    }

                    #endregion
                }

                return Redirect("");
            }
            catch (Exception e)
            {
                //刪掉訂單
                WebApiMethod.Delete_Order((int)Session["New_OrderId"]);
                return Content("Error");
            }
            finally
            {
                db.Connection.Close();
            }

        }

        #endregion

        #region 刪除

        [MyAuthorize(Com = Competence.Delete, function = "出貨資料")]
        public ActionResult Delete(int item, int p = 1)
        {
            ShippingGoodsModel data = new ShippingGoodsModel();
            data.Delete(item);

            return RedirectToAction("Index", new { p = p });
        }

        [MyAuthorize(Com = Competence.Delete, function = "出貨資料")]
        [HttpPost]
        public ActionResult Delete(int[] item)
        {
            ShippingGoodsModel data = new ShippingGoodsModel();
            data.Delete(item);

            return RedirectToAction("Index");
        }

        #endregion

        #region Ajax
        public PartialViewResult Get_SelectGoosList(int[] Id, string GoodsAction)
        {
            MithDataContext db = new MithDataContext();
            List<ShippingGoodsModel.OrderDeatailShow> SelectGoosList = new List<ShippingGoodsModel.OrderDeatailShow>();
            try
            {
                if (Id.Any() && Id != null)
                {
                    for (int i = 0; i < Id.Count(); i++)
                    {
                        var OrderDetail = db.OrderDetail_Index_View.FirstOrDefault(f => f.Id == Id[i]);
                        SelectGoosList.Add(new ShippingGoodsModel.OrderDeatailShow()
                        {
                            Id = OrderDetail.Id,
                            Goods_SN = OrderDetail.Goods_SN,
                            Goods_Name = OrderDetail.Goods_Name,
                            Goods_ColorName = OrderDetail.Goods_ColorName,
                            BuySize = OrderDetail.BuySize,
                            BuyQuantity = OrderDetail.BuyQuantity
                        });
                    }
                    ViewData["SelectGoosList"] = SelectGoosList;
                    ViewData["GoodsAction"] = GoodsAction;
                }
            }
            catch
            {
            }
            finally
            {
                db.Connection.Close();
            }
            return PartialView("_SelectGoosList");
        }

        //更新訂單狀態
        [HttpPost]
        public ActionResult Update_Status(string OrderId = "", string OrderDetailId = "", int Status = 0)
        {
            try
            {
                MithDataContext db = new MithDataContext();
                GoodsModel data = new GoodsModel();
                //從首頁(Index)來
                if (OrderId != "")
                {
                    var item = OrderId.Split(",");
                    for (int i = 0; i < item.Count(); i++)
                    {
                        //前台點擊備貨
                        if (Status == 1)
                        {
                            //只有狀態為未接單的訂單能備貨
                            var OrderDetail = db.OrderDetail.Where(w => w.OrderId == int.Parse(item[i]) && w.LogisticsStatus == -1);
                            foreach (var item2 in OrderDetail)
                            {
                                item2.LogisticsStatus = Status;
                                db.SubmitChanges();
                            }
                        }

                    }
                }
                //從編輯(Edit)來
                if (OrderDetailId != "")
                {
                    var item = OrderDetailId.Split(",");
                    for (int i = 0; i < item.Count(); i++)
                    {
                        //前台點擊備貨
                        if (Status == 1)
                        {
                            //只有狀態為未接單的訂單能備貨
                            var OrderDetail = db.OrderDetail.FirstOrDefault(w => w.Id == int.Parse(item[i]) && w.LogisticsStatus == -1);

                            OrderDetail.LogisticsStatus = Status;
                            db.SubmitChanges();
                        }

                    }
                }
                db.Connection.Close();
                return Content("Success");
            }
            catch { return Content("Failure "); }
        }
        #endregion

        //[HttpPost]
        //public ActionResult Export(string keyword = "")
        //{
        //    ShippingGoodsModel data = new ShippingGoodsModel();
        //    //---------------匯出條件搜索---------------//

        //    if (keyword != null)
        //    {
        //        data.Keyword = keyword;
        //    }
        //    //---------------匯出條件搜索end---------------//
        //    return File(data.Set_Excel(data.Export_Data(data)), "application/vnd.ms-excel", "出貨資料.xls");
        //}
    }
}

