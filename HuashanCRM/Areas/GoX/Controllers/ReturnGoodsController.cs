﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;
using System.Text;
using System.IO;
using Mith.Models;
using Mith.Areas.GoX.Models;

namespace Mith.Areas.GoX.Controllers
{
    public class ReturnGoodsController : CategoryController
    {
        int fun_id = 13;

        #region 首頁
        [MyAuthorize(Com = Competence.Read, function = "退貨資料")]
				public ActionResult Index_Mith(string keyword = "", int Search_LogisticsStatus = 0,int Search_AgreeRefund = 0, int Search_NotLogisticsSN = 0, int Search_CompanyId = 0, DateTime? CreateTime_St = null, DateTime? CreateTime_End = null, int p = 1, int show_number = 8)
        {
            ReturnGoodsModel data = new ReturnGoodsModel();
            var fun = FunctionModel.Get_One(fun_id);
            string get = "";
            if (keyword != "")
            {
                data.Keyword = keyword;
                ViewData["keyword"] = keyword;
                get += Method.Get_URLGet("keyword", keyword);
            }
						if (Search_LogisticsStatus != 0)
            {
							data.Search_LogisticsStatus = Search_LogisticsStatus;
							ViewData["Search_LogisticsStatus"] = Search_LogisticsStatus;
							get += Method.Get_URLGet("Search_LogisticsStatus", Search_LogisticsStatus.ToString());
            }
            if (Search_AgreeRefund != 0)
            {
                data.Search_AgreeRefund = Search_AgreeRefund;
                ViewData["Search_AgreeRefund"] = Search_AgreeRefund;
                get += Method.Get_URLGet("Search_AgreeRefund", Search_AgreeRefund.ToString());
            }
            if (Search_NotLogisticsSN != 0)
            {
                data.Search_NotLogisticsSN = Search_NotLogisticsSN;
                ViewData["Search_NotLogisticsSN"] = Search_NotLogisticsSN;
                get += Method.Get_URLGet("Search_NotLogisticsSN", Search_NotLogisticsSN.ToString());
            }
            if (Search_CompanyId > 0)
            {
                data.Search_CompanyId = Search_CompanyId;
                ViewData["Search_CompanyId"] = Search_CompanyId;
                get += Method.Get_URLGet("Search_CompanyId", Search_CompanyId.ToString());
            }
            if (CreateTime_St != null)
            {
                data.CreateTime_St = CreateTime_St;
                ViewData["CreateTime_St"] = CreateTime_St;
                get += Method.Get_URLGet("CreateTime_St", CreateTime_St.Value.ToString("yyyy/MM/dd HH:mm:ss"));
            }
            if (CreateTime_End != null)
            {
                data.CreateTime_End = CreateTime_End;
                ViewData["CreateTime_End"] = CreateTime_End;
                get += Method.Get_URLGet("CreateTime_End", CreateTime_End.Value.ToString("yyyy/MM/dd HH:mm:ss"));
            }

            ViewBag.Title = fun.Name;
            ViewBag.Title_link = "物流管理 > <a href=\"\">" + fun.Name + "</a>";

            ViewData["p"] = p;
            ViewData["page"] = data.Get_Page(p, show_number);
            ViewData["number"] = show_number;
            ViewData["get"] = get;
            Session["Url"] = Request.Url.AbsoluteUri;
            var Result = data.Get_Data(p, show_number);
            return View(Result);
        }

        [MyAuthorize(Com = Competence.Read, function = "退貨資料")]
				public ActionResult Index_Company(string keyword = "", int Search_LogisticsStatus = 0, int Search_AgreeRefund = 0, int Search_NotLogisticsSN = 0, DateTime? CreateTime_St = null, DateTime? CreateTime_End = null,
            int p = 1, int show_number = 8)
        {
            ReturnGoodsModel data = new ReturnGoodsModel();
            var fun = FunctionModel.Get_One(fun_id);
            string get = "";
            if (keyword != "")
            {
                data.Keyword = keyword;
                ViewData["keyword"] = keyword;
                get += Method.Get_URLGet("keyword", keyword);
            }
						if (Search_LogisticsStatus != 0)
						{
							data.Search_LogisticsStatus = Search_LogisticsStatus;
							ViewData["Search_LogisticsStatus"] = Search_LogisticsStatus;
							get += Method.Get_URLGet("Search_LogisticsStatus", Search_LogisticsStatus.ToString());
						}
            if (Search_AgreeRefund != 0)
            {
                data.Search_AgreeRefund = Search_AgreeRefund;
                ViewData["Search_AgreeRefund"] = Search_AgreeRefund;
                get += Method.Get_URLGet("Search_AgreeRefund", Search_AgreeRefund.ToString());
            }
            if (Search_NotLogisticsSN != 0)
            {
                data.Search_NotLogisticsSN = Search_NotLogisticsSN;
                ViewData["Search_NotLogisticsSN"] = Search_NotLogisticsSN;
                get += Method.Get_URLGet("Search_NotLogisticsSN", Search_NotLogisticsSN.ToString());
            }
            if (CreateTime_St != null)
            {
                data.CreateTime_St = CreateTime_St;
                ViewData["CreateTime_St"] = CreateTime_St;
                get += Method.Get_URLGet("CreateTime_St", CreateTime_St.Value.ToString("yyyy/MM/dd HH:mm:ss"));
            }
            if (CreateTime_End != null)
            {
                data.CreateTime_End = CreateTime_End;
                ViewData["CreateTime_End"] = CreateTime_End;
                get += Method.Get_URLGet("CreateTime_End", CreateTime_End.Value.ToString("yyyy/MM/dd HH:mm:ss"));
            }
            //廠商編號
            data.Search_CompanyId = Mith.Models.Method.Get_CompanyId_Admin(Request.Cookies, Session);

            ViewBag.Title = fun.Name;
            ViewBag.Title_link = "物流管理 > <a href=\"\">" + fun.Name + "</a>";

            ViewData["p"] = p;
            ViewData["page"] = data.Get_Page(p, show_number);
            ViewData["number"] = show_number;
            ViewData["get"] = get;
            Session["Url"] = Request.Url.AbsoluteUri;

            var Result = data.Get_Data(p, show_number);
            return View(Result);
        }

        #endregion

        #region 編輯

        [MyAuthorize(Com = Competence.Update, function = "退貨資料")]
        public ActionResult Edit_Mith(int Id = 0)
        {
            MithDataContext db = new MithDataContext();
            ReturnGoodsModel data = new ReturnGoodsModel();
            var fun = FunctionModel.Get_One(fun_id);

            var item = data.Get_One(Id);
            if (item == null)
            {
                return RedirectToAction("Index_Mith");
            }
            db.Connection.Close();
            ViewBag.Title = "物流管理 > " + fun.Name + " > 詳情 ";
            ViewBag.Title_link = "物流管理 > <a href=/Gox/ReturnGoods/Index_Mith>" + fun.Name + "</a> > 詳情";
            return View("Edit_Mith", item);

        }

        [MyAuthorize(Com = Competence.Update, function = "退貨資料")]
        public ActionResult Edit_Company(int Id = 0)
        {
            MithDataContext db = new MithDataContext();
            ReturnGoodsModel data = new ReturnGoodsModel();
            var fun = FunctionModel.Get_One(fun_id);

            var item = data.Get_One(Id);
            if (item == null)
            {
                return RedirectToAction("Index_Mith");
            }
            string CompanyTel = db.Company.FirstOrDefault(f => f.Name == item.Company_Name).Tel;
            if (!string.IsNullOrWhiteSpace(CompanyTel))
            {
                ViewData["CompanyTel"] = "0000000" + CompanyTel.Substring(CompanyTel.Length - 3, 3);
            }
            else
            {
                ViewData["CompanyTel"] = "0000000000";
            }

            db.Connection.Close();
            ViewBag.Title = "物流管理 > " + fun.Name + " > 詳情 ";
            ViewBag.Title_link = "物流管理 > <a href=/Gox/ReturnGoods/Index_Company>" + fun.Name + "</a> > 詳情";
            return View("Edit_Company", item);

        }

        [MyAuthorize(Com = Competence.Update, function = "退貨資料")]
        [ValidateInput(true)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Edit(ReturnGoodsModel.ReturnGoodsShow item)
        {
            ReturnGoodsModel data = new ReturnGoodsModel();
            var fun = FunctionModel.Get_One(fun_id);
            string url = (string)Session["Url"];
            int user_id = Method.Get_UserId_Admin(Request.Cookies, Session);
            item.LastUserId = user_id;

            if (data.Update(item) <= 0)
            {
                ErrorRecordModel.Error_Record(Method.Get_UserId_Admin(Request.Cookies, Session), "ReturnGoods_5", Request.UserAgent, true, "資料新增失敗");
                TempData["err"] = "ReturnGoods_5";
                return RedirectToAction("Index");
            }

            TempData["FromAction"] = "儲存成功！";
            if (!string.IsNullOrWhiteSpace(url))
            {
                return Redirect(url);
            }
            else
            {
                return RedirectToAction("Index_Company");
            }
        }

        #endregion

        #region 刪除

        [MyAuthorize(Com = Competence.Delete, function = "退貨資料")]
        public ActionResult Delete(int item, int p = 1)
        {
            ReturnGoodsModel data = new ReturnGoodsModel();
            data.Delete(item);

            return RedirectToAction("Index", new { p = p });
        }

        [MyAuthorize(Com = Competence.Delete, function = "退貨資料")]
        [HttpPost]
        public ActionResult Delete(int[] item)
        {
            ReturnGoodsModel data = new ReturnGoodsModel();
            data.Delete(item);

            return RedirectToAction("Index");
        }

        #endregion

        #region Ajax
        public PartialViewResult Get_SelectGoosList(int[] Id, string GoodsAction)
        {
            MithDataContext db = new MithDataContext();
            List<ReturnGoodsModel.OrderDeatailShow> SelectGoosList = new List<ReturnGoodsModel.OrderDeatailShow>();
            try
            {
                if (Id.Any() && Id != null)
                {
                    for (int i = 0; i < Id.Count(); i++)
                    {
                        var OrderDetail = db.OrderDetail_Index_View.FirstOrDefault(f => f.Id == Id[i]);
                        SelectGoosList.Add(new ReturnGoodsModel.OrderDeatailShow()
                        {
                            Id = OrderDetail.Id,
                            Goods_SN = OrderDetail.Goods_SN,
                            Goods_Name = OrderDetail.Goods_Name,
                            Goods_ColorName = OrderDetail.Goods_ColorName,
                            BuySize = OrderDetail.BuySize,
                            BuyQuantity = OrderDetail.BuyQuantity
                        });
                    }
                    ViewData["SelectGoosList"] = SelectGoosList;
                    ViewData["GoodsAction"] = GoodsAction;
                }
            }
            catch
            {
            }
            finally
            {
                db.Connection.Close();
            }
            return PartialView("_SelectGoosList");
        }

        //更新訂單狀態
        [HttpPost]
        public ActionResult Update_Status(string OrderId = "", string OrderDetailId = "", int Status = 0)
        {
            try
            {
                MithDataContext db = new MithDataContext();
                GoodsModel data = new GoodsModel();
                //從首頁(Index)來
                if (OrderId != "")
                {
                    var item = OrderId.Split(",");
                    for (int i = 0; i < item.Count(); i++)
                    {
                        //前台點擊備貨
                        if (Status == 1)
                        {
                            //只有狀態為未接單的訂單能備貨
                            var OrderDetail = db.OrderDetail.Where(w => w.OrderId == int.Parse(item[i]) && w.LogisticsStatus == -1);
                            foreach (var item2 in OrderDetail)
                            {
                                item2.LogisticsStatus = Status;
                                db.SubmitChanges();
                            }
                        }

                    }
                }
                //從編輯(Edit)來
                if (OrderDetailId != "")
                {
                    var item = OrderDetailId.Split(",");
                    for (int i = 0; i < item.Count(); i++)
                    {
                        //前台點擊備貨
                        if (Status == 1)
                        {
                            //只有狀態為未接單的訂單能備貨
                            var OrderDetail = db.OrderDetail.FirstOrDefault(w => w.Id == int.Parse(item[i]) && w.LogisticsStatus == -1);

                            OrderDetail.LogisticsStatus = Status;
                            db.SubmitChanges();
                        }

                    }
                }
                db.Connection.Close();
                return Content("Success");
            }
            catch { return Content("Failure "); }
        }
        #endregion

        //[HttpPost]
        //public ActionResult Export(string keyword = "")
        //{
        //    ReturnGoodsModel data = new ReturnGoodsModel();
        //    //---------------匯出條件搜索---------------//

        //    if (keyword != null)
        //    {
        //        data.Keyword = keyword;
        //    }
        //    //---------------匯出條件搜索end---------------//
        //    return File(data.Set_Excel(data.Export_Data(data)), "application/vnd.ms-excel", "退貨資料.xls");
        //}
    }
}

