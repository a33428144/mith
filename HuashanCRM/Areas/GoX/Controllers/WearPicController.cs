﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text;
using System.IO;
using Ionic.Zip;
using System.IO.Compression;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Mith.Models;
using Mith.Areas.GoX.Models;

namespace Mith.Areas.GoX.Controllers
{
    public class WearPicController : CategoryController
    {
        int fun_id = 22;

        [RoleAuthorize(Role = "亞設人員")]
        [MyAuthorize(Com = Competence.Read, function = "配件圖片")]
        public ActionResult Index(string keyword = "", string sort = "", string Search_Type = "0", int Search_Display = 0, DateTime? CreateTime_St = null, DateTime? CreateTime_End = null, int p = 1, int show_number = 10)
        {
            WearPicModel data = new WearPicModel();
            var fun = FunctionModel.Get_One(fun_id);

            ViewBag.Title = fun.Name;
            ViewBag.Title_link = "活動管理 > <a href=\"\">" + fun.Name + " (配件、背景、人偶)" + "</a>";

            string get = "";

            if (keyword != "")
            {
                data.Keyword = keyword;
                ViewData["keyword"] = keyword;
                get += Method.Get_URLGet("keyword", keyword);
            }

            if (Search_Type != "0")
            {
                data.Search_Type = Search_Type;
                ViewData["Search_Type"] = Search_Type;
                get += Method.Get_URLGet("Search_Type", Search_Type.ToString());
            }
            if (Search_Display != 0)
            {

                data.Search_Display = Search_Display;
                ViewData["Search_Display"] = Search_Display;
                get += Method.Get_URLGet("Search_Display", Search_Display.ToString());
            }
            if (CreateTime_St != null)
            {
                data.CreateTime_St = CreateTime_St;
                ViewData["CreateTime_St"] = CreateTime_St;
                get += Method.Get_URLGet("CreateTime_St", CreateTime_St.Value.ToString("yyyy/MM/dd HH:mm:ss"));
            }
            if (CreateTime_End != null)
            {
                data.CreateTime_End = CreateTime_End;
                ViewData["CreateTime_End"] = CreateTime_End;
                get += Method.Get_URLGet("CreateTime_End", CreateTime_End.Value.ToString("yyyy/MM/dd HH:mm:ss"));
            }
            data.Sort = sort;
            get += Method.Get_URLGet("sort", sort);

            ViewData["p"] = p;
            ViewData["page"] = data.Get_Page(p, show_number);
            ViewData["keyword"] = keyword;
            ViewData["get"] = get;
            Session["Url"] = Request.Url.AbsoluteUri;
            return View(data.Get_Data(p, show_number));
        }

        [MyAuthorize(Com = Competence.Insert, function = "配件圖片")]
        public ActionResult Create()
        {
            WearPicModel data = new WearPicModel();
            var fun = FunctionModel.Get_One(fun_id);

            ViewBag.Title = fun.Name + " > 新增";
						ViewBag.Title_link = "活動管理 > <a href=\"Index\">" + fun.Name + " (配件、背景、人偶)" + "</a> > 新增";
            ViewBag.c_title = fun.Name + "資料";

            return View();
        }

        [ValidateInput(true)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Create(WearPicModel.WearPicShow item, List<HttpPostedFileBase> Files = null)
        {
            WearPicModel data = new WearPicModel();
            var fun = FunctionModel.Get_One(fun_id);
            if (ModelState.IsValid)
            {
                #region 新增圖片 ( 複選多張 )
                if (Files.Any() && Files[0] != null)
                {
                    foreach (var i in Files)
                    {
                        string SN = i.FileName.Substring(0, 11);
                        string Pic_Remark = i.FileName.Substring(10, 1);
                        string Pic_Type = i.FileName.Substring(11, 1);
                        string Path_Big = null;
                        string Path_Middle = null;
                        string Path_Small = null;
                        string sContainer = "wear";
                        string sImagePath = "Big/" + SN + "/";
                        string FileExtension = System.IO.Path.GetExtension(i.FileName);

                        string name = Method.RandomKey(4, true, false, false, false) + Pic_Remark + Pic_Type;

                        Size Middle = new Size();
                        Middle = new Size(540, 960);
                        Size Small = new Size();
                        Small = new Size(360, 640);

                        if (Pic_Type == "1")
                        {
                            Path_Big = BlobHelper.Upload_Blob(new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + FileExtension }, i.InputStream, name + FileExtension, Middle, Small, false)[0];
                        }
                        else
                        {
                            Path_Big = BlobHelper.Upload_Blob(new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + FileExtension }, i.InputStream, name + FileExtension, Middle, Small, true)[0];
                            Path_Middle = Path_Big.Replace("Big", "Middle");
                            Path_Small = Path_Big.Replace("Big", "Small");
                        }

                        int count = data.CheckSN_Count(SN);
                        //如果Count為0，代表沒有此序號進行Insert
                        if (count == 0)
                        {
                            data.Insert_multiple(Path_Big, Path_Middle, Path_Small, SN, Pic_Type);
                        }
                        //否則進行Update
                        else
                        {
                            data.Update_multiple(Path_Big, Path_Middle, Path_Small, SN, Pic_Type);
                        }
                    }
                }
                #endregion
            }

            TempData["FromAction"] = "新增成功！";
            return RedirectToAction("Index");
        }

        [MyAuthorize(Com = Competence.Update, function = "配件圖片")]
        public ActionResult Edit(int id = 0, int p = 1)
        {
            WearPicModel data = new WearPicModel();
            var fun = FunctionModel.Get_One(fun_id);

            ViewBag.Title = fun.Name + " > 編輯";
						ViewBag.Title_link = "活動管理 > <a href=\"../Index\">" + fun.Name + " (配件、背景、人偶)" + "</a> > 編輯";
            ViewBag.c_title = fun.Name + "資料";
            ViewData["p"] = p;
            var item = data.Get_One(id);
            if (item == null)
            {
                return RedirectToAction("Create");
            }
            return View(item);
        }

        [ValidateInput(true)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Edit(WearPicModel.WearPicShow item,
        HttpPostedFileBase Pic1_Big, HttpPostedFileBase Pic2_Big, HttpPostedFileBase Pic3_Big,
            HttpPostedFileBase Pic4_Big, HttpPostedFileBase Pic5_Big, int p = 1)
        {
            string url = (string)Session["Url"];
            WearPicModel data = new WearPicModel();
            var fun = FunctionModel.Get_One(fun_id);
            item.LastUser_Id = Method.Get_UserId_Admin(Request.Cookies, Session);

            if (ModelState.IsValid)
            {
                string sContainer = "wear";
                string sImagePath = "Big/" + item.SN + "/";
                string FileExtension = "";
                string Pic_Remark = "";
                string Pic_Type = "";
                string name = Method.RandomKey(4, true, false, false, false);
                Size Middle = new Size();
                Middle = new Size(540, 960);
                Size Small = new Size();
                Small = new Size(360, 640);

                #region 更新圖片

                //點擊圖
                if (Pic1_Big != null)
                {
                    Pic_Remark = Pic1_Big.FileName.Substring(10, 1);
                    Pic_Type = Pic1_Big.FileName.Substring(11, 1);
                    name = name + Pic_Remark + Pic_Type;
                    FileExtension = System.IO.Path.GetExtension(Pic1_Big.FileName);
                    item.Pic1_Big = BlobHelper.Upload_Blob(new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + FileExtension }, Pic1_Big.InputStream, name + FileExtension, Middle, Small, false)[0];
                    data.UpdatePic(item.Pic1_Big, null, null, item.SN, "1");
                }
                //穿搭圖
                if (Pic2_Big != null)
                {
                    Pic_Remark = Pic2_Big.FileName.Substring(10, 1);
                    Pic_Type = Pic2_Big.FileName.Substring(11, 1);
                    name = name + Pic_Remark + Pic_Type;
                    FileExtension = System.IO.Path.GetExtension(Pic2_Big.FileName);
                    item.Pic2_Big = BlobHelper.Upload_Blob(new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + FileExtension }, Pic2_Big.InputStream, name + FileExtension, Middle, Small, true)[0];

                    item.Pic2_Middle = item.Pic2_Big.Replace("Big", "Middle");
                    item.Pic2_Small = item.Pic2_Big.Replace("Big", "Small");

                    data.UpdatePic(item.Pic2_Big, item.Pic2_Middle, item.Pic2_Small, item.SN, "2");
                }
                #endregion

                if (data.Update_One(item) <= 0)
                {
                    ErrorRecordModel.Error_Record(Method.Get_UserId_Admin(Request.Cookies, Session), "WearPic_5", Request.UserAgent, true, "資料新增失敗");
                    TempData["err"] = "WearPic_5";
                    return RedirectToAction("Index");
                }

            }
            TempData["FromAction"] = "儲存成功！";
            if(!string.IsNullOrWhiteSpace(url))
            {
                return Redirect(url);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        [MyAuthorize(Com = Competence.Delete, function = "配件圖片")]
        public ActionResult Delete(int item, int p = 1)
        {
            WearPicModel data = new WearPicModel();
            data.Delete(item, Server, Method.Get_UserId_Admin(Request.Cookies, Session));
            TempData["FromAction"] = "刪除成功！";
            return RedirectToAction("Index", new { p = p });
        }

        [MyAuthorize(Com = Competence.Delete, function = "配件圖片")]
        [HttpPost]
        public ActionResult Delete(int[] item)
        {

            WearPicModel data = new WearPicModel();
            data.Delete(item, Server, Method.Get_UserId_Admin(Request.Cookies, Session));
            TempData["FromAction"] = "刪除成功！";
            return RedirectToAction("Index");
        }

        #region Ajax
        [HttpPost]
        public ActionResult Ajax_Delete(string SN)
        {
            try
            {
                MithDataContext db = new MithDataContext();
                var item = SN.Split(",");
                for (int i = 0; i < item.Count(); i++)
                {
                    var data = db.WearPic.FirstOrDefault(f => f.SN == item[i]);
                    int Count = db.WearPic.Where(w => w.SN == item[i]).Count();
                    if (data != null && Count == 0)
                    {
                        BlobHelper.delete("wear" + "Big/" + SN, Path.GetFileName(data.Pic1_Big));

                        BlobHelper.delete("wear" + "Big/" + SN, Path.GetFileName(data.Pic2_Big));
                        BlobHelper.delete("wear" + "Middle/" + SN, Path.GetFileName(data.Pic2_Middle));
                        BlobHelper.delete("wear" + "Small/" + SN, Path.GetFileName(data.Pic2_Small));

                        db.WearPic.DeleteOnSubmit(data);
                        db.SubmitChanges();
                    }
                }
                db.SubmitChanges();
                db.Connection.Close();
                return Content("Success");
            }
            catch { return Content("Failure "); }
        }

        [HttpPost]
        public ActionResult Update_Display(string SN, bool Display)
        {
            try
            {
                MithDataContext db = new MithDataContext();
                var item = SN.Split(",");
                for (int i = 0; i < item.Count(); i++)
                {
                    var data = db.WearPic.FirstOrDefault(f => f.SN == item[i]);
                    if (data != null)
                    {
                        data.Display = Display;
                    }
                }
                db.SubmitChanges();
                db.Connection.Close();
                return Content("Success");
            }
            catch { return Content("Failure "); }
        }

        #endregion

        #region 多個檔案下載 & 上傳
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult MiupleUpload(HttpPostedFileBase fileData)
        {
            try
            {
                WearPicModel data = new WearPicModel();
                string SN = fileData.FileName.Substring(0, 11);
                string Pic_Remark = fileData.FileName.Substring(10, 1);
                string Pic_Type = fileData.FileName.Substring(11, 1);
                string Path_Big = null;
                string Path_Middle = null;
                string Path_Small = null;
                string sContainer = "wear";
                string sImagePath = "Big/" + SN + "/";
                string FileExtension = System.IO.Path.GetExtension(fileData.FileName);
                string name = Method.RandomKey(5, true, false, false, false) + Pic_Type;

                Size Middle = new Size();
                Middle = new Size(540, 960);
                Size Small = new Size();
                Small = new Size(360, 640);

                if (Pic_Type == "1")
                {
                    Path_Big = BlobHelper.Upload_Blob(new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + FileExtension }, fileData.InputStream, name + FileExtension, Middle, Small, false)[0];
                }
                else
                {
                    Path_Big = BlobHelper.Upload_Blob(new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + FileExtension }, fileData.InputStream, name + FileExtension, Middle, Small, true)[0];
                    Path_Middle = Path_Big.Replace("Big", "Middle");
                    Path_Small = Path_Big.Replace("Big", "Small");
                }

                int count = data.CheckSN_Count(SN);
                //如果Count為0，代表沒有此序號進行Insert
                if (count == 0)
                {
                    data.Insert_multiple(Path_Big, Path_Middle, Path_Small, SN, Pic_Type);
                }
                //否則進行Update
                else
                {
                    data.Update_multiple(Path_Big, Path_Middle, Path_Small, SN, Pic_Type);
                }

                return Json(new { Success = true });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }

        #endregion

    }
}

