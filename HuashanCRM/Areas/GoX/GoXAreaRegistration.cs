﻿using System.Web.Mvc;
using Mith.Models;

namespace Mith.Areas.GoX
{
    public class GoXAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "GoX";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                Method.Admin_default,
                "GoX/{controller}/{action}/{id}",
                new { action = "Index",controller="Admin", id = UrlParameter.Optional },
                namespaces: new string[] { "Mith.Areas.GoX.Controllers" }
            );
        }
    }
}
