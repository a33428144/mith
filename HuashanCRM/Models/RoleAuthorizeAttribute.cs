﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Mith.Models;
using Mith.Areas.GoX.Models;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;


namespace Mith.Models
{
    public class RoleAuthorizeAttribute : AuthorizeAttribute
    {

        public string Role{ get;set;}


        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (httpContext == null)
                throw new ArgumentNullException("httpContext");
            if (httpContext.Request.Cookies[Method.CookieName_Admin] == null)
            {
                return false;
            }
            HttpCookie hc = httpContext.Request.Cookies[Method.CookieName_Admin];
            string data = "";
            FormsAuthenticationTicket authTicket = Method.Get_Cookie_Ticket_Admin(httpContext.Request.Cookies);

            if (authTicket == null)
            {
                data = Method.AES(false, hc.Value, Method.AES_Key);
                if (data == "")
                {
                    return false;
                }
            }
            else
            {
                if (authTicket.Expired)
                    return false;
                data = authTicket.UserData;
            }

            string[] result = data.Split(',');
            if (result.Count() != 3)
            {
                return false;
            }

            string account = result[0];

            var user = UserProfileModel.Login_Get_One(account);

            if (user == null)
            {
                return false;
            }

            if (user.Role_Name == Role)
            {
                return true;
            }
            else {

                return false;
            }
        }

    }
}