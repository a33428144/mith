﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Mith.Models;
using System.Web.Mvc;

namespace Mith.Models
{
    public class CityArea
    {
        //public static List<SelectListItem> Get_AllCity_Select(int city, bool all)
        //{
        //    MithDataContext db = new MithDataContext();
        //    List<SelectListItem> data = new List<SelectListItem>();
        //    data.Add(new SelectListItem
        //    {
        //        Selected = city == 0,
        //        Text = "縣市",
        //        Value = "0"
        //    });
        //    data.AddRange(db.City.OrderBy(o => o.Id).Select(s =>
        //        new SelectListItem
        //        {
        //            Selected = city == s.Id,
        //            Text = s.Name,
        //            Value = s.Id.ToString()
        //        }));
        //    db.Connection.Close();
        //    return data;
        //}

        public static List<SelectListItem> Get_City_Select(int city, bool all)
        {
            MithDataContext db = new MithDataContext();
            List<SelectListItem> data = new List<SelectListItem>();
            data.Add(new SelectListItem
            {
                Selected = city == 0,
                Text = "縣市",
                Value = "0"
            });
            data.AddRange(db.City.Where(w=>w.Id>0).OrderBy(o => o.Id).Select(s =>
                new SelectListItem
                {
                    Selected = city == s.Id,
                    Text = s.Name,
                    Value = s.Id.ToString()
                }));
            db.Connection.Close();
            return data;
        }

        public static List<SelectListItem> Get_Area_Select(int city, int area, bool all)
        {
            MithDataContext db = new MithDataContext();
            List<SelectListItem> data = new List<SelectListItem>();
            data.Add(new SelectListItem
            {
                Selected = area == 0,
                Text = "區域",
                Value = "0"
            });
            if (city == 0)
            {
                return data;
            }
            data.AddRange(db.Area.Where(w => w.City_Id == city && w.Id>0).OrderBy(o => o.Id).ThenBy(t => t.ZipCode).Select(s =>
                new SelectListItem
                {
                    Selected = area == s.Id,
                    Text = s.Name,
                    Value = s.Id.ToString()
                }));
            db.Connection.Close();
            return data;
        }

        public static string Get_Area_Ajax(int city, int area, bool all)
        {
            var data = Get_Area_Select(city, area, all);
            string result = "";
            foreach (var i in data)
            {
                result += "<option value=\"" + i.Value + "\" " + (i.Selected ? "selected=\"selected\"" : "") + ">" + i.Text + "</option>";
            }
            return result;
        }

				public static List<SelectListItem> Get_Height_Select(int height, bool all)
				{
					MithDataContext db = new MithDataContext();
					List<SelectListItem> data = new List<SelectListItem>();
					data.Add(new SelectListItem
					{
						Selected = height == 0,
						Text = "身高",
						Value = "0"
					});
					data.AddRange(db.Height.Where(w => w.Id > 0).OrderBy(o => o.Id).Select(s =>
							new SelectListItem
							{
								Selected = height == s.Id,
								Text = s.Name,
								Value = s.Id.ToString()
							}));
					db.Connection.Close();
					return data;
				}

				public static List<SelectListItem> Get_Weight_Select(int weight, bool all)
				{
					MithDataContext db = new MithDataContext();
					List<SelectListItem> data = new List<SelectListItem>();
					data.Add(new SelectListItem
					{
						Selected = weight == 0,
						Text = "體重",
						Value = "0"
					});
					data.AddRange(db.Weight.Where(w => w.Id > 0).OrderBy(o => o.Id).Select(s =>
							new SelectListItem
							{
								Selected = weight == s.Id,
								Text = s.Name,
								Value = s.Id.ToString()
							}));
					db.Connection.Close();
					return data;
				}
    }
}