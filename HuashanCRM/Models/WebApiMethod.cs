﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Net;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Security;
using System.Security.Cryptography;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Mith.Areas.GoX.Models;
namespace Mith.Models
{


	public class WebApiMethod
	{
		#region Class

		private class AppErrorClass
		{
			public string Message { get; set; }
			public string CreateTime { get; set; }

		}

		public class VirtualAccountClass
		{
			public string Amount { get; set; }
			public string VirtualAccount { get; set; }
			public string BankCode { get; set; }
			public string BankAccount { get; set; }
			public string AccountName { get; set; }
			public DateTime PaymentTime { get; set; }
		}

		private class RegiserLevel1Class
		{
			public List<RegiserLevel2Class> CityList = new List<RegiserLevel2Class>();
			public List<RegiserLevel2Class> AreaList = new List<RegiserLevel2Class>();
			public List<RegiserLevel2Class> HeightList = new List<RegiserLevel2Class>();
			public List<RegiserLevel2Class> WeightList = new List<RegiserLevel2Class>();
		}

		private class RegiserLevel2Class
		{
			public int Id { get; set; }
			public string Name { get; set; }
			public int? CityId { get; set; }
		}

		private class AreaClass
		{
			public int AreaId { get; set; }
			public string Area_Name { get; set; }
		}

		private class ThemeMenuClass
		{
			public int ThemeMenuId { get; set; }
			public string Name { get; set; }
			public string Pic { get; set; }
		}

		private class ThemeGoodsClass
		{
			public int GoodsId { get; set; }
			public string Goods_Pic { get; set; }
		}

		private class BrandClass
		{
			public int BrandId { get; set; }
			public string Brand_Name { get; set; }
		}

		private class VolunteersOneClass
		{
			public string Email { get; set; }
			public string ShowName { get; set; }
			public string RealName { get; set; }
			public string Tel { get; set; }
			public int CityId { get; set; }
			public int AreaId { get; set; }
			public string Address { get; set; }
			public DateTime Birthday { get; set; }
			public int HeightId { get; set; }
			public int WeightId { get; set; }
			public string Pic { get; set; }
		}

		private class WearClothesClass
		{
			public int GoodsId { get; set; }
			public string GoodsSN { get; set; }
			public string Goods_Name { get; set; }
			public int Goods_PriceId { get; set; }
			public int Goods_Price { get; set; }
			public string Brand_Name { get; set; }
			public string Pattern { get; set; }
			public int Remark { get; set; }
			public string Color { get; set; }
			public string Season { get; set; }
			public string Style { get; set; }
			public string Fit { get; set; }
			public string Pic1 { get; set; }
			public string Pic2 { get; set; }
			public string Pic3 { get; set; }
			public string Pic4 { get; set; }
			public string Pic5 { get; set; }

		}

		private class WearSearchClass
		{
			public List<CategoryClass> ColorList = new List<CategoryClass>();
			public List<CategoryClass> SeasonList = new List<CategoryClass>();
			public List<CategoryClass> StyleList = new List<CategoryClass>();
			public List<CategoryClass> FitList = new List<CategoryClass>();
			public List<CategoryClass> PriceList = new List<CategoryClass>();
		}

		private class CategoryClass
		{
			public int Id { get; set; }
			public string Show { get; set; }
		}

		private class GoodsClass
		{
			public string Name { get; set; }
			public int Price { get; set; }
			public int? SpecialOffer { get; set; }
			public string Content { get; set; }
			public int Type { get; set; }
			public string Type_Name { get; set; }
			public string LimitOrderRange { get; set; }
			public bool LimitOrderStart { get; set; }
			public bool LimitOrderEnd { get; set; }
			public int? FewDaysShipping { get; set; }
			public string Color_Name { get; set; }
			public string ShowText { get; set; }
			public string PicSize { get; set; }
			public List<GoodsPicClass> PicList = new List<GoodsPicClass>();
			public List<GoodsSizeClass> SizeList = new List<GoodsSizeClass>();
			public List<RecommendClass> RecommendList = new List<RecommendClass>();
		}

		private class RecommendClass
		{
			public int GoodsId { get; set; }
			public string Pic1 { get; set; }
		}

		private class GoodsPicClass
		{
			public string Pic { get; set; }
		}

		private class GoodsSizeClass
		{
			public string Size { get; set; }
			public int? Quantity { get; set; }
		}

		private class GoodsQAClass
		{
			public int GoodsId { get; set; }
			public string Goods_Name { get; set; }
			public string MessageTitle { get; set; }
			public string Message { get; set; }
			public string Volunteers_Name { get; set; }
			public string Volunteers_Pic { get; set; }
			public DateTime CreateTime { get; set; }
			public string ReplyMessage { get; set; }
			public DateTime? ReplyTime { get; set; }
		}

		private class GoodsShoppingCartClass
		{
			public int GoodsShoppingCartId { get; set; }
			public int GoodsId { get; set; }
			public string Goods_Name { get; set; }
			public string Goods_Content { get; set; }
			public int Goods_Price { get; set; }
			public int? Goods_SpecialOffer { get; set; }
			public int Goods_Type { get; set; }
			public bool Goods_LimitOrderStart { get; set; }
			public bool Goods_LimitOrderEnd { get; set; }
			public string Goods_Pic { get; set; }
			public string BuySize { get; set; }
			public int BuyQuantity { get; set; }
			public List<GoodsSizeClass> SizeList = new List<GoodsSizeClass>();
		}

		private class ExpertListClass
		{
			public int ExpertId { get; set; }
			public string Name { get; set; }
			public string Pic { get; set; }
			public string Cover { get; set; }
			public string Url { get; set; }
			public string Content { get; set; }
			public bool VolunteersLike { get; set; }
			public int Like { get; set; }
			public int ArticleLikeSum { get; set; }
		}


		private class LikeExpertListClass
		{
			public int ExpertId { get; set; }
			public string Expert_Name { get; set; }
			public string Expert_Pic { get; set; }
			public string Content { get; set; }
		}

		private class ArticleListClass
		{
			public int ArticleId { get; set; }
			public int? ExpertId { get; set; }
			public string Expert_Name { get; set; }
			public string Title { get; set; }
			public string Type_Name { get; set; }
			public string Cover { get; set; }
			public string Pic { get; set; }
			public string Content { get; set; }
			public bool HomePage { get; set; }
			public DateTime CreateTime { get; set; }
		}

		private class ArticleOneClass
		{
			public string Title { get; set; }
			public string FromUrl { get; set; }
			public string Type_Name { get; set; }
			public string Cover { get; set; }
			public string Pic1 { get; set; }
			public string Content1 { get; set; }
			public string Pic2 { get; set; }
			public string Content2 { get; set; }
			public string Pic3 { get; set; }
			public string Content3 { get; set; }
			public string Pic4 { get; set; }
			public string Content4 { get; set; }
			public string Pic5 { get; set; }
			public string Content5 { get; set; }
			public bool VolunteersLike { get; set; }
			public int Like { get; set; }
			public string RecommendSN1 { get; set; }
			public string RecommendSN2 { get; set; }
			public string RecommendSN3 { get; set; }
			public string RecommendSN4 { get; set; }
			public string RecommendSN5 { get; set; }
			public DateTime CreateTime { get; set; }
			public List<ArticleMessageClass> ArticleMessageList = new List<ArticleMessageClass>();
			public List<RecommendClass> RecommendList = new List<RecommendClass>();
		}
		private class ArticleMessageClass
		{
			public int ArticleMessageId { get; set; }
			public string Volunteers_Pic { get; set; }
			public string Volunteers_Name { get; set; }
			public string MessageTitle { get; set; }
			public string Message { get; set; }
			public DateTime CreateTime { get; set; }
			public string Reply_Name { get; set; }
			public string ReplyMessage { get; set; }
			public DateTime? ReplyeTime { get; set; }
		}

		private class StyleAdviceClass
		{
			public int StyleAdviceId { get; set; }
			public int VolunteersId { get; set; }
			public string Volunteers_Pic { get; set; }
			public string Volunteers_Name { get; set; }
			public string MessageTitle { get; set; }
			public string Message { get; set; }
			public DateTime CreateTime { get; set; }
			public string Reply_Name { get; set; }
			public string ReplyMessage { get; set; }
			public DateTime? ReplyeTime { get; set; }
		}

		private class PrivateMessageClass
		{
			public int PrivateMessageId { get; set; }
			public int ExpertId { get; set; }
			public string Expert_Name { get; set; }
			public string Expert_Pic { get; set; }

			public int VolunteersId { get; set; }
			public string Volunteers_Name { get; set; }
			public string Volunteers_Pic { get; set; }
			public DateTime UpdateTime { get; set; }

		}

		private class PrivateMessageDetailClass
		{
			public string From { get; set; }
			public string Message { get; set; }
			public bool Read { get; set; }
			public DateTime CreateTime { get; set; }
			public DateTime? LastReadTime { get; set; }
		}

		private class OrderClass
		{
			public int OrderId { get; set; }
			public string OrderSN { get; set; }
			public int ReturnOrderId { get; set; }
			public int Total { get; set; }
			public int Status { get; set; }
			public string LimitOrderStatus { get; set; }
			public int CashFlowType { get; set; }
			public string CashFlowType_Name { get; set; }
			public string DeadLineRemark { get; set; }
			public string BankCode { get; set; }
			public string VirtualAccount { get; set; }
			public string MithRemark { get; set; }
			public int GoodsId { get; set; }
			public string Goods_Name { get; set; }
			public string Goods_Content { get; set; }
			public string Goods_Pic { get; set; }
			public string Goods_TypeName { get; set; }
			public string Goods_ColorName { get; set; }
			public string BuySize { get; set; }
			public int BuyQuantity { get; set; }
			public int BuyPrice { get; set; }
			public int BuyTotal { get; set; }
			public int ReturnGoodsQuantity { get; set; }
			public int LogisticsStatus { get; set; }
			public string LogisticsStatus_Name { get; set; }
			public string LogisticsSN { get; set; }
			public string LogisticsUrl { get; set; }
			public int CompanyId { get; set; }
			public string Company_Name { get; set; }
			public DateTime CreateTime { get; set; }
			public bool RetunGoods { get; set; }
			public bool CancelOrder { get; set; }
			public List<GoodsPicClass> Goods_PicList = new List<GoodsPicClass>();
		}

		public class OrderDetailClass
		{
			public int OrderId { get; set; }
			public string OrderSN { get; set; }
			public int Total { get; set; }
			public int Status { get; set; }
			public string CashFlowType_Name { get; set; }
			public string VirtualAccount { get; set; }
			public int GoodsId { get; set; }
			public string Goods_Name { get; set; }
			public string Goods_Content { get; set; }
			public string Goods_Pic { get; set; }
			public string Goods_TypeName { get; set; }
			public string BuySize { get; set; }
			public int BuyQuantity { get; set; }
			public int BuyTotal { get; set; }
			public string LogisticsStatus_Name { get; set; }
			public string LogisticsSN { get; set; }
			public string LogisticsUrl { get; set; }
			public string Company_Name { get; set; }
			public DateTime CreateTime { get; set; }
			//public List<GoodsPicClass> Goods_PicList = new List<GoodsPicClass>();
		}

		private class OrderQAClass
		{
			public string MessageTitle { get; set; }
			public string Message { get; set; }
			public string Volunteers_Name { get; set; }
			public string Volunteers_Pic { get; set; }
			public DateTime CreateTime { get; set; }
			public string ReplyMessage { get; set; }
			public DateTime? ReplyTime { get; set; }
		}

		public class ReturnOrderClass
		{
			public int GoodsId { get; set; }
			public int ReturnQuantity { get; set; }
			public int ReturnTotal { get; set; }
		}

		#endregion

		#region App 錯誤Log
		/// <summary>
		/// 取得App 錯誤Log
		/// </summary>
		/// <param name="Version">App版本</param>
		/// <returns>List</returns>
		public static object Get_AppErrorData(string Version = "")
		{

			MithDataContext db = new MithDataContext();
			List<object> obj = new List<object>();
			DateTime LastWeek = DateTime.UtcNow.AddHours(8).AddHours(-7);

			var data = db.App_ErrorRecord.Where(w => w.Version == Version).ToList();

			#region 刪除一個禮拜前的Log
			var Delete = data.Where(w => w.CreateTime < LastWeek);
			if (Delete.Any())
			{
				db.App_ErrorRecord.DeleteAllOnSubmit(Delete);
				db.SubmitChanges();
			}
			#endregion

			//只取前30筆
			data = data.Take(30).ToList();
			AppErrorClass AppErrorData = new AppErrorClass();

			foreach (var s in data)
			{
				AppErrorData = new AppErrorClass
				{
					Message = s.Message,
					CreateTime = s.CreateTime.AddHours(8).ToString("yyyy/MM/dd HH:mm")

				};
				obj.Add(AppErrorData);
			}
			return obj;
		}
		#endregion

		#region 爬黑貓網站
		private static void Crawling_BlackCat(List<Order_API_View> data)
		{
			foreach (var item in data)
			{
				//物流方式 = 黑貓 & 物流狀態 = 出貨中
				if (item.LogisticsType == 1 && item.LogisticsStatus == 2)
				{
					{
						string Url = "http://www.t-cat.com.tw/inquire/TraceDetail.aspx?ReturnUrl=Trace.aspx&BillID=" + item.LogisticsSN;
						string DefaultUserAgent = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; SV1; .NET CLR 1.1.4322; .NET CLR 2.0.50727)";
						HttpWebRequest request = WebRequest.Create(Url) as HttpWebRequest;
						request.Method = "GET";
						request.UserAgent = DefaultUserAgent;

						string result = "";
						// 取得回應資料
						using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
						{
							using (StreamReader sr = new StreamReader(response.GetResponseStream()))
							{
								result = sr.ReadToEnd();
							}
						}
					}
				}
			}
		}
		#endregion

		#region 信用卡

		//信用卡請款
		public static void Request_CreditCard(HttpServerUtilityBase Server, string ORDERNUMBER = "", string AMOUNT = "", string AUTHCODE = "")
		{
			MithDataContext db = new MithDataContext();
			try
			{
				string Url = "https://sslpayment.uwccb.com.tw/EPOSService/CRDOrderService.asmx?wsdl";
				string STOREID = "010391867";
				string CUBKEY = "eec85fee81fb036d31471462846fa422";
				string Result = "";
				//CAVALUE = MD5加密（商店編號+訂單編號+總額+CUBKEY）
				string CAVALUE = Method.CreditCard_MD5(STOREID + ORDERNUMBER + AMOUNT + AUTHCODE + CUBKEY);
				string SoapRequest =
				"<?xml version='1.0' encoding='utf-8'?>" +
				"<soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' " +
				"xmlns:xsd='http://www.w3.org/2001/XMLSchema' " +
				"xmlns:soap='http://www.w3.org/2003/05/soap-envelope'> " +
				"<soap:Body>" +
				"<CRDOrderMethod xmlns='http://tempuri.org/'> " +
				"<strRqXML>" +
				Server.HtmlEncode(
				"<MERCHANTXML>" +
												"<MSGID>ORD0005</MSGID>" +
												"<CAVALUE>" + CAVALUE + "</CAVALUE>" +
												"<CAPTUREORDERINFO>" +
																				"<STOREID>" + STOREID + "</STOREID>" +
																				"<ORDERNUMBER>" + ORDERNUMBER + "</ORDERNUMBER>" +
																				"<AMOUNT>" + AMOUNT + "</AMOUNT>" +
																				"<AUTHCODE>" + AUTHCODE + "</AUTHCODE>" +
												"</CAPTUREORDERINFO>" +
				"</MERCHANTXML>"
				) +
				"</strRqXML>" +
				"</CRDOrderMethod >" +
				"</soap:Body>" +
				"</soap:Envelope>";

				byte[] bs = Encoding.UTF8.GetBytes(SoapRequest);
				HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(Url);
				webRequest.Headers.Add("SOAPAction", "http://tempuri.org/CRDOrderMethod");
				webRequest.ContentType = "application/soap+xml;charset=\"utf-8\"";
				webRequest.Method = "POST";
				webRequest.ContentLength = bs.Length;
				using (Stream reqStream = webRequest.GetRequestStream())
				{
					reqStream.Write(bs, 0, bs.Length);
				}
				string STATUS = "";
				//API Response
				using (WebResponse response = webRequest.GetResponse())
				{
					//跨網域
					//Response.AppendHeader("Access-Control-Allow-Origin", "*");

					//回傳結果轉UTF8
					StreamReader sr = new StreamReader(response.GetResponseStream(), Encoding.UTF8);

					//讀取API回傳結果 (Decode)
					Result = sr.ReadToEnd()
									.Replace("&lt;", "<")
									.Replace("&gt;", ">")
									.Replace("<?xml version=\"1.0\" encoding=\"UTF-8\"?>", "")
									.Replace("<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"><soap:Body><CRDOrderMethodResponse xmlns=\"http://tempuri.org/\"><CRDOrderMethodResult>", "")
									.Replace("</CRDOrderMethodResult></CRDOrderMethodResponse></soap:Body></soap:Envelope>", "");
					//Result = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + Result;
					XmlDocument doc = new XmlDocument();

					//回傳結果string轉Xml
					doc.LoadXml(Result);

					//Parse XML to Get Auth Result
					XmlNode CAPTUREORDERINFO = doc.DocumentElement.SelectSingleNode("CAPTUREORDERINFO");
					Dictionary<string, string> dicAuthValue = new Dictionary<string, string>();

					foreach (XmlNode xNode in CAPTUREORDERINFO.ChildNodes)
					{
						dicAuthValue.Add(xNode.Name.ToString(), xNode.InnerText.ToString());
					}

					STATUS = dicAuthValue["STATUS"];
					//請款未結帳 成功
					if (STATUS == "0000")
					{
						var data = db.Order.FirstOrDefault(f => f.SN == ORDERNUMBER);

						if (data != null)
						{
							data.CathayAuthCode = AUTHCODE;
							data.CathayStatus = "1102";
							db.SubmitChanges();
						}
					}

					sr.Close();
				}

				//return Content(STATUS);
			}
			catch (Exception e)
			{
			}
			finally
			{

				db.Connection.Close();
			}
		}

		//信用卡取消
		public static void Cancel_CreditCard(HttpServerUtilityBase Server, string ordernumber = "")
		{
			MithDataContext db = new MithDataContext();
			try
			{
				//回傳結果
				string Result = null;

				//國泰世華 - 取得取消交易API
				string API = "https://sslpayment.uwccb.com.tw/ssl_CancelOrder.asp";

				//發出 Request
				HttpWebRequest request = HttpWebRequest.Create(API) as HttpWebRequest;

				//要帶的參數
				string Param =
																"storeid=010391867&" +
																"ordernumber=" + ordernumber;

				//將變數進行編碼
				byte[] bs = Encoding.ASCII.GetBytes(Param);
				request.Method = "POST"; // POST方式
				request.KeepAlive = true; //保持連線
				request.ProtocolVersion = HttpVersion.Version10;
				request.ContentType = "application/x-www-form-urlencoded";
				request.ContentLength = bs.Length;

				using (Stream reqStream = request.GetRequestStream())
				{
					reqStream.Write(bs, 0, bs.Length);
				}

				//API Response
				using (WebResponse response = request.GetResponse())
				{
					//回傳結果轉Big5
					StreamReader sr = new StreamReader(response.GetResponseStream(), Encoding.GetEncoding("Big5"));

					//讀取API回傳結果 (Decode)
					Result = sr.ReadToEnd();

					XmlDocument doc = new XmlDocument();

					//回傳結果string轉Xml
					doc.LoadXml(Result);

					sr.Close();
				}
			}
			catch (Exception e)
			{
			}
			finally
			{
				db.Connection.Close();
			}
		}

		//信用卡退款
		public static string Refund_CreditCard(HttpServerUtilityBase Server, string ORDERNUMBER = "", string AMOUNT = "", string AUTHCODE = "")
		{
			MithDataContext db = new MithDataContext();
			try
			{
				string Url = "https://sslpayment.uwccb.com.tw/EPOSService/CRDOrderService.asmx?wsdl";
				string STOREID = "010391867";
				string CUBKEY = "eec85fee81fb036d31471462846fa422";
				string Result = "";
				string STATUS = "";
				//CAVALUE = MD5加密（商店編號+訂單編號+總額+CUBKEY）
				string CAVALUE = Method.CreditCard_MD5(STOREID + ORDERNUMBER + AMOUNT + AUTHCODE + CUBKEY);
				string SoapRequest =
				"<?xml version='1.0' encoding='utf-8'?>" +
				"<soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' " +
				"xmlns:xsd='http://www.w3.org/2001/XMLSchema' " +
				"xmlns:soap='http://www.w3.org/2003/05/soap-envelope'> " +
				"<soap:Body>" +
				"<CRDOrderMethod xmlns='http://tempuri.org/'> " +
				"<strRqXML>" +
				Server.HtmlEncode(
				"<MERCHANTXML>" +
												"<MSGID>ORD0003</MSGID>" +
												"<CAVALUE>" + CAVALUE + "</CAVALUE>" +
												"<REFUNDORDERINFO>" +
																				"<STOREID>" + STOREID + "</STOREID>" +
																				"<ORDERNUMBER>" + ORDERNUMBER + "</ORDERNUMBER>" +
																				"<AMOUNT>" + AMOUNT + "</AMOUNT>" +
																				"<AUTHCODE>" + AUTHCODE + "</AUTHCODE>" +
												"</REFUNDORDERINFO>" +
				"</MERCHANTXML>"
				) +
				"</strRqXML>" +
				"</CRDOrderMethod >" +
				"</soap:Body>" +
				"</soap:Envelope>";

				byte[] bs = Encoding.UTF8.GetBytes(SoapRequest);
				HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(Url);
				webRequest.Headers.Add("SOAPAction", "http://tempuri.org/CRDOrderMethod");
				webRequest.ContentType = "application/soap+xml;charset=\"utf-8\"";
				webRequest.Method = "POST";
				webRequest.ContentLength = bs.Length;
				using (Stream reqStream = webRequest.GetRequestStream())
				{
					reqStream.Write(bs, 0, bs.Length);
				}

				//API Response
				using (WebResponse response = webRequest.GetResponse())
				{
					//跨網域
					//Response.AppendHeader("Access-Control-Allow-Origin", "*");

					//回傳結果轉UTF8
					StreamReader sr = new StreamReader(response.GetResponseStream(), Encoding.UTF8);

					//讀取API回傳結果 (Decode)
					Result = sr.ReadToEnd()
									.Replace("&lt;", "<")
									.Replace("&gt;", ">")
									.Replace("<?xml version=\"1.0\" encoding=\"UTF-8\"?>", "")
									.Replace("<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"><soap:Body><CRDOrderMethodResponse xmlns=\"http://tempuri.org/\"><CRDOrderMethodResult>", "")
									.Replace("</CRDOrderMethodResult></CRDOrderMethodResponse></soap:Body></soap:Envelope>", "");
					//Result = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + Result;
					XmlDocument doc = new XmlDocument();

					//回傳結果string轉Xml
					doc.LoadXml(Result);

					//Parse XML to Get Auth Result
					XmlNode REFUNDORDERINFO = doc.DocumentElement.SelectSingleNode("REFUNDORDERINFO");
					Dictionary<string, string> dicAuthValue = new Dictionary<string, string>();

					foreach (XmlNode xNode in REFUNDORDERINFO.ChildNodes)
					{
						dicAuthValue.Add(xNode.Name.ToString(), xNode.InnerText.ToString());
					}

					STATUS = dicAuthValue["STATUS"];
					sr.Close();
				}

				return STATUS;
			}
			catch (Exception e)
			{
				return e.Message;
			}
			finally
			{
				db.Connection.Close();
			}
		}

		#endregion

		#region 虛擬帳號

		/// <summary>
		/// 產生虛擬帳號
		/// </summary>
		/// <param name="Total">金額</param>
		public static string Generate_VirtualAccount(string Total)
		{
			if (!string.IsNullOrWhiteSpace(Total))
			{
				#region 虛擬帳號檢核

				string Account = "";
				//帳號前4碼
				string CompanyAccount = "2729";

				//帳號5-8碼（三天後的日期）
				string DelineDay = DateTime.UtcNow.AddHours(8).AddDays(3).ToString("MMdd");

				//帳號9、10碼（西元年後兩碼）
				string Year = DateTime.Now.ToString("yyyy");

				//帳號第11-15碼
				string Radom = Method.RandomKey(5, true, false, false, false);

				//算第1碼，權數：4
				string Get1 = CompanyAccount.Substring(0, 1);
				string Sum1 = (int.Parse(Get1) * 4).ToString();
				int No1 = 0;
				if (Sum1.Length == 1)
				{
					No1 = int.Parse(Sum1);
				}
				else
				{
					No1 = int.Parse(Sum1.Substring(1, 1));
				}

				//算第2碼，權數：5
				string Get2 = CompanyAccount.Substring(1, 1);
				string Sum2 = (int.Parse(Get2) * 5).ToString();
				int No2 = 0;
				if (Sum2.Length == 1)
				{
					No2 = int.Parse(Sum2);
				}
				else
				{
					No2 = int.Parse(Sum2.Substring(1, 1));
				}

				//算第3碼，權數：6
				string Get3 = CompanyAccount.Substring(2, 1);
				string Sum3 = (int.Parse(Get3) * 6).ToString();
				int No3 = 0;
				if (Sum3.Length == 1)
				{
					No3 = int.Parse(Sum3);
				}
				else
				{
					No3 = int.Parse(Sum3.Substring(1, 1));
				}

				//算第4碼，權數：7
				string Get4 = CompanyAccount.Substring(3, 1);
				string Sum4 = (int.Parse(Get4) * 7).ToString();
				int No4 = 0;
				if (Sum4.Length == 1)
				{
					No4 = int.Parse(Sum4);
				}
				else
				{
					No4 = int.Parse(Sum4.Substring(1, 1));
				}

				//算第5碼，權數：8
				string Get5 = DelineDay.Substring(0, 1);
				string Sum5 = (int.Parse(Get5) * 8).ToString();
				int No5 = 0;
				if (Sum5.Length == 1)
				{
					No5 = int.Parse(Sum5);
				}
				else
				{
					No5 = int.Parse(Sum5.Substring(1, 1));
				}

				//算第6碼，權數：9
				string Get6 = DelineDay.Substring(1, 1);
				string Sum6 = (int.Parse(Get6) * 9).ToString();
				int No6 = 0;
				if (Sum6.Length == 1)
				{
					No6 = int.Parse(Sum6);
				}
				else
				{
					No6 = int.Parse(Sum6.Substring(1, 1));
				}

				//算第7碼，權數：1
				string Get7 = DelineDay.Substring(2, 1);
				string Sum7 = (int.Parse(Get7) * 1).ToString();
				int No7 = 0;
				if (Sum7.Length == 1)
				{
					No7 = int.Parse(Sum7);
				}
				else
				{
					No7 = int.Parse(Sum7.Substring(1, 1));
				}

				//算第8碼，權數：2
				string Get8 = DelineDay.Substring(3, 1);
				string Sum8 = (int.Parse(Get8) * 2).ToString();
				int No8 = 0;
				if (Sum8.Length == 1)
				{
					No8 = int.Parse(Sum8);
				}
				else
				{
					No8 = int.Parse(Sum8.Substring(1, 1));
				}

				//算第9碼，權數：3
				string Get9 = Year.Substring(2, 1);
				string Sum9 = (int.Parse(Get9) * 3).ToString();
				int No9 = 0;
				if (Sum9.Length == 1)
				{
					No9 = int.Parse(Sum9);
				}
				else
				{
					No9 = int.Parse(Sum9.Substring(1, 1));
				}

				//算第10碼，權數：4
				string Get10 = Year.Substring(3, 1);
				string Sum10 = (int.Parse(Get10) * 4).ToString();
				int No10 = 0;
				if (Sum10.Length == 1)
				{
					No10 = int.Parse(Sum10);
				}
				else
				{
					No10 = int.Parse(Sum10.Substring(1, 1));
				}

				//算第11碼，權數：5
				string Get11 = Radom.Substring(0, 1);
				string Sum11 = (int.Parse(Get11) * 5).ToString();
				int No11 = 0;
				if (Sum11.Length == 1)
				{
					No11 = int.Parse(Sum11);
				}
				else
				{
					No11 = int.Parse(Sum11.Substring(1, 1));
				}

				//算第12碼，權數：6
				string Get12 = Radom.Substring(1, 1);
				string Sum12 = (int.Parse(Get12) * 6).ToString();
				int No12 = 0;
				if (Sum12.Length == 1)
				{
					No12 = int.Parse(Sum12);
				}
				else
				{
					No12 = int.Parse(Sum12.Substring(1, 1));
				}


				//算第13碼，權數：7
				string Get13 = Radom.Substring(2, 1);
				string Sum13 = (int.Parse(Get13) * 7).ToString();
				int No13 = 0;
				if (Sum13.Length == 1)
				{
					No13 = int.Parse(Sum13);
				}
				else
				{
					No13 = int.Parse(Sum13.Substring(1, 1));
				}

				//算第14碼，權數：8
				string Get14 = Radom.Substring(3, 1);
				string Sum14 = (int.Parse(Get14) * 8).ToString();
				int No14 = 0;
				if (Sum14.Length == 1)
				{
					No14 = int.Parse(Sum14);
				}
				else
				{
					No14 = int.Parse(Sum14.Substring(1, 1));
				}

				//算第15碼，權數：9
				string Get15 = Radom.Substring(4, 1);
				string Sum15 = (int.Parse(Get15) * 9).ToString();
				int No15 = 0;
				if (Sum15.Length == 1)
				{
					No15 = int.Parse(Sum15);
				}
				else
				{
					No15 = int.Parse(Sum15.Substring(1, 1));
				}

				//第16碼，前15碼的檢核碼
				string Get16 = No1 + No2 + No3 + No4 + No5 + No6 + No7 + No8 + No9 + No10 + No11 + No12 + No13 + No14 + No15 + "";
				int Chech_Account = 0;
				if (Get16.Length == 1)
				{
					Chech_Account = int.Parse(Get16);
				}
				else
				{
					Chech_Account = int.Parse(Get16.Substring(1, 1));
				}
				Chech_Account = 10 - Chech_Account;
				Account = Get1 + Get2 + Get3 + Get4 + Get5 + Get6 + Get7 + Get8 + Get9 + Get10 + Get11 + Get12 + Get13 + Get14 + Get15;

				#endregion

				#region 金額檢核

				//金額前3碼為0，從第5碼開始計算
				switch (Total.Length)
				{
					case 4:
						Total = "0" + Total;
						break;
					case 3:
						Total = "00" + Total;
						break;
					case 2:
						Total = "000" + Total;
						break;
					case 1:
						Total = "0000" + Total;
						break;
				}

				//算金額第4碼，權數：5
				string Totla_Get4 = Total.Substring(0, 1);
				string Totla_Sum4 = (int.Parse(Totla_Get4) * 8).ToString();
				int Totla_No4 = 0;
				if (Totla_Sum4.Length == 1)
				{
					Totla_No4 = int.Parse(Totla_Sum4);
				}
				else
				{
					Totla_No4 = int.Parse(Totla_Sum4.Substring(1, 1));
				}


				//算金額第5碼，權數：4
				string Totla_Get5 = Total.Substring(1, 1);
				string Totla_Sum5 = (int.Parse(Totla_Get5) * 4).ToString();
				int Totla_No5 = 0;
				if (Totla_Sum5.Length == 1)
				{
					Totla_No5 = int.Parse(Totla_Sum5);
				}
				else
				{
					Totla_No5 = int.Parse(Totla_Sum5.Substring(1, 1));
				}

				//算金額第6碼，權數：3
				string Totla_Get6 = Total.Substring(2, 1);
				string Totla_Sum6 = (int.Parse(Totla_Get6) * 3).ToString();
				int Totla_No6 = 0;
				if (Totla_Sum6.Length == 1)
				{
					Totla_No6 = int.Parse(Totla_Sum6);
				}
				else
				{
					Totla_No6 = int.Parse(Totla_Sum6.Substring(1, 1));
				}

				//算金額第7碼，權數：2
				string Totla_Get7 = Total.Substring(3, 1);
				string Totla_Sum7 = (int.Parse(Totla_Get7) * 2).ToString();
				int Totla_No7 = 0;
				if (Totla_Sum7.Length == 1)
				{
					Totla_No7 = int.Parse(Totla_Sum7);
				}
				else
				{
					Totla_No7 = int.Parse(Totla_Sum7.Substring(1, 1));
				}

				//算金額第8碼，權數：1
				string Totla_Get8 = Total.Substring(4, 1);
				string Totla_Sum8 = (int.Parse(Totla_Get8) * 1).ToString();
				int Totla_No8 = 0;
				if (Totla_Sum8.Length == 1)
				{
					Totla_No8 = int.Parse(Totla_Sum8);
				}
				else
				{
					Totla_No8 = int.Parse(Totla_Sum8.Substring(1, 1));
				}

				//金額的檢核碼
				int Check_Total = Totla_No5 + Totla_No6 + Totla_No7 + Totla_No8;
				//取個位數
				if (Check_Total.ToString().Length == 2)
				{
					Check_Total = int.Parse(Check_Total.ToString().Substring(1, 1));
				}
				Check_Total = 10 - Check_Total;


				//第16檢核碼= 帳號檢核 +金額檢核
				int Check_Sum = Chech_Account + Check_Total;
				//取個位數
				if (Check_Sum.ToString().Length == 2)
				{
					Check_Sum = int.Parse(Check_Sum.ToString().Substring(1, 1));
				}

				//前15碼+第16檢核碼
				Account += Check_Sum.ToString();
				#endregion

				return Account;
			}
			else
			{
				return "";
			}
		}

		/// <summary>
		/// 更新-虛擬帳號繳費狀態
		/// </summary>
		/// <param name="VirtualAccount">虛擬帳號</param>
		/// <param name="PaymentTime">繳費時間</param>
		/// <param name="BankCode">銀行代號</param>
		/// <param name="BankAccount">銀行帳號</param>
		/// <param name="AccountName">帳戶名稱</param>
		/// <returns>List</returns>
		public static void Update_AtmPament(string VirtualAccount, int Total, DateTime PaymentTime, string BankCode, string BankAccount, string AccountName)
		{
			MithDataContext db = new MithDataContext();
			try
			{
				var Order = db.Order.FirstOrDefault(w => w.VirtualAccount == VirtualAccount && w.Total == Total && w.CashFlowStatus == 1);
				if (Order != null)
				{
					Order.CashFlowStatus = 2; //已付款
					Order.PaymentTime = PaymentTime.AddHours(-8);
					Order.BankCode = BankCode;
					Order.BankAccount = BankAccount;
					if (!string.IsNullOrWhiteSpace(AccountName))
					{
						Order.AccountName = AccountName;
					}
					db.SubmitChanges();

					var OrderDetail = db.OrderDetail_Index_View.Where(w => w.OrderId == Order.Id).OrderBy(o => o.CompanyId);
					string BuyGoodsStr = "\n";

					var Volunteers = db.Volunteers.FirstOrDefault(f => f.Id == Order.VolunteersId);

					#region 發送繳費成功Email（會員）
					Task t1 = Task.Run(() =>
					{
						foreach (var item in OrderDetail)
						{
							BuyGoodsStr += "【" + item.Goods_SN + "】" + item.Goods_Name + "（" + item.BuySize + " × " + item.BuyQuantity + "）\n";
						}
						string EmailContent = HttpUtility.HtmlDecode("您好\n訂單編號：" + Order.SN + "\n付款方式：ATM轉帳" + "\n付款金額：" + Order.Total + "\n訂購商品：" + BuyGoodsStr + "\n===================================================================\n您已完成付款，MiTH不會請您變更付款方式" + "\n若有任何問題，可連繫MiTH客服：mith@mix-with.com");

						Method.Send_Mail(Volunteers.Email, "MiTH App 付款完成通知 [訂單編號" + Order.SN + "]", EmailContent);
					});
					#endregion

					#region 發送繳費成功Email（廠商）
					Task t2 = Task.Run(() =>
					{
						HttpContext.Current.Session["CompanyId"] = 0;
						foreach (var item in OrderDetail)
						{
							//CompanyId不同才寄信
							if ((int)HttpContext.Current.Session["CompanyId"] != item.CompanyId)
							{
								string Company_Email = db.Company.FirstOrDefault(f => f.Id == item.CompanyId).ContactEmail != null ? db.Company.FirstOrDefault(f => f.Id == item.CompanyId).ContactEmail : "";

								//只取得訂購此廠商的商品
								var CompanyOrder = OrderDetail.Where(w => w.CompanyId == item.CompanyId);
								BuyGoodsStr = "\n";
								foreach (var item2 in CompanyOrder)
								{
									BuyGoodsStr += "【" + item2.Goods_SN + "】" + item2.Goods_Name + "（" + item2.BuySize + " × " + item2.BuyQuantity + "）\n";
								}

								string EmailContent2 = HttpUtility.HtmlDecode("您好, 消費者已於" + PaymentTime.ToString("yyyy/MM/dd HH:mm") + " 完成付款" + "\n\n訂單編號：" + Order.SN + "\n訂購會員：" + Volunteers.RealName.Substring(0, Volunteers.RealName.Length - 1) + "*" + "\n付款方式：ATM轉帳" + "\n訂購商品：" + BuyGoodsStr + "\n請至後台備貨，並於出貨期限內出貨。");

								//發送Email
								Method.Send_Mail(Company_Email, "MiTH App 訂單備貨通知 [訂單編號" + Order.SN + "]", EmailContent2);
							}
							HttpContext.Current.Session["CompanyId"] = (int)item.CompanyId;
						}
						//清空Session
						HttpContext.Current.Session["CompanyId"] = null;
					});
					#endregion

				};
			}
			catch { }

			finally
			{
				db.Connection.Close();
			}
		}
		#endregion

		#region 訂單
		public static void Delete_Order(int OrderId = 0)
		{
			MithDataContext db = new MithDataContext();
			try
			{
				//取得訂單主檔
				var Order = db.Order.FirstOrDefault(f => f.Id == OrderId);
				//取得訂單細節
				var OrderDetail = db.OrderDetail.Where(f => f.OrderId == OrderId);

				if (Order != null)
				{
					if (OrderDetail != null)
					{
						foreach (var item in OrderDetail)
						{
							int Goods_Type = Goods_Type = db.Goods_Edit_View.FirstOrDefault(f => f.Id == item.GoodsId).Type.Value;

							#region 加回商品數量

							//取得商品數量
							var GoodsQuantity = db.GoodsQuantity.FirstOrDefault(f => f.GoodsId == item.GoodsId && f.Size == item.BuySize);
							//商品底下有這個尺寸
							if (GoodsQuantity != null)
							{
								//非集單商品才加回庫存
								if (Goods_Type != 3)
								{
									//商品庫存
									GoodsQuantity.Quantity += item.BuyQuantity;
									//商品銷售量減掉
									GoodsQuantity.SellQuantity -= item.BuyQuantity;
								}
								//集單商品
								else
								{
									GoodsQuantity.LimitOrderQuantity -= item.BuyQuantity;
								}

								db.SubmitChanges();
							#endregion

								//先刪除訂單細節
								db.OrderDetail.DeleteOnSubmit(item);
								db.SubmitChanges();
							}
						}
						//再刪除訂單主檔
						db.Order.DeleteOnSubmit(Order);
						db.SubmitChanges();
					}
				}
			}
			catch (Exception e)
			{
			}
			finally
			{
				db.Connection.Close();
			}
		}

		//訂單成立寄給廠商
		public static void Send_OrderMail(DateTime CreateTime, int OrderId = 0, string OrderSN = "", int VolunteersId = 0, string CashFlowType_Name = "")
		{
			MithDataContext db = new MithDataContext();
			try
			{
				string Volunteers_Name = db.Volunteers.FirstOrDefault(f => f.Id == VolunteersId).RealName;

				var OrderDetail = db.OrderDetail_Index_View.Where(w => w.OrderId == OrderId).OrderBy(o => o.CompanyId);
				HttpContext.Current.Session["CompanyId"] = 0;
				foreach (var item in OrderDetail)
				{
					//CompanyId不同才寄信
					if ((int)HttpContext.Current.Session["CompanyId"] != item.CompanyId)
					{
						string CompanyEmail = db.Company.FirstOrDefault(f => f.Id == item.CompanyId).ContactEmail != null ? db.Company.FirstOrDefault(f => f.Id == item.CompanyId).ContactEmail : "";

						//只取得訂購此廠商的商品
						var CompanyOrder = OrderDetail.Where(w => w.CompanyId == item.CompanyId);
						string BuyGoodsStr = "\n";
						foreach (var item2 in CompanyOrder)
						{
							BuyGoodsStr += "【" + item2.Goods_SN + "】" + item2.Goods_Name + "（" + item2.BuySize + " × " + item2.BuyQuantity + "）\n";
						}
						//ATM轉帳
						if (CashFlowType_Name == "ATM轉帳")
						{
							string EmailContent2 = HttpUtility.HtmlDecode("您好,有一筆新的訂單\n\n下單時間：" + CreateTime.ToString("yyyy/MM/dd HH:mm") + "\n訂單編號：" + OrderSN + "\n訂購會員：" + Volunteers_Name.Substring(0, Volunteers_Name.Length - 1) + "*" + "\n付款方式：" + CashFlowType_Name + "\n訂購商品：" + BuyGoodsStr);

							//發送Email
							Method.Send_Mail(CompanyEmail, "MiTH App 新訂單通知 [訂單編號" + OrderSN + "]", EmailContent2);
						}
						//信用卡
						else
						{
							string EmailContent2 = HttpUtility.HtmlDecode("您好, 消費者已於" + CreateTime.ToString("yyyy/MM/dd HH:mm") + " 完成付款" + "\n\n訂單編號：" + OrderSN + "\n訂購會員：" + Volunteers_Name.Substring(0, Volunteers_Name.Length - 1) + "*" + "\n付款方式：" + CashFlowType_Name + "\n訂購商品：" + BuyGoodsStr + "\n請至後台備貨，並於出貨期限內出貨。");

							//發送Email
							Method.Send_Mail(CompanyEmail, "MiTH App 訂單備貨通知 [訂單編號" + OrderSN + "]", EmailContent2);
						}

					}
					HttpContext.Current.Session["CompanyId"] = (int)item.CompanyId;
				}
				//清空Session
				HttpContext.Current.Session["CompanyId"] = null;
			}
			catch (Exception e)
			{
			}
			finally
			{
				db.Connection.Close();
			}
		}

		#endregion

		#region  縣市區域List
		/// <summary>
		/// 取得縣市List
		/// </summary>
		/// <returns>List</returns>
		public static object Get_RegiserOptionData()
		{
			MithDataContext db = new MithDataContext();
			try
			{
				RegiserLevel1Class RegiserVolunteersData = new RegiserLevel1Class();
				List<object> obj = new List<object>();


				var Get_City = from i in db.City
											 select new RegiserLevel2Class()
											 {
												 Id = i.Id,
												 Name = i.Name,
												 CityId = null,
											 };

				var Get_Area = from i in db.Area
											 select new RegiserLevel2Class()
											 {
												 Id = i.Id,
												 Name = i.Name,
												 CityId = i.City_Id,
											 };
				var Get_Height = from i in db.Height
												 select new RegiserLevel2Class()
												 {
													 Id = i.Id,
													 Name = i.Name,
													 CityId = null,
												 };

				var Get_Weight = from i in db.Weight
												 select new RegiserLevel2Class()
												 {
													 Id = i.Id,
													 Name = i.Name,
													 CityId = null,
												 };
				RegiserVolunteersData = new RegiserLevel1Class
				{
					CityList = Get_City.ToList(),
					AreaList = Get_Area.ToList(),
					HeightList = Get_Height.ToList(),
					WeightList = Get_Weight.ToList(),
				};
				obj.Add(RegiserVolunteersData);
				db.Connection.Close();
				return obj;
			}
			catch (Exception e)
			{
				return null;
			}
		}


		/// <summary>
		/// 取得區域List
		/// </summary>
		/// <param name="CityId">城市Id</param>
		/// <returns>List</returns>
		public static List<Area> Get_AreaData(int CityId)
		{

			MithDataContext db = new MithDataContext();
			var data = db.Area.Where(w => w.City_Id == CityId).ToList();
			db.Connection.Close();
			return data;
		}
		#endregion

		#region 會員

		/// <summary>
		/// 取得文章List
		/// </summary>
		/// <param name="ArticleId">文章Id</param>
		/// <returns>List</returns>
		public static object Get_VolunteersOneData(int VolunteersId = 0)
		{
			MithDataContext db = new MithDataContext();
			List<object> obj = new List<object>();

			var data = db.Volunteers.FirstOrDefault(f => f.Id == VolunteersId);

			obj.Add(new
			{
				Email = data.Email,
				ShowName = data.ShowName,
				RealName = data.RealName,
				Tel = data.ContactTel,
				CityId = data.CityId,
				AreaId = data.AreaId,
				Address = data.Address,
				Birthday = data.Birthday,
				HeightId = data.Height,
				WeightId = data.Weight,
				Pic = data.Pic
			});

			return obj;

		}

		#endregion

		#region  主題館

		/// <summary>
		/// 取得主題館Menu List
		/// </summary>
		/// <param name="Pic_Size">尺寸大小</param>
		/// <returns>List</returns>
		public static object Get_ThemeMenuData(string Pic_Size = "")
		{
			MithDataContext db = new MithDataContext();

			var data = db.Other.Where(w => w.Id >= 11 && w.Id <= 16).OrderBy(o => o.Sort).ToList();
			ThemeMenuClass ThemeMenuData = new ThemeMenuClass();
			List<object> obj = new List<object>();
			string Pic = null;
			foreach (var s in data)
			{
				switch (Pic_Size)
				{
					case "Big":
						Pic = s.Pic_Big;
						break;
					case "Middle":
						Pic = s.Pic_Middle;
						break;
					case "Small":
						Pic = s.Pic_Small;
						break;
				}

				ThemeMenuData = new ThemeMenuClass
				{
					ThemeMenuId = s.Id,
					Name = s.Content,
					Pic = Pic
				};
				obj.Add(ThemeMenuData);

			}

			db.Connection.Close();
			return obj;
		}

		/// <summary>
		/// 取得主題館商品 List
		/// </summary>
		/// <param name="ThemeMenuId">主題館Id</param>
		/// <param name="BrandId">品牌Id</param>
		/// <param name="Pic_Size">尺寸大小</param>
		/// <returns>List</returns>
		public static object Get_ThemeGoodsData(int ThemeMenuId = 0, int BrandId = 0, string Pic_Size = "")
		{
			MithDataContext db = new MithDataContext();
			ThemeGoodsClass ThemeGoodsData = new ThemeGoodsClass();
			List<object> obj = new List<object>();
			string Pic = null;
			var data = db.ThemeGoods_View.ToList(); ;
			switch (ThemeMenuId)
			{
				#region 搜尋某品牌的商品
				case 0:
					data = data.Where(w => w.BrandId == BrandId).ToList();
					foreach (var s in data)
					{

						switch (Pic_Size)
						{
							case "Big":
								Pic = s.Pic_Big;
								break;
							case "Middle":
								Pic = s.Pic_Middle;
								break;
							case "Small":
								Pic = s.Pic_Small;
								break;
						}

						ThemeGoodsData = new ThemeGoodsClass
						{
							GoodsId = s.GoodsId,
							Goods_Pic = Pic
						};
						obj.Add(ThemeGoodsData);
					}
					obj = obj.OrderBy(emp => Guid.NewGuid()).ToList();
					break;
				#endregion

				#region 廠商分類為 = 韓國、日本、精品店
				case 11:
					data = data.Where(w => w.Company_Type == 2 || w.Company_Type == 3 || w.Company_Type == 5).ToList();
					foreach (var s in data)
					{

						switch (Pic_Size)
						{
							case "Big":
								Pic = s.Pic_Big;
								break;
							case "Middle":
								Pic = s.Pic_Middle;
								break;
							case "Small":
								Pic = s.Pic_Small;
								break;
						}
						ThemeGoodsData = new ThemeGoodsClass
						{
							GoodsId = s.GoodsId,
							Goods_Pic = Pic
						};
						obj.Add(ThemeGoodsData);
					}
					obj = obj.OrderBy(emp => Guid.NewGuid()).ToList();
					break;
				#endregion

				#region 廠商分類 = 品牌商
				case 12:
					data = data.Where(w => w.Company_Type == 6).ToList();
					foreach (var s in data)
					{

						switch (Pic_Size)
						{
							case "Big":
								Pic = s.Pic_Big;
								break;
							case "Middle":
								Pic = s.Pic_Middle;
								break;
							case "Small":
								Pic = s.Pic_Small;
								break;
						}

						ThemeGoodsData = new ThemeGoodsClass
						{
							GoodsId = s.GoodsId,
							Goods_Pic = Pic
						};
						obj.Add(ThemeGoodsData);
					}
					obj = obj.OrderBy(emp => Guid.NewGuid()).ToList();
					break;
				#endregion

				#region 廠商分類 = 獨立設計師
				case 13:
					data = data.Where(w => w.Company_Type == 1).ToList();
					foreach (var s in data)
					{

						switch (Pic_Size)
						{
							case "Big":
								Pic = s.Pic_Big;
								break;
							case "Middle":
								Pic = s.Pic_Middle;
								break;
							case "Small":
								Pic = s.Pic_Small;
								break;
						}

						ThemeGoodsData = new ThemeGoodsClass
						{
							GoodsId = s.GoodsId,
							Goods_Pic = Pic
						};
						obj.Add(ThemeGoodsData);
					}
					obj = obj.OrderBy(emp => Guid.NewGuid()).ToList();
					break;
				#endregion

				#region 商品分類 = 預訂(製)貨
				case 14:
					data = data.Where(w => w.Goods_Type == 3).ToList();
					foreach (var s in data)
					{

						switch (Pic_Size)
						{
							case "Big":
								Pic = s.Pic_Big;
								break;
							case "Middle":
								Pic = s.Pic_Middle;
								break;
							case "Small":
								Pic = s.Pic_Small;
								break;
						}

						ThemeGoodsData = new ThemeGoodsClass
						{
							GoodsId = s.GoodsId,
							Goods_Pic = Pic
						};
						obj.Add(ThemeGoodsData);
					}
					obj = obj.OrderBy(emp => Guid.NewGuid()).ToList();
					break;
				#endregion

				#region 熱銷 Top30 商品
				case 15:
					#region 先取有設定置頂的商品
					var Hot30 = db.ThemeGoods_View.Where(w => w.Hot30 == true).ToList();
					//置頂商品數量
					int Hot30_Count = Hot30.Count();
					//如果有設定Top30商品
					if (Hot30.Any())
					{
						foreach (var s in Hot30)
						{

							switch (Pic_Size)
							{
								case "Big":
									Pic = s.Pic_Big;
									break;
								case "Middle":
									Pic = s.Pic_Middle;
									break;
								case "Small":
									Pic = s.Pic_Small;
									break;
							}

							ThemeGoodsData = new ThemeGoodsClass
							{
								GoodsId = s.GoodsId,
								Goods_Pic = Pic
							};
							obj.Add(ThemeGoodsData);
						}
					}
					#endregion

					#region 再取未設定置頂的商品（真的照銷售排行）
					int Take = 30 - Hot30_Count;

					var NotHot30 = db.ThemeGoods_View.Where(w => w.Hot30 == false).OrderByDescending(o => o.SellQuantityTotal).Take(Take).ToList();
					//如果有設定Top30商品
					if (NotHot30.Any())
					{
						foreach (var s in NotHot30)
						{

							switch (Pic_Size)
							{
								case "Big":
									Pic = s.Pic_Big;
									break;
								case "Middle":
									Pic = s.Pic_Middle;
									break;
								case "Small":
									Pic = s.Pic_Small;
									break;
							}

							ThemeGoodsData = new ThemeGoodsClass
							{
								GoodsId = s.GoodsId,
								Goods_Pic = Pic
							};
							obj.Add(ThemeGoodsData);
						}
					}

					#endregion
					break;
				#endregion

				#region 7天內上架的商品
				case 16:

					#region 置頂
					var OnTop = data.Where(w => w.OnTop == true).ToList();
					foreach (var s in OnTop)
					{

						switch (Pic_Size)
						{
							case "Big":
								Pic = s.Pic_Big;
								break;
							case "Middle":
								Pic = s.Pic_Middle;
								break;
							case "Small":
								Pic = s.Pic_Small;
								break;
						}
						ThemeGoodsData = new ThemeGoodsClass
						{
							GoodsId = s.GoodsId,
							Goods_Pic = Pic
						};
						obj.Add(ThemeGoodsData);
					}

					#endregion

					#region 非置頂
					DateTime LastWeek = DateTime.UtcNow.AddHours(8).AddDays(-7);
					var NotOnTop = data.Where(w => w.OnTop == false && w.CreateTime > LastWeek).ToList();
					foreach (var s in NotOnTop)
					{

						switch (Pic_Size)
						{
							case "Big":
								Pic = s.Pic_Big;
								break;
							case "Middle":
								Pic = s.Pic_Middle;
								break;
							case "Small":
								Pic = s.Pic_Small;
								break;
						}
						ThemeGoodsData = new ThemeGoodsClass
						{
							GoodsId = s.GoodsId,
							Goods_Pic = Pic
						};
						obj.Add(ThemeGoodsData);
					}

					#endregion

					break;
				#endregion
			}
			db.Connection.Close();
			return obj;
		}
		public static object Get_BrandData()
		{
			MithDataContext db = new MithDataContext();

			var data = db.Brand;
			BrandClass BrandData = new BrandClass();
			List<object> obj = new List<object>();
			foreach (var s in data)
			{
				BrandData = new BrandClass
				{
					BrandId = s.Id,
					Brand_Name = s.Name,
				};
				obj.Add(BrandData);
			}

			db.Connection.Close();
			return obj;
		}

		#endregion

		#region  百變穿搭List
		/// <summary>
		/// 取得百變穿搭List
		/// </summary>
		/// <param name="Pic_Size">尺寸大小</param>
		/// <returns>List</returns>
		public static object Get_WearClothesData(string Pic_Size = "")
		{
			MithDataContext db = new MithDataContext();


			WearClothesClass WearClothesData = new WearClothesClass();
			List<object> obj = new List<object>();
			string Pic2 = null;
			string Pic3 = null;
			string Pic4 = null;
			string Pic5 = null;

			#region 穿搭圖

			#region 先取設定為前100件
			var Promotion100 = db.GoodsSN_API_View.Where(w => w.Goods_Status == 3 && w.Pic1_Big != null && w.GoodsId != 0 && w.Goods_Promotion100 == true).ToList();

			Promotion100 = Promotion100.OrderBy(emp => Guid.NewGuid()).ToList();

			foreach (var s in Promotion100)
			{
				switch (Pic_Size)
				{
					case "Big":
						Pic2 = s.Pic2_Big;
						Pic3 = s.Pic3_Big;
						Pic4 = s.Pic4_Big;
						Pic5 = s.Pic5_Big;
						break;
					case "Middle":
						Pic2 = s.Pic2_Middle;
						Pic3 = s.Pic3_Middle;
						Pic4 = s.Pic4_Middle;
						Pic5 = s.Pic5_Middle;
						break;
					case "Small":
						Pic2 = s.Pic2_Small;
						Pic3 = s.Pic3_Small;
						Pic4 = s.Pic4_Small;
						Pic5 = s.Pic5_Small;
						break;
				}

				WearClothesData = new WearClothesClass
				{
					GoodsId = s.GoodsId ?? 0,
					GoodsSN = s.SN,
					Goods_Name = s.Goods_Name,
					Goods_PriceId = Get_Goods_PriceId(s.Goods_Price.Value),
					Goods_Price = s.Goods_Price.Value,
					Brand_Name = s.Brand_Name,
					Pattern = s.Pattern,
					Remark = int.Parse(s.Remark),
					Color = s.Color.ToString(),
					Season = s.Season.ToString(),
					Style = s.Style,
					Fit = s.Fit,
					Pic1 = s.Pic1_Big,
					Pic2 = Pic2,
					Pic3 = Pic3,
					Pic4 = Pic4,
					Pic5 = Pic5
				};
				obj.Add(WearClothesData);

			}
			#endregion

			#region 再取未設定前100件
			var NotPromotion100 = db.GoodsSN_API_View.Where(w => w.Goods_Status == 3 && w.Pic1_Big != null && w.GoodsId != 0 && w.Goods_Promotion100 == false).ToList();

			NotPromotion100 = NotPromotion100.OrderBy(emp => Guid.NewGuid()).ToList();

			foreach (var s in NotPromotion100)
			{
				switch (Pic_Size)
				{
					case "Big":
						Pic2 = s.Pic2_Big;
						Pic3 = s.Pic3_Big;
						Pic4 = s.Pic4_Big;
						Pic5 = s.Pic5_Big;
						break;
					case "Middle":
						Pic2 = s.Pic2_Middle;
						Pic3 = s.Pic3_Middle;
						Pic4 = s.Pic4_Middle;
						Pic5 = s.Pic5_Middle;
						break;
					case "Small":
						Pic2 = s.Pic2_Small;
						Pic3 = s.Pic3_Small;
						Pic4 = s.Pic4_Small;
						Pic5 = s.Pic5_Small;
						break;
				}

				WearClothesData = new WearClothesClass
				{
					GoodsId = s.GoodsId ?? 0,
					GoodsSN = s.SN,
					Goods_Name = s.Goods_Name,
					Goods_PriceId = Get_Goods_PriceId(s.Goods_Price.Value),
					Goods_Price = s.Goods_Price.Value,
					Brand_Name = s.Brand_Name,
					Pattern = s.Pattern,
					Remark = int.Parse(s.Remark),
					Color = s.Color.ToString(),
					Season = s.Season.ToString(),
					Style = s.Style,
					Fit = s.Fit,
					Pic1 = s.Pic1_Big,
					Pic2 = Pic2,
					Pic3 = Pic3,
					Pic4 = Pic4,
					Pic5 = Pic5
				};
				obj.Add(WearClothesData);

			}
			#endregion

			#endregion

			#region 項鍊、配件、人偶圖
			var WearPic = db.WearPic_View.Where(w => w.Display == true && w.Pic1_Big != null).ToList();
			foreach (var s in WearPic)
			{
				switch (Pic_Size)
				{
					case "Big":
						Pic2 = s.Pic2_Big;
						break;
					case "Middle":
						Pic2 = s.Pic2_Middle;
						break;
					case "Small":
						Pic2 = s.Pic2_Small;
						break;
				}

				switch (s.Type)
				{
					//配件類加上"F"
					case "S":
					case "N":
					case "M":
						s.Type = "F" + s.Type;
						break;
					//人偶圖的代號P，改成M
					case "P":
						s.Type = "M";
						break;
					default:
						break;
				}

				WearClothesData = new WearClothesClass
				{
					GoodsId = 0,
					Pattern = s.Type,
					Remark = 0,
					Pic1 = s.Pic1_Big,
					Pic2 = Pic2,
				};
				obj.Add(WearClothesData);

			}
			#endregion

			db.Connection.Close();
			return obj;
		}

		private static int Get_Goods_PriceId(int Goods_Price = 0)
		{
			//對應Category的Number
			int Number = 0;

			//價格 < 1000
			if (Goods_Price <= 1000)
			{
				Number = 3;
			}
			//價格 1000 ~ 3000
			else if (Goods_Price > 1000 && Goods_Price <= 3000)
			{
				Number = 4;
			}
			//價格 3000 ~ 5000
			else if (Goods_Price > 3000 && Goods_Price <= 5000)
			{
				Number = 5;
			}
			//價格 > 5000
			else
			{
				Number = 6;
			}
			return Number;
		}

		/// <summary>
		/// 取得百變穿搭搜尋
		/// </summary
		/// <returns>單筆</returns>
		public static object Get_WearSearchData()
		{
			MithDataContext db = new MithDataContext();
			WearSearchClass WearSearchData = new WearSearchClass();
			List<object> obj = new List<object>();

			var Get_Color = from i in db.Category
											where i.Fun_Id == 8 && i.Group == "Color" && i.Display == true || i.Group == "ApiSearch"
											orderby i.Sort
											select new CategoryClass()
											{
												Id = i.Number.Value,
												Show = i.Remark,
											};

			var Get_Season = from i in db.Category
											 where i.Fun_Id == 8 && i.Group == "Season" && i.Display == true || i.Group == "ApiSearch"
											 orderby i.Sort
											 select new CategoryClass()
											 {
												 Id = i.Number.Value,
												 Show = i.Remark,
											 };

			var Get_Style = from i in db.Category
											where i.Fun_Id == 8 && i.Group == "Style" && i.Display == true || i.Group == "ApiSearch"
											orderby i.Sort
											select new CategoryClass()
											{
												Id = i.Number.Value,
												Show = i.Name,
											};

			var Get_Fit = from i in db.Category
										where i.Fun_Id == 8 && i.Group == "Fit" && i.Display == true || i.Group == "ApiSearch"
										orderby i.Sort
										select new CategoryClass()
										{
											Id = i.Number.Value,
											Show = i.Name,
										};

			var Get_Price = from i in db.Category
											where i.Fun_Id == 8 && i.Group == "Price" && i.Display == true || i.Group == "ApiSearch"
											orderby i.Sort
											select new CategoryClass()
											{
												Id = i.Number.Value,
												Show = i.Name,
											};

			WearSearchData = new WearSearchClass
			{
				ColorList = Get_Color.ToList(),
				SeasonList = Get_Season.ToList(),
				StyleList = Get_Style.ToList(),
				FitList = Get_Fit.ToList(),
				PriceList = Get_Price.ToList()
			};
			obj.Add(WearSearchData);
			db.Connection.Close();
			return obj;
		}
		#endregion

		#region  商品
		/// <summary>
		/// 取得商品內頁(Big)
		/// </summary>
		/// <param name="GoodsId">商品Id</param>
		/// <returns>單筆</returns>
		public static object Get_GoodsData_Big(int GoodsId)
		{
			MithDataContext db = new MithDataContext();
			var data = db.Goods_Edit_View.Where(w => w.Status == 3 && w.Id == GoodsId);
			var Goods_SN = db.GoodsSN_API_View;
			GoodsClass GoodsData = new GoodsClass();
			List<object> obj = new List<object>();
			string LimitOrderRange = null;
			bool LimitOrderStart = false;
			bool LimitOrderEnd = false;
			DateTime today = DateTime.UtcNow.AddHours(8);

			if (data != null)
			{
				foreach (var s in data)
				{
					//取得商品List
					var Get_GoodsPic = from i in db.GoodsPic
														 where i.GoodsId == GoodsId
														 select new GoodsPicClass()
														 {
															 Pic = i.Pic_Big,
														 };

					//取得數量List
					var Get_GoodsSize = from i in db.GoodsQuantity
															where i.GoodsId == GoodsId
															select new GoodsSizeClass()
															{
																Size = i.Size,
																Quantity = i.Quantity.Value
															};

					//取得推薦商品List
					var Get_RecommendGoods =
									from i in db.Goods_Edit_View
									where i.Id != GoodsId && i.Pattern == s.Pattern && i.Season == s.Season && i.Color == s.Color
									select new RecommendClass()
									{
										GoodsId = i.Id,
										Pic1 = Goods_SN.FirstOrDefault(f => f.SN == i.SN).Pic1_Big
									};

					List<RecommendClass> RecommendGoodsList = new List<RecommendClass>();
					int TotalCount = Get_RecommendGoods.ToList().Count;
					Random rd = new Random();//亂數種子

					//同質性商品數量>3
					if (TotalCount > 3)
					{
						//取得三筆資料
						for (int i = 0; i < 3; i++)
						{
							//k的範圍是 > 0，然後 < 同質性商品數量
							int k = rd.Next(0, TotalCount);

							//隨機取得List其中一筆
							RecommendClass Recommend = Get_RecommendGoods.ToList()[k];

							//判斷GoodId是否重複
							int Count = RecommendGoodsList.Where(p => p.GoodsId == Recommend.GoodsId).Count();
							//沒有重複
							if (Count == 0)
							{
								//新增到List裡面
								RecommendGoodsList.Add(Recommend);
							}
							//如果有重複
							else
							{
								while (Count != 0)
								{
									//再隨抽抽取N次
									k = rd.Next(0, TotalCount);
									Recommend = Get_RecommendGoods.ToList()[k];
									Count = RecommendGoodsList.Where(p => p.GoodsId == Recommend.GoodsId).Count();
									if (Count == 0)
									{
										//新增到List裡面
										RecommendGoodsList.Add(Recommend);
									}
								}
							}
						}
					}
					else
					{
						RecommendGoodsList = Get_RecommendGoods.ToList();
					}
					if (s.LimitOrderStart != null)
					{
						LimitOrderRange = s.LimitOrderStart.Value.ToString("yyyy/MM/dd");
						//如果今天日期>集單開始日期，狀態為 true =開始
						if (today > s.LimitOrderStart.Value)
						{
							LimitOrderStart = true;
						}
					}
					if (s.LimitOrderEnd != null)
					{
						LimitOrderRange += "-\r\n" + s.LimitOrderEnd.Value.ToString("yyyy/MM/dd");
						//如果今天日期>集單結束日期，狀態為 true = 結束
						if (today > s.LimitOrderEnd.Value)
						{
							LimitOrderEnd = true;
						}
					}
					string ShowText = db.Other.FirstOrDefault(f => f.Id == s.Type).Content;
					GoodsData = new GoodsClass
					{
						Name = s.Name,
						Price = s.Price,
						SpecialOffer = s.SpecialOffer ?? null,
						Content = HttpUtility.HtmlDecode(s.Content),
						Type = s.Type.Value,
						Type_Name = s.Type_Name,
						LimitOrderRange = LimitOrderRange,
						LimitOrderStart = LimitOrderStart,
						LimitOrderEnd = LimitOrderEnd,
						FewDaysShipping = s.FewDaysShipping ?? null,
						Color_Name = s.Color_Name,
						PicSize = s.Pic_Size,
						ShowText = HttpUtility.HtmlDecode(ShowText),
						PicList = Get_GoodsPic.ToList(),
						SizeList = Get_GoodsSize.ToList(),
						RecommendList = RecommendGoodsList
					};
					obj.Add(GoodsData);

				}
			}
			db.Connection.Close();
			return obj;
		}

		/// <summary>
		/// 取得商品內頁(Middle)
		/// </summary>
		/// <param name="GoodsId">商品Id</param>
		/// <returns>單筆</returns>
		public static object Get_GoodsData_Middle(int GoodsId)
		{
			MithDataContext db = new MithDataContext();
			var data = db.Goods_Edit_View.Where(w => w.Status == 3 && w.Id == GoodsId);
			var Goods_SN = db.GoodsSN_API_View;
			GoodsClass GoodsData = new GoodsClass();
			List<object> obj = new List<object>();
			string LimitOrderRange = null;
			bool LimitOrderStart = false;
			bool LimitOrderEnd = false;
			DateTime today = DateTime.UtcNow.AddHours(8);

			if (data != null)
			{
				foreach (var s in data)
				{
					//取得商品List
					var Get_GoodsPic = from i in db.GoodsPic
														 where i.GoodsId == GoodsId
														 select new GoodsPicClass()
														 {
															 Pic = i.Pic_Middle,
														 };

					//取得數量List
					var Get_GoodsSize = from i in db.GoodsQuantity
															where i.GoodsId == GoodsId
															select new GoodsSizeClass()
															{
																Size = i.Size,
																Quantity = i.Quantity.Value
															};

					//取得推薦商品List
					var Get_RecommendGoods =
									from i in db.Goods_Edit_View
									where i.Id != GoodsId && i.Pattern == s.Pattern && i.Season == s.Season && i.Color == s.Color
									select new RecommendClass()
									{
										GoodsId = i.Id,
										Pic1 = Goods_SN.FirstOrDefault(f => f.SN == i.SN).Pic1_Big
									};

					List<RecommendClass> RecommendGoodsList = new List<RecommendClass>();
					int TotalCount = Get_RecommendGoods.ToList().Count;
					Random rd = new Random();//亂數種子

					//同質性商品數量>3
					if (TotalCount > 3)
					{
						//取得三筆資料
						for (int i = 0; i < 3; i++)
						{
							//k的範圍是 > 0，然後 < 同質性商品數量
							int k = rd.Next(0, TotalCount);

							//隨機取得List其中一筆
							RecommendClass Recommend = Get_RecommendGoods.ToList()[k];

							//判斷GoodId是否重複
							int Count = RecommendGoodsList.Where(p => p.GoodsId == Recommend.GoodsId).Count();
							//沒有重複
							if (Count == 0)
							{
								//新增到List裡面
								RecommendGoodsList.Add(Recommend);
							}
							//如果有重複
							else
							{
								while (Count != 0)
								{
									//再隨抽抽取N次
									k = rd.Next(0, TotalCount);
									Recommend = Get_RecommendGoods.ToList()[k];
									Count = RecommendGoodsList.Where(p => p.GoodsId == Recommend.GoodsId).Count();
									if (Count == 0)
									{
										//新增到List裡面
										RecommendGoodsList.Add(Recommend);
									}
								}
							}
						}
					}
					else
					{
						RecommendGoodsList = Get_RecommendGoods.ToList();
					}
					if (s.LimitOrderStart != null)
					{
						LimitOrderRange = s.LimitOrderStart.Value.ToString("yyyy/MM/dd");
						//如果今天日期>集單開始日期，狀態為 true =開始
						if (today > s.LimitOrderStart.Value)
						{
							LimitOrderStart = true;
						}
					}
					if (s.LimitOrderEnd != null)
					{
						LimitOrderRange += "-\r\n" + s.LimitOrderEnd.Value.ToString("yyyy/MM/dd");
						//如果今天日期>集單結束日期，狀態為 true = 結束
						if (today > s.LimitOrderEnd.Value)
						{
							LimitOrderEnd = true;
						}
					}
					string ShowText = db.Other.FirstOrDefault(f => f.Id == s.Type).Content;
					GoodsData = new GoodsClass
					{
						Name = s.Name,
						Price = s.Price,
						SpecialOffer = s.SpecialOffer ?? null,
						Content = HttpUtility.HtmlDecode(s.Content),
						Type = s.Type.Value,
						Type_Name = s.Type_Name,
						LimitOrderRange = LimitOrderRange,
						LimitOrderStart = LimitOrderStart,
						LimitOrderEnd = LimitOrderEnd,
						FewDaysShipping = s.FewDaysShipping ?? null,
						Color_Name = s.Color_Name,
						PicSize = s.Pic_Size,
						ShowText = HttpUtility.HtmlDecode(ShowText),
						PicList = Get_GoodsPic.ToList(),
						SizeList = Get_GoodsSize.ToList(),
						RecommendList = RecommendGoodsList
					};
					obj.Add(GoodsData);

				}
			}
			db.Connection.Close();
			return obj;
		}

		/// <summary>
		/// 取得商品內頁(Small)
		/// </summary>
		/// <param name="GoodsId">商品Id</param>
		/// <returns>單筆</returns>
		public static object Get_GoodsData_Small(int GoodsId)
		{
			MithDataContext db = new MithDataContext();
			var data = db.Goods_Edit_View.Where(w => w.Status == 3 && w.Id == GoodsId);
			var Goods_SN = db.GoodsSN_API_View;
			GoodsClass GoodsData = new GoodsClass();
			List<object> obj = new List<object>();
			string LimitOrderRange = null;
			bool LimitOrderStart = false;
			bool LimitOrderEnd = false;
			DateTime today = DateTime.UtcNow.AddHours(8);

			if (data != null)
			{
				foreach (var s in data)
				{
					//取得商品List
					var Get_GoodsPic = from i in db.GoodsPic
														 where i.GoodsId == GoodsId
														 select new GoodsPicClass()
														 {
															 Pic = i.Pic_Small,
														 };

					//取得數量List
					var Get_GoodsSize = from i in db.GoodsQuantity
															where i.GoodsId == GoodsId
															select new GoodsSizeClass()
															{
																Size = i.Size,
																Quantity = i.Quantity.Value
															};

					//取得推薦商品List
					var Get_RecommendGoods =
									from i in db.Goods_Edit_View
									where i.Id != GoodsId && i.Pattern == s.Pattern && i.Season == s.Season && i.Color == s.Color
									select new RecommendClass()
									{
										GoodsId = i.Id,
										Pic1 = Goods_SN.FirstOrDefault(f => f.SN == i.SN).Pic1_Big
									};

					List<RecommendClass> RecommendGoodsList = new List<RecommendClass>();
					int TotalCount = Get_RecommendGoods.ToList().Count;
					Random rd = new Random();//亂數種子

					//同質性商品數量>3
					if (TotalCount > 3)
					{
						//取得三筆資料
						for (int i = 0; i < 3; i++)
						{
							//k的範圍是 > 0，然後 < 同質性商品數量
							int k = rd.Next(0, TotalCount);

							//隨機取得List其中一筆
							RecommendClass Recommend = Get_RecommendGoods.ToList()[k];

							//判斷GoodId是否重複
							int Count = RecommendGoodsList.Where(p => p.GoodsId == Recommend.GoodsId).Count();
							//沒有重複
							if (Count == 0)
							{
								//新增到List裡面
								RecommendGoodsList.Add(Recommend);
							}
							//如果有重複
							else
							{
								while (Count != 0)
								{
									//再隨抽抽取N次
									k = rd.Next(0, TotalCount);
									Recommend = Get_RecommendGoods.ToList()[k];
									Count = RecommendGoodsList.Where(p => p.GoodsId == Recommend.GoodsId).Count();
									if (Count == 0)
									{
										//新增到List裡面
										RecommendGoodsList.Add(Recommend);
									}
								}
							}
						}
					}
					else
					{
						RecommendGoodsList = Get_RecommendGoods.ToList();
					}
					if (s.LimitOrderStart != null)
					{
						LimitOrderRange = s.LimitOrderStart.Value.ToString("yyyy/MM/dd");
						//如果今天日期>集單開始日期，狀態為 true =開始
						if (today > s.LimitOrderStart.Value)
						{
							LimitOrderStart = true;
						}
					}
					if (s.LimitOrderEnd != null)
					{
						LimitOrderRange += "-\r\n" + s.LimitOrderEnd.Value.ToString("yyyy/MM/dd");
						//如果今天日期>集單結束日期，狀態為 true = 結束
						if (today > s.LimitOrderEnd.Value)
						{
							LimitOrderEnd = true;
						}
					}
					string ShowText = db.Other.FirstOrDefault(f => f.Id == s.Type).Content;
					GoodsData = new GoodsClass
					{
						Name = s.Name,
						Price = s.Price,
						SpecialOffer = s.SpecialOffer ?? null,
						Content = HttpUtility.HtmlDecode(s.Content),
						Type = s.Type.Value,
						Type_Name = s.Type_Name,
						LimitOrderRange = LimitOrderRange,
						LimitOrderStart = LimitOrderStart,
						LimitOrderEnd = LimitOrderEnd,
						FewDaysShipping = s.FewDaysShipping ?? null,
						Color_Name = s.Color_Name,
						PicSize = s.Pic_Size,
						ShowText = HttpUtility.HtmlDecode(ShowText),
						PicList = Get_GoodsPic.ToList(),
						SizeList = Get_GoodsSize.ToList(),
						RecommendList = RecommendGoodsList
					};
					obj.Add(GoodsData);

				}
			}
			db.Connection.Close();
			return obj;
		}


		/// <summary>
		/// 取得商品的Q&A
		/// </summary>
		public static object Get_GoodsQAData(int GoodsId = 0)
		{
			MithDataContext db = new MithDataContext();
			var data = db.GoodsQA_View.Where(w => w.GoodsId == GoodsId).ToList();
			GoodsQAClass GoodsQAData = new GoodsQAClass();
			List<object> obj = new List<object>();

			foreach (var s in data)
			{

				GoodsQAData = new GoodsQAClass
				{
					GoodsId = s.GoodsId,
					Goods_Name = s.Goods_Name,
					Volunteers_Name = s.API_Volunteers_Name,
					Volunteers_Pic = s.Volunteers_Pic,
					MessageTitle = s.MessageTitle,
					Message = HttpUtility.HtmlDecode(s.Message),
					CreateTime = s.CreateTime.Value.AddHours(-8),  //檢視表裡的CreateTime是 +8
					ReplyMessage = s.ReplyMessage,
					ReplyTime = s.ReplyTime
				};
				obj.Add(GoodsQAData);

			}

			db.Connection.Close();
			return obj;
		}

		/// <summary>
		/// 取得購物車的商品
		/// </summary>
		public static object Get_GoodsShoppingCartData(int VolunteersId = 0)
		{
			MithDataContext db = new MithDataContext();
			var data = db.GoodsShoppingCart_View.Where(w => w.VolunteersId == VolunteersId).ToList();

			GoodsShoppingCartClass GoodsShoppingCartData = new GoodsShoppingCartClass();
			List<object> obj = new List<object>();

			DateTime today = DateTime.UtcNow.AddHours(8);

			foreach (var s in data)
			{
				bool LimitOrderEnd = false;
				if (s.Goods_Type == 3)
				{
					//如果今天日期>集單結束日期，狀態為 true = 結束
					if (today > s.Goods_LimitOrderEnd.Value)
					{
						LimitOrderEnd = true;
					}
				}

				//取得數量List
				var Get_GoodsSize = from i in db.GoodsQuantity
														where i.GoodsId == s.GoodsId
														select new GoodsSizeClass()
														{
															Size = i.Size,
															Quantity = i.Quantity.Value
														};

				GoodsShoppingCartData = new GoodsShoppingCartClass
				{
					GoodsShoppingCartId = s.GoodsShoppingCartId,
					GoodsId = s.GoodsId,
					Goods_Name = s.Goods_Name,
					Goods_Content = HttpUtility.HtmlDecode(s.Goods_Content),
					Goods_Price = s.Goods_Price.Value,
					Goods_SpecialOffer = s.Goods_SpecialOffer,
					Goods_Type = s.Goods_Type.Value,
					Goods_LimitOrderEnd = LimitOrderEnd,
					Goods_Pic = s.Goods_Pic,
					BuySize = s.BuySize,
					BuyQuantity = s.BuyQuantity.Value,
					SizeList = Get_GoodsSize.ToList(),
				};
				obj.Add(GoodsShoppingCartData);

			}

			db.Connection.Close();
			return obj;
		}
		#endregion

		#region 達人

		/// <summary>
		/// 取得達人List
		/// </summary>
		/// <returns>List</returns>
		public static object Get_ExpertData(int ExpertId = 0, int VolunteersId = 0)
		{
			MithDataContext db = new MithDataContext();
			List<object> obj = new List<object>();

			var data = db.Expert_View.ToList();

			if (ExpertId > 0)
			{
				data = data.Where(w => w.Id == ExpertId).ToList();
			}
			else
			{
				data = data.Where(w => w.Enable == true).ToList();

			}

			ExpertListClass ExpertListData = new ExpertListClass();

			#region 計算達人過去一個月追蹤總和
			List<Article_View> ExpertLikeData = db.ExecuteQuery<Article_View>
				(@"Select " +
									"ExpertId, " +
									"Count (ExpertId) as [Like] " +
					"From LikeExpert " +
					"Where DATEADD(hh,8,CreateTime)  >= DATEADD(hh,8, DateAdd(m,-1,GetDate()))" +
					"And ExpertId > 0" +
					"Group By " +
									"ExpertId "
				).ToList();
			#endregion

			foreach (var s in data)
			{
				bool VolunteersLike = false;

				#region 會員是否有追蹤此達人
				if (VolunteersId > 0)
				{
					var LikeArticle = db.LikeExpert.FirstOrDefault(f => f.VolunteersId == VolunteersId && f.ExpertId == s.Id);
					if (LikeArticle != null)
					{
						VolunteersLike = true;
					}
				}
				#endregion

				ExpertListData = new ExpertListClass
				{
					ExpertId = s.Id,
					Name = s.Name,
					Pic = s.Pic,
					Cover = s.Cover,
					Url = s.Url,
					Content = HttpUtility.HtmlDecode(s.Content),
					VolunteersLike = VolunteersLike,
					Like = s.Like,
					//過去一個月追蹤達人總和
					ArticleLikeSum = ExpertLikeData.FirstOrDefault(f => f.ExpertId == s.Id) != null ? ExpertLikeData.FirstOrDefault(f => f.ExpertId == s.Id).Like : 0
				};
				obj.Add(ExpertListData);
			}

			obj = obj.OrderBy(emp => Guid.NewGuid()).ToList();

			return obj;
		}

		/// <summary>
		/// 取得追蹤達人List
		/// </summary>
		/// <returns>List</returns>
		public static object Get_LikeExpertData(int VolunteersId)
		{

			MithDataContext db = new MithDataContext();
			List<object> obj = new List<object>();

			var data = db.LikeExpert_View.Where(w => w.VolunteersId == VolunteersId);

			LikeExpertListClass LikeExpertListData = new LikeExpertListClass();

			foreach (var s in data)
			{
				LikeExpertListData = new LikeExpertListClass
				{
					ExpertId = s.ExpertId,
					Expert_Name = s.Expert_Name,
					Expert_Pic = s.Expert_Pic,
				};
				obj.Add(LikeExpertListData);
			}
			return obj;
		}

		#endregion

		#region 文章
		/// <summary>
		/// 取得文章List
		/// </summary>
		/// <param name="From">來自於哪頁：Fashion、Expert</param>
		/// <returns>List</returns>
		public static object Get_ArticleData(string From = "", int ExpertId = 0)
		{

			MithDataContext db = new MithDataContext();
			List<object> obj = new List<object>();
			var data = db.Article_View.Where(w => w.Display == true);
			if (ExpertId > 0)
			{
				data = data.Where(w => w.ExpertId.Value == ExpertId);
			}

			switch (From)
			{
				//取得 造型達人文章
				case "Expert":
					data = data.Where(w => w.ExpertId != null);
					break;
				//取得 時尚新訊文章
				case "Fashion":
					data = data.Where(w => w.ExpertId == null);
					break;
				default:
					data = null;
					break;
			}

			ArticleListClass ArticleListData = new ArticleListClass();
			bool OnTop = false;

			foreach (var s in data)
			{
				if (s.OnTop != null)
				{
					OnTop = s.OnTop.Value;
				}
				ArticleListData = new ArticleListClass
				{
					ArticleId = s.Id,
					ExpertId = s.ExpertId, //造型達人才有
					Expert_Name = s.Expert_Name,  //造型達人才有
					Type_Name = s.Type_Name, //時尚新訊才有
					Cover = s.Cover, //時尚新訊才有
					Title = s.Title,
					Pic = s.Pic1,
					Content = HttpUtility.HtmlDecode(s.Content1),
					HomePage = OnTop,//時尚新訊才有
					CreateTime = s.CreateTime.Value.AddHours(-8),  //檢視表裡的CreateTime是 +8
				};
				obj.Add(ArticleListData);

			}
			return obj;
		}

		/// <summary>
		/// 取得文章細節
		/// </summary>
		/// <param name="ArticleId">文章Id</param>
		/// <returns>List</returns>
		public static object Get_ArticleOneData(int ArticleId = 0, int VolunteersId = 0)
		{

			MithDataContext db = new MithDataContext();
			List<object> obj = new List<object>();
			var data = db.Article_View.FirstOrDefault(w => w.Id == ArticleId);
			ArticleOneClass ArticleOneData = new ArticleOneClass();

			//文章留言List
			var ArticleMessage = from i in db.ArticleMessage_View
													 where i.ArticleId == ArticleId
													 select new ArticleMessageClass()
													 {
														 ArticleMessageId = i.Id,
														 Volunteers_Name = i.API_Volunteers_Name,
														 Volunteers_Pic = i.API_Volunteers_Pic,
														 MessageTitle = i.MessageTitle,
														 Message = HttpUtility.HtmlDecode(i.Message),
														 CreateTime = i.CreateTime.Value.AddHours(-8),
														 //檢視表裡的CreateTime是 +8
														 Reply_Name = i.Expert_Name,
														 ReplyMessage = HttpUtility.HtmlDecode(i.ReplyMessage),
														 ReplyeTime = i.ReplyTime
													 };
			List<RecommendClass> Get_RecommendList = new List<RecommendClass>();
			int GoodsId = 0;
			string Pic = "";
			bool VolunteersLike = false;
			if (data != null)
			{
				#region 取得推薦商品清單

				if (!string.IsNullOrWhiteSpace(data.RecommendSN1))
				{
					GoodsId = db.Goods_Edit_View.FirstOrDefault(f => f.SN == data.RecommendSN1).Id;
					Pic = db.GoodsSN_API_View.FirstOrDefault(f => f.SN == data.RecommendSN1).Pic1_Big;
					Get_RecommendList.Add(new RecommendClass()
					{
						GoodsId = GoodsId,
						Pic1 = Pic
					});
				}
				if (!string.IsNullOrWhiteSpace(data.RecommendSN2))
				{
					GoodsId = db.Goods_Edit_View.FirstOrDefault(f => f.SN == data.RecommendSN2).Id;
					Pic = db.GoodsSN_API_View.FirstOrDefault(f => f.SN == data.RecommendSN2).Pic1_Big;
					Get_RecommendList.Add(new RecommendClass()
					{
						GoodsId = GoodsId,
						Pic1 = Pic
					});
				}
				if (!string.IsNullOrWhiteSpace(data.RecommendSN3))
				{
					GoodsId = db.Goods_Edit_View.FirstOrDefault(f => f.SN == data.RecommendSN3).Id;
					Pic = db.GoodsSN_API_View.FirstOrDefault(f => f.SN == data.RecommendSN3).Pic1_Big;
					Get_RecommendList.Add(new RecommendClass()
					{
						GoodsId = GoodsId,
						Pic1 = Pic
					});
				}
				if (!string.IsNullOrWhiteSpace(data.RecommendSN4))
				{
					GoodsId = db.Goods_Edit_View.FirstOrDefault(f => f.SN == data.RecommendSN4).Id;
					Pic = db.GoodsSN_API_View.FirstOrDefault(f => f.SN == data.RecommendSN4).Pic1_Big;
					Get_RecommendList.Add(new RecommendClass()
					{
						GoodsId = GoodsId,
						Pic1 = Pic
					});
				}
				if (!string.IsNullOrWhiteSpace(data.RecommendSN5))
				{
					GoodsId = db.Goods_Edit_View.FirstOrDefault(f => f.SN == data.RecommendSN5).Id;
					Pic = db.GoodsSN_API_View.FirstOrDefault(f => f.SN == data.RecommendSN5).Pic1_Big;
					Get_RecommendList.Add(new RecommendClass()
					{
						GoodsId = GoodsId,
						Pic1 = Pic
					});
				}

				Get_RecommendList = Get_RecommendList.OrderBy(emp => Guid.NewGuid()).ToList();

				#endregion

				#region 此會員是否按過此文章讚
				if (VolunteersId > 0)
				{
					var LikeArticle = db.LikeArticle.FirstOrDefault(f => f.VolunteersId == VolunteersId && f.ArticleId == ArticleId);
					if (LikeArticle != null)
					{
						VolunteersLike = true;
					}
				}
				#endregion

				ArticleOneData = new ArticleOneClass
				{
					Title = data.Title,
					FromUrl = data.FromUrl,
					Type_Name = data.Type_Name, //時尚新訊才有
					Cover = data.Cover,//時尚新訊才有
					Pic1 = data.Pic1,
					Content1 = HttpUtility.HtmlDecode(data.Content1),
					Pic2 = data.Pic2,
					Content2 = HttpUtility.HtmlDecode(data.Content2),
					Pic3 = data.Pic3,
					Content3 = HttpUtility.HtmlDecode(data.Content3),
					Pic4 = data.Pic4,
					Content4 = HttpUtility.HtmlDecode(data.Content4),
					Pic5 = data.Pic5,
					Content5 = HttpUtility.HtmlDecode(data.Content5),
					VolunteersLike = VolunteersLike,
					Like = data.Like,
					CreateTime = data.CreateTime.Value.AddHours(-8),  //檢視表裡的CreateTime是 +8
					RecommendSN1 = data.RecommendSN1,
					RecommendSN2 = data.RecommendSN2,
					RecommendSN3 = data.RecommendSN3,
					RecommendSN4 = data.RecommendSN4,
					RecommendSN5 = data.RecommendSN5,
					ArticleMessageList = ArticleMessage.ToList(),
					RecommendList = Get_RecommendList
				};
				obj.Add(ArticleOneData);
				return obj;
			}
			else
			{
				return null;
			}
		}

		#endregion

		#region 穿搭諮詢

		/// <summary>
		/// 取得穿搭諮詢List
		/// </summary>
		/// <param name="ExpertId">達人Id</param>
		/// <returns>List</returns>
		public static object Get_StyleAdviceData(int ExpertId = 0)
		{

			MithDataContext db = new MithDataContext();
			List<object> obj = new List<object>();
			var data = db.StyleAdvice_View.Where(w => w.ExpertId == ExpertId).ToList();
			StyleAdviceClass StyleAdviceData = new StyleAdviceClass();

			foreach (var s in data)
			{
				StyleAdviceData = new StyleAdviceClass
				{
					StyleAdviceId = s.Id,
					VolunteersId = s.VolunteersId,
					Volunteers_Name = s.API_Volunteers_Name,
					Volunteers_Pic = s.Volunteers_Pic,
					MessageTitle = s.MessageTitle,
					Message = HttpUtility.HtmlDecode(s.Message),
					CreateTime = s.CreateTime.Value.AddHours(-8),  //檢視表裡的CreateTime是 +8 
					Reply_Name = s.Expert_Name,
					ReplyMessage = HttpUtility.HtmlDecode(s.ReplyMessage),
					ReplyeTime = s.ReplyTime
				};
				obj.Add(StyleAdviceData);

			}
			return obj;
		}

		/// <summary>
		/// 取得私訊List
		/// </summary>
		/// <param name="ExpertId">達人Id</param>
		/// <returns>List</returns>
		public static object Get_PrivateMessageData(int ExpertId = 0, int VolunteersId = 0)
		{

			MithDataContext db = new MithDataContext();
			List<object> obj = new List<object>();
			var data = db.PrivateMessageGroup_View.ToList();
			//取得達人底下的私訊會員
			if (ExpertId != 0)
			{
				data = data.Where(w => w.ExpertId == ExpertId).ToList();
			}

			if (VolunteersId != 0)
			{
				data = data.Where(w => w.VolunteersId == VolunteersId).ToList();
			}

			PrivateMessageClass PrivateMessageData = new PrivateMessageClass();

			foreach (var s in data)
			{
				DateTime UpdateTime = db.PrivateMessageContent.OrderByDescending(o => o.CreateTime).FirstOrDefault(f => f.PrivateMessageGroupId == s.Id).CreateTime;
				PrivateMessageData = new PrivateMessageClass
				{
					PrivateMessageId = s.Id,
					ExpertId = s.ExpertId,
					Expert_Name = s.Expert_Name,
					Expert_Pic = s.Expert_Pic,
					VolunteersId = s.VolunteersId,
					Volunteers_Name = s.API_Volunteers_Name,
					Volunteers_Pic = s.Volunteers_Pic,
					UpdateTime = UpdateTime
				};
				obj.Add(PrivateMessageData);
			}
			return obj;
		}

		/// <summary>
		/// 取得私訊對話內容List
		/// </summary>
		/// <param name="PrivateMessageId">私訊群組Id</param>
		/// <param name="From">身份：Volunteers、Expert</param>
		/// <returns>List</returns>
		public static object Get_PrivateMessageDetailData(int PrivateMessageId = 0, string From = "")
		{

			MithDataContext db = new MithDataContext();
			List<object> obj = new List<object>();
			var data = db.PrivateMessageContent.Where(w => w.PrivateMessageGroupId == PrivateMessageId);
			PrivateMessageDetailClass PrivateMessageDetailData = new PrivateMessageDetailClass();
			try
			{
				//更新最後已讀時間
				var Update = data.OrderByDescending(o => o.Id).FirstOrDefault(w => w.From == From);

				if (Update != null)
				{
					Update.ReadTime = DateTime.UtcNow;
					db.SubmitChanges();
				}

				DateTime? LastReadTime = null;
				switch (From)
				{
					//來自會員
					case "Volunteers":

						#region 取得會員未讀訊息
						var VolunteersNotRead = data.Where(w => w.From == "Expert" && w.Read == false).ToList();
						if (VolunteersNotRead.Any())
						{
							VolunteersNotRead.ForEach(a => a.Read = true);
							db.SubmitChanges();
						}
						#endregion

						LastReadTime = data.FirstOrDefault(f => f.From == "Expert" && f.ReadTime != null) != null ? data.OrderByDescending(o => o.Id).FirstOrDefault(f => f.From == "Expert" && f.ReadTime != null).ReadTime : null;

						break;

					//來自達人
					case "Expert":

						#region 取得達人未讀訊息
						var ExpertNotRead = data.Where(w => w.From == "Volunteers" && w.Read == false).ToList();
						if (ExpertNotRead.Any())
						{
							ExpertNotRead.ForEach(a => a.Read = true);
							db.SubmitChanges();
						}
						#endregion

						LastReadTime = data.FirstOrDefault(f => f.From == "Volunteers" && f.ReadTime != null) != null ? data.OrderByDescending(o => o.Id).FirstOrDefault(f => f.From == "Volunteers" && f.ReadTime != null).ReadTime : null;

						break;
					default:
						//data = null;
						break;

				}


				foreach (var s in data)
				{
					PrivateMessageDetailData = new PrivateMessageDetailClass
					{
						Message = HttpUtility.HtmlDecode(s.Message),
						From = s.From,
						Read = s.Read,//是否已讀
						CreateTime = s.CreateTime,
						LastReadTime = LastReadTime // 最後已讀淑間
					};
					obj.Add(PrivateMessageDetailData);
				}
				return obj;
			}
			catch (Exception e)
			{
				return null;
			}
			finally
			{
				db.Connection.Close();
			}
		}

		#endregion

		#region 訂單

		//取得已結單訂單
		public static object Get_FinalOrderData(int VolunteersId = 0)
		{
			MithDataContext db = new MithDataContext();

			List<Order_Index_View> data = db.ExecuteQuery<Order_Index_View>
				(@"Select " +
					"Id, " +
					"SN, " +
					"Total, " +
					"CreateTime " +
				"From Order_Index_View " +
				"Where VolunteersId = " + VolunteersId +
				"And Status = 2" +
				"Group By " +
				"Id, " +
				"SN , " +
				"Total, " +
				"CreateTime "
									).ToList();


			OrderClass OrderData = new OrderClass();
			List<object> obj = new List<object>();
			foreach (var s in data)
			{
				OrderData = new OrderClass
				{
					OrderId = s.Id,
					OrderSN = s.SN,
					Total = s.Total,
					CreateTime = s.CreateTime.Value.AddHours(-8)
				};
				obj.Add(OrderData);

			}

			db.Connection.Close();
			return obj;
		}

		//取得全部訂單
		public static object Get_OrderDetailData(int VolunteersId = 0, int OrderId = 0, string Pic_Size = "")
		{
			MithDataContext db = new MithDataContext();
			var data = db.Order_API_View.Where(w => w.VolunteersId == VolunteersId).ToList();

			if (OrderId > 0)
			{
				data = data.Where(w => w.OrderId == OrderId).ToList();
			}

			OrderClass OrderData = new OrderClass();
			List<object> obj = new List<object>();
			foreach (var s in data)
			{
				//int Check = data.Where(w => w.RelativeId == s.OrderId).Count();
				//if (Check == 0)
				//{
				string LogisticsUrl = "";
				string BankCode = "";
				string DeadLineRemark = "";
				bool CancelOrder = false;
				int ReturnGoodsQuantity = 0;
				string LimitOrderStatus = "";
				bool ReturnGoods = true;

				//黑貓宅急便
				if (s.LogisticsType == 1)
				{
					LogisticsUrl = "http://www.t-cat.com.tw/inquire/TraceDetail.aspx?ReturnUrl=Trace.aspx&BillID=";
				}
				// 7-11 交貨便
				else
				{
					LogisticsUrl = "https://eservice.7-11.com.tw/e-tracking/search.aspx?";
				}

				//ATM轉帳
				if (s.CashFlowType == 1)
				{
					BankCode = "013 國泰世華";
					DeadLineRemark = "請於 " + s.CreateTime.Value.AddDays(3).ToString("yyyy/MM/dd HH:mm") + " 前匯款至以下ATM帳戶";
				}
				//繳費狀態=未付款，付費方式=ATM轉帳，才可以取消訂單
				if (s.CashFlowStatus == 1 && s.CashFlowType == 1)
				{
					CancelOrder = true;
				}
				//正物流
				if (s.Direction == 1)
				{
					var ReturnOrder = db.Order.Where(f => f.RelativeId == s.OrderId);
					foreach (var item in ReturnOrder)
					{
						var ReturnOrderDetail = db.OrderDetail.FirstOrDefault(f => f.OrderId == item.Id && f.GoodsId == s.GoodsId);
						if (ReturnOrderDetail != null)
						{
							ReturnGoodsQuantity += ReturnOrderDetail.BuyQuantity;
						}
					}
				}
				//逆物流
				else
				{
					var ReturnOrder = db.Order.Where(f => f.RelativeId == s.RelativeId);
					foreach (var item in ReturnOrder)
					{
						var ReturnOrderDetail = db.OrderDetail.FirstOrDefault(f => f.OrderId == item.Id && f.GoodsId == s.GoodsId);
						if (ReturnOrderDetail != null)
						{
							ReturnGoodsQuantity += ReturnOrderDetail.BuyQuantity;
						}
					}
				}
				if (s.Goods_TypeName == "訂製款-集量生產")
				{
					LimitOrderStatus = db.ExecuteQuery<string>
					(@"Select LimitStatus
                      From [dbo].[LimitOrder_Index_View]
                      Where Id = " + s.GoodsId.Value +
							" And Size='" + s.BuySize + "'").FirstOrDefault();
				}

				if (s.ShippingTime != null)
				{
					DateTime Now = DateTime.UtcNow.AddDays(8);
					//出貨時間+10天 < 現在
					if (s.ShippingTime.Value.AddDays(10) < Now)
					{
						//不能退貨
						ReturnGoods = false;
					}
				}


				OrderData = new OrderClass
				{
					OrderId = s.OrderId,
					OrderSN = s.SN,
					ReturnOrderId = s.OrderDetailId.Value,
					ReturnGoodsQuantity = ReturnGoodsQuantity,
					Total = s.Total,
					Status = s.Status,
					LimitOrderStatus = LimitOrderStatus,
					CashFlowType = s.CashFlowType,
					CashFlowType_Name = s.CashFlowType_Name + " (" + s.CashFlowStatus_Name + ")",
					BankCode = BankCode,
					VirtualAccount = s.VirtualAccount,
					DeadLineRemark = DeadLineRemark,
					MithRemark = "Mith不會以任何方式請您變更付款方式" + "\n若有任何問題，可連繫MiTH客服\nmith@mix-with.com",
					GoodsId = s.GoodsId ?? 0,
					Goods_Name = s.Goods_Name,
					Goods_Content = HttpUtility.HtmlDecode(s.Goods_Content),
					Goods_TypeName = s.Goods_TypeName,
					Goods_ColorName = s.Goods_ColorName,
					Goods_Pic = s.Goods_Pic,
					Goods_PicList = Get_GoodsPicList(s.GoodsId ?? 0, Pic_Size).ToList(),
					BuySize = s.BuySize,
					BuyQuantity = s.BuyQuantity.Value,
					BuyPrice = s.BuyPrice.Value,
					BuyTotal = s.BuyTotal.Value,
					LogisticsStatus = s.LogisticsStatus.Value,
					LogisticsStatus_Name = s.LogisticsStatus_Name,
					LogisticsSN = s.LogisticsSN,
					LogisticsUrl = LogisticsUrl + s.LogisticsSN,
					CompanyId = s.CompanyId.Value,
					Company_Name = s.Brand_Name,
					CancelOrder = CancelOrder,
					RetunGoods = ReturnGoods,
					CreateTime = s.CreateTime.Value.AddHours(-8)

				};
				obj.Add(OrderData);
				//}
			}
			db.Connection.Close();
			return obj;
		}

		private static List<GoodsPicClass> Get_GoodsPicList(int GoodsId = 0, string Pic_Size = "")
		{
			MithDataContext db = new MithDataContext();
			try
			{
				if (Pic_Size == "Big")
				{
					var GoodsPicList = from i in db.GoodsPic
														 where i.GoodsId == GoodsId
														 select new GoodsPicClass()
														 {
															 Pic = i.Pic_Big,
														 };
					return GoodsPicList.ToList();
				}
				else if (Pic_Size == "Middle")
				{
					var GoodsPicList = from i in db.GoodsPic
														 where i.GoodsId == GoodsId
														 select new GoodsPicClass()
														 {
															 Pic = i.Pic_Middle,
														 };
					return GoodsPicList.ToList();
				}
				else
				{
					var GoodsPicList = from i in db.GoodsPic
														 where i.GoodsId == GoodsId
														 select new GoodsPicClass()
														 {
															 Pic = i.Pic_Small,
														 };
					return GoodsPicList.ToList();
				}
			}
			catch (Exception e)
			{
				return null;
			}
			finally
			{
				db.Connection.Close();
			}

		}

		/// <summary>
		/// 取得訂單的Q&A
		/// </summary>
		public static object Get_OrderQAData(int OrderId = 0, int VolunteersId = 0, int CompanyId = 0)
		{
			MithDataContext db = new MithDataContext();
			var data = db.OrderQA_View.Where(w => w.OrderId == OrderId && w.VolunteersId == VolunteersId && w.CompanyId == CompanyId).ToList();
			OrderQAClass OrderQAData = new OrderQAClass();
			List<object> obj = new List<object>();

			foreach (var s in data)
			{
				OrderQAData = new OrderQAClass
				{
					Volunteers_Name = s.API_Volunteers_Name,
					Volunteers_Pic = s.Volunteers_Pic,
					MessageTitle = s.MessageTitle,
					Message = HttpUtility.HtmlDecode(s.Message),
					CreateTime = s.CreateTime.Value.AddHours(-8),  //檢視表裡的CreateTime是 +8
					ReplyMessage = s.ReplyMessage,
					ReplyTime = s.ReplyTime
				};
				obj.Add(OrderQAData);
			}

			db.Connection.Close();
			return obj;
		}

		#endregion

		#region 電子發票


		public static void Generate_Invoice(int OrderId)
		{
			MithDataContext db = new MithDataContext();
			var filename = AppDomain.CurrentDomain.BaseDirectory + "Log.txt";
			var sw = new System.IO.StreamWriter(filename, true);
			sw.WriteLine(DateTime.UtcNow.AddHours(8).ToString("yyyy/MM/dd HH:mm:ss") + "   " + "Start");
			try
			{
				string API = "https://einvoice.allpay.com.tw/Invoice/Issue";
				//回傳結果
				string Result = "";
				int TimeStamp = Convert.ToInt32(DateTime.UtcNow.AddHours(8).Subtract(new DateTime(1970, 1, 1)).TotalSeconds); //時間戳記

				var Order = db.Order.FirstOrDefault(f => f.Id == OrderId);

				//預設不列印紙本
				string Print = "0";
				//預設不捐贈發票
				string Donation = "2";
				//統一編號
				string CustomerIdentifier = "";
				//愛心碼
				string LoveCode = "";

				if (Order != null)
				{
					string Volunteers_Email = db.Volunteers.FirstOrDefault(f => f.Id == Order.VolunteersId).Email;
					//消費者要列印紙本發票
					if (Order.PrintPaper == true)
					{
						Print = "1"; //列印紙本
						Donation = "2"; //不能捐贈						
					}

					//消費者要捐贈發票
					if (Order.ContributeReceipt == true)
					{
						Print = "0";//不列印紙本
						Donation = "1"; //捐贈
						LoveCode = "7000"; //世界展望會愛心碼
					}

					//消費者有輸入統一編號
					if (!string.IsNullOrWhiteSpace(Order.TaxId))
					{
						Print = "1"; //列印紙本
						Donation = "2"; //不能捐贈
						CustomerIdentifier = Order.TaxId; //統一編號
					}

					//取得退貨的訂單主檔
					var RelaveOrder = db.Order_Edit_View.Where(f => f.RelativeId == OrderId);
					//退貨List
					List<ReturnOrderClass> RelaveOrderList = new List<ReturnOrderClass>();

					//此訂單有退貨商品
					if (RelaveOrder.Any())
					{
						foreach (var item in RelaveOrder)
						{
							var RelaveOrderDetail = db.OrderDetail_Edit_View.Where(f => f.OrderId == item.Id).ToList();

							foreach (var item2 in RelaveOrderDetail)
							{
								RelaveOrderList.Add(new ReturnOrderClass() { GoodsId = item2.GoodsId, ReturnQuantity = item2.BuyQuantity, ReturnTotal = item2.BuyTotal }); ;
							}
						}
					}

					//訂單細節
					var OrderDetail = db.OrderDetail_Edit_View.Where(w => w.OrderId == OrderId);
					int BuyQuantity = 0;
					int BuyTotal = 0;
					int SalesAmount = 0;
					string ItemName = ""; //購買商品名稱，用"|"隔開
					string ItemCount = ""; //購買商品數量，用"|"隔開
					string ItemWord = ""; //購買商品單位，用"|"隔開
					string ItemPrice = ""; //購買商品單價，用"|"隔開
					string ItemAmount = ""; //購買商品合計，用"|"隔開

					foreach (var item in OrderDetail)
					{
						var CheckOrder = RelaveOrderList.Where(f => f.GoodsId == item.GoodsId);

						//有對應的退貨商品
						if (CheckOrder.Any())
						{
							BuyTotal = item.BuyTotal - CheckOrder.Sum(s => s.ReturnTotal);
							BuyQuantity = item.BuyQuantity - CheckOrder.Sum(s => s.ReturnQuantity);
						}
						else
						{
							BuyTotal = item.BuyTotal;
							BuyQuantity = item.BuyQuantity;
						}
						//至少購買一件
						if (BuyQuantity > 0)
						{
							SalesAmount += BuyTotal;
							ItemName += item.Goods_Name.Replace("|", "") + "|";//以防商品名稱裡有"|"，Replace掉
							ItemCount += BuyQuantity.ToString("") + "|";
							ItemWord += "件|";
							ItemPrice += item.BuyPrice + "|";
							ItemAmount += (item.BuyPrice * BuyQuantity) + "|";
						}
					}
					//去掉最後一個"|"
					ItemName = ItemName.Substring(0, ItemName.Length - 1);
					ItemCount = ItemCount.Substring(0, ItemCount.Length - 1);
					ItemWord = ItemWord.Substring(0, ItemWord.Length - 1);
					ItemPrice = ItemPrice.Substring(0, ItemPrice.Length - 1);
					ItemAmount = ItemAmount.Substring(0, ItemAmount.Length - 1);

					/*******(帶給歐付寶參數)***********************************************************/
					SortedDictionary<string, string> Str = new SortedDictionary<string, string>();
					Str.Add("MerchantID", "1132842");//廠商編號
					Str.Add("RelateNumber", Order.SN);//訂單編號 
					Str.Add("TimeStamp", TimeStamp.ToString());
					Str.Add("CustomerID", "");
					Str.Add("CustomerIdentifier", CustomerIdentifier);
					Str.Add("CustomerName", HttpUtility.UrlEncode(Order.ReceiverName));
					Str.Add("CustomerAddr", HttpUtility.UrlEncode(Order.ReceiverAddress));
					Str.Add("CustomerPhone", Order.ReceiverTel.ToString());
					Str.Add("CustomerEmail", HttpUtility.UrlEncode(Volunteers_Email));
					Str.Add("ClearanceMark", "");
					Str.Add("Print", Print);//是否列印紙本
					Str.Add("Donation", Donation);//是否捐贈發票
					Str.Add("LoveCode", LoveCode); //愛心碼
					Str.Add("CarruerType", "");
					Str.Add("CarruerNum", "");
					Str.Add("TaxType", "1");//若為應稅時，則 value = '1'
					Str.Add("SalesAmount", SalesAmount.ToString());
					Str.Add("ItemPrice", ItemPrice);
					Str.Add("ItemAmount", ItemAmount);
					Str.Add("ItemCount", ItemCount);
					Str.Add("InvType", "07");
					Str.Add("vat", "1");//若為含稅價，則 value = '1 '
					/*******(參數結束)***********************************************************/

					string strConn = "";
					string str_pre = "";
					foreach (var item in Str)
					{
						strConn += string.Format("&{0}={1}", item.Key, item.Value);
					}
					//CheckMacValue 頭加上HashKey，尾加上HashIV
					str_pre += "HashKey=lqoAFYu8w0ejrWgl" + strConn + "&HashIV=qR7ChZ8DUWly7tQi";

					string EncodeStrPost = HttpUtility.UrlEncode(str_pre);
					EncodeStrPost = EncodeStrPost.ToLower();

					//檢查碼要使用MD5加密
					MD5 md5Hasher = MD5.Create();
					byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(EncodeStrPost));
					StringBuilder sBuilder = new StringBuilder();
					for (int i = 0; i < data.Length; i++)
					{
						sBuilder.Append(data[i].ToString("X2"));
					}
					string CheckMacValue = sBuilder.ToString();

					/***********不帶入檢查碼規則之參數***********************/
					Str.Add("ItemName", HttpUtility.UrlEncode(ItemName));
					Str.Add("ItemWord", HttpUtility.UrlEncode(ItemWord));
					Str.Add("InvoiceRemark", HttpUtility.UrlEncode("亞設國際"));
					/**************************************************************/

					Str.Add("CheckMacValue", CheckMacValue);
					string ParameterString = "";
					foreach (var item in Str)
					{
						ParameterString += string.Format("&{0}={1}", item.Key, item.Value);
					}

					using (WebClient wc = new System.Net.WebClient())
					{
						wc.Encoding = Encoding.UTF8;
						wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
						//讀取API回傳結果

						Result = wc.UploadString(API, ParameterString);
						sw.WriteLine(DateTime.UtcNow.AddHours(8).ToString("yyyy/MM/dd HH:mm:ss") + "   " + "Result：" + Result);

						var ResultVal = Result.Split("&");
						SortedDictionary<string, string> ResulList = new SortedDictionary<string, string>();
						for (int i = 0; i < ResultVal.Count(); i++)
						{
							//再拆等於（參數名稱：[0]　參數Value：[1]）
							var item = ResultVal[i].Split("=");
							string Key = item[0].ToString(); //參數名稱
							string Value = item[1].ToString(); // 參數Value
							ResulList.Add(Key, Value);


						}
						string RtnCode = ResulList.FirstOrDefault(f => f.Key == "RtnCode").Value;
						string InvoiceNumber = ResulList.FirstOrDefault(f => f.Key == "InvoiceNumber").Value;
						if (InvoiceNumber != "" && RtnCode == "1")
						{
							Order.InvoiceNumber = InvoiceNumber;
							db.SubmitChanges();

							Send_InvoiceEmail(Volunteers_Email, InvoiceNumber);
						}
					}
				}
			}
			catch (Exception ex)
			{
			}
			finally
			{
				sw.WriteLine("---------------------------------------------------");
				sw.Close();
				db.Connection.Close();
			}
		}

		public static void Send_InvoiceEmail(string Volunteers_Email = "", string InvoiceNumber = "")
		{
			string API = "http://einvoice-stage.allpay.com.tw/Notify/InvoiceNotify";
			//回傳結果
			string Result = "";
			int TimeStamp = Convert.ToInt32(DateTime.UtcNow.AddHours(8).Subtract(new DateTime(1970, 1, 1)).TotalSeconds); //時間戳記

			if (Volunteers_Email != "" && InvoiceNumber != "")
			{
				/*******(參數輸入開始)***********************************************************/
				SortedDictionary<string, string> Str = new SortedDictionary<string, string>();
				Str.Add("InvoiceNo", InvoiceNumber); //發票號碼
				Str.Add("InvoiceTag", "I"); //->若為發票開立時，則 VAL = 'I'
				Str.Add("MerchantID", "1132842"); //廠商編號
				Str.Add("Notified", "C"); //->若為發送通知給客戶時，則 VAL = C
				Str.Add("Notify", "E");//若為電子郵件時，則 VAL = 'E'
				Str.Add("NotifyMail", HttpUtility.UrlEncode(Volunteers_Email));
				Str.Add("TimeStamp", TimeStamp.ToString());
				/*******(參數輸入結束)***********************************************************/

				string strConn = "";
				string str_pre = "";
				foreach (var item in Str)
				{
					strConn += string.Format("&{0}={1}", item.Key, item.Value);
				}
				//CheckMacValue 頭加上HashKey，尾加上HashIV
				str_pre += "HashKey=lqoAFYu8w0ejrWgl" + strConn + "&HashIV=qR7ChZ8DUWly7tQi";

				string EncodeStrPost = HttpUtility.UrlEncode(str_pre);
				EncodeStrPost = EncodeStrPost.ToLower();

				//檢查碼要使用MD5加密
				MD5 md5Hasher = MD5.Create();
				byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(EncodeStrPost));
				StringBuilder sBuilder = new StringBuilder();
				for (int i = 0; i < data.Length; i++)
				{
					sBuilder.Append(data[i].ToString("X2"));
				}
				string CheckMacValue = sBuilder.ToString();
				Str.Add("CheckMacValue", CheckMacValue);
				string ParameterString = "";

				foreach (var item in Str)
				{
					ParameterString += string.Format("&{0}={1}", item.Key, item.Value);
				}

				using (WebClient wc = new System.Net.WebClient())
				{
					wc.Encoding = Encoding.UTF8;
					wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
					//讀取API回傳結果
					Result = wc.UploadString(API, ParameterString);
				}
			}
		}

		#endregion


	}
}