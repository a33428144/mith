﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Linq.Expressions;
using System.Data;
using System.Data.Common;
using System.Data.Linq;

namespace Mith.Models
{
    public static class LINQExtension
    {
        public static IEnumerable<TSource> DistinctBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            HashSet<TKey> knownKeys = new HashSet<TKey>();
            foreach (TSource element in source)
            {
                if (knownKeys.Add(keySelector(element)))
                {
                    yield return element;
                }
            }
        }

        public static IQueryable<TSource> Between<TSource, TKey>(this IQueryable<TSource> source, Expression<Func<TSource, TKey>> keySelector, TKey low, TKey high) where TKey : IComparable<TKey>
        {
            Expression key = Expression.Invoke(keySelector, keySelector.Parameters.ToArray());
            Expression lowerBound = Expression.GreaterThanOrEqual(key, Expression.Constant(low));
            Expression upperBound = Expression.LessThanOrEqual(key, Expression.Constant(high));
            Expression and = Expression.AndAlso(lowerBound, upperBound);
            Expression<Func<TSource, bool>> lambda = Expression.Lambda<Func<TSource, bool>>(and, keySelector.Parameters);
            return source.Where(lambda);
        }

        /// <summary>
        /// 隨機取值
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumerable"></param>
        /// <param name="weightFunc"></param>
        /// <returns></returns>
        public static T Random<T>(this IEnumerable<T> enumerable, Func<T, int> weightFunc)
        {
            if (enumerable == null)
                return default(T);
            if (!enumerable.Any())
                return default(T);

            enumerable = enumerable.OrderBy(o => Guid.NewGuid());

            T select = default(T);
            int totalWeight = 0;
            foreach (var data in enumerable)
            {
                int weight = weightFunc(data);
                Random r = new Random();
                int p = r.Next(totalWeight + weight);
                if (p >= totalWeight)
                {
                    select = data;
                }
                totalWeight += weight;
            }
            return select;
        }

        /// <summary>
        /// 隨機取值含拒絕
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumerable"></param>
        /// <param name="weightFunc"></param>
        /// <param name="reject"></param>
        /// <param name="rejectFunc"></param>
        /// <returns></returns>
        public static T Random<T>(this IEnumerable<T> enumerable, Func<T, int> weightFunc, List<int> reject, Func<T, int> rejectFunc)
        {
            if (enumerable == null)
                return default(T);
            if (!enumerable.Any())
                return default(T);

            if (reject != null)
            {
                if (reject.Any())
                {
                    enumerable = enumerable.Where(w => !reject.Contains(rejectFunc(w)));
                }
            }
            enumerable = enumerable.OrderBy(o => Guid.NewGuid());

            T select = default(T);
            int totalWeight = 0;
            foreach (var data in enumerable)
            {
                int weight = weightFunc(data);
                Random r = new Random();
                int p = r.Next(totalWeight + weight);
                if (p >= totalWeight)
                {
                    select = data;
                }
                totalWeight += weight;
            }
            return select;
        }

        /// <summary>
        /// 重複隨機取值
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumerable"></param>
        /// <param name="weightFunc"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public static List<T> RandomList<T>(this IEnumerable<T> enumerable, Func<T, int> weightFunc, int count)
        {
            if (enumerable == null)
                return new List<T>();
            if (!enumerable.Any())
                return new List<T>();

            if (count >= enumerable.Count())
            {
                return enumerable.OrderBy(o => Guid.NewGuid()).ToList();
            }
            
            T select = default(T);
            List<T> result = new List<T>();

            for (int i = 1; i <= count; i++)
            {
                int totalWeight = 0;
                enumerable = enumerable.OrderBy(o => Guid.NewGuid());
                foreach (var data in enumerable)
                {
                    int weight = weightFunc(data);
                    Random r = new Random();
                    int p = r.Next(totalWeight + weight);
                    if (p >= totalWeight)
                    {
                        select = data;
                    }
                    totalWeight += weight;
                }
                result.Add(select);
            }
            return result;
        }

        /// <summary>
        /// 隨機取值List
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumerable"></param>
        /// <param name="weightFunc"></param>
        /// <param name="count"></param>
        /// <param name="rejectFunc"></param>
        /// <returns></returns>
        public static List<T> RandomList<T>(this IEnumerable<T> enumerable, Func<T, int> weightFunc, int count, Func<T, int> rejectFunc)
        {
            if (enumerable == null)
                return new List<T>();
            if (!enumerable.Any())
                return new List<T>();

            if (count >= enumerable.Count())
            {
                return enumerable.OrderBy(o => Guid.NewGuid()).ToList();
            }

            List<int> reject = new List<int>();
            T select = default(T);
           
            List<T> result = new List<T>();

            for (int i = 1; i <= count; i++)
            {
                enumerable = enumerable.Where(w => !reject.Contains(rejectFunc(w))).OrderBy(o => Guid.NewGuid());
                int totalWeight = 0;
                foreach (var data in enumerable)
                {
                    int weight = weightFunc(data);
                    Random r = new Random();
                    int p = r.Next(totalWeight + weight);
                    if (p >= totalWeight)
                    {
                        select = data;
                    }
                    totalWeight += weight;
                }
                result.Add(select);
                reject.Add(rejectFunc(select));
            }
            return result;
        }

        /// <summary>
        /// 批量更新
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="table">表</param>
        /// <param name="predicate">查询条件表达式</param>
        /// <param name="updater">更新表达式</param>
        /// <returns>影响的行数</returns>
        public static int Update<T>(this Table<T> table, Expression<Func<T, bool>> predicate, Expression<Func<T, T>> updater) where T : class
        {
            string tableName = table.Context.Mapping.GetTable(typeof(T)).TableName;
            DbCommand command = table.Context.GetCommand(table.Where(predicate));
            string sqlCondition = command.CommandText;
            sqlCondition = sqlCondition.Substring(sqlCondition.LastIndexOf("WHERE ", StringComparison.InvariantCultureIgnoreCase) + 6);

            var updateMemberExpr = (MemberInitExpression)updater.Body;
            var updateMemberCollection = updateMemberExpr.Bindings.Cast<MemberAssignment>().Select(c =>
            {
                var p = command.CreateParameter();
                p.ParameterName = c.Member.Name;
                p.Value = ((ConstantExpression)c.Expression).Value;
                return p;
            })
            .ToArray();

            string sqlUpdateBlock = string.Join(", ", updateMemberCollection.Select(c => string.Format("[{0}]=@{0}", c.ParameterName)).ToArray());

            string commandText = string.Format("UPDATE {0} SET {1} FROM {0} AS t0 WHERE {2}", tableName, sqlUpdateBlock, sqlCondition);

            command.Parameters.AddRange(updateMemberCollection);
            command.CommandText = commandText;

            try
            {
                if (command.Connection.State != ConnectionState.Open)
                {
                    command.Connection.Open();
                }
                return command.ExecuteNonQuery();
            }
            finally
            {
                command.Connection.Close();
                command.Dispose();
            }
        }

        public static IEnumerable<T> FindSandwichedItem<T>(this IEnumerable<T> items, Predicate<T> matchFilling)
        {
            if (items == null)
                throw new ArgumentNullException("items");
            if (matchFilling == null)
                throw new ArgumentNullException("matchFilling");

            return FindSandwichedItemImpl(items, matchFilling);
        }

        private static IEnumerable<T> FindSandwichedItemImpl<T>(IEnumerable<T> items, Predicate<T> matchFilling)
        {
            using (var iter = items.GetEnumerator())
            {
                T previous = default(T);
                while (iter.MoveNext())
                {
                    if (matchFilling(iter.Current))
                    {
                        yield return previous;
                        yield return iter.Current;
                        if (iter.MoveNext())
                            yield return iter.Current;
                        else
                            yield return default(T);
                        yield break;
                    }
                    previous = iter.Current;
                }
            }
            // If we get here nothing has been found so return three default values
            yield return default(T); // Previous
            yield return default(T); // Current
            yield return default(T); // Next
        }
    }
}