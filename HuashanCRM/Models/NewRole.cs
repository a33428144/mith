﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Mith.Models;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;

namespace Mith.Models
{

    public class NewAuthorizeAttribute : AuthorizeAttribute
    {
        public string Role { get; set; }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (httpContext == null)
                throw new ArgumentNullException("httpContext");

            string a = System.Web.HttpContext.Current.Session["Mith_Role"].ToString();

            String[] users = Users.Split(',');//取得輸入user清單
            String[] roles = Roles.Split(',');//取得輸入role清單
            if (!httpContext.User.Identity.IsAuthenticated)//判斷是否已驗證
                return false;
            if (!httpContext.Request.Cookies.AllKeys.Contains("Home"))
                return false;

            //if (!Method.Check_Login(httpContext.Request.Cookies, Mith.Models.Method.CookieName_Home))
            //    return false;

            try
            {
                if (httpContext.Request.Cookies[Method.CookieName_Home] == null)
                {
                    return false;
                }

                HttpCookie hc = httpContext.Request.Cookies[Method.CookieName_Home];

                string data = "";
                FormsAuthenticationTicket authTicket = Method.Get_Cookie_Ticket_Home(httpContext.Request.Cookies);

                if (authTicket == null)
                {
                    data = Method.AES(false, hc.Value, Method.AES_Key);
                    if (data == "")
                    {
                        return false;
                    }
                }
                else
                {
                    if (authTicket.Expired)
                        return false;
                    data = authTicket.UserData;
                }

                string[] result = data.Split(',');
                if (result.Count() != 3)
                {
                    return false;
                }

                string account = result[0];
                string name = result[1];
                string password = result[2];



                if (httpContext.Session.IsNewSession)
                {
                    //UserProfileModel.Update_LoginTime(mm.Id);
                    //LoginRecordModel.Login_Record(mm.Account, httpContext.Request, true);
                }
                else
                {
                    /*if ((hc.Expires - DateTime.Now).TotalMinutes <= 60)
                    {
                        LoginCookie(httpContext,hc);
                    }*/
                }


                #region form
                /*FormsAuthenticationTicket authTicket = Method.Get_Cookie_Ticket(httpContext.Request.Cookies, Method.CookieName_Home);

                if (authTicket == null)
                    return false;
                if (!authTicket.IsPersistent && authTicket.Expired)
                    return false;
                if (authTicket.Version != Method.CookieVersion_Home)
                    return false;

                HttpCookie hc = httpContext.Request.Cookies[Method.CookieName_Home];

                if ((DateTime.UtcNow - authTicket.IssueDate).TotalDays > 30)
                {
                    httpContext.Session.RemoveAll();
                    hc.Expires = DateTime.UtcNow.AddDays(-30);
                    httpContext.Response.AppendCookie(hc);
                    return false;
                }
                //string account = Method.Get_Account(httpContext.Request.Cookies, Method.CookieName_Home, httpContext.Session, Method.SessionUserAccount_Home);
                //string account = authTicket.Name;
                //string account = httpContext.User.Identity.Name.ToString(); //登入的使用者帳號

                //var role = VolunteersModel.Get_One(account);
                var data = VolunteersModel.Get_One(authTicket.Name);

                if (data == null)
                    return false;

                if (data.Name + "," + data.Password != authTicket.UserData)
                {
                    httpContext.Session.RemoveAll();
                    hc.Expires = DateTime.UtcNow.AddDays(-30);
                    httpContext.Response.AppendCookie(hc);
                    return false;
                }

                if (httpContext.Session.IsNewSession)
                {
                    //UserProfileModel.Update_LoginTime(mm.Id);
                    //LoginRecordModel.Login_Record(mm.Account, httpContext.Request, true);
                }
                else
                {
                    LoginCookie(httpContext, data, authTicket.IsPersistent);
                }

                if (Method.Get_Session(httpContext.Session, Method.SessionUserAccount_Home) == "")
                {
                    LoginSession(httpContext, data);
                }*/
                #endregion

                return true;
            }
            catch { return false; }
        }

        private void LoginSession(HttpContextBase httpContext, Volunteers item)
        {
            httpContext.Session.RemoveAll();
            httpContext.Session.Add(Method.SessionUserId_Home, item.Id);
            httpContext.Session.Add(Method.SessionUserAccount_Home, item.Email);
            httpContext.Session.Add(Method.SessionUserName_Home, item.RealName);
            httpContext.Session.Add(Method.SessionUserEmail_Home, item.Email);
        }
    }
}