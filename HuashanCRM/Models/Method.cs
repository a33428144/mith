﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.IO;
using System.Text;
using System.Net;
using System.Net.Mail;
using System.Web.Security;
using System.Web.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Security;
using System.Security.Cryptography;
using System.Web.Routing;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Linq;
using NReco.VideoConverter;
using Mith.Areas.GoX.Models;

namespace Mith.Models
{
	public class Method
	{
		public class XmlResult : ActionResult
		{
			private object _objectToSerialize;

			public XmlResult(object objectToSerialize)
			{
				_objectToSerialize = objectToSerialize;
			}

			public override void ExecuteResult(ControllerContext context)
			{
				if (_objectToSerialize != null)
				{
					var xs = new XmlSerializer(_objectToSerialize.GetType());
					context.HttpContext.Response.ContentType = "text/xml";
					xs.Serialize(context.HttpContext.Response.Output, _objectToSerialize);
				}
			}
		}

		#region 宣告

		public static string RootPath
		{
			get { return Get_RootPath(); }
		}
		public static string SiteTitle
		{
			get { return Get_SiteTitle(); }
		}
		public static string SiteDNS
		{
			get { return Get_SiteDNS(); }
		}
		public static string SiteLogo
		{
			get { return Get_SiteLogo(); }
		}
		public static string SiteIcon
		{
			get { return Get_SiteIcon(); }
		}
		public static string SiteWebSEO
		{
			get { return Get_SiteWebSEO(); }
		}
		public static string SiteWebDescription
		{
			get { return Get_SiteWebDescription(); }
		}
		public static bool SiteSSL
		{
			get { return Get_SiteSSL(); }
		}
		public static bool SiteDel
		{
			get { return Get_SiteDel(); }
		}

		public static string CookieName_Home = "Mith_home";
		public static string CookieName_Admin = "Mith_admin";

		//public static int CookieVersion_Home = 94;
		//public static int CookieVersion_Admin = 121;
		public static string Admin_default = "Admin_default";
		public static string Default = "Default";
		public static string Error_Permission = "網址不正確或權限不足!";

		public static string GoX_Default_Controller = "UserProfile";
		public static string GoX_Default_Action = "Login";
		public static string Default_Controller = "Login";
		public static string Default_Action = "Index";

		public static string SessionUserId_Home = "Mith_UserId";
		public static string SessionUserAccount_Home = "Mith_UserAccount";
		public static string SessionUserName_Home = "Mith_UserName";
		public static string SessionUserEmail_Home = "Mith_UserEmail";

		public static string SessionUserId_Admin = "Mith_UserId_admin";
		public static string SessionUserAccount_Admin = "Mith_UserAccount_admin";
		public static string SessionUserName_Admin = "Mith_UserName_admin";
		public static string SessionUserEmail_Admin = "Mith_UserEmail_admin";
		public static string SessionLevel_Admin = "Mith_Level_admin";
		public static string SessionLevelName_Admin = "Mith_LevelName_admin";

		public static string Session_Site_RootPath = "Site_RootPath";
		public static string Session_Site_Title = "Site_Title";
		public static string Session_Site_SSL = "Site_SSL";
		public static string Session_Site_DNS = "Site_DNS";
		public static string Session_Site_Del = "Site_Del";
		public static string Session_Site_Logo = "Site_Logo";
		public static string Session_Site_Icon = "Site_Icon";
		public static string Session_Site_WebSEO = "Site_WebSEO";
		public static string Session_Site_WebDescription = "Site_WebDescription";

		private const string hash = "soohoobook";
		public class Paging
		{
			public int First = 1;
			public int Last = 1;
			public int Total = 1;
			public int Now = 1;
			public int Back = 1;
			public int Next = 1;
			public int Start = 1;
			public int End = 1;
		}

		public class SectionCollectionModel
		{
			[Key]
			public int Id { get; set; }
			[DisplayName("名稱")]
			public String Title { get; set; }
		}

		#endregion

		#region Get

		/// <summary>
		/// 分頁
		/// </summary>
		/// <param name="total">總數量</param>
		/// <param name="current_page">目前頁數</param>
		/// <param name="page_count">單頁數量</param>
		/// <returns>分頁</returns>
		public static Paging Get_Page(int total, int current_page = 1, int page_count = 10, int pages = 5)
		{
			Paging page = new Paging();

			int count = pages;
			int count_com = Convert.ToInt32(Math.Floor((double)count / 2));

			page.First = 1;
			int last = Convert.ToInt32(Math.Ceiling((double)total / (double)page_count));
			page.Last = last <= 0 ? 1 : last;
			page.Total = total;
			page.Now = current_page;

			page.Back = (current_page > 1) ? current_page - 1 : current_page;
			page.Next = (current_page < last) ? current_page + 1 : current_page;

			int start = current_page - count_com;
			page.Start = (start < 1) ? 1 : start;

			int end = page.Start + count - 1;
			end = (end >= last) ? last : end;
			page.End = (end == 0) ? 1 : end;

			return page;
		}

		public static string Get_RootPath()
		{
			try
			{
				return HttpContext.Current.Session[Session_Site_RootPath].ToString();
			}
			catch
			{
				return SiteOptionModel.Get_RootPath();
			}
		}

		public static string Get_SiteTitle()
		{
			try
			{
				return HttpContext.Current.Session[Session_Site_Title].ToString();
			}
			catch
			{
				return SiteOptionModel.Get_Title();
			}
		}

		public static string Get_SiteDNS()
		{
			try
			{
				return HttpContext.Current.Session[Session_Site_DNS].ToString();
			}
			catch
			{
				return SiteOptionModel.Get_DNS();
			}
		}

		public static string Get_SiteLogo()
		{
			try
			{
				return HttpContext.Current.Session[Session_Site_Logo].ToString();
			}
			catch
			{
				return SiteOptionModel.Get_Logo();
			}
		}

		public static string Get_SiteIcon()
		{
			try
			{
				return HttpContext.Current.Session[Session_Site_Icon].ToString();
			}
			catch
			{
				return SiteOptionModel.Get_Icon();
			}
		}

		public static string Get_SiteWebDescription()
		{
			try
			{
				return HttpContext.Current.Session[Session_Site_WebDescription].ToString();
			}
			catch
			{
				return SiteOptionModel.Get_WebDescription();
			}
		}

		public static string Get_SiteWebSEO()
		{
			try
			{
				return HttpContext.Current.Session[Session_Site_WebSEO].ToString();
			}
			catch
			{
				return SiteOptionModel.Get_WebSEO();
			}
		}

		public static bool Get_SiteSSL()
		{
			try
			{
				return (bool)HttpContext.Current.Session[Session_Site_SSL];
			}
			catch
			{
				return SiteOptionModel.Get_SSL();
			}
		}

		public static bool Get_SiteDel()
		{
			try
			{
				return (bool)HttpContext.Current.Session[Session_Site_Del];
			}
			catch
			{
				return SiteOptionModel.Get_Del();
			}
		}

		public static string Get_URLGet(string field, string value)
		{
			if (value != "" && value != null)
			{
				return "&" + field + "=" + value;
			}
			return "";
		}
		public static string Get_URLGet(string field, string[] value)
		{
			if (value != null)
			{
				if (value.Any())
				{
					string tmp = "";
					foreach (var i in value)
					{
						tmp += "&" + field + "=" + i;
					}
					return tmp;
				}
			}
			return "";
		}
		public static string Get_URLGet(string field, int[] value)
		{
			if (value != null)
			{
				if (value.Any())
				{
					string tmp = "";
					foreach (var i in value)
					{
						tmp += "&" + field + "=" + i;
					}
					return tmp;
				}
			}
			return "";
		}

		public static string Get_Form_Params(Dictionary<string, string> param, Encoding e)
		{
			if (param.Any())
			{
				string temp = "";
				foreach (string key in param.Keys)
				{
					temp += HttpUtility.UrlEncode(key, e) + "=" + HttpUtility.UrlEncode(param[key], e) + "&";
				}

				return temp.TrimEnd('&');
			}
			return "";
		}

		private static string Get_Value(JObject j, string key)
		{
			if (j[key] != null)
			{
				return j[key].ToString();
			}
			return "";
		}

		public static RouteValueDictionary Get_AdminRoute(HttpContextBase HttpContext)
		{
			return new RouteValueDictionary
            {
                { "controller", "UserProfile" },
                { "action", "Login" },
                {"ReturnUrl",HttpContext.Request.Url.LocalPath}
            };
		}

		public static RouteValueDictionary Get_Route(HttpContextBase HttpContext)
		{
			return new RouteValueDictionary
            {
                { "controller", "Login" },
                { "action", "Index" },
                {"ReturnUrl",HttpContext.Request.Url.LocalPath}
            };
		}

		#endregion

		#region 取得登入資料
		public static string Get_UserName(HttpCookieCollection hkc, HttpSessionStateBase Session)
		{
			try
			{
				string hkc_name = Method.CookieName_Home;
				string Session_name = Method.SessionUserName_Home;
				string account = Method.Get_Session(Session, Session_name);
				if (account == "")
				{
					if (hkc[hkc_name] != null)
					{
						string data = AES(false, hkc[hkc_name].Value, AES_Key);

						if (data == "")
						{
							FormsAuthenticationTicket authTicket = Method.Get_Cookie_Ticket_Home(hkc);
							if (authTicket == null)
								return "";
							if (authTicket.Expired)
								return "";

							data = authTicket.UserData;
						}
						string[] result = data.Split(',');
						account = result[1];
					}
				}
				return account;
			}
			catch { return ""; }
		}

		public static string Get_UserNameAdmin(HttpCookieCollection hkc, HttpSessionStateBase Session)
		{
			try
			{
				string hkc_name = Method.CookieName_Admin;
				string Session_name = Method.SessionUserName_Admin;
				string account = Method.Get_Session(Session, Session_name);
				if (account == "" || account == "0")
				{
					if (hkc[hkc_name] != null)
					{
						string data = AES(false, hkc[hkc_name].Value, AES_Key);
						if (data == "")
						{
							FormsAuthenticationTicket authTicket = Method.Get_Cookie_Ticket_Admin(hkc);
							if (authTicket == null)
								return "";
							if (authTicket.Expired)
								return "";

							data = authTicket.UserData;
						}
						string[] result = data.Split(',');
						account = result[0];
					}
				}
				return account;
			}
			catch { return ""; }
		}

		public static string Get_Account(HttpCookieCollection hkc, HttpSessionStateBase Session)
		{
			try
			{
				string hkc_name = Method.CookieName_Home;
				string Session_name = Method.SessionUserAccount_Home;
				string account = Method.Get_Session(Session, Session_name);
				if (account == "")
				{
					if (hkc[hkc_name] != null)
					{
						string data = AES(false, hkc[hkc_name].Value, AES_Key);
						if (data == "")
						{
							FormsAuthenticationTicket authTicket = Method.Get_Cookie_Ticket_Home(hkc);
							if (authTicket == null)
								return "";
							if (authTicket.Expired)
								return "";

							data = authTicket.UserData;
						}
						string[] result = data.Split(',');
						account = result[0];
					}
					/*FormsAuthenticationTicket tk = Method.Get_Cookie_Ticket(hkc, hkc_name);
					if (tk == null)
					{
							return "";
					}
					account = tk.Name;*/
				}
				return account;
			}
			catch { return ""; }
		}

		public static string Get_AccountAdmin(HttpCookieCollection hkc, HttpSessionStateBase Session)
		{
			try
			{
				string hkc_name = Method.CookieName_Admin;
				string Session_name = Method.SessionUserAccount_Admin;
				string account = Method.Get_Session(Session, Session_name);
				if (account == "")
				{
					if (hkc[hkc_name] != null)
					{
						string data = AES(false, hkc[hkc_name].Value, AES_Key);
						if (data == "")
						{
							FormsAuthenticationTicket authTicket = Method.Get_Cookie_Ticket_Admin(hkc);
							if (authTicket == null)
								return "";
							if (authTicket.Expired)
								return "";

							data = authTicket.UserData;
						}
						string[] result = data.Split(',');
						account = result[0];
					}
					/*FormsAuthenticationTicket tk = Method.Get_Cookie_Ticket(hkc, hkc_name);
					if (tk == null)
					{
							return "";
					}
					account = tk.Name;*/
				}
				return account;
			}
			catch { return ""; }
		}

		public static int Get_UserId(HttpCookieCollection hkc, HttpSessionStateBase Session)
		{
			try
			{
				VolunteersModel model = new VolunteersModel();
				string hkc_name = Method.CookieName_Home;
				string Session_name = Method.SessionUserId_Home;
				int user_id = Method.Get_Session_Int(Session, Session_name);
				if (user_id <= 0)
				{
					if (hkc[hkc_name] != null)
					{
						string data = AES(false, hkc[hkc_name].Value, AES_Key);
						if (data == "")
						{
							FormsAuthenticationTicket authTicket = Method.Get_Cookie_Ticket_Home(hkc);
							if (authTicket == null)
								return -1;
							if (authTicket.Expired)
								return -1;

							data = authTicket.UserData;
						}
						string[] result = data.Split(',');
						int Id = int.Parse(result[0]);
						var user = model.Get_One(Id);
						if (user == null)
						{
							return -1;
						}
						user_id = user.Id;
					}
				}
				return user_id;
			}
			catch { return -1; }
		}

		public static int Get_UserId_Admin(HttpCookieCollection hkc, HttpSessionStateBase Session)
		{
			try
			{
				string hkc_name = Method.CookieName_Admin;
				string Session_name = Method.SessionUserId_Admin;
				int user_id = 0;
				//if (user_id < 0)
				//{
				if (hkc[hkc_name] != null)
				{
					string data = AES(false, hkc[hkc_name].Value, AES_Key);
					if (data == "")
					{
						FormsAuthenticationTicket authTicket = Method.Get_Cookie_Ticket_Admin(hkc);
						if (authTicket == null)
							return -1;
						if (authTicket.Expired)
							return -1;

						data = authTicket.UserData;
					}
					string[] result = data.Split(',');

					if (SiteOptionModel.Check_SuperAdmin2(result[0], result[2]))
					{
						return 0;
					}

					var user = UserProfileModel.Login_Get_One(result[0]);
					if (user == null)
					{
						return -1;
					}
					user_id = user.Id;
				}
				//}

				return user_id;
			}
			catch { return -1; }
		}

		public static int Get_CompanyId_Admin(HttpCookieCollection hkc, HttpSessionStateBase Session)
		{
			try
			{
				string hkc_name = Method.CookieName_Admin;
				string Session_name = Method.SessionUserId_Admin;
				int companyId = 0;
				//if (user_id < 0)
				//{
				if (hkc[hkc_name] != null)
				{
					string data = AES(false, hkc[hkc_name].Value, AES_Key);
					if (data == "")
					{
						FormsAuthenticationTicket authTicket = Method.Get_Cookie_Ticket_Admin(hkc);
						if (authTicket == null)
							return -1;
						if (authTicket.Expired)
							return -1;

						data = authTicket.UserData;
					}
					string[] result = data.Split(',');

					if (SiteOptionModel.Check_SuperAdmin2(result[0], result[2]))
					{
						return 0;
					}

					var user = UserProfileModel.Login_Get_One(result[0]);
					if (user == null)
					{
						return -1;
					}
					companyId = user.CompanyId ?? -1;
				}
				//}

				return companyId;
			}
			catch { return -1; }
		}

		public static int Get_UserRole_Admin(HttpCookieCollection hkc, HttpSessionStateBase Session)
		{
			try
			{
				string hkc_name = Method.CookieName_Admin;
				string Session_name = Method.SessionUserId_Admin;
				int role = 0;
				//if (user_id < 0)
				//{
				if (hkc[hkc_name] != null)
				{
					string data = AES(false, hkc[hkc_name].Value, AES_Key);
					if (data == "")
					{
						FormsAuthenticationTicket authTicket = Method.Get_Cookie_Ticket_Admin(hkc);
						if (authTicket == null)
							return -1;
						if (authTicket.Expired)
							return -1;

						data = authTicket.UserData;
					}
					string[] result = data.Split(',');

					if (SiteOptionModel.Check_SuperAdmin2(result[0], result[2]))
					{
						return 0;
					}

					var user = UserProfileModel.Login_Get_One(result[0]);
					if (user == null)
					{
						return -1;
					}
					role = user.Role;
				}
				//}

				return role;
			}
			catch { return -1; }
		}

		public static int Get_UserLevel(int UserId)
		{
			try
			{
				MithDataContext db = new MithDataContext();
				int level = db.UserProfile.FirstOrDefault(f => f.Id == UserId).LevelId;
				return level;
			}
			catch { return -1; }
		}

		public static int Get_UserRole(int UserId)
		{
			try
			{
				MithDataContext db = new MithDataContext();
				int role = db.UserProfile.FirstOrDefault(f => f.Id == UserId).Role;
				return role;
			}
			catch { return -1; }
		}

		/// <summary>
		/// 廠商是否為集單廠商
		/// </summary>
		/// <param name="CompanyId">廠商Id</param>
		/// <returns>input</returns>
		public static bool Get_CompanyIsLimitOrder(int CompanyId = 0)
		{
			MithDataContext db = new MithDataContext();
			bool IsLimitOrder = true;
			string GoodsKind = db.Company.FirstOrDefault(f => f.Id == CompanyId).GoodsKind;
			if (!GoodsKind.Contains("3"))
			{
				IsLimitOrder = false;
			}
			db.Connection.Close();
			return IsLimitOrder;
		}

		#endregion

		#region Cookie & Session

		public static FormsAuthenticationTicket Get_Cookie_Ticket_Home(HttpCookieCollection hkc)
		{
			if (!hkc.AllKeys.Contains(Method.CookieName_Home))
				return null;

			try
			{
				HttpCookie hc = hkc[Method.CookieName_Home];
				FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(hc.Value);
				return authTicket;
			}
			catch { }
			return null;
		}

		public static FormsAuthenticationTicket Get_Cookie_Ticket_Admin(HttpCookieCollection hkc)
		{
			if (!hkc.AllKeys.Contains(Method.CookieName_Admin))
				return null;

			try
			{
				HttpCookie hc = hkc[Method.CookieName_Admin];
				FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(hc.Value);
				return authTicket;
			}
			catch { }
			return null;
		}

		public static string Get_Session(HttpSessionStateBase Session, string name)
		{
			if (Session[name] == null)
			{
				return "";
			}
			else
			{
				return Session[name].ToString();
			}
		}

		public static int Get_Session_Int(HttpSessionStateBase Session, string name)
		{
			try
			{
				if (Session[name] == null)
				{
					return -1;
				}
				else
				{
					int result = -1;
					if (Int32.TryParse(Session[name].ToString(), out result))
					{
						return result > 0 ? result : -1;
					}
					else
					{
						return -1;
					}
				}
			}
			catch { return -1; }
		}

		#endregion

		#region 密碼 MD5
		public static string GetMD5(string input, bool small)
		{
			System.Security.Cryptography.MD5 m = System.Security.Cryptography.MD5.Create();
			if (small)
				return BitConverter.ToString(m.ComputeHash(Encoding.UTF8.GetBytes(input))).Replace("-", "").ToLower();
			else
				return BitConverter.ToString(m.ComputeHash(Encoding.UTF8.GetBytes(input))).Replace("-", "");
		}


		/// <summary>
		/// 國泰世華提供的MD5加密
		/// </summary>
		/// <param name="input"></param>
		/// <returns>input</returns>
		public static string CreditCard_MD5(string input)
		{
			input = FormsAuthentication.HashPasswordForStoringInConfigFile(input, "MD5");

			return input;
		}


		public static string Get_HashPassword(string input)
		{
			return Method.GetMD5(hash + input, true);
		}

		public static string AES_Key = "1qaz2wsx3edc";

		/// <summary>
		/// AES
		/// </summary>
		/// <param name="Encrypt">加密 or 解密</param>
		/// <param name="input">明文 or 密文</param>
		/// <param name="key">金鑰</param>
		/// <returns>密文 or 明文</returns>
		public static string AES(bool Encrypt, string input, string key)
		{
			try
			{
				string AES_IV = "sooHOObook!!";
				RijndaelManaged AES = new RijndaelManaged();

				MD5CryptoServiceProvider MD5 = new MD5CryptoServiceProvider();


				byte[] plainTextData = Encrypt ? Encoding.Unicode.GetBytes(input) : Convert.FromBase64String(input);
				byte[] keyData = MD5.ComputeHash(Encoding.Unicode.GetBytes(key));
				byte[] IVData = MD5.ComputeHash(Encoding.Unicode.GetBytes(AES_IV));
				ICryptoTransform transform = Encrypt ? AES.CreateEncryptor(keyData, IVData) : AES.CreateDecryptor(keyData, IVData);
				byte[] outputData = transform.TransformFinalBlock(plainTextData, 0, plainTextData.Length);

				return Encrypt ? Convert.ToBase64String(outputData) : Encoding.Unicode.GetString(outputData);
			}
			catch { return ""; }
		}
		#endregion

		#region 檔案 & Stream

		public static byte[] StreamToBytes(Stream stream)
		{
			byte[] bytes = new byte[stream.Length];
			stream.Read(bytes, 0, bytes.Length);
			// 設置當前流的位置為流的開始 
			stream.Seek(0, SeekOrigin.Begin);
			return bytes;
		}

		public static Stream BytesToStream(byte[] bytes)
		{
			Stream stream = new MemoryStream(bytes);
			return stream;
		}

		public static void StreamToFile(Stream stream, string fileName)
		{
			// 把 Stream 轉換成 byte[] 
			byte[] bytes = new byte[stream.Length];
			stream.Read(bytes, 0, bytes.Length);
			// 設置當前流的位置為流的開始 
			stream.Seek(0, SeekOrigin.Begin);
			// 把 byte[] 寫入文件 
			FileStream fs = new FileStream(fileName, FileMode.Create);
			BinaryWriter bw = new BinaryWriter(fs);
			bw.Write(bytes);
			bw.Close();
			fs.Close();
		}

		public static Stream FileToStream(string fileName)
		{
			// 打開文件 
			FileStream fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read);
			// 讀取文件的 byte[] 
			byte[] bytes = new byte[fileStream.Length];
			fileStream.Read(bytes, 0, bytes.Length);
			fileStream.Close();
			// 把 byte[] 轉換成 Stream 
			Stream stream = new MemoryStream(bytes);
			return stream;
		}

		public static byte[] FileToByte(string fileName)
		{
			// 打開文件 
			FileStream fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read);
			// 讀取文件的 byte[] 
			byte[] bytes = new byte[fileStream.Length];
			fileStream.Read(bytes, 0, bytes.Length);
			fileStream.Close();
			return bytes;
		}

		public static void ByteToFile(byte[] bytes, string fileName)
		{
			Stream stream = new MemoryStream(bytes);
			// 設置當前流的位置為流的開始 
			stream.Seek(0, SeekOrigin.Begin);
			// 把 byte[] 寫入文件 
			FileStream fs = new FileStream(fileName, FileMode.Create);
			BinaryWriter bw = new BinaryWriter(fs);
			bw.Write(bytes);
			bw.Close();
			fs.Close();
		}

		public static byte[] Get_File(string url)
		{
			WebClient webClient = new WebClient();
			return webClient.DownloadData(url);
		}

		public static void Get_File(string url, string path)
		{
			WebClient webClient = new WebClient();
			webClient.DownloadFile(url, path);
		}

		#endregion

		#region 亂數 Random

		public static string RandomNumber(int length, int start, int end)
		{
			Random r = new Random(DateTime.UtcNow.Millisecond);
			int result = r.Next(start, end);

			return string.Format("{0:0000}", result);
		}

		public static string RandomKey(int length, bool number, bool english_s, bool english_b, bool symbol)
		{
			List<string> Code = new List<string>();
			if (!number && !english_s && !english_b && !symbol)
			{
				return "";
			}

			if (number)
			{
				Code.AddRange(new string[] { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" });
			}
			if (english_s)
			{
				Code.AddRange(new string[] { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z" });
			}
			if (english_b)
			{
				Code.AddRange(new string[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" });
			}
			if (symbol)
			{
				Code.AddRange(new string[] { "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "-", "_", "+", "=", "<", ">", "\\", "/", "\"", "'", "{", "}", "[", "]", "|", "~" });
			}

			while (length > Code.Count)
			{
				Code.AddRange(Code);
			}

			return String.Concat(Code.OrderBy(o => Guid.NewGuid()).Take(length));
		}

		#endregion

		#region 登入
		public static bool Is_Login_SuperAdmin(HttpCookieCollection hkc)
		{
			try
			{
				if (hkc.AllKeys.Contains(Method.CookieName_Admin))
				{
					string data = AES(false, hkc[Method.CookieName_Admin].Value, AES_Key);
					if (data == "")
					{
						FormsAuthenticationTicket authTicket = Method.Get_Cookie_Ticket_Admin(hkc);
						if (authTicket == null)
							return false;
						if (authTicket.Expired)
							return false;

						data = authTicket.UserData;
					}
					string[] result = data.Split(',');

					return SiteOptionModel.Check_SuperAdmin2(result[0], result[2]);

				}
			}
			catch { }
			return false;
		}

		public static bool Is_Login_Admin(HttpCookieCollection hkc)
		{
			try
			{
				if (hkc.AllKeys.Contains(Method.CookieName_Admin))
				{
					string data = AES(false, hkc[Method.CookieName_Admin].Value, AES_Key);
					if (data == "")
					{
						FormsAuthenticationTicket authTicket = Method.Get_Cookie_Ticket_Admin(hkc);
						if (authTicket == null)
							return false;
						if (authTicket.Expired)
							return false;

						data = authTicket.UserData;
					}
					string[] result = data.Split(',');
					if (result.Count() != 3)
						return false;

					var mm = UserProfileModel.Login_Get_One(result[0]);

					if (SiteOptionModel.Check_SuperAdmin2(result[0], result[2]))
						return true;

					if (mm == null)
						return false;

					if (mm.Password != result[2])
					{
						return false;
					}
					return true;
				}
			}
			catch { }
			return false;
		}

		public static bool Is_Login(HttpCookieCollection hkc)
		{
			try
			{
				VolunteersModel model = new VolunteersModel();
				if (hkc.AllKeys.Contains(Method.CookieName_Home))
				{
					string data = AES(false, hkc[Method.CookieName_Home].Value, AES_Key);
					if (data == "")
					{
						FormsAuthenticationTicket authTicket = Method.Get_Cookie_Ticket_Home(hkc);
						if (authTicket == null)
							return false;
						if (authTicket.Expired)
							return false;

						data = authTicket.UserData;
					}
					string[] result = data.Split(',');
					if (result.Count() != 3)
						return false;
					int Id = int.Parse(result[0]);
					var user = model.Get_One(Id);

					if (user == null)
						return false;

					if (user.Password != result[2])
					{
						return false;
					}
					return true;
				}
			}
			catch { }
			return false;
		}
		#endregion

		#region 上傳
		public static string Upload_File(HttpPostedFileBase upload, string dir, HttpServerUtilityBase Server)
		{
			string real_url = "/upload/" + dir + "/";
			try
			{
				if (upload != null && upload.ContentLength > 0)
				{
					string img_dir = "~" + real_url;
					if (!System.IO.Directory.Exists(Server.MapPath(img_dir)))
					{
						System.IO.Directory.CreateDirectory(Server.MapPath(img_dir));
					}

					int count = 1;
					string temp = "";
					string time = DateTime.UtcNow.ToString("yyyyMMddHHmmss");
					//string name1 = time; //System.IO.Path.GetFileNameWithoutExtension(upload.FileName);
					string name2 = System.IO.Path.GetExtension(upload.FileName);
					string url = "";// Server.MapPath(img_dir + name1 + name2);

					do
					{
						temp = time + count + name2;
						url = Server.MapPath(img_dir + temp);
						count++;
					} while (System.IO.File.Exists(url));
					upload.SaveAs(url);
					real_url += temp;
				}
				else
				{
					real_url = "";
				}
			}
			catch //(Exception e)
			{
				real_url = "";
				//real_url = e.Message;
			}

			return real_url;
		}

		public static string Upload_Image(HttpPostedFileBase upload, string path, string Id, HttpServerUtilityBase Server, string Pic_Type = "")
		{
			string real_url = "/Upload/" + path + "/Big/" + Id + "/";

			try
			{
				if (upload != null && upload.ContentLength > 0)
				{
					if (!upload.ContentType.ToLower().StartsWith("image"))
					{
						return "";
					}
					string img_dir = "~" + real_url;
					if (!System.IO.Directory.Exists(Server.MapPath(img_dir)))
					{
						System.IO.Directory.CreateDirectory(Server.MapPath(img_dir));
					}
					if (!System.IO.Directory.Exists(Server.MapPath(img_dir.Replace("/Big/", "/Middle/"))))
					{
						System.IO.Directory.CreateDirectory(Server.MapPath(img_dir.Replace("/Big/", "/Middle/")));
					}
					if (!System.IO.Directory.Exists(Server.MapPath(img_dir.Replace("/Big/", "/Small/"))))
					{
						System.IO.Directory.CreateDirectory(Server.MapPath(img_dir.Replace("/Big/", "/Small/")));
					}
					string url = "";// Server.MapPath(img_dir + upload.FileName);
					string file = "";
					//五碼數字
					string name = Method.RandomKey(5, true, false, false, false);
					if (Pic_Type != "")
					{
						name = Pic_Type + name;
					}
					string Extension = System.IO.Path.GetExtension(upload.FileName);

					do
					{
						file = name + Extension;
						url = Server.MapPath(img_dir + file);
					} while (System.IO.File.Exists(url));
					upload.SaveAs(url);
					//upload.SaveAs(Server.MapPath(img_dir.Replace("/Big/", "/Middle/") + temp));
					//upload.SaveAs(Server.MapPath(img_dir.Replace("/Big/", "/Small/") + temp));
					real_url += file;
				}
				else
				{
					real_url = "";
				}
			}
			catch (Exception e)
			{
				ErrorRecordModel.Error_Record(-1, "Method.Upload_Image", "", true, e.Message + "<br />\r\n<br />\r\n" + e.Source + "<br />\r\n<br />\r\n" + e.StackTrace);
				real_url = "";
				//real_url = e.Message;
			}

			return real_url;
		}

		#endregion

		#region 圖片處理
		public static string Catch_Image(string video, HttpServerUtilityBase Server, TimeSpan ts)
		{
			string video_path = Server.MapPath("~" + video);
			string pic = Path.GetDirectoryName(video).Replace("\\", "/") + "/" + Path.GetFileNameWithoutExtension(video) + ".jpg";
			string pic_path = Server.MapPath("~" + pic);
			//FFMpegConverter ff = new FFMpegConverter();
			//FileStream fs = new FileStream(pic_path, FileMode.Create, FileAccess.Write);

			try
			{
				FFMpeg_CatchImage(Server.MapPath("~" + "/bin/ffmpeg.exe"), video_path, pic_path, ts);
				//ff.GetVideoThumbnail(video_path, fs, second);
			}
			catch
			{
				try
				{
					FFMpeg_CatchImage(Server.MapPath("~" + "/bin/ffmpeg.exe"), video_path, pic_path, new TimeSpan(0, 0, 1));
					//ff.GetVideoThumbnail(video_path, fs);
				}
				catch { }
			}

			if (!File.Exists(pic_path))
			{
				pic = "";
			}
			return pic;
		}

		private static void FFMpeg_CatchImage(string exe, string video_path, string pic_path, TimeSpan ts)
		{
			Process p;
			ProcessStartInfo info = new ProcessStartInfo();
			info.FileName = exe;
			info.WindowStyle = ProcessWindowStyle.Hidden;
			info.Arguments = " -ss " + ts.ToString(@"hh\:mm\:ss") + " -i \"" + video_path + "\" \"" + pic_path + "\" -r 1 -vframes 1 -an -f mjpeg";
			p = Process.Start(info);

			while (!p.HasExited) { Thread.Sleep(10); }
		}


		public enum ResizeType
		{
			Auto,
			Height,
			Width,
			Fixed
		}

		public static Image ResizeImage(Stream s, Size size, ResizeType rt = ResizeType.Auto)
		{
			MemoryStream tmp = new MemoryStream();
			byte[] tmp2 = Method.StreamToBytes(s);
			tmp.Write(tmp2, 0, tmp2.Length);
			tmp.Flush();
			return ResizeImage(Image.FromStream(tmp), size, rt);
		}

		public static Image ResizeImage(string s, Size size, ResizeType rt = ResizeType.Auto)
		{
			return ResizeImage(Image.FromFile(s), size, rt);
		}

		public static Image ResizeImage(Image imgToResize, Size size, ResizeType rt = ResizeType.Auto)
		{
			int sourceWidth = imgToResize.Width;
			int sourceHeight = imgToResize.Height;

			int destWidth = size.Width;
			int destHeight = size.Height;

			if (rt != ResizeType.Fixed)
			{
				float nPercent = 0;
				float nPercentW = 0;
				float nPercentH = 0;

				nPercentW = ((float)size.Width / (float)sourceWidth);
				nPercentH = ((float)size.Height / (float)sourceHeight);

				if (rt == ResizeType.Height)
				{
					nPercent = nPercentH;
				}
				else if (rt == ResizeType.Width)
				{
					nPercent = nPercentW;
				}
				else
				{
					if (nPercentH < nPercentW)
						nPercent = nPercentH;
					else
						nPercent = nPercentW;
				}

				if (nPercent > 1.0f)
					nPercent = 1.0f;

				destWidth = (int)Math.Round(((float)sourceWidth * nPercent));
				destHeight = (int)Math.Round(((float)sourceHeight * nPercent));
			}
			Bitmap b = new Bitmap(destWidth, destHeight);
			Graphics g = Graphics.FromImage((Image)b);
			g.InterpolationMode = InterpolationMode.HighQualityBicubic;

			g.DrawImage(imgToResize, 0, 0, destWidth, destHeight);
			g.Dispose();

			return (Image)b;
		}

		public static Stream CompressionImage(string path, int quality, ImageFormat img_format)
		{
			return CompressionImage(Image.FromFile(path), quality, img_format);
		}

		public static Stream CompressionImage(Image img, int quality, ImageFormat img_format)
		{
			Stream s = new MemoryStream();

			ImageCodecInfo code = ImageCodecInfo.GetImageDecoders().FirstOrDefault(f => f.FormatID == img_format.Guid);
			if (code == null)
			{
				return null;
			}

			EncoderParameters myEncoderParameter = new EncoderParameters(1);
			myEncoderParameter.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, quality);
			img.Save(s, code, myEncoderParameter);
			s.Position = 0;
			return s;
		}

		#endregion

		#region 其他

		/// <summary>
		/// 組SQL語法
		/// </summary>
		/// <param name="sql">原始字串</param>
		/// <param name="col">欄位名稱</param>
		/// <param name="logic">連接邏輯(AND,OR)</param>
		/// <param name="value">判斷值</param>
		/// <param name="op">判斷式(=,>)</param>
		/// <param name="value_com">判斷值是否需要加上"</param>
		/// <returns>組合結果</returns>
		public static string SQL_Combin(string sql, string col, string logic, string value, string op, bool value_com)
		{
			op = " " + op + " ";
			logic = " " + logic + " ";
			if (value_com)
			{
				value = "\"" + value + "\"";
			}
			if (sql == "")
			{
				sql = col + op + value;
			}
			else
			{
				sql += logic + col + op + value;
			}

			sql = "(" + sql + ")";

			return sql;
		}

		/// <summary>
		/// 生日轉成年齡
		/// </summary>
		/// <param name="Birthday">生日</param>
		/// <returns>歲數</returns>
		public static int Get_Age(DateTime Birthday)
		{
			DateTime Now = DateTime.UtcNow.AddHours(8);
			int Age = Now.Year - Birthday.Year;
			if (Birthday.Month >= Now.Month)
			{
				if (Birthday.Day > Now.Day)
				{
					return Age - 1;
				}
				else
				{
					return Age;
				}
			}
			else
			{
				return Age;
			}
		}

		#endregion

		#region 下拉式選單

		/// <summary>
		/// 取得Category下拉選單
		/// </summary>
		/// <param name="Group">群組</param>
		/// <param name="Code">值</param>
		/// <returns>選單</returns>
		public static List<SelectListItem> Get_Category_SelectListItem(int Fun_Id, string Group, int num)
		{
			MithDataContext db = new MithDataContext();
			List<SelectListItem> data = new List<SelectListItem>();
			if (Fun_Id == 5 && Group == "Type")
			{
				data.Add(new SelectListItem
				{
					Selected = num == 0,
					Text = "請選擇或輸入文字",
					Value = "-1"
				});
			}
			else
			{
				data.Add(new SelectListItem
				{
					Selected = num == 0,
					Text = "請選擇或輸入文字",
					Value = "0"
				});
			}
			var Category = db.Category.Where(w => w.Fun_Id == Fun_Id && w.Group == Group);
			foreach (var item in Category)
			{
				data.AddRange(new SelectListItem
				{
					Selected = num == item.Number,
					Text = item.Name,
					Value = item.Number.ToString()
				});
			}
			db.Connection.Close();
			return data;
		}

		/// <summary>
		/// 取得款式下拉選單
		/// </summary>
		/// <param name="Group">群組</param>
		/// <param name="Code">值</param>
		/// <returns>選單</returns>
		public static List<SelectListItem> Get_Pattern_SelectListItem(string val = "")
		{
			MithDataContext db = new MithDataContext();
			List<SelectListItem> data = new List<SelectListItem>();
			data.Add(new SelectListItem
			{
				Selected = val == "0",
				Text = "請選擇或輸入文字",
				Value = "0"
			});
			var Category = db.Category.Where(w => w.Fun_Id == 8 && w.Group == "Pattern");
			foreach (var item in Category)
			{
				data.AddRange(new SelectListItem
				{
					Selected = val == item.Remark,
					Text = item.Name,
					Value = item.Remark
				});
			}
			db.Connection.Close();
			return data;
		}

		/// <summary>
		/// 取得廠商下拉選單
		/// </summary>
		/// <param name="companyId">廠商Id</param>
		/// <returns>選單</returns>
		public static List<SelectListItem> Get_Comapny_SelectListItem(int companyId)
		{
			MithDataContext db = new MithDataContext();
			List<SelectListItem> data = new List<SelectListItem>();
			data.Add(new SelectListItem
			{
				Selected = companyId == 0,
				Text = "請選擇或輸入文字",
				Value = "0"
			});
			foreach (var item in db.Company)
			{
				data.AddRange(new SelectListItem
				{
					Selected = companyId == item.Id,
					Text = "【" + item.SN + "】" + item.Name,
					Value = item.Id.ToString()
				});
			}
			db.Connection.Close();
			return data;
		}

		/// <summary>
		/// 取得達人下拉選單
		/// </summary>
		/// <param name="expertId">達人Id</param>
		/// <returns>選單</returns>
		public static List<SelectListItem> Get_Expert_SelectListItem(int expertId)
		{
			MithDataContext db = new MithDataContext();
			List<SelectListItem> data = new List<SelectListItem>();
			data.Add(new SelectListItem
			{
				Selected = expertId == 0,
				Text = "請選擇或輸入文字",
				Value = "0"
			});
			foreach (var item in db.Expert)
			{
				data.AddRange(new SelectListItem
				{
					Selected = expertId == item.Id,
					Text = item.Name,
					Value = item.Id.ToString()
				});
			}
			db.Connection.Close();
			return data;
		}

		/// <summary>
		/// 取得會員下拉選單
		/// </summary>
		/// <param name="VolunteersId">會員Id</param>
		/// <returns>選單</returns>
		public static List<SelectListItem> Get_VolunteersId_SelectListItem(int VolunteersId)
		{
			MithDataContext db = new MithDataContext();
			List<SelectListItem> data = new List<SelectListItem>();
			data.Add(new SelectListItem
			{
				Selected = VolunteersId == 0,
				Text = "請選擇或輸入文字",
				Value = "0"
			});
			foreach (var item in db.Volunteers)
			{
				data.AddRange(new SelectListItem
				{
					Selected = VolunteersId == item.Id,
					Text = item.RealName,
					Value = item.Id.ToString()
				});
			}
			db.Connection.Close();
			return data;
		}

		#endregion

		#region 用於單選&複選紐

		/// <summary>
		/// 取得分類選單
		/// </summary>
		/// <param name="Fun_Id">Fun_Id</param>
		/// <param name="Group">群組</param>
		/// <returns>選單</returns>
		public static List<Category> GetCategoryList(int Fun_Id = 0, string Group = "")
		{
			MithDataContext db = new MithDataContext();
			List<Category> CategoryList = new List<Category>();
			try
			{
				CategoryList = db.Category.OrderBy(o => o.Sort).Where(w => w.Fun_Id == Fun_Id && w.Group == Group && w.Display == true).ToList();
				return CategoryList;
			}
			catch
			{
				return null;
			}
			finally
			{
				db.Connection.Close();

			}
		}
		#endregion

		public static string Get_PatternName(string Remark)
		{
			MithDataContext db = new MithDataContext();
			string name = "";
			name = db.Category.FirstOrDefault(w => w.Remark == Remark).Name;
			db.Connection.Close();
			return name;
		}

		public static string Get_ActivityPicName(int ActivityPicId)
		{
			string name = "";

			switch (ActivityPicId)
			{
				case 1:
					name = "圖片1";
					break;
				case 2:
					name = "圖片2";
					break;
				case 3:
					name = "圖片4";
					break;
				case 4:
					name = "圖片4";
					break;
				case 5:
					name = "圖片5";
					break;
			}
			return name;
		}

		//發送Email
		public static void Send_Mail(string address, string title, string content)
		{
			SiteOptionModel.SiteOptionShow mail_config = SiteOptionModel.Get();
			//目前伺服器架在香港，所以智邦Email那邊，要設定允許香港IP
			string EmailDomain = "mix-with.com";
			string EmailAccount = "mith@mix-with.com";
			string EmailPassword = "a23690013";
			string EmailName = "MiTH App";
			int EmailPort = 25;
			var fromAddress = new MailAddress(EmailAccount, EmailName);
			var toAddress = new MailAddress(address, "To");

			var smtp = new SmtpClient
			{
				Host = EmailDomain,
				Port = EmailPort,
				EnableSsl = false, //沒用SSL
				DeliveryMethod = SmtpDeliveryMethod.Network,
				UseDefaultCredentials = false,
				Credentials = new NetworkCredential(EmailAccount, EmailPassword)
			};
			using (var message = new MailMessage(fromAddress, toAddress)
			{
				Subject = title,
				Body = HttpUtility.HtmlDecode(content)
			})
				try
				{
					//發送
					smtp.Send(message);
				}
				catch (Exception ex)
				{
				}
		}

		//計算結算金額，用於對帳明細
		public static int Get_CalculateTotal(int CashFlowType = 0, int BuyTotal = 0)
		{
			MithDataContext db = new MithDataContext();
			//結算總額
			int CalculateTotal = 0;
			//刷卡處理費用
			int CreditCard_Total = 0;
			//刷卡處理費用
			int Receive_Total = 0;

			if (CashFlowType == 2)
			{
				Receive_Total = int.Parse(db.Other.FirstOrDefault(f => f.Id == 22).Content);
			}
			else
			{
				Receive_Total = int.Parse(db.Other.FirstOrDefault(f => f.Id == 21).Content);
			}

			//Mith平台抽成費用
			int Commission_Total = 0;

			#region 計算刷卡處理費用
			//金流方式 = 信用卡
			if (CashFlowType == 2)
			{
				//刷卡費率 (%)
				double CreditCard_Percent = int.Parse(db.Other.FirstOrDefault(f => f.Id == 20).Content);
				//轉成百分比
				CreditCard_Percent = CreditCard_Percent / 100;
				//四捨五入取整數
				CreditCard_Total = Convert.ToInt16(Math.Round(BuyTotal * CreditCard_Percent, 0));
			}
			#endregion

			#region 計算/Mith平台抽成費用
			//Mith平台抽成(%)
			double Commission_Percent = int.Parse(db.Other.FirstOrDefault(f => f.Id == 23).Content);
			//轉成百分比
			Commission_Percent = Commission_Percent / 100;
			//四捨五入取整數
			Commission_Total = Convert.ToInt16(Math.Round(BuyTotal * Commission_Percent, 0));
			#endregion

			CalculateTotal = BuyTotal - CreditCard_Total - Receive_Total - Commission_Total;

			return CalculateTotal;
		}
	}
}