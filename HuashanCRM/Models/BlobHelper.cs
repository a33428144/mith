﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure;
using System.Diagnostics;
using System.IO;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

namespace Mith.Models
{
	public class BlobFileDesc
	{
		public string sContainer;
		public string sFilePath;
		public string sFilename;
	}
	public class BlobHelper
	{
		private static string connection = "DefaultEndpointsProtocol=https;AccountName=mithblob;AccountKey=WZpMY6BE/roUJnkkgIBxKJrvMqjPgUet8qq0DqEJj+Pnt2ar9upbmACysAiVRlwjYCWN7F0SK7mQZPA4zWXGBA==";

		public static string GetFileContentType(string sFilename)
		{
			if (sFilename.IndexOf(".jpg", StringComparison.OrdinalIgnoreCase) >= 0)
			{
				return "image/jpeg";
			}
			else if (sFilename.IndexOf(".png", StringComparison.OrdinalIgnoreCase) >= 0)
			{
				return "image/png";
			}
			else if (sFilename.IndexOf(".mp3", StringComparison.OrdinalIgnoreCase) >= 0)
			{
				return "audio/mp3";
			}
			else
			{
				return "application/octet-stream";
			}
		}

		/// <summary>
		/// 上傳檔案
		/// </summary>
		public static string[] Upload_Blob(BlobFileDesc aBlobfile, Stream aFileStream, string Name, Size Middle, Size Small, bool Resize)
		{
			string sContainer = aBlobfile.sContainer;
			string sFilePath = aBlobfile.sFilePath;
			string sFilename = Name;
			//CloudStorageAccount storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("AzureStorageConnectionString"));
			CloudStorageAccount storageAccount = CloudStorageAccount.Parse(connection);

			CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
			CloudBlobContainer container =
					blobClient.GetContainerReference(sContainer);

			container.CreateIfNotExists();

			container.SetPermissions(
					new BlobContainerPermissions
					{
						PublicAccess = BlobContainerPublicAccessType.Container
					});
			//container.Exists
			CloudBlockBlob blob = container.GetBlockBlobReference(sFilePath + sFilename);
			int i = 1;
			string s1 = Path.GetFileNameWithoutExtension(sFilename);
			string s2 = Path.GetExtension(sFilename);
			while (Exists(blob))
			{
				sFilename = s1 + "(" + (i++) + ")" + s2;
				blob = container.GetBlockBlobReference(sFilePath + sFilename);
			}

			//Debug.WriteLine(blob.Uri.AbsolutePath.Replace("https://", "http://"));
			blob.UploadFromStream(aFileStream);

			blob.FetchAttributes();
			blob.Properties.ContentType = BlobHelper.GetFileContentType(sFilePath + sFilename);
			blob.SetProperties();

			string[] result = new string[2];
			result[0] = blob.Uri.AbsoluteUri.Replace("https://", "http://");
			result[1] = sFilename;
			//return blob.Uri.AbsoluteUri.Replace("https://", "http://");
			//return sFilename;

			if (Resize == true)
			{
				//縮成中Size
				BlobFileDesc MiddleBlobfile = new BlobFileDesc()
				{
					sContainer = aBlobfile.sContainer,
					sFilePath = sFilePath.Replace("Big", "Middle"),
					sFilename = sFilename
				};
				Resize_Blob(aBlobfile, MiddleBlobfile, Middle);

				//縮成小Size
				BlobFileDesc SmallBlobfile = new BlobFileDesc()
				{
					sContainer = aBlobfile.sContainer,
					sFilePath = sFilePath.Replace("Big", "Small"),
					sFilename = sFilename
				};
				Resize_Blob(aBlobfile, SmallBlobfile, Small);
			}

			return result;
		}

		/// <summary>
		/// 縮小照片尺寸
		/// </summary>
		public static string Resize_Blob(BlobFileDesc aOrgBlobfile, BlobFileDesc aNewBlobfile, Size ReSize)
		{
			double NewWidth = ReSize.Width;
			double NewHeight = ReSize.Height;
			CloudStorageAccount storageAccount = CloudStorageAccount.Parse(connection);
			CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

			CloudBlobContainer orgContainer =
					blobClient.GetContainerReference(aOrgBlobfile.sContainer);
			CloudBlobContainer newContainer =
					blobClient.GetContainerReference(aNewBlobfile.sContainer);
			if (orgContainer.Exists() == false)
				return "";
			newContainer.CreateIfNotExists();
			newContainer.SetPermissions(
					new BlobContainerPermissions
					{
						PublicAccess = BlobContainerPublicAccessType.Container
					});

			CloudBlockBlob orgBlob = orgContainer.GetBlockBlobReference(aOrgBlobfile.sFilePath + aOrgBlobfile.sFilename);
			CloudBlockBlob newBlob = newContainer.GetBlockBlobReference(aNewBlobfile.sFilePath + aNewBlobfile.sFilename);
			try
			{
				Stream orgMem = new MemoryStream();
				orgBlob.DownloadToStream(orgMem);
				orgMem.Position = 0;
				Image aOrgImage = Image.FromStream(orgMem);
				Image aNewImage = BlobHelper.ResizeImage(aOrgImage, ReSize);
				Stream newMem = new MemoryStream();
				EncoderParameters encode = new EncoderParameters(1);
				encode.Param[0] = new EncoderParameter(Encoder.Quality, 80);
				var encoders = ImageCodecInfo.GetImageEncoders();
				var png = encoders.FirstOrDefault(t => t.MimeType == "image/png");

				aNewImage.Save(newMem, png, encode);
				aNewImage.Dispose();
				encode.Dispose();

				newBlob.Properties.ContentType = "image/png";
				newMem.Position = 0;
				newBlob.UploadFromStream(newMem);
				newBlob.FetchAttributes();
				newBlob.SetProperties();
				return newBlob.Uri.AbsoluteUri.Replace("https://", "http://");
			}
			catch (Exception e)
			{
				e.ToString();
				return "";
			}
		}

		/// <summary>
		/// 上傳檔案 & 同時縮小照片尺寸
		/// </summary>
		public static string UploadResize_Blob(Image image, BlobFileDesc aOrgBlobfile, Size ReSize)
		{
			int NewWidth = ReSize.Width;
			int NewHeight = ReSize.Height;
			CloudStorageAccount storageAccount = CloudStorageAccount.Parse(connection);
			CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

			CloudBlobContainer orgContainer =
					blobClient.GetContainerReference(aOrgBlobfile.sContainer);

			CloudBlockBlob newBlob = orgContainer.GetBlockBlobReference(aOrgBlobfile.sFilePath + aOrgBlobfile.sFilename);
			long ContentLength = image.PropertyItems.Length;
			try
			{
				Bitmap bmp = new Bitmap(NewWidth, NewHeight);
				bmp.SetResolution(72, 72);
				Graphics g = Graphics.FromImage(bmp);
				g.InterpolationMode = InterpolationMode.HighQualityBicubic;
				g.SmoothingMode = SmoothingMode.HighQuality;
				g.PixelOffsetMode = PixelOffsetMode.HighQuality;
				g.CompositingQuality = CompositingQuality.HighQuality;
				g.DrawImage(image, 0, 0, NewWidth, NewHeight);
				g.Dispose();

				EncoderParameters encoder = new EncoderParameters(1);
				encoder.Param[0] = new EncoderParameter(Encoder.Compression, 100);

				Stream newMem = new MemoryStream();

				bmp.Save(newMem, ImageCodecInfo.GetImageEncoders()[1], encoder);
				bmp.Dispose();
				encoder.Dispose();

				newBlob.Properties.ContentType = "image/jpeg";
				newMem.Position = 0;
				newBlob.UploadFromStream(newMem);
				newBlob.FetchAttributes();
				newBlob.SetProperties();

				decimal MB = Math.Round(((decimal)newBlob.Properties.Length / 1024) / 1024, 1);

				HttpContext.Current.Session["MB"] = MB;
				return newBlob.Uri.AbsoluteUri.Replace("https://", "http://");
			}
			catch (Exception e)
			{
				e.ToString();
				return null;
			}
		}


		public static bool Exists(CloudBlockBlob blob)
		{
			try
			{
				blob.FetchAttributes();
				return true;
			}
			catch
			{
				return false;
			}
		}
		public static bool Exists(CloudBlobContainer container)
		{
			try
			{
				container.FetchAttributes();
				return true;
			}
			catch
			{
				return false;
			}
		}


		public static Image ResizeImage(Image imgToResize, Size size)
		{
			int sourceWidth = imgToResize.Width;
			int sourceHeight = imgToResize.Height;

			float nPercent = 0;
			float nPercentW = 0;
			float nPercentH = 0;

			nPercentW = ((float)size.Width / (float)sourceWidth);
			nPercentH = ((float)size.Height / (float)sourceHeight);

			if (nPercentH < nPercentW)
				nPercent = nPercentH;
			else
				nPercent = nPercentW;

			if (nPercent > 1.0f)
				nPercent = 1.0f;

			int destWidth = (int)Math.Round(((float)sourceWidth * nPercent));
			int destHeight = (int)Math.Round(((float)sourceHeight * nPercent));

			Bitmap b = new Bitmap(destWidth, destHeight);
			Graphics g = Graphics.FromImage((Image)b);
			g.InterpolationMode = InterpolationMode.HighQualityBicubic;

			g.DrawImage(imgToResize, 0, 0, destWidth, destHeight);
			g.Dispose();

			return (Image)b;
		}

		public static bool SetBlobContainerPermissions(string sContainer, BlobContainerPublicAccessType blobContainerPublicAccessType)
		{
			CloudStorageAccount storageAccount = CloudStorageAccount.Parse(connection);
			CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

			CloudBlobContainer aContainer = blobClient.GetContainerReference(sContainer);
			if (aContainer.Exists())
				aContainer.SetPermissions(new BlobContainerPermissions() { PublicAccess = blobContainerPublicAccessType });
			return true;
		}
		public static bool SetBlobContentType(string sContainer, string sFilePath, string sFilename, string sContentType)
		{
			CloudStorageAccount storageAccount = CloudStorageAccount.Parse(connection);
			CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
			CloudBlobContainer aContainer = blobClient.GetContainerReference(sContainer);
			CloudBlockBlob blob = aContainer.GetBlockBlobReference(sFilePath + sFilename);
			blob.FetchAttributes();
			blob.Properties.ContentType = sContentType;
			blob.SetProperties();

			return true;
		}
		public static bool SetBlobContainerBlobsContentType(string sContainer, string sContentType)
		{
			CloudStorageAccount storageAccount = CloudStorageAccount.Parse(connection);
			CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
			CloudBlobContainer aContainer = blobClient.GetContainerReference(sContainer);
			foreach (CloudBlockBlob blob in aContainer.ListBlobs(null, true))
			{
				blob.FetchAttributes();
				blob.Properties.ContentType = sContentType;
				blob.SetProperties();
			}

			return true;
		}

		/// <summary>
		/// 刪除Blob
		/// </summary>
		public static void delete(string sContainer, string name)
		{

			try
			{
				// Retrieve storage account from connection-string.
				CloudStorageAccount storageAccount = CloudStorageAccount.Parse(connection);

				// Create the blob client.
				CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

				// Retrieve reference to a previously created container.
				CloudBlobContainer container = blobClient.GetContainerReference(sContainer);

				// Retrieve reference to a blob named "myimage.jpg".
				CloudBlockBlob blob = container.GetBlockBlobReference(name);

				// Delete the blob.
				blob.DeleteIfExists();

			}
			catch (Exception e)
			{
				Console.Write(e.Message);
				// Output the stack trace.

			}
		}

		/// <summary>
		/// 刪除Container
		/// </summary>
		public static void delete_Container(string containerName)
		{
			// Retrieve storage account from connection-string.
			CloudStorageAccount storageAccount = CloudStorageAccount.Parse(connection);

			// Create the blob client.
			CloudBlobClient BlobClient = storageAccount.CreateCloudBlobClient();

			CloudBlobContainer container =
			BlobClient.GetContainerReference(containerName);
			try
			{
				container.FetchAttributes();
				container.Delete();
			}
			catch (Exception e)
			{
				Console.Write(e.Message);
				// Output the stack trace.

			}
		}


		public byte[] DownloadFileFromBlob(string SN, string name)
		{

			string sContainer = "goods-sn/" + "Big/" + SN;
			// Retrieve storage account from connection-string.
			CloudStorageAccount storageAccount = CloudStorageAccount.Parse(connection);

			// Create the blob client.
			CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

			// Retrieve reference to a previously created container.
			CloudBlobContainer container = blobClient.GetContainerReference(sContainer);

			// Retrieve reference to a blob named "myimage.jpg".
			CloudBlockBlob blob = container.GetBlockBlobReference(name);
			// Read content 
			using (MemoryStream ms = new MemoryStream())
			{
				blob.DownloadToStream(ms);
				return ms.ToArray();
			}
		}

	}
}