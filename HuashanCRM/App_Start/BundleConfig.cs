﻿using System.Web;
using System.Web.Optimization;

namespace Mith
{
    public class BundleConfig
    {
        // 如需 Bundling 的詳細資訊，請造訪 http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jQuery/jquery.unobtrusive*",
                        "~/Scripts/jQuery/jquery.validate*"));
            //==================================================================================================
            bundles.Add(new StyleBundle("~/css/Index").Include("~/Content/bootstrap.css",
                                                                                                  "~/Content/bootstrap-theme.min.css",
                                                                                                  "~/Content/type.css"));
            //==================================================================================================
            bundles.Add(new ScriptBundle("~/js/Index").Include("~/Scripts/jquery.min.js",
                                                  "~/Scripts/bootstrap.js",
                                                   "~/Scripts/bootstrap.min.js"));
            //==================================================================================================
            bundles.Add(new StyleBundle("~/css/HomeIndex").Include("~/Content/Web/Index.css"));

            //==================================================================================================
            bundles.Add(new StyleBundle("~/Content/Admin/Bucket/css").Include(//"~/Scripts/plugin/bootstrap/css/bootstrap.css",
                                                               "~/Content/Admin/Bucket/bootstrap-reset.css",
                "~/Content/FontAwesome/css/font-awesome.css",
                                                               "~/Content/Admin/Bucket/style.css",
                                                               "~/Content/Admin/Bucket/style-responsive.css"));
            //==================================================================================================
            bundles.Add(new ScriptBundle("~/Scripts/plugin/TimePicker/js").Include("~/Scripts/plugin/TimePicker/jquery-ui-timepicker-addon.js",
                                                                        "~/Scripts/plugin/TimePicker/jquery.ui.datepicker-zh-TW.js",
                                                                        "~/Scripts/plugin/TimePicker/jquery-ui-timepicker-zh-TW.js"));
            bundles.Add(new StyleBundle("~/Scripts/plugin/TimePicker/css").Include("~/Scripts/plugin/TimePicker/jquery-ui-1.10.4.custom.css",
                                                                    "~/Scripts/plugin/TimePicker/jquery-ui-timepicker-addon.css"));
            //==================================================================================================

            bundles.Add(new ScriptBundle("~/Scripts/plugin/ckeditor/js").Include("~/Scripts/plugin/ckeditor/start.js",
                                                                                 "~/Scripts/plugin/ckeditor/ckeditor.js"));
            //==================================================================================================
            bundles.Add(new StyleBundle("~/css/AdminIndex").Include(
                "~/Scripts/plugin/bootstrap/css/bootstrap.css",
                "~/Scripts/plugin/lightbox/lightbox.css",
                "~/Scripts/plugin/Uploadify/uploadify.css"));

            bundles.Add(new ScriptBundle("~/js/AdminIndex").Include("~/Scripts/start.js",
                                                               "~/Scripts/jQuery/jquery.cookie.js",
                                                               "~/Scripts/jQuery/jquery-1.10.2.js",
                                                               "~/Scripts/jQuery/jquery-ui-1.10.4.js",
                                                               "~/Scripts/plugin/bootstrap/js/bootstrap.js",
                                                               "~/Scripts/plugin/jquery.easing.1.3.js",
                                                               "~/Scripts/plugin/jquery.dcjqaccordion.2.9.js",
                                                               "~/Scripts/plugin/jquery.nicescroll.js",
                                                               "~/Scripts/Admin/Bucket/dashboard.js",
                                                               "~/Scripts/Admin/Bucket/scripts.js",
                                                               "~/Scripts/plugin/date.js",
                                                               "~/Scripts/jquery.unobtrusive-ajax.js",
                                                               "~/Scripts/scrollup.js",
                                                               "~/Scripts/time.js",
                                                                "~/Scripts/plugin/lightbox/lightbox.js",
                                                                "~/Scripts/Admin/Web/Introduction.js",
                                                                "~/Scripts/jQuery/jquery.unobtrusive*",
                                                                "~/Scripts/jQuery/jquery.validate*",
                                                                "~/Scripts/plugin/kabbar.jquery.js"
                //"~/Scripts/plugin/Uploadify/jquery.uploadify.js"
                                                                ));
            //==================================================================================================
            bundles.Add(new ScriptBundle("~/js/AdminContent").Include(
                                                                "~/Scripts/jQuery/jquery-ui-1.11.4.js",
                                                                "~/Scripts/Admin/Content.js"));
            //==================================================================================================
            bundles.Add(new ScriptBundle("~/js/Import_Goods").Include(
                                                                        "~/Scripts/Import_Goods/bootbox.js",
                                                                        "~/Scripts/Import_Goods/jquery.form.js",
                                                                        "~/Scripts/Import_Goods/project.js",
                                                                        "~/Scripts/Import_Goods/project.extends.js",
                                                                        "~/Scripts/Import_Goods/project.ZipCode.js"));
            //
            //==================================================================================================
            bundles.Add(new ScriptBundle("~/js/Import_GoodsSN").Include(
                                                            "~/Scripts/Import_GoodsSN/bootbox.js",
                                                            "~/Scripts/Import_GoodsSN/jquery.form.js",
                                                            "~/Scripts/Import_GoodsSN/project.js",
                                                            "~/Scripts/Import_GoodsSN/project.extends.js",
                                                            "~/Scripts/Import_GoodsSN/project.ZipCode.js"));
            //
            //==================================================================================================
            bundles.Add(new ScriptBundle("~/js/AdminSiteOption").Include("~/Scripts/Admin/Web/SiteOption.js",
                                                                        "~/Scripts/plugin/kabbar.jquery.js",
                                                                        "~/Scripts/jQuery/jquery.unobtrusive*",
                                                                        "~/Scripts/jQuery/jquery.validate*"));
            //==================================================================================================
            bundles.Add(new ScriptBundle("~/js/AdminMemberCreate").Include("~/Scripts/Admin/Web/MemberCreate.js",
                                                                           "~/Scripts/plugin/DateSelect.js",
                                                                           "~/Scripts/jQuery/jquery.unobtrusive*",
                                                                           "~/Scripts/jQuery/jquery.validate*",
                                                                           "~/Scripts/plugin/kabbar.jquery.js"));
            //==================================================================================================


            bundles.Add(new StyleBundle("~/css/AdminTokenIndex").Include("~/Content/Admin/Content.css",
     "~/Scripts/plugin/TimePicker/jquery-ui-1.10.4.custom.css",
                                                        "~/Content/Admin/Web/TokenIndex.css",
                                                        "~/Content/Admin/superfish.css"
                                                       ));
            //==================================================================================================
            bundles.Add(new ScriptBundle("~/js/AdminApiIndex").Include("~/Scripts/Admin/Content.js",
                                                                        "~/Scripts/Admin/Action.js",
                                                                        "~/Scripts/Admin/Web/ApiKeyIndex.js"));

            bundles.Add(new StyleBundle("~/css/AdminApiIndex").Include("~/Content/Admin/Content.css",
                                                                    "~/Content/Admin/Web/ApiKeyIndex.css"));
            //==================================================================================================
            bundles.Add(new ScriptBundle("~/js/AdminApiFunIndex").Include("~/Scripts/Admin/Content.js",
                //"~/Scripts/Admin/Action.js",
                                                                        "~/Scripts/Admin/Web/ApiFunIndex.js",
                                                                        "~/Scripts/plugin/superfish.js"));

            bundles.Add(new StyleBundle("~/css/AdminApiFunIndex").Include("~/Content/Admin/Content.css",
                                                                    "~/Content/Admin/Web/ApiFunIndex.css",
                                                                    "~/Content/Admin/superfish.css"));
            //==================================================================================================
            bundles.Add(new StyleBundle("~/css/AdminFunIndex").Include("~/Content/Admin/Content.css",
                                                                    "~/Content/Admin/superfish.css",
                                                                    "~/Content/Admin/Web/FunIndex.css"));
            //==================================================================================================
            bundles.Add(new StyleBundle("~/Scripts/plugin/videojs/css").Include("~/Scripts/plugin/videojs/video-js.css"));
            bundles.Add(new ScriptBundle("~/Scripts/plugin/videojs/js").Include("~/Scripts/plugin/videojs/video.js"));
            //==================================================================================================
            bundles.Add(new StyleBundle("~/Scripts/plugin/videojs/css").Include("~/Scripts/plugin/videojs/video-js.css"));
            bundles.Add(new ScriptBundle("~/Scripts/plugin/videojs/js").Include("~/Scripts/plugin/videojs/video.js"));
            //============================
            bundles.Add(new ScriptBundle("~/js/AdminAppIndex").Include("~/Scripts/Admin/Content.js",
                                                                        "~/Scripts/Admin/Action.js",
                                                                        "~/Scripts/Admin/Web/NewsIndex.js",
                                                                        "~/Scripts/plugin/superfish.js"));

            bundles.Add(new StyleBundle("~/css/AdminAppIndex").Include("~/Content/Admin/Content.css",
                                                                    "~/Content/Admin/Web/AppIndex.css",
                                                                    "~/Content/Admin/superfish.css"));
            //==================================================================================================
            bundles.Add(new ScriptBundle("~/js/AdminUserLevelCreate").Include(
                                                               "~/Scripts/Admin/Web/UserLevelCreate.js"));
            bundles.Add(new ScriptBundle("~/js/AdminUserLevelEdit").Include(
                                                   "~/Scripts/Admin/Web/UserLevelEdit.js"));
            //==================================================================================================
            bundles.Add(new ScriptBundle("~/js/AdminCompanyCreate").Include(
                                                               "~/Scripts/Admin/Web/CompanyCreate.js"));
            bundles.Add(new ScriptBundle("~/js/AdminCompanyEdit").Include(
                                                   "~/Scripts/Admin/Web/CompanyEdit.js"));

            bundles.Add(new ScriptBundle("~/js/AdminCompanyIndex").Include("~/Scripts/Admin/Web/CompanyIndex.js"));

            bundles.Add(new StyleBundle("~/css/AdminCompanyIndex").Include("~/Content/Admin/Content.css",
                                                                    "~/Content/Admin/Web/CompanyIndex.css",
                                                                    "~/Content/Admin/superfish.css"));

            //==================================================================================================
            bundles.Add(new ScriptBundle("~/js/AdminVolunteersIndex").Include("~/Scripts/Admin/Web/VolunteersIndex.js"));
            bundles.Add(new StyleBundle("~/css/AdminVolunteersIndex").Include("~/Content/Admin/Content.css",
                                                                    "~/Content/Admin/Web/VolunteersIndex.css",
                                                                    "~/Content/Admin/superfish.css"));

						bundles.Add(new ScriptBundle("~/js/AdminVolunteerEdit").Include("~/Scripts/Admin/Web/VolunteerEdit.js",
                                                            "~/Scripts/plugin/DateSelect.js",
                                                            "~/Scripts/jQuery/jquery.unobtrusive*",
                                                            "~/Scripts/jQuery/jquery.validate*"));
            //==================================================================================================


            bundles.Add(new StyleBundle("~/css/AdminLogin").Include("~/Content/Admin/Content.css",
                                                                    "~/Content/Admin/Web/Login.css"));

            //==================================================================================================

            bundles.Add(new StyleBundle("~/css/AdminUserLevelIndex_Mith").Include("~/Content/Admin/Content.css",
                                                                    "~/Content/Admin/Web/UserLevelIndex_Mith.css"));
            bundles.Add(new StyleBundle("~/css/AdminUserLevelIndex_Company").Include("~/Content/Admin/Content.css",
                                                        "~/Content/Admin/Web/UserLevelIndex_Company.css"));
            //==================================================================================================

            bundles.Add(new ScriptBundle("~/js/AdminUserProfileIndex").Include("~/Scripts/Admin/Web/UserProfileIndex.js"));
            bundles.Add(new StyleBundle("~/css/AdminUserProfileIndex_Mith").Include("~/Content/Admin/Content.css",
                                                        "~/Content/Admin/Web/UserProfileIndex_Mith.css"));
            bundles.Add(new StyleBundle("~/css/AdminUserProfileIndex_Company").Include("~/Content/Admin/Content.css",
                                            "~/Content/Admin/Web/UserProfileIndex_Company.css"));

            bundles.Add(new ScriptBundle("~/js/AdminUserProfileCreate").Include(
                                                    "~/Scripts/Admin/Web/UserProfileCreate.js"));

            bundles.Add(new ScriptBundle("~/js/AdminUserProfileEdit").Include(
                                                               "~/Scripts/Admin/Web/UserProfileEdit.js"));
            //==================================================================================================

            bundles.Add(new ScriptBundle("~/js/AdminLoginIndex").Include("~/Scripts/Admin/Content.js",
                                                                        "~/Scripts/Admin/Web/NewsIndex.js"));

            bundles.Add(new StyleBundle("~/css/AdminLoginIndex").Include("~/Content/Admin/Content.css",
                                                                    "~/Content/Admin/Web/LoginIndex.css"));
            //==================================================================================================
            bundles.Add(new ScriptBundle("~/js/AdminMailIndex").Include("~/Scripts/Admin/Content.js",
                                                                        "~/Scripts/Admin/Web/MailIndex.js"));

            bundles.Add(new StyleBundle("~/css/AdminMailIndex").Include("~/Content/Admin/Content.css",
                                                                    "~/Content/Admin/Web/MailIndex.css"));
            //==================================================================================================
            bundles.Add(new ScriptBundle("~/js/AdminErrorIndex").Include("~/Scripts/Admin/Content.js",
                                                                        "~/Scripts/Admin/Web/ErrorIndex.js"));

            bundles.Add(new StyleBundle("~/css/AdminErrorIndex").Include("~/Content/Admin/Content.css",
                                                                    "~/Content/Admin/Web/ErrorIndex.css"));
            //==================================================================================================
            bundles.Add(new ScriptBundle("~/js/AdminAppCreate").Include("~/Scripts/Admin/Web/AppCreate.js",
                                                                        "~/Scripts/jQuery/jquery.unobtrusive*",
                                                                        "~/Scripts/jQuery/jquery.validate*"));
            bundles.Add(new StyleBundle("~/css/AdminAppCreate").Include("~/Content/Admin/Content.css"));
            //==================================================================================================
            bundles.Add(new StyleBundle("~/css/AdminAppMessageIndex").Include("~/Content/Admin/Content.css",
                                                                    "~/Content/Admin/Web/AppMessageIndex.css"));
            //==================================================================================================
            bundles.Add(new StyleBundle("~/css/AdminAppErrorIndex").Include("~/Content/Admin/Content.css",
                                                                    "~/Content/Admin/Web/AppErrorIndex.css"));
            bundles.Add(new ScriptBundle("~/js/AdminAppErrorIndex").Include("~/Scripts/Admin/Content.js",
                                                                        "~/Scripts/Admin/Action.js",
                                                                        "~/Scripts/Admin/Web/AppErrorIndex.js",
                                                                        "~/Scripts/jQuery/jquery-ui-1.10.4.js"));
            //==================================================================================================
            bundles.Add(new StyleBundle("~/css/HomeError").Include("~/Content/Web/Error.css"));
            //==================================================================================================
            bundles.Add(new StyleBundle("~/css/LoginIndex").Include("~/Content/Web/LoginIndex.css"));
            bundles.Add(new StyleBundle("~/css/LoginPasswordForgot").Include("~/Content/Web/LoginPasswordForgot.css"));
            bundles.Add(new StyleBundle("~/css/PasswordForgot").Include("~/Content/Web/PasswordForgot.css"));
            bundles.Add(new StyleBundle("~/css/EmailConfirm").Include("~/Content/Web/EmailConfirm.css"));
            //==================================================================================================
            bundles.Add(new StyleBundle("~/css/LoginAgree").Include("~/Content/Web/Agree.css"));
            bundles.Add(new StyleBundle("~/css/Register2").Include("~/Content/Web/Register2.css"));
            //==================================================================================================
            bundles.Add(new StyleBundle("~/css/Page").Include("~/Content/Admin/Page.css"));
            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                        "~/Content/themes/base/jquery.ui.core.css",
                        "~/Content/themes/base/jquery.ui.resizable.css",
                        "~/Content/themes/base/jquery.ui.selectable.css",
                        "~/Content/themes/base/jquery.ui.accordion.css",
                        "~/Content/themes/base/jquery.ui.autocomplete.css",
                        "~/Content/themes/base/jquery.ui.button.css",
                        "~/Content/themes/base/jquery.ui.dialog.css",
                        "~/Content/themes/base/jquery.ui.slider.css",
                        "~/Content/themes/base/jquery.ui.tabs.css",
                        "~/Content/themes/base/jquery.ui.datepicker.css",
                        "~/Content/themes/base/jquery.ui.progressbar.css",
                        "~/Content/themes/base/jquery.ui.theme.css"));
            //==================================================================================================
            bundles.Add(new StyleBundle("~/css/Loading").Include(
                "~/Content/Loading/style.css",
                "~/Content/Loading/font-awesome.css"
                ));
            //==================================================================================================
            bundles.Add(new ScriptBundle("~/js/Autocomplete").Include("~/Scripts/Autocomplete.js"));
            //=================================================================================================

            bundles.Add(new StyleBundle("~/css/AdminGoodsSN_SettingIndex").Include("~/Content/Admin/Content.css",
                                                                    "~/Content/Admin/Web/GoodsSN_SettingIndex.css",
                                                                    "~/Content/Admin/superfish.css"));
            bundles.Add(new ScriptBundle("~/js/AdminGoodsSN_SettingIndex").Include(
                                             "~/Scripts/jQuery/jquery-barcode.js",
                                           "~/Scripts/Admin/Web/GoodsSN_SettingIndex.js"));

            bundles.Add(new ScriptBundle("~/js/AdminGoodsSN_SettingCreate").Include(
                               "~/Scripts/Admin/Web/GoodsSN_SettingCreate.js"));


            bundles.Add(new ScriptBundle("~/js/AdminGoodsSN_SettingEdit").Include(
                                            "~/Scripts/Admin/Web/GoodsSN_SettingEdit.js"));

            //==================================================================================================
            bundles.Add(new StyleBundle("~/css/AdminBrandIndex").Include("~/Content/Admin/Content.css",
                                                                    "~/Content/Admin/Web/BrandIndex.css",
                                                                    "~/Content/Admin/superfish.css"));

            bundles.Add(new ScriptBundle("~/js/AdminBrandCreate").Include(
                                                       "~/Scripts/Admin/Web/BrandCreate.js"));
            //==================================================================================================
            bundles.Add(new StyleBundle("~/css/AdminGoodsIndex_Company").Include("~/Content/Admin/Content.css",
                                "~/Content/Admin/Web/GoodsIndex_Company.css"));
            bundles.Add(new StyleBundle("~/css/AdminGoodsIndex_Mith").Include("~/Content/Admin/Content.css",
                                            "~/Content/Admin/Web/GoodsIndex_Mith.css"));

            bundles.Add(new ScriptBundle("~/js/AdminGoodsIndex").Include(
                                                        "~/Scripts/Admin/Web/GoodsIndex.js"));

            bundles.Add(new ScriptBundle("~/js/AdminGoodsCreate").Include(
                                                                "~/Scripts/Admin/Web/GoodsCreate.js"));
            bundles.Add(new ScriptBundle("~/js/AdminGoodsEdit_Mith").Include(
                                                    "~/Scripts/Admin/Web/GoodsEdit_Mith.js"));
            bundles.Add(new ScriptBundle("~/js/AdminGoodsEdit_Company").Include(
                                        "~/Scripts/Admin/Web/GoodsEdit_Company.js"));
            bundles.Add(new StyleBundle("~/css/AdminGoodsEdit").Include(
                                           "~/Content/Admin/Web/GoodsEdit.css"));
            //==================================================================================================
            bundles.Add(new StyleBundle("~/css/AdminGoodsQAIndex_Mith").Include("~/Content/Admin/Content.css",
                                                                    "~/Content/Admin/Web/GoodsQAIndex_Mith.css",
                                                                    "~/Content/Admin/superfish.css"));

            bundles.Add(new StyleBundle("~/css/AdminGoodsQAIndex_Company").Include("~/Content/Admin/Content.css",
                                                        "~/Content/Admin/Web/GoodsQAIndex_Company.css",
                                                        "~/Content/Admin/superfish.css"));

            bundles.Add(new ScriptBundle("~/js/AdminGoodsQAEdit").Include(
                            "~/Scripts/Admin/Web/GoodsQAEdit.js"));
            //==================================================================================================
            bundles.Add(new StyleBundle("~/css/AdminExpertIndex").Include("~/Content/Admin/Content.css",
                                            "~/Content/Admin/Web/ExpertIndex.css",
                                            "~/Content/Admin/superfish.css"));

            bundles.Add(new ScriptBundle("~/js/AdminExpertCreate").Include(
                               "~/Scripts/Admin/Web/ExpertCreate.js"));
            //==================================================================================================
            bundles.Add(new StyleBundle("~/css/AdminOtherIndex").Include("~/Content/Admin/Content.css",
                                            "~/Content/Admin/Web/OtherIndex.css",
                                            "~/Content/Admin/superfish.css"));

            bundles.Add(new ScriptBundle("~/js/AdminOtherEdit").Include(
                   "~/Scripts/Admin/Web/OtherEdit.js"));
            //==================================================================================================
            bundles.Add(new StyleBundle("~/css/AdminActivityIndex").Include("~/Content/Admin/Content.css",
                                            "~/Content/Admin/Web/ActivityIndex.css",
                                            "~/Content/Admin/superfish.css"));

						//活動管理 / 最新商品
            bundles.Add(new ScriptBundle("~/js/AdminActivity_New_Index").Include(
																												"~/Scripts/Admin/Web/Activity_New_Index.js"));

						//活動管理 / 前100件展示款
						bundles.Add(new ScriptBundle("~/js/AdminActivity_Promotion100_Index").Include(
																							"~/Scripts/Admin/Web/Activity_Promotion100_Index.js"));

						//活動管理 / 熱銷款 Top30
						bundles.Add(new ScriptBundle("~/js/AdminActivity_Hot30_Index").Include(
																							"~/Scripts/Admin/Web/Activity_Hot30_Index.js"));

						//活動管理 / 活動圖片
						bundles.Add(new ScriptBundle("~/js/AdminActivity_ActivityPic_Index").Include(
																							"~/Scripts/Admin/Web/Activity_ActivityPic_Index.js"));

						//活動管理 = 活動圖片
						bundles.Add(new ScriptBundle("~/js/AdminActivityPicEdit").Include(
																							"~/Scripts/Admin/Web/ActivityPicEdit.js"));
            //==================================================================================================
            bundles.Add(new StyleBundle("~/css/AdminWearPicIndex").Include("~/Content/Admin/Content.css",
                                            "~/Content/Admin/Web/WearPicIndex.css",
                                            "~/Content/Admin/superfish.css"));
            bundles.Add(new ScriptBundle("~/js/AdminWearPicIndex").Include(
                   "~/Scripts/Admin/Web/WearPicIndex.js"));

            bundles.Add(new ScriptBundle("~/js/AdminWearPicCreate").Include(
                               "~/Scripts/Admin/Web/WearPicCreate.js"));

            bundles.Add(new ScriptBundle("~/js/AdminWearPicEdit").Include(
                                            "~/Scripts/Admin/Web/WearPicEdit.js"));
            //==================================================================================================
            bundles.Add(new StyleBundle("~/css/AdminOrderIndex_Mith").Include("~/Content/Admin/Content.css",
                                                          "~/Content/Admin/Web/OrderIndex_Mith.css"));

            bundles.Add(new StyleBundle("~/css/AdminOrderIndex_Company").Include("~/Content/Admin/Content.css",
                                              "~/Content/Admin/Web/OrderIndex_Company.css"));

            bundles.Add(new ScriptBundle("~/js/AdminOrderIndex").Include(
                                            "~/Scripts/Admin/Web/OrderIndex.js"));

            //==================================================================================================
            bundles.Add(new StyleBundle("~/css/AdminExpertArticleIndex").Include("~/Content/Admin/Content.css",
                                            "~/Content/Admin/Web/ExpertArticleIndex.css",
                                            "~/Content/Admin/superfish.css"));

            bundles.Add(new ScriptBundle("~/js/AdminExpertArticleCreate").Include(
                   "~/Scripts/Admin/Web/ExpertArticleCreate.js"));

            //==================================================================================================
            bundles.Add(new StyleBundle("~/css/AdminArticleMessageIndex").Include(
                                            "~/Content/Admin/Content.css",
                                            "~/Content/Admin/Web/ArticleMessageIndex.css",
                                            "~/Content/Admin/superfish.css"));
            //==================================================================================================
            bundles.Add(new StyleBundle("~/css/AdminFashionArticleIndex").Include("~/Content/Admin/Content.css",
                                            "~/Content/Admin/Web/FashionArticleIndex.css",
                                            "~/Content/Admin/superfish.css"));

						bundles.Add(new ScriptBundle("~/js/AdminFashionArticleIndex").Include(
																							"~/Scripts/Admin/Web/FashionArticleIndex.js"));

            bundles.Add(new ScriptBundle("~/js/AdminFashionArticleCreate").Include(
                   "~/Scripts/Admin/Web/FashionArticleCreate.js"));
            //==================================================================================================
            bundles.Add(new StyleBundle("~/css/AdminStyleAdviceIndex").Include(
                                            "~/Content/Admin/Content.css",
                                            "~/Content/Admin/Web/StyleAdviceIndex.css",
                                            "~/Content/Admin/superfish.css"));
            //==================================================================================================
            bundles.Add(new StyleBundle("~/css/AdminPrivateMessageIndex_Group").Include(
                                            "~/Content/Admin/Content.css",
                                            "~/Content/Admin/Web/PrivateMessageIndex_Group.css",
                                            "~/Content/Admin/superfish.css"));

            bundles.Add(new StyleBundle("~/css/AdminPrivateMessageIndex_Content").Include(
                                "~/Content/Admin/Content.css",
                                "~/Content/Admin/Web/PrivateMessageIndex_Content.css",
                                "~/Content/Admin/superfish.css"));
            //==================================================================================================
            bundles.Add(new StyleBundle("~/css/AdminLikeExpertIndex").Include(
                                            "~/Content/Admin/Content.css",
                                            "~/Content/Admin/Web/LikeExpertIndex.css",
                                            "~/Content/Admin/superfish.css"));
            //==================================================================================================
            bundles.Add(new StyleBundle("~/css/AdminOrderEdit").Include("~/Content/Admin/Content.css",
                                                                    "~/Content/Admin/Web/OrderEdit.css",
                                                                    "~/Content/Admin/superfish.css"));

            bundles.Add(new ScriptBundle("~/js/AdminOrderEdit").Include("~/Scripts/Admin/Web/OrderEdit.js",
                                                            "~/Scripts/jQuery/jquery.unobtrusive*",
                                                            "~/Scripts/jQuery/jquery.validate*"));

            //==================================================================================================

            bundles.Add(new StyleBundle("~/css/AdminOrderQAIndex_Mith").Include("~/Content/Admin/Content.css",
                                                        "~/Content/Admin/Web/OrderQAIndex_Mith.css",
                                                        "~/Content/Admin/superfish.css"));

            bundles.Add(new StyleBundle("~/css/AdminOrderQAIndex_Company").Include("~/Content/Admin/Content.css",
                                                        "~/Content/Admin/Web/OrderQAIndex_Company.css",
                                                        "~/Content/Admin/superfish.css"));

            bundles.Add(new ScriptBundle("~/js/AdminOrderQAEdit").Include(
                            "~/Scripts/Admin/Web/OrderQAEdit.js"));
            //==================================================================================================
            bundles.Add(new StyleBundle("~/css/AdminLimitOrderIndex_Mith").Include("~/Content/Admin/Content.css",
     "~/Content/Admin/Web/LimitOrderIndex_Mith.css",
     "~/Content/Admin/superfish.css"));

            bundles.Add(new StyleBundle("~/css/AdminLimitOrderIndex_Company").Include("~/Content/Admin/Content.css",
                "~/Content/Admin/Web/LimitOrderIndex_Company.css",
                "~/Content/Admin/superfish.css"));

            bundles.Add(new ScriptBundle("~/js/LimitOrderIndex").Include("~/Scripts/Admin/Web/LimitOrderIndex.js",
                                                "~/Scripts/jQuery/jquery.unobtrusive*",
                                                "~/Scripts/jQuery/jquery.validate*"));
            //==================================================================================================

            bundles.Add(new StyleBundle("~/css/AdminShippingGoodsIndex_Mith").Include("~/Content/Admin/Content.css",
                                                                    "~/Content/Admin/Web/ShippingGoodsIndex_Mith.css",
                                                                    "~/Content/Admin/superfish.css"));

            bundles.Add(new StyleBundle("~/css/AdminShippingGoodsIndex_Company").Include("~/Content/Admin/Content.css",
                                                        "~/Content/Admin/Web/ShippingGoodsIndex_Company.css",
                                                        "~/Content/Admin/superfish.css"));

            bundles.Add(new ScriptBundle("~/js/AdminShippingGoodsEdit").Include(
                                    "~/Scripts/Admin/Web/ShippingGoodsEdit.js"));

            //==================================================================================================

            bundles.Add(new StyleBundle("~/css/AdminReturnGoodsIndex_Mith").Include("~/Content/Admin/Content.css",
                                                                    "~/Content/Admin/Web/ReturnGoodsIndex_Mith.css",
                                                                    "~/Content/Admin/superfish.css"));

            bundles.Add(new StyleBundle("~/css/AdminReturnGoodsIndex_Company").Include("~/Content/Admin/Content.css",
                                                        "~/Content/Admin/Web/ReturnGoodsIndex_Company.css",
                                                        "~/Content/Admin/superfish.css"));

            bundles.Add(new ScriptBundle("~/js/AdminReturnGoodsEdit").Include(
                                    "~/Scripts/Admin/Web/ReturnGoodsEdit.js"));

            //==================================================================================================

            bundles.Add(new StyleBundle("~/css/AdminRefundIndex").Include("~/Content/Admin/Content.css",
               "~/Content/Admin/Web/RefundIndex.css",
                "~/Content/Admin/superfish.css"));

            //==================================================================================================
            bundles.Add(new ScriptBundle("~/js/AdminRefundEdit").Include(
                        "~/Scripts/Admin/Web/RefundEdit.js"));
						//==================================================================================================

						bundles.Add(new StyleBundle("~/css/AdminAccountingIndex").Include("~/Content/Admin/Content.css",
							 "~/Content/Admin/Web/AccountingIndex.css",
								"~/Content/Admin/superfish.css"));

						bundles.Add(new ScriptBundle("~/js/AdminAccountingIndex").Include(
							"~/Scripts/Admin/Web/AccountingIndex.js"));

        }
    }
}