﻿$(function () {
    String.prototype.trim = function () { return this.replace(/(^\s*)|(\s*$)/g, ""); }
});

function ajax_post(path, params, target, loading, nodata) {
    $(target).html(loading);
    
    $.ajax({
        type: "post",
        traditional: true,
        async: true,
        url: path,
        data: params,
        //dataType: "json",
        //jsonp: "callback",
        //jsonpCallback: "dool",
        success: function (data) {
            if (data) {
                $(target).html(data);
            }
            else {
                $(target).html(nodata);
            }
        }
    });
}

function ajax_send(path, params) {
    var temp = "";

    $.ajax({
        type: "post",
        traditional: true,
        async: false,
        url: path,
        data: params,
        //dataType: "json",
        //jsonp: "callback",
        //jsonpCallback: "dool",
        success: function (data) {
            temp = data;
        }
    });
    return temp;

}

function post_to_url(path, params, method, enctype) {
    method = method || "post"; // Set method to post by default, if not specified.

    // The rest of this code assumes you are not using a library.
    // It can be made less wordy if you use one.
    var form = document.createElement("form");
    if (enctype) {
        form.setAttribute("enctype", "multipart/form-data");
    }
    form.setAttribute("method", method);
    form.setAttribute("action", path);

    for (var key in params) {
        if (params.hasOwnProperty(key)) {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", params[key]);

            form.appendChild(hiddenField);
        }
    }

    document.body.appendChild(form);
    form.submit();
}

function covert_time(target) {
    $(target).each(function () {
        var d = new Date();
        var n = d.getTimezoneOffset() * (-1);
        var time = new Date($(this).html());
        time.setMinutes(time.getMinutes() + n);
        $(this).html(time.toLocaleString());
    });
}