﻿$(function () {
    $("select[name=year],select[name=month]").change(function () {
        var y = parseInt($("select[name=year] option:selected").val(), 10);
        var m = parseInt($("select[name=month] option:selected").val(), 10);

        if ($("select[name=date]").attr("data-val") != undefined) {
            date = Get_Number(1, Get_Days(y, m), parseInt($("select[name=date]").attr("data-val"), 10));
            $("select[name=date]").html(date);
            $("select[name=date]").removeAttr("data-val");
        }
        else {
            date = Get_Number(1, Get_Days(y, m), -1);
            $("select[name=date]").html(date);
        }
    });

    var d = new Date();
    var year = Get_Number(1950, d.getFullYear(), 
        $("select[name=year]").attr("data-val") != undefined ?
            parseInt($("select[name=year]").attr("data-val"), 10) :-1);
    $("select[name=year]").html(year);
    if ($("select[name=year]").attr("data-val") != undefined)
    {
        $("select[name=year]").removeAttr("data-val");
    }

    var month = Get_Number(1, 12,
        $("select[name=month]").attr("data-val") != undefined ?
            parseInt($("select[name=month]").attr("data-val"), 10) : -1);
    $("select[name=month]").html(month);
    if ($("select[name=month]").attr("data-val") != undefined) {
        $("select[name=month]").removeAttr("data-val");
    }
    $("select[name=year]").trigger('change');
});

function Get_Number(start,end,select)
{
    var selected = "selected=\"selected\"";
    var html = "<option value=\"\" " + (select==0 ? selected :"") + ">請選擇</option>";
    for (var i = start; i <= end; i++) {
        html += "<option value=\"" + i + "\" " + (select == i ? selected : "") + ">" + i + "</option>";
    }
    return html;
}
function Get_Days(y, m)
{
    if (y % 4 == 0 && m == 2)
    {
        return 29;
    }
    if (m == 2) {
        return 28;
    }
    if (m == 1 || m == 3 || m == 5 || m == 7 || m == 8 || m == 10 || m == 12)
    {
        return 31;
    }
    if (m == 4 || m == 6 || m == 9 || m == 11) {
        return 30;
    }
    return 0;
}