﻿function codeAddress(address) {
    //var address = $("#search_txt").val();
    geocoder.geocode({ 'address': address  }, function (results, status) {

        if (status == google.maps.GeocoderStatus.OK) {
            set_marker(results);
        }
        else {
            alert('錯誤訊息: ' + status);
        }
    });
}

function set_marker(results)
{
    map.setCenter(new google.maps.LatLng(results[0].geometry.location.lat(), results[0].geometry.location.lng()));
    map.setZoom(16);
    //$("#lat").val(results[0].geometry.location.lat());
    //$("#lng").val(results[0].geometry.location.lng());
    //var html = "<ul>";
    clear_marker();
    marker = [];
    for (var i in results) {
        var myLatlng = new google.maps.LatLng(results[i].geometry.location.lat(), results[i].geometry.location.lng());
        var m = new google.maps.Marker({ position: myLatlng, title: results[i].formatted_address });
        marker.push(m);
        marker[i].setMap(map);
        $("#Latitude").val(results[i].geometry.location.lat());
        $("#Longitude").val(results[i].geometry.location.lng());
        $("#Address").val(results[i].formatted_address);
        //marker[i].addListener("click", function (MouseEvent) {
        //    $("#Latitude").val(this.getPosition().lat());
        //    $("#Longitude").val(this.getPosition().lng());
        //    $("#Address").val(this.getTitle());
        //});
        //html += "<li><a href=\"javascript:void(0)\" lat=\"" + myLatlng.lat() + "\" lng=\"" + myLatlng.lng() + "\">" + results[i].formatted_address + "</a></li>";
    }
    //html += "</ul>";

    //$("#show").html(html);
}
function initialize() {
    map = new google.maps.Map(document.getElementById("map-canvas"),
        get_option(23.973875, 120.982024, 8));
    var contextMenu = new ContextMenu(map, get_contextMenuOptions());

    google.maps.event.addListener(map, 'rightclick', function (mouseEvent) {
        contextMenu.show(mouseEvent.latLng);
    });

    google.maps.event.addListener(contextMenu, 'menu_item_selected', function (latLng, eventName) {
        switch (eventName) {
            case 'new_target_click':
                $("#Latitude").val("");
                $("#Longitude").val("");
                clear_marker();
                marker = [];
                geocoder.geocode({ 'latLng': latLng }, function (results, status) {

                    if (status == google.maps.GeocoderStatus.OK) {
                        var m = new google.maps.Marker({ position: latLng, title: results[0].formatted_address });
                        marker.push(m);
                        marker[marker.length - 1].setMap(map);
                        marker[marker.length - 1].addListener("click", function (MouseEvent) {
                            $("#Latitude").val(this.getPosition().lat());
                            $("#Longitude").val(this.getPosition().lng());
                            $("#Address").val(this.getTitle());
                        });
                    } else {
                        alert('錯誤訊息: ' + status);
                    }
                });
                
                break;
            case 'clear_target_click':
                clear_marker();
                marker = [];
                break;
            case 'zoom_in_click':
                map.setZoom(map.getZoom() + 1);
                break;
            case 'zoom_out_click':
                map.setZoom(map.getZoom() - 1);
                break;
            case 'center_map_click':
                map.panTo(latLng);
                break;
        }
    });
}

function get_option(lat, lng, zoom) {
    return {
        center: new google.maps.LatLng(lat, lng),
        zoom: zoom,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
}
function clear_marker() {
    for (var i in marker) {
        marker[i].setMap(null);
    }
}

function get_contextMenuOptions() {
    var contextMenuOptions = {};
    contextMenuOptions.classNames = { menu: 'context_menu', menuSeparator: 'context_menu_separator' };

    //	create an array of ContextMenuItem objects
    //	an 'id' is defined for each of the four directions related items
    var menuItems = [];
    menuItems.push({ className: 'context_menu_item', eventName: 'new_target_click', id: 'new_target', label: '新增座標' });
    contextMenuOptions.menuItems = menuItems;
    return contextMenuOptions;
}