/*
Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function (config) {
    config.enterMode = CKEDITOR.ENTER_BR;
    config.shiftEnterMode = CKEDITOR.ENTER_P;
    config.htmlEncodeOutput = true;
    config.toolbar_Full = [['Source', '-', 'Preview', '-', 'Templates'],
                        ['Undo', 'Redo', '-', 'SelectAll', 'RemoveFormat'],
                        ['Styles', 'Format', 'Font', 'FontSize'],
                        ['TextColor', 'BGColor'],
                        ['Maximize', 'ShowBlocks', '-', 'About'], '/',
                        ['Bold', 'Italic', 'Underline', 'Strike', '-', 'Subscript', 'Superscript'],
                        ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', 'Blockquote', 'CreateDiv'],
                        ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
                        ['Link', 'Unlink', 'Anchor'],
                        ['Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak'],
                        ['Code']];
    config.extraPlugins = 'video';
    config.allowedContent = true;
    config.filebrowserImageUploadUrl = '/GoX/Upload/Picture?fun_id=' + $("input[name=Fun_Id]").val();

};
