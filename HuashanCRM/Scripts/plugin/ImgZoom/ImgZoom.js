﻿/*
 * Image preview script 
 * powered by jQuery (http://www.jquery.com)
 * 
 * written by Alen Grakalic (http://cssglobe.com)
 * 
 * for more info visit http://cssglobe.com/post/1695/easiest-tooltip-and-image-preview-using-jquery
 *
 */

this.imagePreview = function () {
    /* CONFIG */

    xOffset = 10;
    yOffset = 20;

    // these 2 variable determine popup's distance from the cursor
    // you might want to adjust to get the right result

    /* END CONFIG */
    $('img').hover(function (e) {
        this.t = this.title;
        this.title = '';
        var p, c = (this.t != '') ? '<br/>' + this.t : '';
        $('body').append('<p id="preview"><img src="' + this.src + '" alt="Image preview" />' + c + '</p>');

        // load image and get size
        p = $('#preview');
        p.fadeIn('fast')
            .find('img').load(function () {
                // get image dimensions after it has been loaded
                p.data('widths', [$(window).width(), p.find('img').width()]);
                // set image to 100% to fit in preview window
                $(this).width('100%');
                position(e);
            });
    },
    function () {
        this.title = this.t;
        $('#preview').remove();
    });

    $('img').mousemove(function (e) {
        position(e);
    });

    var position = function (e) {
        var w, prevw = $('#preview'),
            w = $.data(prevw[0], 'widths');
        if (w) {
            prevw
                .css('top', e.pageY + yOffset)
                .css('left', e.pageX + xOffset)
                .css('max-width', (e.pageX + prevw.outerWidth() > w[0]) ? w[0] - e.pageX - xOffset : w[1] || 'auto');
        }
    };

};

// starting the script on page load
$(document).ready(function () {
    imagePreview();
});