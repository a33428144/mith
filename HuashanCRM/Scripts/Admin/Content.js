﻿

$.widget("custom.combobox", {
    _create: function () {
        this.wrapper = $("<span>")
            .addClass("custom-combobox")
            .insertAfter(this.element);

        this.element.hide();
        this._createAutocomplete();
        this._createShowAllButton();
    },

    _createAutocomplete: function () {
        var selected = this.element.children(":selected"),
            value = selected.val() ? selected.text() : "";

        this.input = $("<input>")
            .appendTo(this.wrapper)
            .val(value)
            .attr("title", "")
            .addClass("custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left")
            .autocomplete({
                delay: 0,
                minLength: 0,
                source: $.proxy(this, "_source")
            })
            .tooltip({
                tooltipClass: "ui-state-highlight"
            });

        this._on(this.input, {
            autocompleteselect: function (event, ui) {
                ui.item.option.selected = true;
                this._trigger("select", event, {
                    item: ui.item.option
                });
            },

            autocompletechange: "_removeIfInvalid"
        });
    },

    _createShowAllButton: function () {
        var input = this.input,
            wasOpen = false;

        $("<a>")
            .attr("tabIndex", -1)
            .tooltip()
            .appendTo(this.wrapper)
            .button({
                icons: {
                    primary: "ui-icon-triangle-1-s"
                },
                text: false
            })
            .removeClass("ui-corner-all")
            .addClass("custom-combobox-toggle ui-corner-right")
            .mousedown(function () {
                wasOpen = input.autocomplete("widget").is(":visible");
            })
            .click(function () {
                input.focus();

                // Close if already visible
                if (wasOpen) {
                    return;
                }

                // Pass empty string as value to search for, displaying all results
                input.autocomplete("search", "");
            });
    },

    _source: function (request, response) {
        var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
        response(this.element.children("option").map(function () {
            var text = $(this).text();
            if (this.value && (!request.term || matcher.test(text)))
                return {
                    label: text,
                    value: text,
                    option: this
                };
        }));
    },

    _removeIfInvalid: function (event, ui) {

        // Selected an item, nothing to do
        if (ui.item) {
            return;
        }

        // Search for a match (case-insensitive)
        var value = this.input.val(),
            valueLowerCase = value.toLowerCase(),
            valid = false;
        this.element.children("option").each(function () {
            if ($(this).text().toLowerCase() === valueLowerCase) {
                this.selected = valid = true;
                return false;
            }
        });

        // Found a match, nothing to do
        if (valid) {
            return;
        }

        // Remove invalid value
        this.input
            .val("")
            .attr("title", "沒有此選項")
            .tooltip("open");
        this.element.val("");
        this._delay(function () {
            this.input.tooltip("close").attr("title", "");
        }, 500);
        this.input.autocomplete("instance").term = "";
    },

    _destroy: function () {
        this.wrapper.remove();
        this.element.show();
    }
});


$(function () {
    $(".combobox").combobox({

        select: function (event, ui) {
        }
    });

    $(".date_search").datetimepicker({
        dateFormat: 'yy/mm/dd',
        onClose: function (dateText, inst) {
            if (dateText != "" && dateText != null)
            { $(this).val(dateText); }
        },
        onSelect: function (e, t) {
            if (!t.clicks) t.clicks = 0;

            if (++t.clicks === 2) {
                t.inline = false;
                t.clicks = 0;
            }
            setTimeout(function () {
                t.clicks = 0
            }, 500);
        }
    });

    $("#all").click(function () {
        $(".delete").each(function () {
            if ($("#all").prop("checked"))
                $(this).prop("checked", $("#all").prop("checked"));
            else
                $(this).prop("checked", false);
        });
    });

    //document.onkeydown = fkey;
    //document.onkeypress = fkey
    //document.onkeyup = fkey;

    //var wasPressed = false;

    //function fkey(e) {
    //    e = e || window.event;
    //    if (wasPressed) return;

    //    if (e.keyCode == 116) {
    //        alert("f5 pressed");
    //        return false;
    //    }
    //}
});

$(document).keydown(function (event) {
    //按F4
    if (event.keyCode == 115) {
        var date = new Date();
        //Cookie預設5秒
        date.setTime(date.getTime() + (5 * 1000));
        $.cookie('chk_GoodsSN', null, { expires: date, path: '/' });
        $.cookie('chk_Goods', null, { expires: date, path: '/' });
        $.cookie('chk_Order', null, { expires: date, path: '/' });
        $.cookie('chk_ShippingGoods', null, { expires: date, path: '/' });
        //清掉搜尋變數後的網址
        var url = location.protocol + "//" + location.host + location.pathname;
        location.href = url;
        return false;
    }

});