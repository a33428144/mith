﻿
var DialogTitle;
$(function () {
    $("#dialog").dialog({
        autoOpen: false,
        height: 450,
        width: 650,
        close: function () {
            $('html, body').css('overflow', 'auto');
            $("#overlay").css("display", "none");
        }
    });

    //去7-11選擇退貨門市，從7-11導回來
    if ($("#From").val() == "711") {
        $("#overlay").css("display", "block");
        var ShippingGoods_submit = ".ShippingGoods_submit" + $("#GoodsId2").val();
        $(ShippingGoods_submit).click();

        setTimeout(function () {
            $('html, body').css('overflow', 'hidden');
            $("#dialog").dialog("open");
        }, 1000);

        DialogTitle = setInterval("SetDialogTitle()", 500);
    }
});

function OpenDialog() {
    setTimeout(function () {
        $('html, body').css('overflow', 'hidden');
        $("#dialog").dialog("open");
        $("#overlay").css("display", "block");
    }, 1000);

    DialogTitle = setInterval("SetDialogTitle()", 500);
    return true;
}

function SetDialogTitle() {
    $(".ui-dialog-title").text($(".title").text())
    if ($(".ui-dialog-title").text() != "") {
        clearInterval(DialogTitle);
    }
}

function SelectReturnStore() {
    var GoodsId = $("#GoodsId").val();
    $.ajax({
        type: "post",
        traditional: true,
        async: true,
        url: "/Gox/LimitOrder/Session_GoodsId",
        data: { GoodsId: GoodsId },
        success: function (data) {
            form1.submit();
        },
        error: function (jqXHR, exception) {
            alert("系統發生錯誤！")
        }
    });

}

//出貨
function ShippingGoods() {
    var Check = 0;
    var ShippingVal_711 = "";
    var ShippingVal_BlackCat = "";
    var lastIndex = 0;

    //7-11出貨
    for (var i = 0; i < $('input[name="Seven"]').length; i++) {
        var Seven = "#Seven_" + i;
        ShippingVal_711 += $(Seven).val() + ",";
    }
    lastIndex = ShippingVal_711.lastIndexOf(',');
    if (lastIndex > -1) {
        ShippingVal_711 = ShippingVal_711.substring(0, ShippingVal_711.length - 1);
    }
    $("#ShippingVal_711").val(ShippingVal_711);

    //黑貓出貨
    for (var i = 0; i < $('input[name="LogisticsSN"]').length; i++) {
        var LogisticsSN = ".LogisticsSN_" + i;
        var LogisticsSN_Valid = "#LogisticsSN_Valid_" + i;

        if ($(LogisticsSN).val() == "" || $(LogisticsSN).val() == null) {
            $(LogisticsSN_Valid).text("物流編號必填");
            $(LogisticsSN_Valid).css("display", "block")
            Check++;
        }
        else {
            var regex = new RegExp(/^[0-9_]+$/);
            if (regex.test($(LogisticsSN).val()) == false) {
                $(LogisticsSN_Valid).text("只能輸入數字");
                $(LogisticsSN_Valid).css("display", "block")
                Check++;
            }
        }

        var OrderDetailId_BlackCat = "#" + i;
        ShippingVal_BlackCat += $(OrderDetailId_BlackCat).val() + "=" + $(LogisticsSN).val() + ",";

    }
    lastIndex = ShippingVal_BlackCat.lastIndexOf(',');
    if (lastIndex > -1) {
        ShippingVal_BlackCat = ShippingVal_BlackCat.substring(0, ShippingVal_BlackCat.length - 1);
    }
    $("#ShippingVal_BlackCat").val(ShippingVal_BlackCat);

    //退貨門市
    var ReturnStoreName = $("#ReturnStoreName").val();
    if (ReturnStoreName == "") {
        alert("請選擇退貨門市！");
        return false
    }
    //物流編號皆都有填寫
    if (Check == 0) {
        if (confirm("確定要出貨?")) {
            ShippingGoodsForm.submit();
            $('body').css('overflow', 'hidden')
            $.isLoading({ text: "處理中" });
            // Setup Loading plugin
            $("#load-overlay .demo p").removeClass("alert-success");
            // Re-enabling event
            setTimeout(function () {
                $.isLoading("hide");
                $("#load-overlay .demo p").html("Content Loaded")
                                            .addClass("alert-success");
            }, 500000);
            return true;
        }
        else {
            return false;
        }
    }
    else {
        return false;
    }
}

function LogisticsSN_onkeydown(Sort) {
    var LogisticsSN_Valid = "#LogisticsSN_Valid_" + Sort;
    $(LogisticsSN_Valid).css("display", "none")
}

function CheckSuccess(status) {
    if (status == "Success") {
        $(".isloading-overlay").css("display", "none");
        alert("儲存成功！");
        $("html").css("overflow", "auto");
    }
    else {
        alert("儲存失敗！");
    }
}