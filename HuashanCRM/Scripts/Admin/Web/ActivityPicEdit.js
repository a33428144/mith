﻿

$(function () {
    var _URL = window.URL;

    $("input[type='file'][name=Pic1]").change(function () {
        var files = $("input[type='file'][name=Pic1]")[0].files;
        var file;
        var img = new Image();

        if ((file = this.files[0])) {
            img.onload = function () {
                var Size = Math.round(file.size / 1024);

                if (Size > 1024) {
                    alert("圖片1請勿上傳1MB以上！")
                    $("input[type='file'][name=Pic1]").val(null);
                }

            };
            img.src = _URL.createObjectURL(file);
        }
    });

    $("input[type='file'][name=Pic2]").change(function () {
        var files = $("input[type='file'][name=Pic2]")[0].files;
        var file;
        var img = new Image();

        if ((file = this.files[0])) {
            img.onload = function () {
                var Size = Math.round(file.size / 1024);

                if (Size > 1024) {
                    alert("圖片2請勿上傳1MB以上！")
                    $("input[type='file'][name=Pic2]").val(null);
                }

            };
            img.src = _URL.createObjectURL(file);
        }
    });

    $("input[type='file'][name=Pic3]").change(function () {
        var files = $("input[type='file'][name=Pic3]")[0].files;
        var file;
        var img = new Image();

        if ((file = this.files[0])) {
            img.onload = function () {
                var Size = Math.round(file.size / 1024);

                if (Size > 1024) {
                    alert("圖片3請勿上傳1MB以上！")
                    $("input[type='file'][name=Pic3]").val(null);
                }

            };
            img.src = _URL.createObjectURL(file);
        }
    });

    $("input[type='file'][name=Pic4]").change(function () {
        var files = $("input[type='file'][name=Pic4]")[0].files;
        var file;
        var img = new Image();

        if ((file = this.files[0])) {
            img.onload = function () {
                var Size = Math.round(file.size / 1024);

                if (Size > 1024) {
                    alert("圖片4請勿上傳1MB以上！")
                    $("input[type='file'][name=Pic4]").val(null);
                }

            };
            img.src = _URL.createObjectURL(file);
        }
    });

    $("input[type='file'][name=Pic5]").change(function () {
        var files = $("input[type='file'][name=Pic5]")[0].files;
        var file;
        var img = new Image();

        if ((file = this.files[0])) {
            img.onload = function () {
                var Size = Math.round(file.size / 1024);

                if (Size > 1024) {
                    alert("圖片5請勿上傳1MB以上！")
                    $("input[type='file'][name=Pic5]").val(null);
                }

            };
            img.src = _URL.createObjectURL(file);
        }
    });

});

//按下送出
function Send() {

    $('body').css('overflow', 'hidden')
    $.isLoading({ text: "處理中" });
    // Setup Loading plugin
    $("#load-overlay .demo p").removeClass("alert-success");
    // Re-enabling event
    setTimeout(function () {
        $.isLoading("hide");
        $("#load-overlay .demo p").html("Content Loaded")
                                    .addClass("alert-success");
    }, 500000);
    return true;
}