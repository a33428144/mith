﻿$(document).ready(function () {

    $("input[type='file'][name=Pic]").change(function () {
        $("#Pic_Valid").css("display", "none");
        var iSize = ($("input[type='file'][name=Pic]")[0].files[0].size / 1024);
        iSize = Math.round(iSize)
        if (iSize > 1024) {
            alert("大頭照請勿上傳1MB以上！")
            $('input[type=file][name=Pic]').val(null)
            return false;
        }
    });

    $('input[type=file][name=Cover]').change(function () {
        var iSize_BG = ($('input[type=file][name=Cover]')[0].files[0].size / 1024);
        iSize_BG = Math.round(iSize_BG)
        if (iSize_BG > 1024) {
            alert("封面照請勿上傳1MB以上！")
            $('input[type=file][name=Cover]').val(null)
            return false;
        }
    });

});
//按下送出
function Send() {

    //新增頁，需驗證大頭照
    if (window.location.href.match('Create') != null) {
        var Pic = $('input[type=file][name=Pic]').val();
        if (Pic == "" || Pic == null) {
            $("#Pic_Valid").css("display", "block");
            $('html, body').scrollTop(0);
        }

        //驗證通過
        if ($("#Form").valid() && Pic != "" && Pic != null) {
            $('body').css('overflow', 'hidden')
            $.isLoading({ text: "處理中" });
            // Setup Loading plugin
            $("#load-overlay .demo p").removeClass("alert-success");
            // Re-enabling event
            setTimeout(function () {
                $.isLoading("hide");
                $("#load-overlay .demo p").html("Content Loaded")
                                            .addClass("alert-success");
            }, 500000);
            return true;
        }
        else {

            return false
        };
    }
    //編輯頁
    else {
        //驗證通過
        if ($("#Form").valid()) {
            $('body').css('overflow', 'hidden')
            $.isLoading({ text: "處理中" });
            // Setup Loading plugin
            $("#load-overlay .demo p").removeClass("alert-success");
            // Re-enabling event
            setTimeout(function () {
                $.isLoading("hide");
                $("#load-overlay .demo p").html("Content Loaded")
                                            .addClass("alert-success");
            }, 500000);
            return true;
        }
        else {

            return false
        };
    }

}