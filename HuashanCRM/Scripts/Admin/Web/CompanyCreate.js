﻿$(function () {
    $("#CityId").change(function () {
        $.ajax({
            type: "post",
            traditional: true,
            async: true,
            url: "/Home/Ajax_Area",
            data: { city: $(this).val(), all: false },
            //dataType: "json",
            //jsonp: "callback",
            //jsonpCallback: "dool",
            success: function (data) {
                if (data) {
                    $("#AreaId").html(data);
                }
            },
            error: function (xhr, status, error) {
                alert(xhr.responseText);
            }
        });
    });

    //商品序號打字
    $("#SN").keydown(function () {
        $("#Check_SN").val("False");
        $("#SN").css("background-color", "#fff");
        $("#SN_Valid").css("display", "none");
    });

    $('input:radio[name=Type]').change(function () {
        $("#Check_SN").val("False");
        $("#SN").css("background-color", "#fff");
        $("#SN_Valid").css("display", "none");
    });

});

//Step1 按下下一步
function Step1_Send() {

    //驗證通過
    if ($("#Form").valid()) {
        return true;
    }
    else {
        $('html, body').scrollTop(0);
        return false
    };
}

//Step2 確認廠商編號
function Check_SN_Click() {
    if ($("#SN").val().length == 3) {
        var SN = $('input[name=Type]:checked').val() + $("#SN").val()
        $.ajax({
            type: "post",
            traditional: true,
            async: true,
            url: "/GoX/Company/Check_SN",
            data: { SN: SN },
            success: function (data) {
                if (data != "Reply") {
                    $("#SN").css("background-color", "#d5ffd5!important");
                    $("#SN_Valid").css("display", "inline");
                    $("#SN_Valid").css("color", "#00AA00");
                    $("#SN_Valid").text("編號可以使用");

                    $("#Check_SN").val("True")
                }
                else {
                    $("#SN").css("background-color", "#ffe5e5!important");
                    $("#SN_Valid").css("display", "inline");
                    $("#SN_Valid").css("color", "red");
                    $("#SN_Valid").text("編號重複");

                    $("#Check_SN").val("False")
                }
            }
        });
    }
}

//Step2 按下下一步
function Step2_Send() {

    var Check_SN = $("#Check_SN").val();
    if ($("#Form").valid()) {
        if (Check_SN == "True") {
            var chk_Company = "";
            var checkedList = $('input[name="GoodsKindList"]:checked');
            if (checkedList.length == 0) {
                alert("【商品種類】需至少勾選一項！")
                return false;
            }
            checkedList.each(function () {
                if (chk_Company.match($(this).val()) == null) {
                    chk_Company += $(this).val() + ",";
                }
            });
            //去除最後一個逗號
            var lastIndex = chk_Company.lastIndexOf(',');
            if (lastIndex > -1) {
                chk_Company = chk_Company.substring(0, chk_Company.length - 1);
            }

            $("#GoodsKind").val(chk_Company);
            return true;
        }
        else {
            alert("請驗證廠商編號！");
            return false;
        }
    }

}

//Step3 按下送出
function Step3_Send() {

    $('body').css('overflow', 'hidden')
    $.isLoading({ text: "處理中" });
    // Setup Loading plugin
    $("#load-overlay .demo p").removeClass("alert-success");
    // Re-enabling event
    setTimeout(function () {
        $.isLoading("hide");
        $("#load-overlay .demo p").html("Content Loaded")
                                    .addClass("alert-success");
    }, 500000);
    return true;
}

