﻿
$(window).load(function () {

    $("#ExpertId").combobox({
        select: function (event, ui) {
            var sel = this.value;
            if (sel == 0) {
                $("#ExpertId_Valid").css("display", "inline");
            }
            else {
                $("#ExpertId_Valid").css("display", "none");
            }
        }
    });

    $("input[type='file'][name=Pic1]").change(function () {
        var iSize = ($("input[type='file'][name=Pic1]")[0].files[0].size / 1024);
        iSize = Math.round(iSize)
        if (iSize > 1024) {
            alert("圖片1請勿上傳1MB以上！")
            $("input[type='file'][name=Pic1]").val(null);
            return false;
        }
    });

    $("input[type='file'][name=Pic2]").change(function () {
        var iSize = ($("input[type='file'][name=Pic2]")[0].files[0].size / 1024);
        iSize = Math.round(iSize)
        if (iSize > 1024) {
            alert("圖片2請勿上傳1MB以上！")
            $("input[type='file'][name=Pic2]").val(null);
            return false;
        }
    });

    $("input[type='file'][name=Pic3]").change(function () {
        var iSize = ($("input[type='file'][name=Pic3]")[0].files[0].size / 1024);
        iSize = Math.round(iSize)
        if (iSize > 1024) {
            alert("圖片3請勿上傳1MB以上！")
            $("input[type='file'][name=Pic3]").val(null);
            return false;
        }
    });

    $("input[type='file'][name=Pic4]").change(function () {
        var iSize = ($("input[type='file'][name=Pic4]")[0].files[0].size / 1024);
        iSize = Math.round(iSize)
        if (iSize > 1024) {
            alert("圖片4請勿上傳1MB以上！")
            $("input[type='file'][name=Pic4]").val(null);
            return false;
        }
    });

    $$("input[type='file'][name=Pic5]").change(function () {
        var iSize = ($("input[type='file'][name=Pic5]")[0].files[0].size / 1024);
        iSize = Math.round(iSize)
        if (iSize > 1024) {
            alert("圖片5請勿上傳1MB以上！")
            $("input[type='file'][name=Pic5]").val(null);
            return false;
        }
    });

    //商品序號RecommendSN1打字
    $("#RecommendSN1").keydown(function () {
        $("#Check_SN").val(null);
        $("#RecommendSN1").css("background-color", "#fff");
        $("#RecommendSN1_Valid").css("display", "none");
    });

    //商品序號RecommendSN2打字
    $("#RecommendSN2").keydown(function () {
        $("#Check_SN").val(null);
        $("#RecommendSN2").css("background-color", "#fff");
        $("#RecommendSN2_Valid").css("display", "none");
    });

    //商品序號RecommendSN3打字
    $("#RecommendSN3").keydown(function () {
        $("#Check_SN").val(null);
        $("#RecommendSN3").css("background-color", "#fff");
        $("#RecommendSN3_Valid").css("display", "none");
    });

    //商品序號RecommendSN4打字
    $("#RecommendSN4").keydown(function () {
        $("#Check_SN").val(null);
        $("#RecommendSN4").css("background-color", "#fff");
        $("#RecommendSN4_Valid").css("display", "none");
    });

    //商品序號RecommendSN5打字
    $("#RecommendSN5").keydown(function () {
        $("#Check_SN").val(null);
        $("#RecommendSN5").css("background-color", "#fff");
        $("RecommendSN5_Valid").css("display", "none");
    });

});

//確認商品編號
function Check_SN_Click(Id) {

    var Name = "#RecommendSN" + Id
    var SN = $(Name).val().replace(" ", "");

    $(Name).val(SN);
    $.ajax({
        type: "post",
        traditional: true,
        async: true,
        url: "/WebAPI/Check_RecommendSN",
        data: { RecommendSN: SN },
        //dataType: "json",
        success: function (data) {
            if (data == "true") {
                $(Name).css("background-color", "#d5ffd5!important");
                $(Name + "_Valid").css("display", "inline");
                $(Name + "_Valid").css("color", "#00AA00");
                $(Name + "_Valid").text("商品編號正確");
                $("#Check_SN").val("True");
            }
            else {
                $(Name).css("background-color", "#ffe5e5!important");
                $(Name + "_Valid").css("display", "inline");
                $(Name + "_Valid").css("color", "red");
                $(Name + "_Valid").text("商品編號錯誤");
                $("#Check_SN").val("False");
            }
        }
    });
}

//按下送出
function Send() {
    var ExpertId = $("#ExpertId").val();
    var Type = $("#Type").val();
    var Check_SN = $("#Check_SN").val();

    if ( ExpertId == "0") {
        $("#ExpertId_Valid").css("display", "inline");
    }

    if (Type == "0") {
        $("#Type_Valid").css("display", "inline");
    }

    //驗證通過
    if ($("#Form").valid() && ExpertId != "0" && Type != "0") {
        var sum = $("#RecommendSN1").val().length + $("#RecommendSN2").val().length + $("#RecommendSN3").val().length +
                        $("#RecommendSN4").val().length + $("#RecommendSN5").val().length;
        if (sum == 0) {
            Check_SN = "True";
        }

        if (Check_SN == "True") {
            $('body').css('overflow', 'hidden')
            $.isLoading({ text: "處理中" });
            // Setup Loading plugin
            $("#load-overlay .demo p").removeClass("alert-success");
            // Re-enabling event
            setTimeout(function () {
                $.isLoading("hide");
                $("#load-overlay .demo p").html("Content Loaded")
                                            .addClass("alert-success");
            }, 500000);
            return true;
        }
        else if (Check_SN == "False") {
            alert("商品序號有錯誤！")
            return false;
        }
        else {
            alert("請驗證商品序號！")
            return false;
        }
    }
    else {
        $('html, body').scrollTop(0);
        return false;
    };
};