﻿
$(window).load(function () {


    //Step1頁才執行
    if (window.location.href.match('Step1') != null) {

        //讀取DB的Value
        Check_SN_Click("Load");
        Type_SelectValue($("#Type").val());

        if ($("#SizeType").val() == 'Many') {
            $("#ManySize").css("display", "block");
            $("#OneSize").css("display", "none");
        }
        else {
            $("#ManySize").css("display", "none");
            $("#OneSize").css("display", "block");
        }

        //商品序號打字
        $("#SN").keydown(function () {
            $("#Check_SN").val("");
            $("#SN").css("background-color", "#fff");
            $("#SN_Valid").css("display", "none");

            $("#Link1").attr("href", "#");
            $("#Pic1").attr("src", "");
            $("#Pic1").css("display", "none");
        });
    }
    else {
        $("#dialog").dialog({
            autoOpen: false,
            height: 200,
            width: 560,
            close: function () {
                $("#overlay").css("display", "none");
            }
        });

    }
});


//更動類別
function Type_SelectValue(sel) {
    // 類別 = 現貨
    if (sel == 1) {
        $("#LimitOrderGroup").css("display", "none");
        $("#FewDaysGroup").css("display", "none");
        $("#SizeGroup").css("display", "block");
        $("#Type_Valid").css("display", "none");
        $("#FewDaysShipping").val(null);
        $("#LimitOrderStart").val(null);
        $("#LimitOrderEnd").val(null);
    }
        // 類別 = 預訂(製)貨
    else if (sel == 2) {
        $("#LimitOrderGroup").css("display", "none");
        $("#FewDaysGroup").css("display", "block");
        $("#SizeGroup").css("display", "block");
        $("#Type_Valid").css("display", "none");
        $("#LimitOrderStart").val(null);
        $("#LimitOrderEnd").val(null);
    }
        // 類別 = 訂製款-集量生產
    else if (sel == 3) {
        $("#LimitOrderGroup").css("display", "block");
        $("#FewDaysGroup").css("display", "block");
        $("#SizeGroup").css("display", "block");
        $("#Type_Valid").css("display", "none");
    }
        // 類別 = 特別訂購款
    else if (sel == 4) {
        $("#LimitOrderGroup").css("display", "none");
        $("#FewDaysGroup").css("display", "none");
        $("#SizeGroup").css("display", "none");
        $("#Type_Valid").css("display", "none");
        $("#FewDaysShipping").val(null);
        $("#LimitOrderStart").val(null);
        $("#LimitOrderEnd").val(null);
    }
    else {
        $("#LimitOrderGroup").css("display", "none");
        $("#FewDaysGroup").css("display", "none");
        $("#SizeGroup").css("display", "block");
        $("#Type_Valid").css("display", "inline");
        $("#FewDaysShipping").val(null);
        $("#LimitOrderStart").val(null);
        $("#LimitOrderEnd").val(null);
    }
}

//確認商品編號
function Check_SN_Click(action) {
    var SN = $("#SN").val().replace(" ", "");
    $("#SN").val(SN);
    $.ajax({
        type: "post",
        traditional: true,
        async: true,
        url: "/GoX/Goods/Check_SN",
        data: { SN: SN },
        dataType: "json",
        success: function (data) {
            var length = data.PicList.length;
            var count = 0;
            if (action == "Click") {
                count = data.Count;
            }
            var sum = length - count;
            $("#Check_SN").val(length);
            if (length > 0 && count == 0) {
                $("#SN").css("background-color", "#d5ffd5!important");
                $("#SN_Valid").css("display", "none");
                $.each(data, function (index, item) {
                    $.each(item, function (index, item) {
                        if (item.Pic1_Big != null && item.Pic1_Big != "") {
                            $("#Link1").attr("href", item.Pic1_Big);
                            $("#Pic1").attr("src", item.Pic1_Big)
                            $("#Pic1").css("display", "inline")
                        }
                    });
                });
            }
            else if (length > 0 && count > 0) {
                $("#SN").css("background-color", "#ffe5e5!important");
                $("#SN_Valid").css("display", "inline");
                $("#SN_Valid").text("商品編號已被使用");

                $("#Link1").attr("href", "#");
                $("#Pic1").attr("src", "");
                $("#Pic1").css("display", "none");
            }
            else if (length == 0 && count == 0) {
                $("#SN").css("background-color", "#ffe5e5!important");
                $("#SN_Valid").css("display", "inline");
                $("#SN_Valid").text("商品編號錯誤");

                $("#Link1").attr("href", "#");
                $("#Pic1").attr("src", "");
                $("#Pic1").css("display", "none");
            }
        }
    });
}

function OpenDialog() {
    $("#UpdateArea").css("display", "block");
    $("#dialog").dialog("open");
    $("#overlay").css("display", "block");
}

//Step1按下送出
function Step1_Send() {
    var SN = $("#SN").val();
    var Check_SN = $("#Check_SN").val();

    if (SN != "" && Check_SN == "") {
        $("#SN_Valid").css("display", "inline");
        $("#SN").css("background-color", "#ffe5e5")
        //$("#Code").css("background-color", "#cce2fc")
        $("#SN_Valid").text("驗證商品序號");
        $('html, body').scrollTop(0);
    }
    if (Check_SN != "" && Check_SN != 0) {
        $('body').css('overflow', 'hidden')
        $.isLoading({ text: "處理中" });
        // Setup Loading plugin
        $("#load-overlay .demo p").removeClass("alert-success");
        // Re-enabling event
        setTimeout(function () {
            $.isLoading("hide");
            $("#load-overlay .demo p").html("Content Loaded")
                                        .addClass("alert-success");
        }, 500000);
        return true;
    }
    else {
        if (SN == null || SN == "") {
            $('html, body').scrollTop(0);
        }
        return false
    };
}


//Step2 按下送出
function Step2_Send() {

    //---------------------------風格---------------------------
    var chk_Style = "";
    var checkedList_Style = $('input[name="StyleList"]:checked');
    if (checkedList_Style.length == 0) {
        alert("【風格】需至少勾選一項！")
        return false;
    }
    else if (checkedList_Style.length > 2) {
        alert("【風格】最多勾選兩項！")
        return false;
    }
    checkedList_Style.each(function () {
        if (chk_Style.match($(this).val()) == null) {
            chk_Style += $(this).val() + ",";
        }
    });
    //去除最後一個逗號
    var lastIndex = chk_Style.lastIndexOf(',');
    if (lastIndex > -1) {
        chk_Style = chk_Style.substring(0, chk_Style.length - 1);
    }
    $("#Style").val(chk_Style);


    //---------------------------適合---------------------------
    var chk_Fit = "";
    var checkedList_Fit = $('input[name="FitList"]:checked');
    if (checkedList_Fit.length == 0) {
        alert("【適合】需至少勾選一項！")
        return false;
    }
    else if (checkedList_Fit.length > 2) {
        alert("【適合】最多勾選兩項！")
        return false;
    }
    checkedList_Fit.each(function () {
        if (chk_Fit.match($(this).val()) == null) {
            chk_Fit += $(this).val() + ",";
        }
    });
    //去除最後一個逗號
    var lastIndex2 = chk_Fit.lastIndexOf(',');
    if (lastIndex2 > -1) {
        chk_Fit = chk_Fit.substring(0, chk_Fit.length - 1);
    }
    $("#Fit").val(chk_Fit);


    $('body').css('overflow', 'hidden')
    $.isLoading({ text: "處理中" });
    // Setup Loading plugin
    $("#load-overlay .demo p").removeClass("alert-success");
    // Re-enabling event
    setTimeout(function () {
        $.isLoading("hide");
        $("#load-overlay .demo p").html("Content Loaded")
                                    .addClass("alert-success");
    }, 500000);
    return true;
}

