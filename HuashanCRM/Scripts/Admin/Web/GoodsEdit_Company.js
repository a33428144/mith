﻿
$(window).load(function () {

    //Step1頁才執行
    if (window.location.href.match('Step1') != null) {

        //廠商不是集單類型，類別移除"集單"的選項
        if ($("#IsLimitOrder").val() == "False") {
            $('#Type option[value=3]').remove();
            $("#ChangeText1").text("庫存量");
            $("#ChangeText2").text("庫存量");
        }

        if ($('#BrandId option').length == 0) {
            alert("需至少設定一個品牌，請洽亞設人員！")
            location.href = "/GoX/Goods/Index_Company"
        }

        Check_SN_Click("Load");
        Type_SelectValue($("#Type").val());

        if ($('input:radio:checked[name="SizeType"]').val() == 'Many') {
            $("#ManySize").css("display", "block");
            $("#OneSize").css("display", "none");
        }
        else {
            $("#ManySize").css("display", "none");
            $("#OneSize").css("display", "block");
        }

        //商品序號打字
        $("#SN").keydown(function () {
            $("#Check_SN").val("");
            $("#SN").css("background-color", "#fff");
            $("#SN_Valid").css("display", "none");

            $("#Link1").attr("href", "#");
            $("#Pic1").attr("src", "");
            $("#Pic1").css("display", "none");
        });

        $("#BrandId").combobox({
            select: function (event, ui) {
                var sel = this.value;
                if (sel == "0") {
                    $("#BrandId_Valid").css("display", "block");
                }
                else {
                    $("#BrandId_Valid").css("display", "none");
                }
            }
        });

        $("#Type").combobox({
            select: function (event, ui) {

                var sel = this.value;
                Type_SelectValue(sel);
            }
        });

        $('input:radio[name=SizeType]').change(function () {
            if (this.value == 'Many') {
                $("#ManySize").css("display", "block");
                $("#OneSize").css("display", "none");
            }
            else {
                $("#ManySize").css("display", "none");
                $("#OneSize").css("display", "block");
            }
        });


    }
        //Step2頁才執行
    else {

        Check_SN_Click("Click");

        if ($("#PicCount").val() == 0) {
            $("#PicLink").css("display", "none");
        }

        $("#dialog").dialog({
            autoOpen: false,
            height: 280,
            width: 580,
            close: function () {
                $("#overlay").css("display", "none");
            }
        });

        $("#dialog2").dialog({
            autoOpen: false,
            height: 220,
            width: 350,
            close: function () {
                $('html, body').css('overflow', 'auto');
                $("#overlay").css("display", "none");
            }
        });

        //圖片上傳
        $("input[type='file'][name=Files]").change(function () {
            var files = this.files;
            var count = parseInt($("#PicCount").val()) + parseInt(files.length);
            //最多6張
            if (count > 6) {
                alert("商品照片最多上傳6張！");
                $("input[type='file'][name=Files]").val(null);
            }
            else {
                $("#Files").css("width", "82px")
                $("#ShowFileName").css("display", "block");
                $("#ShowFileName").html("<legend>上傳檔案</legend>");
                //至少傳一張圖
                if (files.length > 0) {
                    $("#save_sort").css("display", "none");
                    for (var i = 0; i < files.length; i++) {
                        ReadFiles(files[i]);
                    }
                }
                    //沒有傳圖片
                else {
                    $("#save_sort").css("display", "block");
                }
            }
            setTimeout(function () {
                //有圖片尺寸錯誤
                if ($("#SizeError").length > 0) {
                    $("#save_sort").css("display", "none");
                }
                else {
                    $("#save_sort").css("display", "block");
                }
            }, 500);

        });
    }
});

var width, height, filename;

function ReadFiles(file) {

    var reader = new FileReader();
    var image = new Image();

    reader.readAsDataURL(file);
    reader.onload = function (_file) {
        image.src = _file.target.result;
        image.onload = function () {
            width = this.width;
            height = this.height,
            filename = file.name;

            if (this.width > 1080 || this.height > 800) {
                $('#ShowFileName').append("<div id='SizeError' style='color:red'>" + file.name + " 尺寸不符規定！" + "</div> <br>");
            }
            else {
                $('#ShowFileName').append("<div>" + file.name + "</div> <br>");
            }

            var Size = Math.round(file.size / 1024);

            if (Size > 1024) {
                $('#ShowFileName').append("<div id='SizeError' style='color:red'>" + file.name + " 請勿上傳1MB以上！" + "</div> <br>");
            }
        };
    };
}

function OpenDialog_QA() {

    $('html, body').css('overflow', 'hidden');
    $("#dialog2").dialog("open");
    $("#overlay").css("display", "block");
}


//更動類別
function Type_SelectValue(sel) {
    // 類別 = 現貨
    if (sel == 1) {
        $("#LimitOrderGroup").css("display", "none");
        $("#FewDaysGroup").css("display", "none");
        $("#SizeGroup").css("display", "block");
        $("#Type_Valid").css("display", "none");
        $("#FewDaysShipping").val(null);
        $("#LimitOrderStart").val(null);
        $("#LimitOrderEnd").val(null);
    }
        // 類別 = 預訂(製)貨
    else if (sel == 2) {
        $("#LimitOrderGroup").css("display", "none");
        $("#FewDaysGroup").css("display", "block");
        $("#SizeGroup").css("display", "block");
        $("#Type_Valid").css("display", "none");
        $("#LimitOrderStart").val(null);
        $("#LimitOrderEnd").val(null);
    }
        // 類別 = 訂製款-集量生產
    else if (sel == 3) {
        $("#LimitOrderGroup").css("display", "block");
        $("#FewDaysGroup").css("display", "block");
        $("#SizeGroup").css("display", "block");
        $("#Type_Valid").css("display", "none");
    }
        // 類別 = 特別訂購款
    else if (sel == 4) {
        $("#LimitOrderGroup").css("display", "none");
        $("#FewDaysGroup").css("display", "none");
        $("#SizeGroup").css("display", "none");
        $("#Type_Valid").css("display", "none");
        $("#FewDaysShipping").val(null);
        $("#LimitOrderStart").val(null);
        $("#LimitOrderEnd").val(null);
    }
    else {
        $("#LimitOrderGroup").css("display", "none");
        $("#FewDaysGroup").css("display", "none");
        $("#SizeGroup").css("display", "block");
        $("#Type_Valid").css("display", "inline");
        $("#FewDaysShipping").val(null);
        $("#LimitOrderStart").val(null);
        $("#LimitOrderEnd").val(null);
    }
}

//確認商品編號
function Check_SN_Click(action) {
    var SN = $("#SN").val().replace(" ", "");
    $("#SN").val(SN);
    $.ajax({
        type: "post",
        traditional: true,
        async: true,
        url: "/GoX/Goods/Check_SN",
        data: { SN: SN },
        dataType: "json",
        success: function (data) {
            var length = data.PicList.length;
            var count = 0;
            if (action == "Click") {
                count = data.Count;
            }
            var sum = length - count;
            $("#Check_SN").val(length);
            if (length > 0 && count == 0) {
                $("#SN").css("background-color", "#d5ffd5!important");
                $("#SN_Valid").css("display", "none");
                $.each(data, function (index, item) {
                    $.each(item, function (index, item) {
                        if (item.Pic1_Big != null && item.Pic1_Big != "") {
                            $("#Link1").attr("href", item.Pic1_Big);
                            $("#Pic1").attr("src", item.Pic1_Big)
                            $("#Pic1").css("display", "inline")
                        }
                    });
                });
            }
            else if (length > 0 && count > 0) {
                $("#SN").css("background-color", "#ffe5e5!important");
                $("#SN_Valid").css("display", "inline");
                $("#SN_Valid").text("商品編號已被使用");

                $("#Link1").attr("href", "#");
                $("#Pic1").attr("src", "");
                $("#Pic1").css("display", "none");
            }
            else if (length == 0 && count == 0) {
                $("#SN").css("background-color", "#ffe5e5!important");
                $("#SN_Valid").css("display", "inline");
                $("#SN_Valid").text("商品編號錯誤");

                $("#Link1").attr("href", "#");
                $("#Pic1").attr("src", "");
                $("#Pic1").css("display", "none");
            }
        }
    });
}

function OpenDialog() {
    $("#UpdateArea").css("display", "block");
    $("#dialog").dialog("open");
    $("#overlay").css("display", "block");
}

function DeletePic() {
    var checkedList = $('input[name="delete"]:checked');
    var Send = true;

    var Show = $('input[name=Show]:checked').val();
    if (confirm("確定要儲存?")) {
        checkedList.each(function () {
            if (Show == $(this).val()) {
                alert("封面照無法刪除！");
                Send = false;
                return false;
            }
        });

        return Send;
    }
    else {
        return false;
    }
};

function Show_Loading() {
    $(".isloading-overlay").css("background", "none")
    $('body').css('overflow', 'hidden')
    $.isLoading({ text: "處理中" });
    // Setup Loading plugin
    $("#load-overlay .demo p").removeClass("alert-success");
    // Re-enabling event

    setTimeout(function () {
        $.isLoading("hide");
        $("#load-overlay .demo p").html("Content Loaded")
                                    .addClass("alert-success");
    }, 450000);
}

function Ajax_Success() {
    $(".isloading-overlay").css("display", "none");
    $('html, body').css('overflowY', 'auto');
}

//Step1按下送出
function Step1_Send() {
    var SN = $("#SN").val();
    var Check_SN = $("#Check_SN").val();
    var BrandId = $("#BrandId").val();
    var Type = $("#Type").val();
    var Price = $("#Price").val();
    var SpecialOffer = $("#SpecialOffer").val();
    var LimitOrderStart = new Date($('#LimitOrderStart').val());
    var LimitOrderEnd = new Date($('#LimitOrderEnd').val());
    if (SN != "" && Check_SN == "") {
        $("#SN_Valid").css("display", "inline");
        $("#SN").css("background-color", "#ffe5e5")
        //$("#Code").css("background-color", "#cce2fc")
        $("#SN_Valid").text("驗證商品序號");
        $('html, body').scrollTop(0);
    }
    if (BrandId == "0") {
        $("#BrandId_Valid").css("display", "block");
    }

    if (Type == "0") {
        $("#Type_Valid").css("display", "inline");
    }


    //驗證
    if ($("#Form").valid() && Check_SN != "" && Check_SN != 0 && BrandId != "0" && Type != "0") {

        if (LimitOrderStart.getTime() > LimitOrderEnd.getTime()) {
            alert("【集單日期】的開始日期不能大於結束日期！");
            return false;
        }

        if (SpecialOffer != "") {
            if (parseInt(SpecialOffer) >= parseInt(Price)) {
                alert("特價不能大於 (等於) 售價！");
                $('html, body').scrollTop(0);
                return false;
            }
        }

        $('body').css('overflow', 'hidden')
        $.isLoading({ text: "處理中" });
        // Setup Loading plugin
        $("#load-overlay .demo p").removeClass("alert-success");
        // Re-enabling event
        setTimeout(function () {
            $.isLoading("hide");
            $("#load-overlay .demo p").html("Content Loaded")
                                        .addClass("alert-success");
        }, 500000);
        return true;
    }
    else {
        if (SN == null || SN == "") {
            $('html, body').scrollTop(0);
        }
        return false
    };
}


//Step2 按下送出
function Step2_Send() {

    //---------------------------風格---------------------------
    var chk_Style = "";
    var checkedList_Style = $('input[name="StyleList"]:checked');
    if (checkedList_Style.length == 0) {
        alert("【風格】需至少勾選一項！")
        return false;
    }
    else if (checkedList_Style.length > 2) {
        alert("【風格】最多勾選兩項！")
        return false;
    }
    checkedList_Style.each(function () {
        if (chk_Style.match($(this).val()) == null) {
            chk_Style += $(this).val() + ",";
        }
    });
    //去除最後一個逗號
    var lastIndex = chk_Style.lastIndexOf(',');
    if (lastIndex > -1) {
        chk_Style = chk_Style.substring(0, chk_Style.length - 1);
    }
    $("#Style").val(chk_Style);


    //---------------------------適合---------------------------
    var chk_Fit = "";
    var checkedList_Fit = $('input[name="FitList"]:checked');
    if (checkedList_Fit.length == 0) {
        alert("【適合】需至少勾選一項！")
        return false;
    }
    else if (checkedList_Fit.length > 2) {
        alert("【適合】最多勾選兩項！")
        return false;
    }
    checkedList_Fit.each(function () {
        if (chk_Fit.match($(this).val()) == null) {
            chk_Fit += $(this).val() + ",";
        }
    });
    //去除最後一個逗號
    var lastIndex2 = chk_Fit.lastIndexOf(',');
    if (lastIndex2 > -1) {
        chk_Fit = chk_Fit.substring(0, chk_Fit.length - 1);
    }
    $("#Fit").val(chk_Fit);


    $('body').css('overflow', 'hidden')
    $.isLoading({ text: "處理中" });
    // Setup Loading plugin
    $("#load-overlay .demo p").removeClass("alert-success");
    // Re-enabling event
    setTimeout(function () {
        $.isLoading("hide");
        $("#load-overlay .demo p").html("Content Loaded")
                                    .addClass("alert-success");
    }, 500000);
    return true;
}

