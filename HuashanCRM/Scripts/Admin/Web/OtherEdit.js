﻿$(function () {
    var _URL = window.URL;

    $("#Pic_Big").change(function () {
        var files = $('#Pic_Big')[0].files;

        for (var i = 0; i < files.length; i++) {
            var j = i + 1;

            var file = files[i].name.split(".");
            var file;
            var name = file[0];
            var extension = file[1];
            var img = new Image();
            if ((file = this.files[i])) {
                img.onload = function () {
                    var sum = this.width / this.height;
                    if (sum != 1) {
                        $("#ShowPic_Big").text("比例錯誤，長寬比應該為 1:1");
                        $("#save_sort").css("display", "none");
                    }
                    else {
                        $("#ShowPic_Big").text("");
                        $("#save_sort").css("display", "block");
                    }
                };
                img.src = _URL.createObjectURL(file);
            }
        }
    });
});


//按下送出
function Send() {
    //驗證通過
    if ($("#Form").valid()) {
        $('body').css('overflow', 'hidden')
        $.isLoading({ text: "處理中" });
        // Setup Loading plugin
        $("#load-overlay .demo p").removeClass("alert-success");
        // Re-enabling event
        setTimeout(function () {
            $.isLoading("hide");
            $("#load-overlay .demo p").html("Content Loaded")
                                        .addClass("alert-success");
        }, 500000);
        return true;
    }
    else {

        return false
    };
}