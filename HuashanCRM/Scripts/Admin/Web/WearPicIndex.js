﻿
$(function () {

    //單選打勾
    $(".delete").on("change", function () {
        var chk_Wear = "";
        //載入Cookie的值
        if ($.cookie('chk_Wear') != null && $.cookie('chk_Wear') != "") {
            chk_Wear += $.cookie('chk_Wear') + ",";
        }
        //把打勾取消
        if (chk_Wear.match($(this).val()) != null) {
            // Id + ","
            chk_Wear = chk_Wear.replace($(this).val() + ",", "");
        }
        //有勾選的CheckBox
        var checkedList = $('input[name="item"]:checked');
        checkedList.each(function () {
            if (chk_Wear.match($(this).val()) == null) {
                chk_Wear += $(this).val() + ",";
            }
        });
        //如果第一個字是逗號，去掉
        var Index = chk_Wear.indexOf(",");
        if (Index == 0) {
            chk_Wear = chk_Wear.substring(1);
        }

        //去除最後一個逗號
        var lastIndex = chk_Wear.lastIndexOf(',');
        if (lastIndex > -1) {
            chk_Wear = chk_Wear.substring(0, chk_Wear.length - 1);
        }

        //將值塞到Cookie
        var date = new Date();
        //Cookie預設十分鐘
        date.setTime(date.getTime() + (600 * 1000));
        $.cookie('chk_Wear', chk_Wear, { expires: date, path: '/' })
    });

    $.cookie.json = true;
    //載入有勾選的值
    Get_CheckboxVal();
});

//全選打勾
function AllCheck() {
    var chk_Wear = "";

    //載入Cookie的值
    if ($.cookie('chk_Wear') != null && $.cookie('chk_Wear') != "") {
        chk_Wear += $.cookie('chk_Wear') + ",";
    }

    $(".delete").each(function () {
        if ($("#AllCheck").prop("checked")) {
            $(this).prop("checked", $("#AllCheck").prop("checked"));
            if (chk_Wear.match($(this).val()) == null) {
                chk_Wear += $(this).val() + ",";
            }

        }
        else {
            $(this).prop("checked", false);
            //把打勾取消
            if (chk_Wear.match($(this).val()) != null) {
                // Id + ","
                chk_Wear = chk_Wear.replace($(this).val() + ",", "");
            }
        }
    });

    //如果第一個字是逗號，去掉
    var Index = chk_Wear.indexOf(",");
    if (Index == 0) {
        chk_Wear = chk_Wear.substring(1);
    }

    //去除最後一個逗號
    var lastIndex = chk_Wear.lastIndexOf(',');
    if (lastIndex > -1) {
        chk_Wear = chk_Wear.substring(0, chk_Wear.length - 1);
    }

    //將值塞到Cookie
    var date = new Date();
    //Cookie預設十分鐘
    date.setTime(date.getTime() + (600 * 1000));
    $.cookie('chk_Wear', chk_Wear, { expires: date, path: '/' })
};

//載入有勾選的值
function Get_CheckboxVal() {

    var chk_Wear = "";

    if (typeof $.cookie('chk_Wear') != 'undefined') {
        chk_Wear = $.cookie('chk_Wear');
    }
    $(".delete").each(function () {
        if (chk_Wear != "") {
            if (chk_Wear.match(this.id) != null) {
                $("#" + this.id).prop('checked', true);
            }
        }
    });
}

//上架
function PutOnSale() {

    var chk_Wear;
    if (typeof $.cookie('chk_Wear') != 'undefined') {
        chk_Wear = $.cookie('chk_Wear');
    }
    if (chk_Wear != null) {
        if (confirm("確定要上架?")) {
            $('body').css('overflow', 'hidden')
            $.isLoading({ text: "處理中" });
            // Setup Loading plugin
            $("#load-overlay .demo p").removeClass("alert-success");
            // Re-enabling event
            setTimeout(function () {
                $.isLoading("hide");
                $("#load-overlay .demo p").html("Content Loaded")
                                            .addClass("alert-success");
            }, 500000);

            $.ajax({
                type: "post",
                traditional: true,
                async: true,
                url: "/Gox/WearPic/Update_Display",
                data: { SN: chk_Wear, Display: true },
                success: function (data) {
                    if (data = "Success") {
                        //將值塞到Cookie
                        var date = new Date();
                        //Cookie預設5秒
                        date.setTime(date.getTime() + (5 * 1000));
                        $.cookie('chk_Wear', null, { expires: date, path: '/' });
                        location.href = location.href;
                        CheckSuccess(data);
                    }
                },
                error: function (jqXHR, exception) {
                    alert("系統發生錯誤！")
                }
            });
        }
    }
    else {
        alert("未勾選任何項目！");
        return false;
    }
}

//下架
function PullOffShelves() {

    var chk_Wear;
    if (typeof $.cookie('chk_Wear') != 'undefined') {
        chk_Wear = $.cookie('chk_Wear');
    }
    if (chk_Wear != null) {
        if (confirm("確定要下架?")) {
            $('body').css('overflow', 'hidden')
            $.isLoading({ text: "處理中" });
            // Setup Loading plugin
            $("#load-overlay .demo p").removeClass("alert-success");
            // Re-enabling event
            setTimeout(function () {
                $.isLoading("hide");
                $("#load-overlay .demo p").html("Content Loaded")
                                            .addClass("alert-success");
            }, 500000);

            $.ajax({
                type: "post",
                traditional: true,
                async: true,
                url: "/Gox/WearPic/Update_Display",
                data: { SN: chk_Wear, Display: false },
                success: function (data) {
                    if (data = "Success") {
                        //將值塞到Cookie
                        var date = new Date();
                        //Cookie預設5秒
                        date.setTime(date.getTime() + (5 * 1000));
                        $.cookie('chk_Wear', null, { expires: date, path: '/' });
                        location.href = location.href;
                        CheckSuccess(data);
                    }
                },
                error: function (jqXHR, exception) {
                    alert("系統發生錯誤！")
                }
            });
        }
    }
    else {
        alert("未勾選任何項目！");
        return false;
    }
}


function Ajax_Delete() {
    var chk_Wear = "";
    if (typeof $.cookie('chk_Wear') != 'undefined') {
        chk_Wear = $.cookie('chk_Wear');
    }
    if (chk_Wear != null) {
        if (confirm("確定要刪除?")) {
            $('body').css('overflow', 'hidden')
            $.isLoading({ text: "處理中" });
            // Setup Loading plugin
            $("#load-overlay .demo p").removeClass("alert-success");
            // Re-enabling event
            setTimeout(function () {
                $.isLoading("hide");
                $("#load-overlay .demo p").html("Content Loaded")
                                            .addClass("alert-success");
            }, 500000);

            $.ajax({
                type: "post",
                traditional: true,
                async: true,
                url: "/Gox/WearPic/Ajax_Delete",
                data: { Id: chk_Wear},
                success: function (data) {
                    if (data = "Success") {
                        //將值塞到Cookie
                        var date = new Date();
                        //Cookie預設5秒
                        date.setTime(date.getTime() + (5 * 1000));
                        $.cookie('chk_Wear', null, { expires: date, path: '/' });
                        location.href = location.href;
                        CheckSuccess(data);
                    }
                },
                error: function (jqXHR, exception) {
                    alert("系統發生錯誤！")
                }
            });
        }
      }
    else {
        alert("未勾選任何項目！");
        return false;
    }
}

function CheckSuccess(status) {
    if (status == "Success") {
        $(".isloading-overlay").css("display", "none");
        alert("儲存成功！");
        $("html").css("overflow", "auto");
    }
    else {
        alert("儲存失敗！");
    }
}