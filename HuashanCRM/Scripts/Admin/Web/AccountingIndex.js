﻿
$(window).load(function () {

    //載入 Search_Direction
    var Search_Direction = $('input[name=Search_Direction]:checked').val()
    if (Search_Direction == 0) {
        $("#span_CashFlow1").css("display", "none")
        $("#span_CashFlow2").css("display", "none")

        if (typeof ($('input:radio[name=Search_CashFlow]')[0]) != "undefined") {
            $('input:radio[name=Search_CashFlow]')[0].checked = true;
        }
        if (typeof ($('input:radio[name=Search_LogisticsStatus]')[0]) != "undefined") {
            $('input:radio[name=Search_LogisticsStatus]')[0].checked = true;
        }
    }
    else if (Search_Direction == 1) {
        $("#span_CashFlow1").css("display", "inline")
        $("#span_CashFlow2").css("display", "none")

    }
    else if (Search_Direction == 2) {
        $("#span_CashFlow1").css("display", "none")
        $("#span_CashFlow2").css("display", "inline")
    }


    $('input[name=Search_Direction]').change(function () {

        if (this.value == 0) {
            $("#span_CashFlow1").css("display", "none")
            $("#span_CashFlow2").css("display", "none")

            if (typeof ($('input:radio[name=Search_CashFlow]')[0]) != "undefined") {
                $('input:radio[name=Search_CashFlow]')[0].checked = true;
            }
            if (typeof ($('input:radio[name=Search_LogisticsStatus]')[0]) != "undefined") {
                $('input:radio[name=Search_LogisticsStatus]')[0].checked = true;
            }
            
        }
        else if (this.value == 1) {
            $("#span_CashFlow1").css("display", "inline")
            $("#span_CashFlow2").css("display", "none")

        }
        else if (this.value == 2) {
            $("#span_CashFlow1").css("display", "none")
            $("#span_CashFlow2").css("display", "inline")
        }
    });
});

function Search_Mith() {
    document.SearchForm.action = "/GoX/Accounting/Index_Mith";
    document.SearchForm.method = "get";
    SearchForm.submit();
};

function Search_Company() {
    document.SearchForm.action = "/GoX/Accounting/Index_Company";
    document.SearchForm.method = "get";
    SearchForm.submit();
};

function Export_Mith() {
    document.SearchForm.action = "/GoX/Accounting/Export";
    document.SearchForm.method = "post";
    SearchForm.submit();
};

function Export_Company() {
    document.SearchForm.action = "/GoX/Accounting/Export";
    document.SearchForm.method = "post";
    SearchForm.submit();
};