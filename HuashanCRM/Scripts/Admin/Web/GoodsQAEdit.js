﻿
//按下送出
function Send() {
    //驗證通過
    if ($("#Form").valid()) {
        $('body').css('overflow', 'hidden')
        $.isLoading({ text: "處理中" });
        // Setup Loading plugin
        $("#load-overlay .demo p").removeClass("alert-success");
        // Re-enabling event
        setTimeout(function () {
            $.isLoading("hide");
            $("#load-overlay .demo p").html("Content Loaded")
                                        .addClass("alert-success");
        }, 500000);
        return true;
    }
    else {
        return false
    };
}