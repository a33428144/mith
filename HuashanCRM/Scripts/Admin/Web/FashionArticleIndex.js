﻿
$(function () {

    var form = $("#SearchForm");
    //讀取區塊的Val
    var Search_Area = $('input:radio:checked[name="Search_Area"]').val();
    if (Search_Area == "Left") {
        form.attr('action', '/GoX/FashionArticle/Show_Area1');
        form.attr('data-ajax-update', '#Update_Show_Area1');
    }
    else {
        form.attr('action', '/GoX/FashionArticle/Show_Area2');
        form.attr('data-ajax-update', '#Update_Show_Area2');
    }

    $('input[type=radio][name=Search_Area]').change(function () {
        if (this.value == "Left") {
            form.attr('action', '/GoX/FashionArticle/Show_Area1');
            form.attr('data-ajax-update', '#Update_Show_Area1');
        }
        else {
            form.attr('action', '/GoX/Activity/Show_Area2');
            form.attr('data-ajax-update', '#Update_Show_Area2');
        }
    });

    Change_height();
});

function Search() {

    //類別
    if ($("#keyword").val() !="" || $("#Search_Type").val() != "0") {
        return true;
    }
    else {
        alert("請輸入標題，或選擇分類！")
        return false;
    }
}

//點移至置頂
function ToOnTop() {

    if (confirm("確定要顯示在首頁文章?")) {
        var chk_Article = "";
        var Area1 = $(".Area1>li").length;
        //有勾選的CheckBox
        var checkedList = $('input[name="Area2Chk"]:checked');

        if (checkedList.length == 0) {
            alert("未勾選任何項目！");
            return false;
        }

        if (parseInt(Area1 + checkedList.length) > 6) {
            alert("首頁文章：" + Area1 + "篇，非首頁您勾選了：" + checkedList.length + "篇\n相加已超過6篇！")
            return false;
        }

        checkedList.each(function () {
            chk_Article += $(this).val() + ",";
            Move_li($(this).val(), "ToOnTop");
        });

        //去除最後一個逗號
        var lastIndex = chk_Article.lastIndexOf(',');
        if (lastIndex > -1) {
            chk_Article = chk_Article.substring(0, chk_Article.length - 1);
        }

        //將值塞到Cookie
        var date = new Date();
        //Cookie預設十分鐘
        date.setTime(date.getTime() + (40 * 1000));
        $.cookie('chk_Article', chk_Article, { expires: date, path: '/' })

        Show_Loading();

        $.ajax({
            type: "post",
            traditional: true,
            async: true,
            url: "/GoX/FashionArticle/Ajax_OnTop",
            data: {
                chk_Article: $.cookie('chk_Article'),
                OnTop: true
            },
            success: function (data) {
                Get_Result(data);
            }
        });

        return true;
    }
    else {
        return false;
    }
}

//點移至非首頁
function ToNotTop() {
    if (confirm("確定要從首頁文章移除?")) {

        var chk_Article = "";
        //有勾選的CheckBox
        var checkedList = $('input[name="Area1Chk"]:checked');

        if (checkedList.length == 0) {
            alert("未勾選任何項目！");
            return false;
        }

        checkedList.each(function () {
            if (chk_Article.match($(this).val()) == null) {
                chk_Article += $(this).val() + ",";
                Move_li($(this).val(), "ToNotTop");
            }
        });

        //去除最後一個逗號
        var lastIndex = chk_Article.lastIndexOf(',');
        if (lastIndex > -1) {
            chk_Article = chk_Article.substring(0, chk_Article.length - 1);
        }

        //將值塞到Cookie
        var date = new Date();
        //Cookie預設十分鐘
        date.setTime(date.getTime() + (40 * 1000));
        $.cookie('chk_Article', chk_Article, { expires: date, path: '/' })

        Show_Loading();

        $.ajax({
            type: "post",
            traditional: true,
            async: true,
            url: "/GoX/FashionArticle/Ajax_OnTop",
            data: {
                chk_Article: chk_Article,
                OnTop: false
            },
            success: function (data) {
                Get_Result(data);
            }
        });

        return true;
    }
    else {
        return false;
    }
}

function Show_Loading() {
    $(".isloading-overlay").css("background", "none")
    $('body').css('overflow', 'hidden')
    $.isLoading({ text: "處理中" });
    // Setup Loading plugin
    $("#load-overlay .demo p").removeClass("alert-success");
    // Re-enabling event

    setTimeout(function () {
        $.isLoading("hide");
        $("#load-overlay .demo p").html("Content Loaded")
                                    .addClass("alert-success");
    }, 450000);
}

function Ajax_Success() {
    Change_height();
    $(".isloading-overlay").css("display", "none");
    $('html, body').css('overflowY', 'auto');
}

function Get_Result(status) {
    $(".isloading-overlay").css("display", "none");

    if (status == "Success") {
        alert("儲存成功！");
        $("html").css("overflow", "auto");
    }
    else {
        alert("儲存失敗！");
    }
}

//跑迴圈
function Get_data(target) {
    var arr = [];
    $(target).each(function () {
        arr.push($(this).attr("data-id"));
    });
    return arr;
}

//更新Area > li項目
function Move_li(Id, Action) {

    //將項目從【非置頂】移到【置頂】
    if (Action == "ToOnTop") {
        var li_Id = "#li2_" + Id;
        var li_html = "<li class='item ui-state-default' data-id='" + Id + "' id='li1_" + Id + "'>" + $(li_Id).html() + "</li>";
        $(li_Id).remove();
        $("#Update_Show_Area1").prepend(li_html);
    }
        //將項目從【置頂】移到【非置頂】
    else {
        var li_Id = "#li1_" + Id;
        var li_html = "<li class='item ui-state-default' data-id='" + Id + "' id='li2_" + Id + "'>" + $(li_Id).html() + "</li>";
        $(li_Id).remove();
        $("#Update_Show_Area2").prepend(li_html);
    }
}


//調整高度
function Change_height() {

    var Area1_count = $(".Area1>li").length;
    var Area2_count = $(".Area2>li").length;

    //其他路線 高度
    if (Area1_count > Area2_count) {
        $(".Area1").css("height", 45 * Area1_count);
        $(".Area2").css("height", 45 * Area1_count);
    }
    else {
        $(".Area1").css("height", 45 * Area2_count);
        $(".Area2").css("height", 45 * Area2_count);
    }

    //如果都沒資料，預設高度為125px
    if (Area1_count < 3 && Area2_count < 3) {
        $(".Area1").css("height", 125);
        $(".Area2").css("height", 125);
    }
}

