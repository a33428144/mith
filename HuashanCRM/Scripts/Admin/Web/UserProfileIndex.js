﻿
$(function () {
    //更動角色
    $("#Search_Role").combobox({
        select: function (event, ui) {
            var sel = this.value;
            $.ajax({
                type: "post",
                traditional: true,
                async: true,
                url: "/Gox/UserProfile/Ajax_UserLevel",
                data: { role: sel },
                success: function (data) {
              
                    if (data) {

                        $("#Search_Level").html(data);
                    }
                }
            });
        }
    });
});