﻿
$(function () {
    var _URL = window.URL;
    $("input[type='file'][name=Pic]").change(function () {
        var files = $("input[type='file'][name=Pic]")[0].files;
        var file;
        var img = new Image();

        if ((file = this.files[0])) {
            img.onload = function () {
                var Size = Math.round(file.size / 1024);
                if (Size > 1024) {
                    alert("大頭照請勿上傳1MB以上！")
                    $("input[type='file'][name=Pic]").val(null);
                }

            };
            img.src = _URL.createObjectURL(file);
        }
    });

    $("#CityId").change(function () {
        $.ajax({
            type: "post",
            traditional: true,
            async: true,
            url: "/Home/Ajax_Area",
            data: { city: $(this).val(), zip: false, all: false },
            //dataType: "json",
            //jsonp: "callback",
            //jsonpCallback: "dool",
            success: function (data) {
                if (data) {
                    $("#AreaId").html(data);
                }
            },
            error: function (xhr, status, error) {
                alert(xhr.responseText);
            }
        });
    });
});

//新增按下送出
function Send_Create() {

    //驗證通過
    if ($("#Form").valid()) {
        $('body').css('overflow', 'hidden')
        $.isLoading({ text: "處理中" });
        // Setup Loading plugin
        $("#load-overlay .demo p").removeClass("alert-success");
        // Re-enabling event
        setTimeout(function () {
            $.isLoading("hide");
            $("#load-overlay .demo p").html("Content Loaded")
                                        .addClass("alert-success");
        }, 500000);
        return true;
    }
    else {
        $('html, body').scrollTop(0);
        return false
    };
}

//編輯按下送出
function Send_Edit() {

    $('body').css('overflow', 'hidden')
    $.isLoading({ text: "處理中" });
    // Setup Loading plugin
    $("#load-overlay .demo p").removeClass("alert-success");
    // Re-enabling event
    setTimeout(function () {
        $.isLoading("hide");
        $("#load-overlay .demo p").html("Content Loaded")
                                    .addClass("alert-success");
    }, 500000);
    return true;
}