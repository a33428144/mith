﻿
$(function () {
    $('#file_upload').uploadify({
        //'swf': "~/Scripts/plugin/Uploadify/uploadify.swf?var=" + (new DateTime()).Date,
        'swf': '/Scripts/plugin/Uploadify/uploadify.swf',
        'uploader': '/GoX/WearPic/MiupleUpload',
        'queueSizeLimit': 999,
        'multi': true,
        'auto': false,
        //'fileTypeExts': '*.png',
        //'buttonText': '選擇檔案',
        'removeCompleted': false,
        'buttonImage': "/Scripts/plugin/Uploadify/browse-btn.png",
        'onSelect': function (file) {

            //$(".fileName").each(function (index) {
            var name = file.name.split(".")[0];
            var extension = file.name.split(".")[1];
            //var file = $(this).text().split(".");
            //var name = file[0];
            //var extension = file[1].split(" (")[0];
            var Id = "#Check_" + file.id;

            if (extension == "png" || extension == "PNG") {
                if (name.length != 12) {
                    $(Id).text(file.name + "，長度不為12碼！");
                    $(Id).css("color", "red");
                    $("#save_sort").css("display", "none");
                }
                else {
                    var regex;

                    //檢查最後一碼
                    regex = new RegExp(/^[12]+$/);
                    if (regex.test(name.substr(11, 1)) == false) {
                        $(Id).text(file.name + "，最後一碼有錯誤！");
                        $(Id).css("color", "red");
                        $('#CheckFiles').val("False");
                        $("#save_sort").css("display", "none");
                    }
                    //檢查11碼
                    regex = new RegExp(/^[NSBPM]+$/);
                    if (regex.test(name.substr(10, 1)) == false) {
                        $(Id).text(file.name + "，第11碼有錯誤！");
                        $(Id).css("color", "red");
                        $('#CheckFiles').val("False");
                        $("#save_sort").css("display", "none");
                    }

                    //檢查前10碼
                    regex = new RegExp(/^[0-9]+$/);
                    if (regex.test(name.substr(0, 10)) == false) {
                        $(Id).text(file.name + "，第10碼有錯誤！");
                        $(Id).css("color", "red");
                        $('#CheckFiles').val("False");
                        $("#save_sort").css("display", "none");
                    }

                    if ($('#CheckFiles').val() == "True") {
                        $(Id).remove();
                    }
                    ShowSendButton();
                }
            }
            else {
                $("#save_sort").css("display", "none");
                $(Id).text(file.name + "，副檔名不為 .png");
                $(Id).css("color", "red");
            }
            if ($(Id).text() == "") {
                $(Id).remove();
            }
        },

        'onUploadComplete': function (file, data, response) {
            $('#file_upload').uploadify('upload');
        },
        'onQueueComplete': function (queueData) {
            //alert(queueData.uploadsSuccessful);
        }
    });
});

//按下上傳
function Upload() {
    if ($(".uploadify-queue-item").length > 0) {
        $('#file_upload').uploadify('upload');
    }
    else {
        alert("請選擇檔案！");
    }
}

//取消上傳
function Cancel(Id) {
    $(Id).remove();
    ShowSendButton();
}

//判斷是否顯示上傳按鈕
function ShowSendButton() {

    if ($(".uploadify-Check").length > 0) {
        $('#CheckFiles').val("False");
    }
    else {
        $('#CheckFiles').val("True");
    }
    var result = $('#CheckFiles').val();
    if (result == "True") {
        $("#save_sort").css("display", "block");
    }
    else {
        $("#save_sort").css("display", "none");
    }
}
