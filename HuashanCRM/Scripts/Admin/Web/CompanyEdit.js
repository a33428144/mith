﻿$(function () {

    $("#CityId").change(function () {

        $.ajax({
            type: "post",
            traditional: true,
            async: true,
            url: "/Home/Ajax_Area",
            data: { city: $(this).val(), all: false },
            //dataType: "json",
            //jsonp: "callback",
            //jsonpCallback: "dool",
            success: function (data) {
                if (data) {
                    $("#AreaId").html(data);
                }
            },
            error: function (xhr, status, error) {
                alert(xhr.responseText);
            }
        });
    });

    $('input:radio[name=Type]').change(function () {       
        var SN = this.value + $("#SN").val().substr(1, 3);
        $.ajax({
            type: "post",
            traditional: true,
            async: true,
            url: "/GoX/Company/Check_SN",
            data: { SN: SN },
            success: function (data) {
                if (data == "Reply" && SN != $("#SN").val()) {
                    alert("更動類別後，廠商編號為【" + SN + "】，此編號重複")
                    $("#save_sort").css("display", "none");
                }
                else {
                    $("#save_sort").css("display", "block");
                }
            }
        });


    });
});

//Step1 按下下一步
function Step1_Send() {

    //驗證通過
    if ($("#Form").valid()) {
        $('body').css('overflow', 'hidden')
        $.isLoading({ text: "處理中" });
        // Setup Loading plugin
        $("#load-overlay .demo p").removeClass("alert-success");
        // Re-enabling event
        setTimeout(function () {
            $.isLoading("hide");
            $("#load-overlay .demo p").html("Content Loaded")
                                        .addClass("alert-success");
        }, 500000);
        return true;
    }
    else {
        $('html, body').scrollTop(0);
        return false
    };
}

//Step2 按下下一步
function Step2_Send() {

    var chk_Company = "";
    var checkedList = $('input[name="GoodsKindList"]:checked');
    if (checkedList.length == 0) {
        alert("【商品種類】需至少勾選一項！")
        return false;
    }
    checkedList.each(function () {
        if (chk_Company.match($(this).val()) == null) {
            chk_Company += $(this).val() + ",";
        }
    });
    //去除最後一個逗號
    var lastIndex = chk_Company.lastIndexOf(',');
    if (lastIndex > -1) {
        chk_Company = chk_Company.substring(0, chk_Company.length - 1);    }

    $("#GoodsKind").val(chk_Company);

    $('body').css('overflow', 'hidden')
    $.isLoading({ text: "處理中" });
    // Setup Loading plugin
    $("#load-overlay .demo p").removeClass("alert-success");
    // Re-enabling event
    setTimeout(function () {
        $.isLoading("hide");
        $("#load-overlay .demo p").html("Content Loaded")
                                    .addClass("alert-success");
    }, 500000);
    return true;
}

//Step3 按下送出
function Step3_Send() {

    $('body').css('overflow', 'hidden')
    $.isLoading({ text: "處理中" });
    // Setup Loading plugin
    $("#load-overlay .demo p").removeClass("alert-success");
    // Re-enabling event
    setTimeout(function () {
        $.isLoading("hide");
        $("#load-overlay .demo p").html("Content Loaded")
                                    .addClass("alert-success");
    }, 500000);
    return true;
}

