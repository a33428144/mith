﻿


$(window).load(function () {

    //Step1頁才執行
    if (window.location.href.match('Step1') != null) {

        //廠商不是集單類型，類別移除"集單"的選項
        if ($("#IsLimitOrder").val() == "False") {
            $('#Type option[value=3]').remove();
            $("#ChangeText1").text("庫存量");
            $("#ChangeText2").text("庫存量");
        }

        if ($('#BrandId option').length == 0) {
            alert("需至少設定一個品牌，請洽亞設人員！")
            location.href = "/GoX/Goods/Index_Company"
        }

        //商品序號打字
        $("#SN").keydown(function () {
            $("#Check_SN").val("");
            $("#SN").css("background-color", "#fff");
            $("#SN_Valid").css("display", "none");

            $("#Link1").attr("href", "#");
            $("#Pic1").attr("src", "");
            $("#Pic1").css("display", "none");

            $("#Link2").attr("href", "#");
            $("#Pic2").attr("src", "");
            $("#Pic2").css("display", "none");

            $("#Link3").attr("href", "#");
            $("#Pic3").attr("src", "");
            $("#Pic3").css("display", "none");
        });

        $("#BrandId").combobox({
            select: function (event, ui) {
                var sel = this.value;
                if (sel == "0") {
                    $("#BrandId_Valid").css("display", "block");
                }
                else {
                    $("#BrandId_Valid").css("display", "none");
                }
            }
        });

        $("#Type").combobox({
            select: function (event, ui) {
                var sel = this.value;
                Type_SelectValue(sel);
            }
        });

        $('input:radio[name=SizeType]').change(function () {
            if (this.value == 'Many') {
                $("#ManySize").css("display", "block");
                $("#OneSize").css("display", "none");
            }
            else {
                $("#ManySize").css("display", "none");
                $("#OneSize").css("display", "block");
            }
        });
    }
    else {
        $("#dialog2").dialog({
            autoOpen: false,
            height: 200,
            width: 420,
            close: function () {
                $('html, body').css('overflow', 'auto');
                $("#overlay").css("display", "none");
            }
        });
        var _URL = window.URL;
        $("input[type='file'][name=Files]").change(function () {
            var files = $("input[type='file'][name=Files]")[0].files;
            var file;
            var img = new Image();
            if (files.length > 6) {
                alert("商品照片最多上傳6張！");
                $("input[type='file'][name=Files]").val(null);
            }
            else {
                for (var i = 0; i < files.length; i++) {
                    if ((file = this.files[i])) {
                        img.onload = function () {
                            if (this.width > 1080 || this.height > 800) {
                                alert("有照片尺寸不符規定！");
                                $("input[type='file'][name=Files]").val(null);
                            }
                        };
                        img.src = _URL.createObjectURL(file);
                    }
                }
            }
        });
    }
});

function OpenDialog_QA() {

    $('html, body').css('overflow', 'hidden');
    $("#dialog2").dialog("open");
    $("#overlay").css("display", "block");
}

//更動類別
function Type_SelectValue(sel) {

    // 類別 = 現貨
    if (sel == 1) {
        $("#LimitOrderGroup").css("display", "none");
        $("#FewDaysGroup").css("display", "none");
        $("#SizeGroup").css("display", "block");
        $("#Type_Valid").css("display", "none");
        $("#FewDaysShipping").val(null);
        $("#LimitOrderStart").val(null);
        $("#LimitOrderEnd").val(null);
    }
        // 類別 = 預訂(製)貨
    else if (sel == 2) {
        $("#LimitOrderGroup").css("display", "none");
        $("#FewDaysGroup").css("display", "block");
        $("#SizeGroup").css("display", "block");
        $("#Type_Valid").css("display", "none");
        $("#LimitOrderStart").val(null);
        $("#LimitOrderEnd").val(null);
    }
        // 類別 = 訂製款-集量生產
    else if (sel == 3) {
        $("#LimitOrderGroup").css("display", "block");
        $("#FewDaysGroup").css("display", "block");
        $("#SizeGroup").css("display", "block");
        $("#Type_Valid").css("display", "none");
    }
        // 類別 = 特別訂購款
    else if (sel == 4) {
        $("#LimitOrderGroup").css("display", "none");
        $("#FewDaysGroup").css("display", "none");
        $("#SizeGroup").css("display", "none");
        $("#Type_Valid").css("display", "none");
        $("#FewDaysShipping").val(null);
        $("#LimitOrderStart").val(null);
        $("#LimitOrderEnd").val(null);
    }
    else {
        $("#LimitOrderGroup").css("display", "none");
        $("#FewDaysGroup").css("display", "none");
        $("#SizeGroup").css("display", "block");
        $("#Type_Valid").css("display", "inline");
        $("#FewDaysShipping").val(null);
        $("#LimitOrderStart").val(null);
        $("#LimitOrderEnd").val(null);
    }
}

//確認商品編號
function Check_SN_Click() {
    var SN = $("#SN").val().replace(" ", "");
    $("#SN").val(SN);
    $.ajax({
        type: "post",
        traditional: true,
        async: true,
        url: "/GoX/Goods/Check_SN",
        data: { SN: SN },
        dataType: "json",
        success: function (data) {
            var length = data.PicList.length;
            var count = data.Count;
            var sum = length - count;
            $("#Check_SN").val(length);
            if (length > 0 && count==0) {
                $("#SN").css("background-color", "#d5ffd5!important");
                $("#SN_Valid").css("display", "none");
                $.each(data, function (index, item) {
                    $.each(item, function (index, item) {
                        if (item.Pic1_Big != null && item.Pic1_Big != "") {
                            $("#Link1").attr("href", item.Pic1_Big);
                            $("#Pic1").attr("src", item.Pic1_Big)
                            $("#Pic1").css("display", "inline")
                        }
                    });
                });
            }
            else if (length > 0 && count > 0) {
                $("#SN").css("background-color", "#ffe5e5!important");
                $("#SN_Valid").css("display", "inline");
                $("#SN_Valid").text("商品編號已被使用");

                $("#Link1").attr("href", "#");
                $("#Pic1").attr("src", "");
                $("#Pic1").css("display", "none");
            }
            else if (length == 0 && count == 0) {
                $("#SN").css("background-color", "#ffe5e5!important");
                $("#SN_Valid").css("display", "inline");
                $("#SN_Valid").text("商品編號錯誤");

                $("#Link1").attr("href", "#");
                $("#Pic1").attr("src", "");
                $("#Pic1").css("display", "none");
            }
        }
    });
}

//Step1按下送出
function Step1_Send() {
    var SN = $("#SN").val();
    var Check_SN = $("#Check_SN").val();
    var BrandId = $("#BrandId").val();
    var Type = $("#Type").val();
    var Price = $("#Price").val();
    var SpecialOffer = $("#SpecialOffer").val();
    var LimitOrderStart = new Date($('#LimitOrderStart').val());
    var LimitOrderEnd = new Date($('#LimitOrderEnd').val());

    if (SN != "" && Check_SN == "") {
        $("#SN_Valid").css("display", "inline");
        $("#SN").css("background-color", "#ffe5e5")
        $("#SN_Valid").text("驗證商品序號");
        $('html, body').scrollTop(0);
    }
    if (BrandId == "0") {
        $("#BrandId_Valid").css("display", "block");
        $('html, body').scrollTop(0);
    }

    if (Type == "0") {
        $("#Type_Valid").css("display", "inline");
        $('html, body').scrollTop(0);
    }

    //驗證
    if ($("#Form").valid() && Check_SN != "" && Check_SN != 0 && BrandId != "0" && Type != "0") {

        if (LimitOrderStart.getTime() > LimitOrderEnd.getTime()) {
            alert("【集單日期】的開始日期不能大於結束日期！");
            $('html, body').scrollTop(0);
            return false;
        }

        if (SpecialOffer != "") {
            if (parseInt(SpecialOffer) >= parseInt(Price)) {
                alert("特價不能大於 (等於) 售價！");
                $('html, body').scrollTop(0);
                return false;
            }
        }

        return true;
    }
    else {
        if (SN == null || SN == "") {
            $('html, body').scrollTop(0);
        }
        return false
    };
}


//Step2 按下送出
function Step2_Send() {

    //---------------------------風格---------------------------
    var chk_Style = "";
    var checkedList_Style = $('input[name="StyleList"]:checked');
    if (checkedList_Style.length == 0) {
        alert("【風格】需至少勾選一項！")
        return false;
    }
    else if (checkedList_Style.length > 2) {
        alert("【風格】最多勾選兩項！")
        return false;
    }
    checkedList_Style.each(function () {
        if (chk_Style.match($(this).val()) == null) {
            chk_Style += $(this).val() + ",";
        }
    });
    //去除最後一個逗號
    var lastIndex = chk_Style.lastIndexOf(',');
    if (lastIndex > -1) {
        chk_Style = chk_Style.substring(0, chk_Style.length - 1);
    }
    $("#Style").val(chk_Style);


    //---------------------------適合---------------------------
    var chk_Fit = "";
    var checkedList_Fit = $('input[name="FitList"]:checked');
    if (checkedList_Fit.length == 0) {
        alert("【適合】需至少勾選一項！")
        return false;
    }
    else if (checkedList_Fit.length > 2) {
        alert("【適合】最多勾選兩項！")
        return false;
    }
    checkedList_Fit.each(function () {
        if (chk_Fit.match($(this).val()) == null) {
            chk_Fit += $(this).val() + ",";
        }
    });
    //去除最後一個逗號
    var lastIndex2 = chk_Fit.lastIndexOf(',');
    if (lastIndex2 > -1) {
        chk_Fit = chk_Fit.substring(0, chk_Fit.length - 1);
    }
    $("#Fit").val(chk_Fit);


    $('body').css('overflow', 'hidden')
    $.isLoading({ text: "處理中" });
    // Setup Loading plugin
    $("#load-overlay .demo p").removeClass("alert-success");
    // Re-enabling event
    setTimeout(function () {
        $.isLoading("hide");
        $("#load-overlay .demo p").html("Content Loaded")
                                    .addClass("alert-success");
    }, 500000);
    return true;
}

