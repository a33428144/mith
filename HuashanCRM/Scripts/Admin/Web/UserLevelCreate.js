﻿


$(window).load(function () {
    if ($("#CompanyId").val() > 0) {
        $("#Company_Link").click();
        $(".table").css("display", "grid");
    }
    $('#Level_Role option[value=3]').remove();

    //更動角色
    $("#Level_Role").combobox({
        select: function (event, ui) {
            $("#Level_Role_Valid").css("display", "none");
            var sel = this.value;
            if (sel == 1) {
                $("#MithFunction").css("display", "table");
                $("#CompanyFunction").css("display", "none");
                //$("#Mith_Link").click();
                //$(".table").css("display", "grid");
            }
            else if (sel == 2) {
                $("#MithFunction").css("display", "none");
                $("#CompanyFunction").css("display", "table");
            }
            else {
                $("#MithFunction").css("display", "none");
                $("#CompanyFunction").css("display", "none");
            }
        }
    });

    checkfun("#read", ".chk_read");
    checkfun("#insert", ".chk_insert");
    checkfun("#update", ".chk_update");
    checkfun("#delete", ".chk_delete");

    checkfun("#read2", ".chk2_read");
    checkfun("#insert2", ".chk2_insert");
    checkfun("#update2", ".chk2_update");
    checkfun("#delete2", ".chk2_delete");

    $(".fun_check").click(function () {
        var tmp = $(this);
        $(".chk_fun" + $(this).val()).each(function () {
            if ($(tmp).prop("checked"))
                $(this).prop("checked", $(tmp).prop("checked"));
            else
                $(this).prop("checked", false);
        });
    });

    $(".fun_check2").click(function () {
        var tmp = $(this);
        $(".chk2_fun" + $(this).val()).each(function () {
            if ($(tmp).prop("checked"))
                $(this).prop("checked", $(tmp).prop("checked"));
            else
                $(this).prop("checked", false);
        });
    });
});

function checkfun(id, cls) {
    $(id).click(function () {
        $(cls).each(function () {
            if ($(id).prop("checked"))
                $(this).prop("checked", $(id).prop("checked"));
            else
                $(this).prop("checked", false);
        });
    });
}

//按下送出
function Send() {
    var Level_Role = $("#Level_Role").val();
    if (Level_Role == "0") {
        $("#Level_Role_Valid").css("display","inline");
    }
    //驗證
    if ($("#Form").valid() && Level_Role != "0") {

        //Show Loading
        $('body').css('overflow', 'hidden')
        $.isLoading({ text: "處理中" });
        $("#load-overlay .demo p").removeClass("alert-success");
        setTimeout(function () {
            $.isLoading("hide");
            $("#load-overlay .demo p").html("Content Loaded")
                                        .addClass("alert-success");
        }, 500000);
        return true;
    }
    else {
        $('html, body').scrollTop(0);
        return false
    };
}

