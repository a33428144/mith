﻿
$(function () {

    $("#dialog").dialog({
        autoOpen: false,
        height: 450,
        width: 650,
        close: function () {
            $('html, body').css('overflow', 'auto');
            $("#overlay").css("display", "none");
        }
    });
    $("#dialog2").dialog({
        autoOpen: false,
        height: 230,
        width: 350,
        close: function () {
            $('html, body').css('overflow', 'auto');
            $("#overlay").css("display", "none");
        }
    });

    //去7-11選擇退貨門市，從7-11導回來
    if ($("#From").val() == "711") {
        $("#overlay").css("display", "block");
        Get_CheckboxVal();

        $(".ShippingGoods_submit").click();

        setTimeout(function () {
            $('html, body').css('overflow', 'hidden');
            $("#dialog").dialog("open");
        }, 1000);
    }

    $("#CityId").change(function () {
        $.ajax({
            type: "post",
            traditional: true,
            async: true,
            url: "/Home/Ajax_Area",
            data: { city: $(this).val(), zip: false, all: false },
            //dataType: "json",
            //jsonp: "callback",
            //jsonpCallback: "dool",
            success: function (data) {
                if (data) {
                    $("#AreaId").html(data);
                }
            },
            error: function (xhr, status, error) {
                alert(xhr.responseText);
            }
        });
    });

    $("#AreaId").change(function () {
        $("#ReceiverAddress_Valid").css("display", "none")
    });

    $("#ReceiverAddress").keydown(function () {
        $("#ReceiverAddress_Valid").css("display", "none")
    });
    $("#LogisticsSN").keydown(function () {
        $("#LogisticsSN_Valid ").css("display", "none")
    });

});

function OpenDialog(GoodsAction) {
    var chk_OrderEdit = $('input[name="Id"]:checked');
    if (chk_OrderEdit.length == 0) {
        alert("未勾選任何商品！");
        return false;
    }
    else {
        //出貨
        if (GoodsAction == "ShippingGoods") {
            var NotStockUpGoods = 0; //未接單的商品
            var td_item3;
            for (var i = 0; i < chk_OrderEdit.length; i++) {
                var sort = i + 1;
                td_item3 = "#item3_" + sort;

                if ($(td_item3).text().match('未接單') != null) {
                    NotStockUpGoods++;
                }
            }
            if (NotStockUpGoods > 0) {
                alert("勾選的商品中，有商品未接單！");
                return false;
            }
        }
        //退貨
        else {
            if (chk_OrderEdit.length > 1) {
                alert("一次只能退一件商品！");
                return false;
            }
        }
    }
    setTimeout(function () {
        $('html, body').css('overflow', 'hidden');
        $("#dialog").dialog("open");
        $("#overlay").css("display", "block");
    }, 1000);
    return true;
}

function OpenDialog_QA() {

    $('html, body').css('overflow', 'hidden');
    $("#dialog2").dialog("open");
    $("#overlay").css("display", "block");
}

function SelectReturnStore() {
    var chk_OrderEdit = $('input[name="Id"]:checked');
    var chk_val = "";

    for (var i = 0; i < chk_OrderEdit.length; i++) {
        chk_val += chk_OrderEdit[i].value + ",";
    }
    var lastIndex = chk_val.lastIndexOf(',');
    if (lastIndex > -1) {
        chk_val = chk_val.substring(0, chk_val.length - 1);
    }
    $.ajax({
        type: "post",
        traditional: true,
        async: true,
        url: "/Gox/Order/Session_chk_OrderEdit",
        data: { chk_OrderEdit: chk_val },
        success: function (data) {
            form1.submit();
        },
        error: function (jqXHR, exception) {
            alert("系統發生錯誤！")
        }
    });

}

//備貨
function StockUp() {
    var chk_OrderEdit = $('input[name="Id"]:checked');
    var ShippingGoods = 0; //出貨中的商品
    var td_item3;
    var chk_OrderDetail = "";
    for (var i = 0; i < chk_OrderEdit.length; i++) {
        var sort = i + 1;
        td_item3 = "#item3_" + sort;

        if ($(td_item3).text().match('出貨') != null) {
            ShippingGoods++;
        }
        chk_OrderDetail += chk_OrderEdit[i].value + ",";
    }
    var lastIndex = chk_OrderDetail.lastIndexOf(',');
    if (lastIndex > -1) {
        chk_OrderDetail = chk_OrderDetail.substring(0, chk_OrderDetail.length - 1);
    }

    if (ShippingGoods == 0) {
        if (confirm("確定要備貨?")) {
            $('body').css('overflow', 'hidden')
            $.isLoading({ text: "處理中" });
            // Setup Loading plugin
            $("#load-overlay .demo p").removeClass("alert-success");
            // Re-enabling event
            setTimeout(function () {
                $.isLoading("hide");
                $("#load-overlay .demo p").html("Content Loaded")
                                            .addClass("alert-success");
            }, 500000);

            $.ajax({
                type: "post",
                traditional: true,
                async: true,
                url: "/Gox/Order/Update_Status",
                data: { OrderDetailId: chk_OrderDetail, Status: 1 },
                success: function (data) {
                    if (data = "Success") {
                        location.href = location.href;
                        CheckSuccess(data);
                    }
                },
                error: function (jqXHR, exception) {
                    alert("系統發生錯誤！")
                }
            });
        }

    }
    else {
        alert("勾選的商品中，有商品已出貨！");
    }
}

//出貨
function ShippingGoods() {

    var chk_OrderEdit = $('input[name="Id"]:checked');
    var ShippingVal = "";
    var td_item9;
    var Total = 0;

    for (var i = 0; i < chk_OrderEdit.length; i++) {
        td_item9 = "#item9_" + chk_OrderEdit[i].className;
        Total += parseInt($(td_item9).text().replace(",",""));
        ShippingVal += chk_OrderEdit[i].value + ",";
    }
    var lastIndex = ShippingVal.lastIndexOf(',');
    if (lastIndex > -1) {
        ShippingVal = ShippingVal.substring(0, ShippingVal.length - 1);
    }
    $("#ShippingVal").val(ShippingVal);
    $("#Total").val(Total);

    if ($("#LogisticsType_Name").text().match('黑貓') != null) {
        var LogisticsSN = $("#LogisticsSN").val();
        if (LogisticsSN == "") {
            $("#LogisticsSN_Valid").text("物流編號必填")
            $("#LogisticsSN_Valid").css("display", "block")
            return false
        }
        else {
            //驗證序號，只能輸入數字
            var regex = new RegExp(/^[0-9_]+$/);

            if (regex.test(LogisticsSN) == false) {
                $("#LogisticsSN_Valid").text("只能輸入數字")
                $("#LogisticsSN_Valid").css("display", "block")
                return false
            }
            else {
                $("#LogisticsSN_Valid").css("display", "none")
            }
        }
    }
    else {
        var ReturnStoreName = $("#ReturnStoreName").val();
        if (ReturnStoreName == "") {
            alert("請選擇退貨門市！");
            return false
        }
    }
    if (confirm("確定要出貨?")) {
        ShippingGoodsForm.submit();
        $('body').css('overflow', 'hidden')
        $.isLoading({ text: "處理中" });
        // Setup Loading plugin
        $("#load-overlay .demo p").removeClass("alert-success");
        // Re-enabling event
        setTimeout(function () {
            $.isLoading("hide");
            $("#load-overlay .demo p").html("Content Loaded")
                                        .addClass("alert-success");
        }, 500000);
        return true;
    }
    else {
        return false;
    }
}

//退貨
function ReturnGoods() {
    var j = 1;
    var ReturnGoodsVal = "";
    for (var i = 0, l = $(".BuyQuantitySelect").length; i < l; i++) {
        var Select = "#BuyQuantity_" + j
        j++;
        ReturnGoodsVal += $(Select).val() + ",";
        //去除最後一個逗號
    }
    var lastIndex = ReturnGoodsVal.lastIndexOf(',');
    if (lastIndex > -1) {
        ReturnGoodsVal = ReturnGoodsVal.substring(0, ReturnGoodsVal.length - 1);
    }
    $("#ReturnGoodsVal").val(ReturnGoodsVal);

    var CityId = $("#CityId").val();
    var AreaId = $("#AreaId").val();
    var ReceiverAddress = $("#ReceiverAddress").val();
    var LogisticsSN = $("#LogisticsSN").val();
    if (CityId == 0 || ReceiverAddress == "") {
        $("#ReceiverAddress_Valid").css("display", "block")
    }
    else {
        if (AreaId == 0) {
            $("#ReceiverAddress_Valid").css("display", "block")
        }
    }

    if (LogisticsSN == "") {
        $("#LogisticsSN_Valid").text("物流編號必填")
        $("#LogisticsSN_Valid").css("display", "block")
    }
    else {
        //驗證序號，只能輸入小寫英文、數字
        var regex = new RegExp(/^[0-9_]+$/);

        if (regex.test(LogisticsSN) == false) {
            $("#LogisticsSN_Valid").text("只能輸入數字")
            $("#LogisticsSN_Valid").css("display", "block")
            return false
        }
        else {
            $("#LogisticsSN_Valid").css("display", "none")
        }
    }

    if (AreaId != 0 && ReceiverAddress != "" && LogisticsSN != "") {
        if (confirm("確定要退貨?")) {
            Form.submit();
            return true;
        }
        else {
            return false;
        }
    }
    else {
        return false;
    }
}

//載入有勾選的值
function Get_CheckboxVal() {
    var chk_OrderEdit = $("#chk_OrderEdit").val();

    $('input[type=checkbox]').each(function () {
        if (chk_OrderEdit != "") {

            var item = chk_OrderEdit.split(',');

            for (var i = 0; i < item.length; i++) {
                if (item[i] == this.id) {
                    $("#" + this.id).prop('checked', true);
                }
            }

        }
    });
}

function CheckSuccess(status) {
    if (status == "Success") {
        $(".isloading-overlay").css("display", "none");
        alert("儲存成功！");
        $("html").css("overflow", "auto");
    }
    else {
        alert("儲存失敗！");
    }
}