﻿
$(function () {

    var Now = new Date();
    var Year = Now.getFullYear();
    jQuery('#RefundTime').datetimepicker({
        format: 'Y/m/d H:i',
        lang: 'zh-TW',
        step: 30,
        timepickerScrollbar: true,
        todayButton: false,
        yearStart: Year,
        yearEnd: Year,
    });

    var CashFlowType = $("#CashFlowType").val();
    var CashFlowStatus = $("#CashFlowStatus").val();

    //付款方式=信用卡 & 已付款
    if (CashFlowType == 2 && CashFlowStatus == 4) {
        $('#RefundTotal').attr('disabled', true); //無法修改退款金額
    }
});


//按下送出
function Send() {
    var CashFlowStatus = $("#CashFlowStatus").val();
    var CashFlowType = $("#CashFlowType").val();
    var RefundTime = $("#RefundTime").val();

    if (CashFlowType == 1 && RefundTime == null || RefundTime == "") {
        $("#RefundTime_Valid").css("display", "inline");
    }
    if ($("#Form").valid()) {
        if (CashFlowType == 1 && RefundTime == null || RefundTime == "") {
            return false;
        }
        //尚未退款，先confirm確認
        if (CashFlowStatus == 3) {
            if (confirm("確定要退款?")) {
                $('body').css('overflow', 'hidden')
                $.isLoading({ text: "處理中" });
                // Setup Loading plugin
                $("#load-overlay .demo p").removeClass("alert-success");
                // Re-enabling event
                setTimeout(function () {
                    $.isLoading("hide");
                    $("#load-overlay .demo p").html("Content Loaded")
                                                .addClass("alert-success");
                }, 500000);
                return true;
            }
            else {
                return false;
            }
        }
        //已經退款過
        else {
            $('body').css('overflow', 'hidden')
            $.isLoading({ text: "處理中" });
            // Setup Loading plugin
            $("#load-overlay .demo p").removeClass("alert-success");
            // Re-enabling event
            setTimeout(function () {
                $.isLoading("hide");
                $("#load-overlay .demo p").html("Content Loaded")
                                            .addClass("alert-success");
            }, 500000);
            return true;
        }
    }
    else {
        $('html, body').scrollTop(0);
        return false
    };


}