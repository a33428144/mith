﻿$(function () {
    $("#file_upload").click(function () {
        var file = document.getElementById("file_video").files;
        if (file.length == 1) {
            $(this).val("上傳中...");
            $(this).attr("disabled", true);
            var fd = new FormData();
            fd.append('file_video', file[0]);
            fd.append('mid', $("#video_id").val());
            fd.append('fid', $("#video_fid").val());

            $.ajaxQueue({
                url: $("#root").val() + "/GoX/Video/Upload",
                type: "POST",
                contentType: false,
                processData: false,
                cache: false,
                data: fd,
                dataType: "json",
                success: function (data) {
                    if (data.status == true) {
                        alert('上傳成功，按下確定重新整理頁面');
                        location = location;
                    }
                    else {
                        alert('上傳失敗');
                    }
                    $("#file_upload").val("上傳");
                    $("#file_upload").removeAttr("disabled");
                },
                error: function () {
                    alert('上傳失敗');
                    $("#file_upload").val("上傳");
                    $("#file_upload").removeAttr("disabled");
                }
            });
        }
        else {
            alert('請先選擇檔案');
        }
    });
});