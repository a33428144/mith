﻿
$(function () {
    //匯入Excel
    var actionUrls = {
        Import: '@Url.Action("Import", "GoodsSN_Setting")',
    };
    project.ZipCode.Initialize(actionUrls);


    //單選打勾
    $(".delete").on("change", function () {
        var chk_GoodsSN = "";
        //載入Cookie的值
        if ($.cookie('chk_GoodsSN') != null && $.cookie('chk_GoodsSN') != "") {
            chk_GoodsSN += $.cookie('chk_GoodsSN') + ",";
        }
        //把打勾取消
        if (chk_GoodsSN.match($(this).val()) != null) {
            // Id + ","
            chk_GoodsSN = chk_GoodsSN.replace($(this).val() + ",", "");
        }
        //有勾選的CheckBox
        var checkedList = $('input[name="item"]:checked');
        checkedList.each(function () {
            if (chk_GoodsSN.match($(this).val()) == null) {
                chk_GoodsSN += $(this).val() + ",";
            }
        });
        //如果第一個字是逗號，去掉
        var Index = chk_GoodsSN.indexOf(",");
        if (Index == 0) {
            chk_GoodsSN = chk_GoodsSN.substring(1);
        }

        //去除最後一個逗號
        var lastIndex = chk_GoodsSN.lastIndexOf(',');
        if (lastIndex > -1) {
            chk_GoodsSN = chk_GoodsSN.substring(0, chk_GoodsSN.length - 1);
        }

        //將值塞到Cookie
        var date = new Date();
        //Cookie預設十分鐘
        date.setTime(date.getTime() + (600 * 1000));
        $.cookie('chk_GoodsSN', chk_GoodsSN, { expires: date, path: '/' })
    });

    $.cookie.json = true;
    //載入有勾選的值
    Get_CheckboxVal();
});

function Search_Mith() {
    document.SearchForm.action = "/GoX/GoodsSN_Setting/Index_Mith";
    document.SearchForm.method = "get";
    SearchForm.submit();
};

function Search_Company() {
    document.SearchForm.action = "/GoX/GoodsSN_Setting/Index_Company";
    document.SearchForm.method = "get";
    SearchForm.submit();
};

function PrintBarcode() {

    $('#barcodeTarget').html("");

    var chk_GoodsSN = "";
    if (typeof $.cookie('chk_GoodsSN') != 'undefined') {
        chk_GoodsSN = $.cookie('chk_GoodsSN');
    }
    var chk = chk_GoodsSN.split(",");

    for (var i = 0; i < chk.length; i++) {
        //chk[i] = chk[i].substr(2, 13);
      
        if (i == 0) {
            $('#barcodeTarget').append('<div class="Barcode' + i + '" ></div>');
        }
        else {
            $('#barcodeTarget').append('<div class="Barcode' + i + '"  style="margin-top:260px"></div>');
        }
        //$('#barcodeTarget').append('<div style="font-size: 16px;margin-top:2px;margin-left:10px">' + chk[i] + '</div><br>');
    
        $('.Barcode' + i).barcode(chk[i], "code128", { barWidth: 1, barHeight: 50, fontSize: 16, addQuietZone: false, moduleSize: 1, output: "css" });

    }

    //if (checkedList.length == 0) {
    //    alert("請勾選要列印的商品編號！");
    //}

    //checkedList.each(function () {

    setTimeout("GetPrintHtml()", 1000);


}

function GetPrintHtml() {
    //var divContents = $("#barcodeTarget").html();
    var divContents = $("#barcodeTarget").html();
    var printWindow = window.open('', '', 'height=600,width=800');
    printWindow.document.write('<html>');
    printWindow.document.write('<heard><title></title></heard>');
    printWindow.document.write('<body style="margin:8px">');
    printWindow.document.write(divContents);
    printWindow.document.write('</body>');
    printWindow.document.write('</html>');
    printWindow.document.close();
    printWindow.print();
}

//全選打勾
function AllCheck() {
    var chk_GoodsSN = "";

    //載入Cookie的值
    if ($.cookie('chk_GoodsSN') != null && $.cookie('chk_GoodsSN') != "") {
        chk_GoodsSN += $.cookie('chk_GoodsSN') + ",";
    }

    $(".delete").each(function () {
        if ($("#AllCheck").prop("checked")) {
            $(this).prop("checked", $("#AllCheck").prop("checked"));
            if (chk_GoodsSN.match($(this).val()) == null) {
                chk_GoodsSN += $(this).val() + ",";
            }

        }
        else {
            $(this).prop("checked", false);
            //把打勾取消
            if (chk_GoodsSN.match($(this).val()) != null) {
                // Id + ","
                chk_GoodsSN = chk_GoodsSN.replace($(this).val() + ",", "");
            }
        }
    });

    //如果第一個字是逗號，去掉
    var Index = chk_GoodsSN.indexOf(",");
    if (Index == 0) {
        chk_GoodsSN = chk_GoodsSN.substring(1);
    }

    //去除最後一個逗號
    var lastIndex = chk_GoodsSN.lastIndexOf(',');
    if (lastIndex > -1) {
        chk_GoodsSN = chk_GoodsSN.substring(0, chk_GoodsSN.length - 1);
    }

    //將值塞到Cookie
    var date = new Date();
    //Cookie預設十分鐘
    date.setTime(date.getTime() + (600 * 1000));
    $.cookie('chk_GoodsSN', chk_GoodsSN, { expires: date, path: '/' })
};

//載入有勾選的值
function Get_CheckboxVal() {

    var chk_GoodsSN = "";

    if (typeof $.cookie('chk_GoodsSN') != 'undefined') {
        chk_GoodsSN = $.cookie('chk_GoodsSN');
    }
    $(".delete").each(function () {
        if (chk_GoodsSN != "") {
            if (chk_GoodsSN.match(this.id) != null) {
                $("#" + this.id).prop('checked', true);
            }
        }
    });
}

function DownloadFile() {
    var chk_GoodsSN;
    if (typeof $.cookie('chk_GoodsSN') != 'undefined') {
        chk_GoodsSN = $.cookie('chk_GoodsSN');
    }
    if (chk_GoodsSN != null) {
        location.href = "/Gox/GoodsSN_Setting/Download?SN=" + chk_GoodsSN;
        //將值塞到Cookie
        var date = new Date();
        //Cookie預設5秒
        date.setTime(date.getTime() + (5 * 1000));
        $.cookie('chk_GoodsSN', null, { expires: date, path: '/' });

    }
    else {
        alert("未勾選任何商品！");
    }
}

function Ajax_Delete() {
    var chk_GoodsSN = "";
    if (typeof $.cookie('chk_GoodsSN') != 'undefined') {
        chk_GoodsSN = $.cookie('chk_GoodsSN');
    }
    if (chk_GoodsSN != null) {
        if (confirm("確定要刪除?")) {
            $('body').css('overflow', 'hidden')
            $.isLoading({ text: "處理中" });
            // Setup Loading plugin
            $("#load-overlay .demo p").removeClass("alert-success");
            // Re-enabling event
            setTimeout(function () {
                $.isLoading("hide");
                $("#load-overlay .demo p").html("Content Loaded")
                                            .addClass("alert-success");
            }, 500000);

            $.ajax({
                type: "post",
                traditional: true,
                async: true,
                url: "/Gox/GoodsSN_Setting/Delete_Ajax",
                data: { SN: chk_GoodsSN},
                success: function (data) {
                    if (data = "Success") {
                        //將值塞到Cookie
                        var date = new Date();
                        //Cookie預設5秒
                        date.setTime(date.getTime() + (5 * 1000));
                        $.cookie('chk_GoodsSN', null, { expires: date, path: '/' });
                        location.href = location.href;
                        CheckSuccess(data);
                    }
                },
                error: function (jqXHR, exception) {
                    alert("系統發生錯誤！")
                }
            });
        }
      }
    else {
        alert("未勾選任何項目！");
        return false;
    }
}

function CheckSuccess(status) {
    if (status == "Success") {
        $(".isloading-overlay").css("display", "none");
        alert("儲存成功！");
        $("html").css("overflow", "auto");
    }
    else {
        alert("儲存失敗！");
    }
}