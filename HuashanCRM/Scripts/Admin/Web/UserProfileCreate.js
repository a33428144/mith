﻿
$(function () {
    //更動角色
    $("#Role").combobox({
        select: function (event, ui) {
            $("#Role_Valid").css("display", "none");
            var sel = this.value;
            $.ajax({
                type: "post",
                traditional: true,
                async: true,
                url: "/Gox/UserProfile/Ajax_UserLevel",
                data: { role: sel },
                success: function (data) {
                    if (data) {
                        $("#LevelId").html(data);
                    }
                }
            });
        
            if (sel == 0 || sel == 1) {
                $("#CompanyGroup").hide();
                $("#ExpertGroup").hide();
                $("#LevelGroup").show();
            }
            else if (sel == 2) {
                $("#CompanyGroup").show();
                $("#ExpertGroup").hide();
                $("#LevelGroup").show();
            }
            else if (sel == 3) {
                $("#CompanyGroup").hide();
                $("#ExpertGroup").show();
                $("#LevelGroup").hide();
            }
            else {
            }
        }
    });

    $("#CompanyId").combobox({
        select: function (event, ui) {

            var sel = this.value;
            if (sel == 0) {
                $("#CompanyId_Valid").css("display", "block");
            }
            else {
                $("#CompanyId_Valid").css("display", "none");
            }
        }
    });

    $("#ExpertId").combobox({
        select: function (event, ui) {

            var sel = this.value;
            if (sel == 0) {
                $("#ExpertId_Valid").css("display", "block");
            }
            else {
                $("#ExpertId_Valid").css("display", "none");
            }
        }
    });

    $("#LevelId").combobox({
        select: function (event, ui) {

            var sel = this.value;
            if (sel == 0) {
                $("#LevelId_Valid").css("display", "inline");
            }
            else {
                $("#LevelId_Valid").css("display", "none");
            }
        }
    });

});

//按下送出
function Send_Mith() {
    var Role = $("#Role").val();
    var CompanyId = $("#CompanyId").val();
    var ExpertId = $("#ExpertId").val();
    var LevelId = $("#LevelId").val();

    if (Role == "0") {
        $("#Role_Valid").css("display", "block");
    }
        //角色：亞設人員
    else if (Role == "1") {

    }
        //角色：廠商，沒選擇廠商
    else if (Role == "2" && CompanyId == "0") {
        $("#CompanyId_Valid").css("display", "block");
    }
        //角色：達人，沒選擇達人
    else if (Role == "3" && ExpertId == "0") {
        $("#ExpertId_Valid").css("display", "block");
    }

    //權限
    if (LevelId == "0") {
        $("#LevelId_Valid").css("display", "inline");
    }



    //驗證通過
    if ($("#Form").valid() && Role != "0") {

        //角色：廠商，沒選擇廠商
        if (Role == "2" && CompanyId == "0") {
            return false;
        }
            //角色：達人，沒選擇達人
        else if (Role == "3" && ExpertId == "0") {
            return false;
        }

        $('body').css('overflow', 'hidden')
        $.isLoading({ text: "處理中" });
        // Setup Loading plugin
        $("#load-overlay .demo p").removeClass("alert-success");
        // Re-enabling event
        setTimeout(function () {
            $.isLoading("hide");
            $("#load-overlay .demo p").html("Content Loaded")
                                        .addClass("alert-success");
        }, 500000);
        return true;
    }
    else {

        return false
    };
}

function Send_Company() {

    var LevelId = $("#LevelId").val();

    //權限
    if (LevelId == "0") {
        $("#LevelId_Valid").css("display", "inline");
    }

    //驗證通過
    if ($("#Form").valid() && LevelId != "0") {

        $('body').css('overflow', 'hidden')
        $.isLoading({ text: "處理中" });
        // Setup Loading plugin
        $("#load-overlay .demo p").removeClass("alert-success");
        // Re-enabling event
        setTimeout(function () {
            $.isLoading("hide");
            $("#load-overlay .demo p").html("Content Loaded")
                                        .addClass("alert-success");
        }, 500000);
        return true;
    }
    else {

        return false
    };
}