﻿
$(function () {

    //單選打勾
    $(".delete").on("change", function () {
        var chk_ShippingGoods = "";
        //載入Cookie的值
        if ($.cookie('chk_ShippingGoods') != null && $.cookie('chk_ShippingGoods') != "") {
            chk_ShippingGoods += $.cookie('chk_ShippingGoods') + ",";
        }
        //把打勾取消
        if (chk_ShippingGoods.match($(this).val()) != null) {
            // Id + ","
            chk_ShippingGoods = chk_ShippingGoods.replace($(this).val() + ",", "");
        }
        //有勾選的CheckBox
        var checkedList = $('input[name="item"]:checked');
        checkedList.each(function () {
            if (chk_ShippingGoods.match($(this).val()) == null) {
                chk_ShippingGoods += $(this).val() + ",";
            }
        });
        //如果第一個字是逗號，去掉
        var Index = chk_ShippingGoods.indexOf(",");
        if (Index == 0) {
            chk_ShippingGoods = chk_ShippingGoods.substring(1);
        }

        //去除最後一個逗號
        var lastIndex = chk_ShippingGoods.lastIndexOf(',');
        if (lastIndex > -1) {
            chk_ShippingGoods = chk_ShippingGoods.substring(0, chk_ShippingGoods.length - 1);
        }

        //將值塞到Cookie
        var date = new Date();
        //Cookie預設十分鐘
        date.setTime(date.getTime() + (600 * 1000));
        $.cookie('chk_ShippingGoods', chk_ShippingGoods, { expires: date, path: '/' })
    });

    $.cookie.json = true;
    //載入有勾選的值
    Get_CheckboxVal();
});

//全選打勾
function AllCheck() {
    var chk_ShippingGoods = "";
    //載入Cookie的值
    if ($.cookie('chk_ShippingGoods') != null && $.cookie('chk_ShippingGoods') != "") {
        chk_ShippingGoods += $.cookie('chk_ShippingGoods') + ",";
    }

    $(".delete").each(function () {
        if ($("#AllCheck").prop("checked")) {
            $(this).prop("checked", $("#AllCheck").prop("checked"));
            if (chk_ShippingGoods.match($(this).val()) == null) {
                chk_ShippingGoods += $(this).val() + ",";
            }

        }
        else {
            $(this).prop("checked", false);
            //把打勾取消
            if (chk_ShippingGoods.match($(this).val()) != null) {
                // Id + ","
                chk_ShippingGoods = chk_ShippingGoods.replace($(this).val() + ",", "");
            }
        }
    });

    //如果第一個字是逗號，去掉
    var Index = chk_ShippingGoods.indexOf(",");
    if (Index == 0) {
        chk_ShippingGoods = chk_ShippingGoods.substring(1);
    }

    //去除最後一個逗號
    var lastIndex = chk_ShippingGoods.lastIndexOf(',');
    if (lastIndex > -1) {
        chk_ShippingGoods = chk_ShippingGoods.substring(0, chk_ShippingGoods.length - 1);
    }

    //將值塞到Cookie
    var date = new Date();
    //Cookie預設十分鐘
    date.setTime(date.getTime() + (600 * 1000));
    $.cookie('chk_ShippingGoods', chk_ShippingGoods, { expires: date, path: '/' })

};

//載入有勾選的值
function Get_CheckboxVal() {

    var chk_ShippingGoods = "";

    if (typeof $.cookie('chk_ShippingGoods') != 'undefined') {
        chk_ShippingGoods = $.cookie('chk_ShippingGoods');
    }
    $(".delete").each(function () {
        if (chk_ShippingGoods != null && chk_ShippingGoods != "") {
            if (chk_ShippingGoods.match(this.id) != null) {
                $("#" + this.id).prop('checked', true);
            }
        }
    });
}

function Print_711() {
    form1.submit();
};

function Print_BlackCate() {
    var divContents = $("#PrintTarget").html();
    var printWindow = window.open('', '', 'height=400,width=800');
    printWindow.document.write('<html><head><title></title>');
    printWindow.document.write('<style>');
    printWindow.document.write(' th{padding:5px;font-weight:bold} td{padding:5px; line-height: 25px;}');
    printWindow.document.write('</style>');    

    printWindow.document.write('</style>');
    printWindow.document.write('</head><body style="color:black!important">');
    //printWindow.document.write('<link rel=stylesheet type="text/css" href="/Scripts/plugin/bootstrap/css/bootstrap.css">');
    //printWindow.document.write('<link rel=stylesheet type="text/css" href="/Content/Admin/Web/OrderEdit.css">');
    

    printWindow.document.write(divContents);
    printWindow.document.write('</body></html>');
    printWindow.document.close();
    printWindow.print();

}
