﻿
//按下送出
function Send() {
    var title = $("#Title").val();
    var Content = $("#Content").val();
    if (title == "" || title == null || Content == "" || Content ==null) {
        return true;
    }
    if (title != "" && Content !="") {
        $('body').css('overflow', 'hidden')
        $.isLoading({ text: "Loading" });
        $("#load-overlay .demo p").removeClass("alert-success");
        setTimeout(function () {
            $.isLoading("hide");
            $("#load-overlay .demo p").html("Content Loaded")
                                        .addClass("alert-success");
        }, 500000);
        return true;
    }
}
