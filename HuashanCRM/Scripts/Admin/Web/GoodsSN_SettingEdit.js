﻿$(function () {
    var sn = $("#SN").val();
    var _URL = window.URL;

    $("#Pic1_Big").change(function () {

        $('#CheckFiles').val("True");
        $('#ShowPic1_Big').html("");
        var files = $('#Pic1_Big')[0].files;

        for (var i = 0; i < files.length; i++) {
            var j = i + 1;

            var file = files[i].name.split(".");
            var name = file[0];
            var type = name.substr(15, 1);
            var extension = file[1];

            $("#ShowPic1_Big").append("<span style='color:red' id='a" + j + "'></span><br>");
            if (extension == "png" || extension == "PNG") {
                //檢查商品序號
                if (name.substr(0, 15) != sn) {
                    $("#a" + j).text("檔案名稱錯誤，應為" + sn+"1");
                    $("#a" + j).css("color", "red");
                    $('#CheckFiles').val("False");
                    $("#save_sort").css("display", "none");
                }
                ShowSendButton();
            }
            else {
                $("#save_sort").css("display", "none");
                $("#a" + j).text("副檔名不為 .png");
                $("#a" + j).css("color", "red");
            }
        }
    });

    $("#Pic2_Big").change(function () {

        $('#CheckFiles').val("True");
        $('#ShowPic2_Big').html("");
        var files = $('#Pic2_Big')[0].files;

        for (var i = 0; i < files.length; i++) {
            var j = i + 1;

            var file = files[i].name.split(".");
            var name = file[0];
            var type = name.substr(15, 1);
            var extension = file[1];

            $("#ShowPic2_Big").append("<span style='color:red' id='b" + j + "'></span><br>");
            if (extension == "png" || extension == "PNG") {
                //檢查商品序號
                if (name.substr(0, 15) != sn) {
                    $("#b" + j).text("檔案名稱錯誤，應為" + sn + "2");
                    $("#b" + j).css("color", "red");
                    $('#CheckFiles').val("False");
                    $("#save_sort").css("display", "none");
                }
                ShowSendButton();
            }
            else {
                $("#save_sort").css("display", "none");
                $("#b" + j).text("副檔名不為 .png");
                $("#b" + j).css("color", "red");
            }
        }
    });

    $("#Pic3_Big").change(function () {

        $('#CheckFiles').val("True");
        $('#ShowPic3_Big').html("");
        var files = $('#Pic3_Big')[0].files;

        for (var i = 0; i < files.length; i++) {
            var j = i + 1;

            var file = files[i].name.split(".");
            var name = file[0];
            var type = name.substr(15, 1);
            var extension = file[1];

            $("#ShowPic3_Big").append("<span style='color:red' id='c" + j + "'></span><br>");
            if (extension == "png" || extension == "PNG") {
                //檢查商品序號
                if (name.substr(0, 15) != sn) {
                    $("#c" + j).text("檔案名稱錯誤，應為" + sn + "3");
                    $("#c" + j).css("color", "red");
                    $('#CheckFiles').val("False");
                    $("#save_sort").css("display", "none");
                }
                ShowSendButton();
            }
            else {
                $("#save_sort").css("display", "none");
                $("#c" + j).text("副檔名不為 .png");
                $("#c" + j).css("color", "red");
            }
        }
    });

    $("#Pic4_Big").change(function () {

        $('#CheckFiles').val("True");
        $('#ShowPic4_Big').html("");
        var files = $('#Pic4_Big')[0].files;

        for (var i = 0; i < files.length; i++) {
            var j = i + 1;

            var file = files[i].name.split(".");
            var name = file[0];
            var type = name.substr(15, 1);
            var extension = file[1];

            $("#ShowPic4_Big").append("<span style='color:red' id='d" + j + "'></span><br>");
            if (extension == "png" || extension == "PNG") {
                //檢查商品序號
                if (name.substr(0, 15) != sn) {
                    $("#d" + j).text("檔案名稱錯誤，應為" + sn + "4");
                    $("#d" + j).css("color", "red");
                    $('#CheckFiles').val("False");
                    $("#save_sort").css("display", "none");
                }
                ShowSendButton();
            }
            else {
                $("#save_sort").css("display", "none");
                $("#d" + j).text("副檔名不為 .png");
                $("#d" + j).css("color", "red");
            }
        }
    });

    $("#Pic5_Big").change(function () {

        $('#CheckFiles').val("True");
        $('#ShowPic5_Big').html("");
        var files = $('#Pic5_Big')[0].files;

        for (var i = 0; i < files.length; i++) {
            var j = i + 1;

            var file = files[i].name.split(".");
            var name = file[0];
            var type = name.substr(15, 1);
            var extension = file[1];

            $("#ShowPic5_Big").append("<span style='color:red' id='e" + j + "'></span><br>");
            if (extension == "png" || extension == "PNG") {
                //檢查商品序號
                if (name.substr(0, 15) != sn) {
                    $("#e" + j).text("檔案名稱錯誤，應為" + sn + "5");
                    $("#e" + j).css("color", "red");
                    $('#CheckFiles').val("False");
                    $("#save_sort").css("display", "none");
                }
                ShowSendButton();
            }
            else {
                $("#save_sort").css("display", "none");
                $("#e" + j).text("副檔名不為 .png");
                $("#e" + j).css("color", "red");
            }
        }
    });

});
function ShowSendButton() {
    var result = $('#CheckFiles').val();
    if (result == "True") {
        $("#save_sort").css("display", "block");
    }
    else {
        $("#save_sort").css("display", "none");
    }
}

//按下送出
function Send() {

    $('body').css('overflow', 'hidden')
    $.isLoading({ text: "處理中" });
    // Setup Loading plugin
    $("#load-overlay .demo p").removeClass("alert-success");
    // Re-enabling event
    setTimeout(function () {
        $.isLoading("hide");
        $("#load-overlay .demo p").html("Content Loaded")
                                    .addClass("alert-success");
    }, 500000);
    return true;
}