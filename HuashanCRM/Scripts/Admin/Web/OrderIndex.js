﻿
$(window).load(function () {
    $("#dialog").dialog({
        autoOpen: false,
        height: 250,
        width: 400,
        close: function () {
            $('html, body').css('overflow', 'auto');
            $("#overlay").css("display", "none");
        }
    });

    //廠商Index
    if (location.href.match('Company') != null) {
        //購買商品中有備貨、出貨時，移除Checkbox
        Delete_Checkbox();
    }

    //載入 Search_Direction
    var Search_Direction = $('input[name=Search_Direction]:checked').val()
    if (Search_Direction == 0) {
        $("#span_CashFlow1").css("display", "none")
        $("#span_CashFlow2").css("display", "none")
        $("#span_LogisticsStatus1").css("display", "none")
        $("#span_LogisticsStatus2").css("display", "none")
        $("#span_LogisticsStatus3").css("display", "inline")

        if (typeof ($('input:radio[name=Search_CashFlow]')[0]) != "undefined") {
            $('input:radio[name=Search_CashFlow]')[0].checked = true;
        }
        if (typeof ($('input:radio[name=Search_LogisticsStatus]')[0]) != "undefined") {
            $('input:radio[name=Search_LogisticsStatus]')[0].checked = true;
        }
    }
    else if (Search_Direction == 1) {
        $("#span_CashFlow1").css("display", "inline")
        $("#span_CashFlow2").css("display", "none")
        $("#span_LogisticsStatus1").css("display", "inline")
        $("#span_LogisticsStatus2").css("display", "none")
        $("#span_LogisticsStatus3").css("display", "none")

    }
    else if (Search_Direction == 2) {
        $("#span_CashFlow1").css("display", "none")
        $("#span_CashFlow2").css("display", "inline")
        $("#span_LogisticsStatus1").css("display", "none")
        $("#span_LogisticsStatus2").css("display", "inline")
        $("#span_LogisticsStatus3").css("display", "none")
    }


    $('input[name=Search_Direction]').change(function () {

        if (this.value == 0) {
            $("#span_CashFlow1").css("display", "none")
            $("#span_CashFlow2").css("display", "none")
            $("#span_LogisticsStatus1").css("display", "none")
            $("#span_LogisticsStatus2").css("display", "none")
            $("#span_LogisticsStatus3").css("display", "inline")

            if (typeof ($('input:radio[name=Search_CashFlow]')[0]) != "undefined") {
                $('input:radio[name=Search_CashFlow]')[0].checked = true;
            }
            if (typeof ($('input:radio[name=Search_LogisticsStatus]')[0]) != "undefined") {
                $('input:radio[name=Search_LogisticsStatus]')[0].checked = true;
            }
            
        }
        else if (this.value == 1) {
            $("#span_CashFlow1").css("display", "inline")
            $("#span_CashFlow2").css("display", "none")
            $("#span_LogisticsStatus1").css("display", "inline")
            $("#span_LogisticsStatus2").css("display", "none")
            $("#span_LogisticsStatus3").css("display", "none")

        }
        else if (this.value == 2) {
            $("#span_CashFlow1").css("display", "none")
            $("#span_CashFlow2").css("display", "inline")
            $("#span_LogisticsStatus1").css("display", "none")
            $("#span_LogisticsStatus2").css("display", "inline")
            $("#span_LogisticsStatus3").css("display", "none")
        }
    });

    //單選打勾
    $(".delete").on("change", function () {
        var chk_Order = "";
        //載入Cookie的值
        if ($.cookie('chk_Order') != null && $.cookie('chk_Order') != "") {
            chk_Order += $.cookie('chk_Order') + ",";
        }
        //把打勾取消
        if (chk_Order.match($(this).val()) != null) {
            // Id + ","
            chk_Order = chk_Order.replace($(this).val() + ",", "");
        }
        //有勾選的CheckBox
        var checkedList = $('input[name="item"]:checked');
        checkedList.each(function () {
            if (chk_Order.match($(this).val()) == null) {
                chk_Order += $(this).val() + ",";
            }
        });
        //如果第一個字是逗號，去掉
        var Index = chk_Order.indexOf(",");
        if (Index == 0) {
            chk_Order = chk_Order.substring(1);
        }

        //去除最後一個逗號
        var lastIndex = chk_Order.lastIndexOf(',');
        if (lastIndex > -1) {
            chk_Order = chk_Order.substring(0, chk_Order.length - 1);
        }

        //將值塞到Cookie
        var date = new Date();
        //Cookie預設十分鐘
        date.setTime(date.getTime() + (600 * 1000));
        $.cookie('chk_Order', chk_Order, { expires: date, path: '/' })
    });

    $.cookie.json = true;
    //載入有勾選的值
    Get_CheckboxVal();
});

function OpenDialog_QA() {

    $('html, body').css('overflow', 'hidden');
    $("#dialog").dialog("open");
    $("#overlay").css("display", "block");
}

function Delete_Checkbox() {
    var tr_lengh = $("table> tbody > tr").length;
    for (var i = 1; i <= tr_lengh; i++) {

        var td_item0 = ".item0_" + i;
        var td_item2 = ".item2_" + i;
        var td_item5 = ".item5_" + i;

        //代表已經備貨
        if ($(td_item2).text().match('未接單') == null) {
            $(td_item0).text("");
            $(td_item5).text("是");
            $(td_item5).css("color", "#00AA00");
        }
    }
}

function Search_Company() {
    document.SearchForm.action = "/GoX/Order/Index_Company";
    document.SearchForm.method = "get";
    SearchForm.submit();
};

function Search_Mith() {
    document.SearchForm.action = "/GoX/Order/Index_Mith";
    document.SearchForm.method = "get";
    SearchForm.submit();
};

//覆核
function StockUp() {
    var chk_Order;
    if (typeof $.cookie('chk_Order') != 'undefined') {
        chk_Order = $.cookie('chk_Order');
    }
    if (chk_Order != null) {
        if (confirm("確定要備貨?")) {
            $('body').css('overflow', 'hidden')
            $.isLoading({ text: "處理中" });
            // Setup Loading plugin
            $("#load-overlay .demo p").removeClass("alert-success");
            // Re-enabling event
            setTimeout(function () {
                $.isLoading("hide");
                $("#load-overlay .demo p").html("Content Loaded")
                                            .addClass("alert-success");
            }, 500000);

            $.ajax({
                type: "post",
                traditional: true,
                async: true,
                url: "/Gox/Order/Update_Status",
                data: { OrderId: chk_Order, Status: 1 },
                success: function (data) {
                    if (data = "Success") {
                        //將值塞到Cookie
                        var date = new Date();
                        //Cookie預設5秒
                        date.setTime(date.getTime() + (5 * 1000));
                        $.cookie('chk_Order', null, { expires: date, path: '/' });
                        location.href = location.href;
                        CheckSuccess(data);
                    }
                },
                error: function (jqXHR, exception) {
                    alert("系統發生錯誤！")
                }
            });
        }
    }
    else {
        alert("未勾選任何項目！");
        return false;
    }
}

//全選打勾
function AllCheck() {
    var chk_Order = "";

    //載入Cookie的值
    if ($.cookie('chk_Order') != null && $.cookie('chk_Order') != "") {
        chk_Order += $.cookie('chk_Order') + ",";
    }

    $(".delete").each(function () {
        if ($("#AllCheck").prop("checked")) {
            $(this).prop("checked", $("#AllCheck").prop("checked"));
            if (chk_Order.match($(this).val()) == null) {
                chk_Order += $(this).val() + ",";
            }

        }
        else {
            $(this).prop("checked", false);
            //把打勾取消
            if (chk_Order.match($(this).val()) != null) {
                // Id + ","
                chk_Order = chk_Order.replace($(this).val() + ",", "");
            }
        }
    });

    //如果第一個字是逗號，去掉
    var Index = chk_Order.indexOf(",");
    if (Index == 0) {
        chk_Order = chk_Order.substring(1);
    }

    //去除最後一個逗號
    var lastIndex = chk_Order.lastIndexOf(',');
    if (lastIndex > -1) {
        chk_Order = chk_Order.substring(0, chk_Order.length - 1);
    }

    //將值塞到Cookie
    var date = new Date();
    //Cookie預設十分鐘
    date.setTime(date.getTime() + (600 * 1000));
    $.cookie('chk_Order', chk_Order, { expires: date, path: '/' })
};

//載入有勾選的值
function Get_CheckboxVal() {

    var chk_Order = "";

    if (typeof $.cookie('chk_Order') != 'undefined') {
        chk_Order = $.cookie('chk_Order');
    }
    $(".delete").each(function () {
        if (chk_Order != null && chk_Order != "") {
            if (chk_Order.match(this.id) != null) {
                $("#" + this.id).prop('checked', true);
            }
        }
    });
}

function CheckSuccess(status) {
    if (status == "Success") {
        $(".isloading-overlay").css("display", "none");
        alert("儲存成功！");
        $("html").css("overflow", "auto");
    }
    else {
        alert("儲存失敗！");
    }
}