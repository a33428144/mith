﻿
$(function () {
    ChangeButton_Color();

    var form = $("#SearchForm");
    //讀取區塊的Val
    var Search_Area = $('input:radio:checked[name="Search_Area"]').val();
    if (Search_Area == "Left") {
        form.attr('action', '/GoX/Activity/Promotion100_Area1');
        form.attr('data-ajax-update', '#Update_Promotion100_Area1');
    }
    else {
        form.attr('action', '/GoX/Activity/Promotion100_Area2');
        form.attr('data-ajax-update', '#Update_Promotion100_Area2');
    }

    $('input[type=radio][name=Search_Area]').change(function () {
        if (this.value == "Left") {
            form.attr('action', '/GoX/Activity/Promotion100_Area1');
            form.attr('data-ajax-update', '#Update_Promotion100_Area1');
        }
        else {
            form.attr('action', '/GoX/Activity/Promotion100_Area2');
            form.attr('data-ajax-update', '#Update_Promotion100_Area2');
        }
    });

    Change_height();
});

function Search() {
    var sum = 0;
    //關鍵字
    if ($("#keyword").val() != "") {
        sum++;
    }
    //類別
    if ($("#Search_Type").val() != "0") {
        sum++;
    }
    //季節
    if ($("#Search_Season").val() != "0") {
        sum++;
    }
    //顏色
    if ($("#Search_Color").val() != "0") {
        sum++;
    }
    //風格
    if ($("#Search_Style").val() != "0") {
        sum++;
    }
    //適合
    if ($("#Search_Fit").val() != "0") {
        sum++;
    }
    //公司
    if ($("#Search_CompanyId").val() != "0") {
        sum++;
    }

    if ($("#CreateTime_St").val() != "") {
        sum++;
    }
    if ($("#CreateTime_End").val() != "") {
        sum++;
    }

    //搜尋一個條件以上
    if (sum >= 1) {
        return true;
    }
    else {
        alert("請至少設定一個搜尋條件！")
        return false;
    }
}

function ChangeButton_Color() {
    var ButtonId = "#" + $("#Search_Pattern").val();
    $(ButtonId).css("box-shadow", "inset 0px 1px 0px 0px #ddd");
    $(ButtonId).css("border", "1px solid #999");
    $(ButtonId).css("background", "#999");
    $(ButtonId).attr("href", "#");
}

//點前100件
function ToPromotion100() {

    if (confirm("確定要設定為【前100件】?")) {
        var chk_Activity = "";
        var Area1 = $(".Area1>li").length;
        //有勾選的CheckBox
        var checkedList = $('input[name="Area2Chk"]:checked');

        if (checkedList.length == 0) {
            alert("未勾選任何項目！");
            return false;
        }

        if (parseInt(Area1 + checkedList.length) > 100) {
            alert("前100數量：" + Area1 + "件，非前100您勾選了：" + checkedList.length + "件\n相加已超過100件！")
            return false;
        }

        checkedList.each(function () {
            chk_Activity += $(this).val() + ",";
            Move_li($(this).val(), "ToPromotion100");
        });

        //去除最後一個逗號
        var lastIndex = chk_Activity.lastIndexOf(',');
        if (lastIndex > -1) {
            chk_Activity = chk_Activity.substring(0, chk_Activity.length - 1);
        }

        //將值塞到Cookie
        var date = new Date();
        //Cookie預設60秒
        date.setTime(date.getTime() + (60 * 1000));
        $.cookie('chk_Activity', chk_Activity, { expires: date, path: '/' })

        Show_Loading();

        $.ajax({
            type: "post",
            traditional: true,
            async: true,
            url: "/GoX/Activity/Ajax_Promotion100",
            data: {
                chk_Activity: $.cookie('chk_Activity'),
                Promotion100: true
            },
            success: function (data) {
                Get_Result(data);
            }
        });

        return true;
    }
    else {
        return false;
    }
}

//點非前100件
function ToNotPromotion100() {
    if (confirm("確定要設定為【非前100件】")) {

        var chk_Activity = "";
        //有勾選的CheckBox
        var checkedList = $('input[name="Area1Chk"]:checked');

        if (checkedList.length == 0) {
            alert("未勾選任何項目！");
            return false;
        }

        checkedList.each(function () {
            if (chk_Activity.match($(this).val()) == null) {
                chk_Activity += $(this).val() + ",";
                Move_li($(this).val(), "ToNotPromotion100");
            }
        });

        //去除最後一個逗號
        var lastIndex = chk_Activity.lastIndexOf(',');
        if (lastIndex > -1) {
            chk_Activity = chk_Activity.substring(0, chk_Activity.length - 1);
        }

        //將值塞到Cookie
        var date = new Date();
        //Cookie預設60秒
        date.setTime(date.getTime() + (60 * 1000));
        $.cookie('chk_Activity', chk_Activity, { expires: date, path: '/' })

        Show_Loading();

        $.ajax({
            type: "post",
            traditional: true,
            async: true,
            url: "/GoX/Activity/Ajax_Promotion100",
            data: {
                chk_Activity: chk_Activity,
                Promotion100: false
            },
            success: function (data) {
                Get_Result(data);
            }
        });

        return true;
    }
    else {
        return false;
    }
}

function Show_Loading() {
    $(".isloading-overlay").css("background", "none")
    $('body').css('overflow', 'hidden')
    $.isLoading({ text: "處理中" });
    // Setup Loading plugin
    $("#load-overlay .demo p").removeClass("alert-success");
    // Re-enabling event

    setTimeout(function () {
        $.isLoading("hide");
        $("#load-overlay .demo p").html("Content Loaded")
                                    .addClass("alert-success");
    }, 445000);
}

function Ajax_Success() {
    Change_height();
    $(".isloading-overlay").css("display", "none");
    $('html, body').css('overflowY', 'auto');
}

function Get_Result(status) {
    $(".isloading-overlay").css("display", "none");

    if (status == "Success") {
        alert("儲存成功！");
        $("html").css("overflow", "auto");
    }
    else {
        alert("儲存失敗！");
    }
}

//跑迴圈
function Get_data(target) {
    var arr = [];
    $(target).each(function () {
        arr.push($(this).attr("data-id"));
    });
    return arr;
}

//更新Area > li項目
function Move_li(Id, Action) {

    //將項目從【非置頂】移到【置頂】
    if (Action == "ToPromotion100") {
        var li_Id = "#li2_" + Id;
        var li_html = "<li class='item ui-state-default' data-id='" + Id + "' id='li1_" + Id + "'>" + $(li_Id).html() + "</li>";
        $(li_Id).remove();
        $("#Update_Promotion100_Area1").prepend(li_html);
    }
        //將項目從【置頂】移到【非置頂】
    else {
        var li_Id = "#li1_" + Id;
        var li_html = "<li class='item ui-state-default' data-id='" + Id + "' id='li2_" + Id + "'>" + $(li_Id).html() + "</li>";
        $(li_Id).remove();
        $("#Update_Promotion100_Area2").prepend(li_html);
    }
}

//調整高度
function Change_height() {

    var Area1_count = $(".Area1>li").length;
    var Area2_count = $(".Area2>li").length;

    //其他路線 高度
    if (Area1_count > Area2_count) {
        $(".Area1").css("height", 45 * Area1_count);
        $(".Area2").css("height", 45 * Area1_count);
    }
    else {
        $(".Area1").css("height", 45 * Area2_count);
        $(".Area2").css("height", 45 * Area2_count);
    }

    //如果都沒資料，預設高度為125px
    if (Area1_count < 3 && Area2_count < 3) {
        $(".Area1").css("height", 125);
        $(".Area2").css("height", 125);
    }
}

