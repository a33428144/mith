﻿$(function () {
    var sn = $("#SN").val();
    var _URL = window.URL;

    $("#Pic1_Big").change(function () {
        $('#CheckFiles').val("True");
        $('#ShowPic1_Big').html("");
        var files = $('#Pic1_Big')[0].files;

        for (var i = 0; i < files.length; i++) {
            var j = i + 1;

            var file = files[i].name.split(".");
            var name = file[0];
            var type = name.substr(11, 1);
            var extension = file[1];
            $("#ShowPic1_Big").append("<span style='color:red' id='a" + j + "'></span><br>");
            if (extension == "png" || extension == "PNG") {
                //檢查圖片序號
                if (name.substr(0, 11) != sn) {
                    $("#a" + j).text("檔案名稱錯誤，應為" + sn+"1");
                    $("#a" + j).css("color", "red");
                    $('#CheckFiles').val("False");
                    $("#save_sort").css("display", "none");
                }
                ShowSendButton();
            }
            else {
                $("#save_sort").css("display", "none");
                $("#a" + j).text("副檔名不為 .png");
                $("#a" + j).css("color", "red");
            }
        }
    });

    $("#Pic2_Big").change(function () {
        $('#CheckFiles').val("True");
        $('#ShowPic2_Big').html("");
        var files = $('#Pic2_Big')[0].files;

        for (var i = 0; i < files.length; i++) {
            var j = i + 1;

            var file = files[i].name.split(".");
            var name = file[0];
            var type = name.substr(11, 1);
            var extension = file[1];
            $("#ShowPic2_Big").append("<span style='color:red' id='b" + j + "'></span><br>");
            if (extension == "png" || extension == "PNG") {
                //檢查圖片序號
                if (name.substr(0, 11) != sn) {
                    $("#b" + j).text("檔案名稱錯誤，應為" + sn + "2");
                    $("#b" + j).css("color", "red");
                    $('#CheckFiles').val("False");
                    $("#save_sort").css("display", "none");
                }
                ShowSendButton();
            }
            else {
                $("#save_sort").css("display", "none");
                $("#b" + j).text("副檔名不為 .png");
                $("#b" + j).css("color", "red");
            }
        }
    });

});
function ShowSendButton() {
    var result = $('#CheckFiles').val();
    if (result == "True") {
        $("#save_sort").css("display", "block");
    }
    else {
        $("#save_sort").css("display", "none");
    }
}

//按下送出
function Send() {

    //驗證通過
    $('body').css('overflow', 'hidden')
    $.isLoading({ text: "處理中" });
    // Setup Loading plugin
    $("#load-overlay .demo p").removeClass("alert-success");
    // Re-enabling event
    setTimeout(function () {
        $.isLoading("hide");
        $("#load-overlay .demo p").html("Content Loaded")
                                    .addClass("alert-success");
    }, 500000);
    return true;
}