﻿
$(window).load(function () {
    Set_DefaulVal($("#Role").val())
    //更動角色
    $("#Role").combobox({
        select: function (event, ui) {
            $("#Role_Valid").css("display", "none");
            var sel = this.value;
            $.ajax({
                type: "post",
                traditional: true,
                async: true,
                url: "/Gox/UserProfile/Ajax_UserLevel",
                data: { role: sel },
                success: function (data) {
                    if (data) {
                        $("#LevelId").html(data);
                    }
                }
            });

            if (sel == 0 || sel == 1) {
                $("#CompanyGroup").hide();
                $("#ExpertGroup").hide();
            }
            else if (sel == 2) {
                $("#CompanyGroup").show();
                $("#ExpertGroup").hide();
            }
            else if (sel == 3) {
                $("#CompanyGroup").hide();
                $("#ExpertGroup").show();
                $('#LevelGroup').css('display', 'none')
            }
            else {
            }
        }
    });

    $("#CompanyId").combobox({
        select: function (event, ui) {

            var sel = this.value;
            if (sel == 0) {
                $("#CompanyId_Valid").css("display", "block");
            }
            else {
                $("#CompanyId_Valid").css("display", "none");
            }
        }
    });

    $("#ExpertId").combobox({
        select: function (event, ui) {

            var sel = this.value;
            if (sel == 0) {
                $("#ExpertId_Valid").css("display", "block");
            }
            else {
                $("#ExpertId_Valid").css("display", "none");
            }
        }
    });

    $("#LevelId").combobox({
        select: function (event, ui) {

            var sel = this.value;
            if (sel == 0) {
                $("#LevelId_Valid").css("display", "inline");
            }
            else {
                $("#LevelId_Valid").css("display", "none");
            }
        }
    });

    $("#OldPassword").keydown(function () {
        $('#Check_OldPassword').val("");
        $("#OldPassword").css("background-color", "#fff")
        $("#OldPassword_Valid").css("display", "none");
    });
});

function Set_DefaulVal(sel) {
    if (sel == 0 || sel == 1) {
        $("#CompanyGroup").hide();
        $("#ExpertGroup").hide();
        $("#LevelGroup").show();
    }
    else if (sel == 2) {
        $("#CompanyGroup").show();
        $("#ExpertGroup").hide();
        $("#LevelGroup").show();
    }
    else if (sel == 3) {
        $("#CompanyGroup").hide();
        $("#ExpertGroup").show();
        $("#LevelGroup").hide();
    }
    else {
    }
}

//確認商品編號
function Check_OldPassword_Click() {
    var OldPassword = $("#OldPassword").val().replace(" ", "");
    $("#OldPassword").val(OldPassword);
    $.ajax({
        type: "post",
        traditional: true,
        async: true,
        url: "/GoX/UserProfile/Check_OldPassword",
        data: { OldPassword: OldPassword },
        success: function (data) {
            $('#Check_OldPassword').val(data);
            if (data == "True") {
                $("#OldPassword_Valid").text(null);
                $("#OldPassword_Valid").css("display", "none")
                $("#OldPassword").css("background-color", "#d5ffd5")
                $('html, body').scrollTop(0);
            }
            else {
                $("#OldPassword_Valid").text("密碼錯誤");
                $("#OldPassword_Valid").css("display", "block")
                $('html, body').scrollTop(0);
            }
        }
    });
}

//按下送出
function Send_Mith() {
    var Role = $("#Role").val();
    var CompanyId = $("#CompanyId").val();
    var ExpertId = $("#ExpertId").val();
    var LevelId = $("#LevelId").val();

    if (Role == "0") {
        $("#Role_Valid").css("display", "block");
    }
        //角色：亞設人員
    else if (Role == "1") {

    }
        //角色：廠商，沒選擇廠商
    else if (Role == "2" && CompanyId == "0") {
        $("#CompanyId_Valid").css("display", "block");
    }
        //角色：達人，沒選擇達人
    else if (Role == "3" && ExpertId == "0") {
        $("#ExpertId_Valid").css("display", "block");
    }

    //權限
    if (LevelId == "0") {
        $("#LevelId_Valid").css("display", "inline");
    }

    //驗證通過
    if ($("#Form").valid() && Role != "0") {

        //角色：廠商，沒選擇廠商
        if (Role == "2" && CompanyId == "0") {
            return false;
        }
            //角色：達人，沒選擇達人
        else if (Role == "3" && ExpertId == "0") {
            return false;
        }

        var User_Level = $("#User_Level").val();
        var Check_OldPassword = $("#Check_OldPassword").val();
        //非最高權限帳戶
        if (User_Level > 3) {
            var OldPassword = $("#OldPassword").val();
            var Password = $("#Password").val();
            var ConfirmPassword = $("#ConfirmPassword").val();
            //有輸入新密碼
            if (Password != null && Password != "" && ConfirmPassword != null && ConfirmPassword != "") {
                //有輸入舊密碼
                if (OldPassword != null && OldPassword != "") {

                    if (Check_OldPassword == "") {
                        $("#OldPassword_Valid").text("請驗證舊密碼");
                        $("#OldPassword_Valid").css("display", "block")
                        $('html, body').scrollTop(0);
                        return false;
                    }
                }
                    //沒輸入舊密碼
                else {
                    $("#OldPassword_Valid").text("請輸入舊密碼");
                    $("#OldPassword_Valid").css("display", "block")
                    $('html, body').scrollTop(0);
                    return false;
                }
            }

            //密碼錯誤
            if (Check_OldPassword == "False") {
                $("#OldPassword_Valid").text("密碼錯誤");
                $("#OldPassword_Valid").css("display", "block")
                $('html, body').scrollTop(0);
                return false;
            }
        }

        $('body').css('overflow', 'hidden')
        $.isLoading({ text: "處理中" });
        // Setup Loading plugin
        $("#load-overlay .demo p").removeClass("alert-success");
        // Re-enabling event
        setTimeout(function () {
            $.isLoading("hide");
            $("#load-overlay .demo p").html("Content Loaded")
                                        .addClass("alert-success");
        }, 500000);
        return true;
    }
    else {

        return false
    };
}

function Send_Company() {
    var LevelId = $("#LevelId").val();

    //權限
    if (LevelId == "0") {
        $("#LevelId_Valid").css("display", "inline");
    }

    //驗證通過
    if ($("#Form").valid() && LevelId != "0") {

        var User_Level = $("#User_Level").val();
        var Check_OldPassword = $("#Check_OldPassword").val();
        //非最高權限帳戶
        if (User_Level > 3) {
            var OldPassword = $("#OldPassword").val();
            var Password = $("#Password").val();
            var ConfirmPassword = $("#ConfirmPassword").val();
            //有輸入新密碼
            if (Password != null && Password != "" && ConfirmPassword != null && ConfirmPassword != "") {
                //有輸入舊密碼
                if (OldPassword != null && OldPassword != "") {

                    if (Check_OldPassword == "") {
                        $("#OldPassword_Valid").text("請驗證舊密碼");
                        $("#OldPassword_Valid").css("display", "block")
                        $('html, body').scrollTop(0);
                        return false;
                    }
                }
                    //沒輸入舊密碼
                else {
                    $("#OldPassword_Valid").text("請輸入舊密碼");
                    $("#OldPassword_Valid").css("display", "block")
                    $('html, body').scrollTop(0);
                    return false;
                }
            }

            //密碼錯誤
            if (Check_OldPassword == "False") {
                $("#OldPassword_Valid").text("密碼錯誤");
                $("#OldPassword_Valid").css("display", "block")
                $('html, body').scrollTop(0);
                return false;
            }
        }
        $('body').css('overflow', 'hidden')
        $.isLoading({ text: "處理中" });
        // Setup Loading plugin
        $("#load-overlay .demo p").removeClass("alert-success");
        // Re-enabling event
        setTimeout(function () {
            $.isLoading("hide");
            $("#load-overlay .demo p").html("Content Loaded")
                                        .addClass("alert-success");
        }, 500000);
        return true;
    }
    else {
        return false
    };
}