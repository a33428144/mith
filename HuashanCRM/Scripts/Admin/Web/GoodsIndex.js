﻿
$(function () {

    if ($("#IsLimitOrder").val() == "False") {
        $('#Type option[value=Search_Type]').remove();
        $("#ChangeText").text("庫存量");
    }
    //匯入Excel
    var actionUrls = {
        Import: '@Url.Action("Import", "Goods")',
    };
    project.ZipCode.Initialize(actionUrls);

    //單選打勾
    $(".delete").on("change", function () {
        var chk_Goods = "";
        //載入Cookie的值
        if ($.cookie('chk_Goods') != null && $.cookie('chk_Goods') != "") {
            chk_Goods += $.cookie('chk_Goods') + ",";
        }
        //把打勾取消
        if (chk_Goods.match($(this).val()) != null) {
            // Id + ","
            chk_Goods = chk_Goods.replace($(this).val() + ",", "");
        }
        //有勾選的CheckBox
        var checkedList = $('input[name="item"]:checked');
        checkedList.each(function () {
            if (chk_Goods.match($(this).val()) == null) {
                chk_Goods += $(this).val() + ",";
            }
        });
        //如果第一個字是逗號，去掉
        var Index = chk_Goods.indexOf(",");
        if (Index == 0) {
            chk_Goods = chk_Goods.substring(1);
        }

        //去除最後一個逗號
        var lastIndex = chk_Goods.lastIndexOf(',');
        if (lastIndex > -1) {
            chk_Goods = chk_Goods.substring(0, chk_Goods.length - 1);
        }

        //將值塞到Cookie
        var date = new Date();
        //Cookie預設十分鐘
        date.setTime(date.getTime() + (600 * 1000));
        $.cookie('chk_Goods', chk_Goods, { expires: date, path: '/' })
    });

    $.cookie.json = true;
    //載入有勾選的值
    Get_CheckboxVal();
});

function Search_Mith() {
    var show_number = $("#show_number").val();

    //要輸入數字
    var regex = new RegExp(/^[0-9_]+$/);
    if (regex.test(show_number) == false) {
        alert("筆數請輸入數字！");
        return false
    }
    else {
        //上限50筆
        if (show_number > 50) {
            alert("筆數最多50筆！");
            return false;
        }
    }

    document.SearchForm.action = "/GoX/Goods/Index_Mith";
    document.SearchForm.method = "get";
    SearchForm.submit();
};

function Search_Company() {
    document.SearchForm.action = "/GoX/Goods/Index_Company";
    document.SearchForm.method = "get";
    SearchForm.submit();
};

function Export_Mith() {
    document.SearchForm.action = "/GoX/Goods/Export_Mith";
    document.SearchForm.method = "post";
    SearchForm.submit();
};

function Export_Company() {
    document.SearchForm.action = "/GoX/Goods/Export_Company";
    document.SearchForm.method = "post";
    SearchForm.submit();
};

//覆核
function ReviewGoods() {
    var chk_Goods;
    if (typeof $.cookie('chk_Goods') != 'undefined') {
        chk_Goods = $.cookie('chk_Goods');
    }
    if (chk_Goods != null) {
        if (confirm("確定要覆核?")) {
            $('body').css('overflow', 'hidden')
            $.isLoading({ text: "處理中" });
            // Setup Loading plugin
            $("#load-overlay .demo p").removeClass("alert-success");
            // Re-enabling event
            setTimeout(function () {
                $.isLoading("hide");
                $("#load-overlay .demo p").html("Content Loaded")
                                            .addClass("alert-success");
            }, 500000);

            $.ajax({
                type: "post",
                traditional: true,
                async: true,
                url: "/Gox/Goods/Update_Status",
                data: { Id: chk_Goods, Status: 2 },
                success: function (data) {
                    if (data = "Success") {
                        //將值塞到Cookie
                        var date = new Date();
                        //Cookie預設5秒
                        date.setTime(date.getTime() + (5 * 1000));
                        $.cookie('chk_Goods', null, { expires: date, path: '/' });
                        location.href = location.href;
                        CheckSuccess(data);
                    }
                },
                error: function (jqXHR, exception) {
                    alert("系統發生錯誤！")
                }
            });
        }
    }
    else {
        alert("未勾選任何項目！");
        return false;
    }
}

//上架
function PutOnSale() {
    var chk_Goods;
    if (typeof $.cookie('chk_Goods') != 'undefined') {
        chk_Goods = $.cookie('chk_Goods');
    }
    if (chk_Goods != null) {
        if (confirm("確定要上架?")) {
            $('body').css('overflow', 'hidden')
            $.isLoading({ text: "處理中" });
            // Setup Loading plugin
            $("#load-overlay .demo p").removeClass("alert-success");
            // Re-enabling event
            setTimeout(function () {
                $.isLoading("hide");
                $("#load-overlay .demo p").html("Content Loaded")
                                            .addClass("alert-success");
            }, 500000);

            $.ajax({
                type: "post",
                traditional: true,
                async: true,
                url: "/Gox/Goods/Update_Status",
                data: { Id: chk_Goods, Status: 3 },
                success: function (data) {
                    if (data = "Success") {
                        //將值塞到Cookie
                        var date = new Date();
                        //Cookie預設5秒
                        date.setTime(date.getTime() + (5 * 1000));
                        $.cookie('chk_Goods', null, { expires: date, path: '/' });
                        location.href = location.href;
                        CheckSuccess(data);
                    }
                },
                error: function (jqXHR, exception) {
                    alert("系統發生錯誤！")
                }
            });
        }
    }
    else {
        alert("未勾選任何項目！");
        return false;
    }
}

//刪除
function DeleteGoods() {

    var chk_Goods;
    if (typeof $.cookie('chk_Goods') != 'undefined') {
        chk_Goods = $.cookie('chk_Goods');
    }
    if (chk_Goods != null) {
        if (confirm("確定要刪除?")) {
            $('body').css('overflow', 'hidden')
            $.isLoading({ text: "處理中" });
            // Setup Loading plugin
            $("#load-overlay .demo p").removeClass("alert-success");
            // Re-enabling event
            setTimeout(function () {
                $.isLoading("hide");
                $("#load-overlay .demo p").html("Content Loaded")
                                            .addClass("alert-success");
            }, 500000);

            $.ajax({
                type: "post",
                traditional: true,
                async: true,
                url: "/Gox/Goods/Update_Status",
                data: { Id: chk_Goods, Status: 999 },
                success: function (data) {
                    if (data = "Success") {
                        //將值塞到Cookie
                        var date = new Date();
                        //Cookie預設5秒
                        date.setTime(date.getTime() + (5 * 1000));
                        $.cookie('chk_Goods', null, { expires: date, path: '/' });
                        location.href = location.href;
                        CheckSuccess(data);
                    }
                },
                error: function (jqXHR, exception) {
                    alert("系統發生錯誤！")
                }
            });
        }
    }
    else {
        alert("未勾選任何項目！");
        return false;
    }
}

//下架
function PullOffShelves() {

    var chk_Goods;
    if (typeof $.cookie('chk_Goods') != 'undefined') {
        chk_Goods = $.cookie('chk_Goods');
    }
    if (chk_Goods != null) {
        if (confirm("確定要下架?")) {
            $('body').css('overflow', 'hidden')
            $.isLoading({ text: "處理中" });
            // Setup Loading plugin
            $("#load-overlay .demo p").removeClass("alert-success");
            // Re-enabling event
            setTimeout(function () {
                $.isLoading("hide");
                $("#load-overlay .demo p").html("Content Loaded")
                                            .addClass("alert-success");
            }, 500000);

            $.ajax({
                type: "post",
                traditional: true,
                async: true,
                url: "/Gox/Goods/Update_Status",
                data: { Id: chk_Goods, Status: -1 },
                success: function (data) {
                    if (data = "Success") {
                        //將值塞到Cookie
                        var date = new Date();
                        //Cookie預設5秒
                        date.setTime(date.getTime() + (5 * 1000));
                        $.cookie('chk_Goods', null, { expires: date, path: '/' });
                        location.href = location.href;
                        CheckSuccess(data);
                    }
                },
                error: function (jqXHR, exception) {
                    alert("系統發生錯誤！")
                }
            });
        }
    }
    else {
        alert("未勾選任何項目！");
        return false;
    }
}

//全選打勾
function AllCheck() {
    var chk_Goods = "";
    //載入Cookie的值
    if ($.cookie('chk_Goods') != null && $.cookie('chk_Goods') != "") {
        chk_Goods += $.cookie('chk_Goods') + ",";
    }

    $(".delete").each(function () {
        if ($("#AllCheck").prop("checked")) {
            $(this).prop("checked", $("#AllCheck").prop("checked"));
            if (chk_Goods.match($(this).val()) == null) {
                chk_Goods += $(this).val() + ",";
            }

        }
        else {
            $(this).prop("checked", false);
            //把打勾取消
            if (chk_Goods.match($(this).val()) != null) {
                // Id + ","
                chk_Goods = chk_Goods.replace($(this).val() + ",", "");
            }
        }
    });

    //如果第一個字是逗號，去掉
    var Index = chk_Goods.indexOf(",");
    if (Index == 0) {
        chk_Goods = chk_Goods.substring(1);
    }

    //去除最後一個逗號
    var lastIndex = chk_Goods.lastIndexOf(',');
    if (lastIndex > -1) {
        chk_Goods = chk_Goods.substring(0, chk_Goods.length - 1);
    }

    //將值塞到Cookie
    var date = new Date();
    //Cookie預設十分鐘
    date.setTime(date.getTime() + (600 * 1000));
    $.cookie('chk_Goods', chk_Goods, { expires: date, path: '/' })

};

//載入有勾選的值
function Get_CheckboxVal() {

    var chk_Goods = "";

    if (typeof $.cookie('chk_Goods') != 'undefined') {
        chk_Goods = $.cookie('chk_Goods');
    }
    $(".delete").each(function () {
        if (chk_Goods != null && chk_Goods != "") {
            if (chk_Goods.match(this.id) != null) {
                $("#" + this.id).prop('checked', true);
            }
        }
    });
}

function CheckSuccess(status) {
    if (status == "Success") {
        $(".isloading-overlay").css("display", "none");
        alert("儲存成功！");
        $("html").css("overflow", "auto");
    }
    else {
        alert("儲存失敗！");
    }
}