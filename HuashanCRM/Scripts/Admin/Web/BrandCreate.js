﻿

$(function () {
    var _URL = window.URL;
    $("input[type='file'][name=Pic_Size]").change(function () {
        var files =$("input[type='file'][name=Pic_Size]")[0].files;
        var file;
        var img = new Image();

        if ((file = this.files[0])) {
            img.onload = function () {
                if (this.width < 600 || this.width > 1200) {
                    alert("寬度需介於 600-1200 像素之間！")
                    $("input[type='file'][name=Pic_Size]").val(null);
                }
                if (this.height < 600 || this.height > 1200) {
                    alert("高度需介於 600-1200 像素之間！")
                    $("input[type='file'][name=Pic_Size]").val(null);
                }
                var Size = Math.round(file.size / 1024);

                if (Size > 1024) {
                    alert("尺寸表請勿上傳1MB以上！")
                    $("input[type='file'][name=Pic_Size]").val(null);
                }

            };
            img.src = _URL.createObjectURL(file);
        }
    });


    $("#CompanyId").combobox({
        select: function (event, ui) {

            var sel = this.value;
            if (sel == 0) {
                $("#CompanyId_Valid").css("display", "block");
            }
            else {
                $("#CompanyId_Valid").css("display", "none");
            }
        }
    });

});

//按下送出
function Send() {
    var CompanyId = $("#CompanyId").val();

    //沒選擇廠商
    if (CompanyId == "0") {
        $("#CompanyId_Valid").css("display", "block");
    }

    //驗證通過
    if ($("#Form").valid() && CompanyId != "0") {
        $('body').css('overflow', 'hidden')
        $.isLoading({ text: "處理中" });
        // Setup Loading plugin
        $("#load-overlay .demo p").removeClass("alert-success");
        // Re-enabling event
        setTimeout(function () {
            $.isLoading("hide");
            $("#load-overlay .demo p").html("Content Loaded")
                                        .addClass("alert-success");
        }, 500000);
        return true;
    }
    else {

        return false
    };
}