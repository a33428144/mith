﻿
$(function () {

    var Now = new Date();
    var Year = Now.getFullYear();
    jQuery('#ReturnGoodsTime').datetimepicker({
        format: 'Y/m/d H:i',
        lang: 'zh-TW',
        step: 30,
        timepickerScrollbar: true,
        todayButton: false,
        yearStart: Year,
        yearEnd: Year,
    });

    $("#dialog").dialog({
        autoOpen: false,
        height: 150,
        width: 450,
        close: function () {
            $('html, body').css('overflow', 'auto');
            $("#overlay").css("display", "none");
        }
    });

    //已收到貨
    if ($('input:checkbox:checked[name="ReturnGoods"]').val() == "true") {
        $('#LogisticsSN').attr('disabled', true); //無法修改物流編號
        $('#ReturnGoods').attr('disabled', true); //無法修改已收到貨
        $('#ReturnGoodsTime').attr('disabled', true); //無法修到貨時間
        $('#span_ReturnGoods').css("display", "block");
    }
    else {
        $('#span_ReturnGoods').css("display", "none");
    }
    //同意退款
    if ($('input:checkbox:checked[name="AgreeRefund"]').val() == "true") {
        $('#AgreeRefund').attr('disabled', true); //無法修改同意退款
    }

    $('#ReturnGoods').change(function () {
        if ($(this).is(":checked")) {
            $('#span_ReturnGoods').css("display", "block");
        }
        else {
            $('#span_ReturnGoods').css("display", "none");
        }
    });
});

function OpenDialog_QA() {

    $('html, body').css('overflow', 'hidden');
    $("#dialog").dialog("open");
    $("#overlay").css("display", "block");
}

//按下送出
function Send() {
    var LogisticsSN = $("#LogisticsSN").val();
    var LogisticsType = $("#LogisticsType").val();
    //驗證通過
    if (LogisticsSN != null && LogisticsSN != "") {

        //是否已收到貨
        if (LogisticsType == 1) {
            if ($('input:checkbox:checked[name="ReturnGoods"]').val() == "true" && $("#ReturnGoodsTime").val() == "") {
                $('#ReturnGoodsTime_Valid').css("display", "inline");
                return false;
            }
        }
        //是否同意退款
        if ($('input:checkbox:checked[name="AgreeRefund"]').val() == "true") {
            if (confirm("當您同意退款，無法再改為不同意退款！")) {
                return true;
            }
            else {
                return false;
            }
        }

        $('body').css('overflow', 'hidden')
        $.isLoading({ text: "處理中" });
        // Setup Loading plugin
        $("#load-overlay .demo p").removeClass("alert-success");
        // Re-enabling event
        setTimeout(function () {
            $.isLoading("hide");
            $("#load-overlay .demo p").html("Content Loaded")
                                        .addClass("alert-success");
        }, 500000);
        return true;
    }
    else {
        $('html, body').scrollTop(0);
        return false
    };
}