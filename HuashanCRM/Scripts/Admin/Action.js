﻿$(function () {
    document.createElement('html');
    document.createElement('head');
    document.createElement('body');
    document.createElement('header');
    document.createElement('footer');
    document.createElement('input');
    document.createElement('section');
    document.createElement('aside');
    document.createElement('div');
    document.createElement('Span');
    document.createElement('button');
    document.createElement('ul');
    document.createElement('ol');
    document.createElement('li');
    document.createElement('time');
    document.createElement('p');
    document.createElement('strong');
    document.createElement('label');
    document.createElement('form');
    document.createElement('nav');
    document.createElement('main');
    document.createElement('img');
    document.createElement('h1');
    document.createElement('h2');
    document.createElement('h3');
    document.createElement('h4');
    document.createElement('a');
    document.createElement('article');
    document.createElement('script');
    document.createElement('style');
    document.createElement('link');
    document.createElement('br');
    document.createElement('Select');
    document.createElement('table');
    document.createElement('thead');
    document.createElement('tr');
    document.createElement('tbody');
    document.createElement('td');


    $("#edit_action").change(function () {
        $("#action").attr("action", $(this).val());
    });

    $("#city").change(function () {
        $.ajax({
            type: "post",
            traditional: true,
            async: true,
            url: RootPath + "/Gox/TravelSight/Ajax_Seciton",
            data: { city: $(this).val(), all: false },
            //dataType: "json",
            //jsonp: "callback",
            //jsonpCallback: "dool",
            success: function (data) {
                if (data) {
                    $("#area").html(data);
                }
            }
        });
    });
});