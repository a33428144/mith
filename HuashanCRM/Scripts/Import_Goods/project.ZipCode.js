﻿//================================================================================================
/// <reference path="_references.js" /> Ps.如果你要新增給IntelliSense用的js檔案, 請加在該檔案中
//================================================================================================

(function (app) {
    //===========================================================================================
    var current = app.ZipCode = {};
    //===========================================================================================

    jQuery.extend(app.ZipCode,
    {
        Initialize: function (actionUrls) {

            /// <summary>
            /// 初始化函式
            /// </summary>
            /// <param name="actionUrls"></param>

            jQuery.extend(project.ActionUrls, actionUrls);

            //上傳檔案事件處理
            current.UploadEventHandler();
        },

        UploadEventHandler: function () {
            /// <summary>
            /// 上傳匯入資料
            /// </summary>

            $("#UploadForm2").ajaxForm({
                iframe: true,
                dataType: "json",
                success: function (result) {
                    $("#UploadForm2").resetForm();
                    if (!result.Result) {
                        project.AlertErrorMessage("錯誤", result.Msg);

                    }
                    else {
                        $('#ResultContent').html(result.Msg);
                        ImportData2(result.Msg);
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    $("#UploadForm2").resetForm();
                    project.AlertErrorMessage("錯誤", "檔案上傳錯誤");
                }
            });
        }

    });
})
(project);

function ImportData2(savedFileName) {
    //ImportData: function (savedFileName) {
    /// <summary>
    /// 資料匯入
    /// </summary>
    /// <param name="mainID"></param>
    $.ajax({
        type: 'post',
        url: '/GoX/Goods/Import',
        data: { savedFileName: savedFileName },
        async: false,
        cache: false,
        dataType: 'json',
        success: function (data) {
            if (data.Msg) {
                project.AlertErrorMessage("錯誤", data.Msg);
                $('#UploadModal').modal('hide');
            }
            else {
                $('#UploadModal').modal('hide');
                if (data.RowCount > 0) {
                    project.ShowMessageCallback("訊息", "成功匯入 " + data.RowCount + " 筆資料", function () {
                        window.location.reload();
                    });
                }
                else {
                    project.AlertErrorMessage("錯誤", '匯入失敗');
                    $('#UploadModal').modal('hide');
                }
            }
        },
        error: function () {
            project.AlertErrorMessage("錯誤", "匯入資料發生錯誤");
            $('#UploadModal').modal('hide');
        }
    });
};