﻿$(function () {
    if ($("#Code").length == 1)
    {
        $("#Code").addClass("validate[required,funcCall[authpic_check]]")
    }
    $('form').validationEngine();
});

function name_check(field, rules, i, options)
{
    if (!is_text(field.val(), "name"))
        return options.allrules.name_check.alertText;
}

function authpic_check(field, rules, i, options) {
    if (field.val() != '') {
        var exist = false;
        $.ajax({
            type: "post",
            traditional: true,
            async: false,
            url: RootPath + "/Home/CheckCaptcha",
            data: { value: field.val() },
            dataType: "json",
            //jsonp: "callback",
            //jsonpCallback: "dool",
            success: function (data) {
                if (data) {
                    exist = data;
                }
            }
        });
        if (!exist) {
            return options.allrules.authpic_check.alertText;
        }
    }
}

function toUnicode(theString) {
    var unicodeString = '';
    for (var i = 0; i < theString.length; i++) {
        var theUnicode = theString.charCodeAt(i).toString(16).toUpperCase();
        while (theUnicode.length < 4) {
            theUnicode = '0' + theUnicode;
        }
        theUnicode = '\\u' + theUnicode;
        unicodeString += theUnicode;
    }
    return unicodeString;
}

function is_text(text, type) {
    if (text == '' || text == null || text == undefined)
        return false;

    var n = toUnicode(text).split('\\u');
    for (var i in n) {
        if (n[i] != '') {
            var num = parseInt(n[i], 16);

            switch (type) {
                case "number":
                    if (!(num >= parseInt('0030', 16) && num <= parseInt('0039', 16)))
                        return false;
                    break;
                case "english":
                    if (!(num >= parseInt('0061', 16) && num <= parseInt('007a', 16)) ||
                            (num >= parseInt('0041', 16) && num <= parseInt('005a', 16)))
                        return false;
                    break;
                case "chinese":
                    if (!(num >= parseInt('4e00', 16) && num <= parseInt('9fa5', 16)))
                        return false;
                    break;
                case "name":
                    if (!((num >= parseInt('0061', 16) && num <= parseInt('007a', 16)) ||
                            (num >= parseInt('0041', 16) && num <= parseInt('005a', 16)) ||
                            (num >= parseInt('4e00', 16) && num <= parseInt('9fa5', 16)) ||
                            num == parseInt('0020', 16)))
                        return false;
                    break;
            }
        }
    }
    return true;
}