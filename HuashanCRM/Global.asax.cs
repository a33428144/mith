﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Mith.Controllers;
using Mith.Areas.GoX.Controllers;
using Mith.Models;
using Mith.Areas.GoX.Models;

namespace Mith
{
    // 注意: 如需啟用 IIS6 或 IIS7 傳統模式的說明，
    // 請造訪 http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            MvcHandler.DisableMvcResponseHeader = true;

            AreaRegistration.RegisterAllAreas();

            //WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterAuth();

            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new RazorViewEngine());
            ValueProviderConfig.Initialize();
        }

        void Session_Start(object sender, EventArgs e)
        {
            var site = SiteOptionModel.Get();
            Session[Method.Session_Site_RootPath] = site.RootPath;
            Session[Method.Session_Site_Title] = site.WebTitle;
            Session[Method.Session_Site_SSL] = site.SSL;
            Session[Method.Session_Site_DNS] = site.DNS;
            Session[Method.Session_Site_Del] = site.Del;
            Session[Method.Session_Site_Logo] = site.WebLogo;
            Session[Method.Session_Site_Icon] = site.WebIcon;
            Session[Method.Session_Site_WebDescription] = site.WebDescription;
            Session[Method.Session_Site_WebSEO] = site.WebSEO;
        }

        protected void Application_BeginRequest(Object sender, EventArgs e)
        {
            if (HttpContext.Current.Request.IsSecureConnection.Equals(false) && Method.SiteSSL)
            {
                Response.Redirect("https://" + Request.ServerVariables["HTTP_HOST"] + HttpContext.Current.Request.RawUrl);
            }
            ValueProviderConfig.Initialize();
        }/**/

        protected void Application_Error(object sender, EventArgs e)
        {
            var httpContext = ((MvcApplication)sender).Context;

            //略過這些錯誤
            if (!httpContext.Error.ToString().Contains("上找不到公用動作方法 'Index'。'") &&
                !httpContext.Error.ToString().Contains("A public action method 'Index' was not found on controller") &&
                httpContext.Error.ToString().Contains("is not a valid worksheet name in file")
                )
            {

                var currentController = " ";
                var currentAction = " ";
                var currentArea = " ";
                var currentRouteData = RouteTable.Routes.GetRouteData(new HttpContextWrapper(httpContext));

                if (currentRouteData != null)
                {
                    if (currentRouteData.Values["controller"] != null && !String.IsNullOrEmpty(currentRouteData.Values["controller"].ToString()))
                    {
                        currentController = currentRouteData.Values["controller"].ToString();
                    }

                    if (currentRouteData.Values["action"] != null && !String.IsNullOrEmpty(currentRouteData.Values["action"].ToString()))
                    {
                        currentAction = currentRouteData.Values["action"].ToString();
                    }

                    if (currentRouteData.DataTokens["area"] != null && !String.IsNullOrEmpty(currentRouteData.DataTokens["area"].ToString()))
                    {
                        currentArea = currentRouteData.DataTokens["area"].ToString();
                    }
                }

                var ex = Server.GetLastError();
                var controller = new HomeController();
                var admincontroller = new AdminController();
                var routeData = new RouteData();
                routeData.Values["controller"] = "Home";
                routeData.Values["action"] = "Error";
                string err = null;
                //var action = "Index";

                if (ex != null)
                {
                    var httpEx = ex;
                    err = httpEx.Message;
                    HttpSessionStateBase sessionBase = null;
                    if (httpContext != null)
                    {
                        if (httpContext.Session != null)
                        {
                            sessionBase = new HttpSessionStateWrapper(httpContext.Session);
                        }
                    }
                    var httpRequestBase = new HttpRequestWrapper(Context.Request);

                    if (currentArea == "GoX")
                    {
                        routeData.DataTokens.Add("area", currentArea);//["area"] = currentArea;
                        routeData.Values["controller"] = "Admin";
                        routeData.Values["action"] = "Error";
                        ErrorRecordModel.Error_Record(Method.Get_UserId_Admin(httpContext.Request.Cookies, sessionBase), err, httpRequestBase.UserAgent, true, httpEx.Message + "<br />\r\n<br />\r\n" + httpEx.Source + "<br />\r\n<br />\r\n" + httpEx.StackTrace);
                    }
                    else
                    {
                        routeData.Values["controller"] = "Home";
                        routeData.Values["action"] = "Error";
                        ErrorRecordModel.Error_Record(Method.Get_UserId(httpContext.Request.Cookies, sessionBase), err, httpRequestBase.UserAgent, false, httpEx.Message + "<br />\r\n<br />\r\n" + httpEx.Source + "<br />\r\n<br />\r\n" + httpEx.StackTrace);
                    }
                }

                httpContext.ClearError();
                httpContext.Response.Clear();
                httpContext.Response.StatusCode = ex is HttpException ? ((HttpException)ex).GetHttpCode() : 500;
                httpContext.Response.TrySkipIisCustomErrors = true;

                if (currentArea == "GoX")
                {
                    //admincontroller.TempData["err"] = err;
                    admincontroller.ViewData.Model = new HandleErrorInfo(ex, currentController, currentAction);
                    ((IController)admincontroller).Execute(new RequestContext(new HttpContextWrapper(httpContext), routeData));
                }
                else
                {
                    //controller.TempData["err"] = err;
                    controller.ViewData.Model = new HandleErrorInfo(ex, currentController, currentAction);
                    ((IController)controller).Execute(new RequestContext(new HttpContextWrapper(httpContext), routeData));
                }

            }
        }
    }
}