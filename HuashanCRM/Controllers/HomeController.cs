﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Linq;
using System.IO;
using System.Net;
using System.Text;
using System.Security.Cryptography.X509Certificates;
using System.Drawing;
using System.Drawing.Imaging;
using System.Web.Helpers;
using Mith.Areas.GoX.Models;
using Mith.Models;

namespace Mith.Controllers
{
    [ValidateInput(false)]
    public class HomeController : Controller
    {

			public ActionResult Test()
			{
				return View();
			}

        public ActionResult Index()
        {
            DateTime GTM = new DateTime(1970, 1, 1);//宣告一個GTM時間出來
            DateTime Now = DateTime.UtcNow.AddHours(8);//宣告一個目前的時間
            int timeStamp = Convert.ToInt32(((TimeSpan)Now.Subtract(GTM)).TotalSeconds);


            return Redirect("/Gox");
        }

				#region 信用卡

				//國泰 - 送資料給國泰 Step1
        public ActionResult Send_CreditCard(string OrderSN = "", int App_TotalPrice = 0)
        {
            MithDataContext db = new MithDataContext();
            var data = db.Order.FirstOrDefault(f => f.SN == OrderSN && f.Total == App_TotalPrice);
            db.Connection.Close();
            //if (data != null)
            //{
            ViewData["OrderSN"] = OrderSN;
            ViewData["App_TotalPrice"] = App_TotalPrice.ToString();
            //}
            return View();
        }

        //國泰 - 信用卡付款完成導回來 Step2
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult PaymentCompelete(string strRsXml = "")
        {

            //string strRsXml = Request["strRsXml"].ToString();
            strRsXml = strRsXml.Replace("\\'", "\'");
            var filename = AppDomain.CurrentDomain.BaseDirectory + "Log.txt";
            var sw = new System.IO.StreamWriter(filename, true);
            sw.WriteLine(DateTime.UtcNow.AddHours(8).ToString("yyyy/MM/dd HH:mm:ss") + "   " + "Action Start");
            sw.WriteLine(DateTime.UtcNow.AddHours(8).ToString("yyyy/MM/dd HH:mm:ss") + "   " + "接收 strRsXml：" + strRsXml);
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(strRsXml);
                //Get CA Value
                XmlNode node = doc.DocumentElement.SelectSingleNode("CAVALUE");
                string strCA = node.InnerText;
                //Parse XML to Get Order Data
                XmlNode OrderInfo = doc.DocumentElement.SelectSingleNode("ORDERINFO");
                Dictionary<string, string> dicXmlValue = new Dictionary<string, string>();

                foreach (XmlNode xNode in OrderInfo.ChildNodes)
                {
                    dicXmlValue.Add(xNode.Name.ToString(), xNode.InnerText.ToString());
                }
                string strStoreID = dicXmlValue["STOREID"];
                string strOrderNumber = dicXmlValue["ORDERNUMBER"];
                string strAmount = dicXmlValue["AMOUNT"];

                //Parse XML to Get Auth Result
                XmlNode AuthInfo = doc.DocumentElement.SelectSingleNode("AUTHINFO");
                Dictionary<string, string> dicAuthValue = new Dictionary<string, string>();

                foreach (XmlNode xNode in AuthInfo.ChildNodes)
                {
                    dicAuthValue.Add(xNode.Name.ToString(), xNode.InnerText.ToString());
                }

                string strAuthStatus = dicAuthValue["AUTHSTATUS"];
                string strAuthCode = dicAuthValue["AUTHCODE"];

                sw.WriteLine(DateTime.UtcNow.AddHours(8).ToString("yyyy/MM/dd HH:mm:ss") + "   " + "strAuthStatus：" + strAuthStatus);
                sw.WriteLine(DateTime.UtcNow.AddHours(8).ToString("yyyy/MM/dd HH:mm:ss") + "   " + "strAuthCode：" + strAuthCode);

                string Domain = "mith.azurewebsites.net";
                string CUBKEY = "eec85fee81fb036d31471462846fa422";

                //CAVALUE = MD5(Domain + CUBKEY)
                string CAVALUE = Method.CreditCard_MD5(Domain + CUBKEY);
                string RETURL = "http://mith.azurewebsites.net/Home/Get_OrderInfo";
                //回傳的XML
                string strOrderInfo =
                "<?xml version=\'1.0\' encoding=\'UTF-8\'?>" +
                    "<MERCHANTXML>" +
                        "<CAVALUE>" + CAVALUE + "</CAVALUE>" +
                        "<RETURL>" + RETURL + "</RETURL>" +
                    "</MERCHANTXML>";

                sw.WriteLine(DateTime.UtcNow.AddHours(8).ToString("yyyy/MM/dd HH:mm:ss") + "   " + "回傳 strXML：" + strOrderInfo);
                sw.WriteLine(DateTime.UtcNow.AddHours(8).ToString("yyyy/MM/dd HH:mm:ss") + "   " + "Action End");
                return Content(strOrderInfo, "text/xml");
            }
            catch (Exception ex)
            {
                sw.WriteLine(DateTime.UtcNow.AddHours(8).ToString("yyyy/MM/dd HH:mm:ss") + "　　Error:" + ex.Message + "　" + ex.InnerException);
                return Content(ex.ToString());
            }
            finally
            {
                sw.WriteLine("---------------------------------------------------");
                sw.Close();
            }
        }

        ///國泰 - 取得訂單資訊 Step3
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Get_OrderInfo(string strOrderInfo = "")
        {
            try
            {
                strOrderInfo = strOrderInfo.Replace("\\'", "\'");
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(strOrderInfo);
                //Get CA Value
                XmlNode node = doc.DocumentElement.SelectSingleNode("CAVALUE");
                string strCA = node.InnerText;
                //Parse XML to Get Order Data
                XmlNode OrderInfo = doc.DocumentElement.SelectSingleNode("ORDERINFO");
                Dictionary<string, string> dicXmlValue = new Dictionary<string, string>();

                foreach (XmlNode xNode in OrderInfo.ChildNodes)
                {
                    dicXmlValue.Add(xNode.Name.ToString(), xNode.InnerText.ToString());
                }
                string strStoreID = dicXmlValue["STOREID"];
                string strOrderNumber = dicXmlValue["ORDERNUMBER"];

                MithDataContext db = new MithDataContext();

                return Content("strOrderNumber：" + strOrderNumber + "，授權完成");
            }
            catch (Exception ex)
            {
                return Content(ex.ToString());
            }
        }

				#endregion

				//7-11網頁要號，結果
        public ActionResult eshop_Compelete(string orderno = "", string paymentno = "", string validationno = "", int account = 0, string eshopid = "", string eshopsonid = "", string trade_status = "", string describe = "", int daishou_account = 0, string sender = "", string sender_phone = "", string receiver = "", string receiver_phone = "", string receiver_storestatus = "", string receiver_storeid = "", string receiver_storename = "", string return_storestatus = "", string return_storeid = "", string return_storename = "")
        {
            ViewData["orderno"] = orderno; //顧客訂單編號
            ViewData["paymentno"] = paymentno; //寄貨編號
            ViewData["validationno"] = validationno; //驗證碼
            ViewData["account"] = account; //商品總金額，不足左補0，例如： 00200
            ViewData["eshopid"] = eshopid; //數網編號
            ViewData["eshopsonid"] = eshopsonid; //子 eShop 廠商編號

            ViewData["trade_status"] = trade_status; //處理狀態

            //S --處理成功(正常)
            //F --處理失敗(錯誤)
            //ps:要號重複(orderno 已存在, 直接回結果)
            ViewData["describe"] = describe; //若 status 為 F, 此置入錯誤描述

            ViewData["daishou_account"] = daishou_account; //代收金額
            ViewData["sender"] = sender; //寄件人姓名
            ViewData["sender_phone"] = sender_phone; //寄件人電話

            ViewData["receiver"] = receiver; //收件人姓名
            ViewData["receiver_phone"] = receiver_phone; //收件人電話
            ViewData["receiver_storestatus"] = receiver_storestatus; //收件人門市狀態
            //00：正常
            //01：門市店號舊轉新
            //02：門市關轉店
            ViewData["receiver_storeid"] = receiver_storeid; //收件人門市代號
            ViewData["receiver_storename"] = receiver_storename; //收件人門市名稱

            ViewData["return_storestatus"] = return_storestatus; //退貨時，要退回的門市
            //00：正常
            //01：門市店號舊轉新
            //02：門市關轉店
            ViewData["return_storeid"] = return_storeid; //退貨時，要退回的門市
            ViewData["return_storename"] = return_storename; //退貨時，要退回的門市

            string xmlString = "";
            if (trade_status == "S")
            {
                xmlString = "<msgno>1</msgno><describe></describe>";
            }
            else
            {
                xmlString = "<msgno>2</msgno><describe>" + describe + "</describe>";
            }
            return Content(xmlString, "text/xml");
        }

        public ActionResult Error()
        {
            return View();
        }

        public ActionResult Complete()
        {
            ViewBag.Title = "傳送成功";
            TempData["msg"] = "傳送成功，感謝您的來信。";
            return View("~/Views/Login/Complete.cshtml");
        }

        public ActionResult Ajax_Area(int city, bool all)
        {
            return Content(CityArea.Get_Area_Ajax(city, 0, all));
        }
    }
}
