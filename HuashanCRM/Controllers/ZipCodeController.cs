﻿//using System;
//using System.Collections.Generic;
//using System.Data;
//using System.IO;
//using System.Linq;
//using System.Web;
//using System.Web.Configuration;
//using System.Web.Mvc;
//using Mith.Infrastructure.Helpers;
//using Mith.Models;
//using Newtonsoft.Json;
//using Newtonsoft.Json.Linq;

//namespace Mith.Controllers
//{
//    public class ZipCodeController : Controller
//    {
//        public ActionResult Index()
//        {
//            return View();
//        }

//        private string fileSavedPath = "~/upload/";

//        [HttpPost]
//        public ActionResult Upload(HttpPostedFileBase file)
//        {
//            JObject jo = new JObject();
//            string result = string.Empty;

//            if (file == null)
//            {
//                jo.Add("Result", false);
//                jo.Add("Msg", "請上傳檔案!");
//                result = JsonConvert.SerializeObject(jo);
//                return Content(result, "application/json");
//            }
//            if (file.ContentLength <= 0)
//            {
//                jo.Add("Result", false);
//                jo.Add("Msg", "請上傳正確的檔案.");
//                result = JsonConvert.SerializeObject(jo);
//                return Content(result, "application/json");
//            }

//            string fileExtName = Path.GetExtension(file.FileName).ToLower();

//            if (!fileExtName.Equals(".xlsx", StringComparison.OrdinalIgnoreCase))
//            {
//                jo.Add("Result", false);
//                jo.Add("Msg", "請上傳 .xlsx格式的檔案");
//                result = JsonConvert.SerializeObject(jo);
//                return Content(result, "application/json");
//            }

//            try
//            {
//                var uploadResult = this.FileUploadHandler(file);

//                jo.Add("Result", !string.IsNullOrWhiteSpace(uploadResult));
//                jo.Add("Msg", !string.IsNullOrWhiteSpace(uploadResult) ? uploadResult : "");

//                result = JsonConvert.SerializeObject(jo);
//            }
//            catch (Exception ex)
//            {
//                jo.Add("Result", false);
//                jo.Add("Msg", ex.Message);
//                result = JsonConvert.SerializeObject(jo);
//            }
//            return Content(result, "application/json");
//        }

//        /// <summary>
//        /// Files the upload handler.
//        /// </summary>
//        /// <param name="file">The file.</param>
//        /// <returns></returns>
//        /// <exception cref="System.ArgumentNullException">file;上傳失敗：沒有檔案！</exception>
//        /// <exception cref="System.InvalidOperationException">上傳失敗：檔案沒有內容！</exception>
//        private string FileUploadHandler(HttpPostedFileBase file)
//        {
//            string result;

//            if (file == null)
//            {
//                throw new ArgumentNullException("file", "上傳失敗：沒有檔案！");
//            }
//            if (file.ContentLength <= 0)
//            {
//                throw new InvalidOperationException("上傳失敗：檔案沒有內容！");
//            }

//            try
//            {
//                string virtualBaseFilePath = Url.Content(fileSavedPath);
//                string filePath = HttpContext.Server.MapPath(virtualBaseFilePath);

//                if (!Directory.Exists(filePath))
//                {
//                    Directory.CreateDirectory(filePath);
//                }

//                string newFileName = string.Concat(
//                    DateTime.Now.ToString("yyyyMMddHHmmssfff"),
//                    Path.GetExtension(file.FileName).ToLower());

//                string fullFilePath = Path.Combine(Server.MapPath(fileSavedPath), newFileName);
//                file.SaveAs(fullFilePath);

//                result = newFileName;
//            }
//            catch (Exception ex)
//            {
//                throw;
//            }
//            return result;
//        }

//        [HttpPost]
//        public ActionResult Import(string savedFileName)
//        {
//            var jo = new JObject();
//            string result;

//            try
//            {
//                var fileName = string.Concat(Server.MapPath(fileSavedPath), "/", savedFileName);

//                var import = new List<ImportClass>();

//                var helper = new ImportDataHelper();
//                var checkResult = helper.CheckImportData(fileName, import);

//                jo.Add("Result", checkResult.Success);
//                jo.Add("Msg", checkResult.Success ? string.Empty : checkResult.ErrorMessage);

//                if (checkResult.Success)
//                {
//                    //儲存匯入的資料
//                    helper.SaveImportData(import);
//                }
//                result = JsonConvert.SerializeObject(jo);
//            }
//            catch (Exception ex)
//            {
//                throw;
//            }
//            return Content(result, "application/json");
//        }


//    }
//}