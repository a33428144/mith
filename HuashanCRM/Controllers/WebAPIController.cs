﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Reflection;
using System.Drawing;
using System.Drawing.Imaging;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Linq;
using System.IO;
using System.Net;
using System.Text;
using System.Security.Cryptography.X509Certificates;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Security;
using System.Security.Cryptography;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Mith.Models;
using Mith.Areas.GoX.Models;
using HtmlAgilityPack;

namespace Mith.Controllers
{

	public class WebAPIController : Controller
	{
		//public ActionResult Test()
		//{
		//	MithDataContext db = new MithDataContext();

		//	var data = db.OrderDetail;

		//	foreach (var item in data)
		//	{
		//		var Order = db.Order.FirstOrDefault(f => f.Id == item.OrderId);
		//		if (Order != null)
		//		{
		//			int Direction = Order.Direction;
		//			int CashFlowType = Order.CashFlowType;

		//			item.CalculateTotal = Method.Get_CalculateTotal(Direction, CashFlowType, item.BuyTotal);
		//			db.SubmitChanges();
		//		}
		//	}
		//	db.Connection.Close();
		//	return Content("aaa");
		//}

		#region  7 - 11
		//取得用戶選擇的7-11店家資訊
		[ValidateInput(false)]
		[HttpPost]
		public ActionResult Get_SelectShop(string storeid = "", string storename = "", string storeaddress = "", int servicetype = 0, string tempvar = "")
		{
			try
			{
				int Search = tempvar.IndexOf("Company");

				//從網站來
				if (Search > 0)
				{
					tempvar += "?ReturnStoreId=" + storeid + "&ReturnStoreName=" + storename + "&ReturnStoreAddress=" + storeaddress + "&From=711";
					return Redirect(tempvar);
				}
				else
				{
					return Redirect("soohoobook.mith.store://?storeid=" + @storeid + "&storename=" + storename + "&storeaddress=" + storeaddress);
				}
			}
			catch (Exception e)
			{
				return Content(e.ToString());
			}
		}

		//廠商去7-11寄貨時銷案
		[HttpPost]
		[ValidateInput(false)]
		public ActionResult Get_711_SendGoods()
		{
			var filename = AppDomain.CurrentDomain.BaseDirectory + "Seven.txt";
			var sw = new System.IO.StreamWriter(filename, true);
			MithDataContext db = new MithDataContext();
			DateTime Now = DateTime.UtcNow;
			try
			{
				sw.WriteLine(DateTime.UtcNow.AddHours(8).ToString("yyyy/MM/dd HH:mm:ss") + "   " + "Get_711_SendGoods");
				sw.WriteLine(DateTime.UtcNow.AddHours(8).ToString("yyyy/MM/dd HH:mm:ss") + "   " + "Action Start");
				sw.WriteLine("");
				string xml = "";
				//非指定參數的方式，接收值
				if (Request.InputStream != null)
				{
					StreamReader stream = new StreamReader(Request.InputStream);
					string Result = stream.ReadToEnd(); //回傳結果
					xml = HttpUtility.UrlDecode(Result);
					sw.WriteLine(DateTime.UtcNow.AddHours(8).ToString("yyyy/MM/dd HH:mm:ss") + "   " + "接收:" + xml);
					sw.WriteLine("");
				}
				XmlDocument doc = new XmlDocument();
				doc.LoadXml(xml);

				XmlNodeList xmlList = doc.SelectNodes("/OLTP/AP/Detail");
				foreach (XmlNode node in xmlList)
				{
					string PackageSN = node["OL_Code_2"].InnerText.Substring(0, 8); //交貨便代號前8碼

					var OrderDetail = db.OrderDetail.Where(f => f.PackageSN.Substring(0, 8) == PackageSN);

					XmlElement status = doc.CreateElement("Status");
					XmlElement description = doc.CreateElement("Description");

					//有此貨運編號
					if (OrderDetail.Any())
					{
						status.InnerText = "S";
						node.AppendChild(status);

						description.InnerText = "";
						node.AppendChild(description);
					}
					else
					{
						status.InnerText = "F";
						node.AppendChild(status);

						description.InnerText = "交貨便代號不存在";
						node.AppendChild(description);
					}

				}
				sw.WriteLine(DateTime.UtcNow.AddHours(8).ToString("yyyy/MM/dd HH:mm:ss") + "   " + "回覆:" + doc.InnerXml);
				sw.WriteLine("");
				return Content(doc.InnerXml, "text/xml");
			}
			catch (Exception ex)
			{
				return Content(ex.ToString());
			}
			finally
			{
				db.Connection.Close();
				sw.WriteLine("---------------------------------------------------");
				sw.WriteLine("");
				sw.Close();
			}
		}

		//消費者去7-11取貨時銷案
		[HttpPost]
		[ValidateInput(false)]
		public ActionResult Get_711_ReceiveGoods()
		{
			var filename = AppDomain.CurrentDomain.BaseDirectory + "Seven.txt";
			var sw = new System.IO.StreamWriter(filename, true);
			MithDataContext db = new MithDataContext();
			DateTime Now = DateTime.UtcNow;
			try
			{
				sw.WriteLine(DateTime.UtcNow.AddHours(8).ToString("yyyy/MM/dd HH:mm:ss") + "   " + "Get_711_ReceiveGoods");
				sw.WriteLine(DateTime.UtcNow.AddHours(8).ToString("yyyy/MM/dd HH:mm:ss") + "   " + "Action Start");
				sw.WriteLine("");
				string xml = "";
				//非指定參數的方式，接收值
				if (Request.InputStream != null)
				{
					StreamReader stream = new StreamReader(Request.InputStream);
					string Result = stream.ReadToEnd(); //回傳結果
					xml = HttpUtility.UrlDecode(Result);
					sw.WriteLine(DateTime.UtcNow.AddHours(8).ToString("yyyy/MM/dd HH:mm:ss") + "   " + "接收:" + xml);
					sw.WriteLine("");
				}
				XmlDocument doc = new XmlDocument();
				doc.LoadXml(xml);

				XmlNodeList xmlList = doc.SelectNodes("/OLTP/AP/Detail");
				foreach (XmlNode node in xmlList)
				{
					string ShipmentNo = node["OL_Code_2"].InnerText.Substring(0, 8); //配送編號

					var OrderDetail = db.OrderDetail.Where(f => f.ShipmentSN == ShipmentNo && f.LogisticsStatus == 5);

					XmlElement status = doc.CreateElement("Status");
					XmlElement description = doc.CreateElement("Description");

					//有此貨運編號
					if (OrderDetail.Any())
					{
						status.InnerText = "S";
						node.AppendChild(status);

						description.InnerText = "";
						node.AppendChild(description);

						OrderDetail.ForEach(a => a.LogisticsStatus = 6); //已取貨
						OrderDetail.ForEach(a => a.ReceiveGoodsTime = Now); //取貨時間
						db.SubmitChanges();

					}
					else
					{
						status.InnerText = "F";
						node.AppendChild(status);

						description.InnerText = "配送編號不存在";
						node.AppendChild(description);
					}

				}
				sw.WriteLine(DateTime.UtcNow.AddHours(8).ToString("yyyy/MM/dd HH:mm:ss") + "   " + "回覆:" + doc.InnerXml);
				sw.WriteLine("");
				return Content(doc.InnerXml, "text/xml");
			}
			catch (Exception ex)
			{
				return Content(ex.ToString());
			}
			finally
			{
				db.Connection.Close();
				sw.WriteLine("---------------------------------------------------");
				sw.WriteLine("");
				sw.Close();
			}
		}

		#endregion

		#region 黑貓
		//爬黑貓網站，取得物流狀態
		public ActionResult Get_BlackCat_ReceiveGoods()
		{
			MithDataContext db = new MithDataContext();

			try
			{
				var OrderDetail = db.OrderDetail.Where(w => w.LogisticsSN == "0086225746");

				foreach (var item in OrderDetail)
				{
					WebClient client = new WebClient();
					MemoryStream ms = new MemoryStream(client.DownloadData(
			"http://www.t-cat.com.tw/inquire/TraceDetail.aspx?ReturnUrl=Trace.aspx&BillID=" + item.LogisticsSN));

					HtmlDocument doc = new HtmlDocument();
					doc.Load(ms, Encoding.UTF8);

					foreach (HtmlNode table in doc.DocumentNode.SelectNodes("//table[1]/tr[2]"))
					{
						string LogisticsStatus = table.SelectSingleNode("td[2]").InnerText.Replace(" ", "");

						switch (LogisticsStatus)
						{
							case "順利送達":
								DateTime ReceiveGoodsTime = DateTime.Parse(table.SelectSingleNode("td[3]").InnerText);

								item.LogisticsStatus = 6; //已取貨
								item.ArriveInTime = ReceiveGoodsTime.AddHours(-8); //取貨時間
								item.ReceiveGoodsTime = ReceiveGoodsTime.AddHours(-8); //到貨時間

								break;

							case "取消取件":
							case "調查處理中":
								item.LogisticsStatus = -888;
								break;

						}
						db.SubmitChanges();
					}
					doc = null;
					client.Dispose();
					ms.Close();

				}
				return Json(true, JsonRequestBehavior.AllowGet);
			}
			catch (Exception e)
			{
				return Content("Error");
			}
			finally
			{
				db.Connection.Close();
			}
		}
		#endregion

		#region App 錯誤Log

		//私訊資料
		[HttpPost]
		public ActionResult Get_AppErrorList(string Version = "")
		{
			try
			{
				object obj = WebApiMethod.Get_AppErrorData(Version);
				if (obj != null)
				{
					return Json(obj, JsonRequestBehavior.AllowGet);
				}
				else
				{
					return Json(false, JsonRequestBehavior.AllowGet);
				}
			}
			catch (Exception e)
			{
				return Content("Error");
			};
		}

		//新增文章留言
		[HttpPost]
		[ValidateInput(false)]
		public ActionResult Insert_AppError(string Version = "", string Message = "")
		{
			MithDataContext db = new MithDataContext();
			try
			{
				var item = new App_ErrorRecord()
				{
					Version = Version,
					Message = Message,
					CreateTime = DateTime.UtcNow
				};
				db.App_ErrorRecord.InsertOnSubmit(item);
				db.SubmitChanges();
				return Json(true, JsonRequestBehavior.AllowGet);

			}
			catch (Exception e)
			{
				return Content("Error");
			}
			finally
			{
				db.Connection.Close();
			}
		}

		#endregion

		#region 排程Schedule

		//Azure執行排程 從國泰取得虛擬帳號已繳費清單
		public ActionResult Schedule_Get_AtmPamentList()
		{
			//Call SSL方法
			System.Net.ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(RemoteCertificateValidationCallback);

			try
			{
				//回傳結果
				string Result = null;
				//過去三小時
				string LastOneHours = DateTime.UtcNow.AddHours(7).ToString("yyyyMMddHHmmss");
				string Now = DateTime.UtcNow.AddHours(8).ToString("yyyyMMddHHmmss");

				//國泰世華 - 取得虛擬帳號繳費清單API
				string API = "https://www.myb2b.com.tw/securities/tx10d0_txt.asp";

				//發出 Request
				HttpWebRequest request = HttpWebRequest.Create(API) as HttpWebRequest;

				//要帶的參數
				string Param =
										"cust_id=276599203002&" +
										"cust_pwd=23690013&" +
										"cust_nickname=23644150&" +
										"acno=030035005401&" +
										 "from_date=" + LastOneHours.Substring(0, 8) + "&" +
										"to_date=" + Now.Substring(0, 8) + "&" +
										"from_time=" + LastOneHours.Substring(8, 6) + "&" +
										"to_time=" + Now.Substring(8, 6) + "&" +
										"xml=Y&" +
										"txdate8=Y";

				//將變數進行編碼
				byte[] bs = Encoding.ASCII.GetBytes(Param);
				request.Method = "POST"; // POST方式
				request.KeepAlive = true; //保持連線
				request.ProtocolVersion = HttpVersion.Version10;
				request.ContentType = "application/x-www-form-urlencoded";
				request.ContentLength = bs.Length;

				using (Stream reqStream = request.GetRequestStream())
				{
					reqStream.Write(bs, 0, bs.Length);
				}

				//API Response
				using (WebResponse response = request.GetResponse())
				{
					//跨網域
					Response.AppendHeader("Access-Control-Allow-Origin", "*");

					//回傳結果轉Big5
					StreamReader sr = new StreamReader(response.GetResponseStream(), Encoding.GetEncoding("Big5"));

					//讀取API回傳結果
					Result = sr.ReadToEnd();

					XmlDocument doc = new XmlDocument();

					//回傳結果string轉Xml
					doc.LoadXml(Result);

					//轉時區
					IFormatProvider CultureInfo = new System.Globalization.CultureInfo("zh-TW", true);

					XmlNodeList xmlList = doc.SelectNodes("/TX10D0/TXDETAIL");
					foreach (XmlNode node in xmlList)
					{
						string VirtualAccount = node["MEMO1"].InnerText.Substring(0, 16); //虛擬帳號
						int Total = int.Parse(node["AMOUNT"].InnerText);   //交易金額
						string BankCode = node["MEMO2"].InnerText.Substring(0, 3); //銀行代號
						string BankAccount = node["MEMO2"].InnerText.Substring(3, 16);  //銀行帳號
						string AccountName = node["ACCNAME"].InnerText; //帳號名稱
						DateTime PaymentTime = DateTime.ParseExact
						(
							//交易日期 + 交易時間
						 node["TX_DATE"].InnerText + node["TX_TIME"].InnerText,
						"yyyyMMddHHmmss",
						CultureInfo
						);
						WebApiMethod.Update_AtmPament(VirtualAccount, Total, PaymentTime, BankCode, BankAccount, AccountName);
					}

					//關閉StreamReader
					sr.Close();

				}

				return Json(true, JsonRequestBehavior.AllowGet);
				//return Content(STATUS);
			}
			catch (Exception e)
			{
				return Content(e.ToString());
			}
		}

		//Azure執行排程 - 虛擬帳號相關
		public ActionResult Schedule_About_AtmPayment()
		{
			MithDataContext db = new MithDataContext();
			try
			{
				//少於現在一小時
				DateTime Start = DateTime.Parse(DateTime.UtcNow.AddHours(7).ToString("yyyy/MM/dd HH:") + "30:00");

				//現在
				DateTime End = DateTime.Parse(DateTime.UtcNow.AddHours(8).ToString("yyyy/MM/dd HH:") + "30:00");

				#region 取得虛擬帳號48小時未付款，寄提醒信

				//狀態：未結單，繳費狀態：未繳費
				var data = db.Order_API_View.Where(w =>
						w.Status == 1 &&
						w.CashFlowStatus == 1 &&
						w.CreateTime.Value.AddDays(2) >= Start &&
						w.CreateTime.Value.AddDays(2) < End && w.OrderId == 1);

				foreach (var item in data)
				{
					var OrderDetail = db.OrderDetail_Index_View.Where(w => w.OrderId == item.OrderId);

					string BuyGoodsStr = "\n";

					foreach (var item2 in OrderDetail)
					{
						BuyGoodsStr += "【" + item2.Goods_SN + "】" + item2.Goods_Name + "（" + item2.BuySize + " × " + item2.BuyQuantity + "）\n";
					}


					string EmailContent = HttpUtility.HtmlDecode("您好,提醒您有一筆訂單尚未付款\n\n下單時間：" + item.CreateTime.Value.ToString("yyyy/MM/dd HH:mm") + "\n訂單編號：" + item.SN + "\n付款方式：ATM轉帳" + "\n訂購商品：" + BuyGoodsStr + "\n請於 " + item.CreateTime.Value.AddDays(3).ToString("yyyy/MM/dd HH:mm:ss") + " 前匯款至以下ATM帳戶\n===================================================================" + "\n銀行代號：013(國泰世華)" + "\n虛擬帳號：" + item.VirtualAccount + "\n金　　額：" + String.Format("{0:n0}", item.BuyTotal) + "元\n===================================================================" + "\n MiTH不會請您變更付款方式" + "\n若有任何問題，可連繫MiTH客服：mith@mix-with.com\n");

					//發送Email
					Task t1 = Task.Run(() =>
					{
						Method.Send_Mail(item.Volunteers_Email, "MiTH App 尚未付款通知 [訂單編號" + item.SN + "]", EmailContent);
					});
				}
				#endregion

				#region 取得虛擬帳號72小時未付款，取消訂單

				//狀態：未結單，繳費狀態：未繳費
				var data2 = db.Order_Index_View.Where(w =>
						w.Status == 1 &&
						w.CashFlowStatus == 1 &&
						w.CreateTime.Value.AddDays(3) >= Start &&
						w.CreateTime.Value.AddDays(3) < End);

				foreach (var item in data2)
				{
					//取消訂單
					Cancel_Order(item.Id);
				}

				#endregion

				return Json(true, JsonRequestBehavior.AllowGet);
			}
			catch (Exception e)
			{
				return Content(e.ToString());
			}
			finally
			{
				db.Connection.Close();
			}
		}

		//Azure執行排程 - 刪除未成立的訂單
		public ActionResult Schedule_Delete_Order()
		{
			MithDataContext db = new MithDataContext();
			try
			{
				//狀態= 未成立
				var data = db.Order.Where(w => w.Status == -1);
				foreach (var item in data)
				{
					WebApiMethod.Delete_Order(item.Id);
				}
				return Json(true, JsonRequestBehavior.AllowGet);
				//return Content(STATUS);
			}
			catch (Exception e)
			{
				return Content(e.ToString());
			}
			finally
			{
				db.Connection.Close();
			}
		}

		//Azure執行排程 - 取得未結單的訂單，符合條件的轉為結單
		public ActionResult Schedule_Change_OrderStaus()
		{
			MithDataContext db = new MithDataContext();
			try
			{
				//正物流訂單 狀態 = 未成立
				var data = db.Order.Where(w => w.Status == 1 && w.RelativeId == null);
				foreach (var item in data)
				{
					//訂單底下是否有還有【未出貨】跟【配送中】的商品
					var NotShippingGoods = db.OrderDetail.Where(w => w.OrderId == item.Id && w.LogisticsStatus != 6 && w.LogisticsStatus != 7);

					//沒有【未出貨】跟【配送中】的商品
					if (!NotShippingGoods.Any() && item.Id == 276)
					{
						//取得最後一個商品的取貨時間
						DateTime Last_ReceiveGoodsTime = db.OrderDetail_Edit_View.OrderByDescending(o => o.ReceiveGoodsTime).FirstOrDefault(f => f.OrderId == item.Id && f.LogisticsStatus == 6).ReceiveGoodsTime.Value;

						//現在少一小時
						DateTime Start = DateTime.Parse(DateTime.UtcNow.AddHours(7).ToString("yyyy/MM/dd HH:") + "00:00");

						//現在
						DateTime End = DateTime.Parse(DateTime.UtcNow.AddHours(8).ToString("yyyy/MM/dd HH:") + "00:00");

						//計算現在時間 - 出貨時間
						TimeSpan tsDay = DateTime.UtcNow.AddHours(8) - Last_ReceiveGoodsTime;

						//相減天數
						int Days = (int)tsDay.Days;

						//相減天數 >= 10
						if (Days >= 10)
						{
							//最後取貨時間+10天 >= 現在時間少一小時 && 最後取貨時間+10天 < 現在時間
							if (Last_ReceiveGoodsTime.AddDays(10) >= Start && Last_ReceiveGoodsTime.AddDays(10) < End)
							{
								item.Status = 2; //結單
								item.FinalTime = DateTime.UtcNow; //結單時間
								db.SubmitChanges();

								//歐付寶 - 開立電子發票
								WebApiMethod.Generate_Invoice(item.Id);
							}
						}
					}
				}
				return Json(true, JsonRequestBehavior.AllowGet);
			}
			catch (Exception e)
			{
				return Content(e.ToString());
			}
			finally
			{
				db.Connection.Close();
			}
		}

		//Azure執行排程 - 從7-11 FTP取得【商品到店 CPPS】XML
		public ActionResult Schedule_Get_711_GoodsArriveIn()
		{
			MithDataContext db = new MithDataContext();

			WebClient request = new WebClient();
			DateTime Now = DateTime.UtcNow.AddHours(8);
			string No = "01";


			switch (Now.Hour)
			{
				case 10: //上午10點
					No = "01";
					break;
				case 14://下午2點
					No = "04";
					break;
				case 22://晚上10點
					No = "05";
					break;
				default:
					No = "01";
					break;
			}

			string FTP = "ftp://waws-prod-hk1-007.ftp.azurewebsites.windows.net/" + "851" + Now.ToString("yyyyMMdd") + No + ".CPPS.xml";
			string Account = @"mith\mith";
			string Password = "qwer1234";
			request.Credentials = new NetworkCredential(Account, Password);

			try
			{
				//if (No != "00")
				//{
				byte[] newFileData = request.DownloadData(FTP);
				//讀取xml裡面的文字
				string xml = System.Text.Encoding.UTF8.GetString(newFileData);

				XmlDocument doc = new XmlDocument();
				doc.LoadXml(xml);

				XmlNodeList xmlList = doc.SelectNodes("/PPSDoc/DocContent/PPS");
				foreach (XmlNode node in xmlList)
				{
					string PaymentNo = node["PaymentNo"].InnerText; //交貨便代號前8碼
					string ShipmentNo = node["ShipmentNo"].InnerText; //出貨單編號
					string StoreDate = node["StoreDate"].InnerText.Replace("-", "/"); //到店日期
					string StoreTime = node["StoreTime"].InnerText.Substring(0, 2) + ":" + node["StoreTime"].InnerText.Substring(2, 2) + ":" + node["StoreTime"].InnerText.Substring(4, 2); //到店時間

					DateTime ArriveInTime = DateTime.Parse(StoreDate + " " + StoreTime);

					//物流狀態 = 已出貨
					List<OrderDetail> OrderDetail = db.OrderDetail.Where(f => f.PackageSN.Substring(0, 8) == PaymentNo && f.LogisticsStatus == 2).ToList();

					if (OrderDetail.Any())
					{
						var Goods = db.Goods.FirstOrDefault(f => f.Id == OrderDetail[0].GoodsId);
						var Order = db.Order.FirstOrDefault(f => f.Id == OrderDetail[0].OrderId);
						var Volunteers = db.Volunteers.FirstOrDefault(f => f.Id == Order.VolunteersId);

						string OrderSN = Order.SN;
						string PackageSN = OrderDetail[0].PackageSN;
						string StoreName = Order.ReceiverStoreName;
						string StoreAddress = Order.ReceiverStoreAddress;
						string Volunteers_Name = Volunteers.RealName;
						string Volunteers_Email = Volunteers.Email;

						OrderDetail.ForEach(a => a.LogisticsStatus = 5);
						OrderDetail.ForEach(a => a.ShipmentSN = ShipmentNo);
						OrderDetail.ForEach(a => a.ArriveInTime = ArriveInTime.AddHours(-8));
						db.SubmitChanges();

						string EmailContent = HttpUtility.HtmlDecode("您好，\n已將訂購商品送到指定的門市，\n請於" + ArriveInTime.AddDays(7).ToString("yyyy/MM/dd HH:mm") + " 前至門市取貨，\n\n本次訂單編號：" + OrderSN + "\n配送編號：" + PackageSN + "\n取貨門市：" + StoreName + " (" + StoreAddress + ")\n取貨會員：" + Volunteers_Name.Substring(0, Volunteers_Name.Length - 1) + "*" + "\n===================================================================\nMiTH不會請您變更付款方式" + "\n若有任何問題，可連繫MiTH客服：mith@mix-with.com\n");

						Task t1 = Task.Run(() =>
						{
							Method.Send_Mail(Volunteers_Email, "MiTH App 商品已送達門市通知 [訂單編號" + OrderSN + "]", EmailContent);
						});
					}


					//物流狀態 = 退回門市中
					List<OrderDetail> ReturnOrderDetail = db.OrderDetail.Where(f => f.PackageSN.Substring(0, 8) == PaymentNo && f.LogisticsStatus == 7).ToList();

					if (ReturnOrderDetail.Any())
					{
						var Order = db.Order.FirstOrDefault(f => f.Id == ReturnOrderDetail[0].OrderId);
						var Company = db.Company.FirstOrDefault(f => f.Id == ReturnOrderDetail[0].CompanyId);

						string OrderSN = Order.SN;
						string PackageSN = ReturnOrderDetail[0].PackageSN;
						string StoreName = ReturnOrderDetail[0].ReturnStoreName;
						string StoreAddress = ReturnOrderDetail[0].ReturnStoreAddress;
						string Company_Email = Company.ContactEmail;

						ReturnOrderDetail.ForEach(a => a.LogisticsStatus = 9); //已退回門市
						ReturnOrderDetail.ForEach(a => a.ShipmentSN = ShipmentNo);
						ReturnOrderDetail.ForEach(a => a.ArriveInTime = ArriveInTime.AddHours(-8));
						db.SubmitChanges();

						string EmailContent = HttpUtility.HtmlDecode("您好,\n因消費者未於7天內取貨，包裹已退回您指定的門市，\n\n本次訂單編號：" + OrderSN + "\n配送編號：" + PackageSN + "\n退回門市：" + StoreName + " (" + StoreAddress + ")");

						Task t2 = Task.Run(() =>
						{
							Method.Send_Mail(Company_Email, "MiTH App 商品已退回門市通知 [訂單編號" + OrderSN + "]", EmailContent);
						});
					}
				}
				//}
				return Json(true, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				return Content(ex.ToString());
			}
			finally
			{
				db.Connection.Close();
			}
		}

		//Azure執行排程 - 從7-11 FTP取得【預計退貨檔 CERT】XML
		public ActionResult Schedule_Get_711_ReadyReturn()
		{
			MithDataContext db = new MithDataContext();
			OrderModel data = new OrderModel();
			WebClient request = new WebClient();
			string FileName = "851" + DateTime.UtcNow.AddHours(8).ToString("yyyyMMdd") + "01.CERT.xml";
			string FTP = "ftp://waws-prod-hk1-007.ftp.azurewebsites.windows.net/" + FileName;
			string Account = @"mith\mith";
			string Password = "qwer1234";
			request.Credentials = new NetworkCredential(Account, Password);
			int New_OrderId = 0;
			try
			{
				byte[] newFileData = request.DownloadData(FTP);
				//讀取xml裡面的文字
				string xml = System.Text.Encoding.UTF8.GetString(newFileData);

				XmlDocument doc = new XmlDocument();
				doc.LoadXml(xml);

				XmlNodeList xmlList = doc.SelectNodes("/DCReturnAdviceDoc/DocContent/DCReturnAdvice");
				foreach (XmlNode node in xmlList)
				{
					string PaymentNo = node["PaymentNo"].InnerText; //交貨便代號前8碼
					string ShipmentNo = node["ShipmentNo"].InnerText; //出貨單編號

					//物流狀態 = 已出貨
					var OrderDetail = db.OrderDetail.Where(f => f.PackageSN.Substring(0, 8) == PaymentNo && f.LogisticsStatus == 2);

					var ReturnOrderDetail = db.OrderDetail.Where(f => f.PackageSN.Substring(0, 8) == PaymentNo && f.LogisticsStatus == 7);

					//有未取貨的商品，且還沒新增退貨資料
					if (OrderDetail.Any() && !ReturnOrderDetail.Any())
					{
						int i = 0;
						int TotalPrice = 0;
						foreach (var item in OrderDetail)
						{
							#region 迴圈第一次，新增退貨的訂單主檔
							if (i == 0)
							{
								int OrderId = item.OrderId;
								//新增7-11退貨的訂單主檔
								New_OrderId = data.Insert_ReturnGoods_Order(2, 0, 0, null, 0, " 消費者未取貨，退回原門市", OrderId, 0);
							}
							#endregion

							#region 新增退貨的訂單細節
							int BuyTotal = item.BuyPrice * item.BuyQuantity;
							int CashFlowType = db.Order.FirstOrDefault(f => f.Id == New_OrderId).CashFlowType;							

							var New_OrderDetail = new OrderDetail()
							{
								OrderId = New_OrderId,
								GoodsId = item.GoodsId,
								GoodsType = item.GoodsType,
								CompanyId = item.CompanyId,
								BrandId = item.BrandId,
								BuySize = item.BuySize,
								BuyQuantity = item.BuyQuantity,
								BuyPrice = item.BuyPrice,
								BuyTotal = BuyTotal,
								CalculateTotal = Method.Get_CalculateTotal(CashFlowType, BuyTotal),
								LogisticsSN = item.LogisticsSN,
								PackageSN = item.PackageSN,
								ShipmentSN = ShipmentNo,
								LogisticsCompany = item.LogisticsCompany,
								LogisticsStatus = 8, //退回門市中
								ReturnStoreId = item.ReturnStoreId,
								ReturnStoreName = item.ReturnStoreName,
								ReturnStoreAddress = item.ReturnStoreAddress,
								CreateTime = DateTime.UtcNow,
								UpdateTime = DateTime.UtcNow,
								LastUserId = 0,
							};
							db.OrderDetail.InsertOnSubmit(New_OrderDetail);
							db.SubmitChanges();

							TotalPrice += BuyTotal;
							#endregion
							i++;
						}
						#region 計算訂單總和
						var New_Order = db.Order.FirstOrDefault(f => f.Id == New_OrderId);
						if (New_Order != null)
						{
							New_Order.Total = TotalPrice;
							db.SubmitChanges();
						}

						#endregion
					}
				}

				return Json(true, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				//刪掉訂單
				WebApiMethod.Delete_Order(New_OrderId);
				return Content(ex.ToString());
			}
			finally
			{
				db.Connection.Close();
			}
		}

		#endregion

		#region 信用卡
		//國泰 - 授權完成 Step1（Web）
		[HttpPost]
		[ValidateInput(false)]
		public ActionResult Web_PaymentCompelete(string strRsXml = "")
		{

			//string strRsXml = Request["strRsXml"].ToString();
			strRsXml = strRsXml.Replace("\\'", "\'");
			var filename = AppDomain.CurrentDomain.BaseDirectory + "Log.txt";
			var sw = new System.IO.StreamWriter(filename, true);
			sw.WriteLine(DateTime.UtcNow.AddHours(8).ToString("yyyy/MM/dd HH:mm:ss") + "   " + "Action Start");
			sw.WriteLine(DateTime.UtcNow.AddHours(8).ToString("yyyy/MM/dd HH:mm:ss") + "   " + "接收 strRsXml：" + strRsXml);
			try
			{
				XmlDocument doc = new XmlDocument();
				doc.LoadXml(strRsXml);

				//Parse XML to Get Order Data
				XmlNode OrderInfo = doc.DocumentElement.SelectSingleNode("ORDERINFO");
				Dictionary<string, string> XmlValue = new Dictionary<string, string>();

				foreach (XmlNode xNode in OrderInfo.ChildNodes)
				{
					XmlValue.Add(xNode.Name.ToString(), xNode.InnerText.ToString());
				}
				string strStoreID = XmlValue["STOREID"];
				string strOrderNumber = XmlValue["ORDERNUMBER"];
				string strAmount = XmlValue["AMOUNT"];

				//Parse XML to Get Auth Result
				XmlNode AuthInfo = doc.DocumentElement.SelectSingleNode("AUTHINFO");
				Dictionary<string, string> dicAuthValue = new Dictionary<string, string>();

				foreach (XmlNode xNode in AuthInfo.ChildNodes)
				{
					dicAuthValue.Add(xNode.Name.ToString(), xNode.InnerText.ToString());
				}

				string strAuthStatus = dicAuthValue["AUTHSTATUS"];
				string strAuthCode = dicAuthValue["AUTHCODE"];

				sw.WriteLine(DateTime.UtcNow.AddHours(8).ToString("yyyy/MM/dd HH:mm:ss") + "   " + "strAuthStatus：" + strAuthStatus);
				sw.WriteLine(DateTime.UtcNow.AddHours(8).ToString("yyyy/MM/dd HH:mm:ss") + "   " + "strAuthCode：" + strAuthCode);

				string Domain = "mith.azurewebsites.net";
				string CUBKEY = "eec85fee81fb036d31471462846fa422";

				//CAVALUE = MD5(Domain + CUBKEY)
				string CAVALUE = Method.CreditCard_MD5(Domain + CUBKEY);
				string RETURL = "http://mith.azurewebsites.net/WebAPI/Get_CreditCardInfo";
				//回傳的XML
				string strOrderInfo =
				"<?xml version=\'1.0\' encoding=\'UTF-8\'?>" +
						"<MERCHANTXML>" +
								"<CAVALUE>" + CAVALUE + "</CAVALUE>" +
								"<RETURL>" + RETURL + "</RETURL>" +
						"</MERCHANTXML>";

				sw.WriteLine(DateTime.UtcNow.AddHours(8).ToString("yyyy/MM/dd HH:mm:ss") + "   " + "回傳 strXML：" + strOrderInfo);
				sw.WriteLine(DateTime.UtcNow.AddHours(8).ToString("yyyy/MM/dd HH:mm:ss") + "   " + "Action End");
				return Content(strOrderInfo, "text/xml");
			}
			catch (Exception ex)
			{
				sw.WriteLine(DateTime.UtcNow.AddHours(8).ToString("yyyy/MM/dd HH:mm:ss") + "　　Error:" + ex.Message + "　" + ex.InnerException);
				return Content(ex.ToString());
			}
			finally
			{
				sw.WriteLine("---------------------------------------------------");
				sw.Close();
			}
		}

		//國泰 - 授權完成 Step1（App）
		[HttpPost]
		[ValidateInput(false)]
		public ActionResult App_PaymentCompelete(string strRsXml = "")
		{
			MithDataContext db = new MithDataContext();
			strRsXml = strRsXml.Replace("\\'", "\'");
			try
			{
				XmlDocument doc = new XmlDocument();
				doc.LoadXml(strRsXml);

				//Parse XML to Get Order Data
				XmlNode OrderInfo = doc.DocumentElement.SelectSingleNode("ORDERINFO");
				Dictionary<string, string> XmlValue = new Dictionary<string, string>();

				foreach (XmlNode xNode in OrderInfo.ChildNodes)
				{
					XmlValue.Add(xNode.Name.ToString(), xNode.InnerText.ToString());
				}
				string StoreID = XmlValue["STOREID"];
				string OrderNumber = XmlValue["ORDERNUMBER"];
				string Amount = XmlValue["AMOUNT"];

				//Parse XML to Get Auth Result
				XmlNode AuthInfo = doc.DocumentElement.SelectSingleNode("AUTHINFO");
				Dictionary<string, string> dicAuthValue = new Dictionary<string, string>();

				foreach (XmlNode xNode in AuthInfo.ChildNodes)
				{
					dicAuthValue.Add(xNode.Name.ToString(), xNode.InnerText.ToString());
				}

				string AUTHSTATUS = dicAuthValue["AUTHSTATUS"];
				string AUTHCODE = dicAuthValue["AUTHCODE"];

				string Domain = "mith.azurewebsites.net";
				string CUBKEY = "eec85fee81fb036d31471462846fa422";

				//CAVALUE = MD5(Domain + CUBKEY)
				string CAVALUE = Method.CreditCard_MD5(Domain + CUBKEY);
				string RETURL = "http://mith.azurewebsites.net/WebAPI/Get_CreditCardInfo";
				//回傳的XML
				string strOrderInfo =
				"<?xml version=\'1.0\' encoding=\'UTF-8\'?>" +
						"<MERCHANTXML>" +
								"<CAVALUE>" + CAVALUE + "</CAVALUE>" +
								"<RETURL>" + RETURL + "</RETURL>" +
						"</MERCHANTXML>";

				#region 更新訂單AuthCode
				//授權成功
				if (AUTHSTATUS == "0000")
				{
					var data = db.Order.FirstOrDefault(f => f.SN == OrderNumber);

					if (data != null)
					{
						data.CathayAuthCode = AUTHCODE;
						data.CathayStatus = "0202";
						db.SubmitChanges();
					}
				}
				#endregion

				return Content(strOrderInfo, "text/xml");
			}
			catch (Exception ex)
			{
				return Content(ex.ToString());
			}
			finally
			{
				db.Connection.Close();
			}
		}

		///國泰 - 取得訂單資訊 Step2
		[HttpPost]
		[ValidateInput(false)]
		public ActionResult Get_CreditCardInfo(string strOrderInfo = "")
		{
			MithDataContext db = new MithDataContext();
			try
			{
				strOrderInfo = strOrderInfo.Replace("\\'", "\'");
				XmlDocument doc = new XmlDocument();
				doc.LoadXml(strOrderInfo);

				//Parse XML to Get Order Data
				XmlNode OrderInfo = doc.DocumentElement.SelectSingleNode("ORDERINFO");
				Dictionary<string, string> XmlValue = new Dictionary<string, string>();

				foreach (XmlNode xNode in OrderInfo.ChildNodes)
				{
					XmlValue.Add(xNode.Name.ToString(), xNode.InnerText.ToString());
				}
				string StoreID = XmlValue["STOREID"];
				string OrderNumber = XmlValue["ORDERNUMBER"];

				var data = db.Order.FirstOrDefault(f => f.SN == OrderNumber);

				if (data != null)
				{
					//信用卡授權成功
					if (data.CathayStatus == "0202")
					{
						return Redirect("soohoobook.mith.bank://?Success=True");
					}
					//信用卡授權失敗
					else
					{
						//刪除訂單
						db.Order.DeleteOnSubmit(data);
						db.SubmitChanges();
						return Redirect("soohoobook.mith.bank://?Success=False");
					}
				}
				//後台沒這筆訂單
				else
				{
					return Redirect("soohoobook.mith.bank://?Success=False");
				}
			}
			catch (Exception ex)
			{
				return Content(ex.ToString());
			}
			finally
			{
				db.Connection.Close();
			}
		}

		//Azure執行排程 - 取得已授權，但未請款的訂單
		//public ActionResult Schedule_Request_CreditCard()
		//{
		//    MithDataContext db = new MithDataContext();
		//    try
		//    {
		//        //訂單：已成立、付款狀態：已付款、 授權狀態：已授權
		//        var data = db.Order.Where(w => w.Status != -1 && w.CashFlowType == 2 && w.CashFlowStatus ==
		//            2 && w.CathayStatus == "0202");
		//        foreach (var item in data)
		//        {
		//            WebApiMethod.Request_CreditCard(Server, item.SN, item.Total.ToString(), item.CathayAuthCode);
		//        }
		//        return Json(true, JsonRequestBehavior.AllowGet);
		//        //return Content(STATUS);
		//    }
		//    catch (Exception e)
		//    {
		//        return Content(e.ToString());
		//    }
		//    finally
		//    {
		//        db.Connection.Close();
		//    }
		//}

		//Azure執行排程 - 信用卡已授權，但後台訂單卻未成立
		//public ActionResult Schedule_Cancel_CreditCard()
		//{
		//		MithDataContext db = new MithDataContext();
		//		try
		//		{
		//				//訂單：未成立，授權狀態：已授權
		//				var data = db.Order.Where(w => w.Status == -1 && w.CathayStatus == "0202");
		//				foreach (var item in data)
		//				{
		//						WebApiMethod.Cancel_CreditCard(Server, item.SN);
		//				}
		//				return Json(true, JsonRequestBehavior.AllowGet);
		//		}
		//		catch (Exception e)
		//		{
		//				return Content(e.ToString());
		//		}
		//		finally
		//		{
		//				db.Connection.Close();
		//		}
		//}

		#endregion

		#region 虛擬帳號


		//SSL需要加這段方法
		public static bool RemoteCertificateValidationCallback(Object sender, X509Certificate certificate, X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
		{
			return true;
		}


		#endregion

		#region 身高 & 體重 & 縣市List

		[HttpPost]
		public ActionResult Get_RegiserOptionList()
		{

			try
			{
				object obj = WebApiMethod.Get_RegiserOptionData();
				if (obj != null)
				{
					return Json(obj, JsonRequestBehavior.AllowGet);
				}
				else
				{
					return Json(false, JsonRequestBehavior.AllowGet);
				}
			}
			catch (Exception e)
			{
				return Content("Error");
			};
		}

		[HttpPost]
		public ActionResult Get_AreaList(int CityId = 0)
		{

			try
			{
				object obj = WebApiMethod.Get_AreaData(CityId);
				if (obj != null)
				{
					return Json(obj, JsonRequestBehavior.AllowGet);
				}
				else
				{
					return Json(false, JsonRequestBehavior.AllowGet);
				}
			}
			catch (Exception e)
			{
				return Content("Error");
			};
		}
		#endregion

		#region 主題館Menu
		[HttpPost]
		public ActionResult Get_ThemeMenuList(string Pic_Size = "")
		{

			try
			{
				object obj = WebApiMethod.Get_ThemeMenuData(Pic_Size);
				if (obj != null)
				{
					return Json(obj, JsonRequestBehavior.AllowGet);
				}
				else
				{
					return Json(false, JsonRequestBehavior.AllowGet);
				}
			}
			catch (Exception e)
			{
				return Content("Error");
			};
		}

		[HttpPost]
		public ActionResult Get_ThemeGoodsList(int ThemeMenuId = 0, int BrandId = 0, string Pic_Size = "")
		{

			try
			{
				object obj = WebApiMethod.Get_ThemeGoodsData(ThemeMenuId, BrandId, Pic_Size);
				if (obj != null)
				{
					return Json(obj, JsonRequestBehavior.AllowGet);
				}
				else
				{
					return Json(false, JsonRequestBehavior.AllowGet);
				}
			}
			catch (Exception e)
			{
				return Content("Error");
			};
		}

		[HttpPost]
		public ActionResult Get_BrandList()
		{

			try
			{
				object obj = WebApiMethod.Get_BrandData();
				if (obj != null)
				{
					return Json(obj, JsonRequestBehavior.AllowGet);
				}
				else
				{
					return Json(false, JsonRequestBehavior.AllowGet);
				}
			}
			catch (Exception e)
			{
				return Content("Error");
			};
		}

		#endregion

		#region 會員

		//一般註冊
		[HttpPost]
		public ActionResult Insert_Volunteers_Mith(string Email = "", string Password = "", string Name = "")
		{
			MithDataContext db = new MithDataContext();
			VolunteersModel model = new VolunteersModel();
			try
			{
				//判斷是否申請過會員
				var data = db.Volunteers.FirstOrDefault(f => f.Email.Equals(Email));

				//Null代表沒申請過會員
				if (data == null)
				{
					//驗證Email
					Regex reg_Email = new Regex(@"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
					bool Check_Email = reg_Email.IsMatch(Email);


					//Email格式正確
					if (Check_Email == true && !string.IsNullOrWhiteSpace(Password) && !string.IsNullOrWhiteSpace(Name))
					{
						int Id = db.Volunteers.OrderByDescending(o => o.Id).FirstOrDefault() != null ? db.Volunteers.OrderByDescending(o => o.Id).FirstOrDefault().Id + 1 : 1;

						var item = new Volunteers()
						{
							Id = Id,
							Email = Email,
							Password = Method.Get_HashPassword(Password),
							ShowName = Name,
							Type = 1, //類型 = Mith註冊
							RegisterCode = Method.RandomKey(5, true, true, false, false),
							RecommendId = 0,
							CreateTime = DateTime.UtcNow,
							UpdateTime = DateTime.UtcNow,
							LastUserId = 0,
							Enable = true,
						};
						db.Volunteers.InsertOnSubmit(item);
						db.SubmitChanges();

						List<object> obj = new List<object>();
						obj.Add(new { VolunteersId = Id });

						return Json(obj, JsonRequestBehavior.AllowGet);
					}
					else
					{
						return Content("Email Error");
					}
				}
				else
				{
					return Json(false, JsonRequestBehavior.AllowGet);
				}
			}
			catch (Exception e)
			{
				return Content("Error");
			}
			finally
			{
				db.Connection.Close();
			}
		}

		//FB、微博註冊，如果註冊過直接登入
		[HttpPost]
		public ActionResult Insert_Volunteers_Other(string Email = null, string Password = "", int Type = 0, string Name = null, string Birthday = null, string Pic = null)
		{
			MithDataContext db = new MithDataContext();
			VolunteersModel model = new VolunteersModel();
			try
			{
				//判斷是否申請過會員
				var data = db.Volunteers.FirstOrDefault(f => f.Email.Equals(Email));
				List<object> obj = new List<object>();
				//Null代表沒申請過會員
				if (data == null)
				{
					//驗證Email
					Regex reg_Email = new Regex(@"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
					bool Check_Email = reg_Email.IsMatch(Email);


					//Email格式正確
					if (Check_Email == true)
					{
						int Id = db.Volunteers.OrderByDescending(o => o.Id).FirstOrDefault() != null ? db.Volunteers.OrderByDescending(o => o.Id).FirstOrDefault().Id + 1 : 1;
						DateTime? Bir = null;
						if (!string.IsNullOrWhiteSpace(Birthday))
						{
							Bir = DateTime.Parse(Birthday);
						}
						string RegisterCode = Method.RandomKey(5, true, true, false, false);
						var item = new Volunteers()
						{
							Id = Id,
							Email = Email,
							Password = Method.Get_HashPassword(Password),
							Type = Type,
							RegisterCode = RegisterCode,
							ShowName = Name,
							Birthday = Bir,
							Pic = Pic,
							CreateTime = DateTime.UtcNow,
							UpdateTime = DateTime.UtcNow,
							LastUserId = 0,
							Enable = true,
						};
						db.Volunteers.InsertOnSubmit(item);
						db.SubmitChanges();

						obj.Add(new
						{
							VolunteersId = Id,
							Volunteers_Name = Name,
							Volunteers_Tel = "",
							Volunteers_Address = "",
							Volunteers_Pic = Pic,
							Volunteers_ShareUrl = "http://mith.azurewebsites.net/webapi/ShareCode?Code=" + RegisterCode,
							Role = "Volunteers",
							Status = "New",
							Enable = true,
						});

						return Json(obj, JsonRequestBehavior.AllowGet);
					}
					else
					{
						return Content("Email Error");
					}
				}
				//已經註冊過了
				else
				{
					//加密
					Password = Method.Get_HashPassword(Password);

					//密碼輸入正確，直接登入
					if (data.Password == Password)
					{
						if (!string.IsNullOrWhiteSpace(Name))
						{
							data.ShowName = Name;
						}
						if (!string.IsNullOrWhiteSpace(Birthday))
						{
							data.Birthday = DateTime.Parse(Birthday);
						}
						db.SubmitChanges();

						string City_Name = db.City.FirstOrDefault(f => f.Id == data.CityId) != null ? db.City.FirstOrDefault(f => f.Id == data.CityId).Name : "";

						string Area_Name = db.Area.FirstOrDefault(f => f.Id == data.AreaId) != null ? db.Area.FirstOrDefault(f => f.Id == data.AreaId).Name : "";

						obj.Add(new
						{
							VolunteersId = data.Id,
							Volunteers_Name = data.RealName,
							Volunteers_Tel = data.ContactTel,
							Volunteers_Address = City_Name + Area_Name + data.Address,
							Volunteers_Pic = data.Pic,
							Volunteers_ShareUrl = "http://mith.azurewebsites.net/webapi/ShareCode?Code=" + data.RegisterCode,
							Role = "Volunteers",
							Status = "Old",
							Enable = data.Enable,
						});
						return Json(obj, JsonRequestBehavior.AllowGet);
					}
					else
					{
						return Json(false, JsonRequestBehavior.AllowGet);
					}
				}

			}
			catch (Exception e)
			{
				return Content("Error");
			}
			finally
			{
				db.Connection.Close();
			}
		}

		//確認是否用微博註冊過
		[HttpPost]
		public ActionResult Check_Weibo(string Password = null)
		{
			MithDataContext db = new MithDataContext();
			VolunteersModel model = new VolunteersModel();
			try
			{
				Password = Method.Get_HashPassword(Password);
				//判斷是否申請過會員
				var data = db.Volunteers.FirstOrDefault(f => f.Password == Password && f.Type == 3);
				List<object> obj = new List<object>();
				//不 = Null代表註冊過
				if (data != null)
				{
					obj.Add(new
					{
						VolunteersEMail = data.Email
					});

					return Json(obj, JsonRequestBehavior.AllowGet);
				}
				else
				{
					return Json(false, JsonRequestBehavior.AllowGet);
				}
			}
			catch (Exception e)
			{
				return Content("Error");
			}
			finally
			{
				db.Connection.Close();
			}
		}


		//修改會員資料
		[HttpPost]
		public ActionResult Update_Volunteers(int VolunteersId = 0, string Password = null, string ShowName = null, string RealName = null, string Tel = null, int CityId = 0, int AreaId = 0, string Address = null, string Birthday = null, int HeightId = 0, int WeightId = 0, HttpPostedFileBase File = null)
		{
			MithDataContext db = new MithDataContext();
			VolunteersModel model = new VolunteersModel();
			try
			{
				//判斷會員是否存在
				var data = db.Volunteers.FirstOrDefault(f => f.Id == VolunteersId);
				string Pic = "";
				if (data != null)
				{
					if (!string.IsNullOrWhiteSpace(Password))
					{
						//加密
						data.Password = Method.Get_HashPassword(Password);
					}

					if (!string.IsNullOrWhiteSpace(ShowName))
					{
						data.ShowName = ShowName;
					}
					if (!string.IsNullOrWhiteSpace(RealName))
					{
						data.RealName = RealName;
					}
					if (!string.IsNullOrWhiteSpace(Tel))
					{
						data.ContactTel = Tel;
					}
					if (CityId != 0)
					{
						data.CityId = CityId;
					}
					if (AreaId != 0)
					{
						data.AreaId = AreaId;
					}
					if (!string.IsNullOrWhiteSpace(Address))
					{
						data.Address = Address;
					}
					if (!string.IsNullOrWhiteSpace(Birthday))
					{
						data.Birthday = DateTime.Parse(Birthday);
					}
					if (HeightId != 0)
					{
						data.Height = HeightId;
					}
					if (WeightId != 0)
					{
						data.Weight = WeightId;
					}
					//大頭貼
					if (File != null)
					{
						string sContainer = "volunteers";
						string sImagePath = VolunteersId + "/";
						string FileExtension = "";
						string name = Method.RandomKey(5, true, false, false, false);

						Size Middle = new Size();
						Size Small = new Size();

						//如果大頭照從Mith上傳，先刪除舊圖片
						if (!string.IsNullOrWhiteSpace(data.Pic) && data.Pic.Contains("mithblob"))
						{
							BlobHelper.delete(sContainer + "/" + VolunteersId, Path.GetFileName(data.Pic));
						}

						FileExtension = System.IO.Path.GetExtension(File.FileName);

						decimal MB = Math.Round(((decimal)File.ContentLength / 1024) / 1024, 1);

						BlobFileDesc aBlobfile = new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + FileExtension };

						//大於1MB
						if (MB > 1)
						{
							Image image = Image.FromStream(File.InputStream);
							int NewWidth = Convert.ToInt16(Math.Ceiling(image.Width * 0.7));
							int NewHeight = Convert.ToInt16(Math.Ceiling(image.Height * 0.7));
							Middle = new Size(NewWidth, NewHeight);

							while (MB > 1)
							{
								Pic = BlobHelper.UploadResize_Blob(image, aBlobfile, Middle);
								MB = (decimal)Session["MB"];
								NewWidth = Convert.ToInt16(Math.Ceiling(NewWidth * 0.7));
								NewHeight = Convert.ToInt16(Math.Ceiling(NewHeight * 0.7));
								Middle = new Size(NewWidth, NewHeight);
							}
							image.Dispose();
						}
						else
						{
							Pic = BlobHelper.Upload_Blob(aBlobfile, File.InputStream, name + FileExtension, Middle, Small, false)[0];
						}

						data.Pic = Pic;
					}

					db.SubmitChanges();
					Pic = db.Volunteers.FirstOrDefault(f => f.Id == VolunteersId).Pic;
					List<object> obj = new List<object>();
					obj.Add(new { Volunteers_Pic = '"' + Pic + '"' });
					return Json(obj, JsonRequestBehavior.AllowGet);
				}
				else
				{
					return Json(false, JsonRequestBehavior.AllowGet);
				}
			}
			catch (Exception e)
			{
				return Content("Error");
			}
			finally
			{
				db.Connection.Close();
			}
		}

		//修改會員密碼
		[HttpPost]
		public ActionResult Update_Password(int VolunteersId = 0, string OldPassword = null, string NewPassword = null)
		{
			MithDataContext db = new MithDataContext();
			VolunteersModel model = new VolunteersModel();
			try
			{
				//判斷會員是否存在
				var data = db.Volunteers.FirstOrDefault(f => f.Id == VolunteersId);
				if (data != null)
				{
					//加密
					OldPassword = Method.Get_HashPassword(OldPassword);
					//密碼輸入正確
					if (data.Password == OldPassword)
					{
						data.Password = Method.Get_HashPassword(NewPassword);
						db.SubmitChanges();
						return Json(true, JsonRequestBehavior.AllowGet);
					}
					else
					{
						return Json(false, JsonRequestBehavior.AllowGet);
					}
				}
				else
				{
					return Json(false, JsonRequestBehavior.AllowGet);
				}
			}
			catch (Exception e)
			{
				return Content("Error");
			}
			finally
			{
				db.Connection.Close();
			}
		}

		//取得會員資料
		[HttpPost]
		public ActionResult Get_VolunteersOne(int VolunteersId = 0)
		{

			try
			{
				object obj = WebApiMethod.Get_VolunteersOneData(VolunteersId);
				if (obj != null)
				{
					return Json(obj, JsonRequestBehavior.AllowGet);
				}
				else
				{
					return Json(false, JsonRequestBehavior.AllowGet);
				}
			}
			catch (Exception e)
			{
				return Content("Error");
			};
		}

		//分享MiTH App給別人安裝
		public ActionResult ShareCode(string Code = "", int VolunteersId = 0)
		{
			MithDataContext db = new MithDataContext();
			var filename = AppDomain.CurrentDomain.BaseDirectory + "Log.txt";
			var sw = new System.IO.StreamWriter(filename, true);
			sw.WriteLine(DateTime.UtcNow.AddHours(8).ToString("yyyy/MM/dd HH:mm:ss") + "   " + "Start");
			try
			{
				string IP = "";
				//判斷是否有設定代理伺服器
				if (Request.ServerVariables["HTTP_VIA"] == null)
				{
					IP = Request.ServerVariables["REMOTE_ADDR"].ToString();
				}
				else
				{
					IP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
				}
				var Agent = Request.UserAgent;

				string Brand = Agent.Split("/")[1];
				sw.WriteLine(DateTime.UtcNow.AddHours(8).ToString("yyyy/MM/dd HH:mm:ss") + "   " + "IP:" + IP + ",Brand:" + Brand);
				List<object> obj = new List<object>();
				obj.Add(new { IP = IP, Brand = Brand, Agent = Agent });

				//如果Code不為Null和空白，新增資料
				if (!string.IsNullOrWhiteSpace(Code))
				{
					//排除微軟和Mac
					if (!Agent.Contains("Windows NT") && !Agent.Contains("Macintosh"))
					{
						sw.WriteLine(DateTime.UtcNow.AddHours(8).ToString("yyyy/MM/dd HH:mm:ss") + "   " + "Code:" + Code);
						var data = db.MappingCode.FirstOrDefault(f => f.Code == Code && f.IP == IP);
						if (data != null)
						{
							data.CreateTime = DateTime.UtcNow;
							db.SubmitChanges();
						}
						else
						{
							if (Brand.Length > 10)
							{
								var item = new MappingCode()
								{
									Code = Code,
									IP = IP,
									Brand = Brand,
									CreateTime = DateTime.UtcNow,
								};
								db.MappingCode.InsertOnSubmit(item);
								db.SubmitChanges();
							}
						}

						if (Agent.Contains("Android"))
						{
							return Content("Android");
						}
						else
						{
							return Content("ios");

						}
					}
					return Content("不是使用手機");
				}

				//Code為""，取得上線會員
				else
				{
					if (VolunteersId != 0)
					{
						var data = db.Volunteers.FirstOrDefault(f => f.Id == VolunteersId);
						Code = db.MappingCode.FirstOrDefault(f => f.IP == IP && f.Brand == Brand) != null ? db.MappingCode.FirstOrDefault(f => f.IP == IP && f.Brand == Brand).Code : "";
						sw.WriteLine(DateTime.UtcNow.AddHours(8).ToString("yyyy/MM/dd HH:mm:ss") + "   " + "Code:" + Code);
						if (Code == "")
						{
							sw.WriteLine(DateTime.UtcNow.AddHours(8).ToString("yyyy/MM/dd HH:mm:ss") + "   " + "Code=null");
							return Json(obj, JsonRequestBehavior.AllowGet);
						}
						int RecommendId = db.Volunteers.FirstOrDefault(f => f.RegisterCode == Code) != null ? db.Volunteers.FirstOrDefault(f => f.RegisterCode == Code).Id : 0;

						sw.WriteLine(DateTime.UtcNow.AddHours(8).ToString("yyyy/MM/dd HH:mm:ss") + "   " + "RecommendId:" + RecommendId);
						if (data != null & data.RecommendId == 0)
						{
							data.RecommendId = RecommendId;
							db.SubmitChanges();
							sw.WriteLine(DateTime.UtcNow.AddHours(8).ToString("yyyy/MM/dd HH:mm:ss") + "   " + "Update Success");
						}
					}
					return Json(true, JsonRequestBehavior.AllowGet);
				}

			}
			catch (Exception e)
			{
				sw.WriteLine(DateTime.UtcNow.AddHours(8).ToString("yyyy/MM/dd HH:mm:ss") + "　　Error:" + e.Message + "　" + e.InnerException);
				return Content(e.ToString());
			}
			finally
			{
				sw.WriteLine("---------------------------------------------------");
				sw.Close();
				db.Connection.Close();
			}
		}
		#endregion

		#region 登入登出 & 忘記密碼

		[HttpPost]
		public ActionResult Login(string Email = "", string Password = "")
		{

			MithDataContext db = new MithDataContext();
			VolunteersModel model = new VolunteersModel();
			try
			{
				//加密
				Password = Method.Get_HashPassword(Password);

				//驗證Email
				Regex reg_Email = new Regex(@"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
				bool Check_Email = reg_Email.IsMatch(Email);

				//Email格式正確
				if (Check_Email == true)
				{
					#region 會員登入
					List<object> obj = new List<object>();
					var data = db.Volunteers.FirstOrDefault(w => w.Email.Equals(Email));
					if (data != null)
					{
						//密碼輸入正確
						if (data.Password == Password)
						{
							string City_Name = db.City.FirstOrDefault(f => f.Id == data.CityId) != null ? db.City.FirstOrDefault(f => f.Id == data.CityId).Name : null;

							string Area_Name = db.Area.FirstOrDefault(f => f.Id == data.AreaId) != null ? db.Area.FirstOrDefault(f => f.Id == data.AreaId).Name : null;

							obj.Add(new
							{
								VolunteersId = data.Id,
								Volunteers_Name = data.RealName,
								Volunteers_Tel = data.ContactTel,
								Volunteers_Address = City_Name + Area_Name + data.Address,
								Volunteers_Pic = data.Pic,
								Volunteers_ShareUrl = "http://mith.azurewebsites.net/webapi/ShareCode?Code=" + data.RegisterCode,
								Role = "Volunteers",
								Enable = data.Enable
							});
							return Json(obj, JsonRequestBehavior.AllowGet);
						}
					}
					#endregion
				}
				else
				{
					#region 達人登入
					List<object> obj = new List<object>();
					var data = db.UserProfile.FirstOrDefault(w => w.Account == Email && w.Enable == true);

					if (data != null)
					{
						//帳密輸入正確
						if (data.Account == Email && data.Password == Password)
						{
							var Expert = db.Expert_View.FirstOrDefault(f => f.Id == data.ExpertId);
							string Expert_Pic = Expert.Pic;
							string Expert_Cover = Expert.Cover;
							obj.Add(new { ExpertId = data.ExpertId, Expert_Pic = Expert_Pic, Expert_Cover = Expert_Cover, Role = "Expert" });
							return Json(obj, JsonRequestBehavior.AllowGet);
						}
					}
					#endregion
				}

				return Json(false, JsonRequestBehavior.AllowGet);

			}
			catch (Exception e)
			{
				return Content("Error");
			}
			finally
			{
				db.Connection.Close();
			}
		}

		[HttpPost]
		public ActionResult Logout(int VolunteersId = 0, int ExpertId = 0, int RandomCode = 0)
		{
			MithDataContext db = new MithDataContext();
			try
			{
				#region 會員登出
				if (VolunteersId > 0)
				{
					var Volunteers = db.PushToken.FirstOrDefault(f => f.VolunteerId == VolunteersId && f.RandomCode == RandomCode);
					//沒傳過Token（Insert）
					if (Volunteers != null)
					{
						db.PushToken.DeleteOnSubmit(Volunteers);
						db.SubmitChanges();
					}
				}
				#endregion

				#region 達人登出
				if (ExpertId > 0)
				{
					var Expert = db.PushToken.FirstOrDefault(f => f.ExpertId == ExpertId && f.RandomCode == RandomCode);
					//沒傳過Token（Insert）
					if (Expert != null)
					{
						db.PushToken.DeleteOnSubmit(Expert);
						db.SubmitChanges();
					}
				}
				#endregion

				return Json(true, JsonRequestBehavior.AllowGet);
			}
			catch (Exception e)
			{
				return Content("Error");
			}
			finally
			{
				db.Connection.Close();
			};
		}

		[HttpPost]
		public ActionResult Push_Token(int AppId = 0, string Token = null, int VolunteersId = 0, int ExpertId = 0)
		{
			MithDataContext db = new MithDataContext();
			try
			{
				int RandomCode = int.Parse(Method.RandomKey(6, true, false, false, false));
				if (Token != null)
				{
					var Get_Token = db.PushToken.FirstOrDefault(f => f.Token == Token);
					if (Get_Token != null)
					{
						RandomCode = Get_Token.RandomCode;
					}
					//新的Token才新增
					if (Get_Token == null)
					{
						#region 會員傳Token
						if (VolunteersId > 0)
						{
							var data = new PushToken()
							{
								AppID = AppId, //哪個App
								RandomCode = RandomCode, //登出使用
								Token = Token,  //Token
								CreateTime = DateTime.UtcNow,
								UpdateTime = DateTime.UtcNow,
								LastSendTime = null,
								BadgeCount = 0,
								VolunteerId = VolunteersId,
							};
							db.PushToken.InsertOnSubmit(data);
							db.SubmitChanges();
						}
						#endregion

						#region 達人傳Token
						if (ExpertId > 0)
						{
							var data = new PushToken()
							{
								AppID = AppId, //哪個App
								RandomCode = RandomCode,
								Token = Token, //Token
								CreateTime = DateTime.UtcNow,
								UpdateTime = DateTime.UtcNow,
								LastSendTime = null,
								BadgeCount = 0,
								ExpertId = ExpertId,
							};
							db.PushToken.InsertOnSubmit(data);
							db.SubmitChanges();
						}
						#endregion
					}

					List<object> obj = new List<object>();
					obj.Add(new { RandomCode = RandomCode });
					return Json(obj, JsonRequestBehavior.AllowGet);
				}

				else
				{
					return Json(false, JsonRequestBehavior.AllowGet);
				}
			}
			catch (Exception e)
			{
				return Content("Error");
			}
			finally
			{
				db.Connection.Close();
			};
		}

		//寄送認證信
		[HttpPost]
		public ActionResult Send_ForgotPassword(string VolunteersEmail = "")
		{
			MithDataContext db = new MithDataContext();

			try
			{
				string RandomCode = Method.RandomKey(5, false, true, false, false);
				List<object> obj = new List<object>();

				#region 先刪除舊的認證碼
				var data = db.ForgotPassword.Where(f => f.VolunteersEmail.Equals(VolunteersEmail));


				if (data != null)
				{
					db.ForgotPassword.DeleteAllOnSubmit(data);
					db.SubmitChanges();
				}

				//新增新的認證碼
				var ForgotPassword = new ForgotPassword()
				{
					VolunteersEmail = VolunteersEmail,
					RandomCode = RandomCode
				};
				db.ForgotPassword.InsertOnSubmit(ForgotPassword);
				db.SubmitChanges();
				#endregion

				//發送 Email，確認Email是否存在
				var data2 = db.Volunteers.FirstOrDefault(w => w.Email == VolunteersEmail);

				if (data2 != null)
				{
					bool UpdatePassword = true;

					if (data2.Type > 1)
					{
						UpdatePassword = false;
					}

					obj.Add(new
					{
						Enable = data2.Enable,
						UpdatePassword = UpdatePassword
					});
					//會員為啟動，且為MiTH註冊才寄信
					if (data2.Enable == true && UpdatePassword == true)
					{
						string EmailContent = HttpUtility.HtmlDecode("您好，你的認證碼為：" + RandomCode + "\n===================================================================\n若有任何問題，可連繫MiTH客服：mith@mix-with.com\n");

						Method.Send_Mail(VolunteersEmail, "MiTH App忘記密碼認證", EmailContent);

					}
					return Json(obj, JsonRequestBehavior.AllowGet);
				}
				else
				{
					return Json(false, JsonRequestBehavior.AllowGet);
				}

			}
			catch (Exception e)
			{
				return Content("Error");
			}
			finally
			{
				db.Connection.Close();
			}
		}

		//輸入認證碼
		[HttpPost]
		public ActionResult Check_ForgotPassword(string RandomCode = "")
		{
			MithDataContext db = new MithDataContext();

			try
			{
				var data = db.ForgotPassword.FirstOrDefault(f => f.RandomCode == RandomCode);

				if (data != null)
				{
					//刪掉此組認證碼
					db.ForgotPassword.DeleteOnSubmit(data);
					db.SubmitChanges();

					int VolunteersId = db.Volunteers.FirstOrDefault(f => f.Email == data.VolunteersEmail) != null ? db.Volunteers.FirstOrDefault(f => f.Email == data.VolunteersEmail).Id : 0;
					List<object> obj = new List<object>();
					obj.Add(new { VolunteersId = VolunteersId });
					return Json(obj, JsonRequestBehavior.AllowGet);
				}
				else
				{
					return Json(false, JsonRequestBehavior.AllowGet);
				}
			}
			catch (Exception e)
			{
				return Content("Error");
			}
			finally
			{
				db.Connection.Close();
			}
		}

		#endregion

		#region 百變穿搭
		[HttpPost]
		public ActionResult Get_WearClothesList(string Pic_Size = "")
		{

			try
			{
				if (Pic_Size != "")
				{
					object obj = WebApiMethod.Get_WearClothesData(Pic_Size);
					if (obj != null)
					{
						return Json(obj, JsonRequestBehavior.AllowGet);
					}
					else
					{
						return Json(false, JsonRequestBehavior.AllowGet);
					}
				}
				else
				{
					return Json(false, JsonRequestBehavior.AllowGet);
				}
			}
			catch (Exception e)
			{
				return Content("Error");
			};
		}

		public ActionResult Get_WearSearchList()
		{

			try
			{

				object obj = WebApiMethod.Get_WearSearchData();
				if (obj != null)
				{
					return Json(obj, JsonRequestBehavior.AllowGet);
				}
				else
				{
					return Json(false, JsonRequestBehavior.AllowGet);
				}
			}
			catch (Exception e)
			{
				return Content("Error");
			};
		}

		[HttpPost]
		public ActionResult Update_GoodsWearCount(int GoodsId)
		{
			MithDataContext db = new MithDataContext();
			var data = db.Goods.FirstOrDefault(f => f.Id == GoodsId);
			try
			{
				//不為Null代表帳號正確
				if (data != null)
				{
					data.WearCount += 1;
					db.SubmitChanges();
					return Json(true, JsonRequestBehavior.AllowGet);
				}
				else
				{
					return Json(false, JsonRequestBehavior.AllowGet);
				}

			}
			catch (Exception e)
			{
				return Content("Error");
			}
			finally
			{
				db.Connection.Close();
			};
		}

		#endregion

		#region 商品

		[HttpPost]
		public ActionResult Get_GoodsOne(int GoodsId = 0, string Pic_Size = "")
		{
			try
			{
				object obj = new object();

				if (Pic_Size == "Big")
				{
					obj = WebApiMethod.Get_GoodsData_Big(GoodsId);
				}
				else if (Pic_Size == "Middle")
				{
					obj = WebApiMethod.Get_GoodsData_Middle(GoodsId);
				}
				else
				{
					obj = WebApiMethod.Get_GoodsData_Small(GoodsId);
				}

				if (obj != null)
				{
					return Json(obj, JsonRequestBehavior.AllowGet);
				}
				return Json(false, JsonRequestBehavior.AllowGet);
			}
			catch (Exception e)
			{
				return Content("Error");
			};
		}

		//新增貨到通知次數
		[HttpPost]
		public ActionResult Update_GoodsNoticeCount(int GoodsId = 0)
		{
			MithDataContext db = new MithDataContext();
			try
			{
				var data = db.Goods.FirstOrDefault(f => f.Id == GoodsId);
				if (data != null)
				{
					data.NoticeCount += 1;
					db.SubmitChanges();

					string Email = db.Company.FirstOrDefault(f => f.Id == data.CompanyId).ContactEmail;

					if (!string.IsNullOrEmpty(Email))
					{
						string EmailContent = HttpUtility.HtmlDecode("商品：" + "【" + data.SN + "】" + data.Name + "\n已無庫存，請盡速補貨！");
						Method.Send_Mail(Email, "點擊貨到通知Email", EmailContent);

					}
					return Json(true, JsonRequestBehavior.AllowGet);
				}
				else
				{
					return Json(false, JsonRequestBehavior.AllowGet);
				}

			}
			catch (Exception e)
			{
				return Content("Error");
			}
			finally
			{
				db.Connection.Close();
			}
		}

		//取得商品 Q&A
		[HttpPost]
		public ActionResult Get_GoodsQAList(int GoodsId = 0)
		{

			try
			{
				object obj = WebApiMethod.Get_GoodsQAData(GoodsId);
				if (obj != null)
				{
					return Json(obj, JsonRequestBehavior.AllowGet);
				}
				else
				{
					return Json(false, JsonRequestBehavior.AllowGet);
				}
			}
			catch (Exception e)
			{
				return Content("Error");
			};
		}

		//新增商品 Q&A
		[HttpPost]
		public ActionResult Insert_GoodsQA(int GoodsId = 0, int VolunteersId = 0, string MessageTitle = "", string Message = "")
		{

			MithDataContext db = new MithDataContext();
			try
			{
				int CompanyId = db.Goods.FirstOrDefault(f => f.Id == GoodsId).CompanyId;
				var data = new GoodsQA()
				{
					GoodsId = GoodsId,
					VolunteersId = VolunteersId,
					MessageTitle = MessageTitle,
					Message = HttpUtility.HtmlEncode(Message),
					CreateTime = DateTime.UtcNow,
					ReplyMessage = null,
					ReplyTime = null,
					ReplyUserId = 0,
					CompanyId = CompanyId,
				};
				db.GoodsQA.InsertOnSubmit(data);
				db.SubmitChanges();

				Task t1 = Task.Run(() =>
				{
					var Volunteers = db.Volunteers.FirstOrDefault(f => f.Id == VolunteersId);
					var Goods = db.Goods.FirstOrDefault(f => f.Id == GoodsId);

					string EmailContent = HttpUtility.HtmlDecode("您好，商品有一則提問\n商品：【" + Goods.SN + "】" + Goods.Name + "\n提問會員：" + Volunteers.RealName + "\n提問標題：" + MessageTitle + "\n提問內容：" + HttpUtility.HtmlEncode(Message) + "\n請至後台確認並回覆");

					Method.Send_Mail(Volunteers.Email, "MiTH App 商品有一則提問 [商品編號" + Goods.SN + "]", EmailContent);
				});

				return Json(true, JsonRequestBehavior.AllowGet);
			}
			catch (Exception e)
			{
				return Content("Error");
			}
			finally
			{
				db.Connection.Close();
			}
		}

		#endregion

		#region 購物車的商品

		//取得購物車商品
		[HttpPost]
		public ActionResult Get_GoodsShoppingCartList(int VolunteersId = 0)
		{
			try
			{
				if (VolunteersId != 0)
				{
					object obj = WebApiMethod.Get_GoodsShoppingCartData(VolunteersId);
					if (obj != null)
					{
						return Json(obj, JsonRequestBehavior.AllowGet);
					}
					else
					{
						return Json(false, JsonRequestBehavior.AllowGet);
					}
				}
				else
				{
					return Json(false, JsonRequestBehavior.AllowGet);
				}
			}
			catch (Exception e)
			{
				return Content("Error");
			};
		}

		//新增購物車商品
		[HttpPost]
		public ActionResult Insert_GoodsShoppingCart(int VolunteersId = 0, int GoodsId = 0, string BuySize = "", int BuyQuantity = 0)
		{
			MithDataContext db = new MithDataContext();
			try
			{
				//商品底下有這個尺寸，而且數量 > 0
				int GoodsQuantity = db.GoodsQuantity.Where(w => w.GoodsId == GoodsId && w.Size == BuySize && w.Quantity > 0).Count();

				if (GoodsQuantity > 0)
				{
					var data = db.GoodsShoppingCart.FirstOrDefault(f => f.VolunteersId == VolunteersId && f.GoodsId == GoodsId && f.BuySize == BuySize);

					//會員的購物車沒此商品
					if (data == null)
					{
						var GoodsShoppingCart = new GoodsShoppingCart()
						{
							VolunteersId = VolunteersId,
							GoodsId = GoodsId,
							BuySize = BuySize,
							BuyQuantity = BuyQuantity,
							Createtime = DateTime.UtcNow
						};
						db.GoodsShoppingCart.InsertOnSubmit(GoodsShoppingCart);
						db.SubmitChanges();
					}
					//會員的購物車有此商品，數量進行累加
					else
					{
						data.BuyQuantity += BuyQuantity;
						db.SubmitChanges();
					}
					return Json(true, JsonRequestBehavior.AllowGet);
				}
				else
				{
					return Json(false, JsonRequestBehavior.AllowGet);
				}
			}
			catch (Exception e)
			{
				return Content("Error");
			}
			finally
			{
				db.Connection.Close();
			}
		}

		//刪除購物車商品
		[HttpPost]
		public ActionResult Delete_GoodsShoppingCart(int GoodsShoppingCartId = 0)
		{
			MithDataContext db = new MithDataContext();
			try
			{
				//文章
				var data = db.GoodsShoppingCart.FirstOrDefault(w => w.Id == GoodsShoppingCartId);

				if (data != null)
				{
					//刪文章
					db.GoodsShoppingCart.DeleteOnSubmit(data);
					db.SubmitChanges();
					return Json(true, JsonRequestBehavior.AllowGet);
				}
				else
				{
					return Json(false, JsonRequestBehavior.AllowGet);
				}
			}
			catch (Exception e)
			{
				return Content("Error");
			}
			finally
			{
				db.Connection.Close();
			}
		}

		#endregion

		#region 達人

		//達人資料
		[HttpPost]
		public ActionResult Get_ExpertList(int ExpertId = 0, int VolunteersId = 0)
		{
			try
			{
				object obj = WebApiMethod.Get_ExpertData(ExpertId, VolunteersId);
				if (obj != null)
				{
					return Json(obj, JsonRequestBehavior.AllowGet);
				}
				else
				{
					return Json(false, JsonRequestBehavior.AllowGet);
				}
			}
			catch (Exception e)
			{
				return Content("Error");
			};
		}

		//穿搭諮詢回覆
		[HttpPost]
		public ActionResult Update_Expert(int ExpertId = 0, string Content = "", HttpPostedFileBase Pic = null, HttpPostedFileBase Cover = null)
		{
			MithDataContext db = new MithDataContext();
			try
			{
				var data = db.Expert.FirstOrDefault(f => f.Id == ExpertId);

				if (data != null)
				{
					#region 更新圖片
					string sContainer = "expert";
					string sImagePath = ExpertId + "/";
					string FileExtension = "";
					string name = Method.RandomKey(5, true, false, false, false);
					Size Middle = new Size();
					Size Small = new Size();
					string Pic2 = "";
					string Cover2 = "";

					if (Pic != null)
					{
						FileExtension = System.IO.Path.GetExtension(Pic.FileName);
						//先刪除舊圖片
						BlobHelper.delete(sContainer + "/" + ExpertId, Path.GetFileName(data.Pic));

						decimal MB = Math.Round(((decimal)Pic.ContentLength / 1024) / 1024, 1);

						//大於1MB
						if (MB > 1)
						{
							BlobFileDesc aBlobfile = new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + "1" + FileExtension };

							Image image = Image.FromStream(Pic.InputStream);
							int NewWidth = Convert.ToInt16(Math.Ceiling(image.Width * 0.7));
							int NewHeight = Convert.ToInt16(Math.Ceiling(image.Height * 0.7));
							Middle = new Size(NewWidth, NewHeight);

							while (MB > 1)
							{
								Pic2 = BlobHelper.UploadResize_Blob(image, aBlobfile, Middle);
								MB = (decimal)Session["MB"];
								NewWidth = Convert.ToInt16(Math.Ceiling(NewWidth * 0.7));
								NewHeight = Convert.ToInt16(Math.Ceiling(NewHeight * 0.7));
								Middle = new Size(NewWidth, NewHeight);
							}
							image.Dispose();
						}
						else
						{
							Pic2 = BlobHelper.Upload_Blob(new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + "1" + FileExtension }, Pic.InputStream, name + "1" + FileExtension, Middle, Small, false)[0];
						}
					}

					if (Cover != null)
					{
						FileExtension = System.IO.Path.GetExtension(Cover.FileName);
						//先刪除舊圖片
						BlobHelper.delete(sContainer + "/" + ExpertId, Path.GetFileName(data.Cover));

						decimal MB = Math.Round(((decimal)Cover.ContentLength / 1024) / 1024, 1);

						//大於1MB
						if (MB > 1)
						{
							BlobFileDesc aBlobfile = new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + "2" + FileExtension };

							Image image = Image.FromStream(Cover.InputStream);
							int NewWidth = Convert.ToInt16(Math.Ceiling(image.Width * 0.7));
							int NewHeight = Convert.ToInt16(Math.Ceiling(image.Height * 0.7));
							Middle = new Size(NewWidth, NewHeight);

							while (MB > 1)
							{
								Cover2 = BlobHelper.UploadResize_Blob(image, aBlobfile, Middle);
								MB = (decimal)Session["MB"];
								NewWidth = Convert.ToInt16(Math.Ceiling(NewWidth * 0.7));
								NewHeight = Convert.ToInt16(Math.Ceiling(NewHeight * 0.7));
								Middle = new Size(NewWidth, NewHeight);
							}
							image.Dispose();
						}
						else
						{
							Cover2 = BlobHelper.Upload_Blob(new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + "2" + FileExtension }, Cover.InputStream, name + "2" + FileExtension, Middle, Small, false)[0];
						}
					}
					#endregion

					if (Content != "")
					{
						data.Content = HttpUtility.HtmlEncode(Content);
					}
					if (Pic2 != "")
					{
						data.Pic = Pic2;
					}
					if (Cover2 != "")
					{
						data.Cover = Cover2;
					}
					db.SubmitChanges();
					return Json(true, JsonRequestBehavior.AllowGet);
				}
				else
				{
					return Json(false, JsonRequestBehavior.AllowGet);
				}
			}
			catch (Exception e)
			{
				return null;
			}
			finally
			{
				db.Connection.Close();
			}
		}

		#endregion

		#region 追蹤達人


		//取得追蹤達人
		[HttpPost]
		public ActionResult Get_LikeExpertList(int VolunteersId)
		{
			try
			{
				object obj = WebApiMethod.Get_LikeExpertData(VolunteersId);
				if (obj != null)
				{
					return Json(obj, JsonRequestBehavior.AllowGet);
				}
				else
				{
					return Json(false, JsonRequestBehavior.AllowGet);
				}
			}
			catch (Exception e)
			{
				return Content("Error");
			};
		}

		//新增 or 移除追蹤達人
		[HttpPost]
		public ActionResult InsertDelete_LikeExpert(int VolunteersId = 0, int ExpertId = 0, bool Like = true)
		{
			MithDataContext db = new MithDataContext();
			try
			{
				var data = db.LikeExpert.FirstOrDefault(f => f.VolunteersId == VolunteersId && f.ExpertId == ExpertId);

				//追衝次數
				var data2 = db.Expert.FirstOrDefault(f => f.Id == ExpertId);

				//沒追蹤此達人，追蹤他 (Reutrn true)
				if (data == null && Like == true)
				{
					var LikeExpert = new LikeExpert()
					{
						ExpertId = ExpertId,
						VolunteersId = VolunteersId,
						CreateTime = DateTime.UtcNow
					};
					db.LikeExpert.InsertOnSubmit(LikeExpert);
					//收藏次數 +1
					data2.Like += 1;
					db.SubmitChanges();

					return Json(true, JsonRequestBehavior.AllowGet);
				}

				//已經追蹤此達人，還要追蹤他 (Reutrn false)
				else if (data != null && Like == true)
				{
					return Json(false, JsonRequestBehavior.AllowGet);
				}

				//有追蹤此達人，要移除他 (Reutrn true)
				else if (data != null && Like == false)
				{
					db.LikeExpert.DeleteOnSubmit(data);
					//收藏次數 -1
					data2.Like -= 1;
					db.SubmitChanges();
					return Json(true, JsonRequestBehavior.AllowGet);
				}

				//已經移除此達人，還要移除他 (Reutrn false)
				else
				{
					return Json(false, JsonRequestBehavior.AllowGet);
				}

			}
			catch (Exception e)
			{
				return Content("Error");
			}
			finally
			{
				db.Connection.Close();
			}
		}

		#endregion

		#region 文章

		//文章資料
		[HttpPost]
		public ActionResult Get_ArticleList(string From = "", int ExpertId = 0)
		{
			try
			{
				object obj = WebApiMethod.Get_ArticleData(From, ExpertId);
				if (obj != null)
				{
					return Json(obj, JsonRequestBehavior.AllowGet);
				}
				else
				{
					return Json(false, JsonRequestBehavior.AllowGet);
				}
			}
			catch (Exception e)
			{
				return Content("Error");
			};
		}

		//文章細節
		[HttpPost]
		public ActionResult Get_ArticleOne(int ArticleId = 0, int VolunteersId = 0)
		{
			try
			{
				object obj = WebApiMethod.Get_ArticleOneData(ArticleId, VolunteersId);
				if (obj != null)
				{
					return Json(obj, JsonRequestBehavior.AllowGet);
				}
				else
				{
					return Json(false, JsonRequestBehavior.AllowGet);
				}
			}
			catch (Exception e)
			{
				return Content("Error");
			};
		}


		//新增 or 移除文章按讚
		[HttpPost]
		public ActionResult InsertDelete_LikeArticle(int VolunteersId = 0, int ArticleId = 0, bool Like = true)
		{
			MithDataContext db = new MithDataContext();
			try
			{
				var data = db.LikeArticle.FirstOrDefault(f => f.VolunteersId == VolunteersId && f.ArticleId == ArticleId);

				//追蹤次數
				var data2 = db.Article.FirstOrDefault(f => f.Id == ArticleId);

				//這篇文章沒按讚，要按讚 (Reutrn true)
				if (data == null && Like == true)
				{
					var LikeArticle = new LikeArticle()
					{
						ArticleId = ArticleId,
						VolunteersId = VolunteersId,
						CreateTime = DateTime.UtcNow
					};
					db.LikeArticle.InsertOnSubmit(LikeArticle);
					//按讚次數 +1
					data2.Like += 1;
					db.SubmitChanges();

					return Json(true, JsonRequestBehavior.AllowGet);
				}

				//這篇文章已經按讚，還要按讚 (Reutrn false)
				else if (data != null && Like == true)
				{
					return Json(false, JsonRequestBehavior.AllowGet);
				}

				//這篇文章有按讚，要移除讚 (Reutrn true)
				else if (data != null && Like == false)
				{
					db.LikeArticle.DeleteOnSubmit(data);
					//按讚次數 -1
					data2.Like -= 1;
					db.SubmitChanges();
					return Json(true, JsonRequestBehavior.AllowGet);
				}

				//這篇文章沒按讚，還要移除讚 (Reutrn false)
				else
				{
					return Json(false, JsonRequestBehavior.AllowGet);
				}

			}
			catch (Exception e)
			{
				return Content("Error");
			}
			finally
			{
				db.Connection.Close();
			}
		}

		//新增文章
		[HttpPost]
		public ActionResult Insert_Article(int? ExpertId = null, string Title = "", string FromUrl = "",
				HttpPostedFileBase File1 = null, string Content1 = "",
				HttpPostedFileBase File2 = null, string Content2 = "",
				HttpPostedFileBase File3 = null, string Content3 = "",
				HttpPostedFileBase File4 = null, string Content4 = "",
				HttpPostedFileBase File5 = null, string Content5 = "",
				string RecommendSN1 = "",
				string RecommendSN2 = "",
				string RecommendSN3 = "",
				string RecommendSN4 = "",
				string RecommendSN5 = "")
		{
			MithDataContext db = new MithDataContext();
			try
			{
				int Id = db.Article.OrderByDescending(o => o.Id).FirstOrDefault() != null ? db.Article.OrderByDescending(o => o.Id).FirstOrDefault().Id + 1 : 1;

				#region 上傳圖片
				string sContainer = "article";
				string sImagePath = Id + "/";
				string FileExtension = "";
				string name = Method.RandomKey(5, true, false, false, false);

				Size Middle = new Size();
				Size Small = new Size();
				string Pic1 = null;
				string Pic2 = null;
				string Pic3 = null;
				string Pic4 = null;
				string Pic5 = null;

				if (File1 != null)
				{
					FileExtension = System.IO.Path.GetExtension(File1.FileName);

					decimal MB = Math.Round(((decimal)File1.ContentLength / 1024) / 1024, 1);

					BlobFileDesc aBlobfile = new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + "1" + FileExtension };

					//大於1MB
					if (MB > 1)
					{
						Image image = Image.FromStream(File1.InputStream);
						int NewWidth = Convert.ToInt16(Math.Ceiling(image.Width * 0.7));
						int NewHeight = Convert.ToInt16(Math.Ceiling(image.Height * 0.7));
						Middle = new Size(NewWidth, NewHeight);

						while (MB > 1)
						{
							Pic1 = BlobHelper.UploadResize_Blob(image, aBlobfile, Middle);
							MB = (decimal)Session["MB"];
							NewWidth = Convert.ToInt16(Math.Ceiling(NewWidth * 0.7));
							NewHeight = Convert.ToInt16(Math.Ceiling(NewHeight * 0.7));
							Middle = new Size(NewWidth, NewHeight);
						}
						image.Dispose();
					}
					else
					{
						//上傳圖片1
						Pic1 = BlobHelper.Upload_Blob(aBlobfile, File1.InputStream, name + "1" + FileExtension, Middle, Small, false)[0];
					}
				}
				if (File2 != null)
				{
					FileExtension = System.IO.Path.GetExtension(File2.FileName);

					decimal MB = Math.Round(((decimal)File2.ContentLength / 1024) / 1024, 1);

					BlobFileDesc aBlobfile = new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + "2" + FileExtension };

					//大於1MB
					if (MB > 1)
					{
						Image image = Image.FromStream(File2.InputStream);
						int NewWidth = Convert.ToInt16(Math.Ceiling(image.Width * 0.7));
						int NewHeight = Convert.ToInt16(Math.Ceiling(image.Height * 0.7));
						Middle = new Size(NewWidth, NewHeight);

						while (MB > 1)
						{
							Pic2 = BlobHelper.UploadResize_Blob(image, aBlobfile, Middle);
							MB = (decimal)Session["MB"];
							NewWidth = Convert.ToInt16(Math.Ceiling(NewWidth * 0.7));
							NewHeight = Convert.ToInt16(Math.Ceiling(NewHeight * 0.7));
							Middle = new Size(NewWidth, NewHeight);
						}
						image.Dispose();
					}
					else
					{
						//上傳圖片2
						Pic2 = BlobHelper.Upload_Blob(aBlobfile, File2.InputStream, name + "2" + FileExtension, Middle, Small, false)[0];
					}
				}
				if (File3 != null)
				{
					FileExtension = System.IO.Path.GetExtension(File3.FileName);

					decimal MB = Math.Round(((decimal)File3.ContentLength / 1024) / 1024, 1);

					BlobFileDesc aBlobfile = new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + "3" + FileExtension };

					//大於1MB
					if (MB > 1)
					{
						Image image = Image.FromStream(File3.InputStream);
						int NewWidth = Convert.ToInt16(Math.Ceiling(image.Width * 0.7));
						int NewHeight = Convert.ToInt16(Math.Ceiling(image.Height * 0.7));
						Middle = new Size(NewWidth, NewHeight);

						while (MB > 1)
						{
							Pic3 = BlobHelper.UploadResize_Blob(image, aBlobfile, Middle);
							MB = (decimal)Session["MB"];
							NewWidth = Convert.ToInt16(Math.Ceiling(NewWidth * 0.7));
							NewHeight = Convert.ToInt16(Math.Ceiling(NewHeight * 0.7));
							Middle = new Size(NewWidth, NewHeight);
						}
						image.Dispose();
					}
					else
					{
						//上傳圖片3
						Pic3 = BlobHelper.Upload_Blob(aBlobfile, File3.InputStream, name + "3" + FileExtension, Middle, Small, false)[0];
					}
				}
				if (File4 != null)
				{
					FileExtension = System.IO.Path.GetExtension(File4.FileName);

					decimal MB = Math.Round(((decimal)File4.ContentLength / 1024) / 1024, 1);

					BlobFileDesc aBlobfile = new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + "4" + FileExtension };

					//大於1MB
					if (MB > 1)
					{
						Image image = Image.FromStream(File4.InputStream);
						int NewWidth = Convert.ToInt16(Math.Ceiling(image.Width * 0.7));
						int NewHeight = Convert.ToInt16(Math.Ceiling(image.Height * 0.7));
						Middle = new Size(NewWidth, NewHeight);

						while (MB > 1)
						{
							Pic4 = BlobHelper.UploadResize_Blob(image, aBlobfile, Middle);
							MB = (decimal)Session["MB"];
							NewWidth = Convert.ToInt16(Math.Ceiling(NewWidth * 0.7));
							NewHeight = Convert.ToInt16(Math.Ceiling(NewHeight * 0.7));
							Middle = new Size(NewWidth, NewHeight);
						}
						image.Dispose();
					}
					else
					{
						//上傳圖片4
						Pic4 = BlobHelper.Upload_Blob(aBlobfile, File4.InputStream, name + "4" + FileExtension, Middle, Small, false)[0];
					}
				}
				if (File5 != null)
				{
					FileExtension = System.IO.Path.GetExtension(File5.FileName);

					decimal MB = Math.Round(((decimal)File5.ContentLength / 1024) / 1024, 1);

					BlobFileDesc aBlobfile = new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + "5" + FileExtension };

					//大於1MB
					if (MB > 1)
					{
						Image image = Image.FromStream(File5.InputStream);
						int NewWidth = Convert.ToInt16(Math.Ceiling(image.Width * 0.7));
						int NewHeight = Convert.ToInt16(Math.Ceiling(image.Height * 0.7));
						Middle = new Size(NewWidth, NewHeight);

						while (MB > 1)
						{
							Pic5 = BlobHelper.UploadResize_Blob(image, aBlobfile, Middle);
							MB = (decimal)Session["MB"];
							NewWidth = Convert.ToInt16(Math.Ceiling(NewWidth * 0.7));
							NewHeight = Convert.ToInt16(Math.Ceiling(NewHeight * 0.7));
							Middle = new Size(NewWidth, NewHeight);
						}
						image.Dispose();
					}
					else
					{
						//上傳圖片5
						Pic5 = BlobHelper.Upload_Blob(aBlobfile, File5.InputStream, name + "5" + FileExtension, Middle, Small, false)[0];
					}
				}

				#endregion

				var item = new Article()
				{
					Id = Id,
					ExpertId = ExpertId,
					Title = Title,
					Type = 0,
					FromUrl = FromUrl,

					Pic1 = Pic1,
					Content1 = HttpUtility.HtmlEncode(Content1),
					Pic2 = Pic2,
					Content2 = HttpUtility.HtmlEncode(Content2),
					Pic3 = Pic3,
					Content3 = HttpUtility.HtmlEncode(Content3),
					Pic4 = Pic4,
					Content4 = HttpUtility.HtmlEncode(Content4),
					Pic5 = Pic5,
					Content5 = HttpUtility.HtmlEncode(Content5),

					RecommendSN1 = RecommendSN1,
					RecommendSN2 = RecommendSN2,
					RecommendSN3 = RecommendSN3,
					RecommendSN4 = RecommendSN4,
					RecommendSN5 = RecommendSN5,

					Like = 0,
					Display = true,
					CreateTime = DateTime.UtcNow,
					UpdateTime = DateTime.UtcNow,
					LastUserId = 0
				};
				db.Article.InsertOnSubmit(item);
				db.SubmitChanges();
				return Json(true, JsonRequestBehavior.AllowGet);
			}
			catch (Exception e)
			{
				return Content("Error");
			}
			finally
			{
				db.Connection.Close();
			}
		}

		//更新文章
		[HttpPost]
		public ActionResult Update_Article(int ArticleId = 0, string Title = "", string FromUrl = "",
				HttpPostedFileBase File1 = null, string Content1 = "",
				HttpPostedFileBase File2 = null, string Content2 = "",
				HttpPostedFileBase File3 = null, string Content3 = "",
				HttpPostedFileBase File4 = null, string Content4 = "",
				HttpPostedFileBase File5 = null, string Content5 = "",
				string RecommendSN1 = "",
				string RecommendSN2 = "",
				string RecommendSN3 = "",
				string RecommendSN4 = "",
				string RecommendSN5 = "")
		{
			MithDataContext db = new MithDataContext();
			try
			{
				var data = db.Article.FirstOrDefault(f => f.Id == ArticleId);

				if (data != null)
				{
					#region 更新圖片
					string sContainer = "article";
					string sImagePath = ArticleId + "/";
					string FileExtension = "";
					string name = Method.RandomKey(5, true, false, false, false);

					Size Middle = new Size();
					Size Small = new Size();
					string Pic1 = null;
					string Pic2 = null;
					string Pic3 = null;
					string Pic4 = null;
					string Pic5 = null;

					if (File1 != null)
					{
						FileExtension = System.IO.Path.GetExtension(File1.FileName);

						//先刪除舊圖片
						BlobHelper.delete(sContainer + "/" + ArticleId, Path.GetFileName(data.Pic1));

						decimal MB = Math.Round(((decimal)File1.ContentLength / 1024) / 1024, 1);

						BlobFileDesc aBlobfile = new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + "1" + FileExtension };

						//大於1MB
						if (MB > 1)
						{
							Image image = Image.FromStream(File1.InputStream);
							int NewWidth = Convert.ToInt16(Math.Ceiling(image.Width * 0.7));
							int NewHeight = Convert.ToInt16(Math.Ceiling(image.Height * 0.7));
							Middle = new Size(NewWidth, NewHeight);

							while (MB > 1)
							{
								Pic1 = BlobHelper.UploadResize_Blob(image, aBlobfile, Middle);
								MB = (decimal)Session["MB"];
								NewWidth = Convert.ToInt16(Math.Ceiling(NewWidth * 0.7));
								NewHeight = Convert.ToInt16(Math.Ceiling(NewHeight * 0.7));
								Middle = new Size(NewWidth, NewHeight);
							}
							image.Dispose();
						}
						else
						{
							//上傳圖片1
							Pic1 = BlobHelper.Upload_Blob(aBlobfile, File1.InputStream, name + "1" + FileExtension, Middle, Small, false)[0];
						}
					}
					if (File2 != null)
					{
						FileExtension = System.IO.Path.GetExtension(File2.FileName);

						//先刪除舊圖片
						BlobHelper.delete(sContainer + "/" + ArticleId, Path.GetFileName(data.Pic2));

						decimal MB = Math.Round(((decimal)File2.ContentLength / 1024) / 1024, 1);

						BlobFileDesc aBlobfile = new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + "2" + FileExtension };

						//大於1MB
						if (MB > 1)
						{
							Image image = Image.FromStream(File2.InputStream);
							int NewWidth = Convert.ToInt16(Math.Ceiling(image.Width * 0.7));
							int NewHeight = Convert.ToInt16(Math.Ceiling(image.Height * 0.7));
							Middle = new Size(NewWidth, NewHeight);

							while (MB > 1)
							{
								Pic2 = BlobHelper.UploadResize_Blob(image, aBlobfile, Middle);
								MB = (decimal)Session["MB"];
								NewWidth = Convert.ToInt16(Math.Ceiling(NewWidth * 0.7));
								NewHeight = Convert.ToInt16(Math.Ceiling(NewHeight * 0.7));
								Middle = new Size(NewWidth, NewHeight);
							}
							image.Dispose();
						}
						else
						{
							//上傳圖片2
							Pic2 = BlobHelper.Upload_Blob(aBlobfile, File2.InputStream, name + "2" + FileExtension, Middle, Small, false)[0];
						}
					}
					if (File3 != null)
					{
						FileExtension = System.IO.Path.GetExtension(File3.FileName);

						//先刪除舊圖片
						BlobHelper.delete(sContainer + "/" + ArticleId, Path.GetFileName(data.Pic3));

						decimal MB = Math.Round(((decimal)File3.ContentLength / 1024) / 1024, 1);

						BlobFileDesc aBlobfile = new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + "3" + FileExtension };

						//大於1MB
						if (MB > 1)
						{
							Image image = Image.FromStream(File3.InputStream);
							int NewWidth = Convert.ToInt16(Math.Ceiling(image.Width * 0.7));
							int NewHeight = Convert.ToInt16(Math.Ceiling(image.Height * 0.7));
							Middle = new Size(NewWidth, NewHeight);

							while (MB > 1)
							{
								Pic3 = BlobHelper.UploadResize_Blob(image, aBlobfile, Middle);
								MB = (decimal)Session["MB"];
								NewWidth = Convert.ToInt16(Math.Ceiling(NewWidth * 0.7));
								NewHeight = Convert.ToInt16(Math.Ceiling(NewHeight * 0.7));
								Middle = new Size(NewWidth, NewHeight);
							}
							image.Dispose();
						}
						else
						{
							//上傳圖片3
							Pic3 = BlobHelper.Upload_Blob(aBlobfile, File3.InputStream, name + "3" + FileExtension, Middle, Small, false)[0];
						}
					}
					if (File4 != null)
					{
						FileExtension = System.IO.Path.GetExtension(File4.FileName);

						//先刪除舊圖片
						BlobHelper.delete(sContainer + "/" + ArticleId, Path.GetFileName(data.Pic4));

						decimal MB = Math.Round(((decimal)File4.ContentLength / 1024) / 1024, 1);

						BlobFileDesc aBlobfile = new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + "4" + FileExtension };

						//大於1MB
						if (MB > 1)
						{
							Image image = Image.FromStream(File4.InputStream);
							int NewWidth = Convert.ToInt16(Math.Ceiling(image.Width * 0.7));
							int NewHeight = Convert.ToInt16(Math.Ceiling(image.Height * 0.7));
							Middle = new Size(NewWidth, NewHeight);

							while (MB > 1)
							{
								Pic4 = BlobHelper.UploadResize_Blob(image, aBlobfile, Middle);
								MB = (decimal)Session["MB"];
								NewWidth = Convert.ToInt16(Math.Ceiling(NewWidth * 0.7));
								NewHeight = Convert.ToInt16(Math.Ceiling(NewHeight * 0.7));
								Middle = new Size(NewWidth, NewHeight);
							}
							image.Dispose();
						}
						else
						{
							//上傳圖片4
							Pic4 = BlobHelper.Upload_Blob(aBlobfile, File4.InputStream, name + "4" + FileExtension, Middle, Small, false)[0];
						}
					}
					if (File5 != null)
					{
						FileExtension = System.IO.Path.GetExtension(File5.FileName);

						//先刪除舊圖片
						BlobHelper.delete(sContainer + "/" + ArticleId, Path.GetFileName(data.Pic5));

						decimal MB = Math.Round(((decimal)File5.ContentLength / 1024) / 1024, 1);

						BlobFileDesc aBlobfile = new BlobFileDesc() { sContainer = sContainer, sFilePath = sImagePath, sFilename = name + "5" + FileExtension };

						//大於1MB
						if (MB > 1)
						{
							Image image = Image.FromStream(File5.InputStream);
							int NewWidth = Convert.ToInt16(Math.Ceiling(image.Width * 0.7));
							int NewHeight = Convert.ToInt16(Math.Ceiling(image.Height * 0.7));
							Middle = new Size(NewWidth, NewHeight);

							while (MB > 1)
							{
								Pic5 = BlobHelper.UploadResize_Blob(image, aBlobfile, Middle);
								MB = (decimal)Session["MB"];
								NewWidth = Convert.ToInt16(Math.Ceiling(NewWidth * 0.7));
								NewHeight = Convert.ToInt16(Math.Ceiling(NewHeight * 0.7));
								Middle = new Size(NewWidth, NewHeight);
							}
							image.Dispose();
						}
						else
						{
							//上傳圖片5
							Pic5 = BlobHelper.Upload_Blob(aBlobfile, File5.InputStream, name + "5" + FileExtension, Middle, Small, false)[0];
						}
					}

					#endregion

					if (Title != "")
					{
						data.Title = Title;
					}
					if (FromUrl != "")
					{
						data.FromUrl = FromUrl;
					}
					if (Pic1 != null)
					{
						data.Pic1 = Pic1;
					}
					if (Content1 != "")
					{
						data.Content1 = HttpUtility.HtmlEncode(Content1);
					}
					if (Pic2 != null)
					{
						data.Pic2 = Pic2;
					}
					if (Content2 != "")
					{
						data.Content2 = HttpUtility.HtmlEncode(Content2);
					}
					if (Pic3 != null)
					{
						data.Pic3 = Pic3;
					}
					if (Content3 != "")
					{
						data.Content3 = HttpUtility.HtmlEncode(Content3);
					}
					if (Pic4 != null)
					{
						data.Pic4 = Pic4;
					}
					if (Content4 != "")
					{
						data.Content4 = HttpUtility.HtmlEncode(Content4);
					}
					if (Pic5 != null)
					{
						data.Pic5 = Pic5;
					}
					if (Content5 != "")
					{
						data.Content5 = HttpUtility.HtmlEncode(Content5);
					}
					if (RecommendSN1 != "")
					{
						data.RecommendSN1 = RecommendSN1;
					}
					if (RecommendSN2 != "")
					{
						data.RecommendSN2 = RecommendSN2;
					}
					if (RecommendSN3 != "")
					{
						data.RecommendSN3 = RecommendSN3;
					}
					if (RecommendSN4 != "")
					{
						data.RecommendSN4 = RecommendSN4;
					}
					if (RecommendSN5 != "")
					{
						data.RecommendSN5 = RecommendSN5;
					}

					data.UpdateTime = DateTime.UtcNow;
					db.SubmitChanges();
				};

				return Json(true, JsonRequestBehavior.AllowGet);
			}
			catch (Exception e)
			{
				return Content("Error");
			}
			finally
			{
				db.Connection.Close();
			}
		}

		//檢查商品編號
		[HttpPost]
		public ActionResult Check_RecommendSN(string RecommendSN = "")
		{
			MithDataContext db = new MithDataContext();
			//上架商品
			int Count = db.Goods_Edit_View.Where(w => w.SN == RecommendSN && w.Status == 3).Count();
			db.Connection.Close();
			if (Count > 0)
			{
				return Json(true, JsonRequestBehavior.AllowGet);
			}
			else
			{
				return Json(false, JsonRequestBehavior.AllowGet);
			}
		}

		//刪除文章
		[HttpPost]
		public ActionResult Delete_Article(int ArticleId = 0)
		{
			MithDataContext db = new MithDataContext();
			try
			{
				//文章
				var Article = db.Article.FirstOrDefault(w => w.Id == ArticleId);
				//文章留言
				var ArticleMessage = db.ArticleMessage.Where(w => w.ArticleId == ArticleId);
				//文章按讚
				var LikeArticle = db.LikeArticle.Where(w => w.ArticleId == ArticleId);

				if (Article != null)
				{
					//刪文章留言
					if (ArticleMessage != null)
					{
						db.ArticleMessage.DeleteAllOnSubmit(ArticleMessage);
						db.SubmitChanges();
					}
					//刪文章按讚
					if (LikeArticle != null)
					{
						db.LikeArticle.DeleteAllOnSubmit(LikeArticle);
						db.SubmitChanges();
					}

					//刪文章
					db.Article.DeleteOnSubmit(Article);
					db.SubmitChanges();
					return Json(true, JsonRequestBehavior.AllowGet);
				}
				else
				{
					return Json(false, JsonRequestBehavior.AllowGet);
				}
			}
			catch (Exception e)
			{
				return Content("Error");
			}
			finally
			{
				db.Connection.Close();
			}
		}

		//新增文章留言
		[HttpPost]
		public ActionResult Insert_ArticleMessage(int ArticleId = 0, int VolunteersId = 0, string MessageTitle = "", string Message = "")
		{
			MithDataContext db = new MithDataContext();
			try
			{
				var item = new ArticleMessage()
				{
					ArticleId = ArticleId,
					VolunteersId = VolunteersId,
					MessageTitle = MessageTitle,
					Message = HttpUtility.HtmlEncode(Message),
					CreateTime = DateTime.UtcNow
				};
				db.ArticleMessage.InsertOnSubmit(item);
				db.SubmitChanges();
				return Json(true, JsonRequestBehavior.AllowGet);

			}
			catch (Exception e)
			{
				return Content("Error");
			}
			finally
			{
				db.Connection.Close();
			}
		}

		//文章留言回覆
		[HttpPost]
		public ActionResult Update_ArticleMessage(int ArticleMessageId = 0, string ReplyMessage = "")
		{
			MithDataContext db = new MithDataContext();
			try
			{
				var data = db.ArticleMessage.FirstOrDefault(f => f.Id == ArticleMessageId);
				if (data != null)
				{
					data.ReplyMessage = ReplyMessage;
					data.ReplyTime = DateTime.UtcNow;
					db.SubmitChanges();
					return Json(true, JsonRequestBehavior.AllowGet);
				}
				else
				{
					return Json(false, JsonRequestBehavior.AllowGet);
				}

			}
			catch (Exception e)
			{
				return Content("Error");
			}
			finally
			{
				db.Connection.Close();
			}
		}

		#endregion

		#region 穿搭諮詢

		//穿搭諮詢資料
		[HttpPost]
		public ActionResult Get_StyleAdviceList(int ExpertId = 0)
		{
			try
			{
				object obj = WebApiMethod.Get_StyleAdviceData(ExpertId);
				if (obj != null)
				{
					return Json(obj, JsonRequestBehavior.AllowGet);
				}
				else
				{
					return Json(false, JsonRequestBehavior.AllowGet);
				}
			}
			catch (Exception e)
			{
				return Content("Error");
			};
		}

		//新增穿搭諮詢
		[HttpPost]
		public ActionResult Insert_StyleAdvice(int ExpertId = 0, int VolunteersId = 0, string MessageTitle = "", string Message = "")
		{
			MithDataContext db = new MithDataContext();
			try
			{
				var item = new StyleAdvice()
				{
					ExpertId = ExpertId,
					VolunteersId = VolunteersId,
					MessageTitle = MessageTitle,
					Message = HttpUtility.HtmlEncode(Message),
					CreateTime = DateTime.UtcNow
				};
				db.StyleAdvice.InsertOnSubmit(item);
				db.SubmitChanges();
				return Json(true, JsonRequestBehavior.AllowGet);

			}
			catch (Exception e)
			{
				return Content("Error");
			}
			finally
			{
				db.Connection.Close();
			}
		}

		//穿搭諮詢回覆
		[HttpPost]
		public ActionResult Update_StyleAdvice(int StyleAdviceId = 0, string ReplyMessage = "")
		{
			MithDataContext db = new MithDataContext();
			try
			{
				var data = db.StyleAdvice.FirstOrDefault(f => f.Id == StyleAdviceId);
				if (data != null)
				{
					data.ReplyMessage = ReplyMessage;
					data.ReplyTime = DateTime.UtcNow;
					db.SubmitChanges();
					return Json(true, JsonRequestBehavior.AllowGet);
				}
				else
				{
					return Json(false, JsonRequestBehavior.AllowGet);
				}

			}
			catch (Exception e)
			{
				return Content("Error");
			}
			finally
			{
				db.Connection.Close();
			}
		}

		#endregion

		#region 私訊

		//私訊資料
		[HttpPost]
		public ActionResult Get_PrivateMessageList(int ExpertId = 0, int VolunteersId = 0)
		{
			try
			{
				object obj = WebApiMethod.Get_PrivateMessageData(ExpertId, VolunteersId);
				if (obj != null)
				{
					return Json(obj, JsonRequestBehavior.AllowGet);
				}
				else
				{
					return Json(false, JsonRequestBehavior.AllowGet);
				}
			}
			catch (Exception e)
			{
				return Content("Error");
			};
		}


		//私訊對話內容
		[HttpPost]
		public ActionResult Get_PrivateMessageDetailList(int PrivateMessageId = 0, string From = "")
		{
			try
			{
				object obj = WebApiMethod.Get_PrivateMessageDetailData(PrivateMessageId, From);
				if (obj != null)
				{
					return Json(obj, JsonRequestBehavior.AllowGet);
				}
				else
				{
					return Json(false, JsonRequestBehavior.AllowGet);
				}
			}
			catch (Exception e)
			{
				return Content("Error");
			};
		}

		//新增私訊
		[HttpPost]
		public ActionResult Insert_PrivateMessage(int ExpertId = 0, int VolunteersId = 0, string Message = "", string From = "")
		{
			MithDataContext db = new MithDataContext();
			try
			{
				var data = db.PrivateMessageGroup.FirstOrDefault(f => f.ExpertId == ExpertId && f.VolunteersId == VolunteersId);
				//沒有此達人跟此會員的對話群組
				if (data == null)
				{
					var PrivateMessageGroup = new PrivateMessageGroup()
					{
						ExpertId = ExpertId,
						VolunteersId = VolunteersId,
					};
					db.PrivateMessageGroup.InsertOnSubmit(PrivateMessageGroup);
					db.SubmitChanges();

					int PrivateMessageGroupId = db.PrivateMessageGroup.OrderByDescending(o => o.Id).FirstOrDefault().Id;

					var PrivateMessageContent = new PrivateMessageContent()
					{
						PrivateMessageGroupId = PrivateMessageGroupId,
						Message = HttpUtility.HtmlEncode(Message),
						From = From,
						Read = false,
						ReadTime = null,
						CreateTime = DateTime.UtcNow
					};
					db.PrivateMessageContent.InsertOnSubmit(PrivateMessageContent);
					db.SubmitChanges();
				}
				//有此達人跟此會員的對話群組，只新增對話內容
				else
				{
					var PrivateMessageContent = new PrivateMessageContent()
					{
						PrivateMessageGroupId = data.Id,
						Message = HttpUtility.HtmlEncode(Message),
						From = From,
						CreateTime = DateTime.UtcNow
					};
					db.PrivateMessageContent.InsertOnSubmit(PrivateMessageContent);
					db.SubmitChanges();
				}
				return Json(true, JsonRequestBehavior.AllowGet);
			}
			catch (Exception e)
			{
				return Content("Error");
			}
			finally
			{
				db.Connection.Close();
			}
		}


		#endregion

		#region 訂單

		//已結單訂單
		[HttpPost]
		public ActionResult Get_FinalOrderList(int VolunteersId = 0)
		{
			try
			{
				object obj = WebApiMethod.Get_FinalOrderData(VolunteersId);
				if (obj != null)
				{
					return Json(obj, JsonRequestBehavior.AllowGet);
				}
				else
				{
					return Json(false, JsonRequestBehavior.AllowGet);
				}
			}
			catch (Exception e)
			{
				return Content("Error");
			};
		}

		//全部訂單
		[HttpPost]
		public ActionResult Get_OrderDeatailList(int VolunteersId = 0, int OrderId = 0, string Pic_Size = "")
		{
			try
			{
				object obj = WebApiMethod.Get_OrderDetailData(VolunteersId, OrderId, Pic_Size);
				if (obj != null)
				{
					return Json(obj, JsonRequestBehavior.AllowGet);
				}
				else
				{
					return Json(false, JsonRequestBehavior.AllowGet);
				}
			}
			catch (Exception e)
			{
				return Content("Error");
			};
		}

		//訂單不會真的成立
		[HttpPost]
		public ActionResult Insert_Order(int VolunteersId = 0, string GoodsShoppingCart = "", int App_TotalPrice = 0, bool LimitOrder = false)
		{
			MithDataContext db = new MithDataContext();
			//正物流訂單最後一碼為1
			string Radom = Method.RandomKey(4, true, false, false, false) + "1";
			string SN = DateTime.Now.ToString("yyyyMMdd").Substring(2, 6) + Radom;
			int Id = 0;
			List<object> obj = new List<object>();

			try
			{
				List<WebApiMethod.OrderDetailClass> OrderDetail_Json = JsonConvert.DeserializeObject<List<WebApiMethod.OrderDetailClass>>(GoodsShoppingCart);

				#region 檢查商品庫存
				string CheckBuyQuantity = "";

				//商品庫存
				int TotalQuantity = 0;
				//商品總額
				int TotalPrice = 0;

				foreach (var item in OrderDetail_Json)
				{
					var GoodsQuantity = db.GoodsQuantity.FirstOrDefault(f => f.GoodsId == item.GoodsId && f.Size == item.BuySize);
					string Goods_Name = db.Goods_Edit_View.FirstOrDefault(f => f.Id == item.GoodsId).Name;
					int Goods_Type = Goods_Type = db.Goods_Edit_View.FirstOrDefault(f => f.Id == item.GoodsId).Type.Value;

					//商品底下有這個尺寸
					if (GoodsQuantity != null)
					{
						//集單商品不檢查
						if (Goods_Type != 3)
						{
							//數量 = 商品數量 - 購買數量
							TotalQuantity = (GoodsQuantity.Quantity ?? 0) - item.BuyQuantity;

							//商品數量 < 0 代表不足
							if (TotalQuantity < 0)
							{
								obj.Add(new { GoodsId = item.GoodsId, Goods_Name = Goods_Name, BuySize = item.BuySize });
								CheckBuyQuantity += "0";
							}
						}
					}
					//商品沒這個尺寸
					else
					{
						obj.Add(new { GoodsId = item.GoodsId, Goods_Name = Goods_Name, BuySize = item.BuySize });
						CheckBuyQuantity += "0";
					}
				}
				#endregion

				//商品數量都足夠
				if (!string.IsNullOrWhiteSpace(SN) && CheckBuyQuantity == "")
				{
					//訂單Id
					Id = db.Order.OrderByDescending(o => o.Id).FirstOrDefault() != null ? db.Order.OrderByDescending(o => o.Id).FirstOrDefault().Id + 1 : 1;

					#region 新增訂單主檔
					var Order = new Order()
					{
						Id = Id,
						VolunteersId = VolunteersId,
						SN = SN,
						Direction = 1,
						LimitOrder = LimitOrder,
						Status = -1, //未成立
						LogisticsType = 0,
						CashFlowStatus = 1,
						CathayStatus = "9999",
						ReceiverName = "",
						ReceiverTel = "",
						ReceiverAddress = "",
						PrintPaper = false,
						CreateTime = DateTime.UtcNow,
						UpdateTime = DateTime.UtcNow,
						LastUserId = 0,
					};
					db.Order.InsertOnSubmit(Order);
					db.SubmitChanges();
					#endregion

					#region 新增訂單細節
					foreach (var item2 in OrderDetail_Json)
					{
						//取得商品資訊
						var Goods = db.Goods.FirstOrDefault(f => f.Id == item2.GoodsId);

						var GoodsQuantity = db.GoodsQuantity.FirstOrDefault(f => f.GoodsId == item2.GoodsId && f.Size == item2.BuySize);
						if (GoodsQuantity != null)
						{
							//非集單商品才減掉庫存，增加銷售量
							if (Goods.Type != 3)
							{
								//商品庫存
								GoodsQuantity.Quantity -= item2.BuyQuantity;
								//商品銷售量
								if (GoodsQuantity.SellQuantity == null)
								{
									GoodsQuantity.SellQuantity = 0;
								}
								GoodsQuantity.SellQuantity += item2.BuyQuantity;
								db.SubmitChanges();
							}
							//集單商品
							else
							{
								//商品集單量
								GoodsQuantity.LimitOrderQuantity += item2.BuyQuantity;
								db.SubmitChanges();
							}
						}

						//價格 = 商品價格 * 購買數量
						int BuyTotal = 0;
						int BuyPrice = 0;
						if (Goods.SpecialOffer != null && Goods.SpecialOffer > 0)
						//商品有特價
						{
							BuyTotal = Goods.SpecialOffer.Value * item2.BuyQuantity;
							BuyPrice = Goods.SpecialOffer.Value;
						}
						else
						{
							BuyTotal = Goods.Price * item2.BuyQuantity;
							BuyPrice = Goods.Price;
						}

						var OrderDetail = new OrderDetail()
						{
							OrderId = Id,
							GoodsId = Goods.Id,
							GoodsType = Goods.Type.Value,
							CompanyId = Goods.CompanyId,
							BrandId = Goods.BrandId,
							BuySize = item2.BuySize,
							BuyQuantity = item2.BuyQuantity,
							BuyPrice = BuyPrice,
							BuyTotal = BuyTotal,
							LogisticsSN = "",
							LogisticsCompany = 0,
							LogisticsStatus = -1,
							ShippingDeadline = DateTime.UtcNow,
							CreateTime = DateTime.UtcNow,
							UpdateTime = DateTime.UtcNow,
							LastUserId = 0,
						};
						db.OrderDetail.InsertOnSubmit(OrderDetail);
						db.SubmitChanges();
						TotalPrice += BuyTotal;
					}

					#endregion

					TotalPrice = 1;
					App_TotalPrice = 1;

					#region 計算訂單總和
					var data2 = db.Order.FirstOrDefault(f => f.Id == Id);
					if (data2 != null)
					{
						data2.Total = TotalPrice;
						db.SubmitChanges();
					}
					//前後台計算金額相同
					if (TotalPrice == App_TotalPrice)
					{
						obj.Clear();
						obj.Add(new { OrderId = Id, OrderSN = SN });
					}
					else
					{
						obj.Add(new { OrderId = 0, OrderSn = "" });
						WebApiMethod.Delete_Order(Id);
					}
					#endregion
				}
				return Json(obj, JsonRequestBehavior.AllowGet);
			}
			catch (Exception e)
			{
				//刪掉訂單
				WebApiMethod.Delete_Order(Id);
				return Content("Error");
			}
			finally
			{
				db.Connection.Close();
			}
		}

		//訂單真的成立
		[HttpPost]
		public ActionResult Update_Order(int OrderId = 0, int LogisticsType = 0, int CashFlowType = 0, string ReceiverName = "", string ReceiverTel = "", string ReceiverAddress = "", string ReceiverStoreId = "", string ReceiverStoreName = "", string ReceiverStoreAddress = "", string TaxId = "", bool PrintPaper = false, bool ContributeReceipt = false, int ExpertId = 0)
		{
			MithDataContext db = new MithDataContext();
			var filename = AppDomain.CurrentDomain.BaseDirectory + "Log.txt";
			var sw = new System.IO.StreamWriter(filename, true);
			sw.WriteLine(DateTime.UtcNow.AddHours(8).ToString("yyyy/MM/dd HH:mm:ss") + "   " + "Action Start");
			try
			{
				string VirtualAccount = null;
				string BuyGoodsStr = "\n";
				var Order = db.Order.FirstOrDefault(f => f.Id == OrderId);
				string Volunteers_Email = "";
				if (Order != null)
				{
					Volunteers_Email = db.Volunteers.FirstOrDefault(f => f.Id == Order.VolunteersId).Email;

					#region 更新訂單主檔

					Order.Status = 1; //未結單
					Order.LogisticsType = LogisticsType; //物流方式
					Order.CashFlowType = CashFlowType; //金流方式

					//付款方式為ATM轉帳，國泰狀態 = Null
					if (CashFlowType == 1)
					{
						Order.CathayStatus = null;
						//產生虛擬帳號
						VirtualAccount = WebApiMethod.Generate_VirtualAccount(Order.Total.ToString());
						Order.VirtualAccount = VirtualAccount;
					}
					//付款方式為信用卡，金流狀態 = 已付款
					else
					{
						Order.CashFlowStatus = 2; //已付款
						Order.PaymentTime = DateTime.UtcNow;
					}
					Order.ReceiverName = ReceiverName;
					Order.ReceiverTel = ReceiverTel;
					Order.ReceiverAddress = ReceiverAddress;
					if (ReceiverStoreId != "")
					{
						Order.ReceiverStoreId = ReceiverStoreId;
						Order.ReceiverStoreName = ReceiverStoreName;
						Order.ReceiverStoreAddress = ReceiverStoreAddress;
					}

					Order.TaxId = TaxId;
					Order.PrintPaper = PrintPaper;
					Order.ContributeReceipt = ContributeReceipt;
					if (ExpertId > 0)
					{
						Order.ExpertId = ExpertId;
					}
					Order.CreateTime = DateTime.UtcNow;
					Order.UpdateTime = DateTime.UtcNow;
					db.SubmitChanges();

					#endregion

					#region 更新訂單細節

					var OrderDetail = db.OrderDetail.Where(f => f.OrderId == OrderId);
					int FewDaysShipping = 0;

					foreach (var item in OrderDetail)
					{
						//取得商品資料
						var Goods = db.Goods_Edit_View.FirstOrDefault(f => f.Id == item.GoodsId);
						//商品幾天出貨，預設3天
						FewDaysShipping = Goods.FewDaysShipping ?? 3;

						item.LogisticsCompany = LogisticsType; //目前物流公司 = 物流方式
						item.LogisticsStatus = -1; //未接單

						//商品為集單
						if (Goods.Type == 3)
						{
							//出貨期限 = 集單結束日期 + 商品幾天出貨
							item.ShippingDeadline = Goods.LimitOrderEnd.Value.AddDays(FewDaysShipping);

						}
						//非集單
						else
						{
							DateTime Now = DateTime.Parse(DateTime.Now.ToString("yyyy/MM/dd") + " 23:59:00");

							//出貨期限 = 現在 + 商品幾天出貨
							item.ShippingDeadline = Now.AddDays(FewDaysShipping);
						}
						item.CalculateTotal = Method.Get_CalculateTotal(CashFlowType, item.BuyTotal);
						item.CreateTime = DateTime.UtcNow;
						item.UpdateTime = DateTime.UtcNow;
						db.SubmitChanges();

						BuyGoodsStr += "【" + Goods.SN + "】" + Goods.Name + "（" + item.BuySize + " × " + item.BuyQuantity + "）\n";

						#region 刪除購物車商品

						var GoodsShoppingCart = db.GoodsShoppingCart.FirstOrDefault
								(w =>
								w.VolunteersId == Order.VolunteersId &&
								w.GoodsId == item.GoodsId &&
								w.BuySize == item.BuySize &&
								w.BuyQuantity == item.BuyQuantity
								);
						if (GoodsShoppingCart != null)
						{
							db.GoodsShoppingCart.DeleteOnSubmit(GoodsShoppingCart);
							db.SubmitChanges();
						}
						#endregion

					}
					#endregion
				}

				List<object> obj = new List<object>();

				#region 呈現給App完成訂單的文字

				string BankCode = "";
				string DeadLineRemark = "";
				string CashFlowType_Name = db.Category.FirstOrDefault(f => f.Fun_Id == 10 && f.Group == "CashFlowType" && f.Number == Order.CashFlowType).Name;

				//付款方式為ATM轉帳
				if (Order.CashFlowType == 1)
				{
					BankCode = "013 國泰世華";
					DeadLineRemark = "請於 " + Order.CreateTime.AddHours(8).AddDays(3).ToString("yyyy/MM/dd HH:mm") + " 前匯款至以下ATM帳戶，逾期訂單將自動取消";
				}
				//付款方式為信用卡
				else
				{
					//跟國泰請款
					WebApiMethod.Request_CreditCard(Server, Order.SN, Order.Total.ToString(), Order.CathayAuthCode);
				}

				obj.Add(new
				{
					VolunteersEmail = Volunteers_Email,
					OrderSN = Order.SN,
					CashFlowType = Order.CashFlowType,
					CashFlowType_Name = CashFlowType_Name,
					DeadLineRemark = DeadLineRemark,
					BankCode = BankCode,
					VirtualAccount = VirtualAccount,
					Total = Order.Total,
					MithRemark = "Mith不會以任何方式請您變更付款方式" + "\n若有任何問題，可連繫MiTH客服\nmith@mix-with.com"
				});

				#endregion

				#region 寄送訂單成立Email（會員）
				//取得會員Email
				string Volunteers_Name = db.Volunteers.FirstOrDefault(f => f.Id == Order.VolunteersId).RealName;
				string EmailContent = "";
				//付款方式為ATM轉帳
				if (CashFlowType == 1)
				{
					VirtualAccount = WebApiMethod.Generate_VirtualAccount(Order.Total.ToString());
					EmailContent = HttpUtility.HtmlDecode("您好\n本次訂單編號：" + Order.SN + "\n付款方式：ATM轉帳" + "\n訂購商品：" + BuyGoodsStr + "\n請於 " + Order.CreateTime.AddHours(8).AddDays(3).ToString("yyyy/MM/dd HH:mm:ss") + " 前匯款至以下ATM帳戶，逾期訂單將自動取消\n===================================================================" + "\n銀行代號：013(國泰世華)" + "\n虛擬帳號：" + VirtualAccount + "\n金　　額：" + String.Format("{0:n0}", Order.Total) + "元\n===================================================================" + "\n MiTH不會請您變更付款方式" + "\n若有任何問題，可連繫MiTH客服：mith@mix-with.com\n");
				}
				//付款方式為信用卡
				else
				{
					EmailContent = HttpUtility.HtmlDecode("您好\n您的訂單已成立並付款完成，廠商將盡快出貨給您\n本次訂單編號：" + Order.SN + "\n付款方式：信用卡付款" + "\n訂購會員：" + Volunteers_Name.Substring(0, Volunteers_Name.Length - 1) + "*" + "\n訂購商品：" + BuyGoodsStr + "\n===================================================================\nMiTH不會請您變更付款方式" + "\n若有任何問題，可連繫MiTH客服：mith@mix-with.com\n");
				}


				Task t1 = Task.Run(() =>
				{
					Method.Send_Mail(Volunteers_Email, "MiTH App 訂單成立通知 [訂單編號" + Order.SN + "]", EmailContent);
				});

				#endregion

				#region 寄送訂單成立Email（廠商）

				Task t2 = Task.Run(() =>
				{
					WebApiMethod.Send_OrderMail(Order.CreateTime.AddHours(8), OrderId, Order.SN, Order.VolunteersId, CashFlowType_Name);
				});
				#endregion
				return Json(obj, JsonRequestBehavior.AllowGet);
			}
			catch (Exception e)
			{
				sw.WriteLine(DateTime.UtcNow.AddHours(8).ToString("yyyy/MM/dd HH:mm:ss") + "   " + "Error：" + e.ToString());
				return Content("Error");
			}
			finally
			{
				db.Connection.Close();
				sw.WriteLine("---------------------------------------------------");
				sw.Close();
			}

		}



		//取消訂單
		public ActionResult Cancel_Order(int OrderId = 0)
		{
			MithDataContext db = new MithDataContext();
			VolunteersModel model = new VolunteersModel();
			try
			{
				//判斷會員是否存在
				var Order = db.Order.FirstOrDefault(f => f.Id == OrderId);
				var OrderDetail = db.OrderDetail.Where(f => f.OrderId == OrderId);
				DateTime Now = DateTime.UtcNow.AddHours(8);
				if (Order != null)
				{
					var Volunteers = db.Volunteers.FirstOrDefault(f => f.Id == Order.VolunteersId);

					//訂單主檔狀態 = 已取消
					Order.Status = -999;
					db.SubmitChanges();

					string BuyGoodsStr = "\n";
					foreach (var item in OrderDetail)
					{
						//訂單細節物流狀態 = 已取消
						item.LogisticsStatus = -999;
						db.SubmitChanges();

						//取得商品資訊
						var Goods = db.Goods.FirstOrDefault(f => f.Id == item.GoodsId);

						var GoodsQuantity = db.GoodsQuantity.FirstOrDefault(f => f.GoodsId == item.GoodsId && f.Size == item.BuySize);

						//商品底下有這個尺寸
						if (GoodsQuantity != null)
						{
							//非集單商品才加掉庫存，扣掉銷售量
							if (Goods.Type != 3)
							{
								//商品庫存
								GoodsQuantity.Quantity += item.BuyQuantity;
								//商品銷售量
								GoodsQuantity.SellQuantity -= item.BuyQuantity;
								db.SubmitChanges();
							}
							//集單商品
							else
							{
								//商品集單量
								GoodsQuantity.LimitOrderQuantity -= item.BuyQuantity;
								db.SubmitChanges();
							}
						}
						BuyGoodsStr += "【" + Goods.SN + "】" + Goods.Name + "（" + item.BuySize + " × " + item.BuyQuantity + "）\n";
					}

					#region 發送取消訂單Email（會員）
					Task t1 = Task.Run(() =>
					{
						string EmailContent = HttpUtility.HtmlDecode("您好，您的訂單已取消成功\n取消時間：" + Now.ToString("yyyy/MM/dd HH:mm") + "\n訂單編號：" + Order.SN + "\n訂購會員：" + Volunteers.RealName.Substring(0, Volunteers.RealName.Length - 1) + "*" + "\n訂購商品：" + BuyGoodsStr + "\n===================================================================\n若有任何問題，可連繫MiTH客服：mith@mix-with.com");

						Method.Send_Mail(Volunteers.Email, "MiTH App 訂單取消成功 [訂單編號" + Order.SN + "]", EmailContent);
					});
					#endregion


					var OrderDetail2 = db.OrderDetail_Index_View.Where(w => w.OrderId == Order.Id).OrderBy(o => o.CompanyId);

					#region 發送繳費成功Email（廠商）
					Task t2 = Task.Run(() =>
					{
						Session["CompanyId"] = 0;
						foreach (var item in OrderDetail2)
						{
							//CompanyId不同才寄信
							if ((int)Session["CompanyId"] != item.CompanyId)
							{
								string Company_Email = db.Company.FirstOrDefault(f => f.Id == item.CompanyId).ContactEmail != null ? db.Company.FirstOrDefault(f => f.Id == item.CompanyId).ContactEmail : "";

								//只取得訂購此廠商的商品
								var CompanyOrder = OrderDetail2.Where(w => w.CompanyId == item.CompanyId);
								BuyGoodsStr = "\n";
								foreach (var item2 in CompanyOrder)
								{
									BuyGoodsStr += "【" + item2.Goods_SN + "】" + item2.Goods_Name + "（" + item2.BuySize + " × " + item2.BuyQuantity + "）\n";
								}

								string EmailContent2 = HttpUtility.HtmlDecode("您好,此筆訂單已於 " + Now.ToString("yyyy/MM/dd HH:mm") + " 取消" + "\n\n訂單編號：" + Order.SN + "\n訂購會員：" + Volunteers.RealName.Substring(0, Volunteers.RealName.Length - 1) + "*" + "\n付款方式：ATM轉帳" + "\n訂購商品：" + BuyGoodsStr + "\n可至後台確認。");

								//發送Email
								Method.Send_Mail(Company_Email, "MiTH App 訂單取消通知 [訂單編號" + Order.SN + "]", EmailContent2);
							}
							Session["CompanyId"] = (int)item.CompanyId;
						}
						//清空Session
						Session["CompanyId"] = null;
					});
					#endregion

					return Json(true, JsonRequestBehavior.AllowGet);
				}
				else
				{
					return Json(false, JsonRequestBehavior.AllowGet);
				}
			}
			catch (Exception e)
			{
				return Content("Error");
			}
			finally
			{
				db.Connection.Close();
			}
		}

		//取得訂單 Q&A
		[HttpPost]
		public ActionResult Get_OrderQAList(int OrderId = 0, int VolunteersId = 0, int CompanyId = 0)
		{

			try
			{
				object obj = WebApiMethod.Get_OrderQAData(OrderId, VolunteersId, CompanyId);
				if (obj != null)
				{
					return Json(obj, JsonRequestBehavior.AllowGet);
				}
				else
				{
					return Json(false, JsonRequestBehavior.AllowGet);
				}
			}
			catch (Exception e)
			{
				return Content("Error");
			};
		}

		//新增訂單 Q&A
		[HttpPost]
		public ActionResult Insert_OrderQA(int OrderId = 0, int VolunteersId = 0, int CompanyId = 0, string MessageTitle = "", string Message = "")
		{

			MithDataContext db = new MithDataContext();
			try
			{
				var data = new OrderQA()
				{
					OrderId = OrderId,
					VolunteersId = VolunteersId,
					MessageTitle = MessageTitle,
					Message = HttpUtility.HtmlEncode(Message),
					CreateTime = DateTime.UtcNow,
					ReplyMessage = null,
					ReplyTime = null,
					ReplyUserId = 0,
					CompanyId = CompanyId,
				};
				db.OrderQA.InsertOnSubmit(data);
				db.SubmitChanges();

				Task t1 = Task.Run(() =>
				{
					var Order = db.Order_API_View.FirstOrDefault(f => f.OrderId == OrderId);

					string EmailContent = HttpUtility.HtmlDecode("您好，訂單有一則提問\n訂單編號：" + Order.SN + "\n提問會員：" + Order.Volunteers_Name + "\n提問標題：" + MessageTitle + "\n提問內容：" + HttpUtility.HtmlEncode(Message) + "\n請至後台確認並回覆");

					Method.Send_Mail(Order.Volunteers_Email, "MiTH App 訂單有一則提問 [訂單編號" + Order.SN + "]", EmailContent);
				});
				return Json(true, JsonRequestBehavior.AllowGet);
			}
			catch (Exception e)
			{
				return Content("Error");
			}
			finally
			{
				db.Connection.Close();
			}
		}

		#endregion

		#region 物流

		/// <summary>
		/// 退貨
		/// </summary>
		[HttpPost]
		public ActionResult ReturnGoods(int ReturnOrderId = 0, int ReturnQuantity = 0, string ReceiverAddress = "", int ReceiveTimeRange = 0, string Remark = "", int App_TotalPrice = 0)
		{
			MithDataContext db = new MithDataContext();
			OrderModel data = new OrderModel();
			int New_OrderId = 0;
			try
			{
				var Old_OrderDetail = db.OrderDetail.FirstOrDefault(f => f.Id == ReturnOrderId);
				int ReturnTotal = 0;

				ReturnTotal = Old_OrderDetail.BuyPrice * ReturnQuantity;

				//前後台計算金額相同
				if (ReturnTotal == App_TotalPrice)
				{
					//新增退貨的訂單主檔
					New_OrderId = data.Insert_ReturnGoods_Order(1, 0, 0, ReceiverAddress, ReceiveTimeRange, Remark, Old_OrderDetail.OrderId, 0);

					#region 新增退貨的訂單細節
					var New_OrderDetail = new OrderDetail()
					{
						OrderId = New_OrderId,
						GoodsId = Old_OrderDetail.GoodsId,
						GoodsType = Old_OrderDetail.GoodsType,
						CompanyId = Old_OrderDetail.CompanyId,
						BrandId = Old_OrderDetail.BrandId,
						BuySize = Old_OrderDetail.BuySize,
						BuyQuantity = ReturnQuantity,
						BuyPrice = Old_OrderDetail.BuyPrice,
						BuyTotal = ReturnTotal,
						LogisticsSN = "", //由後台人員Key物流編號
						LogisticsCompany = Old_OrderDetail.LogisticsCompany,
						LogisticsStatus = 3, //退貨中
						CreateTime = DateTime.UtcNow,
						UpdateTime = DateTime.UtcNow,
						LastUserId = 0,
					};
					db.OrderDetail.InsertOnSubmit(New_OrderDetail);
					db.SubmitChanges();

					#endregion

					#region 計算訂單總和
					var New_Order = db.Order.FirstOrDefault(f => f.Id == (int)New_OrderId);
					if (New_Order != null)
					{
						New_Order.Total = ReturnTotal;
						db.SubmitChanges();
					}
					#endregion

					return Json(true, JsonRequestBehavior.AllowGet);
				}
				//前後台計算金額不符
				else
				{
					//刪掉訂單
					WebApiMethod.Delete_Order((int)New_OrderId);
					return Json(false, JsonRequestBehavior.AllowGet);
				}
			}
			catch (Exception e)
			{
				//刪掉訂單
				WebApiMethod.Delete_Order((int)New_OrderId);
				return Content("Error");
			}
			finally
			{
				db.Connection.Close();
			}

		}

		#endregion

		#region 購物須知
		public ActionResult Get_ShoppingExplain()
		{
			MithDataContext db = new MithDataContext();
			try
			{

				string ShoppingExplain = db.Other.FirstOrDefault(f => f.Id == 17).Content;
				List<object> obj = new List<object>();
				obj.Add(new
				{
					ShoppingExplain = HttpUtility.HtmlDecode(ShoppingExplain)
				});
				return Json(obj, JsonRequestBehavior.AllowGet);
			}
			catch (Exception e)
			{
				return Content("Error");
			}
			finally
			{
				db.Connection.Close();
			}
		}

		#endregion

	}
}
