﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using Mith.Models;
using LinqToExcel;

namespace Mith.Infrastructure.Helpers
{
    public partial class ImportClass
    {
        public string SN { get; set; }
    }
    public class ImportDataHelper
    {
        /// <summary>
        /// 檢查匯入的 xlsx資料.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <param name="importSN">The import zip codes.</param>
        /// <returns></returns>
        public CheckResult CheckImportData(
            string fileName,
            List<ImportClass> importSN)
        {
            var result = new CheckResult();

            var targetFile = new FileInfo(fileName);

            if (!targetFile.Exists)
            {
                result.ID = Guid.NewGuid();
                result.Success = false;
                result.ErrorCount = 0;
                result.ErrorMessage = "匯入的資料檔案不存在";
                return result;
            }

            var excelFile = new ExcelQueryFactory(fileName);

            //欄位對映
            excelFile.AddMapping<ImportClass>(x => x.SN, "序號");


            //SheetName
            var excelContent = excelFile.Worksheet<ImportClass>("促銷碼管理");
            int errorCount = 0;
            int rowIndex = 1;
            var importErrorMessages = new List<string>();

            //檢查資料
            foreach (var row in excelContent)
            {

                var errorMessage = new StringBuilder();
                var GoodsSN_Setting = new ImportClass();

                //CityName
                if (string.IsNullOrWhiteSpace(row.SN))
                {
                    errorMessage.Append("不能為空白 ");
                }
                else
                {
                    bool Check = IsNumOrEn(row.SN);

                    MithDataContext db = new MithDataContext();
                    var Check_Repeat = db.GoodsSN_Setting.FirstOrDefault(w => w.SN == row.SN);
                    db.Connection.Close();

                    //if (row.SN.Length == 15 && Check == true)
                    //{
                    //}
                    //else 
                    if (row.SN.Length == 15 && Check == false)
                    {
                        errorMessage.Append("包含特殊字元");
                    }
                    else if (row.SN.Length > 15 && Check == true)
                    {
                        errorMessage.Append("大於15位數");
                    }
                    else if (row.SN.Length > 15 && Check == false)
                    {
                        errorMessage.Append("大於15位數，且包含特殊字元");
                    }
                    else if (row.SN.Length < 15 && Check == true)
                    {
                        errorMessage.Append("小於15位數");
                    }
                    else if (Check_Repeat != null)
                    {
                        errorMessage.Append("重複資料");
                    }
                    else if (row.SN.Length < 15 && Check == false)
                    {
                        errorMessage.Append("小於15位數，且包含特殊字元");
                    }
                }

                GoodsSN_Setting.SN = row.SN;
                //=============================================================================
                if (errorMessage.Length > 0)
                {
                    errorCount += 1;
                    importErrorMessages.Add(string.Format(
                        "第 {0} 列資料發現錯誤：{1}{2}",
                        rowIndex,
                        errorMessage,
                        "<br/>"));
                }
                importSN.Add(GoodsSN_Setting);
                rowIndex += 1;
            }

            try
            {
                result.ID = Guid.NewGuid();
                result.Success = errorCount.Equals(0);
                result.RowCount = importSN.Count;
                result.ErrorCount = errorCount;

                string allErrorMessage = string.Empty;

                foreach (var message in importErrorMessages)
                {
                    allErrorMessage += message;
                }

                result.ErrorMessage = allErrorMessage;

                return result;
            }
            catch (Exception ex)
            {
                throw;
            }
        }


        /// <summary>
        /// Saves the import data.
        /// </summary>
        /// <param name="importZipCodes">The import zip codes.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void SaveImportData(IEnumerable<ImportClass> importSN, string fileName = "")
        {
            MithDataContext db = new MithDataContext();
            try
            {
                foreach (var item in importSN)
                {
                    var Repeat_Code = db.GoodsSN_Setting.FirstOrDefault(f => f.SN == item.SN);

                    if (Repeat_Code != null)
                    {
                        continue;
                    }


                    GoodsSN_Setting new_item = new GoodsSN_Setting
                    {
                        SN = item.SN,
                        CreateTime = DateTime.UtcNow,
                        UpdateTime = DateTime.UtcNow,
                        LastUserId = 0
                    };
                    db.GoodsSN_Setting.InsertOnSubmit(new_item);
                    db.SubmitChanges();
                    db.Connection.Close();
                }
                if (File.Exists(fileName))
                {
                    File.Delete(fileName);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                db.Connection.Close();
            }
        }

        public static bool IsAlphaNumeric(String str)
        {
            return (str != string.Empty && !Regex.IsMatch(str, "[^a-zA-Z0-9]"))
                ? true : false;
        }

        public bool IsNumOrEn(string str)
        {
            System.Text.RegularExpressions.Regex reg1 = new System.Text.RegularExpressions.Regex(@"^[A-Za-z0-9]+$");
            return reg1.IsMatch(str);
        }
    }
}